@if(isset($button_mode))
<div style="display:inline-flex">
    @if (isset($set_as_completed))
        <a href="{!! $set_as_completed !!}" class="btn btn-xs btn-success ignore-click set-status"><i class="fa fa-check"></i></a>
    @endif
    @if (isset($set_as_rejected))
        <a href="{!! $set_as_rejected !!}" class="btn btn-xs btn-danger ignore-click set-status"><i class="fa fa-remove"></i></a>
    @endif
    @if (isset($set_as_onprogress))
        <a href="{!! $set_as_onprogress !!}" class="btn btn-xs btn-primary ignore-click  set-status"><i class="fa fa-repeat"></i></a>
    @endif
    @if (isset($is_printed))
        <a href="{!! $is_printed !!}" class="btn btn-xs btn-default ignore-click set-completed">
            <i class="icon-ship"></i>
        </a>
    @endif
</div>
@else
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($view_package))
                <li><a href="{!! $view_package !!}" target="_blank"><i class="icon-stack2"></i> View Package</a></li>
            @endif
            @if (isset($upload))
                <li><a href="{!! $upload !!}" class="ignore-click upload"><i class="icon-upload4"></i> Upload</a></li>
            @endif
            @if (isset($upload_cancel))
                <li><a href="{!! $upload_cancel !!}" class="ignore-click upload_cancel"><i class="icon-upload"></i> Upload</a></li>
            @endif
            
            @if (isset($detail))
                <li><a href="{!! $detail !!}" target="_blank"><i class="icon-stack2"></i> Detail</a></li>
            @endif
            @if (isset($revisi))
                <li><a href="{!! $revisi !!}" class="ignore-click revisi"><i class="glyphicon glyphicon-repeat"></i> Revisi</a></li>
            @endif
            @if (isset($editpl))
                <li><a href="{!! $editpl !!}" target="_blank" ><i class="icon-pencil7"></i> Edit PL</a></li>
            @endif
            @if (isset($costco))
                <li><a href="{!! $costco !!}" ><i class="glyphicon glyphicon-upload "></i> Upload Costco</a></li>
            @endif
            @if (isset($detcostco))
                <li><a href="{!! $detcostco !!}" target="_blank"><i class="glyphicon glyphicon-list-alt" ></i> Detail Costco</a></li>
            @endif
            @if (isset($barcode))
                <li><a href="{!! $barcode !!}" target="_blank"><i class="icon-barcode2"></i> Barcode</a></li>
            @endif
            @if (isset($print))
                <li><a href="{!! $print !!}" target="_blank"><i class="icon-printer4"></i> Print</a></li>
            @endif
            @if (isset($set_as_completed))
                <li><a href="{!! $set_as_completed !!}" class="ignore-click set-status"><i class="icon-checkmark4"></i> Set As Completed</a></li>
            @endif
            @if (isset($set_as_rejected))
                <li><a href="{!! $set_as_rejected !!}" class="ignore-click set-status"><i class="icon-cross2"></i> Set As Rejected</a></li>
            @endif
            @if (isset($set_as_onprogress))
                <li><a href="{!! $set_as_onprogress !!}" class="ignore-click set-status"><i class="icon-reload-alt"></i> Set As On Progress</a></li>
            @endif
            @if (isset($location))
                <li><a href="{!! $location !!}" target="_blank"><i class="icon-file-eye"></i>View Location</a></li>
            @endif
            @if (isset($print_rack))
               <li><a href="{!! $print_rack !!}" target="_blank"><i class="icon-printer4"></i> Print</a></li>
            @endif
            @if (isset($edit))
               <li><a href="{!! $edit !!}"><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            <!-- @if (isset($delete))
                <li><a href="{{ $delete }}" data-id="{{ isset($userid) ? $userid : 'kosong' }}" data-roleid="{{ isset($roleid) ? $roleid : 'kosong' }}" class="ignore-click deleteUser deleterole"><i class="icon-close2"></i> Delete</a></li>
            @endif -->
            @if (isset($delete_role))
                <li><a href="{{ $delete_role }}" data-roleid="{{ isset($roleid) ? $roleid : 'kosong' }}" class="ignore-click deleteRole"><i class="icon-close2"></i> Delete</a></li>
            @endif
            @if (isset($delete_user))
                <li><a href="{{ $delete_user }}" data-userid="{{ isset($userid) ? $userid : 'kosong' }}" class="ignore-click deleteUser"><i class="icon-close2"></i> Delete</a></li>
            @endif
            @if (isset($delete_area))
                <li><a href="{{ $delete_area }}" data-areaid="{{ isset($areaid) ? $areaid : 'kosong' }}" class="ignore-click deleteArea"><i class="icon-close2"></i> Delete</a></li>
            @endif
            @if (isset($delete_location))
                <li><a href="{{ $delete_location }}" data-locationid="{{ isset($locationid) ? $locationid : 'kosong' }}" class="ignore-click deleteLocation"><i class="icon-close2"></i> Delete</a></li>
            @endif
            @if (isset($delete_sewing))
                <li><a href="{{ $delete_sewing }}" data-sewingid="{{ isset($sewingid) ? $sewingid : 'kosong' }}" class="ignore-click deleteSewing"><i class="icon-close2"></i> Delete</a></li>
            @endif
            @if (isset($edit_modal))
                <li><a href="#" onclick="edit('{!! $edit_modal !!}')" ><i class="icon-pencil6"></i> Edit</a></li>
            @endif

        </ul>
    </li>
</ul>
@endif
