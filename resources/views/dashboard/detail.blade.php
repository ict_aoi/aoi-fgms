@extends('layouts.app', ['active' => 'dashboard'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">PACKAGE DETAILS</span> (po number : #{{$po_number}})</h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home4 position-left"></i> Dashboard</a></li>
            <li class="active"><a href="#"> Package Details</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="table_package_detail">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>BARCODE ID</th>
                        <th>SCAN ID</th>
                        <th>SIZE</th>
                        <th>QTY</th>
                        <th>DIMENSION (L x W x H (satuan))</th>
                        <th>GW (satuan)</th>
                        <th>NW (satuan)</th>
                        <th>LOCATION</th>
                        <th>DEPARTMENT</th>
                        <th>STATUS</th>
                        <!-- <th>ACTION</th> -->
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('ajaxPackageData') }}" id="package_detail_get_data" data-ponumber = '{{ $po_number}}' data-planrefnumber = '{{ $plan_ref_number }}'></a>
@endsection

@include('dashboard/_js_detail')
@section('js')
<script type="text/javascript">
$( document ).ready(function() {
    //datatables
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });
    var url = $('#package_detail_get_data').attr('href');
    var ponumber = $('#package_detail_get_data').data('ponumber');
    var plan_ref_number = $('#package_detail_get_data').data('planrefnumber');
    var table = $('#table_package_detail').DataTable({
        ajax: {
            'url' : url,
            'data': {po_number: ponumber, plan_ref_number: plan_ref_number}
        },
        columns: [
            {data: 'package_number', name: 'package_number'},
            {data: 'barcode_id', name: 'barcode_id'},
            {data: 'scan_id', name: 'scan_id'},
            {data: 'customer_size', name: 'customer_size'},
            {data: 'inner_pack', name: 'inner_pack'},
            {data: 'dimension', name: 'dimension'},
            {data: 'gross', name: 'gross'},
            {data: 'net', name: 'net'},
            {data: 'location', name: 'location'},
            {data: 'current_department', name: 'current_department'},
            {data: 'current_status', name: 'current_status'},
            // {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });

});

</script>
@endsection
