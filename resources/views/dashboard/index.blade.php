@extends('layouts.app', ['active' => 'dashboard'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <!-- <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">DASHBOARD</span></h4>
        </div>
    </div> -->
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home4 position-left"></i> Dashboard</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<!-- STATUS -->
<div class="row">
    <div class="form-group col-md-12 hidden">
        <label><b>FILTER BY FREQUENCY</b> </label>
        <select data-placeholder="select trigger label" id="frequency" class="select form-control">
            <option value="daily">DAILY</option>
            <option value="weekly">WEEKLY</option>
            <option value="monthly">MONTHLY</option>
            <option value="yearly" selected="selected">YEARLY</option>
        </select>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="panel panel-body">
            <div class="media no-margin">
                <div class="media-left media-middle">
                    <i class="icon-dropbox icon-3x text-success-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="no-margin text-semibold" id="total_preparation">{{ isset($count_preparation) ? $count_preparation : 0 }}</h3>
                    <span class="text-uppercase text-size-mini text-muted">Total Packing</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="panel panel-body">
            <div class="media no-margin">
                <div class="media-left media-middle">
                    <i class="icon-office icon-3x text-indigo-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="no-margin text-semibold" id="total_sewing">{{ isset($count_sewing) ? $count_sewing : 0 }}</h3>
                    <span class="text-uppercase text-size-mini text-muted">Total Sewing</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="panel panel-body">
            <div class="media no-margin">
                <div class="media-left media-middle">
                    <i class="icon-location4 icon-3x text-blue-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="no-margin text-semibold" id="total_inventory">{{ isset($count_inventory) ? $count_inventory : 0 }}</h3>
                    <span class="text-uppercase text-size-mini text-muted">Total Inventory</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="panel panel-body">
            <div class="media no-margin">
                <div class="media-left media-middle">
                    <i class="icon-search4 icon-3x text-danger-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="no-margin text-semibold" id="total_qcinspect">{{ isset($count_qcinspect) ? $count_qcinspect : 0 }}</h3>
                    <span class="text-uppercase text-size-mini text-muted">Total QC Inspect</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-offset-6 col-md-offset-9 col-sm-6 col-md-3">
        <div class="panel panel-body">
            <div class="media no-margin">
                <div class="media-left media-middle">
                    <i class="icon-truck icon-3x text-default-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="no-margin text-semibold" id="total_shipping">{{ isset($count_shipping) ? $count_shipping : 0 }}</h3>
                    <span class="text-uppercase text-size-mini text-muted">Total Shipping</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /STATUS -->

<!-- TABLE -->
<div class="panel panel-flat">
    <div class="panel-body">
        <input type="hidden" id="filter_click" name="filter_click" value="0">
          <div class="form-group" id="filter_by_po">
            <label><b>Choose PO Number</b></label>
            <div class="input-group">
                <input type="text" class="form-control" name="po" id="po" placeholder="PO Number" value="">
                <div class="input-group-btn">
                    <button type="button" id='filterPo' class="btn btn-primary">Filter</button>
                </div>
            </div>
          </div>
          <div class="form-group hidden" id="filter_by_plan">
            <label><b>Choose Plan Stuffing Date (month/day/year)</b></label>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                <input type="text" class="form-control daterange-basic" name="date_range_plan" id="date_range_plan">
                <div class="input-group-btn">
                    <button type="button" id="filterPlan" class="btn btn-primary">Filter</button>
                </div>
            </div>
          </div>
          <div class="form-group hidden" id="filter_by_podd">
            <label><b>Choose PODD (month/day/year)</b></label>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                <input type="text" class="form-control daterange-basic" name="date_podd" id="date_podd">
                <div class="input-group-btn">
                    <button type="button" id="filterPodd" class="btn btn-primary">Filter</button>
                </div>
            </div>
          </div>
          <div class="form-group">
            <label class="radio-inline"><input type="radio" name="radio_status" value="po" checked="checked">Filter by PO Number</label>
            <label class="radio-inline"><input type="radio" name="radio_status" value="plan">Filter by Plan Stuffing Date</label>
            <label class="radio-inline"><input type="radio" name="radio_status" value="podd">Filter by PODD</label>
        </div>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="col-md-12" style="float:right">
            <span class="label label-roundless label-danger" style="float:right">REJECTED</span>
            <span class="label label-roundless label-primary" style="float:right">ON PROGRESS</span>
            <span class="label label-roundless label-success" style="float:right">COMPLETED</span>
        </div>
    </div>
    <div class="panel-body">
      <div class="row {{ Auth::user()->admin_role !== 1 ? 'hidden' : '' }}">
            <div class="col-md-2"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->factory_id }}" {{ $val->factory_id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="row">
            <button type="button" class="btn btn-default pull-right" id="export_dashboard"><i class="icon-cloud-download"></i> Download Dashboard</button>
        </div>
        <div class="table-responsive">
            <table class="table datatable-button-html5-basic" id="table_po">
                <thead>
                    <tr>
                        <th rowspan="2">#</th>
                        <th rowspan="2">PLAN STUFFING</th>
                        <th rowspan="2">PO NUMBER</th>
                        <th rowspan="2">PLAN REF NUMBER</th>
                        <th rowspan="2">ORDER STATUS</th>
                        <th rowspan="2">BRAND</th>
                        <th rowspan="2">ART. NO</th>
                        <th rowspan="2">INVOICE</th>
                        <th rowspan="2">DEST</th>
                        <th rowspan="2">QTY</th>
                        <th rowspan="2">STYLE</th>
                        <th rowspan="2">FWD</th>
                        <th rowspan="2">SHIPMODE</th>
                        <th rowspan="2">QTY INTG.</th>
                        <th rowspan="2">TOTAL PACKAGE</th>
                        <th colspan="5" class="text-center">STATUS</th>
                        <th rowspan="2">ACTION</th>
                    </tr>
                    <tr>
                        <th style="text-align:center" class="col-xs-4">PREPARATION</th>
                        <th style="text-align:center" class="col-xs-4">SEWING</th>
                        <th style="text-align:center" class="col-xs-4">INVENTORY</th>
                        <th style="text-align:center" class="col-xs-4">QC INSPECT</th>
                        <th style="text-align:center" class="col-xs-4">SHIPPING</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<!-- /TABLE -->

<!-- IMPORTANT LINK -->
<a href="{{ route('dashboard') }}" id="po_count_status"></a>
<a href="{{ route('dashboardExport') }}" id="dashboard_export"></a>
<a href="{{ route('ajaxGetData') }}" id="po_get_data"></a>
<!-- /IMPORTANT LINK -->
@endsection

@section('modal')
<!-- MODAL PO NUMBER -->
<!-- <div id="modal_ponumber_" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content modal-lg">
      <div class="modal-body">
      <form class="form-horizontal" action="#">
          <div class="panel-body">
              <fieldset>
                  <legend class="text-semibold">
                      <i class="icon-file-text2 position-left"></i><h4 id="title_ponumber"></h4>
                  </legend>

                  <table class="table" id="body_ponumber">
                      <thead>
                          <tr>
                              <th>Plan Ref Number</th>
                              <th>Item Qty</th>
                              <th>Pkg Count</th>
                              <th>Net Net</th>
                              <th>Net</th>
                              <th>Gross</th>
                              <th>Unit1</th>
                              <th>Vol</th>
                              <th>Unit2</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td id="planrefnumber"></td>
                              <td id="itemqty"></td>
                              <td id="pkgcount"></td>
                              <td id="netnet"></td>
                              <td id="net"></td>
                              <td id="gross"></td>
                              <td id="unit1"></td>
                              <td id="vol"></td>
                              <td id="unit2"></td>
                          </tr>
                      </tbody>
                  </table>
              </fieldset>
          </div>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div> -->
<!-- /MODAL PO NUMBER -->

<!-- MODAL PLAN REF NUMBER -->
<!-- <div id="modal_planrefnumber_" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      <form class="form-horizontal" action="#">
          <div class="panel-body">
              <fieldset>
                  <legend class="text-semibold">
                      <i class="icon-file-text2 position-left"></i>
                      <div id="title"></div>
                  </legend>

                  <div id="packing_plan_data">
                      <div class="form-group">
                          <label class="col-md-3 control-label">Enter your name:</label>
                          <div class="col-md-9">
                              <input type="text" class="form-control" placeholder="Eugene Kopyov">
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="col-lg-3 control-label">Enter your password:</label>
                          <div class="col-lg-9">
                              <input type="password" class="form-control" placeholder="Your strong password">
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="col-lg-3 control-label">Repeat password:</label>
                          <div class="col-lg-9">
                              <input type="password" class="form-control" placeholder="Repeat password">
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="col-lg-3 control-label">Your message:</label>
                          <div class="col-lg-9">
                              <textarea rows="5" cols="5" class="form-control" placeholder="Enter your message here"></textarea>
                          </div>
                      </div>
                  </div>
              </fieldset>
          </div>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div> -->
<!-- /MODAL PLAN REF NUMBER -->
@endsection

@include('dashboard/_js_index')
@section('js')
<script type="text/javascript" src="{{ url('js/plugins/tables/datatables/extensions/fixed_columns.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/pages/datatables_extension_fixed_columns.js') }}"></script>
<script type="text/javascript">
$( document ).ready(function() {
    //url
    var url = $('#po_get_data').attr('href');
    var url_count = $('#po_count_status').attr('href');
    var url_download = $('#dashboard_export').attr('href');

    $('#export_dashboard').on('click', function(){
         var radio = $('input[name=radio_status]:checked').val();
        var param ="";
        if (radio=='po') {
            param = $("#po").val();
        }else if (radio=='plan') {
            param = $('#date_range_plan').val();
        }else if (radio=='podd') {
            param = $('#date_podd').val();
        }
        
        window.location.href = url_download+'?factory_id='+$('#factory_id').val()+'&radio='+radio+'&param='+param;
        // window.location.href = url_download+'?factory_id='+$('#factory_id').val();

    });

    //datatables
    // $.extend( $.fn.dataTable.defaults, {
    //     stateSave: true,
    //     autoWidth: false,
    //     autoLength: false,
    //     processing: true,
    //     serverSide: true,
    //     pageLength: 5, // default records per page
    //     lengthMenu: [[5, 25, 100, -1], [5, 25, 100, 'All']],
    //     dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
    //     language: {
    //         search: '<span>Filter:</span> _INPUT_',
    //         searchPlaceholder: 'Type to filter...',
    //         lengthMenu: '<span>Show:</span> _MENU_',
    //         paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
    //     }
    // });

    //datatables
        $.extend( $.fn.dataTable.defaults, {
            scrollX: true,
            scrollY: "50vh",
            scrollCollapse: true,
            stateSave: true,
            autoWidth: false,
            autoLength: false,
            processing: true,
            serverSide: true,
            paging: false,
            ordering: false,
            info: false,
            searching: false,
            //searching: true, 
            paging: false, 
           // info: false,
            scrollY:        "300px",
            scrollX:        true,
            scrollCollapse: true,
            fixedColumns:   {
                leftColumns: 5
            },
            sDom: 'tF',
        });

    var table = $('#table_po').DataTable({
        //pageLength: 5,
        //lengthMenu: [[5, 10, 20, 100, -1], [5, 10, 20, 100, 'All']],
        ajax: {
            url: url,
            data: function (d) {
                return $.extend({},d,{
                    "frequency": $('#frequency').val(),
                    "factory_id": $('#factory_id').val(),
                    "radio_status": $('input[name=radio_status]:checked').val(),
                    "date_range_plan": $('#date_range_plan').val(),
                    "filter_click": $('#filter_click').val(),
                    "po": $('#po').val(),
                    "date_podd": $('#date_podd').val(),
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(1).css('min-width', '150px');
            $('td', row).eq(3).css('min-width', '75px');
            $('td', row).eq(14).css('min-width', '150px');
            $('td', row).eq(15).css('min-width', '150px');
            $('td', row).eq(16).css('min-width', '200px');
            $('td', row).eq(17).css('min-width', '200px');
            $('td', row).eq(18).css('min-width', '150px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'plan_stuffing', name: 'plan_stuffing'},
            {data: 'po_number', name: 'po_number'},
            {data: 'plan_ref_number', name: 'plan_ref_number'},
            {data: 'order_status', name: 'order_status'},
            {data: 'brand', name: 'brand'},
            {data: 'art_no', name: 'art_no'},
            {data: 'invoice', name: 'invoice'},
            {data: 'country_name', name: 'country_name'},
            {data: 'new_qty', name: 'new_qty'},
            {data: 'style', name: 'style'},
            {data: 'fwd', name: 'fwd'},
            {data: 'shipmode', name: 'shipmode'},
            {data: 'qty_intg', name: 'qty_intg'},
            {data: 'total_package', name: 'total_package', sortable: false, orderable: false, searchable: false},
            {data: 'preparation', name: 'preparation', sortable: false, orderable: false, searchable: false},
            {data: 'sewing', name: 'sewing', sortable: false, orderable: false, searchable: false},
            {data: 'inventory', name: 'inventory', sortable: false, orderable: false, searchable: false},
            {data: 'qcinspect', name: 'qcinspect', sortable: false, orderable: false, searchable: false},
            {data: 'shipping', name: 'shipping', sortable: false, orderable: false, searchable: false},
            {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false}
        ],
    });

    $('#factory_id').on('change', function() {
          //reload data count
        var frequency = $('#frequency').val();

        $.ajax({ //update total status
            url: url_count,
            data: {frequency: frequency, factory_id: $(this).val()},
            success: function(response){

                $('#total_preparation').text(response['count_preparation']);
                $('#total_sewing').text(response['count_sewing']);
                $('#total_inventory').text(response['count_inventory']);
                $('#total_qcinspect').text(response['count_qcinspect']);
                $('#total_shipping').text(response['count_shipping']);

                $('.cl_request').text(response['total_request_change']).trigger('change');
            }
        }),

        //reload data table
        table.ajax.reload( null, false );
    });

    $('#filterPo').on('click', function(event) {
      event.preventDefault();

        var po = $('#po').val();

        if (po.length >= 4 ) {
            $('#filter_click').val(1);
          loading_po();
          //reload data table
          table.ajax.reload( null, false );
        }else{
          myalert('error','Please enter PO minimal 4 character');
          return false;
        }
    });

    $('#po').keyup(function(e) {
        var po = $('#po').val();

        if (e.keyCode == 13 || e.which == 13) {
            if (po.length >= 4 ) {
                $('#filter_click').val(1);

                loading_po();
                //reload data table
                table.ajax.reload( null, false );
            }else{
              myalert('error','Please enter PO minimal 4 character');
              return false;
            }
        }
    });

    $('#filterPlan').on('click', function(event) {
        event.preventDefault();
  
           //check location
           if($('#date_range_plan').val() == '') {
                alert('Please select date range first');
                return false;
            }

            $('#filter_click').val(1);

            loading_po();
            table.ajax.reload(null, false);
      });

    $('#filterPodd').on('click', function(event) {
        event.preventDefault();
  
           //check location
           if($('#date_podd').val() == '') {
                alert('Please select date range first');
                return false;
            }

            $('#filter_click').val(1);

            loading_po();
            table.ajax.reload(null, false);
      });

    //choose filter
    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'po') {
            if($('#filter_by_po').hasClass('hidden')) {
                $('#filter_by_po').removeClass('hidden');
            }

            $('#filter_by_plan').addClass('hidden');
            $('#filter_by_podd').addClass('hidden');
        }
        else if (this.value == 'plan') {
            if($('#filter_by_plan').hasClass('hidden')) {
                $('#filter_by_plan').removeClass('hidden');
            }

            $('#filter_by_po').addClass('hidden');
            $('#filter_by_podd').addClass('hidden');
        }
        else if (this.value == 'podd') {
            if($('#filter_by_podd').hasClass('hidden')) {
                $('#filter_by_podd').removeClass('hidden');
            }

            $('#filter_by_po').addClass('hidden');
            $('#filter_by_plan').addClass('hidden');
        }

        $('#filter_click').val(1);       
        loading_po();

        //reload data table
        table.ajax.reload( null, false );
    });

    //reload data every x second
    // setInterval( function () {
    //     //reload data count
    //     var frequency = $('#frequency').val();

    //     $.ajax({ //update total status
    //         url: url_count,
    //         data: {frequency: frequency, factory_id: $('#factory_id').val()},
    //         success: function(response){

    //             $('#total_preparation').text(response['count_preparation']);
    //             $('#total_sewing').text(response['count_sewing']);
    //             $('#total_inventory').text(response['count_inventory']);
    //             $('#total_qcinspect').text(response['count_qcinspect']);
    //             $('#total_shipping').text(response['count_shipping']);

    //             $('.cl_request').text(response['total_request_change']).trigger('change');
    //         }
    //     }),

    //     //reload data table
    //     table.ajax.reload( null, false );
    // }, 30000 );

    //modal po number
    // $('#table_po').on('click', '.modal_ponumber', function () {
    //     var ponumber = $(this).data('ponumber');
    //     var planrefnumber= $(this).data('planrefnumber');
    //     var itemqty= $(this).data('itemqty');
    //     var pkgcount= $(this).data('pkgcount');
    //     var netnet= $(this).data('netnet');
    //     var net= $(this).data('net');
    //     var gross= $(this).data('gross');
    //     var unit1= $(this).data('unit1');
    //     var vol= $(this).data('vol');
    //     var unit2= $(this).data('unit2');
    //
    //     $('#title_ponumber').text('PO NUMBER #'+ponumber);
    //     $('#planrefnumber').text(planrefnumber);
    //     $('#itemqty').text(itemqty);
    //     $('#pkgcount').text(pkgcount);
    //     $('#netnet').text(netnet);
    //     $('#net').text(net);
    //     $('#gross').text(gross);
    //     $('#unit1').text(unit1);
    //     $('#vol').text(vol);
    //     $('#unit2').text(unit2);
    //     $('#modal_ponumber_').modal('show');
    // });

    //modal plan ref number
    // $('.modal_planrefnumber').on('click', function(){
    //     $('#modal_planrefnumber_').modal('show')
    // });

    //select frequency
    $('#frequency').change(function() {
        var total_status;
        var result_datatables;
        var frequency = $(this).val();

        $.ajax({
            url: url_count,
            data: {frequency: frequency, factory_id: $('#factory_id').val()},
            success: function(response){
                $('#total_preparation').text(response['count_preparation']);
                $('#total_sewing').text(response['count_sewing']);
                $('#total_inventory').text(response['count_inventory']);
                $('#total_qcinspect').text(response['count_qcinspect']);
                $('#total_shipping').text(response['count_shipping']);

                $('.cl_request').text(response['total_request_change']).trigger('change');

                table.draw();
            }
        })
    });
});

// loading
  function loading_po() {
      $('#table-po').block({
          message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
          overlayCSS: {
              backgroundColor: '#fff',
              opacity: 0.8,
              cursor: 'wait'
          },
          css: {
              border: 0,
              padding: '10px 15px',
              color: '#fff',
              width: 'auto',
              '-webkit-border-radius': 2,
              '-moz-border-radius': 2,
              backgroundColor: '#333'
              // 'z-index': 1,
              // position: absolute
          }
      });

  }

</script>
@endsection
