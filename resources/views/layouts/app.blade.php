<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.interface.club/limitless/layout_2/LTR/default/layout_navbar_fixed.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Sep 2017 02:26:21 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- <meta name="viewport" content="width=1024"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Finish Goods Apps</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ url('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ url('css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ url('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ url('css/core.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ url('css/components.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ url('css/colors.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
	<style>
		.click-request{
			cursor: pointer;
		}
	</style>

	<!-- Core JS files -->
	<script type="text/javascript" src="{{ url('js/plugins/loaders/pace.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('js/core/libraries/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('js/core/libraries/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('js/plugins/loaders/blockui.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ url('js/plugins/ui/nicescroll.min.js') }}"></script>
	<script type="text/javascript" src="{{url('js/plugins/tables/datatables/datatables.min.js')}}"></script>
	<script type="text/javascript" src="{{ url('js/plugins/notifications/jgrowl.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('js/plugins/ui/moment/moment.min.js') }}"></script>

	<script type="text/javascript" src="{{ url('js/core/app.js') }}"></script>
	<script type="text/javascript" src="{{ url('js/pages/layout_fixed_custom.js') }}"></script>
	<script type="text/javascript" src="{{ url('js/plugins/notifications/bootbox.min.js')}}"></script>
	<script type="text/javascript" src="{{ url('js/plugins/notifications/sweet_alert.min.js')}}"></script>

	<script type="text/javascript" src="{{ url('js/plugins/forms/selects/select2.min.js') }}"></script>
	<!-- /theme JS files -->

	<script type="text/javascript">
		$(document).ready(function() {
			// update info request carton
			var url_dashboard = $('#po_count_status').attr('href');

			    $.ajax({ //update total status
			            url: url_dashboard,
			            data: {},
			            success: function(response){

			                $('.cl_request').text(response['total_request_change']);
			                $('#total_waiting').text(response['count_waiting']);
			                $('#total_hold').text(response['count_hold']);
			                $('#total_onprogress').text(response['count_onprogress']);
			                $('#total_done').text(response['count_done']);
			            }
			        });
			//
		});
		function myalert(type, text) {
			var color = '#66BB6A';
			var title = 'Good Job!';
			if(type == 'error') {
				color = '#EF5350';
				title = 'Oops...';
			}
			swal({
				title: title,
				text: text,
				confirmButtonColor: color,
				type: type
			});
		}

		function isNumberKey(evt)
		{
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode > 31 && (charCode < 48 || charCode > 57))
				return false;

			return true;
		}

		function isNumberDot(evt) {
          var theEvent = evt || window.event;
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
          if (key.length == 0) return;
          var regex = /^[0-9.\b]+$/;
          if (!regex.test(key)) {
              theEvent.returnValue = false;
              if (theEvent.preventDefault) theEvent.preventDefault();
          }
      }

      // loading
	function loading_process() {
	    $('#table-list').block({
	        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
	        overlayCSS: {
	            backgroundColor: '#fff',
	            opacity: 0.8,
	            cursor: 'wait'
	        },
	        css: {
	            border: 0,
	            padding: '10px 15px',
	            color: '#fff',
	            width: 'auto',
	            '-webkit-border-radius': 2,
	            '-moz-border-radius': 2,
	            backgroundColor: '#333'
	        }
	    });
	}

	function loading() {
		$('#listing-tab').block({
			message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
			overlayCSS: {
				backgroundColor: '#fff',
				opacity: 0.8,
				cursor: 'wait'
			},
			css: {
				border: 0,
				padding: '10px 15px',
				color: '#fff',
				width: 'auto',
				'-webkit-border-radius': 2,
				'-moz-border-radius': 2,
				backgroundColor: '#333'
			}
		});
	
	}
	</script>
</head>

<body class="navbar-top">
	<!-- Main navbar -->
	<div class="navbar navbar-default navbar-fixed-top header-highlight">
		<div class="navbar-header">
			<a class="navbar-brand" href="#" style="color:#fff">FINISHED GOODS</a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

			</ul>

			<p class="navbar-text"><span class="label bg-success">Online</span></p>

			<ul class="nav navbar-nav navbar-right">


				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<span>{{ Auth::user()->name }} ({{ Auth::user()->factory_name }})</span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{  route('user.accountSetting',Auth::User()->id) }}"><i class="icon-user-lock"></i> Change Password</a></li>
                        <li><a href="{{ route('logout') }}"
							onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
							<i class="icon-switch2"></i> Logout</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</li>
					</ul>
				</li>
			</ul>
			@permission(['menu-request-carton'])
			<div class="pull-right click-request" title="Request Carton">
				<p class="navbar-text bg-default"><i class="icon icon-bell2"></i>
				</p>
				<a href="{{ route('packing.requestcarton') }}">
					<span class="badge badge-danger cl_request">{{ $total_request }}</span>
				</a>
			</div>
			@endpermission
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main sidebar-fixed">
				<div class="sidebar-content">


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								@include('layouts/menu')
							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				@yield('page_header')

				<!-- Content area -->
				<div class="content">

					@yield('content')

					<!-- Footer -->
                    <div class="footer text-muted text-center">
						&copy; 2018. <a href="#">Finish Goods Apps</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@yield('modal')
@yield('js_extension')
@yield('js')
</body>

<!-- Mirrored from demo.interface.club/limitless/layout_2/LTR/default/layout_navbar_fixed.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Sep 2017 02:26:21 GMT -->
</html>
