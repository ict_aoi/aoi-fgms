<!-- DASHBOARD -->
@permission(['menu-dashboard'])
<li class="{{ isset($active) && $active == 'dashboard' ? 'active' : '' }}">
    <a href="{{ route('dashboard') }}"> <span>DASHBOARD</span></a>
</li>
@endpermission
<!-- END OF DASHBOARD -->

<!-- MENU ADMIN -->
@permission(['menu-user-management'])
<li class="navigation-header"><span>User Management</span><i class="icon-menu" title="User Management"></i></li>
<li class="{{ isset($active) && $active == 'user' ? 'active' : '' }}">
    <a href="{{ route('user.index') }}"><i class="icon-users"></i><span>USER</span></a>
</li>
<li class="{{ isset($active) && $active == 'roles' ? 'active' : '' }}">
    <a href="{{ route('role.index') }}"><i class="glyphicon glyphicon-repeat"></i><span>ROLES</span></a>
</li>
@endpermission
<!-- END OF MENU ADMIN -->

<!-- MENU PACKING PREPARATION -->
@permission(['menu-packing-preparation'])
<li class="navigation-header"><span>Packing Preparation</span><i class="icon-menu" title="Packing Preparation"></i></li>
<li class="{{ isset($active) && $active == 'packinglist' ? 'active' : '' }}">
    <a href="{{ route('packing.packingList') }}"><i class="icon-list"></i><span>PACKING LIST</span></a>
</li>

<li class="{{ isset($active) && $active == 'packinglistcancel' ? 'active' : '' }}">
    <a href="{{ route('packing.packingListCancel') }}"><i class="icon-list-numbered"></i><span>PACKING LIST CANCEL ORDER</span></a>
</li>


<li class="{{ isset($active) && $active == 'historyrevisi' ? 'active' : '' }}">
    <a href="{{ route('packing.historyRevisi') }}"><i class="icon-clipboard3"></i><span>HISTORY REVISI</span></a>
</li>
<!-- <li class="{{ isset($active) && $active == 'packagedetail' ? 'active' : '' }}">
    <a href="{{ route('packing.packageDetail') }}"><i class="icon-clipboard2"></i><span>PACKAGE DETAIL</span></a>
</li> -->
@endpermission
<!-- END OF MENU PACKING -->

@permission(['menu-package-preparation'])
<li class="{{ isset($active) && $active == 'packagepreparation' ? 'active' : '' }}">
    <a href="{{ route('packing.packagepreparation') }}"><i class="icon-trash-alt"></i><span>PACKAGE PREPARATION</span></a>
</li>
@endpermission

@permission(['menu-package-cancel'])
<li class="{{ isset($active) && $active == 'packagecancel' ? 'active' : '' }}">
    <a href="{{ route('packing.packagecancel') }}"><i class="icon-trash-alt"></i><span>DELETE PACKAGE</span></a>
</li>
@endpermission



@permission(['menu-request-carton'])
<li class="{{ isset($active) && $active == 'requestcarton' ? 'active' : '' }}">
    <a href="{{ route('packing.requestcarton') }}"><i class="icon-list-ordered"></i><span>REQUEST CARTON</span></a>
</li>
@endpermission

@permission(['menu-transfer-style'])
<li class="{{ isset($active) && $active == 'transferstyle' ? 'active' : '' }}">
    <a href="{{ route('packing.transferstyle') }}"><i class="icon-transmission"></i><span>TRANSFER STYLE</span></a>
</li>
@endpermission

@permission(['menu-package-rasio'])
<li class="{{ isset($active) && $active == 'packagerasio' ? 'active' : '' }}">
    <a href="{{ route('packing.packagerasio') }}"><i class="icon-list"></i><span>PACKAGE RASIO</span></a>
</li>
@endpermission

@permission(['menu-po-sync'])
<li class="{{ isset($active) && $active == 'posync' ? 'active' : '' }}">
    <a href="{{ route('packing.posync') }}"><i class="icon-cloud-download"></i><span>PO SYNC</span></a>
</li>
@endpermission

<!-- MENU SEWING -->
@permission(['menu-sewing'])
<li class="navigation-header"><span>SEWING</span><i class="icon-menu" title="sewing"></i></li>
<li class="{{ isset($active) && $active == 'sewingcheckin' ? 'active' : '' }}">
    <a href="{{ route('sewing.checkin') }}"><i class="icon-arrow-up16"></i> <span>CHECK IN</span></a>
</li>
<li class="{{ isset($active) && $active == 'sewingcompleted' ? 'active' : '' }}">
    <a href="{{ route('sewing.checkout') }}"><i class="icon-arrow-down16"></i> <span>SEWING COMPLETED</span></a>
</li>
@endpermission
@permission(['menu-sewing-master'])
<li class="{{ isset($active) && $active == 'mastersewing' ? 'active' : '' }}">
    <a href="{{ route('sewing.master') }}"><i class="icon-stack2"></i> <span>MASTER SEWING</span></a>
</li>
@endpermission
<!-- END OF MENU SEWING -->

<!-- MENU INVENTORY MANAGEMENT-->
@permission(['menu-inventory-management','menu-inventory-checkin', 'menu-inventory-placement', 'menu-inventory-area-and-location'])
<li class="navigation-header"><span>Inventory Management</span><i class="icon-menu" title="inventory_management"></i></li>
    @permission(['menu-inventory-management','menu-inventory-checkin'])
    <li class="{{ isset($active) && $active == 'inventorycheckin' ? 'active' : '' }}">
        <a href="{{ route('inventory.checkin') }}"><i class="icon-arrow-up16"></i> <span>CHECK IN</span></a>
    </li>
    @endpermission
    @permission(['menu-inventory-management','menu-inventory-placement'])
    <li class="{{ isset($active) && $active == 'inventoryplacement' ? 'active' : '' }}">
        <a href="{{ route('inventory.placement') }}"><i class="icon-paperplane"></i> <span>PLACEMENT</span></a>
    </li>
    @endpermission
    @permission(['menu-inventory-management','menu-inventory-area-and-location'])
    <li class="{{ isset($active) && $active == 'inventoryarea' ? 'active' : '' }}">
        <a href="{{ route('area') }}"><i class="icon-location4"></i> <span>AREA & LOCATION</span></a>
    </li>
    @endpermission
@endpermission
<!-- END OF MENU INVENTORY MANAGEMENT -->

<!-- menu inventory stock -->
@permission(['menu-inventory-stock'])
<li class="{{ isset($active) && $active == 'inventorystock' ? 'active' : '' }}">
    <a href="{{ route('inventory.stock') }}"><i class="icon-search4"></i> <span>CHECK PLACEMENT</span></a>
</li>
@endpermission
<!-- end menu inventory stock -->

<!-- move inventory -->
@permission(['menu-move-inventory'])
<li class="navigation-header"><span>Move Inventory</span><i class="icon-menu" title="Move Inventory"></i></li>
<li class="{{ isset($active) && $active == 'moveinventory' ? 'active' : '' }}">
    <a href="{{ route('move') }}"><i class="icon-transmission"></i> <span>MOVE INVENTORY</span></a>
</li>
<li class="{{ isset($active) && $active == 'checkoutmove' ? 'active' : '' }}">
    <a href="{{ route('move.checkOut') }}"><i class="icon-airplane3"></i> <span>CHECK OUT INVENTORY</span></a>
</li>
<li class="{{ isset($active) && $active == 'checkinmove' ? 'active' : '' }}">
    <a href="{{ route('move.checkIn') }}"><i class="icon-airplane4"></i> <span>CHECK IN INVENTORY</span></a>
</li>
<li class="{{ isset($active) && $active == 'bcnumber' ? 'active' : '' }}">
    <a href="{{ route('move.beaCukai') }}"><i class="icon-bold2"></i> <span>INSERT BC</span></a>
</li>

@endpermission

<!-- MENU QC INSPECT-->
@permission(['menu-qc-inspect'])
<li class="navigation-header"><span>QC INSPECT</span></li>
<li class="{{ isset($active) && $active == 'qcinspectcheckin' ? 'active' : '' }}">
    <a href="{{ route('qcinspect.checkin') }}"><i class="icon-arrow-up16"></i> <span>CHECK IN</span></a>
</li>
<li class="{{ isset($active) && $active == 'qcinspectcompleted' ? 'active' : '' }}">
    <a href="{{ route('qcinspect.checkout') }}"><i class="icon-arrow-down16"></i> <span>QC INSPECT COMPLETED</span></a>
</li>
@endpermission
<!-- END OF MENU INVENTORY MANAGEMENT -->

<!-- MENU SHIPPING-->
@permission(['menu-shipping'])
<li class="navigation-header"><span>SHIPPING</span><i class="icon-menu" title="shipping"></i></li>
<li class="{{ isset($active) && $active == 'shippingcheckin' ? 'active' : '' }}">
    <a href="{{ route('shipping.checkin') }}"><i class="icon-arrow-up16"></i> <span>READY TO SHIP</span></a>
</li>
<li class="{{ isset($active) && $active == 'shippingcheckout' ? 'active' : '' }}">
    <a href="{{ route('shipping.checkout') }}"><i class="icon-truck"></i> <span>STUFFING</span></a>
</li>
@endpermission
<!-- END OF MENU SHIPPING -->

<!-- MENU EXIM -->

@permission(['menu-exim-load'])
<li class="navigation-header"><span>EXIM PLAN LOAD</span></li>
<li class="{{ isset($active) && $active == 'eximplanload' ? 'active' : '' }}">
    <a href="{{ route('exim.planload') }}"><i class="icon-upload4"></i> <span>UPLOAD PLAN LOAD</span></a>
</li>
<li class="{{ isset($active) && $active == 'eximinvoice' ? 'active' : '' }}">
    <a href="{{ route('exim.invoice') }}"><i class="icon-box-add"></i> <span>INVOICE STORAGE</span></a>
</li>
<li class="{{ isset($active) && $active == 'eximgroupinvoice' ? 'active' : '' }}">
    <a href="{{ route('exim.groupinvoice') }}"><i class="icon-truck"></i> <span>DELIVERY ORDERS</span></a>
</li>
<li class="{{ isset($active) && $active == 'eximdetailinvoice' ? 'active' : '' }}">
    <a href="{{ route('exim.detailInvoice') }}"><i class="icon-list"></i> <span>DETAIL INVOICE</span></a>
</li>
<li class="{{ isset($active) && $active == 'detailDelivery' ? 'active' : '' }}">
    <a href="{{ route('exim.detailDelivery') }}"><i class="icon-paragraph-justify3"></i> <span>DETAIL DELIVERY ORDER</span></a>
</li>
@endpermission
@permission(['menu-'])
<li class="{{ isset($active) && $active == 'eximplanstuffing' ? 'active' : '' }}">
    <a href="{{ route('exim.planstuffing') }}"><i class="icon-folder-search"></i> <span>PLAN STUFFING</span></a>
</li>
@endpermission
<!-- END MENU EXIM -->

<!-- MENU REPORT -->
@permission(['menu-report'])
<li class="navigation-header"><span>REPORT</span><i class="icon-menu" title="report"></i></li>
@permission(['menu-package-calculate'])
<li class="{{ isset($active) && $active == 'reportpackingcalculate' ? 'active' : '' }}">
    <a href="{{ route('report.packingcalculate') }}"><i class="icon-calculator"></i> <span>PACKING CALCULATE</span></a>
</li>
@endpermission
<li class="{{ isset($active) && $active == 'reportpacking' ? 'active' : '' }}">
    <a href="{{ route('report.packing') }}"><i class="icon-dropbox"></i> <span>PACKING</span></a>
</li>
<li class="{{ isset($active) && $active == 'reportsewing' ? 'active' : '' }}">
    <a href="{{ route('report.sewing') }}"><i class="icon-office"></i> <span>SEWING</span></a>
</li>
<li class="{{ isset($active) && $active == 'reportsewingout' ? 'active' : '' }}">
    <a href="{{ route('report.sewingOut') }}"><i class="icon-office"></i> <span>SEWING COMPLETED</span></a>
</li>
<li class="{{ isset($active) && $active == 'reportinventory' ? 'active' : '' }}">
    <a href="{{ route('report.inventory') }}"><i class="icon-location4"></i> <span>INVENTORY</span></a>
</li>
<li class="{{ isset($active) && $active == 'reportinventorypo' ? 'active' : '' }}">
    <a href="{{ route('report.inventoryPo') }}"><i class="icon-location3"></i> <span>INVENTORY PER PO</span></a>
</li>
<li class="{{ isset($active) && $active == 'reportbacktosewing' ? 'active' : '' }}">
    <a href="{{ route('report.backtosewing') }}"><i class="icon-arrow-up-left2"></i> <span>BACK TO SEWING (BONGKARAN)</span></a>
</li>
<li class="{{ isset($active) && $active == 'reportstock' ? 'active' : '' }}">
    <a href="{{ route('report.stock') }}"><i class="icon-paperplane"></i> <span>STOCK</span></a>
</li>
<!-- <li class="{{ isset($active) && $active == 'reportstockcancel' ? 'active' : '' }}">
    <a href="{{ route('report.stockCancel') }}"><i class="icon-stack-cancel"></i> <span>STOCK CANCEL</span></a>
</li> -->
<li class="{{ isset($active) && $active == 'reportstockbalance' ? 'active' : '' }}">
    <a href="{{ route('report.endingBalance') }}"><i class="icon-airplane4"></i> <span>ENDING BALANCE</span></a>
</li>
<li class="{{ isset($active) && $active == 'dashboardlocator' ? 'active' : '' }}">
    <a href="{{ route('report.dashboardlocator') }}"><i class="icon-stack"></i> <span>DASHBOARD LOCATOR</span></a>
</li>
<li class="{{ isset($active) && $active == 'reportqcinspect' ? 'active' : '' }}">
    <a href="{{ route('report.qcinspect') }}"><i class="icon-search4"></i> <span>QC INSPECT</span></a>
</li>
<li class="{{ isset($active) && $active == 'reportshippingIn' ? 'active' : '' }}">
    <a href="{{ route('report.shippingIn') }}"><i class="icon-location4"></i> <span>LOADING AREA</span></a>
</li>
<li class="{{ isset($active) && $active == 'reportshipping' ? 'active' : '' }}">
    <a href="{{ route('report.shipping') }}"><i class="icon-truck"></i> <span>SHIPPING</span></a>
</li>
<li class="{{ isset($active) && $active == 'reportshippingcompleted' ? 'active' : '' }}">
    <a href="{{ route('report.shippingCompleted') }}"><i class="icon-truck"></i> <span>LIST STUFFING</span></a>
</li>
<li class="{{ isset($active) && $active == 'reportinvoice' ? 'active' : '' }}">
    <a href="{{ route('report.reportInvoice') }}"><i class="icon-paperplane"></i> <span>REPORT INVOICE</span></a>
</li>
@permission(['menu-balance-stock'])
<li class="{{ isset($active) && $active == 'reportbalancestock' ? 'active' : '' }}">
    <a href="{{ route('report.stockBalanceAcc') }}"><i class="icon-stackoverflow"></i> <span>BALANCE STOCK</span></a>
</li>
@endpermission
@permission(['menu-bapb'])
<li class="{{ isset($active) && $active == 'reportbapb' ? 'active' : '' }}">
    <a href="{{ route('report.bapbindex') }}"><i class="icon-fire"></i> <span>BAPB DESTROY</span></a>
</li>
@endpermission

@permission(['menu-metal-detector','menu-metal-pso'])
<li class="{{ isset($active) && $active == 'reportmetal' ? 'active' : '' }}">
    <a href="{{ route('report.metalReport') }}"><i class="icon-atom"></i> <span>REPORT METAL FINDING</span></a>
</li>
@endpermission

@endpermission

@permission(['menu-report-carton'])
<li class="navigation-header"><span>REPORT CARTON</span><i class="icon-menu" title="report"></i></li>
<li class="{{ isset($active) && $active == 'reportlistrequestcarton' ? 'active' : '' }}">
    <a href="{{ route('report.listrequest') }}"><i class="icon-screen-full"></i> <span>REQUEST RECEIVE</span></a>
</li>
<li class="{{ isset($active) && $active == 'reportlistrequestcartondetail' ? 'active' : '' }}">
    <a href="{{ route('report.listrequestdetail') }}"><i class="icon-office"></i> <span>REQUEST RECEIVE DETAIL</span></a>
</li>
<li class="{{ isset($active) && $active == 'reportrequestcarton' ? 'active' : '' }}">
    <a href="{{ route('report.requestCarton') }}"><i class="icon-cube2"></i> <span>REQUEST CARTON</span></a>
</li>
@endpermission

@permission(['menu-report-exim'])
<li class="navigation-header"><span>REPORT PLAN LOAD</span><i class="icon-menu" title="report"></i></li>
<li class="{{ isset($active) && $active == 'reportlistdeliveryorder' ? 'active' : '' }}">
    <a href="{{ route('report.listdeliveryorder') }}"><i class="icon-file-check2"></i> <span>LIST DELIVERY ORDER</span></a>
</li>

@endpermission
<!-- END OF MENU REPORT -->

<!-- MENU DOCUMENTATION -->
@permission(['menu-documentation'])
<li>
    <a href=""><span>DOCUMENTATION</span></a>
</li>
@endpermission
<!-- END OF MENU DOCUMENTATION -->

<!-- MENU NEW PLANLOAD  -->
@permission(['menu-plan-load', 'menu-detail-delivery-order'])
<li class="navigation-header"><span>EXIM PLAN LOAD NEW</span><i class="icon-menu" ></i></li>
@endpermission
@permission(['menu-plan-load'])
<li class="{{ isset($active) && $active == 'drafplan' ? 'active' : '' }}">
    <a href="{{ route('planload.drafPlan') }}"><i class="icon-file-text2"></i> <span>DRAF PLAN LOAD</span></a>
</li>
<li class="{{ isset($active) && $active == 'editdraf' ? 'active' : '' }}">
    <a href="{{ route('planload.editPoDraf') }}"><i class="icon-clipboard3"></i> <span>EDIT DRAF</span></a>
</li>
<li class="{{ isset($active) && $active == 'invstorage' ? 'active' : '' }}">
    <a href="{{ route('planload.invoiceStorage')}}"><i class="icon-box-add"></i> <span>GROUP INVOICE</span></a>
</li>
<li class="{{ isset($active) && $active == 'invdetail' ? 'active' : '' }}">
    <a href="{{route('planload.invoiceDetail')}}"><i class="icon-list"></i> <span>INVOICE DETAIL</span></a>
</li>
<li class="{{ isset($active) && $active == 'deliveryorder' ? 'active' : '' }}">
    <a href="{{ route('planload.deliveryOrder') }}"><i class="icon-truck"></i> <span>DELIVERY ORDER</span></a>
</li>
@endpermission
@permission(['menu-plan-load', 'menu-detail-delivery-order'])
<li class="{{ isset($active) && $active == 'detaildelivery' ? 'active' : '' }}">
    <a href="{{ route('planload.detailDelivery') }}"><i class="icon-rocket"></i> <span>DETAIL DELIVERY</span></a>
</li>
@endpermission
<!-- END NEW PLAN LOAD -->


<!-- MENU REKON ACCOUTING -->
@permission(['menu-rekon-accounting'])
<li class="navigation-header"><span>REPORT REKON ACCOUTING</span><i class="icon-menu" ></i></li>
<li class="{{ isset($active) && $active == 'rekoninventory' ? 'active' : '' }}">
    <a href="{{ route('report.Accinvetory') }}"><i class="icon-cart"></i> <span>INVENTORY</span></a>
</li>
<li class="{{ isset($active) && $active == 'rekonshipping' ? 'active' : '' }}">
    <a href="{{ route('report.Accshipment') }}"><i class="icon-truck"></i> <span>SHIPMENT</span></a>
</li>
<li class="{{ isset($active) && $active == 'rekonbacksewing' ? 'active' : '' }}">
    <a href="{{ route('report.Accbacktosewing') }}"><i class="icon-rotate-ccw2"></i> <span>BACK TO SEWING</span></a>
</li>
<li class="{{ isset($active) && $active == 'rekonbalancestock' ? 'active' : '' }}">
    <a href="{{ route('report.Accbalance') }}"><i class="icon-dropbox"></i> <span>BALANCE STOCK</span></a>
</li>
<li class="{{ isset($active) && $active == 'rekondeletepackinglist' ? 'active' : '' }}">
    <a href="{{ route('report.Accdeletepl') }}"><i class="icon-trash"></i> <span>DELETE PACKINGLIST</span></a>
</li>
<li class="{{ isset($active) && $active == 'rekonall' ? 'active' : '' }}">
    <a href="{{ route('report.rekonAll') }}"><i class="icon-stack-check"></i> <span>REPORT ALL</span></a>
</li>
@endpermission
<!-- END MENU REKON ACCOUTING -->

<!-- MENU FREE STOCK -->
@permission(['menu-do-truck'])
<li class="navigation-header"><span>DO TRUCKING</span><i class="icon-menu" ></i></li>
<li class="{{ isset($active) && $active == 'suratjalan' ? 'active' : '' }}">
    <a href="{{ route('delivery.index') }}"><i class="icon-truck"></i> <span>LIST DELIVERY ORDER</span></a>
</li>
 
@endpermission
<!-- END MENU FREE STOCK -->

<!-- MENU FREE STOCK -->
@permission(['menu-free-stock'])
<li class="navigation-header"><span>FREE STOCK</span><i class="icon-menu" ></i></li>
<li class="{{ isset($active) && $active == 'in_freestock' ? 'active' : '' }}">
    <a href="{{ route('freestock.checkIn') }}"><i class="icon-arrow-down7"></i> <span>IN FREE STOCK</span></a>
</li>
<li class="{{ isset($active) && $active == 'list_freestock' ? 'active' : '' }}">
    <a href="{{ route('freestock.listFreeStock') }}"><i class="icon-list-unordered"></i> <span>LIST FREE STOCK</span></a>
</li>
<li class="{{ isset($active) && $active == 'allocat_freestock' ? 'active' : '' }}">
    <a href="{{ route('freestock.allocation') }}"><i class="icon-sigma"></i> <span>FREE STOCK ALLOCATION</span></a>
</li>
<li class="{{ isset($active) && $active == 'out_freestock' ? 'active' : '' }}">
    <a href="{{ route('freestock.checkOut') }}"><i class="icon-arrow-up7"></i> <span>OUT FREE STOCK</span></a>
</li>
 
@endpermission
<!-- END MENU FREE STOCK -->

<!-- MENU STO ACC -->
@permission(['menu-sto-acc'])
<li class="navigation-header"><span>STO ACCOUNTING</span><i class="icon-menu" ></i></li>
<li class="{{ isset($active) && $active == 'scan_sto' ? 'active' : '' }}">
    <a href="{{ route('accSto.scanSto') }}"><i class="icon-html52"></i> <span>SCAN STO</span></a>
</li>
<li class="{{ isset($active) && $active == 'report_sto' ? 'active' : '' }}">
    <a href="{{ route('accSto.reportSto') }}"><i class="icon-stack-text"></i> <span>REPORT STO</span></a>
</li>
<li class="{{ isset($active) && $active == 'final_sto' ? 'active' : '' }}">
    <a href="{{ route('accSto.finalReportSto') }}"><i class="icon-database-check"></i> <span>FINAL STO</span></a>
</li>
@endpermission
<!-- END MENU STO ACC  -->

<!-- metal finding -->
@permission(['menu-metal-detector','menu-metal-pso'])
<li class="navigation-header"><span>METAL DETECTOR</span><i class="icon-menu" ></i></li>
@permission(['menu-metal-detector'])
<li class="{{ isset($active) && $active == 'scanmetal' ? 'active' : '' }}">
    <a href="{{ route('metalfind.scanMetal') }}"><i class="icon-cabinet"></i> <span>METAL FINDING</span></a>
</li>
<li class="{{ isset($active) && $active == 'listscanmetal' ? 'active' : '' }}">
    <a href="{{ route('metalfind.listScanMetal') }}"><i class="icon-dropbox"></i> <span>LIST METAL FINDING</span></a>
</li>
@endpermission

@permission(['menu-metal-pso'])
<li class="{{ isset($active) && $active == 'verifpso' ? 'active' : '' }}">
    <a href="{{ route('metalpso.psoVerify') }}"><i class="icon-cogs"></i> <span>METAL FINDING VERIFICATION
</span></a>
</li>
@endpermission

<!-- metal finding -->

@endpermission