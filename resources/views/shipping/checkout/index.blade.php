@extends('layouts.app', ['active' => 'shippingcheckout'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">STUFFING</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('shipping.checkout') }}"><i class="icon-home4 position-left"></i>STUFFING</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <form action="{{ route('shipping.ajaxSetStatusCheckout') }}" id="form-checkout">
            @csrf
            <div class="row">
                <div class="col-md-10">
                    <input type="text" class="form-control input-new" id="shipping-checkout" name="shippingCheckout" placeholder="#Scan ID"></input>
                    <input type="hidden" id="_barcodeid"></input>
                    <input type="hidden" id="_status"></input>
                </div>
                <div class="col-md-2">
                    <input type="text" id="count_scan" name="count_scan" value="0" style="text-align: center; font-size: 28px;"
                           class="form-control" readonly="readonly"
                           placeholder="">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="radio-inline" style="color: #39e600; font-size: 14px;"><input type="radio" name="radio_status" checked="checked" value="completed" style="width: 1.25em; height: 1.25em; "><b>COMPLETED</b></label>
                        <label class="radio-inline" style="color: red; font-size: 14px;"><input type="radio" name="radio_status" value="cancel" style="width: 1.25em; height: 1.25em; "><b>CANCEL</b></label>
                        <label class="radio-inline" style=" font-size: 14px;"><input type="radio" name="radio_status" value="sendsewing" style="width: 1.25em; height: 1.25em; "><b>BACK TO SEWING</b></label>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="show-result">
                <thead>
                    <tr>
                        <th>BARCODE ID</th>
                        <th>PLAN NUMBER</th>
                        <th>PO NUMBER</th>
                        <th>DEPARTMENT</th>
                        <th>STATUS</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('shipping.ajaxSetStatusCheckout') }}" id="set-status-checkout"></a>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#shipping-checkout').focus();

    //scan package
    $('.input-new').keypress(function(event) {
        if(event.which == 13) {
            event.preventDefault();
            var barcodeid = $('#shipping-checkout').val();
            var status = $('input[name=radio_status]:checked').val();

            $('#_barcodeid').val(barcodeid);
            $('#_status').val(status);

            //cancel
            if($('#shipping-checkout').val() == '') {
                $('#shipping-checkout').val('');
                $('#shipping-checkout').focus();
                $('#_barcodeid').val('');
                $('#_status').val('');
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: $('#set-status-checkout').attr('href'),
                data: {barcodeid: barcodeid, status: status},
                beforeSend: function() {
                    $('#shipping-checkout').val('');
                    $('#shipping-checkout').focus();
                    $('#_barcodeid').val('');
                    $('#_status').val('');
                },
                success: function(response){
                    $('#show-result > tbody').prepend(response);
                    var count_s = +$('#count_scan').val()+1;

                    $('#count_scan').val(count_s);
                },
                error: function(response){
                    if(response['status'] == 422) {
                        myalert('error',response['responseJSON']);
                    }
                    $('#shipping-checkout').val('');
                }
            });
        }
    });
});
</script>
@endsection
