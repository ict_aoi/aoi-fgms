@extends('layouts.app', ['active' => 'out_freestock'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">FREE STOCK CHECK OUT</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#"><i class="icon-home4 position-left"></i>Free Stock Check Out</a></li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <form action="{{ route('freestock.ajaxSetChcekOut') }}" id="form-checkout">
            @csrf
            <div class="row form-group">
                <div class="col-lg-10">
                    <input type="text" name="barcode_id" id="barcode_id" class="form-control input-new" placeholder="#Scan ID">
                </div>
                <div class="col-lg-2">
                    <input type="text" name="count_scan" id="count_scan" value="0" style="text-align: center; font-size: 28px;" class="form-control" readonly="">
                </div>
            </div>
        </form>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="table-list">
                <thead>
                    <tr>
                        <th>BARCODE ID</th>
                        <th>STYLE</th>
                        <th>ART. NO.</th>
                        <th>MANUFACTURING SIZE</th>
                        <th>QTY</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $('#barcode_id').focus();
 

    $('.input-new').keypress(function(event){
        if (event.which==13) {
            event.preventDefault();

            var barcode_id = $('#barcode_id').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: $('#form-checkout').attr('action'),
                data: {barcode_id: barcode_id},
                success: function(response){
                    $('#table-list').prepend(response);
                    var count_s = +$('#count_scan').val()+1;

                    $('#count_scan').val(count_s);
                    $('#barcode_id').val('');
                },
                error: function(response){
                    console.log(response);
                    if(response['status'] == 422) {
                        myalert('error', response['responseJSON']);
                    }
                    $('#barcode_id').val('');
                }
            });

        }
    })
});
</script>
@endsection