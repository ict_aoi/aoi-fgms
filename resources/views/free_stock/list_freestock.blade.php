@extends('layouts.app', ['active' => 'list_freestock'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">LIST STOCK</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#"><i class="icon-home4 position-left"></i>List Stock</a></li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<!-- <div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('freestock.getListFreeStock') }}" id="form_filter">
            @csrf
            <div class="form-group">
                <label><b>PO Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="po_number" id="po_number">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div> -->

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <button class="btn btn-primary" id="btn-calc" name="btn-calc">Stock Calculate</button>
        </div>
        <div class="row">
            <div class="table-responsive">
                <table class="table datatable-save-state" id="table-list">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>PO Number</th>
                            <th>Style</th>
                            <th>Season</th>
                            <th>Art. No.</th>
                            <th>Manuf. Size</th>
                            <th>Stock Qty</th>
                            <th>Allocation Qty</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<a href="{{ route('freestock.exportListFreeStock') }}" id="exportExcel"></a>
@endsection

@include('free_stock/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    // var url = $('#form_filter').attr('action');

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#exportExcel').attr('href');
                                            // + '?po_number=' + $('#po_number').val();
                }
            }
       ],
        ajax: {
            url: "{{ route('freestock.getListFreeStock') }}",
            type: "get"
            // data: function (d) {
            //     return $.extend({},d,{
            //         "po_number": $('#po_number').val(),
            //         "_token": _token
            //     });
            // }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'po_number', name: 'po_number'},
            {data: 'upc', name: 'upc'},
            {data: 'season', name: 'season'},
            {data: 'buyer_item', name: 'buyer_item'},
            {data: 'manufacturing_size', name: 'manufacturing_size'},
            {data: 'qty', name: 'qty'},
            {data: 'allocation_qty', name: 'allocation_qty'}
        ],
    });
    //end of datatables

    table.on('preDraw', function() {
        Pace.start();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    // $('#form_filter').submit(function(event) {
    //     event.preventDefault();

    //     loading_process();
    //     table.clear();
    //     table.draw();
    // });

    $(window).on('load',function(){
        table.draw();
    });

    $('#btn-calc').on('click',function(event){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : "{{ route('freestock.stockCalculate') }}",
            success: function(response) {
                
                var data_response = response.data;
                
                if (data_response.status==200) {
                    
                    myalert('success',response.output);
                }else{
                    myalert('error',response.output);
                }

                table.clear();
                table.draw();
            },
            error: function(response) {

                myalert('error',response);
                console.log(response);
                table.clear();
                table.draw();
            }
        });
    });
});
</script>
@endsection