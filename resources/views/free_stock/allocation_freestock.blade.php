@extends('layouts.app', ['active' => 'allocat_freestock'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">ALLOCATION STOCK</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#"><i class="icon-home4 position-left"></i>Allocation Stock</a></li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="col-lg-10">
            <form action="{{ route('freestock.getDataAllocation') }}" id="form_filter">
                @csrf
                <div class="form-group">
                    <label><b>PO Number</b></label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="po_number" id="po_number">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-1">
            <button type="button" class="btn btn-warning" id="btn-template" style="margin-top: 25px;"><span class="icon-file-download"></span> Template</button>
        </div>
        <div class="col-lg-1">
            <button type="button" class="btn btn-success" id="btn-mdupload" style="margin-top: 25px;"><span class="icon-file-upload"></span> Upload</button>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>PO Number</th>
                        <th>Old PO Number</th>
                        <th>Style</th>
                        <th>Art. No.</th>
                        <th>Manuf. Size</th>
                        <th>Qty</th>
                        <th>Qty Scan Out</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>


<a href="#" id="exportExcel"></a>
<a href="{{ route('freestock.templateAllocation') }}" id="exportTemplate"></a>
@endsection

@section('modal')
<div id="modal_upload_" class="modal fade" role="dialog">
    <form class="form-horizontal" action="{{ route('freestock.uploadAllocation') }}" id="form-upload" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content modal-lg">
                <div class="modal-body">
                    <div class="panel-body">
                        <fieldset>
                            <legend class="text-semibold">
                                <h4 id="title_upload"><i class="icon-upload4 position-left"></i>UPLOAD ALLOCATION<span id='title_po'></span></h4>

                            </legend>
                            <!-- upload excel -->
                            <div class="form-group">
                                <label class="col-lg-3 control-label">File Excel :</label>
                                <div class="col-lg-9">
                                    <input type="file" class="file-styled" name="file_upload" id="file" required>
                                    <span class="help-block" id="excel_po"></span>
                                    <span class="help-block">Accepted formats: xls xlsx.</span>
                                </div>
                            </div>

                            <div id="verify_data">

                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Upload</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@include('free_stock/_js_index_2')
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $('#btn-template').on('click',function(){
        window.location.href = $('#exportTemplate').attr('href');
    });

    $('#btn-mdupload').on('click',function(){
        $('#modal_upload_').modal('show');
    });


    $('#form-upload').submit(function(event){
        event.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : $('#form-upload').attr('action'),
            contentType: false,
            processData: false,
            data :new FormData(this) ,
            beforeSend: function() {
                $('#form-upload .modal-body').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Uploading File</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                
                var data_response = response.data;
                
                if (data_response.status==200) {
                    
                    myalert('success',response.output);
                }else{
                    myalert('error',response.output);
                }
                 $('#form-upload .modal-body').unblock();
                 $('#modal_upload_').modal('hide');
                 
            },
            error: function(response) {
                $('#form-upload .modal-body').unblock();
                myalert('error',response);
                console.log(response);
            }
        });
    });

    var url = $('#form_filter').attr('action');

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            url: url,
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "po_number": $('#po_number').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'po_number', name: 'po_number'},
            {data: 'old_po', name: 'old_po'},
            {data: 'upc', name: 'upc'},
            {data: 'buyer_item', name: 'buyer_item'},
            {data: 'manufacturing_size', name: 'manufacturing_size'},
            {data: 'qty_allocation', name: 'qty_allocation'},
            {data: 'qty_scan', name: 'qty_scan'}
        ],
    });

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        loading_process();
        table.clear();
        table.draw();
    });

    table.on('preDraw', function() {
        Pace.start();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });
});
</script>
@endsection