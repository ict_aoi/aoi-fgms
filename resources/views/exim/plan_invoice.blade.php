@extends('layouts.app', ['active' => 'eximinvoice'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('exim.invoice') }}"><i class="icon-box-add position-left"></i>INVOICE</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('exim.ajaxplanload') }}" id="form_filter">
            <div class="form-group" id="filter_by_statdate">
                <label><b>Choose PODD Check Date (month/day/year)</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_po">
                <label><b>Choose PO Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="po" id="po" placeholder="PO Number">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_custno">
                <label><b>Choose Customer Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="customer_number" id="customer_number" placeholder="Customer Number">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_mdd">
                <label><b>Choose MDD (month/day/year)</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="mdd_date" id="mdd_date">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_lc">
                <label><b>Choose LC Date (month/day/year)</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="lc_date" id="lc_date">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="stat">Filter by PODD Check Date</label>
                <label class="radio-inline hidden"><input type="radio" name="radio_status" value="po_">Filter by PO Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="custno">Filter by Customer Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="mdd">Filter by MDD</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="lc">Filter by LC Date</label>
            </div>
        </form>
        <div class="row">
            <button class="btn btn-danger" id="btnLoadSync"><i class="icon-sync"> Plan Load Sync</i></button>
        </div>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            {{--  <legend class="text-semibold"><i class="icon-make-group position-left"></i> Grouping to Invoice</legend>  --}}
            <div class="form-group">
                <div class="col-sm-3">
                    <label for="plan_stuffing">Plan Stuffing</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                        <input type="text" class="form-control pickadate" name="plan_stuffing" id="plan_stuffing" readonly>
                    </div>
                </div>
                <div class="col-sm-3">
                    <label for="invoice">Invoice</label>
                    <div class="input-group">
                        <div class="col-sm-10">
                            <input type="text" id="invoice" name="invoice" class="form-control" value="{{ $invoice_start }}">
                        </div>
                        <div class="col-sm-2">
                            <button id="searchCancel" title="Search Invoice Cancel"><i class="icon-folder-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <label for="so">SO</label>
                    <input type="text" id="so" name="so" class="form-control" maxlength="10" onkeypress="return isNumberKey(event)" readonly>
                </div>
                <div class="col-sm-2">
                    <label for="remark_co">Remark Container</label>
                    <input type="text" id="remark_co" name="remark_co" class="form-control" readonly>
                </div>

                <input type="hidden" id="is_cancel_invoice" name="is_cancel_invoice" value="0">

                <input type="hidden" id="mdd" name="mdd">
                <input type="hidden" id="kst_statisticaldate" name="kst_statisticaldate">
                <input type="hidden" id="customer_number_hide" name="customer_number_hide">
                <input type="hidden" id="kst_lcdate" name="kst_lcdate">
                <input type="hidden" id="fwd" name="fwd">
                <input type="hidden" id="destination" name="destination">
                <input type="hidden" id="shipmode" name="shipmode">

                <div class="col-sm-2 pull-right">
                    <label for="">Action</label>
                    <div class="input-group-btn"><br>
                        <button type="button" class="btn btn-primary" id="btnUpdate">Update</button>
                    </div>
                </div>
            </div>
        </div>
        <span class="help-block info-check">*No Checked</span><br>
        <div class="row">
            <div class="col-md-2">
                <input type="text" id="cbm_detail_value" name="cbm_detail_value" value="0" style="text-align: center; font-size: 28px;"
                        class="form-control" readonly="readonly"
                        placeholder="">
            </div>
        </div><br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-1"></div>
                <div class="col-md-2">
                    <label for="search_cust">Filter Customer :</label>
                    <select class="filter-select" data-placeholder="Filter Customer" id="search_cust"></select>
                </div>
                <div class="col-md-2 mdd-filter">
                    <label for="search_mdd">Filter MDD :</label>
                    <select class="filter-select" data-placeholder="Filter MDD" id="search_mdd"></select>
                </div>
                <div class="col-md-2 psfd-filter">
                    <label for="search_psfd">Filter PSFD :</label>
                    <select class="filter-select" data-placeholder="Filter PSFD" id="search_psfd"></select>
                </div>
                {{--  <div class="col-md-3">
                    <select class="filter-select" data-placeholder="Filter LC" id="search_lc"></select>
                </div>  --}}
                <div class="col-md-2 priority-filter">
                    <label for="search_priority">Filter Priority :</label>
                    <select class="filter-select" data-placeholder="Filter Priority" id="search_priority"></select>
                </div>
                <div class="col-md-2">
                    <label for="search_whs whs-filter">Filter WHS :</label>
                    <select class="filter-select" data-placeholder="Filter WHS" id="search_whs"></select>
                </div>
            </div>
        </div><br>

        <div class="table-responsive" id="listing-tab">
            <table class="table datatable-column-search-selects" id="table-list">
                <thead>
                    <tr>
                        {{--  <th><input type="checkbox" onclick="toggle(this)"></th>  --}}
                        <th>#</th>
                        <th>JENIS ORDER</th>
                        <th>ORDERNO</th>
                        <th>CRD</th>
                        <th>PSDD</th>
                        <th>STYLE</th>
                        <th class="filter-cust">CUST</th>
                        <th>SHIP MODE</th>
                        <th>Qty</th>
                        <th>CTN</th>
                        <th>CBM</th>
                        <th>Gross</th>
                        <th>Net</th>
                        <th>Article</th>
                        <th>Remarks</th>
                        <th>PLAN REF</th>
                        <th class="filter-mdd">MDD</th>
                        <th class="filter-lc">LC</th>
                        <th>PSFD 1</th>
                        <th>PSFD 2</th>
                        <th class="filter-whs">WHS CODE</th>
                        <th class="filter-priority">PRIORITY CODE</th>
                        <th>SUBSIDIARY</th>
                        <th>FWD</th>
                        <th>PODD CHECK</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                {{--  <tfoot>
                    <tr>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                    </tr>
                </tfoot>  --}}
            </table>
        </div>
    </div>
</div> 
@section('modal')
<div id="modal_invoice_cancel_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-md">
            <div class="panel-heading">
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <fieldset>
                        <legend class="text-semibold">
                            <h4 id="title-invoice-cancel"><i class="position-left"></i>Choose Invoice</h4>
                        </legend>

                        <div id="list-invoice-cancel" class="col-lg-12">
                            <!-- template pilihan -->
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_remarks" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-md">
            <div class="modal-body">
                <div class="panel-body">
                    <form action="{{ route('exim.setRemarkPlan') }}" id="remark_plan">
                        <div class="row form-group">
                            <div class="col-md-3">
                                <label><b>ORDER NO.   :</b></label>
                            </div>
                            <div class="col-md-3">
                                <label id="txorder"><b></b></label>
                            </div>
                            <div class="col-md-3">
                                <label><b>PLAN REF   :</b></label>
                            </div>
                            <div class="col-md-3">
                                <label id="txplan"><b></b></label>
                            </div>
                            
                        </div>
                        <div class="row form-group">
                            <label><b>REMARK ORDER</b></label>
                            <textarea id="rm_order" class="form-control"></textarea>
                        </div>
                        <div class="row form-group">
                            <label><b>REMARK EXIM</b></label>
                            <textarea id="rm_exim" class="form-control"></textarea>
                        </div>
                        <input type="text" id="idorder" hidden=""><input type="text" id="idplanref" hidden="">
                        <div class="row form-group">
                            <button type="submit" class="btn btn-primary" id="btn-save">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                        </div>
                    </form>
                </div>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
        </div>
    </div>
</div>
@endsection
<a href="{{ route('exim.ajaxplanload') }}" id="ajaxShowPlanLoad"></a>
<a href="{{ route('exim.ajaxplanloadajax') }}" id="ajaxShowPlanLoadAjax"></a>
<a href="{{ route('exim.ajaxplanloadajaxmdd') }}" id="ajaxShowPlanLoadAjaxMdd"></a>
<a href="{{ route('exim.ajaxplanloadajaxpsfd') }}" id="ajaxShowPlanLoadAjaxPsfd"></a>
<a href="{{ route('exim.ajaxplanloadajaxlc') }}" id="ajaxShowPlanLoadAjaxLc"></a>
<a href="{{ route('exim.ajaxplanloadajaxpriority') }}" id="ajaxShowPlanLoadAjaxPriority"></a>
<a href="{{ route('exim.ajaxplanloadajaxwhs') }}" id="ajaxShowPlanLoadAjaxWhs"></a>
<a href="{{ route('exim.exportplanload') }}" id="export_plan"></a>
<a href="{{ route('exim.updatePlanLoad') }}" id="updatePlanLoad"></a>
<a href="{{ route('exim.ajaxViewTemplateHeader') }}" id="url_view_template_header"></a>
<a href="{{ route('exim.ajaxViewTemplateHeaderExport') }}" id="url_view_template_header_export"></a>
<a href="{{ route('exim.updateShipmode') }}" id="updateShipmode"></a>
<a href="{{ route('exim.updateMdd') }}" id="updateMdd"></a>
<a href="{{ route('exim.updateRemark') }}" id="updateRemark"></a>
<a href="{{ route('exim.updatePsfdTo') }}" id="updatePsfdTo"></a>
<a href="{{ route('exim.updatePsfdFrom') }}" id="updatePsfdFrom"></a>
<a href="{{ route('exim.invoiceSerial') }}" id="invoiceSerial"></a>
<a href="{{ route('exim.ajaxGetInvoiceCancel') }}" id="ajaxGetInvoiceCancel"></a>
<a href="{{ route('exim.planloadexim') }}" id="planloadexim"></a>
@endsection


@include('report/_js_index')
@section('js')
<script type="text/javascript">

$(document).ready(function() {

    reloadajax();

    var url = $('#ajaxShowPlanLoad').attr('href');
    var url_view_header = $('#url_view_template_header').attr('href');
    var url_view_header_export = $('#url_view_template_header_export').attr('href');

    //datatables
        $.extend( $.fn.dataTable.defaults, {
            stateSave: true,
            autoWidth: false,
            autoLength: false,
            processing: true,
            serverSide: true,
            lengthMenu: [[-1], ["All"]],
            //dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                loadingRecords: '&nbsp;',
                processing: '<div class="spinner"></div>',
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            }
        });

        /* Create an array with the values of all the checkboxes in a column */
        $.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col )
        {
            return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
                return $('input', td).prop('checked') ? '1' : '0';
            } );
        }

        var _token = $("input[name='_token']").val();
        var table = $('#table-list').DataTable({
            //searching: true, 
            paging: false, 
           // info: false,
            scrollY:        "300px",
            scrollX:        true,
            scrollCollapse: true,
            fixedColumns:   {
                leftColumns: 5
            },
            sDom: 'tF',
            buttons: [
                {
                    text: 'Export to Excel',
                    className: 'btn btn-sm bg-success exportExcel',
                    action: function (e, dt, node, config)
                    {
                        var columns = table.settings().init().columns;
                        var current_order = table.order();

                        var column_name = columns[current_order[0][0]].name;
                        var direction = current_order[0][1];
                        var filter = table.search();
                        window.location.href = $('#export_plan').attr('href')
                                                + '?orderby=' + column_name
                                                + '&direction=' + direction
                                                + '&filterby=' + filter
                                                + '&radio_status=' + $('input[name=radio_status]:checked').val()
                                                + '&po=' + $('#po').val()
                                                + '&date_range=' + $('#date_range').val()
                                                + '&customer_number=' + $('#customer_number').val()
                                                + '&lc=' + $('#lc_date').val()
                                                + '&mdd=' + $('#mdd_date').val();
                    }
                }
            ],
            ajax: {
                url: url,
                //type: 'post',
                data: function (d) {
                    return $.extend({},d,{
                        "filterby": $('.dataTables_filter input').val(),
                        "radio_status": $('input[name=radio_status]:checked').val(),
                        "po": $('#po').val(),
                        "date_range": $('#date_range').val(),
                        "customer_number": $('#customer_number').val(),
                        "lc": $('#lc_date').val(),
                        "mdd": $('#mdd_date').val(),
                        "_token": _token
                    });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = table.page.info();
                var value = index+1+info.start;
                // $('td', row).eq(0).html(value);
                $('td', row).eq(1).css('min-width', '150px');
                $('td', row).eq(2).css('min-width', '110px');
                $('td', row).eq(3).css('min-width', '110px');
                $('td', row).eq(4).css('min-width', '110px');
                $('td', row).eq(5).css('min-width', '110px');
                $('td', row).eq(7).css('min-width', '178px');
                $('td', row).eq(14).css('min-width', '50px');
              //  $('td', row).eq(7).css('min-width', '178px');
            },
            columnDefs: [
                {
                    className: 'dt-center'
                },
               /* {
                    targets: [0],
                    orderDataType: 'dom-checkbox'
                }*/
                {
                    'targets': 0,
                    'createdCell':  function (td, cellData, rowData, row, col) {
                           $(td).attr('data-order', 0); 
                    }
                    //'orderDataType': 'dom-checkbox'
                 }
            ],
            columns: [
                // {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'checkbox', name: 'checkbox',  searchable: false, className: "dt-body-center", orderDataType: "dom-checkbox"},
                {data: 'order_type', name: 'order_type', sortable: false, orderable: false, searchable: false},
                {data: 'orderno', name: 'orderno', sortable: false, orderable: false, searchable: false},
                {data: 'crd', name: 'crd', sortable: false, orderable: false, searchable: false},
                {data: 'po_stat_date_adidas', name: 'po_stat_date_adidas', sortable: false, orderable: false, searchable: false},
                {data: 'style', name: 'style', sortable: false, orderable: false, searchable: false},
                {data: 'customer_number', name: 'customer_number'},
                {data: 'shipmode', name: 'shipmode', sortable: false, orderable: false, searchable: false},
                {data: 'total_item_qty', name: 'total_item_qty', sortable: false, orderable: false, searchable: false},
                {data: 'total_ctn', name: 'total_ctn', sortable: false, orderable: false, searchable: false},
                {data: 'cbm', name: 'cbm', sortable: false, orderable: false, searchable: false},
                {data: 'total_gross', name: 'total_gross', sortable: false, orderable: false, searchable: false},
                {data: 'total_net', name: 'total_net', sortable: false, orderable: false, searchable: false},
                {data: 'article', name: 'article', sortable: false, orderable: false, searchable: false},
                {data: 'remarks', name: 'remarks', sortable: false, orderable: false, searchable: false},
                {data: 'plan_ref', name: 'plan_ref', sortable: false, orderable: false, searchable: false},
                {data: 'mdd', name: 'mdd'},
                {data: 'kst_lcdate', name: 'kst_lcdate', sortable: false, orderable: false, searchable: false},
                {data: 'psfd_1', name: 'psfd_1', sortable: false, orderable: false, searchable: false},
                {data: 'psfd_2', name: 'psfd_2', sortable: false, orderable: false, searchable: false},
                {data: 'whs_code', name: 'whs_code'},
                {data: 'priority_code', name: 'priority_code'},
                {data: 'subsidiary', name: 'subsidiary'},
                {data: 'fwd', name: 'fwd', sortable: false, orderable: false, searchable: false},
                {data: 'podd_check', name: 'podd_check'},
                {data: 'status', name: 'status', sortable: false, orderable: false, searchable: false}
                
            ],
        });

        // Listen to change event from checkbox to trigger re-sorting
      //  $('#table-list').on('change', '.clplanref', function() {
            //.clplanref:checked
            // Update data-sort on closest <td>
            //$(this).closest('td').attr('data-order', this.checked ? 1 : 0);
        //    $(this).closest('td').attr('data-order', 1);
            
            // Store row reference so we can reset its data
        //    var $tr = $(this).closest('tr');
            
            // Force resorting
            /*table
            .row($tr)
            .invalidate()
            .order([ 0, 'asc' ])
            .draw();*/
       // });

        /*$('input:checkbox').on('change', function(e) {
            var row = $(this).closest('tr');
            var hmc = row.find(':checkbox:checked').length;
            var kluj = parseInt(hmc);
            console.log(kluj);
            row.find('td.counter').text(kluj);
            table.row(row).invalidate('dom');
        });*/  

         // Individual column searching with text inputs
        //$('.datatable-column-search-inputs tfoot td').not('.no-filter').each(function () {
          //  var title = $('.datatable-column-search-inputs thead th').eq($(this).index()).text();
            //$(this).html('<input type="text" class="form-control input-sm" placeholder="Search '+title+'" />');
          //  var select = $('<select class="filter-select" data-placeholder="Filter"><option value=""></option><option value="600000"></option></select>');
            //$(this).html(select);
        //});
        //var table = $('.datatable-column-search-inputs').DataTable();
        table.columns().every( function () {
            var that = this;
            that.search('').draw();
        //    $('select', this.footer()).on('keyup change clear', function (e) {
        //        if ( that.search() !== this.value ) {
                    
        //                if (e.keyCode == 13){
        //                    loading();
        //                    that.search(this.value).draw();
        //                }
        //        }
                
        //    });
        });
        $('#search_cust').on('change', function(){
            var search = $(this).val();

            // Hide all table tbody rows
            //$('#table-list tbody tr').hide();

            // Count total search result
            //var len = $('#table-list tbody td:nth-child(4):contains("'+search+'")').length;
            var len = $('#table-list tbody tr').length;

            if(len > 0){
                // Searching text in columns and show match row
               // $('#table-list tbody td:contains("'+search+'")').each(function(){
                //    $(this).closest('tr').show();
                //});
                loading();
                table
                .columns( '.filter-cust' )
                .search( search )
                .draw();

                reloadajaxmdd();
            }
        });

        $('#search_mdd').on('change', function(){
            var search = $(this).val();

            // Count total search result
            var len = $('#table-list tbody tr').length;

            if(len > 0){
           
                loading();
                table
                .columns( '.filter-mdd' )
                .search( search )
                .draw();

                reloadajaxpsfd();
            }
        });

        $('#search_psfd').on('change', function(){
            var search = $(this).val();

            // Count total search result
            var len = $('#table-list tbody tr').length;

            if(len > 0){
           
                loading();
                table
                .columns( '.filter-psfd' )
                .search( search )
                .draw();

                reloadajaxpriority();
            }
        });

        // udah gak dipakai
        $('#search_lc').on('change', function(){
            var search = $(this).val();

            // Count total search result
            var len = $('#table-list tbody tr').length;

            if(len > 0){
        
                loading();
                table
                .columns( '.filter-lc' )
                .search( search )
                .draw();
            }
        });
        //
        $('#search_priority').on('change', function(){
            var search = $(this).val();

            // Count total search result
            var len = $('#table-list tbody tr').length;

            if(len > 0){
           
                loading();
                table
                .columns( '.filter-priority' )
                .search( search )
                .draw();

                reloadajaxwhs();
            }
        });

        $('#search_whs').on('change', function(){
            var search = $(this).val();

            // Count total search result
            var len = $('#table-list tbody tr').length;

            if(len > 0){
           
                loading();
                table
                .columns( '.filter-whs' )
                .search( search )
                .draw();

            }
        });
        //
        table
        .on( 'preDraw', function () {
            Pace.start();
        } )
        .on( 'draw.dt', function () {
            Pace.stop();
            $('#listing-tab').unblock();
        } );
        //end of datatables

        // Enable Select2 select for individual column searching
        $('.filter-select').select2();

        $('#form_filter').submit(function(event) {
            event.preventDefault();
    
            //check location
            if($('#date_range').val() == '') {
                alert('Please select date range first');
                return false;
            }

            table.columns().every( function () {
                var that = this;
                that.search('').draw();
            });
    
            loading();
           //table.ajax.reload();
    
            table.draw();
            reloadajax();
            //reloadajaxmdd();
            //reloadajaxpriority();
            //reloadajaxwhs();
        })
    
    
        $('#date_range').on('change', function(){
            date = $(this).val();
        })

        $('#search_po').keyup(function(){
            // if ($(this).val().length >= 4) {
              // Search Text
              var search = $(this).val();

              // Hide all table tbody rows
              $('#table-list tbody tr').hide();

              // Count total search result
              var len = $('#table-list tbody td:nth-child(2):contains("'+search+'")').length;

              if(len > 0){
                // Searching text in columns and show match row
                $('#table-list tbody td:contains("'+search+'")').each(function(){
                  $(this).closest('tr').show();
                });
              }
          // }

        });
    
         //choose filter
        $('input[type=radio][name=radio_status]').change(function() {
            if (this.value == 'po_') {
                if($('#filter_by_po').hasClass('hidden')) {
                    $('#filter_by_po').removeClass('hidden');
                }
    
                $('#filter_by_statdate').addClass('hidden');
                $('#filter_by_cutsno').addClass('hidden');
                $('#filter_by_mdd').addClass('hidden');
                $('#filter_by_lc').addClass('hidden');
            }
            else if (this.value == 'stat') {
                if($('#filter_by_statdate').hasClass('hidden')) {
                    $('#filter_by_statdate').removeClass('hidden');
                }
    
                $('#filter_by_po').addClass('hidden');
                $('#filter_by_cutsno').addClass('hidden');
                $('#filter_by_mdd').addClass('hidden');
                $('#filter_by_lc').addClass('hidden');
            }
            else if(this.value == 'custno') {
                if($('#filter_by_custno').hasClass('hidden')) {
                    $('#filter_by_custno').removeClass('hidden');
                }
    
                $('#filter_by_po').addClass('hidden');
                $('#filter_by_statdate').addClass('hidden');
                $('#filter_by_mdd').addClass('hidden');
                $('#filter_by_lc').addClass('hidden');
            }
            else if(this.value == 'mdd') {
                if($('#filter_by_mdd').hasClass('hidden')) {
                    $('#filter_by_mdd').removeClass('hidden');
                }
    
                $('#filter_by_po').addClass('hidden');
                $('#filter_by_statdate').addClass('hidden');
                $('#filter_by_custno').addClass('hidden');
                $('#filter_by_lc').addClass('hidden');
            }
            else if(this.value == 'lc') {
                if($('#filter_by_lc').hasClass('hidden')) {
                    $('#filter_by_lc').removeClass('hidden');
                }
    
                $('#filter_by_po').addClass('hidden');
                $('#filter_by_statdate').addClass('hidden');
                $('#filter_by_custno').addClass('hidden');
                $('#filter_by_mdd').addClass('hidden');
            }
            
            loading();
    
            table.draw();
        });


    function calculate() {

        var total = 0;

        var info = '*No Checked';

        var arr = [];
        var po_ = [];
        var arrs = [];

        $('.clplanref:checked').each(function(e, i) {
            arr.push($(this).data('cbm'));
            arrs.push({
                'psfd1': $(this).data('psfd1'),
                'mdd': $(this).data('mdd'),
                'statistical': $(this).data('statistical'),
                'customer': $(this).data('customer'),
                'lc': $(this).data('lc'),
                'destination': $(this).data('destination'),
                'fwd': $(this).data('fwd'),
                'shipmode': $(this).data('shipmode')
            });
        });

        //cek psfd1 dll
        $.each(arrs, function(e, i) {
            var checkboxes = document.getElementsByName('selector[]');

            if(i['psfd1'] === ''){
                    myalert('error', 'PSFD 1 must be filled..!')
               
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = false;
                        }
                    }
            }

          /*  if(i['mdd'] === ''){
                    myalert('error', 'MDD must be filled..!')
            
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = false;
                        }
                    }
            }*/

            if(i['statistical'] === ''){
                    myalert('error', 'Statistical must be filled..!')
            
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = false;
                        }
                    }
            }

            if(i['customer'] === ''){
                    myalert('error', 'Customer number must be filled..!')
            
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = false;
                        }
                    }
            }

//            if(i['lc'] === ''){
//                    myalert('error', 'LC Date must be filled..!')
//            
//                    for (var i = 0; i < checkboxes.length; i++) {
//                        if (checkboxes[i].type == 'checkbox') {
//                            checkboxes[i].checked = false;
//                        }
//                    }
//            }

            //if(i['fwd'] === ''){
            //        myalert('error', 'FWD must be filled..!')
            
            //        for (var i = 0; i < checkboxes.length; i++) {
            //            if (checkboxes[i].type == 'checkbox') {
            //                checkboxes[i].checked = false;
            //            }
            //        }
            //}

            if(i['destination'] === ''){
                    myalert('error', 'Destination must be filled..!')
            
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = false;
                        }
                    }
            }

            // penomoran invoice
            if (e >= 0) {
                $('#plan_stuffing').removeAttr('readonly');
                $('#so').removeAttr('readonly');
                $('#remark_co').removeAttr('readonly');
                $('#plan_stuffing').val('');
                $('#so').val('');
                $('#remark_co').val('');
                // cek mdd, statistical date, lc date, customer number, fwd, destination
                if (e == 0){
                    $('#mdd').val(i['mdd']);
                    $('#kst_statisticaldate').val(i['statistical']);
                    $('#customer_number_hide').val(i['customer']);
                    $('#kst_lcdate').val(i['lc']);
                    $('#fwd').val(i['fwd']);
                    $('#destination').val(i['destination']);
                    $('#shipmode').val(i['shipmode']);
                }else{
                    
                    if (i['customer'] != $('#customer_number_hide').val() || i['fwd'] != $('#fwd').val() || i['destination'] != $('#destination').val() || i['shipmode'] != $('#shipmode').val() ) {

                        myalert('error', 'customer/fwd/destination/shipmode must be same..!')
               
                        for (var i = 0; i < checkboxes.length; i++) {
                            if (checkboxes[i].type == 'checkbox') {
                                checkboxes[i].checked = false;
                            }
                        }
                    }
                }
            }else{
                $('#plan_stuffing').attr('readonly', true);
                $('#so').attr('readonly', true);
                $('#remark_co').attr('readonly', true);
                $('#plan_stuffing').val('');
                $('#so').val('');
                $('#remark_co').val('');
            }

        });

        for (var i=0; i<arr.length; i++){
            total += parseFloat(arr[i]);
        }


        $('.clplanref:checked').each(function(e, i) {
            //$('#invoice').val($(this).data('invoice'));
            $('#so').val($(this).data('so'));
            //date  
            if ($(this).data('planstuffing') !== '' ) {
            //    $('#plan_stuffing').val('');
            //}else{
                var date = new Date($(this).data('planstuffing'));
                var picker = $('#plan_stuffing').pickadate('picker');
                picker.set('select', date);
            }

            po_.push($(this).data('po'));
        });

        // info
        var vals = "";

        for (var j=0; j<po_.length; j++){
                vals +=", "+po_[j];
        }

        if (vals) vals = vals.substring(1);


        if (arr.length > 0) {
            info = '*'+arr.length+' Plan Ref Number Checked ( '+vals+' )';
        }

        $('.info-check').html(info);

        if(isNaN(total)){
            $('#cbm_detail_value').val(0);
        }else{
            $('#cbm_detail_value').val(total.toFixed(3));
        }

    }

    // calculate();

    $('div').delegate('input:checkbox', 'click', calculate);


    $('#invoice, #so').on('input', function() {
        this.value = this.value.toUpperCase();
    });

    $('#btnUpdate').on('click', function(){
        var data = [];
        var invoice = $('#invoice').val();
        var plan_stuffing = $('#plan_stuffing').val();
        var customer_number_hide = $('#customer_number_hide').val();
        var fwd = $('#fwd').val();
        var shipmode = $('#shipmode').val();
        var mdd = $('#mdd').val();
        var kst_statisticaldate = $('#kst_statisticaldate').val();
        var kst_lcdate = $('#kst_lcdate').val();
        var destination = $('#destination').val();
        var so = $('#so').val();
        var remark_co = $('#remark_co').val();
        var cbm = $('#cbm_detail_value').val();
        var is_cancel_invoice = $('#is_cancel_invoice').val();
    
        $('.clplanref:checked').each(function() {
            data.push({
                id: $(this).val(),
                ponumber: $(this).data('po'),
                planrefnumber: $(this).data('planrefnumber')
            });
        });
    
        if (data.length > 0) {
            bootbox.confirm("Are you sure update invoice ?", function (result) {
                if (result) {

                    if(invoice == '' || plan_stuffing == '' || so == ''){
                        myalert('error', 'invoice or plan stuffing or SO not found..!');
                        return false;
                    }

                    if(so.length != 10){
                        myalert('error', 'SO must 10 character..!');
                        return false;
                    }

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'post',
                        url: $('#updatePlanLoad').attr('href'),
                        data: {data:data, invoice:invoice, plan_stuffing:plan_stuffing, so:so, remark_co:remark_co, customer_number_hide:customer_number_hide, fwd:fwd, shipmode:shipmode, mdd:mdd, kst_statisticaldate:kst_statisticaldate, kst_lcdate:kst_lcdate, destination:destination,
                        cbm:cbm, is_cancel_invoice:is_cancel_invoice, action:'invoice'},
                        success: function(response){
                            myalert('success', 'Invoice created succesfully..!')
                            $('#invoice').val('');
                            $('#plan_stuffing').val('');
                            $('#so').val('');
                            $('#remark_co').val('');
                            $('.info-check').html('*No Checked');

                            $('#cbm_detail_value').val(0);

                            loading();
                            table.draw();

                            invoice_next();
                        },
                        error: function(response) {
                            if(response['status'] == 422) {
                                myalert('error', 'Oopss Something wrong..!');
                            }
                        }
                    });
                }
            });
        }
    });

    $('#btnLoadSync').on('click', function(){
        bootbox.confirm("Are you sure synchronize plan load ?", function (result) {
            if (result) {
                loading();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'get',
                    url: $('#planloadexim').attr('href'),
                    success: function(response){
                        myalert('success', response);
                        table.draw();
                    },
                    error: function(response) {
                        if(response['status'] == 422) {
                            myalert('error', 'Oopss Something wrong..!');
                        }
                    }
                });
            }
        });
    });

    function invoice_next(){
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: $('#invoiceSerial').attr('href'),
            success: function(response){
                $('#invoice').val(response);
            },
            error: function(response) {
                if(response['status'] == 422) {
                    myalert('error', 'Oopss Something wrong..!');
                }
            }
        });
    }

    $('#table-list').on('blur', '.sp_unfilled', function() {
        var id = $(this).data('id');
        var po_number = $(this).data('ponumber');
        var plan_ref_number = $(this).data('planrefnumber');
        var shipmode = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateShipmode').attr('href'),
            data: {id:id, shipmode:shipmode, po_number:po_number, plan_ref_number:plan_ref_number},
            success: function(response){
                $('#sp_' + id).html(response);
                //$('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });

    });

    $('#table-list').on('blur', '.mdd_unfilled', function() {
        var id = $(this).data('id');
        var po_number = $(this).data('ponumber');
        var plan_ref_number = $(this).data('planrefnumber');
        var mdd = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateMdd').attr('href'),
            data: {id:id, mdd:mdd, po_number:po_number, plan_ref_number:plan_ref_number},
            success: function(response){
                $('#mdd_' + id).html(response);
                //$('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });

    });

    $('#table-list').on('blur', '.ro_unfilled', function() {
        var id = $(this).data('id');
        var po_number = $(this).data('ponumber');
        var plan_ref_number = $(this).data('planrefnumber');
        var remarks_order = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateRemark').attr('href'),
            data: {id:id, remarks_order:remarks_order, po_number:po_number, plan_ref_number:plan_ref_number},
            success: function(response){
                $('#ro_' + id).html(response);
                //$('#form_filter').submit();
               // myalert('success', 'GOOD..!');
            },
            error: function(response) {
               // myalert('error', 'Opss Something wrong..!');
                return false;
            }
        });

    });

    $('#table-list').on('blur', '.psfd_1_unfilled', function() {
        var id = $(this).data('id');
        var po_number = $(this).data('ponumber');
        var plan_ref_number = $(this).data('planrefnumber');
        var psfd_1 = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updatePsfdFrom').attr('href'),
            data: {id:id, psfd_1:psfd_1, po_number:po_number, plan_ref_number:plan_ref_number},
            success: function(response){
                $('#psfd_1_' + id).html(response);
                //$('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });

    });

    $('#table-list').on('blur', '.psfd_2_unfilled', function() {
        var id = $(this).data('id');
        var po_number = $(this).data('ponumber');
        var plan_ref_number = $(this).data('planrefnumber');
        var psfd_2 = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updatePsfdTo').attr('href'),
            data: {id:id, psfd_2:psfd_2, po_number:po_number, plan_ref_number:plan_ref_number},
            success: function(response){
                $('#psfd_2_' + id).html(response);
                //$('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });

    });

    //choose invoice cancel
    $('#searchCancel').on('click',function(){
        $('#modal_invoice_cancel_').modal('show');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url: $('#ajaxGetInvoiceCancel').attr('href'),
            beforeSend: function() {
                $('#modal_invoice_cancel_ > .modal-content').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response){
                $('#modal_invoice_cancel_ > .modal-content').unblock();
                $('#list-invoice-cancel').html(response);
            }
        });
    });

    //
    $('#list-invoice-cancel').on('click','#choose-invoice', function(){
        var invoice = $(this).val();
        $('#invoice').val(invoice);
        $('#is_cancel_invoice').val(1);
        $('#modal_invoice_cancel_').modal('hide');
    });


    $('#remark_plan').submit(function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url:$('#remark_plan').attr('action'),
            type: "POST",
            data:{
                "orderno": $('#idorder').val(),  
                "plan_ref": $('#idplanref').val(),
                "rm_order": $('#rm_order').val(), 
                "rm_exim": $('#rm_exim').val(),                           
                "_token": _token,
            },
            success: function(response){
               myalert('success',response);
                
                $('modal_remarks').modal('hide');


            },
            error: function(response) {
                if(response['status'] == 422) {
                    myalert('error',response);
                }
            }
        });
    });
    

});

//cust
function reloadajax() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url: $('#ajaxShowPlanLoadAjax').attr('href'),
        data: {date_range:$('#date_range').val(), search_cust:$('#search_cust').val(), search_mdd:$('#search_mdd').val(), search_lc:$('#search_lc').val() },
        success: function(response){

            $('#search_cust').empty();
            $('#search_cust').append('<option value=""></option>');

            $.each(response.data_cust, function(i,v){
                $('#search_cust').append('<option value="'+i+'">'+i+'</option>');
            })
        }
    });
}


//mdd
function reloadajaxmdd() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url: $('#ajaxShowPlanLoadAjaxMdd').attr('href'),
        data: {date_range:$('#date_range').val(), search_cust:$('#search_cust').val(), search_mdd:$('#search_mdd').val(), search_lc:$('#search_lc').val() },
        success: function(response){

            $('#search_mdd').empty();
            $('#search_mdd').append('<option value=""></option>');

            $.each(response.data_mdd, function(i,v){
                $('#search_mdd').append('<option value="'+i+'">'+i+'</option>');
            })
        }
    });
}

//lc
function reloadajaxlc() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url: $('#ajaxShowPlanLoadAjaxLc').attr('href'),
        data: {date_range:$('#date_range').val(), search_cust:$('#search_cust').val(), search_mdd:$('#search_mdd').val(),search_psfd:$('#search_psfd').val(), search_lc:$('#search_lc').val() },
        success: function(response){

            $('#search_lc').empty();
            $('#search_lc').append('<option value=""></option>');

            $.each(response.data_lc, function(i,v){
                $('#search_lc').append('<option value="'+i+'">'+i+'</option>');
            })
        }
    });
}

//priority
function reloadajaxpriority() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url: $('#ajaxShowPlanLoadAjaxPriority').attr('href'),
        data: {date_range:$('#date_range').val(), search_cust:$('#search_cust').val(), search_mdd:$('#search_mdd').val(),search_psfd:$('#search_psfd').val(), search_priority:$('#search_priority').val() },
        success: function(response){

            $('#search_priority').empty();
            $('#search_priority').append('<option value=""></option>');

            $.each(response.data_priority, function(i,v){
                $('#search_priority').append('<option value="'+i+'">'+i+'</option>');
            })
        }
    });
}
//by naufal
function reloadajaxpsfd() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url: $('#ajaxShowPlanLoadAjaxPsfd').attr('href'),
        data: {date_range:$('#date_range').val(), search_cust:$('#search_cust').val(), search_mdd:$('#search_mdd').val(), search_psfd:$('#search_psfd').val() },
        success: function(response){

            $('#search_psfd').empty();
            $('#search_psfd').append('<option value=""></option>');

            $.each(response.data_psfd, function(i,v){
                $('#search_psfd').append('<option value="'+i+'">'+i+'</option>');
            })
        }
    });
}

//whs
function reloadajaxwhs() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url: $('#ajaxShowPlanLoadAjaxWhs').attr('href'),
        data: {date_range:$('#date_range').val(), search_cust:$('#search_cust').val(), search_mdd:$('#search_mdd').val(), search_priority:$('#search_priority').val(), search_whs:$('#select_whs').val() },
        success: function(response){

            $('#search_whs').empty();
            $('#search_whs').append('<option value=""></option>');

            $.each(response.data_whs, function(i,v){
                $('#search_whs').append('<option value="'+i+'">'+i+'</option>');
            })
        }
    });
}

function toggle(source) {
    checkboxes = document.getElementsByName('selector[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = source.checked;
    }
}

function remarks(e){
    var orderno = e.getAttribute('data-orderno');
    var plan_ref = e.getAttribute('data-planref');
    var rorder = e.getAttribute('data-rorder');
    var rexim = e.getAttribute('data-rexim');

  
    $('#idorder').val(orderno);
    $('#idplanref').val(plan_ref);
    $('#txorder').text(orderno);
    $('#txplan').text(plan_ref);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url: "{{route('exim.getRemarkPlan')}}",
        data: {orderno:orderno, plan_ref:plan_ref},
        success: function(response){

            $('#rm_order').val(response.rkorder);
            $('#rm_exim').val(response.rkemxim);

        }
    });
    $('#modal_remarks').modal('show');
}

</script>
@endsection
