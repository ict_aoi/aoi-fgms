@extends('layouts.app', ['active' => 'eximdetailinvoice'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">Detail Invoice</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('exim.detailInvoice') }}" id="form_invoice"><i class="icon-location4 position-left"></i> Detail Invoice</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('exim.getdDetailInvoice') }}" id="form_filter">
            @csrf
            <div class="form-group" id="filter_by_invoice">
                <label><b>Invoice Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="invoice_no" id="invoice_no">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden"  id="filter_by_orderno">
                <label><b>PO Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="orderno" id="orderno" >
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_stuffing">
                <label><b>Plan Stuffing</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="planstuff" id="planstuff">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_podd">
                <label><b>PODD Check</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="podd" id="podd">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="invoice">Filter by Invoice Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="po">Filter by PO Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="plan">Filter by Plan Stuffing</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="pdcheck">Filter by PODD</label>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row {{ Auth::user()->admin_role !== 1 ? 'hidden' : '' }}">
            <div class="col-md-2"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->factory_id }}" {{ $val->factory_id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr> 
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>PO</th>
                        <th>Plan Stuffing</th>
                        <th>SO</th>
                        <th>Invoice</th>
                        <th width="200px">FWD</th>
                        <th width="200px">Ship Mode</th>
                        <th>QTY</th>
                        <th>CTN</th>
                        <th>CBM</th>
                        <th>GW</th>
                        <th>NW</th>
                        <th>Remark</th> 
                        <th>Fact</th>
                        <th width="170px">Plan Ref Number</th>                                          
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- by naufal -->
<a href="{{ route('exim.updatePlanRef') }}" id="updatePlanRef"></a>
<a href="{{ route('exim.exportDetailInvoice') }}" id="exportDetailInvoice"></a>
@endsection

@section('modal')
<div id="modal_remark" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-lg">
            <div class="modal-body">
                <div class="panel">
                    <div class="panel-body">
                        <form action="{{ route('exim.setRemark') }}" id="form-remark">
                            <div class="row">
                                <div class="col-md-3">
                                    <label><b>ORDER NO : </b></label>
                                    <label id="txorder"></label>
                                    <input type="text" name="torder" id="torder" hidden="">
                                </div>
                                <div class="col-md-3">
                                    <label><b>INVOICE NO : </b></label>
                                    <label id="txinvoice"></label>
                                    <input type="text" name="tinvoice" id="tinvoice" hidden="">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <label><b>REMARK EXIM</b></label>
                                <textarea id="remarkexim" class="form-control"></textarea>
                            </div>
                            <div class="row">
                                <label><b>REMARK CONTAINER</b></label>
                                <textarea id="remarkcont" class="form-control"></textarea>
                            </div>
                            <hr>
                            <div class="row">
                                <button type="submit" id="btn-save" class="btn btn-primary">Save</button>
                                <button type="button" class="btn btn-default" id='close' onclick="chide();">Close</button> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@include('exim/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    var url = $('#form_filter').attr('action');

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    
                    window.location.href = $('#exportDetailInvoice').attr('href')
                                            + '?invoice=' + $('#invoice_no').val()
                                            + '&orderno=' + $('#orderno').val()
                                            + '&planstuff=' + $('#planstuff').val()
                                            + '&podd=' + $('#podd').val()
                                            + '&factory_id=' + $('#factory_id').val()
                                            + '&radio=' + $('input[name=radio_status]:checked').val();
                }
            }
       ],
        ajax: {
            url: url,
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "radio": $('input[name=radio_status]:checked').val(),
                    "invoice": $('#invoice_no').val(),
                    "orderno": $('#orderno').val(),
                    "planstuff": $('#planstuff').val(),
                    "podd": $('#podd').val(),
                    "factory_id": $('#factory_id').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(5).css('min-width', '200px');
            $('td', row).eq(6).css('min-width', '200px');
            $('td', row).eq(8).css('min-width', '40px');
            $('td', row).eq(13).css('min-width', '70px');
            $('td', row).eq(14).css('min-width', '170px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'orderno', name: 'orderno'},
            {data: 'plan_stuffing', name: 'plan_stuffing'},
            {data: 'so', name: 'so'},
            {data: 'invoice', name: 'invoice'},
            {data: 'fwd', name: 'fwd'},
            {data: 'shipmode', name: 'shipmode'},
            {data: 'total_item_qty', name: 'total_item_qty'},
            {data: 'total_ctn', name: 'total_ctn'},
            {data: 'cbm', name: 'cbm'},
            {data: 'total_gross', name: 'total_gross'},
            {data: 'total_net', name: 'total_net'},
            {data: 'remark', name: 'remark'},
            {data: 'factory_id', name: 'factory_id'},
            {data: 'plan_ref', name: 'plan_ref'},
            {data: 'action', name:'action'},    
        ],
    });
    //end of datatables

    table.on('preDraw', function() {
        Pace.start();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        loading_process();
        table.clear();
        table.draw();
    });


    // $('#table-list').on('blur', '.planref_unfilled', function() {
    //     var id = $(this).data('id');
 
    //     var plan_ref = $(this).val();

    //     $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         }
    //     });
    //     $.ajax({
    //         type: 'post',
    //         url: $('#updatePlanRef').attr('href'),
    //         data: {plan_ref:plan_ref, orderno:id},
    //         beforeSend:function(){
    //             loading_process();
    //         },
    //         success: function(response){
    //             $('#planref_' + id).html(response);
    //             $('#form_filter').submit();
    //         },
    //         error: function(response) {
    //             return false;
    //         }
    //     });

    // });

    $('#table-list').on('blur', '.plenref_unfilled', function() {
        var orderno = $(this).data('orderno');
 
        var plan_ref = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updatePlanRef').attr('href'),
            data: {plan_ref:plan_ref, orderno:orderno},
            beforeSend:function(){
                loading_process();
            },
            success: function(response){
                $('#table-list').unblock();
                $('#planref_' + orderno).html(response);
                $('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });

    });

    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'invoice') {
            if($('#filter_by_invoice').hasClass('hidden')) {
                $('#filter_by_invoice').removeClass('hidden');
            }

            $('#filter_by_orderno').addClass('hidden');
            $('#filter_by_orderno').addClass('hidden');
            $('#filter_by_stuffing').addClass('hidden');
            $('#filter_by_podd').addClass('hidden');

        }else if(this.value=='po'){
             if($('#filter_by_orderno').hasClass('hidden')) {
                $('#filter_by_orderno').removeClass('hidden');
            }

            $('#filter_by_invoice').addClass('hidden');
            $('#filter_by_invoice').addClass('hidden');
            $('#filter_by_stuffing').addClass('hidden');
            $('#filter_by_podd').addClass('hidden');

        }else if(this.value=='plan'){
             if($('#filter_by_stuffing').hasClass('hidden')) {
                $('#filter_by_stuffing').removeClass('hidden');
            }

            $('#filter_by_invoice').addClass('hidden');
            $('#filter_by_invoice').addClass('hidden');
            $('#filter_by_orderno').addClass('hidden');
            $('#filter_by_podd').addClass('hidden');

        }else if(this.value=='pdcheck'){
             if($('#filter_by_podd').hasClass('hidden')) {
                $('#filter_by_podd').removeClass('hidden');
            }

            $('#filter_by_invoice').addClass('hidden');
            $('#filter_by_invoice').addClass('hidden');
            $('#filter_by_orderno').addClass('hidden');
            $('#filter_by_stuffing').addClass('hidden');

        }
        
    });

    $('#factory_id').on('change', function() {
        loading_process();
        table.draw();
    });


    $('#form-remark').submit(function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url:$('#form-remark').attr('action'),
            type: "POST",
            data:{
                "orderno": $('#torder').val(),  
                "invoice": $('#tinvoice').val(),
                "remark_exim": $('#remarkexim').val(), 
                "remark_cont": $('#remarkcont').val(),                           
                "_token": _token,
            },
            success: function(response){
               myalert('success',response);
                chide();
               $('#form_filter').submit();
            },
            error: function(response) {
                if(response['status'] == 422) {
                    myalert('error',response);
                }
            }
        });


    });


    $('#table-list').on('blur', '.fwd_unfilled', function() {
        var dfwd = $(this).data('fwd');
        var invoice = $(this).data('invoice');
        var fwd = $(this).val();
        if (dfwd!=fwd) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: "{{route('exim.updateFwdinv')}}",
                data: {invoice:invoice, fwd:fwd},
                beforeSend:function(){
                    loading_process();
                },
                success: function(response){
                    $('#table-list').unblock();
                    myalert('success',response);
                    $('#form_filter').submit();
                },
                error: function(response) {
                    if(response['status'] == 422) {
                        myalert('error',response);
                    }
                    return false;
                }
            });
        }
        

    });

    $('#table-list').on('blur', '.ship_unfilled', function() {
        var dshipmode = $(this).data('ship');
        var invoice = $(this).data('invoice');
        var shipmode = $(this).val();
     
        if (dshipmode!=shipmode) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: "{{route('exim.updateShipinv')}}",
                data: {invoice:invoice, shipmode:shipmode},
                beforeSend:function(){
                    loading_process();
                },
                success: function(response){
                    $('#table-list').unblock();
                    myalert('success',response);
                    $('#form_filter').submit();
                },
                error: function(response) {
                    if(response['status'] == 422) {
                        myalert('error',response);
                    }
                    return false;
                }
            });
        }
        
    });

    
    
});

function deletepo(e){
        var _token = $("input[name='_token]").val();
         var id = e.getAttribute('data-id');
         var po_number = e.getAttribute('data-orderno');
         var invoice = e.getAttribute('data-invoice');
         var refs = $('#form_invoice').attr('href');
         console.log(po_number);
         bootbox.confirm("Are you sure delete PO "+po_number+ " from Invoice "+invoice+"?", function(result){

                if (result) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url:"{{ route('exim.deletePO') }}",
                        type: "POST",
                        data:{
                            "id": id,                           
                            "_token": _token,
                        },
                        success: function(response){
                            myalert('success', 'Invoice cancel successfully');
                            
                            $('#form_filter').submit();
                        },
                        error: function(response) {
                            if(response['status'] == 422) {
                                myalert('error', 'Oopss Something wrong..!');
                            }
                        }
                    });
                }
         });
    }

function remark(e){
    var orderno = e.getAttribute('data-orderno');
    var invoice = e.getAttribute('data-invoice');
    $('#txorder').text(orderno);
    $('#txinvoice').text(invoice);
    $('#torder').val(orderno);
    $('#tinvoice').val(invoice);
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }); 
    $.ajax({
        type: 'get',
        url: "{{route('exim.getRemark')}}",
        data: {orderno:orderno, invoice:invoice},
        success: function(response){

            $('#remarkexim').val(response.exim);
            $('#remarkcont').val(response.cont);
            

        }
    });
    $('#modal_remark').modal('show');
}

function chide(){
    $('#remarkexim').val('');
    $('#remarkcont').val('');
    $('#modal_remark').modal('hide');
}

</script>
@endsection
