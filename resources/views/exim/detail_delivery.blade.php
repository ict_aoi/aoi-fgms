@extends('layouts.app', ['active' => 'detailDelivery'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">Detail Delivery</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('exim.detailDelivery') }}" id="form_delivery"><i class="icon-location4 position-left"></i> Detail Delivery</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<!-- <div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('exim.ajaxGetDelivery') }}" id="form_filter">
            @csrf
            <div class="form-group" id="filter_by_delivery">
                <label><b>Delivery Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="delivery_no" id="delivery_no">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden"  id="filter_by_invoice">
                <label><b>Invoice Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="invoice_no" id="invoice_no" >
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
             <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="delivery_num">Filter by Delivery Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="invoice_num">Filter by Invoice Number</label>
            </div>
        </form>
    </div>
</div> -->
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('exim.ajaxGetDelivery') }}" id="form_filter">
            <div class="form-group" id="filter_by_delivery">
                <label><b>Delivery Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="delivery_no" id="delivery_no" >
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_stuffing">
                <label><b>Plan Stuffing</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_invoice">
                <label><b>Invoice Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="invoice_no" id="invoice_no" >
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="delivery_num">Filter by Delivery Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="plan_stuff">Filter by Plan Stuffing Date</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="invoice_num">Filter by Invoice Number</label>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Delivery Number</th>
                        <th>Nopol</th>
                        <th>Invoice</th>
                        <th>Plan Stuffing</th>
                        <th>SO</th>
                        <th>Ship Mode</th>
                        <th>Fwd</th>
                        <th>Dest</th>
                        <th>PO</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- by naufal -->

@endsection

@include('packing/packing_list/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    var url = $('#form_filter').attr('action');

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            url: url,
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "radio": $('input[name=radio_status]:checked').val(),
                    "no_delivery": $('#delivery_no').val(),
                    "no_invoice": $('#invoice_no').val(),
                    "date_range": $('#date_range').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'delivery_number', name: 'delivery_number'},
            {data: 'nopol', name: 'nopol'},
            {data: 'invoice', name: 'invoice'},
            {data: 'plan_stuffing', name: 'plan_stuffing'},
            {data: 'so', name: 'so'},
            {data: 'shipmode', name: 'shipmode'},
            {data: 'fwd', name: 'fwd'},
            {data: 'destination', name: 'destination'},
            {data: 'orderno', name: 'orderno'},
            {data: 'action', name:'action'},    
        ],
    });
    //end of datatables

    table.on('preDraw', function() {
        Pace.start();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        //check location
        // if($('#delivery_no').val() == ''||$('#invoice_no').val() == '') {
        //     alert('Please input invoice or delivery number first');
        //     return false;
        // }

        loading_process();

        table.draw();
    });

    

    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'delivery_num') {
            if($('#filter_by_delivery').hasClass('hidden')) {
                $('#filter_by_delivery').removeClass('hidden');
            }

            $('#filter_by_invoice').addClass('hidden');
            $('#filter_by_stuffing').addClass('hidden');

        }else if(this.value=='invoice_num'){
             if($('#filter_by_invoice').hasClass('hidden')) {
                $('#filter_by_invoice').removeClass('hidden');
            }

            $('#filter_by_delivery').addClass('hidden');
            $('#filter_by_stuffing').addClass('hidden');

        }else if(this.value=='plan_stuff'){
             if($('#filter_by_stuffing').hasClass('hidden')) {
                $('#filter_by_stuffing').removeClass('hidden');
            }

            $('#filter_by_delivery').addClass('hidden');
            $('#filter_by_invoice').addClass('hidden');

        }
        
    });



});

function deleteinv(e){
        var _token = $("input[name='_token]").val();
         var invoice = e.getAttribute('data-id');
         var delivery_number = e.getAttribute('data-delivery');
         
         
         bootbox.confirm("Are you sure delete invoice "+invoice+ " from delivery number "+delivery_number+"?", function(result){

                if (result) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url:"{{route('exim.deleteInv')}}",
                        type: "POST",
                        data:{
                            "invoice": invoice,
                            "delivery_number":delivery_number,                           
                            "_token": _token,
                        },
                        success: function(response){
                            myalert('success', 'Invoice cancel successfully');
                            
                            $('#form_filter').submit();
                        },
                        error: function(response) {
                            if(response['status'] == 422) {
                                myalert('error', 'Oopss Something wrong..!');
                            }
                        }
                    });
                }
         });
    }




</script>
@endsection
