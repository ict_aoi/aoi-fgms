@extends('layouts.app', ['active' => 'eximplanstuffing'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('exim.planstuffing') }}"><i class="icon-office position-left"></i>PLAN STUFFING</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('exim.ajaxGetDataPlanStuffing') }}" id="form_filter">
            <div class="form-group" id="filter_by_date">
                <label><b>Choose Date (month/day/year)</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_invoice">
                <label><b>Choose Invoice Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="invoice" id="invoice" placeholder="Invoice Number">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_so">
                <label><b>Choose SO</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="so" id="so" placeholder="SO">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="date">Filter by Date</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="invoice">Filter by Invoice</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="so">Filter by SO</label>
            </div>
        </form>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>PLAN STUFFING</th>
                        <th>INVOICE</th>
                        <th>SO</th>
                        <th>ORDERNO</th>
                        <th>PLAN REFF</th>
                        <th>STYLE</th>
                        <th>CRD</th>
                        <th>DEST</th>
                        <th>SHIP MODE</th>
                        <th>FWD</th>
                        <th>GW</th>
                        <th>NW</th>
                        <th>CBM</th>
                        <th>CTN</th>
                        <th>QTY</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('exim.exportplanstuffing') }}" id="export_plan"></a>
@endsection


@include('report/_js_index')
@section('js')
<script type="text/javascript">

    $(document).ready(function(){

    var url = $('#form_filter').attr('action');

    //datatables
        $.extend( $.fn.dataTable.defaults, {
            stateSave: true,
            autoWidth: false,
            autoLength: false,
            processing: true,
            serverSide: true,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            }
        });

        var _token = $("input[name='_token']").val();
        var table = $('#table-list').DataTable({
            buttons: [
                {
                    text: 'Export to Excel',
                    className: 'btn btn-sm bg-success exportExcel',
                    action: function (e, dt, node, config)
                    {
                        var columns = table.settings().init().columns;
                        var current_order = table.order();

                        var column_name = columns[current_order[0][0]].name;
                        var direction = current_order[0][1];
                        var filter = table.search();
                        window.location.href = $('#export_plan').attr('href')
                                                + '?orderby=' + column_name
                                                + '&direction=' + direction
                                                + '&radio_status=' + $('input[name=radio_status]:checked').val()
                                                + '&date_range=' + $('#date_range').val()
                                                + '&invoice=' + $('#invoice').val()
                                                + '&so=' + $('#so').val()
                                                + '&filterby=' + filter;
                    }
                }
            ],
            ajax: {
                url: url,
                type: 'post',
                data: function (d) {
                    return $.extend({},d,{
                        "filterby": $('.dataTables_filter input').val(),
                        "radio_status": $('input[name=radio_status]:checked').val(),
                        "date_range": $('#date_range').val(),
                        "invoice": $('#invoice').val(),
                        "so": $('#so').val(),
                        "_token": _token
                    });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = table.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
                // $('td', row).eq(1).css('min-width', '150px');
                // $('td', row).eq(2).css('min-width', '150px');
            },
            columnDefs: [
                {
                    className: 'dt-center'
                }
            ],
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'plan_stuffing', name: 'plan_stuffing'},
                {data: 'invoice', name: 'invoice'},
                {data: 'so', name: 'so'},
                {data: 'orderno', name: 'orderno'},
                {data: 'plan_reff', name: 'plan_reff'},
                {data: 'style', name: 'style'},
                {data: 'crd', name: 'crd'},
                {data: 'destination', name: 'destination'},
                {data: 'shipmode', name: 'shipmode'},
                {data: 'fwd', name: 'fwd'},
                {data: 'gw', name: 'gw'},
                {data: 'nw', name: 'nw'},
                {data: 'cbm', name: 'cbm'},
                {data: 'ctn', name: 'ctn'},
                {data: 'qty', name: 'qty'}
                
            ],
        });
        //end of datatables

        table
        .on( 'preDraw', function () {
            Pace.start();
        } )
        .on( 'draw.dt', function () {
            Pace.stop();
            $('#table-list').unblock();
        } );

        $('#form_filter').submit(function(event) {
            event.preventDefault();
            var radio_status = $('input[name=radio_status]:checked').val();
    
            //check
            if(radio_status == 'date'){
                if($('#date_range').val() == '') {
                    myalert('error', 'Please select date range first');
                    return false;
                }
            }else if(radio_status == 'invoice') {
                if($('#invoice').val() == '') {
                    myalert('error', 'Please input invoice first');
                    return false;
                }
            }else if(radio_status == 'so') {
                if($('#so').val() == '') {
                    myalert('error', 'Please input SO first');
                    return false;
                }
            }
    
            loading_process();
    
            table.draw();
        });

    });
        
    $('input[name=radio_status]').change(function(){
        if(this.value == 'date') {
            $('#filter_by_date').removeClass('hidden');
            $('#filter_by_invoice').addClass('hidden');
            $('#filter_by_so').addClass('hidden');
        }else if(this.value == 'invoice') {
            $('#filter_by_date').addClass('hidden');
            $('#filter_by_invoice').removeClass('hidden');
            $('#filter_by_so').addClass('hidden');
        }else if(this.value == 'so') {
            $('#filter_by_date').addClass('hidden');
            $('#filter_by_invoice').addClass('hidden');
            $('#filter_by_so').removeClass('hidden');
        }
    });

</script>
@endsection
