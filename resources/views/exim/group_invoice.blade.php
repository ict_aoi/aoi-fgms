@extends('layouts.app', ['active' => 'eximgroupinvoice'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('exim.groupinvoice') }}"><i class="icon-truck position-left"></i>DELIVERY ORDER</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat hidden">
    <div class="panel-body">
        <form action="{{ route('exim.ajaxinvoice') }}" id="form_filter">
            <div class="form-group hidden" id="filter_by_all">
                <div class="input-group">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_fwd">
                <label><b>Choose FWD</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="fwd" id="fwd" placeholder="FWD">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_shipmode">
                <label><b>Choose Shipmode</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="shipmode" id="shipmode" placeholder="Shipmode">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="all">All</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="fw">Filter by FWD</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="ship">Filter by Shipmode</label>
            </div>
        </form>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <input type="hidden" id="fwd_hide" name="fwd_hide">
            <input type="hidden" id="shipmode_hide" name="shipmode_hide">
            <input type="hidden" id="customer_hide" name="customer_hide">
            <input type="hidden" id="plan_stuffing_hide" name="plan_stuffing_hide">
            {{--  <legend class="text-semibold"><i class="icon-make-group position-left"></i> Grouping to Invoice</legend>  --}}
            <div class="form-group">
                <div class="col-sm-2">
                    <label for="delivery_number">Delivery Order:</label>
                    <input type="text" id="delivery_number" name="delivery_number" class="form-control" value="{{ $sj_start }}" readonly>
                </div>
                <div class="col-sm-2">
                    <label for="nopol">NOPOL:</label>
                    <input type="text" id="nopol" name="nopol" class="form-control" readonly required>
                </div>
                <div class="col-sm-2">
                    <label for="drivers_name">Drivers Name:</label>
                    <input type="text" id="drivers_name" name="drivers_name" class="form-control text-capitalize" readonly required>
                </div>
                <div class="col-sm-2">
                    <label for="drivers_phone">Drivers Phone:</label>
                    <input type="text" id="drivers_phone" name="drivers_phone" class="form-control" readonly required>
                </div>
                <div class="col-sm-2">
                    <label for="warehouse">Warehouse:</label>
                    <input type="text" id="warehouse" name="warehouse" class="form-control text-capitalize" readonly required>
                </div>
                <div class="col-sm-2 pull-right">
                    <label for="">Action</label>
                    <div class="input-group-btn"><br>
                        <button type="button" class="btn btn-primary" id="btnUpdate">Update</button>
                    </div>
                </div>
            </div>
        </div>
        <span class="help-block info-check">*No Checked</span><br>
        <button class="pull-right" type="button" onclick="window.location.reload();" title="Refresh Page"><i class="icon-database-refresh"></i></button>
        <div class="row">
            {{--  <div class="col-md-"></div>  --}}
            <div class="col-md-2">
                <input type="text" id="cbm_detail_value" name="cbm_detail_value" value="0" style="text-align: center; font-size: 28px;"
                        class="form-control" readonly="readonly"
                        placeholder="">
            </div>
        </div><br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-3 plan-filter">
                    <label for="search_plan_stuffing">Filter Plan Stuffing :</label>
                    <select class="form-control filter-select" data-placeholder="Filter Plan Stuffing" id="search_plan_stuffing"></select>
                    {{--  <div class="input-group">
                        <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                        <input type="text" class="form-control pickadate" id="search_plan_stuffing">
                    </div>  --}}
                </div>
                <div class="col-md-3 shipmode-filter">
                    <label for="search_shipmode">Filter Shipmode :</label>
                    <select class="form-control filter-select" data-placeholder="Filter Shipmode" id="search_shipmode"></select>
                </div>
                <div class="col-md-3 fwd-filter">
                    <label for="search_fwd">Filter FWD :</label>
                    <select class="form-control filter-select" data-placeholder="Filter FWD" id="search_fwd"></select>
                </div>
                <div class="col-md-3 destination-filter">
                    <label for="search_destination">Filter Destination :</label>
                    <select class="form-control filter-select" data-placeholder="Filter Destination" id="search_destination"></select>
                </div>
            </div>
        </div><br>
        <div class="table-responsive" id="listing-tab">
            <table class="table datatable-column-search-inputs" id="table-list">
                <thead>
                    <tr>
                        
                        <th>#</th>
                        <th class="filter-plan">PLAN</th>
                        <th>INVOICE</th>
                        <th>REMARK CONT.</th>
                        <th>SO</th>
                        <th class="filter-shipmode">SHIP MODE</th>
                        <th class="filter-fwd">FWD</th>
                        <th class="filter-destination">DEST</th>
                        <th>CUST NO</th>
                        <th>CBM</th>
                        <th>PO</th>
                        <th>PLAN REF</th>
                        <th>ACT</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                        <td class="no-filter"></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('exim.ajaxinvoice') }}" id="ajaxShowInvoice"></a>
<a href="{{ route('exim.exportinvoice') }}" id="export_invoice"></a>
<a href="{{ route('exim.updateInvoice') }}" id="updateInvoice"></a>
<a href="{{ route('exim.cancelInvoice') }}" id="cancelInvoice"></a>
<a href="{{ route('exim.sjSerial') }}" id="sjSerial"></a>
<a href="{{ route('exim.ajaxgroupinvoiceajaxplan') }}" id="ajaxShowGroupInvoiceAjaxPlan"></a>
<a href="{{ route('exim.ajaxgroupinvoiceajaxshipmode') }}" id="ajaxShowGroupInvoiceAjaxShipmode"></a>
<a href="{{ route('exim.ajaxgroupinvoiceajaxfwd') }}" id="ajaxShowGroupInvoiceAjaxFwd"></a>
<a href="{{ route('exim.ajaxgroupinvoiceajaxdestination') }}" id="ajaxShowGroupInvoiceAjaxDestination"></a>

<!-- by naufal -->
<a href="{{ route('exim.updateFwd') }}" id="updateFwd"></a>
<a href="{{ route('exim.updatePlanStuffing') }}" id="updatePlanStuffing"></a>
<a href="{{ route('exim.updateSo') }}" id="updateSo"></a>
<a href="{{ route('exim.updateShip') }}" id="updateShip"></a>
@endsection


@include('report/_js_index')
@section('js')
<script type="text/javascript">

//$(document).ready(function() {
    reloadajax();

    var url = $('#ajaxShowInvoice').attr('href');

    //datatables
        $.extend( $.fn.dataTable.defaults, {
            stateSave: true,
            autoWidth: false,
            autoLength: false,
            processing: true,
            serverSide: true,
            lengthMenu: [[-1], ["All"]],
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            }
        });

        var _token = $("input[name='_token']").val();
        var table = $('#table-list').DataTable({
            searching: true, 
            paging: false, 
           // info: false,
            scrollY:        "300px",
            scrollX:        true,
            scrollCollapse: true,
            fixedColumns:   {
                leftColumns: 3
            },
            sDom: 'tF',
            buttons: [
                {
                    text: 'Export to Excel',
                    className: 'btn btn-sm bg-success exportExcel',
                    action: function (e, dt, node, config)
                    {
                        var columns = table.settings().init().columns;
                        var current_order = table.order();

                        var column_name = columns[current_order[0][0]].name;
                        var direction = current_order[0][1];
                        var filter = table.search();
                        window.location.href = $('#export_invoice').attr('href')
                                                + '?orderby=' + column_name
                                                + '&direction=' + direction
                                                + '&filterby=' + filter
                                                + '&radio_status=' + $('input[name=radio_status]:checked').val()
                                                + '&fwd=' + $('#fwd').val()
                                                + '&shipmode=' + $('#shipmode').val();
                    }
                }
            ],
            ajax: {
                url: url,
                //type: 'post',
                data: function (d) {
                    return $.extend({},d,{
                        "filterby": $('.dataTables_filter input').val(),
                        "radio_status": $('input[name=radio_status]:checked').val(),
                        "fwd": $('#fwd').val(),
                        "shipmode": $('#shipmode').val(),
                        "_token": _token
                    });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = table.page.info();
                var value = index+1+info.start;
                // $('td', row).eq(0).html(value);
                $('td', row).eq(1).css('min-width', '100px');
                $('td', row).eq(2).css('min-width', '100px');
                $('td', row).eq(4).css('min-width', '150px');
                $('td', row).eq(5).css('min-width', '150px');
                $('td', row).eq(6).css('min-width', '170px');
                $('td', row).eq(7).css('min-width', '150px');
                $('td', row).eq(10).css('min-width', '178px');
                $('td', row).eq(11).css('min-width', '178px');
            },
            columnDefs: [
                {
                    className: 'dt-center'
                }
            ],
            columns: [
                // {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
                {data: 'plan_stuffing', name: 'plan_stuffing'},
                {data: 'invoice', name: 'invoice'},
                {data: 'remark_cont', name: 'remark_cont'},
                {data: 'so', name: 'so'},
                {data: 'shipmode', name: 'shipmode'},
                {data: 'fwd', name: 'fwd'},
                {data: 'destination', name: 'destination'},
                {data: 'customer_number', name: 'customer_number'},
                {data: 'cbm', name: 'cbm'},
                {data: 'orderno_concat', name: 'orderno_concat', orderable: false, searchable: false},
                {data: 'ref_concat', name: 'ref_concat', orderable: false, searchable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
        });

        table.columns().every( function () {
            var that = this;
            that.search('').draw();
        });

        $('#search_plan_stuffing').on('change', function(){
            var search = $(this).val();

            // Count total search result
            var len = $('#table-list tbody tr').length;

            if(len > 0){
           
                loading();
                table
                .columns( '.filter-plan' )
                .search( search )
                .draw();

                reloadajaxshipmode();
            }
        });

        $('#search_shipmode').on('change', function(){
            var search = $(this).val();

            // Count total search result
            var len = $('#table-list tbody tr').length;

            if(len > 0){
           
                loading();
                table
                .columns( '.filter-shipmode' )
                .search( search )
                .draw();

                reloadajaxfwd();
            }
        });

        $('#search_fwd').on('change', function(){
            var search = $(this).val();

            // Count total search result
            var len = $('#table-list tbody tr').length;

            if(len > 0){
           
                loading();
                table
                .columns( '.filter-fwd' )
                .search( search )
                .draw();

                reloadajaxdestination();
            }
        });

        $('#search_destination').on('change', function(){
            var search = $(this).val();

            // Count total search result
            var len = $('#table-list tbody tr').length;

            if(len > 0){
           
                loading();
                table
                .columns( '.filter-destination' )
                .search( search )
                .draw();
            }
        });

        table
        .on( 'preDraw', function () {
            Pace.start();
        } )
        .on( 'draw.dt', function () {
            Pace.stop();
            $('#listing-tab').unblock();
        } );
        //end of datatables

        $('#form_filter').submit(function(event) {
            event.preventDefault();
    
            loading();
    
            table.draw();
        })

        // Enable Select2 select for individual column searching
        $('.filter-select').select2();
    
         //choose filter
        $('input[type=radio][name=radio_status]').change(function() {
            if (this.value == 'all') {
                if($('#filter_by_all').hasClass('hidden')) {
                    $('#filter_by_all').removeClass('hidden');
                }
    
                $('#filter_by_fwd').addClass('hidden');
                $('#filter_by_shipmode').addClass('hidden');
            }
            else if (this.value == 'fw') {
                if($('#filter_by_fwd').hasClass('hidden')) {
                    $('#filter_by_fwd').removeClass('hidden');
                }
    
                $('#filter_by_all').addClass('hidden');
                $('#filter_by_shipmode').addClass('hidden');
            }
            else if(this.value == 'ship') {
                if($('#filter_by_shipmode').hasClass('hidden')) {
                    $('#filter_by_shipmode').removeClass('hidden');
                }
    
                $('#filter_by_all').addClass('hidden');
                $('#filter_by_fwd').addClass('hidden');
            }
            
            loading();
    
            table.draw();
        });


    function calculate() {

        var total = 0;

        var info = '*No Checked';

        var arr = [];
        var po_ = [];
        var arrs = [];

        $('.clplanref:checked').each(function(e, i) {
            arr.push($(this).data('cbm'));
            arrs.push({
                'fwd': $(this).data('fwd'),
                'shipmode': $(this).data('shipmode'),
                'customer': $(this).data('customer'),
                'totalbalance': $(this).data('totalbalance'),
                'planstuffing': $(this).data('planstuffing')
            });
        });

        //cek 
        $.each(arrs, function(e, i) {
            var checkboxes = document.getElementsByName('selector[]');

//            if (i['totalbalance'] !== 0){
//                myalert('error', 'Scanning stuffing required..!')
//               
//                for (var i = 0; i < checkboxes.length; i++) {
//                    if (checkboxes[i].type == 'checkbox') {
//                        checkboxes[i].checked = false;
//                    }
//                }
//            }

            if (e >= 0) {
               // $('#delivery_number').removeAttr('readonly');
                $('#nopol').removeAttr('readonly');
                $('#drivers_name').removeAttr('readonly');
                $('#drivers_phone').removeAttr('readonly');
                $('#warehouse').removeAttr('readonly');
               // $('#delivery_number').val('');
                $('#nopol').val('');
                $('#drivers_name').val('');
                $('#drivers_phone').val('');
                $('#warehouse').val('');

                if (e == 0) {
                    $('#warehouse').val(i['fwd']);
                    $('#fwd_hide').val(i['fwd']);
                    $('#shipmode_hide').val(i['shipmode']);
                    $('#customer_hide').val(i['customer']);
                    $('#plan_stuffing_hide').val(i['planstuffing']);
                }else{
                    if (i['fwd'] != $('#fwd_hide').val() || i['shipmode'] != $('#shipmode_hide').val() || i['planstuffing'] != $('#plan_stuffing_hide').val() ) {

                        myalert('error', 'Plan Stuffing/FWD/Shipmode must be same..!')
               
                        for (var i = 0; i < checkboxes.length; i++) {
                            if (checkboxes[i].type == 'checkbox') {
                                checkboxes[i].checked = false;
                            }
                        }
                    }
                }

            }else{
                $('#nopol').attr('readonly', true);
                $('#drivers_name').attr('readonly', true);
                $('#drivers_phone').attr('readonly', true);
                $('#warehouse').attr('readonly', true);
                $('#nopol').val('');
                $('#drivers_name').val('');
                $('#drivers_phone').val('');
                $('#warehouse').val('');
            }

        });

        for (var i=0; i<arr.length; i++){
            total += parseFloat(arr[i]);
        }


        $('.clplanref:checked').each(function(e, i) {

            po_.push($(this).val());
        });

        // info
        var vals = "";

        for (var j=0; j<po_.length; j++){
                vals +=", "+po_[j];
        }

        if (vals) vals = vals.substring(1);


        if (arr.length > 0) {
            info = '*'+arr.length+' Invoice Checked ( '+vals+' )';
        }

        $('.info-check').html(info);

        if(isNaN(total)){
            $('#cbm_detail_value').val(0);
        }else{
            $('#cbm_detail_value').val(total.toFixed(3));
        }

    }

    // calculate();

    $('div').delegate('input:checkbox', 'click', calculate);


    $('#nopol').on('input', function() {
        this.value = this.value.toUpperCase();
    });

    $('#btnUpdate').on('click', function(){
        var data = [];
        var delivery_number = $('#delivery_number').val();
        var nopol = $('#nopol').val();
        var drivers_name = $('#drivers_name').val();
        var drivers_phone = $('#drivers_phone').val();
        var warehouse = $('#warehouse').val();
    
        $('.clplanref:checked').each(function() {
            data.push({
                invoice: $(this).val()
            });
        });
    
        if (data.length > 0) {
            bootbox.confirm("Are you sure update trucking ?", function (result) {
                if (result) {

                    if(delivery_number == '' || drivers_name == '' || drivers_phone == '' || warehouse == ''){
                        myalert('error', 'delivery order/drivers name/phone/warehouse not found..!');
                        return false;
                    }

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'post',
                        url: $('#updateInvoice').attr('href'),
                        data: {data:data, delivery_number:delivery_number, nopol:nopol, drivers_name:drivers_name, drivers_phone:drivers_phone, warehouse:warehouse, action:'invoice'},
                        success: function(response){
                            myalert('success', 'Delivery order created succesfully..!')
                           // $('#delivery_number').val('');
                            $('#nopol').val('');
                            $('#drivers_name').val('');
                            $('#drivers_phone').val('');
                            $('#warehouse').val('');

                            $('.info-check').html('*No Checked');

                            $('#cbm_detail_value').val(0);
                            
                            loading();
                            table.draw();

                            sj_next();
                        },
                        error: function(response) {
                            if(response['status'] == 422) {
                                myalert('error', 'Oopss Something wrong..!');
                            }
                        }
                    });
                }
            });
        }
    });

    $('#table-list').on('blur', '.fwd_unfilled', function() {
        var invoice = $(this).data('id');
        // var po_number = $(this).data('ponumber');
        // var plan_ref_number = $(this).data('planrefnumber');
        var fwd = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateFwd').attr('href'),
            data: {fwd:fwd,invoice:invoice},
            success: function(response){
                $('#fwd_' + invoice).html(response);
            },
            error: function(response) {
                return false;
            }
        });

    });

    $('#table-list').on('blur', '.plan_unfilled', function() {
        var invoice = $(this).data('id');
       
        var planstuffing = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updatePlanStuffing').attr('href'),
            data: {planstuffing:planstuffing, invoice:invoice},
            success: function(response){
                $('#plan_' + invoice).html(response);
            },
            error: function(response) {
                return false;
            }
        });

    });

    $('#table-list').on('blur', '.so_unfilled', function() {
        var invoice = $(this).data('id');
       
        var so = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateSo').attr('href'),
            data: {so:so, invoice:invoice},
            success: function(response){
                $('#so_' + invoice).html(response);
            },
            error: function(response) {
                return false;
            }
        });

    });

    $('#table-list').on('blur', '.ship_unfilled', function() {
        var invoice = $(this).data('id');
       
        var ship = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateShip').attr('href'),
            data: {ship:ship, invoice:invoice},
            success: function(response){
                $('#ship_' + invoice).html(response);
            },
            error: function(response) {
                return false;
            }
        });

    });

    function sj_next(){
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: $('#sjSerial').attr('href'),
            success: function(response){
                $('#delivery_number').val(response);
            },
            error: function(response) {
                if(response['status'] == 422) {
                    myalert('error', 'Oopss Something wrong..!');
                }
            }
        });
    }
    
//});

function toggle(source) {
    checkboxes = document.getElementsByName('selector[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = source.checked;
    }
}

function cancelInvoice(e) {
    var invoice = $(e).data('invoice');

    bootbox.confirm("Are you sure cancel invoice "+invoice+" ?", function (result) {
        if (result) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: $('#cancelInvoice').attr('href'),
                method: 'post',
                data: {invoice:invoice},
                success: function(response){
                    myalert('success', 'Invoice cancel successfully');
                    loading();
    
                    table.draw();
                },
                error: function(response) {
                    if(response['status'] == 422) {
                        myalert('error', 'Oopss Something wrong..!');
                    }
                }
            });
        }
    });
}

//search plan stuffing
function reloadajax() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url: $('#ajaxShowGroupInvoiceAjaxPlan').attr('href'),
        data: {plan_stuffing:$('#search_plan_stuffing').val(), shipmode:$('#search_shipmode').val(), fwd:$('#search_fwd').val(), destination:$('#search_destination').val() },
        success: function(response){

            $('#search_plan_stuffing').empty();
            $('#search_plan_stuffing').append('<option value=""></option>');

            $.each(response.data_plan, function(i,v){
                $('#search_plan_stuffing').append('<option value="'+i+'">'+i+'</option>');
            })
        }
    });
}

//search shipmode
function reloadajaxshipmode() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url: $('#ajaxShowGroupInvoiceAjaxShipmode').attr('href'),
        data: {plan_stuffing:$('#search_plan_stuffing').val(), shipmode:$('#search_shipmode').val(), fwd:$('#search_fwd').val(), destination:$('#search_destination').val() },
        success: function(response){

            $('#search_shipmode').empty();
            $('#search_shipmode').append('<option value=""></option>');

            $.each(response.data_shipmode, function(i,v){
                $('#search_shipmode').append('<option value="'+i+'">'+i+'</option>');
            })
        }
    });
}

//search fwd
function reloadajaxfwd() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url: $('#ajaxShowGroupInvoiceAjaxFwd').attr('href'),
        data: {plan_stuffing:$('#search_plan_stuffing').val(), shipmode:$('#search_shipmode').val(), fwd:$('#search_fwd').val(), destination:$('#search_destination').val() },
        success: function(response){

            $('#search_fwd').empty();
            $('#search_fwd').append('<option value=""></option>');

            $.each(response.data_fwd, function(i,v){
                $('#search_fwd').append('<option value="'+i+'">'+i+'</option>');
            })
        }
    });
}

//search destination
function reloadajaxdestination() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url: $('#ajaxShowGroupInvoiceAjaxDestination').attr('href'),
        data: {plan_stuffing:$('#search_plan_stuffing').val(), shipmode:$('#search_shipmode').val(), fwd:$('#search_fwd').val(), destination:$('#search_destination').val() },
        success: function(response){

            $('#search_destination').empty();
            $('#search_destination').append('<option value=""></option>');

            $.each(response.data_destination, function(i,v){
                $('#search_destination').append('<option value="'+i+'">'+i+'</option>');
            })
        }
    });
}


</script>
@endsection
