@extends('layouts.app', ['active' => 'eximplanload'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('exim.planload') }}"><i class="icon-file-upload2 position-left"></i>UPLOAD PLAN LOAD</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="row">
            {{--  <legend class="text-semibold"><i class="icon-file-upload2 position-left"></i> Upload Plan Load</legend>  --}}
            <form class="form-horizontal" action="{{ route('exim.uploadexcelPlan') }}" id="form-upload" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            {{--  <legend class="text-semibold"><i class="icon-file-upload2 position-left"></i> Upload excel</legend>  --}}

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Customers:</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="customer" id="inputCustomer" required>
                                        <option value="">Choose Customer</option>
                                        @foreach($customers as $key => $val)
                                            <option value="{{ $val->id }}" data-blade="{{ $val->description_blade }}">{{ $val->customer_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                                {{--  <legend class="text-semibold"><i class="icon-file-upload2 position-left"></i> Upload excel</legend>  --}}

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">File Excel:</label>
                                    <div class="col-lg-9">
                                        <input type="file" class="file-styled" name="file_upload" id="file" required>
                                        <span class="help-block">Accepted formats: xls xlsx.</span>
                                    </div>
                                </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row hidden">
                    <div class="col-md-6">
                        <fieldset>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Factory:</label>
                                    <div class="col-lg-9">
                                        <select class="form-control" name="factory_id" id="factory_id">
                                            <option value="">Choose Factory</option>
                                            @foreach($factory as $key => $val)
                                                <option value="{{ $val->factory_id }}">{{ $val->factory_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div id="view_template_header">
                        <!-- template header pilihan -->
                        <section class="panel hidden" style="padding:10px;">
                        </section>
                        <button type="button" class="btn btn-default hidden pull-left" id="export_template">Download Template</button>
                    </div>
                </div><br>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary legitRipple"> Submit<i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
        <hr>

    </div>

</div>
<a href="{{ route('exim.ajaxplanload') }}" id="ajaxShowPlanLoad"></a>
<a href="{{ route('exim.exportplanload') }}" id="export_plan"></a>
<a href="{{ route('exim.updatePlanLoad') }}" id="updatePlanLoad"></a>
<a href="{{ route('exim.ajaxViewTemplateHeader') }}" id="url_view_template_header"></a>
<a href="{{ route('exim.ajaxViewTemplateHeaderExport') }}" id="url_view_template_header_export"></a>
<a href="{{ route('exim.updateShipmode') }}" id="updateShipmode"></a>
<a href="{{ route('exim.updateMdd') }}" id="updateMdd"></a>
<a href="{{ route('exim.updateRemark') }}" id="updateRemark"></a>
<a href="{{ route('exim.updatePsfdTo') }}" id="updatePsfdTo"></a>
@endsection


@include('report/_js_index')
@section('js')
<script type="text/javascript">

{{--  $(document).ready(function() {  --}}

    var url = $('#ajaxShowPlanLoad').attr('href');
    var url_view_header = $('#url_view_template_header').attr('href');
    var url_view_header_export = $('#url_view_template_header_export').attr('href');


    $('#form-upload').submit(function(event) {
        event.preventDefault();

        if ($('#file').val() != '') {

            var formData = new FormData($(this)[0]);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
                }
            });
            $.ajax({
                url: $('#form-upload').attr('action'),
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('#form-upload').block({
                        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Uploading File</span>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: '10px 15px',
                            color: '#fff',
                            width: 'auto',
                            '-webkit-border-radius': 2,
                            '-moz-border-radius': 2,
                            backgroundColor: '#333'
                        }
                    });
                },
                success: function(response) {
                    $('#form-upload').unblock();
                    myalert('success', 'Upload success...');
                    $('#file').val('');
                    $('#inputCustomer').val('').trigger('change');
                    $('#factory_id').val('');
                    table.draw();
                },
                error: function(response) {
                    $('#form-upload').unblock();
                    if(response['status'] == 422) {
                        myalert('error', response['responseJSON']);
                    }
                }
            });


        }else{
            myalert('error','file upload not found');
        }

    });

    //customer change
    $('#inputCustomer').on('change', function(){
        var cst = $(this).val();
        var cst_blade = $('#inputCustomer option:selected').data('blade');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: url_view_header,
            data: {template: cst, template_blade: cst_blade},
            success: function(response) {
                $('#view_template_header > .panel').removeClass('hidden');
                $('#view_template_header > .panel').html('');
                $('#view_template_header > .panel').append(response);
                $('#view_template_header > #export_template').removeClass('hidden');

            },
            error: function(response) {
                $('#view_template_header > .panel').addClass('hidden');
                $('#view_template_header > .panel').html('');
                $('#view_template_header > #export_template').addClass('hidden');
                if(response['status'] == 422) {
                    myalert('error',response['responseJSON']);
                }
            }
        });

    });

    $('#export_template').on('click', function(){
        var cst = $('#inputCustomer option:selected').val();
        var cst_blade = $('#inputCustomers option:selected').data('blade');

        if(cst == ''){
            myalert('error', 'Customer not found');
            return false;
        }

        window.location.href = url_view_header_export
                                            + '?template=' + cst;

    });

</script>
@endsection
