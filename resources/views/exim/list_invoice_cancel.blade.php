<div class="table-responsive">
<table class="table" id="table-invoice">
    <thead>
        <tr>
            <th>#</th>
            <th>Invoice</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $key => $val)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $val->invoice }}</td>
                <td>
                    <button type="button" id="choose-invoice"
                            value="{{ $val->invoice }}"
                            class="btn btn-default">
                            SELECT
                    </button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
</div>