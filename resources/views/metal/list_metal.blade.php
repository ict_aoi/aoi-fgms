@extends('layouts.app', ['active' => 'listscanmetal'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">Metal Detector</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#"><i class="icon-home4 position-left"></i>List Scan Metal</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Metal Detector Date</th>
                        <th>Barcode ID</th>
                        <th>PO Number</th>
                        <th>Style</th>
                        <th>Product Type</th>
                        <th>Article</th>
                        <th>Line</th>
                        <th>Size</th>
                        <th>Carton No.</th>
                        <th>PSO</th>
                        <th>Qty</th>
                        <th>Stuffing Plan</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            url: "{{ route('metalfind.ajaxListScanMetal') }}",
            type: 'get'
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
            {data: 'barcode_id', name: 'barcode_id'},
            {data: 'po_number', name: 'po_number'},
            {data: 'upc', name: 'upc'},
            {data: 'product_category_detail', name: 'product_category_detail'},
            {data: 'buyer_item', name: 'buyer_item'},
            {data: 'line_name', name: 'line_name'},
            {data: 'customer_size', name: 'customer_size'},
            {data: 'package_number', name: 'package_number'},
            {data: 'pso', name: 'pso'},
            {data: 'inner_pack', name: 'inner_pack'},
            {data: 'plan_stuffing', name: 'plan_stuffing'},
            {data: 'status', name: 'status'}
        ],
    });
});
</script>
@endsection
