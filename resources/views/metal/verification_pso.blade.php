@extends('layouts.app', ['active' => 'verifpso'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">Verification Metal</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#"><i class="icon-home4 position-left"></i>Verification</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Metal Detector Date</th>
                        <th>Barcode ID</th>
                        <th>PO Number</th>
                        <th>Style</th>
                        <th>Product Type</th>
                        <th>Article</th>
                        <th>Line</th>
                        <th>Size</th>
                        <th>Carton No.</th>
                        <th>PSO</th>
                        <th>Qty</th>
                        <th>Stuffing Plan</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection


@section('modal')
<div id="modal_verif" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 100%;">
        <div class="modal-content">
            <div class="modal-body">
                <div class="panel-body">
                    <!-- <form action="{{ route('metalpso.changeStatus') }}" id="form-save">
                        @csrf -->
                        <div class="row">
                            <center><h3>STATUS METAL DETECTOR</h3></center>
                            <input type="text" name="barcode_id" id="barcode_id" class="hidden" >
                        </div>
                        <div class="row">
                            
                                <div class="col-lg-4">
                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <center><label style="font-weight: bold;">PO Data</label></center>
                                        <hr>
                                    </div>
                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <div class="col-md-6">
                                            <label style="font-weight: bold;">Metal Detector Date</label>
                                        </div>
                                        <div class="col-md-6">
                                            <label id="md_created"></label>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <div class="col-md-6">
                                            <label style="font-weight: bold;">PO Number</label>
                                        </div>
                                        <div class="col-md-6">
                                            <label id="md_po_number"></label>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <div class="col-md-6">
                                            <label style="font-weight: bold;">Style</label>
                                        </div>
                                        <div class="col-md-6">
                                            <label id="md_upc"></label>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <div class="col-md-6">
                                            <label style="font-weight: bold;">Product Type</label>
                                        </div>
                                        <div class="col-md-6">
                                            <label id="md_product_ctg"></label>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <div class="col-md-6">
                                            <label style="font-weight: bold;">Article</label>
                                        </div>
                                        <div class="col-md-6">
                                            <label id="md_buyer_item"></label>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <div class="col-md-6">
                                            <label style="font-weight: bold;">Line</label>
                                        </div>
                                        <div class="col-md-6">
                                            <label id="md_line"></label>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <div class="col-md-6">
                                            <label style="font-weight: bold;">Customer Size</label>
                                        </div>
                                        <div class="col-md-6">
                                            <label id="md_customer_size"></label>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <div class="col-md-6">
                                            <label style="font-weight: bold;">Manufacturing Size</label>
                                        </div>
                                        <div class="col-md-6">
                                            <label id="md_manufacturing_size"></label>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <div class="col-md-6">
                                            <label style="font-weight: bold;">Carton Number</label>
                                        </div>
                                        <div class="col-md-6">
                                            <label id="md_package_number"></label>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <div class="col-md-6">
                                            <label style="font-weight: bold;">PSO</label>
                                        </div>
                                        <div class="col-md-6">
                                            <label id="md_pso"></label>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <div class="col-md-6">
                                            <label style="font-weight: bold;">Qty</label>
                                        </div>
                                        <div class="col-md-6">
                                            <label id="md_qty"></label>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <div class="col-md-6">
                                            <label style="font-weight: bold;">Stuffing Plan</label>
                                        </div>
                                        <div class="col-md-6">
                                            <label id="md_plan_stuffing"></label>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-lg-4">
                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <center><label style="font-weight: bold;">Metal Detector Change Status</label></center>
                                        <hr>
                                    </div>
                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <label><b>Metal Status</b></label>
                                        <select class="form-control" id="status">
                                            <option value="">--choose status--</option>
                                            <option value="metal found">Metal Found</option>
                                            <option value="pass">Pass</option>
                                        </select>
                                    </div>
                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <label><b>UCC No.</b></label>
                                        <input type="text" name="ucc_no" id="ucc_no" class="form-control" required placeholder="UCC NO">
                                    </div>
                                    <br>

                                    <br>
                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <center><label style="font-weight: bold;">Metal Finding</label></center>
                                        <hr>
                                    </div>
                                    <div class="row form-group" id="metfind" style="padding-left: 10px; padding-right: 10px;"> 
                                        <label class="radio-inline"><input type="radio" name="radio_status"  checked="checked" value="Broken Needle">Broken Needle</label>
                                        <label class="radio-inline"><input type="radio" name="radio_status" value="Metal Flakes">Metal Flakes</label>
                                        <label class="radio-inline"><input type="radio" name="radio_status" value="Others">Others</label>
                                    </div>
                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <div class="col-lg-6">
                                            <label><b>NOTE</b></label>
                                            <input type="text" name="notes" id="notes" class="form-control" placeholder="notes">
                                        </div>
                                        <div class="col-lg-6">
                                            <label><b>Qty</b></label>
                                            <input type="number" min="1" name="qty" id="qty" value="1" class="form-control" required>
                                        </div>
                                        
                                    </div>
                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <label><b>Location</b></label>
                                        <input type="text" name="loct" id="loct" class="form-control" placeholder="location metal finding">
                                    </div>
                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <br>
                                        <button class="btn btn-primary" id="btn-add"><span class="icon-plus3"></span> Add</button>
                                    </div>
                                    
                                </div>

                                <div class="col-lg-4">
                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <center><label style="font-weight: bold;">List findings</label></center>
                                        <hr>
                                    </div>
                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                        <table class="table" id="table-met">
                                            <thead>
                                                <th>#</th>
                                                <th>Case</th>
                                                <th>Note</th>
                                                <th>Location</th>
                                                <th>Qty</th>
                                            </thead>
                                            <tbody class="list-met"></tbody>
                                        </table>
                                    </div>
                                </div>
                              
                            
                        </div>
                        <div class="row">
                            <button class="btn btn-success" type="button" id="btn-save">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('js')
<script type="text/javascript">
$(document).ready(function() {
    var arr = [];

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            url: "{{ route('metalpso.ajaxListVerify') }}",
            type: 'get'
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
            {data: 'barcode_id', name: 'barcode_id'},
            {data: 'po_number', name: 'po_number'},
            {data: 'upc', name: 'upc'},
            {data: 'product_category_detail', name: 'product_category_detail'},
            {data: 'buyer_item', name: 'buyer_item'},
            {data: 'line_name', name: 'line_name'},
            {data: 'customer_size', name: 'customer_size'},
            {data: 'package_number', name: 'package_number'},
            {data: 'pso', name: 'pso'},
            {data: 'inner_pack', name: 'inner_pack'},
            {data: 'plan_stuffing', name: 'plan_stuffing'},
            {data: 'status', name: 'status'}
        ],
    });



    $('#table-list').on('click','.btn-pop',function(event){
        event.preventDefault();
        var barcode = $(this).data('id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url: "{{ route('metalpso.getMetFind') }}",
            data: {barcode_id:barcode},
            success: function(response){
                var data = response.data;
                $('#barcode_id').val(data.barcode_id);
                $('#md_created').text(data.created_at);
                $('#md_po_number').text(data.po_number);
                $('#md_upc').text(data.upc);
                $('#md_product_ctg').text(data.product_category_detail);
                $('#md_buyer_item').text(data.buyer_item);
                $('#md_line').text(data.line_name);
                $('#md_customer_size').text(data.customer_size);
                $('#md_manufacturing_size').text(data.manufacturing_size);
                $('#md_package_number').text(data.package_number);
                $('#md_pso').text(data.pso);
                $('#md_qty').text(data.inner_pack);
                $('#md_plan_stuffing').text(data.plan_stuffing);
                if (data.status=='checkin') {
                    $('#status').val('');
                    $('#table-met > .list-met').empty();
                }else if (data.status=='metal found'){
                    $('#status').val(data.status);
                    $('#ucc_no').val(data.uccno);
                    document.getElementById('ucc_no').disabled =true;
                    document.getElementById('btn-add').disabled =true;
                    var dtl = response.detail;
                    createTbal(dtl);
                    // console.log(dtl);
                }else{
                    $('#status').val(data.status);
                    $('#ucc_no').val(data.uccno);
                    document.getElementById('ucc_no').disabled =true;
                    document.getElementById('btn-add').disabled =true;
                    document.getElementById('btn-save').disabled =true;
                    var dtl = response.detail;
                    createTbal(dtl);
                }
               
                // $("input[name=radio_status][value='"+data.remark+"']").prop('checked', true);
                // $('#notes').val(data.note);
                // $('#loct').val(data.loct);

                // console.log(data.loct_defect);

                
            },
            error: function(response) {
                return false;
               
            }
        });
        $('#modal_verif').modal('show');
    });

    // $('input[name=radio_status]').change(function(event){
    //     if ($('input[name=radio_status]:checked').val()=='Others') {
    //         $('#notes').removeClass('hidden');
    //     }else{
    //         $('#notes').addClass('hidden');
    //     }
    // });


    $('#btn-add').on('click',function(event){
        event.preventDefault();

        var metfind = $('input[name=radio_status]:checked').val();
        var note = $('#notes').val();
        var qty = $('#qty').val();
        var loct = $('#loct').val();

        if (metfind=="Others" && note=="") {
            myalert('error','Note Required ! ! !');

            return false;
        }

        arr.push({
                    'metfind':metfind,
                    'note':note,
                    'qty':qty,
                    'loct':loct
                });

        createTbal(arr);

        $('#notes').val('');
        $('#qty').val(1);
        $('#loct').val('');
    });

    $('#btn-save').click(function(event){
     

        // console.log($('#status').val(),$('input[name=radio_status]:checked').val(),$('#barcode_id').val(),$('#notes').val(),$('#loct').val());

        if($('#status').val()==''){
            myalert('error','Status not set ! ! !');

            return false
        }
   
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{ route('metalpso.changeStatus') }}",
            data:{
                "cekstatus": $('#status').val(),  
                "barcode_id": $('#barcode_id').val(), 
                "no_ucc":$('#no_ucc').val(),   
                "data":arr,                   
                "_token": _token
            },
            beforeSend:function(){
                $('#modal_verif .modal-body').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Uploading File</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response){
                var data_response = response.data;

                if (data_response.status==200) {
                    
                    myalert('success',data_response.output);
                }else{
                    myalert('error',data_response.output);
                }
                $('#modal_verif .modal-body').unblock();
                $('#modal_verif').modal('hide');
                table.clear();
                table.draw();
            },
            error: function(response) {
                // console.log(response['responseJSON']['message']);
                myalert('error',response['responseJSON']['message']);
                $('#modal_verif .modal-body').unblock();
                return false;
               
            }
        });
    });
});

function createTbal(arrs){
    $('#table-met > .list-met').empty();

    for (var i = 0; i < arrs.length; i++) {
        var x = i+1;
        $('#table-met > .list-met').append('<tr> <td>'+x+'</td> <td>'+arrs[i]['metfind']+'</td> <td>'+arrs[i]['note']+'</td> <td>'+arrs[i]['loct']+'</td> <td>'+arrs[i]['qty']+'</td> </tr>');
    }

    // console.log(arrs);
}
</script>
@endsection
