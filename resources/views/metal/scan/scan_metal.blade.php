@extends('layouts.app', ['active' => 'scanmetal'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">Metal Detector</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#"><i class="icon-home4 position-left"></i>Scan Metal</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <form action="{{ route('metalfind.ajaxScanMetal') }}" id="form-checkin">
            @csrf
            <div class="row">
                <div class="col-md-10">
                    <input type="text" class="form-control input-new" id="barcode" name="barcode" placeholder="#Scan ID"></input>
                </div>
                <div class="col-md-2">
                    <input type="text" id="count_scan" name="count_scan" value="0" style="text-align: center; font-size: 28px;"
                           class="form-control" readonly="readonly"
                           placeholder="">
                </div>
            </div>
        </form>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="show-result">
                <thead>
                    <tr>
                        <th>BARCODE ID</th>
                        <th>PACKAGE NO.</th>
                        <th>PO NUMBER</th>
                        <th>STYLE</th>
                        <th>ARTICLE</th>
                        <th>CUSTOMER SIZE</th>
                        <th>MANUFACTURING SIZE</th>
                        <th>PLAN STUFFING</th>
                        <th>LINE</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#qcinspect-checkin').focus();

    

    //scan package
    $('.input-new').keypress(function(event) {
        if(event.which == 13) {
            event.preventDefault();
            var barcodeid = $('#barcode').val();

            
            if($('#barcode').val() == '') {
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: $('#form-checkin').attr('action'),
                data: {barcodeid: barcodeid},
                beforeSend: function() {
                    $('#barcode').val('');
                    $('#barcode').focus();
                },
                success: function(response){
                    $('#show-result > tbody').prepend(response);
                    var count_s = +$('#count_scan').val()+1;

                    $('#count_scan').val(count_s);
                },
                error: function(response){
                    console.log(response);
                    // if(response['status'] == 422) {
                    //     myalert('error', response['responseJSON']);
                    // }
                    $('#barcode').val('');
                }
            });
        }
    });
});
</script>
@endsection
