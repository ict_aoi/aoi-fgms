@extends('layouts.app')

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">INPUT PACKAGE</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('packing.inputPackingPlan') }}"><i class="icon-stack2 position-left"></i> Input Package</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<form id="form-package" class="form-horizontal" action="{{ route('packing.ajaxInputPackage') }}">
    <div class="panel panel-flat">

        <div class="panel-body">

            <!-- PACKAGE FIELD -->
            <div class="row">
                <div class="col-md-12">
                    <legend class="text-semibold"><i class="icon-file-eye position-left"></i> Package Detail</legend>
                </div>
                <div class="col-md-12">
                    <fieldset>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">PO Number:</label>
                            <div class="col-lg-10">
                                <select data-placeholder="select status" name="po_number" class="select form-control" required>
                                    <option value="" selected disabled>---</option>
                                    @foreach($list_po as $key => $value)
                                        <option value="{{ $value->po_number }}">{{ $value->po_number }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Scan ID:</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="scan_id" placeholder="input scan id here" required>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Range:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="range" placeholder="input range here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">From:</label>
                            <div class="col-lg-3">
                                <input type="number" class="form-control" name="from" placeholder="input from here">
                            </div>
                            <label class="col-lg-3 control-label">To:</label>
                            <div class="col-lg-3">
                                <input type="number" class="form-control" name="to" placeholder="input to here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Serial From:</label>
                            <div class="col-lg-3">
                                <input type="number" class="form-control" name="serial_from" placeholder="input serial from here">
                            </div>
                            <label class="col-lg-3 control-label">Serial To:</label>
                            <div class="col-lg-3">
                                <input type="number" class="form-control" name="serial_to" placeholder="input serial to here">
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-6">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Pack Instruction Code:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="pack_instruction_code" placeholder="input pack instruction code here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Line #:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="line" placeholder="input line here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">SKU #:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="sku" placeholder="input sku here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Buyer Item:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="buyer_item" placeholder="input buyer item here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Model Number:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="model_number" placeholder="input model number here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Customer Size:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="customer_size" placeholder="input customer size here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Customer Number:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="customer_number" placeholder="input customer number here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Manufacturing Size:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="manufacturing_size" placeholder="input manufacturing size here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Technical Print Index:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="technical_print_index" placeholder="input technical print index here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">GPS Three Digit Size:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="gps_three_digit_size" placeholder="input gps three digit size here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">VAS/S HAS G01 - Service Identifier:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="service_identifier" placeholder="input VAS/S HAS G01 - service identifier here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">VAS/S HAS L15 - Packing Mode:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="packing_mode" placeholder="input packing mode here">
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Item Qty:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="item_qty" placeholder="input item qty here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Qty per Pkg/Inner Pack:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="inner_pack" placeholder="input qty per pkg/inner pack here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Inner Pkg Count:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="inner_pkg_count" placeholder="input inner pkg count here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Pkg Count:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="pkg_count" placeholder="input pkg count here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">R:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="r_" placeholder="input R here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Pkg Code:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="pkg_code" placeholder="input pkg code here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Net Net:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="net_net" placeholder="input net net here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Net:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="net" placeholder="input net here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Gross:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="gross" placeholder="input gross here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Unit 1:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="unit_1" placeholder="input unit 1 here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Length:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="length" placeholder="input length here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Width:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="width" placeholder="input width here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Height:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="height" placeholder="input height here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Unit 2:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="unit_2" placeholder="input unit 2 here">
                            </div>
                        </div>
                    </fieldset>
                </div>

            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#form-package').submit( function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#form-package').attr('action'),
            data: $('#form-package').serialize(),
            success: function(response) {
                alert('GOOD');
                $('#form-package').trigger("reset");
            },
            error: function(response) {
                alert('NOT GOOD');
            }
        });
    });
});
</script>
@endsection
