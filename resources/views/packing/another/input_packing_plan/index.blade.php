@extends('layouts.app')

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">INPUT PACKING PLAN</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('packing.inputPackingPlan') }}"><i class="icon-file-plus position-left"></i> Input Packing Plan</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<form id="form-packing-plan" class="form-horizontal" action="{{ route('packing.ajaxInputPackingPlan') }}">
    @csrf
    <div class="panel panel-flat">

        <div class="panel-body">

            <!-- IDENTIFICATION FIELD -->
            <div class="row">
                <div class="col-md-12">
                    <legend class="text-semibold"><i class="icon-file-eye position-left"></i> Identification</legend>
                </div>
                <div class="col-md-12">
                    <fieldset>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Plan Ref Number:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="plan_ref_number" placeholder="input plan ref number here" required >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">PO Number:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="po_number" placeholder="input purchase order number here" required >
                            </div>
                        </div>

                    </fieldset>
                </div>

            </div>
            <hr>

            <div class="row">

                <!-- BUYER FIELD -->
                <div class="col-md-6">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-reading position-left"></i> Buyer</legend>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Buyer Name:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="buyer_name" placeholder="Input buyer name here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Buyer Address:</label>
                            <div class="col-lg-9">
                                <textarea rows="5" cols="5" class="form-control" name="buyer_address" placeholder="Input buyer address here"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Buyer City:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="buyer_city" placeholder="Input buyer city here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Buyer Postal Code:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="buyer_postalcode" placeholder="Input buyer postal code here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Buyer Country:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="buyer_country" placeholder="Input buyer country here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Buyer Phone:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="buyer_phone" placeholder="Input buyer phone here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Buyer Fax:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="buyer_fax" placeholder="Input buyer fax here">
                            </div>
                        </div>

                    </fieldset>
                </div>
                <!-- END OF BUYER FIELD -->

                <!-- DESTINATION FIELD -->
                <div class="col-md-6">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-truck position-left"></i> DESTINATION</legend>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Destination Name:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="destination_name" placeholder="Input destination name here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Destination Address:</label>
                            <div class="col-lg-9">
                                <textarea rows="5" cols="5" class="form-control" name="destination_address" placeholder="Input destination address here"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Destination City:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="destination_city" placeholder="Input destination city here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Destination Postal Code:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="destination_postalcode" placeholder="Input destination postal code here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Destination Country:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="destination_country" placeholder="Input destination country here">
                            </div>
                        </div>
                    </fieldset>
                </div>
                <!-- END OF DESTINATION FIELD -->

            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#form-packing-plan').submit( function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#form-packing-plan').attr('action'),
            data: $('#form-packing-plan').serialize(),
            success: function(response) {
                alert('GOOD');
                $('#form-packing-plan').trigger("reset");
            },
            error: function(response) {
                alert('NOT GOOD');
            }
        });
    });
});
</script>
@endsection
