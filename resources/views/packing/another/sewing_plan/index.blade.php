@extends('layouts.app')

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">SEWING PLAN</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('packing.sewingPlan') }}"><i class="icon-list3 position-left"></i> Sewing Plan</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="form-group" style="float:right">
            <button type="button" class="btn btn-xs btn-success upload">UPLOAD</button>
            <button type="button" class="btn btn-xs btn-primary add_plan">ADD PLAN</button>
        </div>
    </div>
    <div class="panel-body">
        <table class="table" id="table-sewing-plan">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>PO Number</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Qty</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<a href="{{ route('packing.ajaxGetSewingPlan')}}" id="link-sewing-plan"></a>
@endsection

@section('modal')
<!-- MODAL UPLOAD -->
<div id="modal_upload_" class="modal fade" role="dialog">
    <form class="form-horizontal" method="post" action="{{ route('packing.uploadPo') }}" id="form-upload">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content modal-lg">
                <div class="modal-body">
                    <div class="panel-body">
                        <fieldset>
                            <legend class="text-semibold">
                                <h4 id="title_upload"><i class="icon-upload4 position-left"></i>UPLOAD SEWING PLAN</h4>
                            </legend>

                            <!-- upload excel -->
                            <div class="form-group">
                                <label class="col-lg-3 control-label">File Excel:</label>
                                <div class="col-lg-9">
                                    <input type="file" class="file-styled" name="file_upload">
                                    <span class="help-block">Accepted formats: xls xlsx. Max file size 2Mb</span>
                                </div>
                            </div>

                            <div id="verify_data">

                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success hidden">Upload</button>
                    <button type="button" id="verify" class="btn btn-success">Verify<i class="icon-checkmark4 position-right"></i></button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /MODAL UPLOAD-->
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    //datatables
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var url = $('#link-sewing-plan').attr('href');

    var table = $('#table-sewing-plan').DataTable({
        ajax: url,
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false  },
            {data: 'po_number', name: 'po_number'},
            {data: 'start', name: 'start'},
            {data: 'end', name: 'end'},
            {data: 'qty', name: 'qty'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
    });
    //end of datatables

    $('.upload').click(function(){
        $('#modal_upload_').modal('show');
    });
});
</script>
@endsection
