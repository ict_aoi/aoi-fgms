@extends('layouts.app', ['active' => 'packagedetail'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">List Package Detail</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('packing.historyRevisi') }}"><i class="icon-location4 position-left"></i> List Package Detail</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('packing.ajaxGetPackageDetail') }}" id="form_filter">
            @csrf
            <div class="form-group">
				<label><b>PO Number</b></label>
				<div class="input-group">
                    <input type="text" class="form-control" name="po_number" id="po_number" required>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
			</div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>RANGE</th>
                        <th>FROM</th>
                        <th>TO</th>
                        <th>PO NUMBER</th>
                        <th>CUSTOMER SIZE</th>
                        <th>ITEM QTY</th>
                        <th>SCAN ID</th>
                        <th data-field="state" data-checkbox="true" class="text-center"><input type="checkbox" name="selectorAll" id="selectorAll" onclick="return sel(this);"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@include('packing/package_detail/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    var url = $('#form_filter').attr('action');

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            url: url,
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "po_number": $('#po_number').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'package_range', name: 'package_range'},
            {data: 'package_from', name: 'package_from'},
            {data: 'package_to', name: 'package_to', sortable: false, orderable: false, searchable: false},
            {data: 'po_number', name: 'po_number'},
            {data: 'customer_size', name: 'customer_size'},
            {data: 'item_qty', name: 'item_qty'},
            {data: 'scan_id', name: 'scan_id'}
        ],
    });
    //end of datatables

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        //check location
        if($('#po_number').val() == '') {
            alert('Please input po number first');
            return false;
        }

        table.draw();
    })
});
</script>
@endsection
