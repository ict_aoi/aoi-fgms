@extends('layouts.app', ['active' => 'packagerasio'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <br>
    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('packing.packagerasio') }}"><i class="icon-location4 position-left"></i> List PO Rasio</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row {{ Auth::user()->admin_role !== 1 ? 'hidden' : '' }} hidden">
            <div class="col-md-2"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->factory_id }}" {{ $val->factory_id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>PO NUMBER</th>
                        <th>PLAN REF</th>
                        <th>CUST</th>
                        <th>CUSTOMER SIZE</th>
                        <th>MANUFACTURING SIZE</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('packing.ajaxGetPackageRasio') }}" id="ajaxGetPackageRasio"></a>
<a href="{{ route('packing.updateManufacturingSize') }}" id="updateManufacturingSize"></a>
@endsection

@section('js')
<script type="text/javascript">
// $(document).ready(function() {
    var url = $('#ajaxGetPackageRasio').attr('href');

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            url: url,
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "factory_id": $('#factory_id').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(5).css('min-width', '250px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'po_number', name: 'po_number'},
            {data: 'plan_ref_number', name: 'plan_ref_number'},
            {data: 'customer_number', name: 'customer_number'},
            {data: 'customer_size', name: 'customer_size'},
            {data: 'manufacturing_size', name: 'manufacturing_size', orderable: false, searchable: false}
        ],
    });
    //end of datatables

    table.on('preDraw', function() {
        Pace.start();
        loading_process();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    $('#factory_id').on('change', function() {
        loading_process();

        table.draw();

    });

    // update manufacturing size
    $('#table-list').on('blur', '.ms_unfilled', function() {
        var scanid = $(this).data('scanid');
        var po_number = $(this).data('ponumber');
        var plan_ref_number = $(this).data('planrefnumber');
        var manufacturing_size = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateManufacturingSize').attr('href'),
            data: {scan_id:scanid, manufacturing_size:manufacturing_size, po_number:po_number, plan_ref_number:plan_ref_number},
            success: function(response){
                $('#ms_' + scanid).html(response);
            },
            error: function(response) {
                return false;
            }
        });
    });

// });

</script>
@endsection
