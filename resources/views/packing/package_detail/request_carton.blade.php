@extends('layouts.app', ['active' => 'requestcarton'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <!-- <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">List Request Carton</span></h4>
            <p class="position-left"></p>
        </div>
    </div> -->
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('packing.requestcarton') }}"><i class="icon-list-ordered position-left"></i> Dashboard Request Carton</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<div class="row">
    <div class="col-sm-6 col-md-3">
        <div class="panel panel-body">
            <div class="media no-margin">
                <div class="media-left media-middle">
                    <i class="icon-list icon-3x text-success-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="no-margin text-semibold" id="total_waiting">{{ isset($count_waiting) ? $count_waiting : 0 }}</h3>
                    <span class="text-uppercase text-size-mini text-muted">Total Waiting</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="panel panel-body">
            <div class="media no-margin">
                <div class="media-left media-middle">
                    <i class="icon-pause icon-3x text-danger-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="no-margin text-semibold" id="total_hold">{{ isset($count_hold) ? $count_hold : 0 }}</h3>
                    <span class="text-uppercase text-size-mini text-muted">Total Hold</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="panel panel-body">
            <div class="media no-margin">
                <div class="media-left media-middle">
                    <i class="icon-upload icon-3x text-blue-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="no-margin text-semibold" id="total_onprogress">{{ isset($count_onprogress) ? $count_onprogress : 0 }}</h3>
                    <span class="text-uppercase text-size-mini text-muted">Total On Progress</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="panel panel-body">
            <div class="media no-margin">
                <div class="media-left media-middle">
                    <i class="icon-inbox icon-3x text-green-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="no-margin text-semibold" id="total_done">{{ isset($count_done) ? $count_done : 0 }}</h3>
                    <span class="text-uppercase text-size-mini text-muted">Total Done</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="all">All</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="waiting">Waiting</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="hold">hold</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="onprogress">onprogress</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="done">done</label>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <!-- <th>#</th> -->
                        <th>DATE</th>
                        <th>LINE</th>
                        <th>PO NUMBER</th>
                        <th>ART. NO.</th>
                        <th>SIZE</th>
                        <th>CTN</th>
                        <th class="text-center">PACKING</th>
                        <th class="text-center">SEWING</th>
                        <th>INFO</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('ajaxGetDataCarton') }}" id="po_count_status"></a>
<a href="{{ route('packing.ajaxGetDataRequestCarton') }}" id="carton_get_data"></a>

@endsection

@section('js')
<script type="text/javascript">
// $(document).ready(function() {
    var url_carton = $('#carton_get_data').attr('href');
    var url_dashboard = $('#po_count_status').attr('href');

    // $.ajax({ //update total status
    //         url: url_dashboard,
    //         data: {},
    //         success: function(response){

    //             $('.cl_request').text(response['total_request_change']);
    //             $('#total_waiting').text(response['count_waiting']);
    //             $('#total_hold').text(response['count_hold']);
    //             $('#total_onprogress').text(response['count_onprogress']);
    //             $('#total_done').text(response['count_done']);
    //         }
    //     }),

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            url: url_carton,
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "radio_status": $('input[name=radio_status]:checked').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            // $('td', row).eq(0).html(value);
            $('td', row).eq(8).css('min-width', '110px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            // {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'request_at', name: 'request_at'},
            {data: 'description', name: 'description'},
            {data: 'po_number_concat', name: 'po_number_concat'},
            {data: 'buyer_item_concat', name: 'buyer_item_concat'},
            {data: 'manufacturing_size_concat', name: 'manufacturing_size_concat'},
            {data: 'carton_qty_concat', name: 'carton_qty_concat'},
            {data: 'status_packing', name: 'status_packing', sortable: false, orderable: false, searchable: false},
            {data: 'status_sewing', name: 'status_sewing', sortable: false, orderable: false, searchable: false},
            {data: 'info', name: 'info'},
            {data: 'btn_reject', name: 'button', orderable: false, searchable: false},
        ],
    });
    //end of datatables

    table.on('preDraw', function() {
        Pace.start();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    $('input[type=radio][name=radio_status]').change(function(event) {
        table.draw();
    });

    //reload data every x second
    setInterval( function () {
        //reload data count
        // var frequency = $('#frequency').val();

        $.ajax({ //update total status
            url: url_dashboard,
            data: {},
            success: function(response){

                $('.cl_request').text(response['total_request_change']).trigger('change');
                $('#total_waiting').text(response['count_waiting']);
                $('#total_hold').text(response['count_hold']);
                $('#total_onprogress').text(response['count_onprogress']);
                $('#total_done').text(response['count_done']);
            }
        }),

         //reload data table
        table.ajax.reload( null, false );
    }, 30000 );

// });

// proses reject
function reject_carton(e) {
    var _token = $("input[name='_token']").val();
    var id =  e.getAttribute('data-id');
    var po_number =  e.getAttribute('data-po');
    var customer_size =  e.getAttribute('data-customersize');

        bootbox.prompt("Please enter your information ( "+po_number+" - Size: "+customer_size+") :", function(result){
            if (result && $.trim($('.bootbox-input-text').val()) !== '' ) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: 'request-carton/reject',
                    type: "GET",
                    data: {
                        "po_number": po_number,
                        "id": id,
                        "result": result,
                        "_token": _token,
                    },
                    beforeSend: function () {
                        loading_process();
                    },
                    complete: function () {
                        $('#table-list').unblock();
                    },
                    success: function (response) {
                        var data_response = response.data;

                        if (data_response.status == 200) {
                            myalert('success', data_response.output);
                            //reload data table
                            table.ajax.reload( null, false );
                        }else{
                            myalert('error', data_response.output);
                        }

                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                    }
                });

            }else{
                bootbox.alert("Prompt dismissed");
            }
        });
    }

function onprogress_carton(e) {
    var _token = $("input[name='_token']").val();
    var id =  e.getAttribute('data-id');
    var po_number =  e.getAttribute('data-po');
    var customer_size =  e.getAttribute('data-customersize');

    bootbox.confirm("Are you sure to progress ( "+po_number+" - Size: "+customer_size+") ?", function(result){
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: 'request-carton/progress',
                    type: "GET",
                    data: {
                        "po_number": po_number,
                        "id": id,
                        "result": result,
                        "_token": _token,
                    },
                    beforeSend: function () {
                        loading_process();
                    },
                    complete: function () {
                        $('#table-list').unblock();
                    },
                    success: function (response) {
                        var data_response = response.data;

                        if (data_response.status == 200) {
                            myalert('success', data_response.output);
                            table.ajax.reload( null, false );
                        }else{
                            myalert('error', data_response.output);
                        }

                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                    }
                });

            }
            // else{
            //     bootbox.alert("Prompt dismissed");
            // }
        });

}

function done_carton(e) {
    var _token = $("input[name='_token']").val();
    var id =  e.getAttribute('data-id');
    var po_number =  e.getAttribute('data-po');
    var customer_size =  e.getAttribute('data-customersize');

    bootbox.confirm("Are you sure to complete carton ( "+po_number+" - Size: "+customer_size+") ?", function(result){
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: 'request-carton/done',
                    type: "GET",
                    data: {
                        "po_number": po_number,
                        "id": id,
                        "result": result,
                        "_token": _token,
                    },
                    beforeSend: function () {
                        loading_process();
                    },
                    complete: function () {
                        $('#table-list').unblock();
                    },
                    success: function (response) {
                        var data_response = response.data;

                        if (data_response.status == 200) {
                            myalert('success', data_response.output);
                            table.ajax.reload( null, false );
                        }else{
                            myalert('error', data_response.output);
                        }

                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                    }
                });

            }
            // else{
            //     bootbox.alert("Prompt dismissed");
            // }
        });

}

// sewing receive
function receive_carton(e) {
    var _token = $("input[name='_token']").val();
    var id =  e.getAttribute('data-id');
    var po_number =  e.getAttribute('data-po');
    var customer_size =  e.getAttribute('data-customersize');

    bootbox.confirm("Are you sure to receive carton ( "+po_number+" - Size: "+customer_size+") ?", function(result){
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: 'request-carton/receive',
                    type: "GET",
                    data: {
                        "po_number": po_number,
                        "id": id,
                        "result": result,
                        "_token": _token,
                    },
                    beforeSend: function () {
                        loading_process();
                    },
                    complete: function () {
                        $('#table-list').unblock();
                    },
                    success: function (response) {
                        var data_response = response.data;

                        if (data_response.status == 200) {
                            myalert('success', data_response.output);
                            table.ajax.reload( null, false );
                        }else{
                            myalert('error', data_response.output);
                        }

                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                    }
                });

            }
            // else{
            //     bootbox.alert("Prompt dismissed");
            // }
        });

}
</script>
@endsection
