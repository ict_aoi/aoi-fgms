@extends('layouts.app', ['active' => 'posync'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <!-- <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">PO SYNC</span></h4>
            <p class="position-left"></p>
        </div>
    </div> -->
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('packing.posync') }}"><i class="icon-cloud-download position-left"></i> PO Sync</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('packing.ajaxGetDataPoSync') }}" id="form_filter">
            <div class="form-group" id="filter_by_statdate">
                <label><b>Choose PODD Check Date (month/day/year)</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_po">
                <label><b>Choose PO Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="po" id="po" placeholder="PO Number">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="stat">Filter by PODD Check Date</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="po_">Filter by PO Number</label>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
            <div class="table-responsive">
                <table class="table datatable-save-state" id="table-list">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>PO</th>
                            <th>Job</th>
                            <th>Article</th>
                            <th>Season</th>
                            <th>Style</th>
                            <th>New Qty</th>
                            <th>CRD</th>
                            <th>LC Date</th>
                            <th>Order Type</th>
                            <th>Document Type</th>
                            <th>PO Stat Date</th>
                            <th>PODD Check</th>
                            <th>Cust.orderNo</th>
                            <th>Cust.No</th>
                            <th>Country Name</th>
                            <th>Sewing Place</th>
                            <th>Stuffing Place</th>
                            <th>Remaks</th>
                            <th>Info</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
    </div>
</div>
<a href="{{ route('packing.exportPoSync') }}" id="export_posync"></a>
@endsection

@include('report/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    //url
    var url = $('#form_filter').attr('action');
    var date = $('#date_range').val();
    var filter = null;

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        lengthMenu: [[5, 25, 50, -1], [5, 25, 50, "All"]],
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();

    var table = $('#table-list').DataTable({
        scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         true,
        fixedColumns:   {
            leftColumns: 2
            //rightColumns: 1
        },
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[1].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#export_posync').attr('href')
                                            + '?date_range=' + date
                                            + '&orderby=' + column_name
                                            + '&direction=' + direction
                                            + '&po=' + $('#po').val()
                                            + '&radio_status=' + $('input[name=radio_status]:checked').val()
                                            + '&filterby=' + filter;
                }
            }
       ],
        ajax: {
            url: url,
            //type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "radio_status": $('input[name=radio_status]:checked').val(),
                    "po": $('#po').val(),
                    "date_range": $('#date_range').val(),
                    "filterby": $('.dataTables_filter input').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
           // $('td', row).eq(1).css('min-width', '150px');
           // $('td', row).eq(2).css('min-width', '150px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'po_number', name: 'po_number'},
            {data: 'job', name: 'job'},
            {data: 'article', name: 'article'},
            {data: 'season', name: 'season'},
            {data: 'style', name: 'style'},
            {data: 'new_qty', name: 'new_qty', sortable: false, orderable: false, searchable: false},
            {data: 'crd', name: 'crd'},
            {data: 'kst_lcdate',name:'kst_lcdate'},
            {data: 'order_type', name: 'order_type'},
            {data: 'document_type', name: 'document_type'},
            {data: 'po_stat_date_adidas', name: 'po_stat_date_adidas'},
            {data: 'podd_check', name: 'podd_check'},
            {data: 'customer_order_number', name: 'customer_order_number'},
            {data: 'customer_number', name: 'customer_number'},
            {data: 'country_name', name: 'country_name'},
            {data: 'sewing_place_update', name: 'sewing_place_update', sortable: false, orderable: false, searchable: false},
            {data: 'stuffing_place_update'},
            {data: 'remark', name: 'remark'},
            {data: 'info_order', name: 'info_order', sortable: false, orderable: false, searchable: false},
            {data: 'status', name: 'status'}
        ],
    });

   table
    .on( 'preDraw', function () {
        Pace.start();
    } )
    .on( 'draw.dt', function () {
        Pace.stop();
       // $('#table-list').unblock();
    } );

    //end of datatables

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        //check location
        if($('#date_range').val() == '') {
            alert('Please select date range first');
            return false;
        }

        //loading_process();

        table.draw();
    })


    $('#date_range').on('change', function(){
        date = $(this).val();
    })

     //choose filter
    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'po_') {
            if($('#filter_by_po').hasClass('hidden')) {
                $('#filter_by_po').removeClass('hidden');
            }

            $('#filter_by_statdate').addClass('hidden');
        }
        else if (this.value == 'stat') {
            if($('#filter_by_statdate').hasClass('hidden')) {
                $('#filter_by_statdate').removeClass('hidden');
            }

            $('#filter_by_po').addClass('hidden');
        }

        //loading_process();

        table.draw();
    });


});

</script>
@endsection
