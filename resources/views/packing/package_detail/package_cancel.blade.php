@extends('layouts.app', ['active' => 'packagecancel'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">List PO</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('packing.packagecancel') }}"><i class="icon-location4 position-left"></i> List PO</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('packing.ajaxGetPackageCancel') }}" id="form_filter">
            @csrf
            <div class="form-group">
				<label><b>PO Number</b></label>
				<div class="input-group">
                    <input type="text" class="form-control" name="po_number" id="po_number" required>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
			</div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>PO NUMBER</th>
                        <th>PLAN REF NUMBER</th>
                        <th>CUST</th>
                        <th>COUNTRY</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    var url = $('#form_filter').attr('action');

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            url: url,
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "po_number": $('#po_number').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'po_number', name: 'po_number'},
            {data: 'plan_ref_number', name: 'plan_ref_number'},
            {data: 'customer_order_number', name: 'customer_order_number'},
            {data: 'country', name: 'country'},
            {data: 'btn_action', name: 'checkbox', orderable: false, searchable: false}
        ],
    });
    //end of datatables

    table.on('preDraw', function() {
        Pace.start();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        //check location
        if($('#po_number').val() == '') {
            alert('Please input po number first');
            return false;
        }

        loading_process();

        table.draw();
    })

});

// proses delete
function delete_plan(e) {
    var _token = $("input[name='_token']").val();
    var po_number =  e.getAttribute('data-po');
    var plan_ref =  e.getAttribute('data-plan');

        bootbox.confirm("Are you sure delete po "+po_number+"?", function(result){
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: 'package-cancel/delete-data',
                    type: "GET",
                    // dataType: "JSON",
                    data: {
                        "po_number": po_number,
                        "plan_ref_number": plan_ref,
                        "_method": 'DELETE',
                        "_token": _token,
                    },
                    beforeSend: function () {
                        loading_process();
                    },
                    complete: function () {
                        $('#table-list').unblock();
                    },
                    success: function (response) {
                        var data_response = response.data;

                        if (data_response.status == 200) {
                            myalert('success', data_response.output);
                        }else{
                            myalert('error', data_response.output);
                        }

                        $('#form_filter').submit();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                        myalert('error', jqXHR['responseJSON']);
                    }
                });

            }
        });
    }
</script>
@endsection
