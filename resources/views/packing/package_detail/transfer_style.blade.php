@extends('layouts.app', ['active' => 'transferstyle'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <br>
    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('packing.transferstyle') }}"><i class="icon-location4 position-left"></i> List PO</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row {{ Auth::user()->admin_role !== 1 ? 'hidden' : '' }}">
            <div class="col-md-2"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->factory_id }}" {{ $val->factory_id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>PO NUMBER</th>
                        <th>CUST</th>
                        <th>COUNTRY</th>
                        <th>LC</th>
                        <th>STATISTICALDATE</th>
                        <th>STYLE</th>
                        <th>TRADEMARK</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('packing.ajaxGetPackageTransfer') }}" id="ajaxGetPackageTransfer"></a>
@endsection

@section('js')
<script type="text/javascript">
// $(document).ready(function() {
    var url = $('#ajaxGetPackageTransfer').attr('href');

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            url: url,
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "factory_id": $('#factory_id').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'po_number', name: 'po_number'},
            {data: 'customer_order_number', name: 'customer_order_number'},
            {data: 'country', name: 'country'},
            {data: 'kst_lcdate', name: 'kst_lcdate'},
            {data: 'kst_statisticaldate', name: 'kst_statisticaldate'},
            {data: 'upc', name: 'upc'},
            {data: 'trademark', name: 'trademark'},
            {data: 'btn_action', name: 'checkbox', orderable: false, searchable: false}
        ],
    });
    //end of datatables

    table.on('preDraw', function() {
        Pace.start();
        loading_process();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    $('#factory_id').on('change', function() {
        loading_process();

        table.draw();

    });

// });

// proses transfer
function transfer_style(e) {
    var _token = $("input[name='_token']").val();
    var po_number =  e.getAttribute('data-po');

        bootbox.confirm("Are you sure transfer style for po "+po_number+"?", function(result){
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: 'transfer-style/transfer-data',
                    type: "GET",
                    // dataType: "JSON",
                    data: {
                        "po_number": po_number,
                        "_method": 'TRANSFER',
                        "_token": _token,
                    },
                    beforeSend: function () {
                        loading_process();
                    },
                    complete: function () {
                        $('#table-list').unblock();
                    },
                    success: function (response) {
                        var data_response = response.data;

                        if (data_response.status == 200) {
                            myalert('success', data_response.output);
                            table.ajax.reload( null, false );
                        }else{
                            myalert('error', data_response.output);
                        }


                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                    }
                });

            }
        });
    }
</script>
@endsection
