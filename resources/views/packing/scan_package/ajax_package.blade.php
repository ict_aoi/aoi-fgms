@foreach($data as $key => $value)
<tr>
<td>{{ $value->barcode_id }}</td>
<td>{{ $value->scan_id }}</td>
<td><span class="label label-flat border-grey text-grey-600">{{ $value->current_department }}</span></td>
<td id="current_status_{{ $value->barcode_id }}">
@if($value->current_status == 'onprogress')
<span class="label label-primary">ON PROGRESS</span>
@elseif($value->current_status == 'completed')
<span class="label label-success">COMPLETED</span>
@else
<span class="label label-danger">REJECTED</span>
@endif
</td>
<td>
    <a href="{{ route('packing.ajaxSetStatusPackage') }}"
       data-barcodeid = "{{ $value->barcode_id }}"
       data-scanid = "{{ $value->scan_id}}"
       data-department = "{{ $value->current_department }}"
       data-status = 'completed'
       class="btn btn-xs btn-success ignore-click set-status">
       <i class="fa fa-check"></i>
    </a>
    <a href="{{ route('packing.ajaxSetStatusPackage') }}"
       data-barcodeid = "{{ $value->barcode_id }}"
       data-scanid = "{{ $value->scan_id}}"
       data-department = "{{ $value->current_department }}"
       data-status = 'rejected'
       class="btn btn-xs btn-danger ignore-click set-status">
       <i class="fa fa-remove"></i>
   </a>
   <a href="{{ route('packing.ajaxSetStatusPackage') }}"
      data-barcodeid = "{{ $value->barcode_id }}"
      data-scanid = "{{ $value->scan_id}}"
      data-department = "{{ $value->current_department }}"
      data-status = 'onprogress'
      class="btn btn-xs btn-primary ignore-click set-status">
      <i class="fa fa-repeat"></i>
  </a>
    <!-- <ul class="icons-list">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="icon-menu9"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li>
                    <a href="{{ route('packing.ajaxSetStatusPackage') }}"
                       data-barcodeid = "{{ $value->barcode_id }}"
                       data-scanid = "{{ $value->scan_id}}"
                       data-department = "{{ $value->current_department }}"
                       data-status = 'completed'
                       class="ignore-click set-status">
                       <i class="icon-checkmark4"></i>
                       Set As Completed
                    </a>
                </li>
                <li>
                    <a href="{{ route('packing.ajaxSetStatusPackage') }}"
                       data-barcodeid = "{{ $value->barcode_id }}"
                       data-scanid = "{{ $value->scan_id}}"
                       data-department = "{{ $value->current_department }}"
                       data-status = 'rejected'
                       class="ignore-click set-status">
                       <i class="icon-cross2"></i>
                       Set As Rejected
                   </a>
               </li>
                <li>
                    <a href="{{ route('packing.ajaxSetStatusPackage') }}"
                       data-barcodeid = "{{ $value->barcode_id }}"
                       data-scanid = "{{ $value->scan_id}}"
                       data-department = "{{ $value->current_department }}"
                       data-status = 'onprogress'
                       class="ignore-click set-status">
                       <i class="icon-reload-alt"></i>
                       Set As On Progress
                   </a>
               </li>
            </ul>
        </li>
    </ul> -->
</td>
</tr>
@endforeach
