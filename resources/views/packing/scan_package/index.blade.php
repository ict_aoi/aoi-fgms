@extends('layouts.app', ['active' => 'packinglist'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">SCAN PACKAGE</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('packing.scanPackage') }}"><i class="icon-home4 position-left"></i> Scan Package</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-12">
                <input type="text" class="form-control input-new" id="search-package" placeholder="#Scan ID"></input>
                <input type="hidden" id="_barcodeid"></input>
                <input type="hidden" id="_status"></input>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <table class="table" id="show-result">
            <thead>
                <tr>
                    <th>BARCODE ID</th>
                    <th>DEPARTMENT</th>
                    <th>STATUS</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<a href="{{ route('packing.ajaxSetStatusPackage') }}" id="set-status-package"></a>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#search-package').focus();

    //get barcode from scanner
    function getBarcodeStatus(value, length) {
        var data = new Array();
        var scan_id = value;
        var scan_length = scan_id.length;
        var barcode_length = parseInt(scan_length) - parseInt(length);
        var barcodeid = '';

        for(var i = barcode_length; i < scan_length; i++) {
            barcodeid += scan_id[i];
        }

        if(scan_length <= length && scan_length > 0) {
            var status = 'completed';
        }
        else if(scan_length > length) {
            var current_status = '';
            var current_length = parseInt(scan_length) - parseInt(length);
            for(var i = length + 1; i < current_length; i++) {
                current_status += scan_id[i];
            }

            if(current_status == 'completed') {
                var status = 'onprogress'
            }
            else if(current_status == 'onprogress') {
                var status = 'completed';
            }
            else {
                var status = current_status;
            }
            console.log(status);
        }
        else {
            $('#search-package').val('');
            return false;
        }

        data[0] = barcodeid;
        data[1] = status;
        return data;
    }

    //scan package
    $('.input-new').keypress(function(event) {
        if(event.which == 13) {
            event.preventDefault();
            var search = $('#search-package').val();
            var length = search.split('-')[0].length;
            console.log(search);

            var data = getBarcodeStatus(search, length);
            var beauty_search = data[0] + '-' + data[1];
            console.log(beauty_search);

            $('#search-package').val(beauty_search);
            $('#_barcodeid').val(data[0]);
            $('#_status').val(data[1]);
        }
    });

    var timer;
    $('#search-package').keyup(function () {
        clearTimeout(timer);
        timer = setTimeout(function (event) {
            var barcodeid = $('#_barcodeid').val();
            var status = $('#_status').val();

            //check apakah search bar sudah diisi atau belum
            if($('#search-package').val() == '') {
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: $('#set-status-package').attr('href'),
                data: {barcodeid: barcodeid, status: status},
                success: function(response){
                    $('#show-result > tbody').append(response);
                },
                error: function(response){
                    if(response['status'] == 422) {
                        alert(response['responseJSON']);
                    }
                    else {
                        alert('not good');
                    }
                    $('#search-package').val('');
                }
            }).done(function() {
                $('#search-package').val('');
                $('#search-package').focus();
                $('#_barcodeid').val('');
                $('#_status').val('');
            });
        }, 1000);
    });

});
</script>
@endsection
