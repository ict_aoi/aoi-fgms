<tr>
    <td>{{ $data['barcode_id'] }}</td>
    <td><span class="label label-flat border-grey text-grey-600">{{ $data['current_department'] }}</span></td>
    <td>
        @if($data['current_status'] == 'onprogress')
        <span class="label label-primary">ON PROGRESS</span>
        @elseif($data['current_status'] == 'completed')
        <span class="label label-success">COMPLETED</span>
        @else
        <span class="label label-danger">REJECTED</span>
        @endif
    </td>
    <td>{{ $data['created_at'] }}</td>
</tr>
