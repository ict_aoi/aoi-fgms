@section('js_extension')
<script type="text/javascript" src="{{url('js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{ url('js/button_datatables/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/button_datatables/buttons.html5.min.js') }}"></script>
<script type="text/javascript" src="{{url('js/plugins/notifications/sweet_alert.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/plugins/ui/moment/moment.min.js')}}"></script>
@endsection
