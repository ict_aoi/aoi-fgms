
@extends('layouts.app', ['active' => 'costco'])

@section('page_header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">UPLOAD COSTCO</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('packing.packingList') }}"><i class="glyphicon glyphicon-upload position-left"></i> UPLOAD COSTCO</a></li>
        </ul>
    </div>
</div>
@endsection

@section('content')
    <div class="panel panel-flat">
        <div class="panel-body">
          <form action="{{ route('packing.uploadCostco') }}" method="post" id="formup"  enctype="multipart/form-data" >    
            @csrf
            <div class="row">
                    <div class="col-lg-4">
                        <label><b>PO Number </b></label>
                        <label><b> : </b></label> 
                        <label><b> {{ $po_number }} </b></label>
                        <input type="hidden" name="po_number" value="{{ $po_number }}" id="po_number">                                               
                    </div>
                    <div class="col-lg-4">
                        <label><b>Plan Ref Number </b></label>
                        <label><b> : </b></label> 
                        <label><b> {{ $plan_ref_number }} </b></label> 
                        <input type="hidden" name="plan_ref_number" value="{{ $plan_ref_number }}" id="plan_ref_number">
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                        <input type="file" name="file_upload" id="file_upload" class="file-styled form-control" required="">
                        <span class="help-block" id="excel_po"></span>
                        <span class="help-block">Accepted formats: xls xlsx.</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <button type="submit" class="btn btn-success">Upload</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript" src="{{url('js/plugins/notifications/sweet_alert.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
       $('#formup').submit(function(event){
        event.preventDefault();
            
            var formData = new FormData($(this)[0]);
            var parameter = ['po_number','plan_ref_number'];
            var val = [$('#po_number').val(),$('#plan_ref_number').val()  ];

            for(var i = 0; i < parameter.length; i++) {
                formData.append(parameter[i], val[i]);
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                    type: 'POST',
                    url : $('#formup').attr('action'),
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(response) {                   
                    myalert('success', 'Upload Success..');
                        window.location.href ='{{ route("packing.packingList")}}';                  
                    },
                    error: function(response) {
                         myalert('error', response['responseJSON']);
                    }
            });

       });
    });
</script>
@endsection






