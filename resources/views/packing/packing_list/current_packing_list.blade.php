@foreach($data as $key => $val)
    <tr>
        <td>{{ str_pad($val->package_range, 4, "0", STR_PAD_LEFT) }}</td>
        <td>{{ $val->package_from }}</td>
        <td>{{ $val->package_to }}</td>
        <td>{{ $val->serial_from }}</td>
        <td>{{ $val->serial_to }}</td>
        <td>{{ $val->po_number }}</td>
        <td>{{ $val->customer_size }}</td>
        <td>{{ $val->item_qty }}</td>
        <td>{{ $val->inner_pack }}</td>
        <td>{{ $val->inner_pkg_count }}</td>
        <td>{{ $val->pkg_count }}</td>
        <td>{{ $val->r }}</td>
        <td>{{ $val->net_net }}</td>
        <td>{{ $val->net }}</td>
        <td>{{ $val->gross }}</td>
        <td>{{ $val->unit_1 }}</td>
        <td>{{ $val->length }}</td>
        <td>{{ $val->width }}</td>
        <td>{{ $val->height }}</td>
        <td>{{ $val->unit_2 }}</td>
        <td>{{ $val->scan_id }}</td>
    </tr>
@endforeach
