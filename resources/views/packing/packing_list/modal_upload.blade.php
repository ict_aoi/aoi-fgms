@extends('layouts.app', ['active' => 'packinglist'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">UPLOAD EXCEL</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#"><i class="icon-file-upload position-left"></i> upload excel</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<form id="upload_po" action="#" class="form-horizontal" enctype="multipart/form-data">
    @csrf
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="form-group">
                <label class="col-lg-3 control-label">File Excel:</label>
                <div class="col-lg-9">
                    <input type="file" class="file-styled" name="file_upload">
                    <span class="help-block">Accepted formats: xls xlsx. Max file size 2Mb</span>
                </div>
            </div>
            <div class="text-right">
                <button type="button" id="verify" class="btn btn-success">Verify<i class="icon-checkmark4 position-right"></i></button>
            </div>
            <br>

            <div id="verify_data">

            </div>
        </div>
    </div>
    <div class="panel panel-flat">
        <div class="panel-body">

            <!-- IDENTIFICATION FIELD -->
            <!-- <div class="row">
                <div class="col-md-12">
                    <legend class="text-semibold"><i class="icon-file-eye position-left"></i> Identification</legend>
                </div>
                <div class="col-md-6">
                    <fieldset>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Plan Ref Number:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="plan_ref_number" placeholder="input plan ref number here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Booking Number:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="booking_number" placeholder="input booking number here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Final Workflow:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="final_workflow" placeholder="input final workflow here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Estimated Ex-Factory Date:</label>
                            <div class="col-lg-9">
                                <input type="date" class="form-control" name="exfactory_date" placeholder="choose estimated ex-factory date">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Notes:</label>
                            <div class="col-lg-9">
                                <textarea rows="5" cols="5" name="notes" class="form-control" placeholder="Enter your message here"></textarea>
                            </div>
                        </div>
                    </fieldset>
                </div>

                <div class="col-md-6">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Plan Date:</label>
                            <div class="col-lg-9">
                                <input type="date" class="form-control" name="plan_date" placeholder="choose estimated ex-factory date">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Authorization#:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="authorization" placeholder="input authorization here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Customer Number:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="customer_number" placeholder="input customer number here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Trigger Label:</label>
                            <div class="col-lg-9">
                                <select data-placeholder="select trigger label" name="trigger_label" class="select">
                                    <option>-</option>
                                    <option value="0" selected="selected">FALSE</option>
                                    <option value="1">TRUE</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div> -->
            <!-- END OF IDENTIFICATION FIELD -->
            <!--<hr> -->

            <div class="row">
                <!-- BUYER FIELD -->
                <div class="col-md-6">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-reading position-left"></i> Buyer</legend>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Buyer Name:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="buyer_name" placeholder="Input buyer name here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Buyer Address:</label>
                            <div class="col-lg-9">
                                <textarea rows="5" cols="5" class="form-control" name="buyer_address" placeholder="Input buyer address here"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Buyer City:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="buyer_city" placeholder="Input buyer city here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Buyer Postal Code:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="buyer_postalcode" placeholder="Input buyer postal code here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Buyer Country:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="buyer_country" placeholder="Input buyer country here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Buyer Phone:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="buyer_phone" placeholder="Input buyer phone here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Buyer Fax:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="buyer_fax" placeholder="Input buyer fax here">
                            </div>
                        </div>

                    </fieldset>
                </div>
                <!-- END OF BUYER FIELD -->

                <!-- DESTINATION FIELD -->
                <div class="col-md-6">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-truck position-left"></i> DESTINATION</legend>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Destination Name:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="destination_name" placeholder="Input destination name here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Destination Address:</label>
                            <div class="col-lg-9">
                                <textarea rows="5" cols="5" class="form-control" name="destination_address" placeholder="Input destination address here"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Destination City:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="destination_city" placeholder="Input destination city here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Destination Postal Code:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="destination_postalcode" placeholder="Input destination postal code here">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Destination Country:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="destination_country" placeholder="Input destination country here">
                            </div>
                        </div>
                    </fieldset>
                </div>
                <!-- END OF DESTINATION FIELD -->

            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </div>
    </div>
</form>


@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {

});
</script>
@endsection
