@extends('layouts.app', ['active' => 'editpackinglist'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">Edit Packinglist</span> (po number : #{{$po_number}} / plan ref number : #{{$plan_ref_number}})</h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#"><i class="icon-pencil7 position-left"></i> Edit Packinglist</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')

<div class="panel panel-flat">
    <div class="panel-body">
        <input type="text" name="po_number" id="po_number" value="{{$po_number}}" hidden="">
        <input type="text" name="plan_ref_number" id="plan_ref_number" value="{{$plan_ref_number}}" hidden="">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>PO NUMBER</th>
                        <th>PLAN REF</th>
                        <th>SCAN ID</th>
                        <th>ART. NO.</th>
                        <th>CUSTOMER SIZE</th>
                        <th>MANUFACTURING SIZE</th>
                        <th>INNER PACK</th>
                        <th>ITEM QTY</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            url:"{{ route('packing.getPackingList') }}",
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "po_number": $('#po_number').val(),
                    "plan_ref_number":$('#plan_ref_number').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'po_number', name: 'po_number'},
            {data: 'plan_ref_number', name: 'plan_ref_number'},
            {data: 'scan_id', name: 'scan_id'},
            {data: 'buyer_item', name: 'buyer_item'},
            {data: 'customer_size', name: 'customer_size'},
            {data: 'manufacturing_size', name: 'manufacturing_size'},
            {data: 'inner_pack', name: 'inner_pack'},
            {data: 'item_qty', name: 'item_qty'}
        ],
    });

    $('#table-list').on('blur', '.buyer_unfilled', function() {
        var scan_id = $(this).data('scanid');
        
 
        var buyer_item = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{ route('packing.editBuyerItem') }}",
            data: {scan_id:scan_id, buyer_item:buyer_item},
            beforeSend: function() {
                $('#table-list').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response){
                $('#buyer_' + scan_id).html(response);
                $('#table-list').unblock();
                table.clear();
                table.draw();
            },
            error: function(response) {
                return false;
               
            }
        });

    });

    $('#table-list').on('blur', '.customer_unfilled', function() {
        var scan_id = $(this).data('scanid');
        
 
        var customer_size = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{ route('packing.editCustomerSize') }}",
            data: {scan_id:scan_id, customer_size:customer_size},
            beforeSend: function() {
                $('#table-list').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response){
                $('#buyer_' + scan_id).html(response);
                $('#table-list').unblock();
                table.clear();
                table.draw();
            },
            error: function(response) {
                return false;
               
            }
        });

    });


    $('#table-list').on('blur', '.manufacturing_unfilled', function() {
        var scan_id = $(this).data('scanid');
        
 
        var manufacturing_size = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{ route('packing.editManufacturingSize') }}",
            data: {scan_id:scan_id, manufacturing_size:manufacturing_size},
            beforeSend: function() {
                $('#table-list').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response){
                $('#buyer_' + scan_id).html(response);
                $('#table-list').unblock();
                table.clear();
                table.draw();
            },
            error: function(response) {
                return false;
               
            }
        });

    });

    
});
</script>
@endsection



