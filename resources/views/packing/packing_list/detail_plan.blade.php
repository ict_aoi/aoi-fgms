@extends('layouts.app', ['active' => 'packinglist'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">PACKAGE DETAILS</span> (po number : #{{$po_number}} / plan ref number : #{{$plan_ref_number}})</h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('packing.packingList') }}"><i class="icon-list position-left"></i> Packing List</a></li>
            <li class="active"><a href="#"> Package Details</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="table_package_detail">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>BARCODE ID</th>
                        <th>SIZE</th>
                        <th>DIMENSION (L x W x H (satuan))</th>
                        <th width="100px">IS COMPLETED</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('packing.ajaxPackageDetailData') }}" id="package_detail_get_data" data-ponumber = '{{ $po_number}}'></a>
<a href="{{ route('packing.ajaxSetStatusPackage') }}" id="set_status_package"></a>
<a href="{{ route('packing.ajaxViewTemplate') }}" id="url_view_template"></a>
<a href="{{ route('packing.ajaxGenerateBarcode') }}" id="url_generate_barcode"></a>
<a href="{{ route('packing.printPreview') }}" id="print_preview"></a>
@endsection

@section('modal')
<!-- MODAL SHIPPING MARK -->
<div id="modal_shippingmark_" class="modal fade" role="dialog">
    <form class="form-horizontal" method="post" action="{{ route('packing.ajaxSetCompleted') }}" target="_blank" id="form-print-shippingmark">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content modal-lg">
                <div class="modal-body">
                    <div class="panel-body">
                        <fieldset>
                            <legend class="text-semibold">
                                <h4 id="title_shippingmark"><i class="icon-file-text2 position-left"></i>PRINT BARCODE & SHIPPING MARK</h4>
                            </legend>
                            <div class="form-group" id="barcode_input">
                                <label class="col-lg-2 control-label">BARCODE:</label>
                                <div class="col-lg-10" id ="generate_barcode">

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">TEMPLATE:</label>
                                <div class="col-lg-10">
                                    <select id="template_shippingmark" data-placeholder="select shippingmark" name="template_shippingmark" class="select form-control" required>
                                        <option value="" selected disabled>CHOOSE TEMPLATE</option>
                                        <option value="australia">Australia</option>
                                        <option value="new_zealand">New Zealand</option>
                                        <option value="sld_ship_a5_address_usa_sld">SLD Ship A5 (Address USA SLD)</option>
                                        <option value="sm_adidas_apparel_fcopy_a5_standar">SM Adidas Apparel FCopy A5 (standar)</option>
                                        <option value="sm_adidas_sld_apparel_a5_fcopy_usa_sld">SM Adidas SLD Apparel A5 Fcopy (USA SLD)</option>
                                        <option value="sm_china">SM China</option>
                                        <option value="sm_egypt_a5">SM Egypt A5</option>
                                        <option value="uk">UK</option>
                                        <option value="sm_adidas_apparel_fcopy_a4_standar">SM Adidas Apparel FCopy A4 (standar)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group hidden" id="size">
                                <label class="col-lg-2 control-label">SIZE:</label>
                                <div class="col-lg-10">
                                    <select id="size_template" data-placeholder="select size template" name="size_template" class="select form-control" required>
                                        <option value="s">Small</option>
                                        <option value="m">Medium</option>
                                        <option value="l" selected>Large</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group hidden" id="font_size">
                                <label class="col-lg-2 control-label">FONT SIZE CUST.O/N:</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" name="font_size_template" onkeypress="return isNumberDot(event);" id="font_size_template" value="65" placeholder="ukuran font size default 65px" required autocomplete="off"></input>
                                </div>
                                <label class="col-lg-2 control-label">FONT MULTI SIZE:</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" name="multi_font_size" id="multi_font_size" onkeypress="return isNumberDot(event);" value="65" placeholder="ukuran font size default 65px" required autocomplete="off"></input>
                                </div>
                            </div>
                            <input type='hidden' id="barcode_id_shippingmark" name="barcode_id_shippingmark"></input>
                            <input type='hidden' id="_current_is_printed" name="_current_is_printed"></input>
                            <input type='hidden' id="po_number" name="po_number"></input>
                            <div id="view_template">
                                <!-- template pilihan -->
                                <section class="panel hidden" style="padding:10px;">
                                </section>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Print</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /MODAL SHIPPING MARK -->

<!-- MODAL SHIPPING MARK PRINT ALL -->
<div id="modal_shippingmark_all_" class="modal fade" role="dialog">
    <form class="form-horizontal" method="post" action="{{ route('packing.ajaxSetCompletedAll') }}" target="_blank" id="form-print-shippingmark_all">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content modal-lg">
                <div class="modal-body">
                    <div class="panel-body">
                        <fieldset>
                            <legend class="text-semibold">
                                <h4 id="title_shippingmark_all"><i class="icon-file-text2 position-left"></i>PRINT BARCODE & SHIPPING MARK</h4>
                            </legend>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">TEMPLATE:</label>
                                <div class="col-lg-10">
                                    <select id="template_shippingmark_all" data-placeholder="select shippingmark" name="template_shippingmark" class="select form-control" required>
                                        <option value="" selected disabled>CHOOSE TEMPLATE</option>
                                        <option value="australia">Australia</option>
                                        <option value="new_zealand">New Zealand</option>
                                        <option value="sld_ship_a5_address_usa_sld">SLD Ship A5 (Address USA SLD)</option>
                                        <option value="sm_adidas_apparel_fcopy_a5_standar">SM Adidas Apparel FCopy A5 (standar)</option>
                                        <option value="sm_adidas_sld_apparel_a5_fcopy_usa_sld">SM Adidas SLD Apparel A5 Fcopy (USA SLD)</option>
                                        <option value="sm_china">SM China</option>
                                        <option value="sm_egypt_a5">SM Egypt A5</option>
                                        <option value="uk">UK</option>
                                        <option value="sm_adidas_apparel_fcopy_a4_standar">SM Adidas Apparel FCopy A4 (standar)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group hidden" id="size_all">
                                <label class="col-lg-2 control-label">SIZE:</label>
                                <div class="col-lg-10">
                                    <select id="size_template_all" data-placeholder="select size template" name="size_template" class="select form-control" required>
                                        <option value="s">Small</option>
                                        <option value="m">Medium</option>
                                        <option value="l" selected>Large</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group hidden" id="font_size_all">
                                <label class="col-lg-2 control-label">FONT SIZE CUST.O/N:</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" name="font_size_template" id="font_size_template_all" onkeypress="return isNumberDot(event);" value="65" placeholder="ukuran font size default 65px" required autocomplete="off"></input>
                                </div>
                                <label class="col-lg-2 control-label">FONT MULTI SIZE:</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" name="multi_font_size" id="multi_font_size_all" onkeypress="return isNumberDot(event);" value="65" placeholder="ukuran font size default 65px" required autocomplete="off"></input>
                                </div>

                                <!-- <label class="col-lg-2 control-label">FONT SIZE:</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="font_size_template" id="font_size_template_all" value="65" placeholder="ukuran font size default 65px" required></input>
                                </div> -->
                            </div>
                            <div class="form-group" id="barcode_hide_">
                                <label class="col-lg-2 control-label">HIDE BARCODE:</label>
                                <div class="col-lg-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="barcode_hide" id="barcode_hide">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="package_total_hide_">
                                <label class="col-lg-2 control-label">HIDE PACKAGE TOTAL:</label>
                                <div class="col-lg-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="package_total_hide" id="package_total_hide">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="filter_package_">
                                <label class="col-xs-2 control-label">FILTER PACKAGE:</label>
                                <div class="col-xs-2">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="filter_package" id="filter_package">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-8">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <input type="number" name="filter_from" id="filter_from" class="form-control hidden" placeholder="From">
                                            </div>
                                            <div class="col-xs-6">
                                                <input type="number" name="filter_to" id="filter_to" class="form-control hidden" placeholder="To">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type='hidden' id="po_number_all" name="po_number"></input>
                            <div id="view_template_all">
                                <!-- template pilihan -->
                                <section class="panel hidden" style="padding:10px;">
                                </section>
                            </div>
                            <!-- <div class="form-group hidden" id="font_size_all">
                                <label class="col-lg-2 control-label">FONT SIZE:</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="font_size_template" id="font_size_template_all" value="65" placeholder="ukuran font size default 65px" required></input>
                                </div>
                            </div> -->
                        </fieldset>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Print All</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /MODAL SHIPPING MARK PRINT ALL -->
@endsection

@include('packing/packing_list/_js_detail')
@section('js')
<script type="text/javascript">
$( document ).ready(function() {
    //datatables
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        stateSave: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });
    var url = $('#package_detail_get_data').attr('href');
    var ponumber = $('#package_detail_get_data').data('ponumber');
    var table = $('#table_package_detail').DataTable({
        buttons: [
            {
                text: 'Print All',
                className: 'btn btn-sm bg-primary printAll'
            }
        ],
        ajax: {
            'url' : url,
            'data': {po_number: ponumber}
        },
        columns: [
            {data: 'package_number', name: 'package_number'},
            {data: 'barcode_id', name: 'barcode_id'},
            {data: 'customer_size', name:'package_detail.customer_size'},
            {data: 'dimension', name: 'dimension', sortable: false, orderable: false, searchable: false},
            {data: 'is_printed', name: 'is_printed', sortable: false, orderable: false, searchable: false},
            {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false}
        ]
    });
    //end of datatables

    //
    $('#table_package_detail').on('click','.ignore-click', function() {
        return false;
    });

    //open modal print all
    $('.printAll').on('click', function(){
        $('#modal_shippingmark_all_').modal('show');
        var po_number = $('#package_detail_get_data').data('ponumber');
        $('#po_number_all').val(po_number);
    });

    //open modal print
    $('#table_package_detail').on('click', '.set-completed', function() {
        var this_url = $(this).attr('href');
        var params = getURLParameter(this_url);
        var barcodeid = params['barcodeid'];
        var current = params['current_is_printed'];
        var po_number = $('#package_detail_get_data').data('ponumber');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#url_generate_barcode').attr('href'),
            data: {barcodeid: barcodeid},
            success: function(response) {
                $('#generate_barcode').html('');
                $('#generate_barcode').append(response);
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        }).done(function(response){
            $('#barcode_id_shippingmark').val(barcodeid);
            $('#_current_is_printed').val(current);
            $('#po_number').val(po_number);
            $('#modal_shippingmark_').modal('show');
        });
    });

    //reset modal hidden value
    $('#modal_shippingmark_all_').on('hidden.bs.modal', function() {
        $('#po_number_all').val('');
        $('#view_template_all > .panel').html('');
        $('#view_template_all > .panel').addClass('hidden');
        $('#template_shippingmark_all').val('');
        $('#size_template_all option[value=l]').prop('selected',true);
        $('#font_size_all').addClass('hidden');
        $('#font_size_template_all').val('65');
        $('#multi_font_size_all').val('65');
        $('#size_all').addClass('hidden');
        $('#filter_package').prop('checked', false);
        $('#filter_from').addClass('hidden');
        $('#filter_to').addClass('hidden');
    });

    $('#modal_shippingmark_').on('hidden.bs.modal', function() {
        $('#barcode_id_shippingmark').val('');
        $('#po_number').val('');
        $('#current_is_printed').val('');
        $('#generate_barcode').html('');
        $('#view_template > .panel').html('');
        $('#view_template > .panel').addClass('hidden');
        $('#template_shippingmark').val('');
        $('#size_template option[value=l]').prop('selected',true);
        $('#font_size').addClass('hidden');
        $('#font_size_all').val('65');
        $('#size').addClass('hidden');
    });

    //view template
    $('#template_shippingmark_all').on('change', function(){
        var template = $(this).val();

        if(template == 'sm_adidas_apparel_fcopy_a5_standar' || template == 'sm_china' || template == 'sm_adidas_apparel_fcopy_a4_standar') {
            $('#size_all').removeClass('hidden');
            $('#font_size_all').removeClass('hidden');
        }
        else {
            $('#font_size_all').addClass('hidden');
            $('#font_size_template_all').val('65');
            $('#multi_font_size_all').val('65');
            $('#size_all').addClass('hidden');
            $('#size_template_all option[value=l]').prop('selected',true);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: url_view_template,
            data: {template: template},
            success: function(response) {
                $('#view_template_all > .panel').removeClass('hidden');
                $('#view_template_all > .panel').html('');
                $('#view_template_all > .panel').append(response);
            },
            error: function(response) {
                if(response['status'] == 422) {
                    myalert('error',response['responseJSON']);
                }
            }
        });
    });

    //
    $('#template_shippingmark').on('change', function() {
        var template = $(this).val();
        var barcode_id = $('#barcode_id_shippingmark').val();

        if(template == 'sm_adidas_apparel_fcopy_a4_standar' || template == 'sm_adidas_apparel_fcopy_a5_standar' || template == 'sm_china') {
            $('#size').removeClass('hidden');
            $('#font_size').removeClass('hidden');
        }
        else {
            $('#font_size').addClass('hidden');
            $('#font_size_all').val('65');
            $('#size').addClass('hidden');
            $('#size_template option[value=l]').prop('selected',true);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: url_view_template,
            data: {template: template, barcode_id: barcode_id},
            success: function(response) {
                $('#view_template > .panel').removeClass('hidden');
                $('#view_template > .panel').html('');
                $('#view_template > .panel').append(response);
            },
            error: function(response) {
                if(response['status'] == 422) {
                    myalert('error',response['responseJSON']);
                }
            }
        });
    });

    //
    $('#size_template').on('change', function() {
        var size_template = $(this).val();
        if(size_template == 'l') {
            $('#font_size').removeClass('hidden');
            $('#font_size_template').val('65');
            $('#multi_font_size_all').val('65');
        }
        else {
            $('#font_size').addClass('hidden');
        }
    });

    //
    $('#size_template_all').on('change', function() {
        var size_template = $(this).val();
        if(size_template == 'l') {
            $('#font_size_all').removeClass('hidden');
            $('#font_size_template_all').val('65');
            $('#multi_font_size_all').val('65');
        }
        else {
            $('#font_size_all').addClass('hidden');
        }
    });

    //
    $('#filter_package').change(function() {
        if($(this).is(':checked')) {
            $('#filter_from').removeClass('hidden');
            $('#filter_to').removeClass('hidden');
        }
        else {
            $('#filter_from').addClass('hidden');
            $('#filter_to').addClass('hidden');
        }
    });

    //set all is_printed
    $('#form-print-shippingmark_all').submit(function(event) {
        event.preventDefault();
        var po_number = $('#po_number_all').val();
        var template = $('#template_shippingmark_all').val();
        var size_template = $('#size_template_all').val();
        var font_size_template = $('#font_size_template_all').val();
        var multi_font_size = $('#multi_font_size_all').val();
        var filter_package = $('#filter_package').is(':checked');
        var filter_from = false;
        var filter_to = false;

        if(filter_package) {
            filter_from = $('#filter_from').val();
            filter_to = $('#filter_to').val();

            if(filter_from == '' || filter_to == '') {
                myalert('error', '`From` and `To` cant empty');
                return false;
            }

            if(filter_from < 1) {
                myalert('error', '`From` cant be less than 1');
                return false;
            }

            if(filter_to < 1) {
                myalert('error', '`To` cant be less than 1');
                return false;
            }

            if(filter_from > filter_to) {
                myalert('error', 'Value `From` should less than value `To`');
                return false;
            }
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#form-print-shippingmark_all').attr('action'),
            data: $('#form-print-shippingmark_all').serialize(),
            beforeSend: function() {
                $('#modal_shippingmark_all_ .modal-body').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                table.draw();


                //jika template sm adidas apparel size large maka gunakan parameter font_size_template
                if((template == 'sm_adidas_apparel_fcopy_a4_standar' || template == 'sm_adidas_apparel_fcopy_a5_standar' || template == 'sm_china') && size_template == 'l') {
                    printPage(barcode_id = null, po_number = po_number, template_shippingmark = template,
                                size_template = size_template, type='all', filter_package = filter_package,
                                from = filter_from, to = filter_to,
                                font_size_template = font_size_template,
                                multi_font_size = multi_font_size);
                }
                else {
                    printPage(barcode_id = null, po_number = po_number, template_shippingmark = template,
                                size_template = size_template, type='all', filter_package = filter_package,
                                from = filter_from, to = filter_to);
                }

            },
            error: function(response) {
                $('#modal_shippingmark_all_ .modal-body').unblock();
                $('#modal_shippingmark_all_').modal('hide');
                $('#form-print-shippingmark_all').trigger("reset");
                myalert('error','NOT GOOD');
            }
        });
    });

    //set is_printed
    $('#form-print-shippingmark').submit(function(event) {
        event.preventDefault();
        var barcodeid = $('#barcode_id_shippingmark').val();
        var po_number = $('#po_number').val();
        var template = $('#template_shippingmark').val();
        var size_template = $('#size_template').val();
        var font_size_template = $('#font_size_template').val();
        var multi_font_size = $('#multi_font_size').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#form-print-shippingmark').attr('action'),
            data: $('#form-print-shippingmark').serialize(),
            beforeSend: function() {
                $('#modal_shippingmark_ .modal-body').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#completed_' + barcodeid).html('<i class="icon-checkmark2"></i>');
                $('#completed_' + barcodeid).removeClass();
                $('#completed_' + barcodeid).addClass('label label-success label-rounded');

                //jika template sm adidas apparel size large maka gunakan parameter font_size_template
                if((template == 'sm_adidas_apparel_fcopy_a4_standar' || template == 'sm_adidas_apparel_fcopy_a5_standar' || template == 'sm_china') && size_template == 'l') {
                    printPage(barcode_id = barcodeid, po_number = po_number, template_shippingmark = template,
                                size_template = size_template, type=null, filter_package = false, from = false, to = false,
                                font_size_template = font_size_template,
                                multi_font_size = multi_font_size);
                }
                else {
                    printPage(barcode_id = barcodeid, po_number = po_number, template_shippingmark = template,
                                size_template = size_template, type=null, filter_package = false, from = false, to = false);
                }

                $('#form-print-shippingmark').trigger("reset");
            },
            error: function(response) {
                $('#modal_shippingmark_ .modal-body').unblock();
                $('#modal_shippingmark_').modal('hide');
                myalert('error','NOT GOOD');
            }
        });
    });

    function printPage(barcode_id, po_number, template_shippingmark, size_template, type, filter_package, from, to, font_size_template, multi_font_size) {
        var url_print = $('#print_preview').attr('href');
        if(barcode_id === null) {
            var parameter = '?po_number=' + po_number +
                            '&template_shippingmark=' + template_shippingmark +
                            '&size_template=' + size_template;
        }
        else {
            var parameter = '?barcode_id=' + barcode_id + '&po_number=' + po_number +
                            '&template_shippingmark=' + template_shippingmark +
                            '&size_template=' + size_template;
        }

        //
        if (typeof font_size_template !== 'undefined') {
            var par_font_size = '&font_size_template=' + font_size_template;
            var par_multi_font_size = '&multi_font_size=' + multi_font_size;
            parameter += par_font_size;
            parameter += par_multi_font_size;
        }


        if ($('#barcode_hide').is(':checked')) {
            var barcode_show = '&barcode_show=' + false;
            parameter += barcode_show;
        }else{
            parameter += '&barcode_show=' + true;
        }

        if ($('#package_total_hide').is(':checked')) {
            var package_show = '&package_show=' + false;
            parameter += package_show;
        }else{
            parameter += '&package_show=' + true;
        }

        //
        if(filter_package) {
            if(from == '' || to == '') {
                myalert('error', '`From` and `To` cant empty');
                return false;
            }

            if(from < 1) {
                myalert('error', '`From` cant be less than 1');
                return false;
            }

            if(to < 1) {
                myalert('error', '`To` cant be less than 1');
                return false;
            }

            if(from > to) {
                myalert('error', 'Value `From` should less than value `To`');
                return false;
            }
            var par_filter_package = '&filter_package=' + filter_package +'&from=' + from +'&to=' + to;
            parameter += par_filter_package;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : url_print + parameter,
            success: function(response) {
                window.open(url_print + parameter, '_blank');
                if(type == 'all') {
                    $('#modal_shippingmark_all_ .modal-body').unblock();
                    $('#modal_shippingmark_all_').modal('hide');
                    $('#form-print-shippingmark_all').trigger("reset");
                }
                else {
                    $('#modal_shippingmark_ .modal-body').unblock();
                    $('#modal_shippingmark_').modal('hide');
                    $('#form-print-shippingmark').trigger("reset");
                }

            },
            error: function(response) {
                myalert('error','NOT GOOD');
                if(type == 'all') {
                    $('#modal_shippingmark_all_ .modal-body').unblock();
                    $('#modal_shippingmark_all_').modal('hide');
                    $('#form-print-shippingmark_all').trigger("reset");
                }
                else {
                    $('#modal_shippingmark_ .modal-body').unblock();
                    $('#modal_shippingmark_').modal('hide');
                    $('#form-print-shippingmark').trigger("reset");
                }
            }
        });
    }

    // function printData() {
    //     $('body').append('<iframe id="print_template" style="display:none;" name="print_template"><</iframe>');
    //     $('#print_template').contents().find('body').html('');
    //     $('#print_template').contents().find('body').append($('#view_template').html());
    //     $('#print_template').contents().find('body').append($('#generate_barcode').html());
    //     window.frames["print_template"].focus();
    //     window.frames["print_template"].print();
    // }

    //get parameter from url
    function getURLParameter(url) {
        var params = {};
        url.substring(1).replace(/[?&]+([^=&]+)=([^&]*)/gi,
                function (str, key, value) {
                     params[key] = value;
                });
        return params;
    }
});

</script>
@endsection
