<style type="text/css">

@page {
    margin: 10 20 0 20;
}

@font-face {
    font-family: 'impact' !important;
    src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
}

.template {
   font-size: 35 !important;
   font-family: 'impact';
}

.page-break {
    page-break-after: always;
}

.print-friendly {
    line-height: 55px;
    width: 100%;
}

table.print-friendly tr td, table.print-friendly tr th,
table.print-friendly-single tr td, table.print-friendly-single tr th, {
    page-break-inside: avoid;
}

.barcode {
    float: right;
    margin-bottom:20px;
    line-height: 16px;
    position:fixed;
    top: 15rem;
    margin-right:-50px;
    transform: rotate(270deg);
}

.barcode > img {
    display: block;
    padding: 0px;
    width: 180px;
    height:30px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 14px !important;
}

.item_code {

    font-family: sans-serif;
    font-size: 10px !important;
}

.rcl_code {
    font-family: sans-serif;
    font-size: 12px !important;
    font-weight: bold;
    padding-left: 2px;
}
</style>

@if(isset($size_template))
    @if($size_template == 'l')
    <div class="template" style="font-size: 35 !important">
    @elseif($size_template == 'm')
    <div class="template" style="font-size: 20 !important">
    @elseif($size_template == 's')
    <div class="template" style="font-size: 10 !important">
    @else
    <div class="template" style="font-size: 34 !important">
    @endif
@else
    <div class="template" style="font-size: 34 !important">
@endif

@if(isset($data))
    @foreach($data as $key => $value)
    <table class="print-friendly">
        <tr>
            <td style="width: 60%">CARTON NO./TOTAL CARTONS</td>
            <td>:</td>
            <td colspan="2" style="width: 40%"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; / @if(isset($showpackage)) {{ $value->total_package}} @endif</td>
        </tr>
        <tr>
            <td>PO.NO.</td>
            <td>:</td>
            <td colspan="2">{{ $value->po_number }}</td>
        </tr>
        <tr>
            <td>ART NO.</td>
            <td>:</td>
            <td colspan="2">{{ $value->buyer_item }}</td>
        </tr>
        <tr>
            <td>SIZE</td>
            <td>:</td>
            <td>{{ $value->customer_size }}</td>
            @if(isset($showbarcode))
            <td>
                <div class="barcode">
                    <!-- <div class="img_barcode" style="display:block;padding:0px"> -->
                    <div class="row">
                        <span class="barcode_number">{{ $value->barcode_id }}</span ><span class="rcl_code">{{ $value->recycle==true ? "Recycle" : "" }}</span >
                    </div>
                    <div class="img_barcode">
                        <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128',2,35) }}" style="width: 180px; height:30px" alt="barcode"   />
                    </div>
                    
                    <div class="row">
                        @if(strpos($value->manufacturing_size,',')===false)
                            <span class="item_code">{{ $value->season }}-{{ $value->upc }}-{{ $value->buyer_item }}-{{ $value->manufacturing_size }}</span >
                        @else
                            <span class="item_code">{{ $value->season }}-{{ $value->upc }}-{{ $value->buyer_item }}</span >
                        @endif
                    </div>
                </div>
            </td>
            @endif
        </tr>
        <tr>
            <td>QTY</td>
            <td>:</td>
            <td>{{ $value->inner_pack }}</td>
            <td style="text-align:left;">{{ $value->remark }}</td>
        </tr>
        <tr>
            <td>COUNTRY OF ORIGIN</td>
            <td>:</td>
            <td colspan="2" style="text-transform:uppercase">{{ $value->country_of_origin }}</td>
        </tr>
        <tr>
            <td>MEASUREMENT</td>
            <td>:</td>
            <td colspan="2">{{ $value->length * 1000 }}x{{ $value->width * 1000 }}x{{ $value->height * 1000 }} MM</td>
        </tr>
        <tr>
            <td>GROSS WEIGHT/CARTON WEIGHT</td>
            <td>:</td>
            <td colspan="2">{{ $value->gross }} {{ $value->unit_1 }}/{{ $value->carton_weight }} {{ $value->unit_1 }}</td>
        </tr>

    </table>

    @if($key != (count($data) - 1))
    <div class="page-break"></div>
    @endif
    @endforeach
@else
<table style="width:100%;">
    <tr>
        <td>CARTON NO./TOTAL CARTONS : - / -</td>
    </tr>
    <tr>
        <td>PO.NO. :</td>
    </tr>
    <tr>
        <td>ART NO. :</td>
    </tr>
    <tr>
        <td>SIZE :</td>
    </tr>
    <tr>
        <td>QTY :</td>
    </tr>
    <tr>
        <td>COUNTRY OF ORIGIN :</td>
    </tr>
    <tr>
        <td>MEASUREMENT :</td>
    </tr>
    <tr>
        <td>GROSS WEIGHT/CARTON WEIGHT : -/-</td>
    </tr>
</table>
@endif
</div>
