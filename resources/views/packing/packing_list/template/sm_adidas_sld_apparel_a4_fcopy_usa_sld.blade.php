<style type="text/css">

@page {
    margin: 5 20 0 20;
}

@font-face {
    font-family: 'impact' !important;
    src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
}

.template {
   /*font-size: 54 !important;*/
   font-family: 'impact';
}

.page-break {
    page-break-after: always;
}

.template {
   font-size: 34 !important;
   font-family: 'impact';
}

.page-break {
    page-break-after: always;
}

.print-friendly {
    line-height: 62px;
    width: 100%;
}

table.print-friendly tr td, table.print-friendly tr th,
table.print-friendly-single tr td, table.print-friendly-single tr th, {
    page-break-inside: avoid;
}

.barcode {
    float: right;
    line-height: 16px;
    margin-right: 5%;
}

.img_barcode {
    display: block;
}

.img_barcode > img {
    width: 180px !important;
    height:30px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 14px !important;
}
.item_code {

    font-family: sans-serif;
    font-size: 10px !important;
}

.separator {
    margin: 70 0 0 0;
}

.rcl_code {
    font-family: sans-serif;
    font-size: 11px !important;
    font-weight: bold;
    padding-left: 2px;
}

</style>

@if(isset($size_template))
    @if($size_template == 'l')
    <div class="template" style="font-size: 54 !important">
    @elseif($size_template == 'm')
    <div class="template" style="font-size: 40 !important">
    @elseif($size_template == 's')
    <div class="template" style="font-size: 30 !important">
    @else
    <div class="template" style="font-size: 48 !important">
    @endif
@else
    <div class="template" style="font-size: 48 !important">
@endif

@if(isset($data))
    @foreach($data as $key => $value)
    <table class="print-friendly">
        <tr>
            @if(isset($showbarcode))
            <td style="width: 50%; font-size:40px !important">Country of Origin</td>
            @else
            <td style="width: 50%;">Country of Origin</td>
            @endif
            <td style="width: 5%;">:</td>
            <td colspan="2" style="width: 45%;"><span style="text-transform:uppercase">{{ $value->country_of_origin }}</span></td>
        </tr>
        <tr>
            @if(isset($showbarcode))
            <td style="width: 50%; font-size:40px !important">Gross Weight</td>
            @else
            <td style="width: 50%;">Gross Weight</td>
            @endif
            <td style="width: 5%;">:</td>
            <td style="width: 30%;">{{ $value->gross }}</td>
            <td style="width: 15%; text-align:left;">{{ $value->unit_1 }}</td>
        </tr>
        <tr>
            <td>PO number</td>
            <td>:</td>
            <td colspan="2">{{ $value->po_number }}</td>
        </tr>
        <tr>
            <td>QTY</td>
            <td>:</td>
            <td>{{ $value->inner_pack }}</td>
            <td style="text-align:left;">{{ $value->remark }}</td>
        </tr>
        <tr>
            <td>Size</td>
            <td>:</td>
            <td colspan="2">{{ $value->customer_size }}</td>
        </tr>
        <tr>
            @if(isset($showbarcode))
            <td colspan="4">
                <div class="barcode">
                    <!-- <div class="img_barcode" style="display:block;padding:0px"> -->
                    <div class="row">
                        @if(strpos($value->manufacturing_size,',')===false)
                            <span class="item_code">{{ $value->season }}-{{ $value->upc }}-{{ $value->buyer_item }}-{{ $value->manufacturing_size }}</span >
                        @else
                            <span class="item_code">{{ $value->season }}-{{ $value->upc }}-{{ $value->buyer_item }}</span >
                        @endif
                    </div>
                    <div class="img_barcode">
                        <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128',2,35) }}" style="width: 180px; height:30px" alt="barcode"   />
                    </div>
                    <div class="row">
                        <span class="barcode_number">{{ $value->barcode_id }}</span ><span class="rcl_code">{{ $value->recycle==true ? "Recycle" : "" }}</span >
                    </div>
                    
                    
                </div>
            </td>
            @endif
        </tr>
    </table>


    @if(($key + 1) % 2 == 0 && $key != (count($data) - 1))
    <div class="page-break"></div>
    @elseif(($key + 1) % 2 != 0)
    <div class="separator"></div>
    @endif
    @endforeach
@else
<table style="width:100%;">
    <tr>
        <td>Country of Origin :</td>
    </tr>
    <tr>
        <td>Gross Weight :</td>
    </tr>
    <tr>
        <td>PO Number :</td>
    </tr>
    <tr>
        <td>QTY :</td>
    </tr>
    <tr>
        <td>Size :</td>
    </tr>
</table>
@endif
</div>
