<style type="text/css">

@page {
    margin: 25 20 0 20;
}

@font-face {
    font-family: 'impact' !important;
    src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
}

.template {
   font-size: 36;
   font-family: 'impact';
   margin-bottom: 0px;
}

.page-break {
    page-break-after: always;
} 

.print-friendly {
    line-height: 43px;
    width: 100%;
}

table.print-friendly tr td, table.print-friendly tr th,
table.print-friendly-single tr td, table.print-friendly-single tr th, {
    page-break-inside: avoid;
}

/*.barcode {
    float: right;
    margin-bottom:20px;
    line-height: 16px;
    position:fixed;
    top: 22rem;
    left: 35rem;
    transform: rotate(270deg);
}
*/

.barcode {
    float: right;
    /*margin-bottom:10px;*/
    line-height: 16px;
    margin-right:-60px;
    transform: rotate(270deg);
    top: 165px;
    position: relative;
}

.barcode_img {
    display: block;
    padding: 0px;
    width: 180px;
    height:30px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 14px !important;
}

.item_code {

    font-family: sans-serif;
    font-size: 10px !important;
}

.rcl_code {
    font-family: sans-serif;
    font-size: 12px !important;
    font-weight: bold;
    padding-left: 2px;
}

</style>

<div class="template">
@if(isset($data))
    @foreach($data as $key => $value)
    <table class="print-friendly">
        <tr>
            <td colspan="5">CUST.O/N</td>
            <td>:</td>
            <td colspan="6">
                {{ $value->customer_order_number }}
            </td>
        </tr>
        <tr>
            <td colspan="5">PO NO.</td>
            <td>:</td>
            <td colspan="6">{{ $value->po_number }}</td>
        </tr>
        <tr>
            <td colspan="5">ART.NO.</td>
            <td>:</td>
            <td colspan="6">{{ $value->buyer_item }}</td>
        </tr>
        <tr>
            <td colspan="5">SIZE</td>
            <td>:</td>
            <td colspan="5"> 
                @if(isset($multi_font_size))
                    <span style="font-size: {{$multi_font_size}}px">
                        {{ $value->customer_size }}
                    </span>
                @else
                    {{ $value->customer_size }}
                @endif
            </td>
            <td>
                @if(isset($showbarcode))
                <div class="barcode">
                    <div class="row">
                        <span class="barcode_number">{{ $value->barcode_id }}</span ><span class="rcl_code">{{ $value->recycle==true ? "Recycle" : "" }}</span > 
                    </div>
                    <div class="row">
                        <img class="barcode_img" src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128',2,35) }}" alt="barcode"   />
                    </div>
                    <!-- <div class="img_barcode" style="display:block;padding:0px"> -->
                    
                    <div class="row">
                        @if(strpos($value->manufacturing_size,',')===false)
                            <span class="item_code">{{ $value->season }}-{{ $value->upc }}-{{ $value->buyer_item }}-{{ $value->manufacturing_size }}</span >
                        @else
                            <span class="item_code">{{ $value->season }}-{{ $value->upc }}-{{ $value->buyer_item }}</span >
                        @endif
                    </div>
                </div>
                @endif
            </td>
        </tr>
        <tr>
            <td colspan="5">QTY.</td>
            <td>:</td>
            <td colspan="3">{{ $value->inner_pack }}</td>
            <td colspan="3" style="text-align:left">{{ $value->remark }}</td>
        </tr>
        <tr>
            <td colspan="5">MADE IN</td>
            <td>:</td>
            <td colspan="6" style="text-transform:uppercase">{{ $value->made_in }}</td>
        </tr>
        <tr>
            <td colspan="5">G.W.</td>
            <td>:</td>
            <td colspan="3">{{ $value->gross }}</td>
            <td colspan="3" style="text-transform:uppercase">KGS</td>
        </tr>
        <tr>
            <td colspan="5">N.W.</td>
            <td>:</td>
            <td colspan="3">{{ $value->net }}</td>
            <td colspan="3" style="text-transform:uppercase">KGS</td>
        </tr>
        <!-- <tr></tr> -->
    </table>

    @if(($key + 1) != count($data))
    <div class="page-break"></div>
    @endif
    @endforeach
@else
<table style="width:100%;">
    <tr>
        <td>CUST.O/N :</td>
    </tr>
    <tr>
        <td>PO NO :</td>
    </tr>
    <tr>
        <td>ART.NO. :</td>
    </tr>
    <tr>
        <td>SIZE :</td>
    </tr>
    <tr>
        <td>QTY. :</td>
    </tr>
    <tr>
        <td>MADE IN :</td>
    </tr>
    <tr>
        <td>G.W.</td>
    </tr>
    <tr>
        <td>N.W.</td>
    </tr>
</table>
@endif
</div>
</script>
