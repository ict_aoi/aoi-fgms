<style type="text/css">

@page {
    margin: 10 50 -3 50;
}

@font-face {
    font-family: 'impact' !important;
    src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
}

.template {
   font-size: 40 !important;
   font-family: 'impact';
}

.page-break {
    page-break-after: always;
}

.print-friendly {
    height: 15%;
    width: 100%;
    font-family: sans-serif;
    font-size: 12px !important;
    padding-top: 11px;
}

.print-friendly-single {
    height: 50px;
    width: 100%;
    font-family: sans-serif;
    font-size: 12px !important;

}

/*table.print-friendly tr td, table.print-friendly tr th,
table.print-friendly-single tr td, table.print-friendly-single tr th, {
    page-break-inside: avoid;
    padding: 10px;
}*/

/*table{
    padding-top: 10px;
}*/

tr,td{
    /*border : 1px solid black;*/
    padding-bottom: 55px;
    
}


.barcode {
    line-height: 16px;
}

.img_barcode {
    display: block;
    padding: 0px;
}

.img_barcode > img {
    width: 100px;
    height:25px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 14px !important;
}

.area_barcode {
    width: 33%;
}



</style>

@if(isset($size_template))
    @if($size_template == 'l')
    <div class="template" style="font-size: 40 !important">
    @elseif($size_template == 'm')
    <div class="template" style="font-size: 30 !important">
    @elseif($size_template == 's')
    <div class="template" style="font-size: 20 !important">
    @else
    <div class="template" style="font-size: 40 !important">
    @endif
@else
    <div class="template" style="font-size: 40 !important">
@endif

@if(isset($data))
    @if(count($data) == 1)
    <table class="print-friendly-single" style="width: 5%;">
        <tr>
            <td>
                <p>PO   : {{ isset($data[0]) ? $data[0]->po_number : null }}</p>
                <p>SIZE : {{ isset($data[0]) ? $data[0]->customer_size : null }}</p>
                <p>STYLE : {{ isset($data[0]) ? $data[0]->upc : null }}</p>
                <p>ART.NO : {{ isset($data[0]) ? $data[0]->buyer_item : null }}</p>
                @if(strpos($data[0]->customer_size, '/') === false)
                    <p>QTY  : {{ isset($data[0]) ? $data[0]->inner_pack : null }} {{ isset($data[0]) ? $data[0]->remark : null }}</p>
                @endif
                <!-- <div class="barcode" style="margin-top:0px; margin-bottom:10px;line-height:12px"> -->
                <div class="barcode">
                    <div class="img_barcode">
                        <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data[0]->scan_id, 'C128',2,35) }}" alt="barcode"   />
                    </div>
                    <div class="row">
                        <span class="barcode_number">{{ isset($data[0]) ? $data[0]->scan_id : null }}</span >
                    </div>
                    <div class="row">
                        @if(strpos($data[0]->manufacturing_size,',')===false)
                            <span class="item_code">{{ $data[0]->season }}-{{ $data[0]->upc }}-{{ $data[0]->buyer_item }}-{{ $data[0]->manufacturing_size }}</span >
                        @else
                            <span class="item_code">{{ $data[0]->season }}-{{ $data[0]->upc }}-{{ $data[0]->buyer_item }}</span >
                        @endif
                    </div>
                    
                </div>
            </td>
        </tr>
    </table>
    @else
    <table class="print-friendly">
        @php($chunk = array_chunk($data->toArray(), 3))
        @foreach($chunk as $key => $value)
        
            @if(isset($showbarcode))
            <tr>
                <td class="area_barcode">
                    <center>
                        @if(isset($value[0]))
                        <label>{{ isset($value[0]) ? $value[0]->plan_ref_number : null }} / {{ isset($value[0]) ? $value[0]->po_number : null }}</label><br>
                        <label>{{ isset($value[0]) ? $value[0]->package_from : null }} - {{ isset($value[0]) ? $value[0]->package_to : null }} / {{ isset($value[0]) ? $value[0]->manufacturing_size : null }}</label>
                        <!-- <div class="barcode" style="margin-top:0px; margin-bottom:10px;line-height:12px"> -->
                        <div class="barcode">
                            <!-- <div class="img_barcode" style="display:block;padding:0px"> -->
                            <div class="img_barcode">
                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[0]->scan_id, 'C128',2,35) }}" alt="barcode"   />
                            </div>
                            <div class="row">
                                <span class="barcode_number">{{ isset($value[0]) ? $value[0]->scan_id : null }}</span >
                            </div>
                            @endif
                        </div>
                    </center>
                </td>
                <td class="area_barcode">
                    <center>
                        @if(isset($value[1]))
                        <label>{{ isset($value[1]) ? $value[1]->plan_ref_number : null }} / {{ isset($value[1]) ? $value[1]->po_number : null }}</label><br>
                        <label>{{ isset($value[1]) ? $value[1]->package_from : null }} - {{ isset($value[1]) ? $value[1]->package_to : null }} / {{ isset($value[1]) ? $value[1]->manufacturing_size : null }}</label>
                        <!-- <div class="barcode" style="margin-top:0px; margin-bottom:10px;line-height:12px"> -->
                        <div class="barcode">
                            <!-- <div class="img_barcode" style="display:block;padding:0px"> -->
                            <div class="img_barcode">
                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[1]->scan_id, 'C128',2,35) }}" alt="barcode"   />
                            </div>
                            <div class="row">
                                <span class="barcode_number">{{ isset($value[1]) ? $value[1]->scan_id : null }}</span >
                            </div>
                            @endif
                        </div>
                    </center>
                </td>
                <td class="area_barcode">
                    <center>
                        @if(isset($value[2]))
                        <label>{{ isset($value[2]) ? $value[2]->plan_ref_number : null }} / {{ isset($value[2]) ? $value[2]->po_number : null }}</label><br>
                        <label>{{ isset($value[2]) ? $value[2]->package_from : null }} - {{ isset($value[2]) ? $value[2]->package_to : null }} / {{ isset($value[2]) ? $value[2]->manufacturing_size : null }}</label>
                        <!-- <div class="barcode" style="margin-top:0px; margin-bottom:10px;line-height:12px"> -->
                        <div class="barcode">
                            <!-- <div class="img_barcode" style="display:block;padding:0px"> -->
                            <div class="img_barcode">
                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[2]->scan_id, 'C128',2,35) }}" alt="barcode"   />
                            </div>
                            <div class="row">
                                <span class="barcode_number">{{ isset($value[2]) ? $value[2]->scan_id : null }}</span >
                            </div>
                            @endif
                        </div>
                    </center>
                </td>
            </tr>
            @endif
        
        @endforeach
    </table>
    @endif
@else
<table style="width:100%;">
    <tr>
        <td colspan="8">*ONLY BARCODE</td>
        <td></td>
    </tr>
</table>
@endif
</div>
