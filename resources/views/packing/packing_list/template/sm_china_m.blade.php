<style type="text/css">

@page {
    margin: 15 20 0 20;
}

@font-face {
    font-family: 'impact' !important;
    src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
}

.template {
   font-size: 20 !important;
   font-family: 'impact';
   margin-bottom: 0px;
}

.page-break {
    page-break-after: always;
}

.separator {
    margin: 35 0 0 0;
}


.print-friendly {
    line-height: 25px;
    width: 100%;
}

table.print-friendly tr td, table.print-friendly tr th,
table.print-friendly-single tr td, table.print-friendly-single tr th, {
    page-break-inside: avoid;
}

.barcode {
    float: right;
    margin-bottom:20px;
    line-height: 16px;
    margin-right:-80px;
    transform: rotate(270deg);
}

.barcode > img {
    display: block;
    padding: 0px;
    width: 180px;
    height:30px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 14px !important;
}

.item_code {

    font-family: sans-serif;
    font-size: 10px !important;
}

.rcl_code {
    font-family: sans-serif;
    font-size: 12px !important;
    font-weight: bold;
    padding-left: 2px;
}

</style>

<div class="template" style="font-size: 20 !important">

@if(isset($data))
    @foreach($data as $key => $value)
    <table class="print-friendly">
        <tr>
            <td style="width: 53%">CARTON NO./TOTAL CARTONS</td>
            <td style="width: 2%;">:</td>
            <td colspan="2" style="width: 45%"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; / @if(isset($showpackage)) {{ $value->total_package}} @endif</td>
        </tr>
        <tr>
            <td>PO.NO.</td>
            <td>:</td>
            <td colspan="2">{{ $value->po_number }}</td>
        </tr>
        <tr>
            <td>ART NO.</td>
            <td>:</td>
            <td colspan="2">{{ $value->buyer_item }}</td>
        </tr>
        <tr>
            <td>SIZE</td>
            <td>:</td>
            <td>{{ $value->customer_size }}</td>
            @if(isset($showbarcode))
            <td>
                <div class="barcode">
                    <!-- <div class="img_barcode" style="display:block;padding:0px"> -->
                    <div class="row">
                        @if(strpos($value->manufacturing_size,',')===false)
                            <span class="item_code">{{ $value->season }}-{{ $value->upc }}-{{ $value->buyer_item }}-{{ $value->manufacturing_size }}</span >
                        @else
                            <span class="item_code">{{ $value->season }}-{{ $value->upc }}-{{ $value->buyer_item }}</span > 
                        @endif
                    </div>
                    <div class="img_barcode">
                        <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128',2,35) }}" style="width: 180px; height:30px" alt="barcode"   />
                    </div>
                    <div class="row">
                         <span class="barcode_number">{{ $value->barcode_id }}</span ><span class="rcl_code">{{ $value->recycle==true ? "Recycle" : "" }}</span >
                    </div>
                   
                </div>
            </td>
            @endif
        </tr>
        <tr>
            <td>QTY</td>
            <td>:</td>
            <td style="width: 10%;">{{ $value->inner_pack }}</td>
            <td style="text-align:left;">{{ $value->remark }}</td>
        </tr>
        <tr>
            <td>COUNTRY OF ORIGIN</td>
            <td>:</td>
            <td colspan="2" style="text-transform:uppercase">{{ $value->country_of_origin }}</td>
        </tr>
        <tr>
            <td>MEASUREMENT</td>
            <td>:</td>
            <td colspan="2">{{ $value->length * 1000 }}x{{ $value->width * 1000 }}x{{ $value->height * 1000 }} MM</td>
        </tr>
        <tr>
            <td>GROSS WEIGHT/CARTON WEIGHT</td>
            <td>:</td>
            <td colspan="2">{{ $value->gross }} {{ $value->unit_1 }}/{{ $value->carton_weight }} {{ $value->unit_1 }}</td>
        </tr>

    </table>

    @if(($key + 1) % 3 == 0 && $key != (count($data) - 1))
    <div class="page-break"></div>
    @elseif(($key + 1) % 3 != 0)
    <div class="separator"></div>
    @endif
    @endforeach
@else
<table style="width:100%;">
    <tr>
        <td>CARTON NO./TOTAL CARTONS : - / -</td>
    </tr>
    <tr>
        <td>PO.NO. :</td>
    </tr>
    <tr>
        <td>ART NO. :</td>
    </tr>
    <tr>
        <td>SIZE :</td>
    </tr>
    <tr>
        <td>QTY :</td>
    </tr>
    <tr>
        <td>COUNTRY OF ORIGIN :</td>
    </tr>
    <tr>
        <td>MEASUREMENT :</td>
    </tr>
    <tr>
        <td>GROSS WEIGHT/CARTON WEIGHT : -/-</td>
    </tr>
</table>
@endif
</div>
