<style type="text/css">
    @page {
        margin: 0 30 0 20;
    }


    @font-face {
        font-family: 'impact' !important;
        src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
    }

    .template {
        /*font-size: 54 !important;*/
        font-family: 'impact';
    }

    .page-break {
        page-break-after: always;
    }

    .template {
        font-size: 34 !important;
        font-family: 'impact';
    }

    .page-break {
        page-break-after: always;
    }

    .print-friendly {
        line-height: 62px;
        width: 100%;
    }

    table.print-friendly tr td,
    table.print-friendly tr th,
    table.print-friendly-single tr td,
    table.print-friendly-single tr th,
    {
    page-break-inside: avoid;
    }

    .barcode {
        /* position: absolute;
        top:39%; */
        width: 130px;
        height: 30px;
        /* left: 86%;
        transform: translate(-50, -50);
    transform: rotate(270deg); */

    }

    .img_barcode {
        display: block;
    }

    .img_barcode>img {
        width: 180px !important;
        height: 30px;
    }

    .barcode_number {
        font-family: sans-serif;
        font-size: 12.5px !important;
    }

    .item_code {
        /* position: absolute; */
        /* top:39%; */
        width: 180px;
        height: 30px;
        /* left: 82%; */
        /* transform: translate(-50, -50); */
        /* transform: rotate(270deg); */
        font-family: sans-serif;
        font-size: 12px !important;
    }

    .div_barcode {
        /* position: absolute;
        top: 15%;
        left: 84%;
        transform: translate(-50, -50);
        transform: rotate(270deg); */
        /* line-height: 1.5;    */
        /* position: absolute; */
        transform: rotate(270deg);
        /* margin-top: 5%; */
        margin-right: -10%;

    }

    .rcl_code {
        font-family: sans-serif;
        font-size: 11px !important;
        font-weight: bold;
        padding-left: 2px;
    }

    .po_number {
        font-size: 12px !important;
        font-family: sans-serif;
        line-height: 1.25;
        padding-top: -100px;
    }
</style>

<div class="template" style="font-size: 18 !important; line-height: 0.7cm;line-width:0.2cm">

    @if (isset($data))
        @foreach ($data as $key => $value)
            <div style="page-break-inside: avoid !important; max-height:20%;margin-top:20px;">
                <div style="display:flex">

                    <div>
                        <table class=""
                            style="font-family:Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;max-width:70%">
                            <tr>
                                <td width="15%">G.W</td>
                                <td width="1%">
                                    <center> : </center>
                                </td>
                                <td><label style="margin-right: 55px">{{ $value->gross }}</label> {{ $value->unit_1 }}
                                </td>
                            </tr>
                            <tr>
                                <td>N.W</td>
                                <td>
                                    <center> : </center>
                                </td>
                                <td><label style="margin-right: 55px">{{ $value->net }}</label>{{ $value->unit_1 }}
                                </td>
                            </tr>

                            <tr>
                                <td>CARTON NO</td>
                                <td>
                                    <center> : </center>
                                </td>
                                <td>{{ $value->package_number }} <label style="margin-left: 70px;margin-right:70px"> OF
                                    </label>
                                    {{ $value->total_package }}</td>
                            </tr>

                        </table>
                        MEAS : {{ $value->length * 1000 }} <label style="margin-left: 20px;margin-right: 20px">X</label>
                        {{ $value->width * 1000 }} <label style="margin-left: 20px;margin-right: 20px">X</label>
                        {{ $value->height * 1000 }} <label style="margin-left: 35px;">MM</label>

                    </div>
                    <div class="text-right" style="float: right;padding-top:40px">
                        @if (isset($showbarcode))
                            <div class="div_barcode">
                                <span class="item_code">{{ $value->barcode_id }}</span><br>
                                <img class="barcode"
                                    src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128', 2, 35) }}" /><br>
                                <span class="po_number">{{ $value->po_number }}-{{$value->customer_size}}-{{$value->inner_pack}}</span>

                            </div>
                        @endif

                    </div>
                </div>


            </div>



            {{-- @if ($key + 1 != count($data))
                <div class="page-break"></div>
            @endif --}}
        @endforeach
    @else
        <table style="width:100%;">
            <tr>
                <td>Country of Origin :</td>
            </tr>
            <tr>
                <td>Gross Weight :</td>
            </tr>
            <tr>
                <td>PO Number :</td>
            </tr>
            <tr>
                <td>QTY :</td>
            </tr>
            <tr>
                <td>Size :</td>
            </tr>
        </table>
    @endif
</div>
