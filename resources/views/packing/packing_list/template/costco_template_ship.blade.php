
<style type="text/css">
@page{
    width: 210mm;
    height: 297mm;
    margin: 20 20 20 20;


}
table.tableup{

    font-size: 16px;
    font-weight: bold;
    font-family: 'impact' !important;
    src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
    
}

.page-break {
    page-break-after: always;
}

.barcode > img {
    width: 250px;
    height:45px;
}



.barcode {
    line-height: 16px;
}

.barcode_number {
     font-family: sans-serif;
    font-size: 14px !important; 
}

table.tabelcol  {
    
    page-break-inside: avoid;
    padding-bottom: 20px;
    padding-top: 10px;
}
.item_code {

    font-family: sans-serif;
    font-size: 10px !important;
}

</style>

    @if(isset($data))
        @if(count($data) == 1)
            <table class="tablecol" align="left" >
                <tr>
                    <td style="padding-left: 50px;">
                        <div class="row">
                            <table width="250" class="tableup" >
                                <tr>
                                    <td colspan="1">PO</td>
                                    <td >:</td>
                                    <td colspan="4">{{ $data[0]->po_number }}</td>
                                </tr>
                                <tr>
                                    <td colspan="1">SIZE</td>
                                    <td >:</td>
                                    <td colspan="4">
                                        <label style="
                                            @if(strlen($data[0]->customer_size)<=10)
                                                font-size: 16px;
                                            @else
                                                flex-shrink: 1;
                                                line-height: 0.9;
                                              
                                            @endif
                                        ">{{$data[0]->customer_size}}</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1">QTY</td>
                                    <td >:</td>
                                    <td colspan="1">{{ $data[0]->inner_pack }} </td>
                                    <td colspan="1">{{ isset($data[0]) ? $data[0]->remark : null }}</td>           
                                </tr>
                            </table>
                        </div>

                        <div class="row">
                            <div class="barcode">
                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data[0]->barcode_id, 'C128',2,35) }}" alt="barcode"   />
                            </div>
                            <div class="barcode_number">
                                <div class="row">
                                    <label>{{ isset($data[0]) ? $data[0]->barcode_id : null }}</label>
                                </div>
                                <div class="row">
                                    @if(strpos($data[0]->manufacturing_size,',')===false)
                                        <span class="item_code">{{ $data[0]->season }}-{{ $data[0]->upc }}-{{ $data[0]->buyer_item }}-{{ $data[0]->manufacturing_size }}</span >
                                    @else
                                        <span class="item_code">{{ $data[0]->season }}-{{ $data[0]->upc }}-{{ $data[0]->buyer_item }}</span >
                                    @endif
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>                
        @else
            @php($chunk = array_chunk($data->toArray(), 2))
            @foreach($chunk as $key => $value)            
            <table class="tabelcol" align="center">
                <tr>
                    <td style="padding-right: 50px;">
                        <div class="row">
                            <table width="250" class="tableup" >
                                <tr>
                                    <td colspan="1">PO</td>
                                    <td >:</td>
                                    <td colspan="4">{{ $value[0]->po_number }}</td>
                                </tr>
                                <tr>
                                    <td colspan="1">SIZE</td>
                                    <td >:</td>
                                    <td colspan="4">
                                        <label style="
                                            @if(strlen($value[0]->customer_size)<=10)
                                                font-size: 16px;
                                            @else
                                                flex-shrink: 1;
                                                line-height: 0.9;
                                              
                                            @endif
                                        ">{{$value[0]->customer_size}}</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1">QTY</td>
                                    <td >:</td>
                                    <td colspan="1">{{ $value[0]->inner_pack }} </td>
                                    <td colspan="1">{{ isset($value[0]) ? $value[0]->remark : null }}</td>           
                                </tr>
                            </table>
                        </div>

                        <div class="row">
                            <div class="barcode">
                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[0]->barcode_id, 'C128',2,35) }}" alt="barcode"   />
                            </div>
                            <div class="barcode_number">
                                <div class="row">
                                    <label>{{ isset($value[0]) ? $value[0]->barcode_id : null }}</label>
                                </div>
                                <div class="row">
                                    @if(strpos($value[0]->manufacturing_size,',')===false)
                                        <span class="item_code">{{ $value[0]->season }}-{{ $value[0]->upc }}-{{ $value[0]->buyer_item }}-{{ $value[0]->manufacturing_size }}</span >
                                    @else
                                        <span class="item_code">{{ $value[0]->season }}-{{ $value[0]->upc }}-{{ $value[0]->buyer_item }}</span >
                                    @endif
                                </div>
                            </div>
                        </div>

                    </td>

                    <td style="padding-left: 50px;">
                        <div class="row">
                            <table width="250" class="tableup" >
                                <tr>
                                    <td colspan="1">PO</td>
                                    <td >:</td>
                                    <td colspan="4">{{ $value[1]->po_number }}</td>
                                </tr>
                                <tr>
                                    <td colspan="1">SIZE</td>
                                    <td >:</td>
                                    <td colspan="4">
                                        <label style="
                                            @if(strlen($value[1]->customer_size)<=10)
                                                font-size: 16px;
                                            @else
                                                flex-shrink: 1;
                                                line-height: 0.9;
                                              
                                            @endif
                                        ">{{$value[1]->customer_size}}</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1">QTY</td>
                                    <td >:</td>
                                    <td colspan="1">{{ $value[1]->inner_pack }} </td>
                                    <td colspan="1">{{ isset($value[1]) ? $value[1]->remark : null }}</td>           
                                </tr>
                            </table>
                        </div>

                        <div class="row">
                            <div class="barcode">
                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[1]->barcode_id, 'C128',2,35) }}" alt="barcode"   />
                            </div>
                            <div class="barcode_number">
                               <div class="row">
                                    <label>{{ isset($value[1]) ? $value[1]->barcode_id : null }}</label>
                                </div>
                                <div class="row">
                                    @if(strpos($value[1]->manufacturing_size,',')===false)
                                        <span class="item_code">{{ $value[1]->season }}-{{ $value[1]->upc }}-{{ $value[1]->buyer_item }}-{{ $value[1]->manufacturing_size }}</span >
                                    @else
                                        <span class="item_code">{{ $value[1]->season }}-{{ $value[1]->upc }}-{{ $value[1]->buyer_item }}</span >
                                    @endif
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
                
                
            @endforeach     
        @endif
    @else         
        <table width="250" class="tableup">
            <tr>
                <td colspan="1">PO</td>
                <td >:</td>
                
            </tr>
            <tr>
                <td colspan="1">SIZE</td>
                <td >:</td>
                
            </tr>
            <tr>
                <td colspan="1">QTY</td>
                <td >:</td>
                         
            </tr>
        </table>     
    @endif