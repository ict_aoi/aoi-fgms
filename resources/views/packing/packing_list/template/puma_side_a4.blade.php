<style type="text/css">
    @page {
        margin: 0 30 0 20;
    }


    @font-face {
        font-family: 'impact' !important;
        src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
    }

    .template {
        /*font-size: 54 !important;*/
        font-family: 'impact';
    }

    .page-break {
        page-break-after: always;
    }

    .template {
        font-size: 34 !important;
        font-family: 'impact';
    }

    .page-break {
        page-break-after: always;
    }

    .print-friendly {
        line-height: 62px;
        width: 100%;
    }

    table.print-friendly tr td,
    table.print-friendly tr th,
    table.print-friendly-single tr td,
    table.print-friendly-single tr th,
    {
    page-break-inside: avoid;
    }

    .barcode {
        /* position: absolute;
        top:39%; */
        width: 180px;
        height: 30px;
        /* left: 86%;
        transform: translate(-50, -50);
    transform: rotate(270deg); */

    }

    .img_barcode {
        display: block;
    }

    .img_barcode>img {
        width: 180px !important;
        height: 30px;
    }

    .barcode_number {
        font-family: sans-serif;
        font-size: 12.5px !important;
    }

    .item_code {
        /* position: absolute; */
        /* top:39%; */
        width: 180px;
        height: 30px;
        /* left: 82%; */
        /* transform: translate(-50, -50); */
        /* transform: rotate(270deg); */
        font-family: sans-serif;
        font-size: 14px !important;
    }

    .div_barcode {
        position: absolute;
        top: 25%;
        left: 84%;
        transform: translate(-50, -50);
        transform: rotate(270deg);
        /* line-height: 1.5;    */

    }

    .div_barcode2 {
        position: absolute;
        top: 173%;
        left: 84%;
        transform: translate(-50, -50);
        transform: rotate(270deg);
        line-height: 0.75;

    }

    .div_barcode2 {
        position: absolute;
        top: 160%;
        left: 77%;
        transform: translate(-50, -50);
        transform: rotate(270deg);
        /* line-height: 1.5;    */

    }

    .rcl_code {
        font-family: sans-serif;
        font-size: 11px !important;
        font-weight: bold;
        padding-left: 2px;
    }

    .po_number {
        font-size: 14px !important;
        font-family: sans-serif;
        line-height: 1.25;
    }
</style>

<div class="template" style="font-size: 32 !important; line-height: 1;line-width:0.2cm">

    @if (isset($data))
        @foreach ($data as $key => $value)
            <div
                style="height: 40%; page-break-inside: avoid !important;{{ ($key + 1) % 2 == 0 ? 'margin-top:110px' : 'margin-top:30px' }}">
                CUSTOMER CO NO : {{ $value->customer_order_number }} <br>
                <table class=""
                    style="font-family:Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;width:100%">
                    <tr>
                        <td width="20%">
                            ART. NO
                        </td>

                        <td>: {{ $value->upc }}-{{ $value->buyer_item }}</td>
                    </tr>
                    @php
                        if ($value->rasio == true) {
                            $replace = str_replace('/', ' = ', $value->customer_size);
                            $customer_size = str_replace(',', ' / ', $replace);
                        } else {
                            $customer_size = $value->customer_size;
                        }
                    @endphp
                    <tr>
                        <td>SIZE</td>

                        <td>:
                            @if (isset($multi_font_size))
                                <span style="font-size: {{ $multi_font_size }}px">
                                    {{ $customer_size }}
                                </span>
                            @else
                                {{ $customer_size }}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>QTY</td>

                        <td><label style="margin-right: 75px">: {{ $value->inner_pack }}</label> {{ $value->remark }}
                        </td>
                    </tr>
                    <tr>
                        <td>G.W</td>

                        <td>: <label style="margin-right: 55px">{{ $value->gross }}</label> {{ $value->unit_1 }}</td>
                    </tr>
                    <tr>
                        <td>N.W</td>

                        <td>: <label style="margin-right: 55px">{{ $value->net }}</label>{{ $value->unit_1 }}</td>
                    </tr>
                    <tr>
                        <td>MEAS</td>
                        <td colspan="3">: {{ $value->length * 100 }} <label
                                style="margin-left: 20px;margin-right: 20px">X</label>
                            {{ $value->width * 100 }} <label style="margin-left: 20px;margin-right: 20px">X</label>
                            {{ $value->height * 100 }} <label style="margin-left: 30px;">CM</label></td>
                    </tr>
                </table>
                @if (isset($showbarcode))
                    <div class="{{ ($key + 1) % 2 == 0 ? 'div_barcode2' : 'div_barcode' }}">
                        <span class="item_code">{{ $value->barcode_id }}</span><br>
                        <img class="barcode"
                            src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128', 2, 35) }}" />
                        <label class="po_number">{{ $value->po_number }}</label>

                    </div>
                @endif



                {{-- @if ($key + 1 != count($data))
                <div class="page-break"></div>
            @endif --}}
            </div>
        @endforeach
    @else
        <table style="width:100%;">
            <tr>
                <td>Country of Origin :</td>
            </tr>
            <tr>
                <td>Gross Weight :</td>
            </tr>
            <tr>
                <td>PO Number :</td>
            </tr>
            <tr>
                <td>QTY :</td>
            </tr>
            <tr>
                <td>Size :</td>
            </tr>
        </table>
    @endif
</div>
