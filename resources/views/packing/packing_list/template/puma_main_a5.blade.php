<style type="text/css">
    @page {
        margin: 30 30 0 20;
    }


    @font-face {
        font-family: 'impact' !important;
        src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
    }

    .template {
        /*font-size: 54 !important;*/
        font-family: 'impact';
    }

    .page-break {
        page-break-after: always;
    }

    .template {
        font-size: 34 !important;
        font-family: 'impact';
    }

    .page-break {
        page-break-after: always;
    }

    .print-friendly {
        line-height: 62px;
        width: 100%;
    }

    table.print-friendly tr td,
    table.print-friendly tr th,
    table.print-friendly-single tr td,
    table.print-friendly-single tr th,
    {
    page-break-inside: avoid;
    }

    .barcode {
        /* position: absolute;
        top:39%; */
        width: 180px;
        height: 30px;
        /* left: 86%;
        transform: translate(-50, -50);
    transform: rotate(270deg); */

    }

    .img_barcode {
        display: block;
    }

    .img_barcode>img {
        width: 180px !important;
        height: 30px;
    }

    .barcode_number {
        font-family: sans-serif;
        font-size: 12.5px !important;
    }

    .item_code {
        /* position: absolute; */
        /* top:39%; */
        width: 180px;
        height: 30px;
        /* left: 82%; */
        /* transform: translate(-50, -50); */
        /* transform: rotate(270deg); */
        font-family: sans-serif;
        font-size: 14px !important;
    }

    .div_barcode {
        position: absolute;
        top: 39%;
        left: 84%;
        transform: translate(-50, -50);
        transform: rotate(270deg);
        /* line-height: 1.5;    */

    }

    .rcl_code {
        font-family: sans-serif;
        font-size: 11px !important;
        font-weight: bold;
        padding-left: 2px;
    }

    .po_number {
        font-size: 14px !important;
        font-family: sans-serif;
        line-height: 1.25;
    }
</style>

{{-- @if (isset($size_template))
    @if ($size_template == 'l')
        <div class="template" style="font-size: 54 !important">
        @elseif($size_template == 'm')
            <div class="template" style="font-size: 40 !important">
            @elseif($size_template == 's')
                <div class="template" style="font-size: 30 !important">
                @else
                    <div class="template" style="font-size: 48 !important">
    @endif
@else
    <div class="template" style="font-size: 10 !important"> --}}
{{-- @endif --}}
<div class="template" style="font-size: 27 !important; line-height: 1;line-width:0.2cm">

    @if (isset($data))
        @foreach ($data as $key => $value)
            {{ $value->sku }} <br>
            {{ $value->gps_three_digit_size }}<br>
            <table class=""
                style="font-family:Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;width:100%">
                <tr>
                    <td>CUSTOMER CO NO</td>
                    <td>
                        <center> : </center>
                    </td>
                    <td>{{ $value->customer_order_number }} </td>
                </tr>
                <tr>
                    <td>
                        ART. NO
                    </td>
                    <td>
                        <center> : </center>
                    </td>
                    <td>{{ $value->upc }}-{{ $value->buyer_item }}</td>
                </tr>
                <tr>
                    <td>ART. NAME</td>
                    <td>
                        <center> : </center>
                    </td>
                    <td>
                        @if (isset($multi_font_size))
                            <label style="font-size: {{ $multi_font_size }}px !important">
                                {{ strtoupper($value->model_number) }}
                            </label>
                        @else
                            {{ strtoupper($value->model_number) }}
                        @endif
                    </td>
                </tr>
                @php
                    if ($value->rasio == true) {
                        $replace = str_replace('/', ' = ', $value->customer_size);
                        $customer_size = str_replace(',', ' / ', $replace);
                    } else {
                        $customer_size = $value->customer_size;
                    }
                @endphp
                <tr>
                    <td>SIZE</td>
                    <td>
                        <center> : </center>
                    </td>
                    <td>
                        @if (isset($multi_font_size))
                            <span style="font-size: {{ $multi_font_size }}px">
                                {{ $customer_size }}
                            </span>
                        @else
                            {{ $customer_size }}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>QUANTITY</td>
                    <td>
                        <center> : </center>
                    </td>
                    <td><label style="margin-right: 75px">{{ $value->inner_pack }}</label> {{ $value->remark }}</td>
                </tr>
                <tr>
                    <td>CARTON NO</td>
                    <td>
                        <center> : </center>
                    </td>
                    <td>{{ $value->package_number }} <label style="margin-left: 90px;margin-right:90px"> OF </label>
                        {{ $value->total_package }}</td>
                </tr>
                <tr>
                    <td>MADE IN</td>
                    <td>
                        <center> : </center>
                    </td>
                    <td>INDONESIA</td>
                </tr>
            </table>
            @if (isset($showbarcode))
                <div class="div_barcode">
                    <span class="item_code">{{ $value->barcode_id }}</span><br>
                    <img class="barcode"
                        src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128', 2, 35) }}" />
                    <label class="po_number">{{ $value->po_number }}</label>

                </div>
            @endif


            @if ($key + 1 != count($data))
                <div class="page-break"></div>
            @endif
        @endforeach
    @else
        <table style="width:100%;">
            <tr>
                <td>Country of Origin :</td>
            </tr>
            <tr>
                <td>Gross Weight :</td>
            </tr>
            <tr>
                <td>PO Number :</td>
            </tr>
            <tr>
                <td>QTY :</td>
            </tr>
            <tr>
                <td>Size :</td>
            </tr>
        </table>
    @endif
</div>
