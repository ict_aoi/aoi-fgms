<style type="text/css">

@page {
    margin: 20 20 0 40;
}

@font-face {
    font-family: 'impact' !important;
    src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
}

.template {
   font-size: 32 !important;
   font-family: 'impact';
}

.page-break {
    page-break-after: always;
}

.print-friendly {
    height: 30%;
}

.print-friendly-single {
    height: 30%;
    width: 50%;
}

table.print-friendly tr td, table.print-friendly tr th,
table.print-friendly-single tr td, table.print-friendly-single tr th, {
    page-break-inside: avoid;
}

.ship_to {
    width: 80px;
    line-height: 25px;
    vertical-align: top;
}

.ship_to_value {
    vertical-align: top;
    line-height: 25px;
    text-align: left;
    padding-right: 20px;
}

.country_of_origin {
    line-height: 45px;
}

.barcode {
    margin-top: 20px;
    margin-bottom:20px;
    line-height: 16px;
}

.img_barcode {
    display: block;
    padding: 0px;
}

.img_barcode > img {
    width: 180px;
    height:30px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 14px !important;
}

.item_code {

    font-family: sans-serif;
    font-size: 10px !important;
}

.rcl_code {
    font-family: sans-serif;
    font-size: 12px !important;
    font-weight: bold;
    padding-left: 2px;
}

</style>

@if(isset($size_template))
    @if($size_template == 'l')
    <div class="template" style="font-size: 17 !important">
    @elseif($size_template == 'm')
    <div class="template" style="font-size: 20 !important">
    @elseif($size_template == 's')
    <div class="template" style="font-size: 10 !important">
    @else
    <div class="template" style="font-size: 34 !important">
    @endif
@else
    <div class="template" style="font-size: 34 !important">
@endif

@if(isset($data))
    @if(count($data) == 1)
    <table class="print-friendly-single">
        <tr>
            <td class="ship_to">Ship to:</td>
            <td class="ship_to_value">{{ ucwords(strtolower($data[0]->address)) }}</td>
        </tr>
        <tr>
            <td colspan='2' class="country_of_origin">
                Country of Origin: Made In {{ ucwords(strtolower($data[0]->country_of_origin)) }}
            </td>
        </tr>
        @if(isset($showbarcode))
        <tr>
            <td colspan="2">
                <!-- <div class="barcode" style="margin-top:0px; margin-bottom:10px;line-height:12px"> -->
                <div class="barcode">
                    <!-- <div class="img_barcode" style="display:block;padding:0px"> -->
                    <div class="row">
                        <span class="barcode_number">{{ $data[0]->barcode_id }}</span ><span class="rcl_code">{{ $data[0]->recycle==true ? "Recycle" : "" }}</span > 
                    </div>
                    <div class="img_barcode">
                        <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data[0]->barcode_id, 'C128',2,35) }}" style="width: 180px; height:30px" alt="barcode"   />
                    </div>
                    <div class="row">
                        @if(strpos($data[0]->manufacturing_size,',')===false)
                            <span class="item_code">{{ $data[0]->season }}-{{ $data[0]->upc }}-{{ $data[0]->buyer_item }}-{{ $data[0]->manufacturing_size }}</span >
                        @else
                            <span class="item_code">{{ $data[0]->season }}-{{ $data[0]->upc }}-{{ $data[0]->buyer_item }}</span >
                        @endif
                    </div>
                </div>
            </td>
        </tr>
        @endif
    </table>
    @else
        @php($chunk = array_chunk($data->toArray(), 2))
        @foreach($chunk as $key => $value)
        <table class="print-friendly">
            <tr>
                <td class="ship_to">Ship to:</td>
                <td class="ship_to_value">{{ isset($value[0]) ? ucwords(strtolower($value[0]->address)) : null }}</td>
                <td class="ship_to">Ship to:</td>
                <td class="ship_to_value">{{ isset($value[1]) ? ucwords(strtolower($value[1]->address)) : null }}</td>
            </tr>
            <tr>
                <td colspan='2' class="country_of_origin">
                    Country of Origin: Made In {{ isset($value[0]) ? ucwords(strtolower($value[0]->country_of_origin)) : null }}
                </td>
                <td colspan='2' class="country_of_origin">
                    Country of Origin : Made In {{ isset($value[1]) ? ucwords(strtolower($value[1]->country_of_origin)) : null }}
                </td>
            </tr>
            @if(isset($showbarcode))
            <tr>
                <td colspan="2">
                    <!-- <div class="barcode" style="margin-top:0px; margin-bottom:10px;line-height:12px"> -->
                    <div class="barcode">
                        <!-- <div class="img_barcode" style="display:block;padding:0px"> -->
                        <div class="row">
                            <span class="barcode_number">{{ isset($value[0]) ? $value[0]->barcode_id : null }}</span ><span class="rcl_code">{{isset($value[0]->recycle)==true ? "Recycle" : "" }}</span > 
                        </div>
                        @if(isset($value[0]))
                        <div class="img_barcode">
                            <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[0]->barcode_id, 'C128',2,35) }}" style="width: 180px; height:30px" alt="barcode"   />
                        </div>
                        <div class="row">
                            @if(strpos($value[0]->manufacturing_size,',')===false)
                                <span class="item_code">{{ $value[0]->season }}-{{ $value[0]->upc }}-{{ $value[0]->buyer_item }}-{{ $value[0]->manufacturing_size }}</span >
                            @else
                                <span class="item_code">{{ $value[0]->season }}-{{ $value[0]->upc }}-{{ $value[0]->buyer_item }}</span >
                            @endif
                        </div>
                        
                        @endif
                    </div>
                </td>
                <td colspan="2">
                    <!-- <div class="barcode" style="margin-top:0px; margin-bottom:10px;line-height:12px"> -->
                    <div class="barcode">
                        <!-- <div class="img_barcode" style="display:block;padding:0px"> -->
                        <div class="row">
                            <span class="barcode_number">{{ isset($value[1]) ? $value[1]->barcode_id : null }}</span ><span class="rcl_code">{{isset($value[1]->recycle)==true ? "Recycle" : "" }}</span >
                        </div>
                        @if(isset($value[1]))
                        <div class="img_barcode">
                            <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[1]->barcode_id, 'C128',2,35) }}" style="width: 180px; height:30px" alt="barcode"   />
                        </div>
                        <div class="row">
                            @if(strpos($value[1]->manufacturing_size,',')===false)
                                <span class="item_code">{{ $value[1]->season }}-{{ $value[1]->upc }}-{{ $value[1]->buyer_item }}-{{ $value[1]->manufacturing_size }}</span >
                            @else
                                <span class="item_code">{{ $value[1]->season }}-{{ $value[1]->upc }}-{{ $value[1]->buyer_item }}</span >
                            @endif
                        </div>
                        
                        @endif
                    </div>
                </td>
            </tr>
            @endif
        </table>
        @if(($key+1) % 3 == 0 && ($key +1) != count($chunk))
        <div class="page-break"></div>
        @endif
        @endforeach
    @endif
@else
<table style="width:100%;">
    <tr>
        <td>Ship to : </td>
    </tr>
    <tr>
        <td>Country of Origin :</td>
    </tr>
</table>
@endif
</div>
