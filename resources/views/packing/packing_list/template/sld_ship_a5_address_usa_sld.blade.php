<style type="text/css">

@page {
    margin: 0 10 0 20;
}

@font-face {
    font-family: 'impact' !important;
    src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
}

.template {
   font-size: 34 !important;
   font-family: 'impact';
}

.page-break {
    page-break-after: always;
}

.separator {
    margin: 25 0 0 0;
}

.print-friendly {
    line-height: 30px;
}

table.print-friendly tr td, table.print-friendly tr th,
table.print-friendly-single tr td, table.print-friendly-single tr th, {
    page-break-inside: avoid;
}

.barcode {
    float: right;
    margin-bottom:40px;
    line-height: 16px;
}

.img_barcode {
    display: block;
    padding: 0px;
}

.img_barcode > img {
    width: 180px;
    height:30px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 14px !important;
}

.item_code {

    font-family: sans-serif;
    font-size: 10px !important;
}

.rcl_code {
    font-family: sans-serif;
    font-size: 12px !important;
    font-weight: bold;
}

</style>

@if(isset($size_template))
    @if($size_template == 'l')
    <div class="template" style="font-size: 23 !important">
    @elseif($size_template == 'm')
    <div class="template" style="font-size: 20 !important">
    @elseif($size_template == 's')
    <div class="template" style="font-size: 10 !important">
    @else
    <div class="template" style="font-size: 34 !important">
    @endif
@else
    <div class="template" style="font-size: 34 !important">
@endif

@if(isset($data))
    @foreach($data as $key => $value)
    <table class="print-friendly">
        <tr>
            <td colspan="13">SLD Ship-to :</td>
        </tr>
        <tr>
            <td colspan="13" style="text-transform:uppercase"> {{ $value->address }}</td>
        </tr>
        <tr>
            <td colspan="13">- </td>
        </tr>
        <tr>
            <td colspan="13" style="text-transform:uppercase">{{ $value->city}}</td>
        </tr>
        <tr>
            <td colspan="13" style="text-transform:uppercase">{{ $value->postal }}</td>
        </tr>
        <tr>
            <td colspan="8" style="text-transform:uppercase">{{$value->country}}</td>
            <td colspan="5">
            @if(isset($showbarcode))
                <div class="barcode">
                    <div class="row">
                        <span class="barcode_number">{{ $value->barcode_id }}</span > <span class="rcl_code">{{ $value->recycle==true ? "Recycle" : "" }}</span >
                    </div>
                    <div class="img_barcode">
                        <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128',2,35) }}" alt="barcode"   />
                    </div>
                    
                    <div class="row">
                        @if(strpos($value->manufacturing_size,',')===false)
                            <span class="item_code">{{ $value->season }}-{{ $value->upc }}-{{ $value->buyer_item }}-{{ $value->manufacturing_size }}</span >
                        @else
                            <span class="item_code">{{ $value->season }}-{{ $value->upc }}-{{ $value->buyer_item }}</span >
                        @endif
                    </div>
                </div>
            @endif
            </td>
        </tr>
    </table>

    @if($key % 2 == 1 && $key != (count($data) - 1))
    <div class="page-break"></div>
    @elseif($key % 2 != 1)
    <div class="separator"></div>
    @endif
    @endforeach
@else
<table style="width:100%;">
    <tr>
        <td>SLD Ship-to : </td>
    </tr>
</table>
@endif
</div>
