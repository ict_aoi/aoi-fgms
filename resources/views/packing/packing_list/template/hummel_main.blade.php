<style type="text/css">
    @page {
        margin: 25 20 0 20;
    }

    @font-face {
        font-family: 'impact' !important;
        src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
    }

    .template {
        font-size: 36;
        font-family: 'impact';
        margin-bottom: 0px;
    }

    .page-break {
        page-break-after: always;
    }

    .print-friendly {
        line-height: 43px;
        width: 100%;
    }

    table.print-friendly tr td,
    table.print-friendly tr th,
    table.print-friendly-single tr td,
    table.print-friendly-single tr th,
        {
        page-break-inside: avoid;
    }

    /*.barcode {
    float: right;
    margin-bottom:20px;
    line-height: 16px;
    position:fixed;
    top: 22rem;
    left: 35rem;
    transform: rotate(270deg);
}
*/

    .barcode {
            line-height: 16px;
        }

    .barcode_img {
        display: block;
        padding: 0px;
        width: 180px;
        height: 30px;
    }

    .barcode_number {
        font-family: sans-serif;
        font-size: 14px !important;
    }

    .img_barcode {
        display: block;
        padding: 0px;
    }

    .img_barcode>img {
        width: 180px;
        height: 40px;
    }
    .item_code {

        font-family: sans-serif;
        font-size: 10px !important;
    }

    .rcl_code {
        font-family: sans-serif;
        font-size: 12px !important;
        font-weight: bold;
        padding-left: 2px;
    }
</style>

<div class="template">
    @if (isset($data))
        @foreach ($data as $key => $value)
            <table class="print-friendly">
                <tr>
                    <td colspan="5">
                        {{--  @if (isset($font_size_template))
                            <span style="font-size: {{ $font_size_template }}px">
                                PO {{ $value->customer_order_number }}
                            </span>
                        @else
                        PO {{ $value->customer_order_number }}
                        @endif  --}}
                        PO {{ $value->customer_order_number }}

                    </td>
                    <td></td>
                    <td colspan="6">

                    </td>
                </tr>
                <tr>
                    <td colspan="5">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5">{{ $value->service_identifier }}</td>
                    <td></td>
                    <td colspan="6"></td>
                </tr>
                <tr>
                    <td colspan="5">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5">{{ $value->packing_mode }}</td>
                    <td></td>
                    <td colspan="6"></td>
                </tr>
                <tr>
                    <td colspan="5">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5">{{ $value->upc }}</td>
                    <td></td>
                    <td colspan="6">

                    </td>

                </tr>
                <tr>
                    <td colspan="5">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="6">&nbsp;</td>
                </tr>
                @php
                    if ($value->rasio == true) {
                        $replace = str_replace('/', ' = ', $value->customer_size);
                        $customer_size = str_replace(',', ' / ', $replace);
                    } else {
                        $customer_size = $value->customer_size;
                    }
                @endphp
                <tr>
                    <td colspan="5">
                        @if (isset($multi_font_size))
                            <span style="font-size: {{ $multi_font_size }}px">
                                {{ $customer_size }}
                            </span>
                        @else
                            {{ $customer_size }}
                        @endif
                        <div style="float:right">

                            <div class="barcode">
        
                                <div class="img_barcode">
                                    <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128', 2, 35) }}" alt="barcode" />
                                </div>
        
                                <div class="row">
                                    <span class="item_code">{{ isset($value) ? $value->barcode_id : null }}</span>
                                </div>
                            </div>
        
                        </div>
                    </td>
                    <td></td>
                    <td colspan="6">
                        
                    </td>

                </tr>
                {{--  <tr>
                    <td colspan="5">QTY.</td>
                    <td>:</td>
                    <td colspan="3">{{ $value->inner_pack }}</td>
                    <td colspan="3" style="text-align:left">{{ $value->remark }}</td>
                </tr>
                <tr>
                    <td colspan="5">MADE IN</td>
                    <td>:</td>
                    <td colspan="6" style="text-transform:uppercase">{{ $value->made_in }}</td>
                </tr>
                <tr>
                    <td colspan="5">G.W.</td>
                    <td>:</td>
                    <td colspan="3">{{ $value->gross }}</td>
                    <td colspan="3" style="text-transform:uppercase">KGS</td>
                </tr>
                <tr>
                    <td colspan="5">N.W.</td>
                    <td>:</td>
                    <td colspan="3">{{ $value->net }}</td>
                    <td colspan="3" style="text-transform:uppercase">KGS</td>
                </tr>  --}}
                <!-- <tr></tr> -->
            </table>

            @if ($key + 1 != count($data))
                <div class="page-break"></div>
            @endif
        @endforeach
    @else
        <table style="width:100%;">
            <tr>
                <td>PO NO :</td>
            </tr>
            <tr>
                <td>SERVICE IDENTIFIER :</td>
            </tr>
            <tr>
                <td>PACKING MODE :</td>
            </tr>
            <tr>
                <td>STYLE. :</td>
            </tr>
            <tr>
                <td>SIZE :</td>
            </tr>

        </table>
    @endif
</div>
</script>
