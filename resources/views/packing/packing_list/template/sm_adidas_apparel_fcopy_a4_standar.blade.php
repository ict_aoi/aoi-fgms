<style type="text/css">

@page {
    margin: 20 20 0 20;
}

@font-face {
    font-family: 'impact' !important;
    src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
}

.template {
   font-size: 50;
   font-family: 'impact';
   margin-bottom: 0px;
}

.page-break {
    page-break-after: always;
}

.separator {
    margin: 90 0 0 0;
}

.print-friendly {
    padding-top: 45px;
    line-height: 50px;;
    width: 100%;
}

table.print-friendly tr td, table.print-friendly tr th,
table.print-friendly-single tr td, table.print-friendly-single tr th, {
    page-break-inside: avoid;
}

/* .barcode {
    float: right;
    margin-bottom:20px;
    line-height: 16px;
    margin-right:-80px;
    transform: rotate(270deg);
}

.barcode > img {
    display: block;
    padding: 0px;
    width: 180px;
    height:30px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 14px !important;
} */

.barcode {
    float: right;
    margin-bottom:20px;
    line-height: 16px;
    margin-right:-40px;
    transform: rotate(270deg);
    top: 8px;
    position: relative;
}

.barcode > img {
    display: block;
    padding: 0px;
    width: 180px !important;
    height:30px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 13px !important;
}

.item_code {

    font-family: sans-serif;
    font-size: 10px !important;
}

.grade{
    float: right;
    margin-right: 25px;
    font-size: 50;
   font-family: 'impact';
}

.rcl_code {
    font-family: sans-serif;
    font-size: 11px !important;
    font-weight: bold;
    padding-left: 2px;
}

</style>

<div class="template">
@if(isset($data))
    @foreach($data as $key => $value)
    <table class="print-friendly">
        <tr>
            @if(isset($isbgrade))
                <td colspan="5">CUST.O/N</td>
                <td>:</td>
                <td colspan="5">
                    @if(isset($font_size_template))
                        <span style="font-size: {{$font_size_template}}px">
                            {{ $value->customer_order_number }}
                        </span>
                    @else
                        {{ $value->customer_order_number }}
                    @endif
                </td>
                <td>
                    <div class="grade">
                        B
                    </div>
                </td>
            @else
                <td colspan="5">CUST.O/N</td>
                <td>:</td>
                <td colspan="6">
                    @if(isset($font_size_template))
                        <span style="font-size: {{$font_size_template}}px">
                            {{ $value->customer_order_number }}
                        </span>
                    @else
                        {{ $value->customer_order_number }}
                    @endif
                </td>
            @endif
        </tr>
        <tr>
            <td colspan="5">PO NO</td>
            <td>:</td>
            <td colspan="6">{{ $value->po_number }}</td>
        </tr>
        <tr>
            <td colspan="5">ART.NO.</td>
            <td>:</td>
            <td colspan="6">{{ $value->buyer_item }}</td>
        </tr>
        <tr>
            <td colspan="5">SIZE</td>
            <td>:</td>
            <td colspan="6">
                @if(isset($multi_font_size))
                    <span style="font-size: {{$multi_font_size}}px">
                        {{ $value->customer_size }}
                    </span>
                @else
                    {{ $value->customer_size }}
                @endif
            </td>
            <!-- <td>

            </td> -->
        </tr>
        <tr>
            <td colspan="5">QTY</td>
            <td>:</td>
            <td colspan="3">{{ $value->inner_pack }}</td>
            <td colspan="2" style="text-align:left">{{ $value->remark }}</td>
            <td>
                @if(isset($showbarcode))
                <div class="barcode">
                    <div class="row">
                        <span class="barcode_number">{{ $value->barcode_id }}</span><span class="rcl_code">{{ $value->recycle==true ? "Recycle" : "" }}</span > 
                    </div>
                    <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128',2,35) }}" alt="barcode"   />
                    <div class="row">
                    @if(strpos($value->manufacturing_size,',')===false)
                        <span class="item_code">{{ $value->season }}-{{ $value->upc }}-{{ $value->buyer_item }}-{{ $value->manufacturing_size }}</span >
                    @else
                        <span class="item_code">{{ $value->season }}-{{ $value->upc }}-{{ $value->buyer_item }}</span >
                    @endif
                </div>
                </div>
                @endif
            </td>
        </tr>
        <tr>
            <td colspan="5">MADE IN</td>
            <td>:</td>
            <td colspan="6" style="text-transform:uppercase">{{ $value->made_in }}</td>
        </tr>
        <!-- <tr></tr> -->
    </table>

    @if(($key + 1) % 2 == 0 && $key != (count($data) - 1))
    <div class="page-break"></div>
    @elseif(($key + 1) % 2 != 0)
    <div class="separator"></div>
    @endif
    @endforeach
@else
<table style="width:100%;">
    <tr>
        <td>CUST.O/N :</td>
    </tr>
    <tr>
        <td>PO NO :</td>
    </tr>
    <tr>
        <td>ART.NO. :</td>
    </tr>
    <tr>
        <td>SIZE :</td>
    </tr>
    <tr>
        <td>QTY :</td>
    </tr>
    <tr>
        <td>MADE IN :</td>
    </tr>
</table>
@endif
</div>
</script>
