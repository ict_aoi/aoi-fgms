<style type="text/css">

@page {
    margin: 20 20 0 20;
}

@font-face {
    font-family: 'impact' !important;
    src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
}

.template {
   /*font-size: 36 !important;*/
   font-family: 'impact';
}

.page-break {
    page-break-after: always;
}

.print-friendly {
    line-height: 43px;
    width: 100%;
}

table.print-friendly tr td, table.print-friendly tr th,
table.print-friendly-single tr td, table.print-friendly-single tr th, {
    page-break-inside: avoid;
}

.barcode {
    float: right;
    margin-bottom:20px;
    line-height: 16px;
    position:fixed;
    bottom: 10rem;
    margin-right:-75px;
    transform: rotate(270deg);
}

.barcode > img {
    display: block;
    padding: 0px;
    width: 180px;
    height:30px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 14px !important;
}

.item_code {

    font-family: sans-serif;
    font-size: 10px !important;
}

.rcl_code {
    font-family: sans-serif;
    font-size: 12px !important;
    font-weight: bold;
    padding-left: 2px;
}

</style>

@if(isset($size_template))
    @if($size_template == 'l')
    <div class="template" style="font-size: 35 !important">
    @elseif($size_template == 'm')
    <div class="template" style="font-size: 30 !important">
    @elseif($size_template == 's')
    <div class="template" style="font-size: 20 !important">
    @else
    <div class="template" style="font-size: 40 !important">
    @endif
@else
    <div class="template" style="font-size: 40 !important">
@endif

@if(isset($data))
    @foreach($data as $key => $value)
    <table class="print-friendly">
        <tr>
            <td style="width: 50%">CUST.O/N</td>
            <td>:</td>
            <td colspan="2" style="width: 50%"> {{ $value->customer_order_number }}</td>
        </tr>
        <tr>
            <td>PO NO</td>
            <td>:</td>
            <td colspan="2">{{ $value->po_number }}</td>
        </tr>
        <tr>
            <td>ART NO</td>
            <td>:</td>
            <td colspan="2">{{ $value->buyer_item }}</td>
        </tr>
        <tr>
            <td>SIZE</td>
            <td>:</td>
            <td colspan="2">{{ $value->customer_size }}</td>
        </tr>
        <tr>
            <td>QTY</td>
            <td>:</td>
            <td>{{ $value->inner_pack }}</td>
            <td style="text-align:left;">{{ $value->remark }}</td>
        </tr>
        <tr>
            <td>MADE IN</td>
            <td>:</td>
            <td colspan="2" style="text-transform:uppercase">{{ $value->made_in }}</td>
        </tr>
        <tr>
            <td>TRADEMARK</td>
            <td>:</td>
            <td colspan="2" style="text-transform:uppercase">{{ $value->trademark }}</td>
        </tr>
        <tr>
            <td>COMMODITIES TYPE</td>
            <td>:</td>
            <td style="text-transform:uppercase">{{ $value->commodities_type }}</td>
            @if(isset($showbarcode))
            <td>
                <div class="barcode">
                    <!-- <div class="img_barcode" style="display:block;padding:0px"> -->
                    <div class="row">
                    
                        <span class="barcode_number">{{ $value->barcode_id }}</span ><span class="rcl_code">{{ $value->recycle==true ? "Recycle" : "" }}</span >
                    </div>
                    <div class="img_barcode">
                        <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128',2,35) }}" style="width: 180px; height:30px" alt="barcode"   />
                    </div>
                    @if(strpos($value->manufacturing_size,',')===false)
                        <span class="item_code">{{ $value->season }}-{{ $value->upc }}-{{ $value->buyer_item }}-{{ $value->manufacturing_size }}</span >
                    @else
                        <span class="item_code">{{ $value->season }}-{{ $value->upc }}-{{ $value->buyer_item }}</span >
                    @endif
                </div>
            </td>
            @endif
        </tr>
        <!-- <tr></tr> -->
    </table>

    @if($key != (count($data) - 1))
    <div class="page-break"></div>
    @endif
    @endforeach
@else
<table style="width:100%;">
    <tr>
        <td>CUST.O/N : </td>
    </tr>
    <tr>
        <td>PO NO :</td>
    </tr>
    <tr>
        <td>ART NO :</td>
    </tr>
    <tr>
        <td>SIZE :</td>
    </tr>
    <tr>
        <td>QTY :</td>
    </tr>
    <tr>
        <td>MADE IN :</td>
    </tr>
    <tr>
        <td>TRADEMARK :</td>
    </tr>
    <tr>
        <td>COMMODITIES TYPE : -/-</td>
    </tr>
</table>
@endif
</div>
