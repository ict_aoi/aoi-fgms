<style type="text/css">
    @page {
        margin: 15 20 0 15;
    }


    .template {
        font-size: 36;
        margin-bottom: 0px;
        font-family: Arial, Helvetica, sans-serif;
        font-weight: 600;
    }

    .page-break {
        page-break-after: always;
    }

    .print-friendly {
        line-height: 43px;
        width: 100%;
    }

    table.print-friendly tr td,
    table.print-friendly tr th,
    table.print-friendly-single tr td,
    table.print-friendly-single tr th {
        page-break-inside: avoid;
    }

    /*.barcode {
    float: right;
    margin-bottom:20px;
    line-height: 16px;
    position:fixed;
    top: 22rem;
    left: 35rem;
    transform: rotate(270deg);
}
*/

    .barcode {
        line-height: 16px;
    }

    .barcode_img {
        display: block;
        padding: 0px;
        width: 180px;
        height: 30px;
    }

    .barcode_number {
        font-family: sans-serif;
        font-size: 14px !important;
    }

    .img_barcode {
        display: block;
        padding: 0px;
    }

    .img_barcode>img {
        width: 180px;
        height: 30px;
    }

    .item_code {

        font-family: sans-serif;
        font-size: 13px !important;
        font-weight: bold;
    }

    .rcl_code {
        font-family: sans-serif;
        font-size: 12px !important;
        font-weight: bold;
        padding-left: 2px;
    }

    .border_top {
        border-top: 1px solid black;
    }

    .border_bottom {
        border-bottom: 1px solid black;
    }

    .padding-table {
        padding: 0.65cm 0 0.65cm 0;
    }

    .padding-table_multisize {
        padding: 0.75cm 0 0.65cm 0;
    }
</style>

<div class="template" style="margin:0">
    @if (isset($data))
    @foreach ($data as $key => $value)
    <table class="print-friendly">
        @php
        $explode = explode(' ', $value->bp_name);
        $bp_name = strtoupper($explode[0]);
        @endphp
        <tr>
            <td colspan="12" class="border_top border_bottom padding-table">{{ $bp_name }}</td>

        </tr>
        <tr>
            <td colspan="12" class="border_bottom padding-table">
                {{-- @if (isset($font_size_template))
                            <span style="font-size: {{ $font_size_template }}px">
                PO {{ $value->customer_order_number }}
                </span>
                @else
                PO {{ $value->customer_order_number }}
                @endif --}}
                PO {{ $value->customer_order_number }}
            </td>

        </tr>
        <tr>
            <td colspan="12" class="border_bottom padding-table">{{ $value->buyer_item }}</td>

        </tr>
        @php
        if ($value->rasio == true) {
        $replace = str_replace('/', ' = ', $value->customer_size);
        $customer_size = str_replace(',', ' / ', $replace);
        } else {
        $customer_size = $value->customer_size;
        }
        @endphp
        <tr>
            <td colspan="12" class="border_bottom padding-table_multisize" style="padding:0.75cm 0 0.7cm 0">
                @if (isset($multi_font_size) && $multi_font_size != null)
                <p style="font-size: {{ $multi_font_size }}px;margin:0">
                    {{ $customer_size }}
                </p>
                @else
                {{ $customer_size }}
                @endif
            </td>


        </tr>
        <tr>
            <td colspan="12" class="border_bottom padding-table_multisize">{{ $value->inner_pack }}</td>

        </tr>

        <tr>
            <td colspan="12" class="padding-table" style="padding-right: 5%; ">
                {{-- <div style="float:right">

                    <div class="barcode">

                        <div class="img_barcode">
                            <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128', 2, 35) }}" alt="barcode" />
                        </div>

                        <div class="row">
                            <span class="item_code">{{ isset($value) ? $value->barcode_id : null }}</span>
                        </div>
                    </div>

                </div> --}}
            </td>
        </tr>


        {{-- <tr>
                    <td colspan="5">QTY.</td>
                    <td>:</td>
                    <td colspan="3">{{ $value->inner_pack }}</td>
        <td colspan="3" style="text-align:left">{{ $value->remark }}</td>
        </tr>
        <tr>
            <td colspan="5">MADE IN</td>
            <td>:</td>
            <td colspan="6" style="text-transform:uppercase">{{ $value->made_in }}</td>
        </tr>
        <tr>
            <td colspan="5">G.W.</td>
            <td>:</td>
            <td colspan="3">{{ $value->gross }}</td>
            <td colspan="3" style="text-transform:uppercase">KGS</td>
        </tr>
        <tr>
            <td colspan="5">N.W.</td>
            <td>:</td>
            <td colspan="3">{{ $value->net }}</td>
            <td colspan="3" style="text-transform:uppercase">KGS</td>
        </tr> --}}
        <!-- <tr></tr> -->
    </table>

    @if ($key + 1 != count($data))
    <div class="page-break"></div>
    @endif
    @endforeach
    @else
    <table style="width:100%;">
        <tr>
            <td>BP Name :</td>
        </tr>
        <tr>
            <td>PO NO :</td>
        </tr>
        <tr>
            <td>BUYER ITEM :</td>
        </tr>
        <tr>
            <td>SIZE :</td>
        </tr>
        <tr>
            <td>QTY. :</td>
        </tr>
    </table>
    @endif
</div>
<script>
    $.ajax({
        type: 'POST',
        url: $('#url_generate_barcode').attr('href'),
        data: {
            barcodeid: barcodeid
        },
        success: function(response) {
            $('#generate_barcode').html('');
            $('#generate_barcode').append(response);
        },
        error: function(response) {
            myalert('error', 'NOT GOOD');
        }
    })
</script>
</script>