@extends('layouts.app', ['active' => 'detcostco'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">PACKAGE DETAILS RASIO</span> (po number key : #{{$po_number_key}} || plan ref number : #{{$plan_ref_number}})</h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('packing.packingList') }}"><i class="icon-list position-left"></i> Packing List</a></li>
            <li class="active"><a href="#"> Package Details Rasio</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="table_package_detail_rasio">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>PO NUMBER KEY</th>
                        <th>PO NUMBER</th>
                        <th>BUYER</th>
                        <th>SIZE</th>
                        <th>ITEM QTY</th>
                        <th>INNER PACK</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('packing.getCostcoDetail') }}" id="package_detail_rasio" data-pokey='{{ $po_number_key }}' data-planrefnumber = '{{ $plan_ref_number }}'></a>

@endsection



@include('packing/packing_list/_js_detcos')
@section('js')
<script type="text/javascript">
$( document ).ready(function() {
    //datatables
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        stateSave: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
           
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });
    var url = $('#package_detail_rasio').attr('href');
    var planrefnumber = $('#package_detail_rasio').data('planrefnumber');
    var ponumberkey = $('#package_detail_rasio').data('pokey');
    var table = $('#table_package_detail_rasio').DataTable({

        ajax: {
            'url' : url,
            'data': {pokey:ponumberkey, plan_ref_number: planrefnumber}
        },
       
        columns: [
            { data: 'DT_Row_Index', name: 'DT_Row_Index', sortable:false,orderable:false,sortable:false },
            {data: 'po_number_key', name: 'po_number_key'},
            {data: 'po_number', name: 'po_number'},
            {data: 'buyer_item', name: 'buyer_item'},
            {data: 'customer_size', name:'customer_size'},
            {data: 'item_qty', name: 'item_qty', sortable: false, orderable: false, searchable: false},
            {data: 'inner_pack', name: 'inner_pack', sortable: false, orderable: false, searchable: false}
        ]
    });
    //end of datatables
});

    
</script>
@endsection
