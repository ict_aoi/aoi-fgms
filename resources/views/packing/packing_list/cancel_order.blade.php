@extends('layouts.app', ['active' => 'packinglistcancel'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">PACKING LIST CANCEL ORDER</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('packing.packingList') }}"><i class="icon-list position-left"></i> Packing List Cancel Order</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body" style="background-color: #EE4540;">
        <form action="{{ route('packing.ajaxGetPackingList') }}" id="form_filter">
            <div class="form-group hidden" id="filter_by_lcdate">
                <label><b>Choose LC Date (month/day/year)</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group" id="filter_by_po">
                <label><b>Choose PO Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="po" id="po" placeholder="PO Number">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="po">Filter by PO Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="lc">Filter by LC Date</label>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
        <span id="filter_date" style="float:right"><b></b></span>
    </div>
    <div class="panel-body">
        <table class="table datatable-save-state" id="table-packing-list">
            <thead>
                <tr>
                    <th>#</th>
                    <th>PO Number</th>
                    <th>Plan Ref Number</th>
                    <th>Ordered Date</th>
                    <th>Promised Date</th>
                    <th>Qty</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<a href="{{ route('packing.ajaxGetPackingList')}}" id="link-packing-list"></a>
<a href="{{ route('packing.ajaxGetDetail')}}" id="link-sync-plan"></a>

@endsection

@section('modal')
<!-- MODAL UPLOAD -->
<div id="modal_upload_" class="modal fade" role="dialog">
    <form class="form-horizontal" action="{{ route('packing.uploadExcelCancel') }}" id="form-upload" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content modal-lg">
                <div class="modal-body">
                    <div class="panel-body">
                        <fieldset>
                            <legend class="text-semibold">
                                <h4 id="title_upload"><i class="icon-upload4 position-left"></i>UPLOAD PACKING LIST #<span id='title_po'></span></h4>

                            </legend>
                            <div class="row">
                                <!-- BUYER FIELD -->
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="text-semibold"><i class="icon-reading position-left"></i> Buyer</legend>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Buyer Name:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="buyer_name" name="bp_name" placeholder="-" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Buyer Address:</label>
                                            <div class="col-lg-9">
                                                <textarea rows="5" cols="5" class="form-control" id="buyer_address" placeholder="-" readonly></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Buyer City:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="buyer_city" placeholder="-" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Buyer Postal Code:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="buyer_postal" placeholder="-" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Buyer Country:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="buyer_country" placeholder="-" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Buyer Phone:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="buyer_phone" placeholder="-" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Buyer Fax:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="buyer_fax" placeholder="-" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Plan Ref Number:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="inputPlanRefNumber" name="plan_ref_number" placeholder="" autofocus="">
                                            </div>
                                        </div>

                                    </fieldset>
                                </div>
                                <!-- END OF BUYER FIELD -->

                                <!-- DESTINATION FIELD -->
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="text-semibold"><i class="icon-truck position-left"></i> DESTINATION</legend>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Destination Name:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="destination_name" placeholder="-" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Destination Address:</label>
                                            <div class="col-lg-9">
                                                <textarea rows="5" cols="5" class="form-control" id="destination_address" name="address" placeholder="-" readonly></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Destination City:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="destination_city" name="city" placeholder="-" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Destination Postal Code:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="destination_postal" name="postal" placeholder="-" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Destination Country:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="destination_country" name="country" placeholder="-" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-3">
                                                <input type="checkbox" name="costco">&nbsp;<label>Costco</label>
                                            </div>                                        
                                        </div>
                                       
                                    </fieldset>
                                </div>
                                <!-- END OF DESTINATION FIELD -->
                            </div>
                            <hr>
                            <input type="hidden" name="po_number" id="po_number">
                            <input type="hidden" name="dateordered" id="dateordered">
                            <input type="hidden" name="datepromised" id="datepromised">
                            <input type="hidden" name="grandtotal" id="grandtotal">
                            <input type="hidden" name="bp_code" id="bp_code">
                            <input type="hidden" name="phone" id="phone">
                            <input type="hidden" name="fax" id="fax">
                            <input type="hidden" name="kst_lcdate" id="kst_lcdate">
                            <input type="hidden" name="kst_statisticaldate" id="kst_statisticaldate">
                            <input type="hidden" name="trademark" id="trademark">
                            <input type="hidden" name="made_in" id="made_in">
                            <input type="hidden" name="country_of_origin" id="country_of_origin">
                            <input type="hidden" name="commodities_type" id="commodities_type">
                            <input type="hidden" name="customer_order_number" id="customer_order_number">
                            <input type="hidden" name="remark" id="remark">
                            <input type="hidden" name="upc" id="upc">
                            <input type="hidden" name="season" id="season">
                            <input type="hidden" name="oldpo" id="oldpo">
                            <input type="hidden" name="recycle" id="recycle">


                            <!-- upload excel -->
                            <div class="form-group">
                                <label class="col-lg-3 control-label">File Excel :</label>
                                <div class="col-lg-9">
                                    <input type="file" class="file-styled" name="file_upload" id="file" required>
                                    <span class="help-block" id="excel_po"></span>
                                    <span class="help-block">Accepted formats: xls xlsx.</span>
                                </div>
                            </div>

                            <div id="verify_data">

                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Upload</button>
                    <!-- <button type="button" id="verify" class="btn btn-success">Verify<i class="icon-checkmark4 position-right"></i></button> -->
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /MODAL UPLOAD-->

<!-- MODAL REVISI -->
<div id="modal_revisi_" class="modal fade" role="dialog">
    <form class="form-horizontal" action="{{ route('packing.revisiExcelCancel') }}" id="form-revisi" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content modal-lg">
                <div class="modal-body">
                    <div class="panel-body">
                        <fieldset>
                            <legend class="text-semibold">
                                <h4 id="revisi_title_upload"><i class="icon-upload4 position-left"></i>REVISI PACKING LIST #<span id='revisi_title_po'></span></h4>
                            </legend>

                            <div class="table-responsive">
                                <table class="table table-condensed" id="table-current-packing-list">
                                    <thead>
                                        <tr>
                                            <th>RANGE</th>
                                            <th>FROM</th>
                                            <th>TO</th>
                                            <th>SERIAL FROM</th>
                                            <th>SERIAL TO</th>
                                            <th>PO#</th>
                                            <th>CUSTOMER SIZE</th>
                                            <th>ITEM QTY</th>
                                            <th>QTY PER PKG/ INNER PACK</th>
                                            <th>INNER PKG COUNT</th>
                                            <th>PKG COUNT</th>
                                            <th>R</th>
                                            <th>NET NET</th>
                                            <th>NET</th>
                                            <th>GROSS</th>
                                            <th>UNIT1</th>
                                            <th>L</th>
                                            <th>W</th>
                                            <th>H</th>
                                            <th>UNIT2</th>
                                            <th>SCAN ID</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Plan Ref Number:</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="inputPlanRefNumberNew" name="plan_ref_number_new" placeholder="" autofocus="">
                                </div>
                            </div>

                            <hr>
                            <input type="hidden" name="revisi_po_number" id="revisi_po_number">
                            <input type="hidden" name="revisi_plan_ref_number" id="revisi_plan_ref_number">

                            <input type="hidden" name="revisi_dateordered" id="revisi_dateordered">
                            <input type="hidden" name="revisi_datepromised" id="revisi_datepromised">
                            <input type="hidden" name="revisi_grandtotal" id="revisi_grandtotal">
                            <input type="hidden" name="revisi_bp_code" id="revisi_bp_code">
                            <input type="hidden" name="revisi_phone" id="revisi_phone">
                            <input type="hidden" name="revisi_fax" id="revisi_fax">
                            <input type="hidden" name="revisi_kst_lcdate" id="revisi_kst_lcdate">
                            <input type="hidden" name="revisi_kst_statisticaldate" id="revisi_kst_statisticaldate">
                            <input type="hidden" name="revisi_trademark" id="revisi_trademark">
                            <input type="hidden" name="revisi_made_in" id="revisi_made_in">
                            <input type="hidden" name="revisi_country_of_origin" id="revisi_country_of_origin">
                            <input type="hidden" name="revisi_commodities_type" id="revisi_commodities_type">
                            <input type="hidden" name="revisi_customer_order_number" id="revisi_customer_order_number">
                            <input type="hidden" name="revisi_remark" id="revisi_remark">
                            <input type="hidden" name="revisi_address" id="revisi_destination_address">
                            <input type="hidden" name="revisi_city" id="revisi_destination_city">
                            <input type="hidden" name="revisi_postal" id="revisi_destination_postal">
                            <input type="hidden" name="revisi_country" id="revisi_destination_country">
                            <input type="hidden" name="revisi_bp_name" id="revisi_buyer_name">
                            <input type="hidden" name="revisi_upc" id="revisi_upc">
                            <input type="hidden" name="revisi_season" id="revisi_season">
                            <input type="hidden" name="revisi_oldpo" id="revisi_oldpo">
                            <input type="hidden" name="revisi_recycle" id="revisi_recycle">

                            <!-- upload excel -->
                            <div class="form-group">
                                <label class="col-lg-3 control-label">File Excel :</label>
                                <div class="col-lg-9">
                                    <input type="file" class="file-styled" name="revisi_file_upload" id="revisi_file" required>
                                    <span class="help-block" id="revisi_excel_po"></span>
                                    <span class="help-block">Accepted formats: xls xlsx.</span>
                                </div>
                            </div>

                            <div id="verify_data">

                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Upload</button>
                    <!-- <button type="button" id="verify" class="btn btn-success">Verify<i class="icon-checkmark4 position-right"></i></button> -->
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /MODAL REVISI-->
@endsection

@include('packing/packing_list/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    //url
    var url = $('#link-packing-list').attr('href');

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#table-packing-list').DataTable({
        ajax: {
            url: url,

            //api
            data: {date_range: $('#date_range').val(), po_number: $('#po').val()}

            //sync
            // data: function (d) {
            //     return $.extend({},d,{
            //         "date_range": $('#date_range').val()
            //     });
            // }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false  },
            {data: 'poreference', name: 'poreference'},
            {data: 'plan_ref_number', name: 'plan_ref_number'},
            {data: 'dateordered', name: 'dateordered'},
            {data: 'datepromised', name: 'datepromised'},
            {data: 'total_package', name: 'total_package', searchable: false, orderable: false, sortable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
    });
    //end of datatables

    //
    $('#table-packing-list').on('click','.ignore-click', function() {
        return false;
    });

    //open modal upload (sync)
    // $('#table-packing-list').on('click', '.upload', function() {
    //     var url = $(this).attr('href');
    //     var params = getURLParameter(url);
    //     var po_number = params['po_number'];
    //
    //     $('#file').val('');
    //     $('#title_po').text(po_number);
    //     $('#excel_po').text('PO #' + po_number);
    //
    //     $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         }
    //     });
    //     $.ajax({
    //         type: 'post',
    //         url : $('#link-sync-plan').attr('href'),
    //         data: {po_number: po_number},
    //         success: function(response) {
    //             $('#po_number').val(response['documentno']);
    //             $('#buyer_name').val(response['bp_name']);
    //             $('#destination_address').val(response['address']);
    //             $('#destination_city').val(response['city']);
    //             $('#destination_postal').val(response['postal']);
    //             $('#destination_country').val(response['country']);
    //             $('#dateordered').val(response['dateordered']);
    //             $('#datepromised').val(response['datepromised']);
    //             $('#grandtotal').val(response['grandtotal']);
    //             $('#bp_code').val(response['bp_code']);
    //             $('#phone').val(response['phone']);
    //             $('#fax').val(response['fax']);
    //             $('#kst_lcdate').val(response['kst_lcdate']);
    //             $('#kst_statisticaldate').val(response['kst_statisticaldate']);
    //             $('#trademark').val(response['trademark']);
    //             $('#made_in').val(response['made_in']);
    //             $('#country_of_origin').val(response['country_of_origin']);
    //
    //             $('#modal_upload_').modal('show');
    //         },
    //         error: function(response) {
    //             myalert('error','NOT GOOD');
    //         }
    //     });
    // });

    //open modal upload (api)
    $('#table-packing-list').on('click', '.upload', function() {
        var url = $(this).attr('href');
        var params = getURLParameter(url);
        var ponumber = params['po_number'];

        $('#po_number').val(ponumber);
        $('#buyer_name').val($('#buyer_name_' + ponumber).val());
        $('#destination_address').val($('#destination_address_' + ponumber).val());
        $('#destination_city').val($('#destination_city_' + ponumber).val());
        $('#destination_postal').val($('#destination_postal_' + ponumber).val());
        $('#destination_country').val($('#destination_country_' + ponumber).val());
        $('#dateordered').val($('#dateordered_' + ponumber).val());
        $('#datepromised').val($('#datepromised_' + ponumber).val());
        $('#grandtotal').val($('#grandtotal_' + ponumber).val());
        $('#bp_code').val($('#bp_code_' + ponumber).val());
        $('#phone').val($('#phone_' + ponumber).val());
        $('#fax').val($('#fax_' + ponumber).val());
        $('#kst_lcdate').val($('#kst_lcdate_' + ponumber).val());
        $('#kst_statisticaldate').val($('#kst_statisticaldate_' + ponumber).val());
        $('#trademark').val($('#trademark_' + ponumber).val());
        $('#made_in').val($('#made_in_' + ponumber).val());
        $('#country_of_origin').val($('#country_of_origin_' + ponumber).val());
        $('#commodities_type').val($('#commodities_type_' + ponumber).val());
        $('#customer_order_number').val($('#customer_order_number_' + ponumber).val());
        $('#remark').val($('#remark_' + ponumber).val());
        $('#upc').val($('#upc_' + ponumber).val());
        $('#season').val($('#season_' + ponumber).val());
        $('#oldpo').val($('#oldpo_' + ponumber).val());
        $('#recycle').val($('#recycle_' + ponumber).val());

        $('#file').val('');
        $('#title_po').text(ponumber);
        $('#excel_po').text('PO #' + ponumber);
        $('#modal_upload_').modal('show');
    });

    //open modal revisi
    $('#table-packing-list').on('click', '.revisi', function() {
        var url = $(this).attr('href');
        var params = getURLParameter(url);
        var ponumber = params['po_number'];
        var plan_ref_number = params['plan_ref_number'];

        $('#revisi_buyer_name').val($('#buyer_name_' + ponumber).val());
        $('#revisi_destination_address').val($('#destination_address_' + ponumber).val());
        $('#revisi_destination_city').val($('#destination_city_' + ponumber).val());
        $('#revisi_destination_postal').val($('#destination_postal_' + ponumber).val());
        $('#revisi_destination_country').val($('#destination_country_' + ponumber).val());
        $('#revisi_dateordered').val($('#dateordered_' + ponumber).val());
        $('#revisi_datepromised').val($('#datepromised_' + ponumber).val());
        $('#revisi_grandtotal').val($('#grandtotal_' + ponumber).val());
        $('#revisi_bp_code').val($('#bp_code_' + ponumber).val());
        $('#revisi_phone').val($('#phone_' + ponumber).val());
        $('#revisi_fax').val($('#fax_' + ponumber).val());
        $('#revisi_kst_lcdate').val($('#kst_lcdate_' + ponumber).val());
        $('#revisi_kst_statisticaldate').val($('#kst_statisticaldate_' + ponumber).val());
        $('#revisi_trademark').val($('#trademark_' + ponumber).val());
        $('#revisi_made_in').val($('#made_in_' + ponumber).val());
        $('#revisi_country_of_origin').val($('#country_of_origin_' + ponumber).val());
        $('#revisi_commodities_type').val($('#commodities_type_' + ponumber).val());
        $('#revisi_customer_order_number').val($('#customer_order_number_' + ponumber).val());
        $('#revisi_remark').val($('#remark_' + ponumber).val());
        $('#revisi_upc').val($('#upc_' + ponumber).val());
        $('#revisi_season').val($('#season_' + ponumber).val());
        $('#revisi_oldpo').val($('#oldpo_' + ponumber).val());
        $('#revisi_recycle').val($('#recycle_' + ponumber).val());

        $('#revisi_po_number').val(ponumber);
        $('#revisi_plan_ref_number').val(plan_ref_number);
        $('#revisi_file').val('');
        $('#revisi_title_po').text(ponumber);
        $('#revisi_excel_po').text('REVISI PO #' + ponumber);
        $('#modal_revisi_').modal('show');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : url,
            data: {ponumber: ponumber},
            beforeSend: function() {
                $('#table-current-packing-list').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#table-current-packing-list').unblock();
                $('#table-current-packing-list > tbody').html(response);
            },
            error: function(response) {
                $('#table-current-packing-list').unblock();
                myalert('error',response['responseJSON']);
            }
        });
    });

    //filter (api)
    $('#form_filter').submit(function(event){
        event.preventDefault();
        var date_range = $('#date_range').val();
        var po_number  = $('#po').val();
        var radio_status = $('input[name=radio_status]:checked').val();

        //check location
        if(date_range == '' && po_number == '') {
            myalert('error','Please select filter first');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : url,
            data: {date_range: date_range, po_number: po_number, radio_status: radio_status},
            beforeSend: function() {
                $('#table-packing-list').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#table-packing-list').unblock();

                if(radio_status == 'po') {
                    $('#filter_date').text('CURRENT FILTER: PO Number #' + po_number);
                }
                else if(radio_status == 'lc') {
                    $('#filter_date').text('CURRENT FILTER: ' + date_range);
                }
                $('#filter_date').addClass('label-striped');
                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#table-packing-list').unblock();
                myalert('error',response['responseJSON']);
            }
        });
    });

    //filter (sync)
    // $('#form_filter').submit(function(event){
    //     event.preventDefault();
    //
    //     //check location
    //     if($('#date_range').val() == '') {
    //         myalert('error','Please select date range first');
    //         return false;
    //     }
    //
    //     table.draw();
    // });

    //verify excel
    $('#form-upload').submit(function(event) {
        event.preventDefault();

        //check po_number
        if($('#po_number').val() == '') {
            myalert('error','PO Number not integrated');
            return false;
        }

        //
        var formData = new FormData($(this)[0]);
        var parameter = ['po_number','bp_name','address','city','postal','country','dateordered',
                         'datepromised','grandtotal','bp_code','phone','fax','kst_lcdate',
                         'kst_statisticaldate','trademark','made_in','country_of_origin',
                         'commodities_type', 'customer_order_number', 'remark','upc','season','oldpo','recycle'];
        var val = [
            $('#po_number').val(), $('#buyer_name').val(), $('#destination_address').val(),
            $('#destination_city').val(), $('#destination_postal').val(), $('#destination_country').val(),
            $('#dateordered').val(), $('#datepromised').val(), $('#grandtotal').val(), $('#bp_code').val(),
            $('#phone').val(), $('#fax').val(), $('#kst_lcdate').val(), $('#kst_statisticaldate').val(),
            $('#trademark').val(), $('#made_in').val(), $('#country_of_origin').val(), $('#commodities_type').val(),
            $('#customer_order_number').val(), $('#remark').val(),$('#upc').val(),$('#season').val(),$('#oldpo').val(),$('#recycle').val()
        ]

        for(var i = 0; i < parameter.length; i++) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#form-upload').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $('#form-upload .modal-body').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Uploading File</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#form-upload .modal-body').unblock();
                myalert('success', 'Upload Success..');
                $('#total_package_' + $('#po_number').val()).text(response);
                $('#modal_upload_').modal('hide');
                $('#file').val('');
                $('#inputPlanRefNumber').val('');
                $('#form_filter').submit();
                //table.ajax.reload( null, false ); //sync
            },
            error: function(response) {
                $('#form-upload .modal-body').unblock();
                if(response['status'] == 422) {
                    myalert('error', response['responseJSON']);
                }
            }
        });
    });

    //revisi excel
    $('#form-revisi').submit(function(event) {
        event.preventDefault();

        //check po_number
        if($('#revisi_po_number').val() == '') {
            nyalert('error','PO Number not integrated');
            return false;
        }

        //
        var formData = new FormData($(this)[0]);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#form-revisi').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $('#form-revisi .modal-body').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Uploading File</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                // console.log(response);
                $('#form-revisi .modal-body').unblock();
                myalert('success', 'Upload Success..');
                $('#total_package_' + $('#revisi_po_number').val()).text(response);
                $('#modal_revisi_').modal('hide');
                $('#revisi_file').val('');
                $('#inputPlanRefNumberNew').val('');
                $('#form_filter').submit();
            },
            error: function(response) {
                $('#form-revisi .modal-body').unblock();
                if(response['status'] == 422) {
                    myalert('error', response['responseJSON']);
                }
            }
        });
    });

    //choose filter
    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'po') {
            if($('#filter_by_po').hasClass('hidden')) {
                $('#filter_by_po').removeClass('hidden');
            }

            $('#filter_by_lcdate').addClass('hidden');
        }
        else if (this.value == 'lc') {
            if($('#filter_by_lcdate').hasClass('hidden')) {
                $('#filter_by_lcdate').removeClass('hidden');
            }

            $('#filter_by_po').addClass('hidden');
        }
    });


    //get parameter from url
    function getURLParameter(url) {
        var params = {};
        url.substring(1).replace(/[?&]+([^=&]+)=([^&]*)/gi,
                function (str, key, value) {
                     params[key] = value;
                });
        return params;
    }

    $('#table-packing-list').on('blur', '.planref_unfilled', function() {
        var ponumber = $(this).data('ponumber');
        var planref = $(this).data('planrefnumber');
 
        var new_plan_ref = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{ route('packing.updatePlanref') }}",
            data: {po_number:ponumber, planref:planref, new_plan_ref:new_plan_ref
            },
            beforeSend: function() {
                $('#table-packing-list').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response){
                $('#planref_' + ponumber).html(response);
                $('#form_filter').submit();
            },
            error: function(response) {
                if (response.status==422) {
                    myalert('error',response.responseText);
                    return false;
                    $('#form_filter').submit();
                }
               
               
            }
        });

    });

});
</script>
@endsection
