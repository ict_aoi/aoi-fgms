<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		@page{
			margin: 10 10 10 10;
		}

		header{
	        position: fixed;
	        left: 0px;
	        right: 0px;
	        magin-top: -80px;
	      
	    }

	   footer{
	   		position: fixed; 
            bottom: -35px; 
            left: 0px; 
            right: 0px;
            height: 40px; 
	   }

	    main{
	    	margin-top: 250px;
	    }

		.tbhead{
			border-collapse: collapse;
			width: 100%;
			
		}

		.tbhead, th,tr,td{
			border: 1px solid black;
			/*height: 15px;*/
		}

		.tbdetail{

			border-collapse: collapse;
			width: 100%;
			
		}

		.tbdetail, th,tr,td{
			border: 1px solid black;
		}

		.page-break{
			page-break-after: always;
		}

		.fonttd{
			font-size: 14.6px;
		}
	</style>
</head>
<body>
	<header>
		
		<table class="tbhead">
			<tr>
				<td colspan="4"><center>FORMULIR SURAT JALAN</center></td>
			</tr>
			<tr>
				<td colspan="4">
					<center>
						<label>PT. APPAREL ONE INDONESIA</label> <br>
						<img src="{{ public_path('image/AOI.png') }}" alt="Image" width="100" style="margin-top: 7px;"> <br>
						<label style="margin-top: 7px;">{{$fact}}</label>
					</center>
				</td>
			</tr>
			<tr>
				<td width="15%"><label class="head">Tanggal</label></td>
				<td><label class="head">{{date_format(date_create($head->plan_stuffing),'j-M-y')}}</label></td>

				<td width="10%"><label class="head">Kontainer</label></td>
				<td><label class="head">{{$head->no_container}}</label></td>
			</tr>
			<tr>
				<td><label class="head">No. Urut</label></td>
				<td><label class="head">{{$head->do_number}}</label></td>

				<td><label class="head"></label></td>
				<td><label class="head"></label></td>
			</tr>
			<tr>
				<td><label class="head">Kepada Yth</label></td>
				<td><label class="head">{{$head->whs_name}}</label></td>

				<td><label class="head"></label></td>
				<td><label class="head"><label></td>
			</tr>
			<tr>
				<td><label class="head"></label></td>
				<td><label class="head">{{$head->address}}</label></td>

				<td><label class="head"></label></td>
				<td><label class="head"></label></td>
			</tr>
			<tr>
				<td height="17.5px"><label class="head"></label></td>
				<td height="17.5px"><label class="head"></label></td>

				<td height="17.5px"><label class="head"></label></td>
				<td height="17.5px"><label class="head"></label></td>
			</tr>
		</table>
	</header>

	
	
	<footer>
		
	</footer>
	
	<?php 
			$chunk = array_chunk($data->toArray(),$maxrow);
			$no = 1;
			$totctn = 0;
			$totqty = 0;
			for ($i=0; $i <count($chunk) ; $i++) { 

		?>
			<main>
				<table class="tbdetail">
					<tr><td colspan="8" height="17px"> <center><label style="font-size: 13px;">SURAT JALAN / PACKING LIST</label></center></td></tr>
					<tr>
						<th width="20px"><center>NO.</center></th>
						<th width="155px"><center>STYLE</center></th>
						<th width="100px"><center>NO PO</center></th>
						<th width="100px"><center>PO REF.</center></th>
						<th colspan="2"><center>JUMLAH</center></th>
						<th width="125px"><center>DESTINANTION</center></th>
						<th width="125px"><center>GUDANG</center></th>
					</tr>
					<tr>
						<th width="20px"></th>
						<th width="155px"></th>
						<th width="100px"></th>
						<th width="100px"></th>
						<th width="65px"><center>QTY</center></th>
						<th width="65px"><center>CTN</center></th>
						<th width="125px"></th>
						<th width="125px"></th>
					</tr>

					<?php 
						foreach ($chunk[$i] as $vl) {
					?>
						<tr>
							<td class="fonttd"><center>{{$no}}</center></td>
							<td class="fonttd"><center>{{ $vl->style }}</center></td>
							<td class="fonttd"><center>{{ $vl->po_number }}</center></td>
							<td class="fonttd"><center>{{ $vl->customer_order_number }}</center></td>
							<td class="fonttd"><center>{{ $vl->item_qty }}</center></td>
							<td class="fonttd"><center>{{ $vl->ctn }}</center></td>
							<td class="fonttd"><center>{{ $vl->country_name }}</center></td>
							<td class="fonttd"><center>{{ $vl->whs_name }}</center></td>
						</tr>
					<?php
							$no++;
							$totctn = $totctn+$vl->ctn;
							$totqty = $totqty+$vl->item_qty;
						}
					?>

					@if($i==(count($chunk)-1))
						<tr>
							<td style="height:15px;"></td>
							<td style="height:15px;"></td>
							<td style="height:15px;"></td>
							<td style="height:15px;"></td>
							<td style="height:15px;"></td>
							<td style="height:15px;"></td>
							<td style="height:15px;"></td>
							<td style="height:15px;"></td>
						</tr>
						<tr>
							<td class="fonttd"></td>
							<td class="fonttd"></td>
							<td class="fonttd"></td>
							<td class="fonttd"></td>
							<td class="fonttd"><center>{{ $totqty}}</center></td>
							<td class="fonttd"><center>{{ $totctn }}</center></td>
							<td class="fonttd"></td>
							<td class="fonttd"></td>
						</tr>
					@endif
				</table>

				@if($i==(count($chunk)-1))
					<br>
					<table class="tbdetail">
						<tr>
					        <td width="25%">
					            <center>Di kirim</center>
					        </td>
					        <td width="25%">
					            <center>Di setujui oleh</center>
					        </td>
					        <td width="25%">
					            <center>Di terima oleh</center>
					        </td>
					        <td width="25%">
					        	<center>Security</center>
					        </td>
					    </tr>
					    <tr>
					        <td height="65px">  </td>
					        <td>                </td>
					        <td>                </td>
					        <td>                </td>
					    </tr>
					    <tr>
					        <td height="20px">  </td>
					        <td>                </td>
					        <td>                </td>
					        <td>				</td>
					    </tr>
					    <tr>
					        <td height="10px"> <center><label style="font-size: 10px;">Nama jelas</label></center>  </td>
					        <td>                </td>
					        <td>                </td>
					        <td>                </td>
					    </tr>
					</table>
					<br>
					<label style="font-size: 12px; font-family: calibri; margin-left: 685px;">( FR.02-01-PAC )</label>
				@endif
				
			</main>
			@if($i!=(count($chunk)-1))
				<div class="page-break"></div>
			@endif
			
		<?php

			}
		?>
</body>
</html>