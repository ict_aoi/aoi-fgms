@section('js_extension')
<script type="text/javascript" src="{{ url('js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{ url('js/jszip/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/plugins/notifications/sweet_alert.min.js')}}"></script>
<script type="text/javascript" src="{{ url('js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{ url('js/plugins/pickers/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ url('js/plugins/pickers/anytime.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/plugins/pickers/pickadate/picker.js') }}"></script>
<script type="text/javascript" src="{{ url('js/plugins/pickers/pickadate/picker.date.js') }}"></script>
<script type="text/javascript" src="{{ url('js/plugins/pickers/pickadate/picker.time.js') }}"></script>
<script type="text/javascript" src="{{ url('js/plugins/pickers/pickadate/legacy.js') }}"></script>

<script type="text/javascript" src="{{ url('js/plugins/tables/datatables/extensions/fixed_columns.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/pages/datatables_extension_fixed_columns.js') }}"></script>
<script type="text/javascript" src="{{ url('js/pages/picker_date.js') }}"></script>
@endsection
