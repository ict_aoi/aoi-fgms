@extends('layouts.app', ['active' => 'suratjalan'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">Surat Jalan</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-stack2 position-left"></i> Delivery Order</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('delivery.getDataDO') }}" id="form_filter">
            @csrf
            <div class="form-group hidden" id="filter_by_delivery">
                <label><b>Delivery Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="delivery_no" id="delivery_no">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group" id="filter_by_stuffing">
                <label><b>Plan Stuffing</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control pickadate" name="planstuff" id="planstuff">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" value="plan" checked="checked">Filter by Plan Stuffing</label>
                <label class="radio-inline"><input type="radio" name="radio_status"  value="delivery">Filter by Delivery Number</label>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>DO Number</th>
                        <th>No. Polisi</th>
                        <th>Driver</th>
                        <th>FWD</th>
                        <th>Shipmode</th>
                        <th>Warehouse</th>
                        <th>Expedisi</th>
                        <th>Type</th>
                        <th>Invoice</th>
                        <th>Remark</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('delivery.exportDO') }}" id="exportDO" target="_blank"></a>
@endsection

@section('modal')


</style>
<!-- modal remark -->
<div id="modal_do" class="modal fade" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content modal-lg">
            <div class="modal-body">
                <div class="panel">
                    <div class="panel-body">
                        <div class="row form-group">
                            <div class="col-lg-3">
                                <div class="row">
                                    <label><b>Delivery Number : </b></label>
                                </div>
                                <div class="row">
                                    <label id="txdo"><b></b></label>
                                    <input type="text" name="getid" id="getid" hidden="">
                                    <input type="text" name="getdo" id="getdo" hidden="">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="row">
                                    <label><b>No. Polisi : </b></label>
                                </div>
                                <div class="row">
                                    <label id="txnopol"><b></b></label>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="row">
                                    <label><b>Driver : </b></label>
                                </div>
                                <div class="row">
                                    <label id="txdriver"><b></b></label>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="row">
                                    <label><b>Warehouse : </b></label>
                                </div>
                                <div class="row">
                                    <label id="txwhs"><b></b></label>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-3">
                                <label><b>Template</b></label>
                                <select class="form-control" id="template">
                                    <!-- <option value="">--Pilih Template-</option> -->
                                    <option value="truck_rg">Truck Regular</option>
                                    <option value="container_rg">Container Regular</option>
                                    <option value="truck_sp">Truck Special</option>
                                    <option value="container_sp">Container Special</option>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <label><b>Max Row</b></label>
                                <input type="number" name="maxrow" id="maxrow" min="1" max="40" value="40" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <button class="btn btn-success" id="btn-print" style="margin-top:27px;" onclick="printDO(this);"><span class="icon-printer"></span> Print</button>
                            </div>
                        </div>
                        <br>
                        <div class="row form-group">

                            <!-- table special -->
                            <div class="form-group" id="special-tb">
                                <div class="table-responsive">
                                    <table class="table datatable-save-state" id="table-list-do">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Style</th>
                                                <th>PO Number</th>
                                                <th>Cust. Order</th>
                                                <th>PCS</th>
                                                <th>CTN</th>
                                                <th>Destination</th>
                                                <th>Gudang</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@include('delivery/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $('input[type=radio][name=radio_status]').change(function() {
      
        if (this.value == 'plan') {
            if($('#filter_by_stuffing').hasClass('hidden')) {
                $('#filter_by_stuffing').removeClass('hidden');
            }

            $('#filter_by_delivery').addClass('hidden');
         

        }else if (this.value == 'delivery') {
            if($('#filter_by_delivery').hasClass('hidden')) {
                $('#filter_by_delivery').removeClass('hidden');
            }

            $('#filter_by_stuffing').addClass('hidden');
           
        }
        
    });

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            url:  $('#form_filter').attr('action'),
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "radio": $('input[name=radio_status]:checked').val(),
                    "stuffing" : $('#planstuff').val(),
                    "delivery" : $('#delivery_no').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'do_number', name: 'do_number'},
            {data: 'nopol', name: 'nopol'},
            {data: 'driver', name: 'driver'},
            {data: 'fwd', name: 'fwd'},
            {data: 'shipmode', name: 'shipmode'},
            {data: 'whs_id', name: 'whs_id'},
            {data: 'expedisi', name: 'expedisi'},
            {data: 'type', name: 'type'},
            {data: 'invoice', name: 'invoice'},
            {data: 'remark', name: 'remark'},
            {data: 'action', name: 'action'}
        ],
    });

    table.on('preDraw', function() {
        Pace.start();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        loading_process();
        table.clear();
        table.draw();
    });

});

function _modalDO(e){
    var id = e.getAttribute('data-id');
    var nodo = e.getAttribute('data-do');
    var driver = e.getAttribute('data-driver');
    var nopol = e.getAttribute('data-nopol');
    var whs = e.getAttribute('data-whs');
    $('#txdo').text(nodo);
    $('#txnopol').text(nopol);
    $('#txdriver').text(driver);
    $('#txwhs').text(whs);
    $('#getid').val(id);
    $('#getdo').val(nodo);
    $('#modal_do').modal('show');

   

    var _token = $("input[name='_token']").val();
    var tabledo = $('#table-list-do').DataTable({
        processing: true,
        paging:false,
        searching:false,
        destroy: true,
        ajax: {
            url:  "{{ route('delivery.getdetailDO') }}",
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "id" :id,
                    "do" :nodo,
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = tabledo.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'style', name: 'style'},
            {data: 'po_number', name: 'po_number'},
            {data: 'customer_order_number', name: 'customer_order_number'},
            {data: 'item_qty', name: 'item_qty'},
            {data: 'ctn', name: 'ctn'},
            {data: 'country_name', name: 'country_name'},
            {data: 'warehouse', name: 'warehouse'}
        ],
    });
}

function printDO(e){
    var template = $('#template').val();
    var id = $('#getid').val();
    var do_number = $('#getdo').val();
    var maxrow = $('#maxrow').val();
    var url = $('#exportDO').attr('href');
    var param = '?id='+id+'&template='+template+'&maxrow='+maxrow;

    window.open(url + param, '_blank');
    
    
}
</script>
@endsection