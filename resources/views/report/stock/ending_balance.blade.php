@extends('layouts.app', ['active' => 'reportstockbalance'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <!-- <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">Report Ending Balance</span></h4>
            <p class="position-left"></p>
        </div>
    </div> -->
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('report.endingBalance') }}"><i class="icon-airplane4 position-left"></i> Report Ending Balance</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('report.ajaxGetDataPackingCalculate') }}" id="form_filter">
            <div class="form-group" id="filter_by_current">
                <label><b>Current Date # {{ \Carbon\Carbon::now()->format('d/m/Y H:i:s') }}</b></label>
            </div>
            <div class="form-group hidden" id="filter_by_date">
                <label><b>Choose Date</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control pickadates" name="date_range" id="date_range" value="{{ \Carbon\Carbon::now()->format('j F, Y') }}">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="current">Current Date</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="date">Filter by Date</label>
            </div>
        </form>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row {{ Auth::user()->admin_role !== 1 ? 'hidden' : '' }}">
            <div class="col-md-2"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->factory_id }}" {{ $val->factory_id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>BARCODE ID</th>
                        <th>STYLE</th>
                        <th>ART. NO</th>
                        <th>BRAND</th>
                        <th>PO NUMBER</th>
                        <th>CUST SIZE</th>
                        <th>MAN SIZE</th>
                        <th>DIMENSION (LxWxH(satuan))</th>
                        <th>QTY</th>
                        <th>LOCATION</th>
                        <th>CHECKIN</th>
                        <th>Ending Balance</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('report.ajaxGetDataStockBalance') }}" id="data_stock"></a>
<a href="{{ route('report.exportStockBalance') }}" id="export_stock"></a>
@endsection

@include('report/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    //url
    var url = $('#data_stock').attr('href');
    var filter = null;

    $('.pickadates').pickadate({
        disable: [
                    [2019,8,12],
                    [2019,8,13],
                    [2019,8,14],
                    [2019,8,15],
                    [2019,8,16],
                    [2019,8,17],
                    [2019,9,16],
                    [2019,9,17],
                    [2019,9,18],
                    [2019,9,19],
                    [2019,9,20],
                    [2019,9,21],
                    [2019,9,22],
                    [2019,9,23],
                    [2019,9,24],
                    [2019,9,25],
                    [2019,9,26],
                    [2019,9,27],
                    [2019,9,28],
                    [2019,9,29],
                ],
        min: [2019,8,6]
        //max: [2014,7,14]
    });

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#export_stock').attr('href')
                                            + '?orderby=' + column_name
                                            + '&direction=' + direction
                                            + '&filterby=' + filter
                                            + '&radio_status=' + $('input[name=radio_status]:checked').val()
                                            + '&date_range=' + $('#date_range').val()
                                            + '&factory_id=' + $('#factory_id').val();
                }
            }
       ],
        ajax: {
            url: url,
            type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "radio_status": $('input[name=radio_status]:checked').val(),
                    "date_range": $('#date_range').val(),
                    "factory_id": $('#factory_id').val(),
                    "filterby": $('.dataTables_filter input').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(1).css('min-width', '150px');
            $('td', row).eq(2).css('min-width', '150px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'barcode_id', name: 'barcode_id'},
            {data: 'upc', name: 'upc', sortable: false, orderable: false, searchable: false},
            {data: 'art_no', name: 'art_no'},
            {data: 'brand', name: 'brand'},
            {data: 'po_number', name: 'po_number'},
            {data: 'customer_size', name: 'customer_size'},
            {data: 'manufacturing_size', name: 'manufacturing_size'},
            {data: 'dimension', name: 'dimension', sortable: false, orderable: false, searchable: false},
            {data: 'inner_pack', name: 'inner_pack'},
            {data: 'code', name: 'code', sortable: false, orderable: false, searchable: false},
            {data: 'checkin', name: 'created_at'},
            {data: 'date_balance', name: 'date_balance', sortable: false, orderable: false, searchable: false}
        ],
    });

    table.on('preDraw', function() {
        Pace.start();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    //end of datatables
    $('#factory_id').on('change', function() {
        loading_process();

        table.draw();
    });

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        //check date
        if($('#date_range').val() == '') {
            alert('Please select date range first');
            return false;
        }

        loading_process();

        table.draw();
    })

     //choose filter
     $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'current') {
            if($('#filter_by_current').hasClass('hidden')) {
                $('#filter_by_current').removeClass('hidden');
            }

            $('#filter_by_date').addClass('hidden');
        }
        else if (this.value == 'date') {
            if($('#filter_by_date').hasClass('hidden')) {
                $('#filter_by_date').removeClass('hidden');
            }

            $('#filter_by_current').addClass('hidden');
        }
        loading_process();

        table.draw();
    });

});
</script>
@endsection
