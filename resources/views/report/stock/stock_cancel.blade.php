@extends('layouts.app', ['active' => 'reportstockcancel'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <!-- <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">Report Stock</span></h4>
            <p class="position-left"></p>
        </div>
    </div> -->
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('report.stock') }}"><i class="icon-paperplane position-left"></i> Report Stock</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row {{ Auth::user()->admin_role !== 1 ? 'hidden' : '' }}">
            <div class="col-md-2"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->factory_id }}" {{ $val->factory_id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>BARCODE ID</th>
                        <th>STYLE</th>
                        <th>PO NUMBER</th>
                        <th>CUST SIZE</th>
                        <th>MAN SIZE</th>
                        <th>DIMENSION (LxWxH(satuan))</th>
                        <th>QTY</th>
                        <th>LOCATION</th>
                        <th>CHECKIN</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('report.getDatastockCancel') }}" id="data_stock"></a>
<a href="{{ route('report.exportStockCancel') }}" id="export_stock"></a>
@endsection

@include('report/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    //url
    var url = $('#data_stock').attr('href');
    var filter = null;

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#export_stock').attr('href')
                                            + '?orderby=' + column_name
                                            + '&direction=' + direction
                                            + '&filterby=' + filter
                                            + '&factory_id=' + $('#factory_id').val();
                }
            }
       ],
        ajax: {
            url: url,
            type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "date_range": $('#date_range').val(),
                    "factory_id": $('#factory_id').val(),
                    "filterby": $('.dataTables_filter input').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(1).css('min-width', '150px');
            $('td', row).eq(2).css('min-width', '150px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'barcode_id', name: 'barcode_id'},
            {data: 'upc', name: 'upc', sortable: false, orderable: false, searchable: false},
            {data: 'po_number', name: 'po_summary.po_number'},
            {data: 'customer_size', name: 'package_detail.customer_size'},
            {data: 'manufacturing_size', name: 'package_detail.manufacturing_size'},
            {data: 'dimension', name: 'dimension', sortable: false, orderable: false, searchable: false},
            {data: 'inner_pack', name: 'package_detail.inner_pack'},
            {data: 'code', name: 'locators.code'},
            {data: 'checkin', name: 'package_movements.created_at'}
        ],
    });

    table.on('preDraw', function() {
        Pace.start();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    //end of datatables
    $('#factory_id').on('change', function() {
        loading_process();

        table.draw();
    });

    //
    // $('.exportExcel').click(function(){
    //     $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         }
    //     });
    //     $.ajax({
    //         type: 'get',
    //         url: $('#export_stock').attr('href'),
    //         beforeSend: function() {
    //             $('#table-list').block({
    //              message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing..</span>',
    //                 overlayCSS: {
    //                     backgroundColor: '#fff',
    //                     opacity: 0.8,
    //                     cursor: 'wait'
    //                 },
    //                 css: {
    //                     border: 0,
    //                     padding: '10px 15px',
    //                     color: '#fff',
    //                     width: 'auto',
    //                     '-webkit-border-radius': 2,
    //                     '-moz-border-radius': 2,
    //                     backgroundColor: '#333'
    //                 }
    //             });
    //         },
    //         success: function(response){
    //             console.log('asd')
    //             $('#table-list').unblock();
    //         }
    //     });
    // })
});
</script>
@endsection
