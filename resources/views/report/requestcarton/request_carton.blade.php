@extends('layouts.app', ['active' => 'reportrequestcarton'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <!-- <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">Report Inventory Per Po Number</span></h4>
            <p class="position-left"></p>
        </div>
    </div> -->
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#"><i class="icon-location4 position-left"></i> Report Request Carton Detail</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('report.getReportRequestCarton') }}" id="form_filter">
            @csrf
            <div class="form-group">
                <label><b>Select Request Date</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range" required>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row {{ Auth::user()->admin_role !== 1 ? 'hidden' : '' }}">
            <div class="col-md-2"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->factory_id }}" {{ $val->factory_id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>REQ DATE</th>
                        <th>LINE</th>
                        <th>PO NUMBER</th>
                        <th>SIZE</th>
                        <th>QTY</th>
                        <th>PACKING DONE</th>
                        <th>SEWING RECEIVE</th>
                        <!-- <th>INFO</th> -->
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('report.exportRequestCarton') }}" id="export_request"></a>
@endsection

@include('report/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#export_request').attr('href')
                                            + '?date_range=' + $('#date_range').val()
                                            + '&orderby=' + column_name
                                            + '&direction=' + direction
                                            + '&filterby=' + filter
                                            + '&factory_id=' + $('#factory_id').val();
                }
            }
       ],
        ajax: {
            url: $('#form_filter').attr('action'),
            type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "date_range": $('#date_range').val(),
                    "factory_id": $('#factory_id').val(),
                    "filterby": $('.dataTables_filter input').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'request_at', name: 'request_at'},
            {data: 'line', name: 'line'},
            {data: 'po_number', name: 'po_number'},
            {data: 'manufacturing_size', name: 'manufacturing_size', sortable: false, orderable: false, searchable: false},
            {data: 'carton_qty', name: 'carton_qty', sortable: false, orderable: false, searchable: false},
            {data: 'done', name: 'done', sortable: false, orderable: false, searchable: false},
            {data: 'receive', name: 'receive', sortable: false, orderable: false, searchable: false}
            // {data: 'info', name: 'info'}
        ],
    });
    //end of datatables

    table.on('preDraw', function() {
        Pace.start();
    }).on('draw.dt', function() {
        Pace.stop()
        $('#table-list').unblock();
    });

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        //check location
        if($('#date_range').val() == '') {
            alert('Please select date range first');
            return false;
        }

        loading_process();

        table.draw();
    })

    //
    $('#factory_id').on('change', function() {
        loading_process();
        table.draw();
    });
    $('#date_range').on('change', function(){
        date = $(this).val();
    });
});
</script>
@endsection