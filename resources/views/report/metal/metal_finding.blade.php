@extends('layouts.app', ['active' => 'reportmetal'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">Report Metal Findding</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#"><i class="icon-home4 position-left"></i>Report Metal Findding</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection
 
@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('report.ajaxGetData') }}" id="form_filter">
            @csrf
            <div class="form-group">
                <label><b>Select Checkin Date</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range" required>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
         <div class="row {{ Auth::user()->admin_role !== 1 ? 'hidden' : '' }}">
            <div class="col-md-2"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->factory_id }}" {{ $val->factory_id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Metal Detector Date</th>
                        <th>Barcode ID</th>
                        <th>Package No.</th>
                        <th>PO Number</th>
                        <th>Style</th>
                        <th>Product Type</th>
                        <th>Article</th>
                        <th>Cust. Size</th>
                        <th>Manuf. Size</th>
                        <th>Qty Pack. Plan.</th>
                        <th>Inner Pack</th>
                        <th>Status</th>
                        <th>Operator</th>
                        <th>Verified By</th>
                        <th>Verified Date</th>
                        <th>Released By</th>
                        <th>Released Date</th>
                        <th>Metal Find</th>
                        <th>Note</th>
                        <th>Location</th>
                        <th>Find Qty</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('report.exportMetal') }}" id="exportExcel"></a>
@endsection


@include('report/_js_index')
@section('js')
<script type="text/javascript" src="{{ url('js/plugins/tables/datatables/extensions/fixed_columns.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/pages/datatables_extension_fixed_columns.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#exportExcel').attr('href')
                                            + '?date_range=' + $('#date_range').val()
                                            + '&filterby=' + filter
                                            + '&factory_id=' + $('#factory_id').val();
                }
            }
       ],
        ajax: {
            url: $('#form_filter').attr('action'),
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "date_range": $('#date_range').val(),
                    "factory_id": $('#factory_id').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(1).css('min-width','150px');
            $('td', row).eq(15).css('min-width','150px');
            $('td', row).eq(17).css('min-width','150px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
            {data: 'barcode_id', name: 'barcode_id'},
            {data: 'package_number', name: 'package_number'},
            {data: 'po_number', name: 'po_number'},
            {data: 'upc', name: 'upc'},
            {data: 'product_category_detail', name: 'product_category_detail'},
            {data: 'buyer_item', name: 'buyer_item'},
            {data: 'customer_size', name: 'customer_size'},
            {data: 'manufacturing_size', name: 'manufacturing_size'},
            {data: 'pack_plan', name: 'pack_plan'},
            {data: 'inner_pack', name: 'inner_pack'},
            {data: 'status', name: 'status'},
            {data: 'operator', name: 'operator'},
            {data: 'pso_verify', name: 'pso_verify'},
            {data: 'verify_date', name: 'verify_date'},
            {data: 'pso_release', name: 'pso_release'},
            {data: 'release_date', name: 'release_date'},
            {data: 'metfind', name: 'metfind'},
            {data: 'note', name: 'note'},
            {data: 'loct', name: 'loct'},
            {data: 'qty', name: 'qty'}
        ],
    });

    table
    .on( 'preDraw', function () {
        Pace.start();
        loading_process();
    } )
    .on( 'draw.dt', function () {
        Pace.stop();
        $('#table-list').unblock();
    } );

   
    $('#form_filter').submit(function(event){
        event.preventDefault();

        table.clear();
        table.draw();
    });

  
});
</script>
@endsection
