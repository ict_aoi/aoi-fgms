@extends('layouts.app', ['active' => 'dashboardlocator'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">Dashboard Locator</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('report.dashboardlocator') }}"><i class="icon-stack2 position-left"></i> Dashboard Locator</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('report.ajaxGetDataDashboardLocator') }}" id="form_filter">
            @csrf

            <div class="row">
                <div class="form-group col-md-6">
                    <select class="form-control" id="select_area">
                        <option value="" selected disabled>CHOOSE AREA</option>
                        @foreach($areas as $key => $val)
                            <option value="{{ $val->id }}">{{ $val->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-5">
                    <select class="form-control" id="list-locator" disabled>
                        <option value="" selected disabled>CHOOSE RACK</option>
                    </select>
                </div>
                <div class="col-md-1">
                    <button type="submit" class="btn btn-primary">Filter</button>
                </div>
            </div>

        </form>
    </div>
</div>

<div class="breadcrumb-line breadcrumb-line-component">
    <div class="row col-md-2 pull-right">
            <input type="text" id="count_scan" name="count_scan" value="0" style="text-align: center; font-size: 28px;"
               class="form-control" readonly="readonly"
               placeholder="">
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>PO</th>
                        <th>PLAN REF NO.</th>
                        <th>PSDD</th>
                        <th>PSD</th>
                        <th>PFCD</th>
                        <th>NEED CARTON</th>
                        <th>ACTUAL CARTON</th>
                        <th>BALANCE</th>
                        <th>SHARING RACK</th>
                        <th>FINAL INSPECT</th>
                        <th>REMARK</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('report.exportDashboardLocator') }}" id="export_dashboardlocator"></a>
<a href="{{ route('report.ajaxGetListLocator') }}" id="getListLocator"></a>
<a href="{{ route('report.updatePSD') }}" id="updatePSD"></a>
<a href="{{ route('report.ajaxDashboardSum') }}" id="sumDashboard"></a>
@endsection

@include('report/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    //url
    var url = $('#form_filter').attr('action');
    var locator = $('#list-locator').val();
    var filter = null;

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#export_dashboardlocator').attr('href')
                                            + '?locator=' + locator
                                            + '&orderby=' + column_name
                                            + '&direction=' + direction
                                            + '&filterby=' + filter;
                }
            }
       ],
        ajax: {
            url: url,
            type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "locator": locator,
                    "filterby": $('.dataTables_filter input').val(),
                    "_token": _token
                });
            },
            beforeSend: function() {
                $('#table-list').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            complete: function(response) {
                $('#table-list').unblock();
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(6).css('min-width', '50px');
            $('td', row).eq(7).css('min-width', '50px');
            $('td', row).eq(8).css('min-width', '50px');
            $('td', row).eq(9).css('min-width', '150px');
            $('td', row).eq(10).css('min-width', '50px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'po_number', name: 'po_number'},
            {data: 'plan_ref_number', name: 'plan_ref_number'},
            {data: 'psdd', name: 'psdd', sortable: false, orderable: false, searchable: false},
            {data: 'psd', name: 'psd'},
            {data: 'pfcd', name: 'pfcd', sortable: false, orderable: false, searchable: false},
            {data: 'need_carton', name: 'need_carton'},
            {data: 'actual_carton', name: 'actual_carton'},
            {data: 'balance', name: 'balance'},
            {data: 'sharing_rack', name: 'sharing_rack'},
            {data: 'final_inspect', name: 'final_inspect'},
            {data: 'remark', name: 'remark', sortable: false, orderable: false, searchable: false},
        ],
    });
    //end of datatables

    $('#locator').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#getListArea').attr('href'),
            success: function(response){
                $('#select_area').html(response);
            },
            error: function(response) {
                if(response['status'] == 422) {
                    myalert('error', response['responseJSON']);
                }
            }
        });
    })

    $('#select_area').change(function(){
        var areaid = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#getListLocator').attr('href'),
            data: {areaid:areaid},
            success: function(response){
                $('#list-locator').html(response);
                $('#list-locator').prop('disabled',false);
            },
            error: function(response) {
                if(response['status'] == 422) {
                    myalert('error', response['responseJSON']);
                }
            }
        });
    });

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        //check location
        if(locator == '' || locator == null) {
            myalert('error','Please select rack first');
            return false;
        }

        $.ajax({
           type: "POST",
           url: $('#sumDashboard').attr('href'),
           data: {locator: locator},
           success: function(data)
           {
                $('#count_scan').val(data.sum_rack);
           }
         });

        table.draw();
    });

    $('#list-locator').on('change', function(){
        locator = $(this).val();
    });

    $('#table-list').on('blur', '.psd_unfilled', function(){
        var ponumber = $(this).data('ponumber');
        var psd = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updatePSD').attr('href'),
            data: {ponumber:ponumber, psd:psd},
            success: function(response){
                $('#psd_' + ponumber).html(response);
            },
            error: function(response) {
                return false;
            }
        });
    });

    $('#table-list').on('click', '.psd_filled', function() {
        var ponumber = $(this).data('ponumber');
        var psd = $(this).data('psd');
        var string = '<input type="date" data-ponumber="'+ ponumber +'" data-psd="'+psd+'"'+
                    '" class="psd_unfilled" value="'+ psd +'"></input>';
        $('#psd_' + ponumber).html(string);
    });
});
</script>
@endsection
