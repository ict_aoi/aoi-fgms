@extends('layouts.app', ['active' => 'reportlistdeliveryorder'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('report.listrequest') }}"><i class="icon-location4 position-left"></i> Report List Delivery Order</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('report.ajaxGetDataListDelivery') }}" id="form_filter">
            @csrf
            <div class="form-group" id="filter_exfact">
				<label><b>Select Ex-Fact Date</b></label>
				<div class="input-group">
					<span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range" >
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
			</div>
            <div class="form-group hidden" id="filter_planstuffing">
                <label><b>Plan Stuffing Date</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_plan" id="date_plan" >
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>      
            </div>
            <div class="form-group hidden" id="filter_po">
                <label><b>PO Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control " name="po" id="po" >
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>      
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="exfact">Filter by Ex-Fact Date</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="planstuffing">Filter by Plan Stuffing</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="pono">Filter by PO Number</label>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row {{ Auth::user()->admin_role !== 1 ? 'hidden' : '' }}">
            <div class="col-md-2"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->factory_id }}" {{ $val->factory_id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Ex-Fact Date</th>
                        <th>NO TRUCK</th>
                        <th>NAMA SOPIR</th>
                        <th>NO SOPIR</th>
                        <th>JAM</th>
                        <th>NO.SJ</th>
                        <th>STYLE</th>
                        <th>FACTORY</th>
                        <th>QTY</th>
                        <th>CRD</th>
                        <th>PSDD</th>
                        <th>PO</th>
                        <th>DEST</th>
                        <th>CUST</th>
                        <th>SHIPMODE</th>
                        <th>INVOICE</th>
                        <th>FWD</th>
                        <th>SO</th>
                        <th>CTN</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('report.exportListDelivery') }}" id="export_delivery"></a>
@endsection

@include('report/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    //url
    var url = $('#form_filter').attr('action');
    var date = $('#date_range').val();
    var date_plan = $('#date_plan').val();
    var po = $('#po').val();
    var filter = null;


    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'exfact') {
            if($('#filter_exfact').hasClass('hidden')) {
                $('#filter_exfact').removeClass('hidden');
            }

            $('#filter_planstuffing').addClass('hidden');
            $('#filter_po').addClass('hidden');
            
        }
        else if (this.value == 'planstuffing') {
            if($('#filter_planstuffing').hasClass('hidden')) {
                $('#filter_planstuffing').removeClass('hidden');
            }

            $('#filter_exfact').addClass('hidden');
            $('#filter_po').addClass('hidden');
           
        }else if (this.value == 'pono') {
            if($('#filter_po').hasClass('hidden')) {
                $('#filter_po').removeClass('hidden');
            }

            $('#filter_exfact').addClass('hidden');
            $('#filter_planstuffing').addClass('hidden');
            
        }

       

        loading_process();

        table.draw();
    });

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#export_delivery').attr('href')
                                            + '?date_range=' + date
                                            + '&date_plan='+ $('#date_plan').val() 
                                            + '&po='+ $('#po').val() 
                                            + '&orderby=' + column_name
                                            + '&direction=' + direction
                                            + '&radio_status=' + $('input[name=radio_status]:checked').val()
                                            + '&filterby=' + filter
                                            + '&factory_id=' + $('#factory_id').val();
                }
            }
       ],
        ajax: {
            url: url,
            type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "radio_status" : $('input[name=radio_status]:checked').val(),
                    "date_range": $('#date_range').val(),
                    "date_plan" : $('#date_plan').val(),
                    "po" : $('#po').val(),
                    "factory_id": $('#factory_id').val(),
                    "filterby": $('.dataTables_filter input').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
            {data: 'plan_stuffing', name: 'plan_stuffing'},
            {data: 'nopol', name: 'nopol'},
            {data: 'drivers_name', name: 'drivers_name'},
            {data: 'drivers_phone', name: 'drivers_phone'},
            {data: 'jam', name: 'jam'},
            {data: 'delivery_number', name: 'delivery_number'},
            {data: 'style', name: 'style'},
            {data: 'factory_name', name: 'factory_name'},
            {data: 'new_qty', name: 'new_qty'},
            {data: 'crd', name: 'crd'},
            {data: 'po_stat_date_adidas', name: 'po_stat_date_adidas'},
            {data: 'orderno', name: 'orderno'},
            {data: 'country_name', name: 'country_name'},
            {data: 'customer_number', name: 'customer_number'},
            {data: 'shipmode', name: 'shipmode'},
            {data: 'invoice', name: 'invoice'},
            {data: 'fwd', name: 'fwd'},
            {data: 'so', name: 'so'},
            {data: 'total_ctn', name: 'total_ctn'},
        ],
    });
    //end of datatables

    table.on('preDraw', function() {
        Pace.start();
    }).on('draw.dt', function() {
        Pace.stop()
        $('#table-list').unblock();
    });

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        //check location
        if($('#date_range').val() == '') {
            alert('Please select date range first');
            return false;
        }

        loading_process();

        table.draw();
    })

    //
    $('#factory_id').on('change', function() {
        loading_process();
        table.draw();
    });
    $('#date_range').on('change', function(){
        date = $(this).val();
    })
});
</script>
@endsection
