@extends('layouts.app', ['active' => 'reportshippingIn'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">Report Loading Area</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('report.shippingIn') }}"><i class="icon-truck position-left"></i> Report Loading Area</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>BARCODE ID</th>
                        <th>PO NUMBER</th>
                        <th>SIZE</th>
                        <th>DIMENSION (LxWxH(satuan))</th>
                        <th>CHECK IN</th>
                        <!-- <th>CHECK OUT</th> -->
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('report.ajaxGetDataShippingIn') }}" id="data_shipping_in"></a>
<a href="{{ route('report.exportShippingIn') }}" id="export_shipping_in"></a>
@endsection

@include('report/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    //url
    var url = $('#data_shipping_in').attr('href');
    // var date = $('#date_range').val();

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    window.location.href = $('#export_shipping_in').attr('href');
                }
            }
       ],
        order: [[2, "asc"], [1, "asc"], [ 5, "asc" ]],
        ajax: {
            url: url,
            type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "date_range": $('#date_range').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(1).css('min-width', '150px');
            $('td', row).eq(2).css('min-width', '150px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'barcode_package', name: 'barcode_package'},
            {data: 'po_number', name: 'po_summary.po_number'},
            {data: 'customer_size', name: 'package_detail.customer_size'},
            {data: 'dimension', name: 'dimension', sortable: false, orderable: false, searchable: false},
            {data: 'checkin', name: 'package_movements.created_at', sortable: false, orderable: false, searchable: true}
            // {data: 'checkout', name: 'package_movements.created_at', sortable: false, orderable: false, searchable: false}
        ],
    });
    //end of datatables

    // $('#form_filter').submit(function(event) {
    //     event.preventDefault();

    //     //check location
    //     if($('#date_range').val() == '') {
    //         alert('Please select date range first');
    //         return false;
    //     }

    //     table.draw();
    // })

    // //
    // $('#date_range').on('change', function(){
    //     date = $(this).val();
    // })
});
</script>
@endsection
