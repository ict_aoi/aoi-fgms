@extends('layouts.app', ['active' => 'reportshippingcompleted'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <!-- <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">Report List Stuffing</span></h4>
            <p class="position-left"></p>
        </div>
    </div> -->
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('report.shippingCompleted') }}"><i class="icon-truck position-left"></i> Report List Stuffing</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('report.ajaxGetDataShippingCompleted') }}" id="form_filter">
            @csrf
            <div class="form-group">
                <label><b>Select Date</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range" required>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- <div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('report.ajaxGetDataShippingCompleted') }}" id="form_filter">
            <div class="form-group" id="filter_by_date">
                <label><b>Choose LC Date (month/day/year)</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_po">
                <label><b>Choose PO Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="po" id="po" placeholder="PO Number">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_invoice">
                <label><b>Choose Invoice Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="invoice" id="invoice" placeholder="Invoice Number">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="lc">Filter by LC Date</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="po">Filter by PO Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="invoice">Filter by Invoice Number</label>
            </div>
        </form>
    </div>
</div> -->
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row {{ Auth::user()->admin_role !== 1 ? 'hidden' : '' }}">
            <div class="col-md-2"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->factory_id }}" {{ $val->factory_id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>DATE</th>
                        <th>PO NUMBER</th>
                        <th>PLAN NUMBER</th>
                        <th>BRAND</th>
                        <th>ART. NO</th>
                        <th>CUST.NO</th>
                        <th>CTN</th>
                        <th>TOTAL CTN</th>
                        <th>BALANCE</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('report.ajaxGetDataShippingCompleted') }}" id="data_shipping_completed"></a>
<a href="{{ route('report.exportShippingCompleted') }}" id="export_shipping_completed"></a>
@endsection

@include('report/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    //url
    var url = $('#data_shipping_completed').attr('href');
    var date = $('#date_range').val();

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#export_shipping_completed').attr('href')
                                            + '?date_range=' + date
                                            + '&orderby=' + column_name
                                            + '&direction=' + direction
                                            + '&filterby=' + filter
                                            + '&factory_id=' + $('#factory_id').val();
                }
            }
       ],
       lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        // order: [[0, "asc"], [1, "asc"], [ 5, "asc" ]],
        ajax: {
            url: url,
            type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "date_range": $('#date_range').val(),
                    "factory_id": $('#factory_id').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(1).css('min-width', '150px');
            $('td', row).eq(2).css('min-width', '150px');
        },
        footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // computing column Total of the complete result
            // var total_qty = api
            //     .column( 7 )
            //     .data()
            //     .reduce( function (a, b) {
            //         return intVal(a) + intVal(b);
            //     }, 0 );

            // var total_cbm = api
            //     .column( 8 )
            //     .data()
            //     .reduce( function (a, b) {
            //         return intVal(a) + intVal(b);
            //     }, 0 );

            var total_ctn = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            var total_ctn_all = api
                .column( 8 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            var total_balance = api
                .column( 9 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer by showing the total with the reference of the column index
            // $( api.column( 0 ).footer() ).html('Total');
            // $( api.column( 7 ).footer() ).html(total_qty);
            // $( api.column( 8 ).footer() ).html(total_cbm.toFixed(3));
            $( api.column( 7 ).footer() ).html(total_ctn);
            $( api.column( 8 ).footer() ).html(total_ctn_all);
            $( api.column( 9 ).footer() ).html(total_balance);

        },
        columnDefs: [
            {
                className: 'dt-center'
            },
             {
             'targets': 9,
             'className': 'text-center',
            },
             {
             'targets': [7,8],
             'className': 'text-right',
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'checkout', name: 'checkout'},
            {data: 'po_number', name: 'po_number'},
            {data: 'plan_ref_number', name: 'plan_ref_number'},
            {data: 'brand', name: 'brand'},
            {data: 'buyer_item', name: 'buyer_item'},
            {data: 'customer_number', name: 'customer_number'},
            {data: 'ctn', name: 'ctn', sortable: false, orderable: false, searchable: false},
            {data: 'total_ctn', name: 'total_ctn', sortable: false, orderable: false, searchable: false},
            {data: 'balance', name: 'balance'}
        ],
    });
    //end of datatables
    //
    table
    .on( 'preDraw', function () {
        Pace.start();
    } )
    .on( 'draw.dt', function () {
        Pace.stop();
        $('#table-list').unblock();
    } );

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        //check location
        if($('#date_range').val() == '') {
            alert('Please select date range first');
            return false;
        }

        loading();

        table.draw();
    })

    // loading
    function loading() {
        $('#table-list').block({
            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: '10px 15px',
                color: '#fff',
                width: 'auto',
                '-webkit-border-radius': 2,
                '-moz-border-radius': 2,
                backgroundColor: '#333'
            }
        });
    }

    //
    $('#factory_id').on('change', function() {
        loading();
        table.draw();
    });
    $('#date_range').on('change', function(){
        date = $(this).val();
    })
});
</script>
@endsection
