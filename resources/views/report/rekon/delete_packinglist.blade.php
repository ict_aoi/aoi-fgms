@extends('layouts.app', ['active' => 'rekondeletepackinglist'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">REKON DELETE PACKINGLIST</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#" id="form_invoice"><i class="icon-location4 position-left"></i> Rekon Delete Packinglist</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
	<div class="panel-body">
		<form action="{{ route('report.Accgetdeletepl') }}" id="form_filter">
			@csrf
            <div class="form-group" id="filter_date">
                <label><b>Select Date</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date" id="date">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
		</form>
	</div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Barcode</th>
                        <th>Style</th>
                        <th>Article</th>
                        <th>PO Number</th>
                        <th>Plan Ref</th>
                        <th>Brand</th>
                        <th>Product Ctg.</th>
                        <th>Customer Size</th>
                        <th>Manufacturing Size</th>
                        <th>Qty Pack</th>
                        <th>Location</th>
                        <th>Check In</th>
                        <th>Item Code</th>
                        <th>Factory</th>
                        <th>Qty Ratio</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('report.Accexptdeletepl') }}" id="exportDel"></a>
@endsection

@include('report/_js_index')

@section('js')
<script type="text/javascript">
$(document).ready(function(){
    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#exportDel').attr('href')
                                            + '?date_range=' + $('#date').val()
                                            + '&orderby=' + column_name
                                            + '&direction=' + direction
                                            + '&filterby=' + filter;
                }
            }
       ],
        ajax: {
            url: $('#form_filter').attr('action'),
            type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "date": $('#date').val(),
                    // "factory_id": $('#factory_id').val(),
                    // "filterby": $('.dataTables_filter input').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ], 
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'barcode_id', name: 'barcode_id'},
            {data: 'upc', name: 'upc'},
            {data: 'buyer_item', name: 'buyer_item'},
            {data: 'po_number', name: 'po_number'},
            {data: 'plan_ref_number', name: 'plan_ref_number'},
            {data: 'brand', name: 'brand'},
            {data: 'product_category_detail', name: 'product_category_detail'},
            {data: 'customer_size', name: 'customer_size'},
            {data: 'manufacturing_size', name: 'manufacturing_size'},
            {data: 'qty_pack', name: 'qty_pack'},
            {data: 'location', name: 'location',sortable: false, orderable: false, searchable: false},
            {data: 'checkout', name: 'checkout'},
            {data: 'item_code', name: 'item_code'},
            {data: 'factory_id', name: 'factory_id'},
            {data: 'inner_pack', name: 'inner_pack'}
        ],
    });

    table
    .on( 'preDraw', function () {
        Pace.start();
        loading_process();
    } )
    .on( 'draw.dt', function () {
        Pace.stop();
        $('#table-list').unblock();
    } );

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        //check location
        if($('#date').val() == '') {
            alert('Please select date range first');
            return false;
        }

        table.draw();
    })
});
</script>
@endsection


