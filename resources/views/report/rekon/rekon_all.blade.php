@extends('layouts.app', ['active' => 'rekonall'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">REPORT ALL</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#" id="form_invoice"><i class="icon-location4 position-left"></i> Report All</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')


<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-lg-1"></div>
            <div class="col-lg-2">
                <center>
                    <button class="btn btn-primary" id="rek_inv">REPORT INVENTORY</button>
                </center>
            </div>

            <div class="col-lg-2">
                <center>
                    <button class="btn btn-success" id="rek_ship">REPORT SHIPMENT</button>
                </center>
            </div>

            <div class="col-lg-2">
                <center>
                    <button class="btn btn-info" id="rek_bts">REPORT BACKTOSEWING</button>
                </center>
            </div>

            <div class="col-lg-2">
                <center>
                    <button class="btn btn-basic" id="rek_bal">REPORT BALANCE STOCK</button>
                </center>
            </div>

            <div class="col-lg-2">
                <center>
                    <button class="btn btn-danger" id="rek_del">REPORT DELETE PACKINGLIST</button>
                </center>
            </div>
        </div>
    </div>
</div>
<a href="{{ route('report.rekonInv') }}" id="inv"></a>
<a href="{{ route('report.rekonShip') }}" id="ship"></a>
<a href="{{ route('report.rekonBts') }}" id="bts"></a>
<a href="{{ route('report.rekonBalc') }}" id="balc"></a>
<a href="{{ route('report.rekonDel') }}" id="del"></a>
@endsection

@include('report/_js_index')

@section('js')
<script type="text/javascript">

    $('#rek_inv').click(function(){
        window.location.href = $('#inv').attr('href');
    });


    $('#rek_ship').click(function(){
        window.location.href = $('#ship').attr('href');
    });

    $('#rek_bts').click(function(){
        window.location.href = $('#bts').attr('href');
    });

    $('#rek_bal').click(function(){
        window.location.href = $('#balc').attr('href');
    });

    $('#rek_del').click(function(){
        window.location.href = $('#del').attr('href');
    });

</script>
@endsection


