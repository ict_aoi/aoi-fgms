@extends('layouts.app', ['active' => 'reportpackingcalculate'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <!-- <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">Report Packing Calculate</span></h4>
            <p class="position-left"></p>
        </div>
    </div> -->
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('report.packingcalculate') }}"><i class="icon-dropbox position-left"></i> Report Packing Calculate</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('report.ajaxGetDataPackingCalculate') }}" id="form_filter">
            <div class="form-group" id="filter_by_lcdate">
                <label><b>Choose LC Date (month/day/year)</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_podd">
                <label><b>Choose PODD (month/day/year)</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range_podd" id="date_range_podd">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_po">
                <label><b>Choose PO Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="po" id="po" placeholder="PO Number">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_invoice">
                <label><b>Choose Invoice Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="invoice" id="invoice" placeholder="Invoice Number">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="lc">Filter by LC Date</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="po">Filter by PO Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="invoice">Filter by Invoice Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="podd">Filter by PODD</label>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-md-10"></div>
    <div class="col-md-2 pull-right">
        <input type="text" id="cbm_detail_value" name="cbm_detail_value" value="0" style="text-align: center; font-size: 28px;"
               class="form-control" readonly="readonly"
               placeholder="">
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
            <div class="row {{ Auth::user()->admin_role !== 1 || Auth::user()->admin_exim !== 1 ? 'hidden' : '' }}">
        <!-- if admin exim = 1 -->

                <div class="col-md-2">
                    <input type="text" class="form-control" id="noinvoice" name="noinvoice" placeholder="No Invoice" readonly="readonly">
                </div>

                <div class="col-md-2">
                    <select class="form-control" name="factory_id" id="factory_id">
                        @foreach($factory as $key => $val)
                            <option value="{{ $val->factory_id }}" {{ $val->factory_id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        <span class="help-block info-check">*No Checked</span>
        <hr>
        <!--  -->
        <div class="table-responsive">
            <table class="table datatable-button-html5-basic" id="table-list">
                <thead>
                    <tr>
                        <th class="notexport">#</th>
                        <th>PO Number</th>
                        <th>Plan Ref Number</th>
                        <th>Invoice Number</th>
                        <th>Ordered Date</th>
                        <th>Promised Date</th>
                        <th>Gross</th>
                        <th>Net</th>
                        <th>CBM</th>
                        <th>CTN</th>
                        <th>Qty</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('report.exportPackingCalculate') }}" id="export_packing"></a>
<a href="{{ route('report.updateInvoice') }}" id="updateInvoice"></a>
@endsection

@include('report/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    //url
    var url = $('#form_filter').attr('action');
    var date = $('#date_range').val();
    var date_podd = $('#date_range_podd').val();
    var filter = null;

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...'
            // lengthMenu: '<span>Show:</span> _MENU_'
            // paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    // var po_number = $('#po').val();
    // var radio_status = $('input[name=radio_status]:checked').val();

    var table = $('#table-list').DataTable({
        "paging": false,
        "info": false,
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                // exportOptions: {
                //     columns: 'not(.notexport)'
                // },
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[1].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#export_packing').attr('href')
                                            + '?date_range=' + date
                                            + '&date_range_podd=' + date_podd
                                            + '&orderby=' + column_name
                                            + '&direction=' + direction
                                            + '&po=' + $('#po').val()
                                            + '&invoice=' + $('#invoice').val()
                                            + '&radio_status=' + $('input[name=radio_status]:checked').val()
                                            + '&filterby=' + filter
                                            + '&factory_id=' + $('#factory_id').val();
                }
            }
       ],
        ajax: {
            url: url,
            type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "radio_status": $('input[name=radio_status]:checked').val(),
                    "po": $('#po').val(),
                    "invoice": $('#invoice').val(),
                    "date_range": $('#date_range').val(),
                    "date_range_podd": $('#date_range_podd').val(),
                    "factory_id": $('#factory_id').val(),
                    "filterby": $('.dataTables_filter input').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            // $('td', row).eq(0).html(value);
            $('td', row).eq(1).css('min-width', '150px');
            $('td', row).eq(2).css('min-width', '150px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            // {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
            {data: 'po_number', name: 'po_number'},
            {data: 'plan_ref_number', name: 'plan_ref_number'},
            {data: 'invoice', name: 'invoice'},
            {data: 'dateordered', name: 'dateordered'},
            {data: 'datepromised', name: 'datepromised'},
            {data: 'total_gross', name: 'total_gross', sortable: false, orderable: false, searchable: false},
            {data: 'total_net', name: 'total_net'},
            {data: 'cbm', name: 'cbm'},
            {data: 'total_ctn', name: 'total_ctn'},
            {data: 'total_item_qty', name: 'total_item_qty'}
            // {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
    });

   table
    .on( 'preDraw', function () {
        Pace.start();
    } )
    .on( 'draw.dt', function () {
        Pace.stop();
        $('#table-list').unblock();
    } );

    //end of datatables
    //
    function sel(source)
    {
      checkboxes = document.getElementsByName('selector[]');
      for(var i in checkboxes)
      checkboxes[i].checked = source.checked;
    }

    function calculate() {

        var total = 0;

        var info = '*No Checked';

        var po_ = [];

        var arr = $.map($('.clplanref:checked'), function(e, i) {
            return +e.value;
        });

        // $('span').text('the checked values are: ' + arr.join(','));

        for (var i=0; i<arr.length; i++){
            total += parseFloat(arr[i]);
        }

        if (total>0) {
            $('#noinvoice').removeAttr('readonly');
            $('#noinvoice').val('');
        }else{
            $('#noinvoice').attr('readonly', true);
            $('#noinvoice').val('');
        }


        $('.clplanref:checked').each(function(e, i) {
            $('#noinvoice').val($(this).data('invoice'));
            po_.push($(this).data('po'));
        });

        // info
        var vals = "";

        for (var j=0; j<po_.length; j++){
                vals +=", "+po_[j];
        }

        if (vals) vals = vals.substring(1);


        if (arr.length > 0) {
            info = '*'+arr.length+' Plan Ref Number Checked ( '+vals+' )';
        }

        $('.info-check').html(info);

        $('#cbm_detail_value').val(total.toFixed(3));
    }

    // calculate();

    $('div').delegate('input:checkbox', 'click', calculate);

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        //check location
        if($('#date_range').val() == '') {
            alert('Please select date range first');
            return false;
        }

        loading();

        table.draw();
    })

    //
    $('#factory_id').on('change', function() {
        loading();
       table.draw();
    });

    $('#date_range').on('change', function(){
        date = $(this).val();
    })

    $('#date_range_podd').on('change', function(){
        date_podd = $(this).val();
    })

     //choose filter
    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'po') {
            if($('#filter_by_po').hasClass('hidden')) {
                $('#filter_by_po').removeClass('hidden');
            }

            $('#filter_by_lcdate').addClass('hidden');
            $('#filter_by_invoice').addClass('hidden');
            $('#filter_by_podd').addClass('hidden');
        }
        else if (this.value == 'lc') {
            if($('#filter_by_lcdate').hasClass('hidden')) {
                $('#filter_by_lcdate').removeClass('hidden');
            }

            $('#filter_by_po').addClass('hidden');
            $('#filter_by_invoice').addClass('hidden');
            $('#filter_by_podd').addClass('hidden');
        }else if (this.value == 'invoice') {
            if($('#filter_by_invoice').hasClass('hidden')) {
                $('#filter_by_invoice').removeClass('hidden');
            }

            $('#filter_by_po').addClass('hidden');
            $('#filter_by_lcdate').addClass('hidden');
            $('#filter_by_podd').addClass('hidden');
        }else if (this.value == 'podd') {
            if($('#filter_by_podd').hasClass('hidden')) {
                $('#filter_by_podd').removeClass('hidden');
            }

            $('#filter_by_po').addClass('hidden');
            $('#filter_by_lcdate').addClass('hidden');
            $('#filter_by_invoice').addClass('hidden');
        }

        $('#cbm_detail_value').val(0);

        loading();

        table.draw();
    });
});

$('#noinvoice').keyup(function() {
    this.value = this.value.toUpperCase();
});


$('#noinvoice').on('blur', function(){
    var data = [];
    var invoice = $(this).val();

    $('.clplanref:checked').each(function() {
        data.push({
            ponumber: $(this).data('po'),
            planrefnumber: $(this).data('planrefnumber')
            });
    });

    if (data.length > 0) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateInvoice').attr('href'),
            data: {data:data, invoice:invoice},
            success: function(response){
                $('#noinvoice').val('');
                $('.info-check').html('*No Checked');
                $('#cbm_detail_value').val(0);
                $('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });
    }
});

// loading
function loading() {
    $('#table-list').block({
        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: '10px 15px',
            color: '#fff',
            width: 'auto',
            '-webkit-border-radius': 2,
            '-moz-border-radius': 2,
            backgroundColor: '#333'
        }
    });
}

</script>
@endsection
