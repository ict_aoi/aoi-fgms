@extends('layouts.app', ['active' => 'reportinvoice'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">REPORT INVOICE</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#" id="form_invoice"><i class="icon-location4 position-left"></i> Report Invoice</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
	<div class="panel-body">
		<form action="{{ route('report.getReportInvoice') }}" id="form_filter">
			@csrf
			<div class="form-group" id="filter_by_invoice">
                <label><b>Invoice Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="invoice_no" id="invoice_no">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden"  id="filter_by_orderno">
                <label><b>PO Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="po_number" id="po_number" >
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_stuffing">
                <label><b>Plan Stuffing</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="planstuff" id="planstuff">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_podd">
                <label><b>PODD Check</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="podd" id="podd">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="invoice">Filter by Invoice Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="po">Filter by PO Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="plan">Filter by Plan Stuffing</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="pdcheck">Filter by PODD</label>
            </div>
		</form>
	</div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row {{ Auth::user()->admin_role !== 1 ? 'hidden' : '' }}">
            <div class="col-md-2"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->factory_id }}" {{ $val->factory_id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>PO Number</th>
                        <th>Invoice</th>
                        <th>Plan Stuffing</th>
                        <th>SO</th>
                        <th>FWD</th>
                        <th>Ship Mode</th>
                        <th>Cust. No.</th>
                        <th>Destination</th>
                        <th>QTY</th>
                        <th>CTN</th>
                        <th>CBM</th>
                        <th>GW</th>
                        <th>NW</th>
                        <th>Remark</th> 
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('report.exportReportInvoice') }}" id="ExportDetail"></a>
@endsection


@include('report/_js_index')

@section('js')
<script type="text/javascript">
$(document).ready(function(){
    var url = $('#form_filter').attr('action');
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#ExportDetail').attr('href')
                                            + '?po_number=' + $('#po_number').val()
                                            + '&invoice=' + $('#invoice_no').val()
                                            + '&exfact=' + $('#planstuff').val()
                                            + '&podd=' + $('#podd').val()
                                            + '&factory=' + $('#factory_id').val()
                                            + '&radio=' + $('input[name=radio_status]:checked').val();
                }
            }
       ],
        ajax: {
            url: url,
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "radio": $('input[name=radio_status]:checked').val(),
                    "po_number": $('#po_number').val(),
                    "invoice": $('#invoice_no').val(),
                    "exfact": $('#planstuff').val(),
                    "podd": $('#podd').val(),
                    "factory_id": $('#factory_id').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'po_number', name: 'po_number'},
            {data: 'invoice', name: 'invoice'},
            {data: 'plan_stuffing', name: 'plan_stuffing'},
            {data: 'so', name: 'so'},
            {data: 'fwd', name: 'fwd'},
            {data: 'shipmode', name: 'shipmode'},
            {data: 'customer_number', name: 'customer_number'},
            {data: 'country_name', name: 'country_name'},
            {data: 'item_qty', name: 'item_qty'},
            {data: 'ctn', name: 'ctn'},
            {data: 'cbm', name: 'cbm'},
            {data: 'gross', name: 'gross'},
            {data: 'net', name: 'net'},
            {data: 'remark', name: 'remark'},
        ],
    });
    //end of datatables

    $('input[type=radio][name=radio_status]').change(function() {
      
        if (this.value == 'invoice') {
            if($('#filter_by_invoice').hasClass('hidden')) {
                $('#filter_by_invoice').removeClass('hidden');
            }

            $('#filter_by_orderno').addClass('hidden');
            $('#filter_by_stuffing').addClass('hidden');
            $('#filter_by_podd').addClass('hidden');

        }else if (this.value == 'po') {
            if($('#filter_by_orderno').hasClass('hidden')) {
                $('#filter_by_orderno').removeClass('hidden');
            }

            $('#filter_by_invoice').addClass('hidden');
            $('#filter_by_stuffing').addClass('hidden');
            $('#filter_by_podd').addClass('hidden');

        }else if (this.value == 'plan') {
            if($('#filter_by_stuffing').hasClass('hidden')) {
                $('#filter_by_stuffing').removeClass('hidden');
            }

            $('#filter_by_invoice').addClass('hidden');
            $('#filter_by_orderno').addClass('hidden');
            $('#filter_by_podd').addClass('hidden');

        }else if (this.value == 'pdcheck') {
            if($('#filter_by_podd').hasClass('hidden')) {
                $('#filter_by_podd').removeClass('hidden');
            }

            $('#filter_by_invoice').addClass('hidden');
            $('#filter_by_orderno').addClass('hidden');
            $('#filter_by_stuffing').addClass('hidden');

        }
        
    });

    table.on('preDraw', function() {
        Pace.start();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        loading_process();
        table.clear();
        table.draw();
    });


   $('#factory_id').on('change', function() {
        loading_process();

        table.draw();
    });

    


});


</script>
@endsection