@extends('layouts.app', ['active' => 'user'])

@section('content')
<div class="breadcrumb-line breadcrumb-line-component">
    <ul class="breadcrumb">
        <li><a href="{{ route('user.index') }}"><i class="icon-home2 position-left"></i>User Management</a></li>
        <li class="active">Create User</li>
    </ul>
</div>

<section class="panel">
    <div class="panel-body loader-area">
        <form action="{{ route('user.store') }}" id="main-form" method="POST" class="form-horizontal" enctype="multipart/form-data">
        {{ csrf_field() }}
            <fieldset>
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel panel-default border">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-semibold">USER FORM</h6>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="control-label text-semibold">NIK :</label>
                                        <input type="text" name="nik" placeholder="Nomor Induk Karyawan" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label text-semibold">NAME :</label>
                                        <input type="text" name="name" placeholder="Name" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label text-semibold">EMAIL :</label>
                                        <input type="email" name="email" placeholder="E-mail" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label text-semibold">PASSWORD :</label>
                                        <input type="password" name="password" placeholder="Password" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel panel-default border">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-semibold">USER ROLE &nbsp; <span class="label label-info heading-text">Mapping User with Role.</span></h6>
                                </div>

                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>SELECT ROLE</th>
                                                </tr>
                                            </thead>
                                            <fieldset>
                                                <tr>
                                                    <td>
                                                        <select class="form-control" name="role_id" required>
                                                            <option></option>
                                                            @foreach($roles as $role)
                                                            <option value="{{ $role->id }}">{{ $role->display_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                </tr>
                                            </fieldset>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel panel-default border">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-semibold">USER FACTORY &nbsp; <span class="label label-info heading-text">Mapping User with Factory.</span></h6>
                                </div>

                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>SELECT FACTORY</th>
                                                </tr>
                                            </thead>
                                            <fieldset>
                                                <tr>
                                                    <td>
                                                        <select class="form-control select2-multi" name="factory" id="factory_id" required="">
                                                            <option value=""></option>
                                                            @foreach($factory as $key => $val)
                                                            <option value="{{ $val->factory_id }}">{{ $val->factory_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                </tr>
                                            </fieldset>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <hr>
            <div class="form-group text-right" style="margin-right: 1%;">
                <a href="{{ route('user.index') }}" class="btn btn-danger btn-sm">BACK <i class="icon-arrow-left16 position-right"></i></a>
                <button type="button" class="btn btn-success" id="btn-save-data">SAVE <i class="icon-floppy-disk position-right"></i></button>
            </div>
        </form>
    </div>
</section>
<a href="{{ route('role.index') }}" id="url_user" class="hidden"></a>
@endsection

@section('js')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ url ('js/plugins/notifications/bootbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/plugins/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/plugins/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/plugins/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/plugins/notifications/pnotify.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function (){

    $("#btn-save-data").on("click", function (event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $("#main-form").attr("action"),
            data: $("#main-form").serialize(),
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $('.loader-area').unblock();
            },
            success: function(response) {
                myalert('success','GOOD');
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        })
    });

});
</script>
@endsection
