@extends('layouts.app', ['active' => 'user'])

@section('content')
	<div class="page-header">
		<div class="page-title">
	        <h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">Account Setting</span></h4>
	        <ul class="breadcrumb position-left">
	        	<li class="active"><a href="{{ route('dashboard') }}">Change Password</a></li>
	        </ul>
      	</div>
	</div>
	<hr>
	<section class="panel">
		<div class="panel-body loader-area">
			<form method="POST" class="form-horizontal" role="form" action="{{ route('user.updatepassword', $user->id) }}" id="main-form">
				{{ csrf_field() }}
		      	<div class="form-group">
		        	<div class="row">
		          		<div class="col-md-6">
		           			<label class="control-label text-uppercase text-semibold">NIK</label>
		            		<input type="text" name="nik" value="{{ $user->nik }}" disabled class="form-control text-capitalize text-semibold">
		          		</div>

				        <div class="col-md-6">
				            <label class="control-label text-uppercase text-semibold">Name</label>
				            <input type="text" name="name" value="{{ $user->name }}" disabled class="form-control text-semibold">
				        </div>
		        	</div>
		      	</div>

		      	<div class="form-group">
		        	<div class="row">
		          		<div class="col-md-6 hidden"> 
				            <label class="control-label text-uppercase text-semibold">Division</label>
				            <input type="text" name="divisi" value="{{ $user->division }}" placeholder="DIVISION" class="form-control text-capitalize text-semibold">
				            <span class="help-block">Test</span>
				        </div> 

			          	<div id="password_old_error" class="col-md-6">
				            <label class="control-label text-uppercase text-semibold">Old Password</label>
				            <input type="password" name="password_old" id="password_old" required placeholder="OLD PASSWORD" class="form-control text-semibold">
		            		<span id="password_new_danger" class="help-block text-danger text-semibold">* Required</span>
		    			</div>
			        </div>
			     </div>

		      	<div class="form-group">
		       		<div class="row">
		        		<div id="password_new_error" class="col-md-6">
				            <label class="control-label text-uppercase text-semibold">New Password</label>
				            <input type="password" name="password_new" id="password_new" required placeholder="NEW PASSWORD" class="form-control text-semibold">
		            		<span id="password_new_danger" class="help-block text-danger text-semibold">* Required</span>
		    			</div>

				        <div id="password_confirm_error" class="col-md-6">
				            <label class="control-label text-uppercase text-semibold">Confirm Password</label>
				            <input type="password" name="password_confirm" id="password_confirm" required placeholder="CONFIRM PASSWORD" class="form-control text-semibold">
				            <span id="password_confirm_danger" class="help-block text-danger text-semibold">* Required</span>
		        		</div>
		    		</div>
		    	</div>
		    	<hr>
				<div class="text-right">
					<a href="{{ route('dashboard') }}" class="btn btn-danger"><i class="icon-close2 position-left"></i> BACK</a>
				    <button type="button" class="btn btn-success" id="btn-save-data">SAVE <i class="icon-add position-right"></i></button>
				</div>
		    </form>
	    </div>
	</section>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function () {
	
	var url = $('#main-form').attr('action');
	console.log(url); 

	$('#btn-save-data').on('click', function (event) { 
	    event.preventDefault(); 
	    bootbox.confirm("Are you sure for change your password ?", function (result) { 
	    	if (result) { 

	    		$.ajaxSetup({
		            headers: {
		                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            }
		        });

        		$.ajax({ 
        			type: "POST", 
        			url: $('#main-form').attr('action'), 
        			data: $('#main-form').serialize(), 
        			beforeSend: function () {
		                $('.loader-area').block({
		                    message: '<i class="icon-spinner4 spinner"></i>',
		                    overlayCSS: {
		                        backgroundColor: '#fff',
		                        opacity: 0.8,
		                        cursor: 'wait'
		                    },
		                    css: {   
		                        border: 0,
		                        padding: 0,
		                        backgroundColor: 'none'
		                    }
		                });
		            },
			        complete: function () { 
			        	$('.loader-area').unblock();
			        }, 
			        success: function (response) { 
			        	myalert('success','GOOD');
			        }, 
          			error: function (response) { 
            			$('.loader-area').unblock();
            			if (response['status'] == 400) { 
				            for (i in response['responseJSON']['errors']) { 
				                $('#' + i + '_error').addClass('has-error'); 
				                $('#' + i + '_danger').text(response['responseJSON']['errors'][i]); 
				            } 
			            } else if (response['status'] == 422) { 
			              $("#alert_error").trigger("click", response['responseJSON']); 
			            } else if (response['status'] == 500) { 
			              $("#alert_error").trigger("click", 'Please Contact Admin for Reset Your Password'); 
			            } 
			        } 
			    }) 
		        .done(function(response){ 
		        	$('#password_old').val(''); 
		        	$('#password_new').val(''); 
		        	$('#password_confirm').val(''); 
		        	$('#password_old_error').removeClass('has-error'); 
		        	$('#password_old_danger').text('*Required'); 
		        	$('#password_new_error').removeClass('has-error'); 
		        	$('#password_new_danger').text('*Required'); 
		        	$('#password_confirm_error').removeClass('has-error'); 
		        	$('#password_confirm_danger').text('*Required'); 
		        }); 
      		} 
    	}); 
  	}); 

});
</script>
@endsection
