@extends('layouts.app', ['active' => 'user'])

@section('page_header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i><span class="text-semibold">USER</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('user.index') }}"><i class="icon-users position-left"></i> User</a></li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="form-group" style="float: right;">
            <a href="{{ route('user.create') }}" class="btn btn-sm btn-default"><span class="text-semibold">Add User</span> <i class="icon-plus2 position-right"></i></a>
        </div>
    </div>
    <div class="panel-body loader-area">
        <table class="table datatable-save-state" id="user_table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>NIK</th>
                    <th>NAME</th>
                    <th>EMAIL</th>
                    <th>ACTION</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<!-- IMPORTANT LINK -->
<a href="{{ route('user.getDatatables') }}" id="user_get_data"></a>
<!-- /IMPORTANT LINK -->
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var url = $('#user_get_data').attr('href');
    var table = $('#user_table').DataTable({
        ajax: url,
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false  },
            {data: 'nik', name: 'nik'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
    });
    //end of datatables

    //delete-user
    $("#user_table").on("click", ".deleteUser", function(event) {
        event.preventDefault();
        var id = $(this).data('userid');
        if(id == 'kosong') {
            return false;
        }
        var token = $(this).data("token");
        bootbox.confirm("Are you sure delete this row ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "user/delete-user/"+id,
                    type: "GET",
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": token,
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $('.loader-area').unblock();
                    },
                    success: function () {
                        myalert('success','Data has been deleted');
                        table.ajax.reload();
                    }
                });
            }
        });
    });

    $("#user_table").on("click", ".ignore-click", function() {
        return false;
    });
    //end of delete-user

});

</script>
@endsection
