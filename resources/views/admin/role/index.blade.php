@extends('layouts.app', ['active' => 'roles'])

@section('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i><span class="text-semibold">ROLE</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('area') }}"><i class="glyphicon glyphicon-repeat position-left"></i> Role</a></li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="form-group" style="float: right;">
            <a href="{{ route('role.create') }}" class="btn btn-default btn-sm"><span class="text-semibold">Add Role</span> <i class="icon-plus3 position-right"></i></a>
        </div>
    </div>
    <div class="panel-body loader-area">
        <table class="table datatable-save-state" id="role_table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>NAME</th>
                    <th>DISPLAY NAME</th>
                    <th>DESCRIPTION</th>
                    <th>CREATED AT</th>
                    <th>ACTION</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<!-- IMPORTANT LINK -->
<a href="{{ route('role.getDatatables') }}" id="role_get_data"></a>
<!-- /IMPORTANT LINK -->
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Search',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var url = $('#role_get_data').attr('href');
    var table = $('#role_table').DataTable({
        ajax: url,
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false  },
            {data: 'name', name: 'name'},
            {data: 'display_name', name: 'display_name'},
            {data: 'description', name: 'description'},
            {data: 'created_at', name: 'created_a'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
    });
    //end of datatables

    //delete-role
    $("#role_table").on('click', '.deleteRole', function(event) {
        event.preventDefault();
        var id = $(this).data('roleid');
        if(id == 'kosong') {
            return false;
        }
        var token = $(this).data("token");
        bootbox.confirm("Are you sure delete this row ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "role/delete-role/"+id,
                    type: "GET",
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": token,
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $('.loader-area').unblock();
                    },
                    success: function () {
                        myalert('success','Data has been deleted');
                        table.ajax.reload();
                    }
                });
            }
        });
    });

    $("#role_table").on("click", ".ignore-click", function() {
        return false;
    });
    //end of delete area

});
</script>
@endsection
