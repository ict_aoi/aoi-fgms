@extends('layouts.app', ['active' => 'roles'])

@section('content')
<div class="breadcrumb-line breadcrumb-line-component">
    <ul class="breadcrumb">
        <li><a href="{{ route('role.index') }}"><i class="icon-home2 position-left"></i>Role Management</a></li>
        <li class="active">Edit Role</li>
    </ul>
</div>

	<section class="panel">
		<div class="panel-body loader-area">
			<form action="{{ route('role.update') }}" id="main-form" method="POST" class="form-horizontal">
    		{{ csrf_field() }}
				<fieldset>
					<div class="col-md-5 col-sm-12">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-default">
									<input type="text" class="hidden" name="id" value="{{ $role['_id'] }}">
									<div class="panel-heading">
										<h6 class="panel-title text-semibold">ROLE FORM</h6>
									</div>

									<div class="panel-body">
										<div class="form-group">
											<label class="control-label text-semibold">NAME :</label>
											<input type="text" name="name" value="{{ $role['name'] }}" placeholder="Name" class="form-control">
										</div>
										<div class="form-group">
											<label class="control-label text-semibold">DISPLAY NAME :</label>
											<input type="text" name="display_name" value="{{ $role['display_name'] }}" placeholder="Display Name" class="form-control text-capitalize">
										</div>
										<div class="form-group">
											<label class="control-label text-semibold">Description :</label>
											<input type="text" name="description" value="{{ $role['description'] }}" placeholder="Description" class="form-control text-capitalize">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7 col-sm-12">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h6 class="panel-title text-semibold">ROLE PERMISSION &nbsp; <span class="label label-info heading-text">Mapping Role with Permission Role.</span></h6>
									</div>

									<div class="panel-body">
										<div class="table-responsive">
											<table class="table">
												<thead width="100%">
													<tr>
														<th>ACCESS RIGHTS</th>
														<th>PERMISSION NAME</th>
														<th>DESCRIPTION</th>
													</tr>
												</thead>
												<tbody id="tbody-role" width="100%;">
													@foreach($permissions as $p)
														@php($isRoles = 0)
													<tr>
														<td>
															@foreach($permission_role as $perol)
																@if($p->id == $perol->permission_id)
																	@php($isRoles = 1)
																	@break
																@endif
	                            							@endforeach

	                            							@if($isRoles == 1) 
																<input type="checkbox" id="active" name="active[]" value="{{ $p->id }}" data-on="success" data-off="default" checked>
	                            							@else
																<input type="checkbox" id="active" name="active[]" value="{{ $p->id }}" data-on="success" data-off="default">
	                            							@endif
														</td>
														<td>{{ $p->display_name }}</td>
														<td>{{ $p->description }}</td>
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</fieldset>	
				<hr>
				<div class="form-group text-right" style="margin-right: 1%;">
					<a href="{{ route('role.index') }}" class="btn btn-danger btn-sm">BACK <i class="icon-arrow-left16 position-right"></i></a>
					<button type="button" class="btn btn-success btn-sm save-data">SAVE <i class="icon-plus-circle2 position-right"></i></button>
				</div>
			</form>	
		</div>
    </section>
@endsection

@section('js')
	<!-- Theme JS files -->
    <script type="text/javascript" src="{{ url('js/plugins/notifications/bootbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/plugins/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/plugins/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/plugins/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/plugins/notifications/pnotify.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function (){

	$(".save-data").on('click', function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $("#main-form").attr("action"),
            data: $("#main-form").serialize(),
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {   
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $('.loader-area').unblock();
            },
            success: function(response) {
            	$("#main-form").trigger("reset");
                myalert('success','GOOD');
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        })
    });

});
</script>
@endsection
