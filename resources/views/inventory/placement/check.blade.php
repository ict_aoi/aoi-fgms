@extends('layouts.app', ['active' => 'inventorystock'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <!-- <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">INVENTORY PLACEMENT</span></h4>
            <p class="position-left"></p>
        </div>
    </div> -->

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('inventory.stock') }}"><i class="icon-home4 position-left"></i>Inventory Placement Check</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
            <div class="row">
                <div class="col-md-10">
                    <input type="text" class="form-control input-new" id="inventory-checkout" placeholder="#Scan ID"></input>
                    <input type="hidden" id="_barcodeid"></input>
                    <input type="hidden" id="_status"></input>
                    <input type="hidden" id="_radio_status" value="onprogress"></input>
                </div>
                <div class="col-md-2">
                    <input type="text" id="count_scan" name="count_scan" value="0" style="text-align: center; font-size: 28px;"
                           class="form-control" readonly="readonly"
                           placeholder="">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3 col-md-offset-9">
                    <input type="text" id="locator" name="locator"
                           class="form-control" readonly="readonly"
                           placeholder="RACK">
                </div>
                <input type="hidden" id="barcode_rack" name="barcode_rack"></input>
            </div>
            <br>
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="onprogress">ON PROGRESS</label>
                        <label class="radio-inline"><input type="radio" name="radio_status" value="sendsewing">BACK TO SEWING</label>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="show-result">
                <thead>
                    <tr>
                        <th>BARCODE ID</th>
                        <th>PLAN NUMBER</th>
                        <th>PO NUMBER</th>
                        <th>SIZE</th>
                        <th>DIMENSION</th>
                        <th style="width:100px">RACK</th>
                        <th style="width:200px">SUGGESTION</th>
                        <th>STATUS</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody >
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- important link -->
<a href="{{ route('inventory.ajaxGetLocatorByArea') }}" id="get-locator-by-area"></a>
<a href="{{ route('inventory.ajaxGetArea') }}" id="get-area"></a>
<a href="{{ route('inventory.ajaxGetLocator') }}" id="get-locator"></a>
<a href="{{ route('inventory.ajaxCheckstock') }}" id="checkout"></a>
@endsection

@section('modal')
<div id="modal_locator_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-lg">
            <div class="modal-body">
                <div class="panel-body">
                    <fieldset>
                        <legend class="text-semibold">
                            <h4 id="title-locator"><i class="position-left"></i>Choose Rack</h4>
                        </legend>
                        <div class="form-group">
                            <label for="select_area">Select Area:</label>
                            <select class="form-control" id="select_area">
                            </select>
                        </div>
                        <div id="list-locator" class="col-lg-12">
                            <!-- template pilihan -->
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#inventory-checkout').focus();

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    //scan package
    $('.input-new').keypress(function(event) {
        if(event.which == 13) {
            event.preventDefault();
            var barcodeid = $('#inventory-checkout').val();
            var radio_status = $('input[name=radio_status]:checked').val();

            if(barcodeid.charAt(0) == 'R' || barcodeid.charAt(0) == 'r') {
                $('#barcode_rack').val(barcodeid);
                var status = 'rack';
            }
            else {
                var status = 'completed';
            }
            $('#_barcodeid').val(barcodeid);
            $('#_status').val(status);

            var rack = $('#locator').val();
            var barcoderack = $('#barcode_rack').val();
            var barcodeid = $('#_barcodeid').val();
            var status = $('#_status').val();

            //cancel
            if($('#inventory-checkout').val() == '') {
                $('#inventory-checkout').val('');
                $('#_barcodeid').val('');
                $('#_status').val('');
                return false;
            }

            //check apakah rack sudah diisi atau belum
            if(barcoderack == '' && $.trim(radio_status) === 'onprogress') {
                myalert('error','Please choose rack first');
                $('#inventory-checkout').val('');
                $('#_barcodeid').val('');
                $('#_status').val('');
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //check apakah yg discan barcode package atau barcode locator
            if(status === 'rack') {
                $.ajax({
                    type: 'post',
                    url: $('#get-locator').attr('href'),
                    data: {barcodeid: barcodeid},
                    beforeSend: function() {
                        $('#inventory-checkout').val('');
                        $('#inventory-checkout').focus();
                        $('#_barcodeid').val('');
                        $('#_status').val('');
                    },
                    success: function(response){
                        $('#locator').val(response);
                        $('_barcodeid').val('');
                        $('_status').val('');
                    },
                    error: function(response){
                        if(response['status'] == 422) {
                            myalert('error',response['responseJSON']);
                        }
                        $('#inventory-checkout').val('');
                        $('#_barcodeid').val('');
                        $('#_status').val('');
                    }
                });
            }
            else {
                //check rack
                if(rack == '' && $.trim(radio_status) === 'onprogress') {
                    myalert('error','Please choose rack first');
                    $('#inventory-checkout').val('');
                    $('#_barcodeid').val('');
                    $('#_status').val('');
                    return false;
                }
                $.ajax({
                    type: 'post',
                    url: $('#checkout').attr('href'),
                    data: {barcodeid: barcodeid, barcode_rack: barcoderack, rack: rack, status: status, radio_status: radio_status},
                    beforeSend: function() {
                        $('#inventory-checkout').val('');
                        $('#inventory-checkout').focus();
                        $('#_barcodeid').val('');
                        $('#_status').val('');
                    },
                    success: function(response){
                        $('#show-result > tbody').prepend(response);
                        var count_s = +$('#count_scan').val()+1;

                        $('#count_scan').val(count_s);
                    },
                    error: function(response){
                        if(response['status'] == 422) {
                            myalert('info',response['responseJSON']);
                        }
                        $('#inventory-checkout').val('');
                        $('#_barcodeid').val('');
                        $('#_status').val('');
                    }
                });
            }
        }
    });

    //
    $('#locator').on('click', function(){
        $('#modal_locator_').modal('show');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#get-area').attr('href'),
            beforeSend: function() {
                $('#modal_locator_ > .modal-content').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response){
                $('#modal_locator_ > .modal-content').unblock();
                $('#select_area').html('');
                $('#select_area').append(response);
            }
        });
    });

    $('#select_area').change(function(){
        var areaid = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#get-locator-by-area').attr('href'),
            data: {areaid:areaid},
            beforeSend: function() {
                $('#modal_locator_ > .modal-content').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response){
                $('#modal_locator_ > .modal-content').unblock();
                $('#list-locator').html(response);
            },
            error: function(response) {
                if(response['status'] == 422) {
                    myalert('error', response['responseJSON']);
                }
            }
        });
    });

    $('#list-locator').on('click','#choose_location', function(){
        var barcode_rack = $(this).data('barcode');
        var rack = $(this).data('code');
        $('#barcode_rack').val(barcode_rack);
        $('#locator').val(rack);
        $('#modal_locator_').modal('hide');
    });


    $('input[name=radio_status]').change(function(event) {
        $('#_radio_status').val($(this).val());
    });

});
</script>
@endsection
