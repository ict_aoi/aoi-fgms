<tr>
    <td>{{ $data->barcode_id }}</td>
    <td>{{ $data->plan_ref_number }}</td>
    <td>{{ $data->po_number }}</td>
    <td>{{ $data->customer_size }}</td>
    <td>{{ $data->length .'x'. $data->width .'x'. $data->height . '('. $data->unit_2 .')' }}</td>
    <td>{{ $rack }}</td>
    <td>
        @php($racklist = array())
        @foreach($rack_suggestion as $rack)
            @php($racklist[] = $rack->code)
        @endforeach
        @php($suggestion = implode(', ',$racklist))
        {{ isset($suggestion) ? $suggestion : '-' }}
    </td>
    <td>
        @if($movement['status_to'] == 'completed')
            @if($movement['department_to'] != 'preparation')
            <span class="label label-success">COMPLETED</span>
            @else
            <span class="label label-primary">PREPARATION</span>
            @endif
        @endif
    </td>
    <td>{{ $movement['created_at'] }}</td>
</tr>
