<div class="table-responsive">
    <table class="table" id="list-location">
    <thead>
        <tr>
            <th>Barcode Rak</th>
            <th>Name</th>
            <th>Rack</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($locators as $key => $val)
            <tr>
                <td>{{ $val->barcode }}</td>
                <td>{{ $val->code }}</td>
                <td>{{ $val->rack }}</td>
                <td>
                    <button type="button" id="choose_location"
                            data-barcode="{{ $val->barcode }}"
                            data-code="{{ $val->code }}"
                            class="btn btn-default">
                            SELECT
                    </button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
</div>
<script type="text/javascript">
    var table = $('#list-location').DataTable({
        "lengthChange": false,
        "pageLength": 5
    });
</script>
