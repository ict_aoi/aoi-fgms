@extends('layouts.app')

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">PLACEMENT</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('inventory.placement') }}"><i class="icon-home4 position-left"></i> Placement</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<form action="{{ route('inventory.ajaxCheckout') }}" id="form-placement">
@csrf
    <div class="panel panel-flat">
        <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" class="form-control input-new" id="inventory-checkout" placeholder="#Scan ID"></input>
                        <input type="hidden" id="_barcodeid"></input>
                        <input type="hidden" id="_status"></input>
                    </div>
                </div>
            </form>
        </div>
        <div class="panel-body">
            <table class="table" id="show-result">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>BARCODE ID</th>
                        <th>LENGTH</th>
                        <th>WIDTH</th>
                        <th>HEIGHT</th>
                        <th>SATUAN</th>
                    </tr>
                </thead>
                <tbody >
                </tbody>
            </table>
        </div>
        <div class="panel-footer">
            <div class="container-fluid">
        		<div class="row">
                    <div class="col-md-3 col-md-offset-8">
                        <input type="text" id="locator" name="locator"
                               class="form-control" readonly="readonly"
                               placeholder="CHOOSE RACK">
                    </div>
                    <input type="hidden" id="barcode_rack" name="barcode_rack"></input>
                    <div class="col-md-1" style="float:right">
                        <button type="submit" class="btn btn-success"> SUBMIT</button>
                    </div>
                </div>
            </div>
    	</div>
    </div>
</form>

<!-- important link -->
<a href="{{ route('inventory.ajaxGetBarcode') }}" id="get-barcode"></a>
<a href="{{ route('inventory.ajaxGetLocator') }}" id="get-locator"></a>
<a href="{{ route('inventory.ajaxGetArea') }}" id="get-area"></a>
@endsection

@section('modal')
<!-- MODAL LOCATOR -->
<div id="modal_locator" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content modal-lg">
      <div class="modal-body">
      <form class="form-horizontal" action="#">
          <div class="panel-body">
              <fieldset>
                  <legend class="text-semibold">
                      <h4 id="modal_title"><i class="icon-file-text2 position-left"></i>CHOOSE RACK</h4>
                  </legend>

                  <div class="col-md-12">
                      <div id="list_area">
                          <div class="form-group">
                              <label class="col-lg-2 control-label">AREA:</label>
                              <div class="col-lg-10">
                                  <select id="area" data-placeholder="select area" name="area" class="select form-control" required></select>
                              </div>
                          </div>

                      </div>
                  </div>

                 <div id="list_loc_"></div>
              </fieldset>
          </div>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- /MODAL LOCATOR -->
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#inventory-checkout').focus();

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    //get barcode from scanner
    function getBarcodeStatus(value, length) {
        var data = new Array();
        var scan_id = value;
        var scan_length = scan_id.length;
        var barcode_length = parseInt(scan_length) - parseInt(length);
        var barcodeid = '';

        for(var i = barcode_length; i < scan_length; i++) {
            barcodeid += scan_id[i];
        }

        if(scan_length <= length && scan_length > 0) {
            var status = 'completed';
        }
        else if(scan_length > length) {
            var current_status = '';
            var current_length = parseInt(scan_length) - parseInt(length);
            for(var i = length + 1; i < current_length; i++) {
                current_status += scan_id[i];
            }

            if(current_status == 'onprogress') {
                var status = 'completed';
            }
            else if(current_status == 'completed') {
                var status = 'onprogress';
            }
            else {
                var status = current_status;
            }
            console.log(status);
        }
        else {
            $('#inventory-checkout').val('');
            return false;
        }

        data[0] = barcodeid;
        data[1] = status;
        return data;
    }

    //scan package
    $('.input-new').keypress(function(event) {
        if(event.which == 13) {
            event.preventDefault();
            var search = $('#inventory-checkout').val();
            var length = search.split('-')[0].length;
            console.log(search);

            var data = getBarcodeStatus(search, length);
            var beauty_search = data[0] + '-' + data[1];
            console.log(beauty_search);

            $('#inventory-checkout').val(beauty_search);
            $('#_barcodeid').val(data[0]);
            $('#_status').val(data[1]);
        }
    });

    var timer;
    var barcode_list_ = new Array();
    $('#inventory-checkout').keyup(function () {
        clearTimeout(timer);
        timer = setTimeout(function (event) {
            var barcodeid = $('#_barcodeid').val();
            var status = $('#_status').val();

            //check apakah search bar sudah diisi atau belum
            if($('#inventory-checkout').val() == '') {
                $('#inventory-checkout').val('');
                return false;
            }

            if(status == 'onprogress') {
                $('#inventory-checkout').val('');
                return false;
            }

            //check apakah barcode sudah ada atau belum
            var check_barcode_ = check_barcode(barcodeid);
            if(!check_barcode_) {
                $('#inventory-checkout').val('');
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: $('#get-barcode').attr('href'),
                data: {barcodeid: barcodeid},
                success: function(response){
                    barcode_list_.push(barcodeid);
                    console.log(barcode_list_);
                    $('#show-result > tbody').append(response);
                },
                error: function(response){
                    if(response['status'] == 422) {
                        console.log(response['responseJSON']);
                    }
                    $('#inventory-checkout').val('');
                }
            }).done(function() {
                $('#inventory-checkout').val('');
                $('#inventory-checkout').focus();
                $('#_barcodeid').val('');
                $('#_status').val('');
            });
        }, 1000);
    });

    //check barcode
    function check_barcode(barcode) {
        for(var i = 0; i < barcode_list_.length; i++) {
            if(barcode == barcode_list_[i]) {
                return false;
            }
        }
        return true;
    };

    //open modal and get areas list
    $('#locator').on('click', function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#get-area').attr('href'),
            success: function(response){
                $('#area').append(response);
            },
            error: function(response){
                if(response['status'] == 422) {
                    console.log(response['responseJSON']);
                }
                $('#inventory-checkout').val('');
            }
        });
        $('#modal_locator').modal('show');
    });

    //get location based on selected area
    $('#area').on('change', function() {
        var areaid = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#get-locator').attr('href'),
            data: {areaid:areaid},
            success: function(response){
                $('#list_loc_').html(response);
            },
            error: function(response){
                if(response['status'] == 422) {
                    console.log(response['responseJSON']);
                }
                $('#inventory-checkout').val('');
            }
        });
    });

    //chose location
    $('#list_loc_').on('click', '#choose_location', function() {
        var barcode_rack = $(this).data('barcode');
        var code = $(this).data('code');
        $('#barcode_rack').val(barcode_rack);
        $('#locator').val(code);
        $('#modal_locator').modal('hide');
    });

    //submit
    $('#form-placement').submit(function(event) {
        event.preventDefault();

        //check location
        if($('#barcode_rack').val() == '') {
            alert('Please choose rack first');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#form-placement').attr('action'),
            data: $('#form-placement').serialize(),
            success: function(response) {
                console.log(response);
                alert('Success');
                $('#form-placement').trigger("reset");
                $('#_barcodeid').val('');
                $('#_status').val('');
                $('#barcode_rack').val('');
                $('#show-result > tbody').html('');
            },
            error: function(response) {
                alert('NOT GOOD');
            }
        });
    });
});
</script>
@endsection
