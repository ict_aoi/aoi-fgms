@extends('layouts.app', ['active' => 'inventoryarea'])

@section('content')
	<br>
<div class="breadcrumb-line breadcrumb-line-component">
    <ul class="breadcrumb">
        <li><a href="{{ route('area') }}"><i class="icon-home2 position-left"></i>Area & Location</a></li>
    </ul>
</div>


<form action="{{ route('location.update') }}" id="main-form" method="POST" class="form-horizontal" enctype="multipart/form-data">
	                    {{ csrf_field() }}
	<section class="panel">
		<div class="panel-body">
				   <div class="adv-table">
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-12">
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default border">
						<div class="panel-heading">
							<h6 class="panel-title">LOCATION FORM<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<div class="heading-elements">
								<ul class="icons-list">
									<li><a data-action="collapse"></a></li>
								</ul>
							</div>
						</div>
                        <input type="text" name="id" class="hidden" value="{{ $location->id }}">
						<div class="panel-body">
														<div><label class="control-label col-md-2">BARCODE</label>
														 <input type="text" name="barcode" class="form-control" id="code" value="{{ $location->barcode }}" readonly>
														</div>
                            <div><label class="control-label">CODE</label>
                             <input type="text" name="code" placeholder="CODE" class="form-control" id="code" value="{{ $location->code }}">
                            </div>
                            <div><label class="control-label">RACK</label>
                             <input type="text" name="rack" placeholder="Rack" class="form-control" id="rack" value="{{ $location->rack }}">
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="form-group text-right" style="margin-top: 10px;">
		<button type="submit" class="btn btn-success btn-lg save-data" name="update">SAVE <i class="icon-floppy-disk position-left"></i></button>
	</div>

</div>
</section>
	<a href="{{ route('role.index') }}" id="url_role" class="hidden"></a>
</form>
@endsection
@section('content-js')

    <script type="text/javascript" src="{{ url ('js/bootbox.js')}}"></script>
<script>
$(".save-data").on("click", function () {
        submitData();
    });


    validator = $("#main-form").validate({
        ignore: 'input[type=hidden], .select2-search__field',
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        errorPlacement: function (error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                }
                else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            }

            else if (element.parent().hasClass('bootstrap-select') ) {
                error.appendTo(element.parent().parent());
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        rules: {

            phone:{
                maxlength:50,
                number:true,
            },
        }
    });


function submitData()
{

    var fd = $("#main-form").serializeArray();
    console.log(fd);
    if ($("#main-form").valid()) {
        $.ajax({
            url:"{{url('/location/update/{id}')}}",
            type: 'POST',
            url: apiurl,
            data: fd,
            beforeSend: function () {
                $('.panel-body').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            success: function (data) {
                    console.log(data);
                    if(data.response.Status == 0)
                    {
                        new PNotify({
                            title: 'success!',
                            text: 'Data berhasil disimpan ke server',
                            addclass: 'bg-success'
                        });
                        $('.panel-body').unblock();
                    }
                    else
                    {
                        if(data.response.Message)
                        {
                            $.each(data.response.Message, function(i, v) {
                                var msg = '<label class="validation-error-label" for="'+i+'">'+v+'</label>';
                                $("input[name="+i+"],select[name="+i+"]").after(msg);
                            });
                            var keys = Object.keys(data.response.Message);
                            $('input[name="'+keys[0]+'"]').focus();
                        }
                        new PNotify({
                            title: 'Failed!',
                            text:  'data Failed to store',
                            addclass: 'bg-danger'
                        });
                        $('.panel-body').unblock();
                    }
            },
            error: function (xhr) {
                console.log(xhr);
                new PNotify({
                            title: 'Failed!',
                            text:  xhr,
                            addclass: 'bg-danger'
                        });
                $('.panel-body').unblock();
            },
            dataType: 'json'
        });
    } else {
        new PNotify({
            title: 'Validasi Error!',
            text: 'Isian data masih ada yang kurang, silahkan perbaiki.',
            addclass: 'bg-danger'
        });
    }
};
</script>
@endsection
