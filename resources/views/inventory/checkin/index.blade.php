@extends('layouts.app', ['active' => 'inventorycheckin'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">INVENTORY CHECK IN</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('inventory.checkin') }}"><i class="icon-home4 position-left"></i>Inventory Check In</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <form action="{{ route('inventory.ajaxSetStatusCheckin') }}" id="form-checkin">
            @csrf
            <div class="row">
                <div class="col-md-10">
                    <input type="text" class="form-control input-new" id="inventory-checkin" name="inventoryCheckin" placeholder="#Scan ID"></input>
                    <input type="hidden" id="_barcodeid"></input>
                    <input type="hidden" id="_status"></input>
                </div>
                <div class="col-md-2">
                    <input type="text" id="count_scan" name="count_scan" value="0" style="text-align: center; font-size: 28px;"
                           class="form-control" readonly="readonly"
                           placeholder="">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="onprogress">ON PROGRESS</label>
                        <label class="radio-inline"><input type="radio" name="radio_status" value="rejected">REJECTED</label>
                        <label class="radio-inline"><input type="radio" name="radio_status" value="cancel">CANCEL</label>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="show-result">
                <thead>
                    <tr>
                        <th>BARCODE ID</th>
                        <th>PLAN NUMBER</th>
                        <th>PO NUMBER</th>
                        <th>DEPARTMENT</th>
                        <th style="width:200px">SUGGESTION</th>
                        <th>STATUS</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('inventory.ajaxSetStatusCheckin') }}" id="set-status-checkin"></a>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#inventory-checkin').focus();

    //get barcode from scanner
    // function getBarcodeStatus(value, length) {
    //     var data = new Array();
    //     var scan_id = value;
    //     var scan_length = scan_id.length;
    //     var barcode_length = parseInt(scan_length) - parseInt(length);
    //     var barcodeid = '';
    //
    //     for(var i = barcode_length; i < scan_length; i++) {
    //         barcodeid += scan_id[i];
    //     }
    //
    //     if(scan_length <= length && scan_length > 0) {
    //         var status = 'onprogress';
    //     }
    //     else if(scan_length > length) {
    //         var current_status = '';
    //         var current_length = parseInt(scan_length) - parseInt(length);
    //         for(var i = length + 1; i < current_length; i++) {
    //             current_status += scan_id[i];
    //         }
    //
    //         if(current_status == 'onprogress') {
    //             var status = 'rejected';
    //         }
    //         else if(current_status == 'rejected') {
    //             var status = 'cancel';
    //         }
    //         else if(current_status == 'cancel') {
    //             var status = 'onprogress';
    //         }
    //         else {
    //             var status = current_status;
    //         }
    //         console.log(status);
    //     }
    //     else {
    //         $('#inventory-checkin').val('');
    //         return false;
    //     }
    //
    //     data[0] = barcodeid;
    //     data[1] = status;
    //     return data;
    // }

    //scan package
    $('.input-new').keypress(function(event) {
        if(event.which == 13) {
            event.preventDefault();
            var barcodeid = $('#inventory-checkin').val();
            var status = $('input[name=radio_status]:checked').val();

            $('#_barcodeid').val(barcodeid);
            $('#_status').val(status);if($('#inventory-checkin').val() == '') {
                $('#inventory-checkin').val('');
                $('#inventory-checkin').focus();
                $('#_barcodeid').val('');
                $('#_status').val('');
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: $('#set-status-checkin').attr('href'),
                data: {barcodeid: barcodeid, status: status},
                beforeSend: function() {
                    $('#inventory-checkin').val('');
                    $('#inventory-checkin').focus();
                    $('#_barcodeid').val('');
                    $('#_status').val('');
                },
                success: function(response){
                    $('#show-result > tbody').prepend(response);
                    var count_s = +$('#count_scan').val()+1;

                    $('#count_scan').val(count_s);
                },
                error: function(response){
                    if(response['status'] == 422) {
                        myalert('error', response['responseJSON']);
                    }
                    $('#inventory-checkin').val('');
                }
            });
        }
    });
});
</script>
@endsection
