<style type="text/css">

@page {
    margin: 25 20 0 45;
}

@font-face {
    font-family: 'impact' !important;
    src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
}

.template {
   font-size: 40 !important;
   font-family: 'impact';
}

.page-break {
    page-break-after: always;
}

.print-friendly {
    height: 15%;
    width: 100%;
}

.print-friendly-single {
    height: 15%;
    width: 100%;
}

table.print-friendly tr td, table.print-friendly tr th,
table.print-friendly-single tr td, table.print-friendly-single tr th, {
    page-break-inside: avoid;
    padding: 20px;
}

.barcode {
    line-height: 18px;
    padding: 10px;
}

.img_barcode {
    display: block;
    padding: 0px;
}

.img_barcode > img {
    width: 180px;
    height:50px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 14px !important;
}

.area_barcode {
    width: 50%;
}

</style>

@if(isset($size_template))
	@if($size_template == 'l')
	<div class="template" style="font-size: 40 !important">
	@elseif($size_template == 'm')
	<div class="template" style="font-size: 30 !important">
	@elseif($size_template == 's')
	<div class="template" style="font-size: 20 !important">
	@else
	<div class="template" style="font-size: 40 !important">
	@endif
@else
	<div class="template" style="font-size: 40 !important">
@endif

@if(isset($data))
    @if(count($data) == 1)
    <table class="print-friendly-single">
        <tr>
            <td>
                <!-- <div class="barcode" style="margin-top:0px; margin-bottom:10px;line-height:12px"> -->
                <div class="barcode">
                    <div class="img_barcode">
                        <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data[0]->barcode, 'C128',2,35) }}" alt="barcode"   />
                    </div>
                    <span class="barcode_number">{{ isset($data[0]) ? $data[0]->barcode : null }}</span >
                </div>
            </td>
        </tr>
    </table>
    @else
        @php($chunk = array_chunk($data->toArray(), 2))
        @foreach($chunk as $key => $value)
        <table class="print-friendly">
            <tr>
                <td class="area_barcode">
                    <!-- <div class="barcode" style="margin-top:0px; margin-bottom:10px;line-height:12px"> -->
                    <div class="barcode">
                        <!-- <div class="img_barcode" style="display:block;padding:0px"> -->
                        @if(isset($value[0]))
                        <h4>{{ isset($value[0]) ? $value[0]->code : null }}</h4>
                        <div class="img_barcode">
                            <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[0]->barcode, 'C128',2,35) }}" alt="barcode"   />
                        </div>
                        <span class="barcode_number">{{ isset($value[0]) ? $value[0]->barcode : null }}</span >
                        @endif
                    </div>
                </td>
                <td class="area_barcode">
                    <!-- <div class="barcode" style="margin-top:0px; margin-bottom:10px;line-height:12px"> -->
                    <div class="barcode">
                        <!-- <div class="img_barcode" style="display:block;padding:0px"> -->
                        @if(isset($value[1]))
                        <h4>{{ isset($value[1]) ? $value[1]->code : null }}</h4>
                        <div class="img_barcode">
                            <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[1]->barcode, 'C128',2,35) }}" alt="barcode"   />
                        </div>
                        <span class="barcode_number">{{ isset($value[1]) ? $value[1]->barcode : null }}</span >
                        @endif
                    </div>
                </td>
            </tr>
        </table>

        @if(($key + 1) % 3 == 0 && ($key + 1) != count($chunk))
        <div class="page-break"></div>
        @endif
        @endforeach
    @endif
@else
<table style="width:100%;">
    <tr>
        <td colspan="8">*ONLY BARCODE</td>
        <td></td>
    </tr>
</table>
@endif
</div>
