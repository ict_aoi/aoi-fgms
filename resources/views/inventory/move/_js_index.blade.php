@section('js_extension')
<script type="text/javascript" src="{{url('js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/plugins/notifications/sweet_alert.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{ url('js/plugins/pickers/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ url('js/plugins/pickers/anytime.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/plugins/pickers/pickadate/picker.js') }}"></script>
<script type="text/javascript" src="{{ url('js/plugins/pickers/pickadate/picker.date.js') }}"></script>
<script type="text/javascript" src="{{ url('js/plugins/pickers/pickadate/picker.time.js') }}"></script>
<script type="text/javascript" src="{{ url('js/plugins/pickers/pickadate/legacy.js') }}"></script>

<script type="text/javascript" src="{{ url('js/pages/picker_date.js') }}"></script>
@endsection