@extends('layouts.app', ['active' => 'moveinventory'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">HAND OVER SETTING</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#"><i class="icon-location4 position-left"></i> Hand Over Setting</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('handover.ajaxGetPo') }}" id="form_filter">
            @csrf
            <div class="form-group">
                <label><b>PO Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="po_number" id="po_number" required>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>PO Number</th>
                        <th>Plan Ref</th>
                        <th>Ctn</th>
                        <th>Factory</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection

@section('modal')
<div id="modal_handover" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-lg">
            <div class="modal-body">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <form action="{{ route('handover.setHandover') }}" id="form-handover" method="POST">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>PO Number</label>
                                    <input type="text" name="txpo_number" id="txpo_number" class="form-control txpo_number" readonly="">
                                </div>
                                <div class="col-md-6">
                                    <label>Plan Ref Number</label>
                                    <input type="text" name="txplan_ref" id="txplan_ref" class="form-control txplan_ref" readonly="">
                                    <input type="text" name="txfactory" id="txfactory" class="hidden">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Nomor BC</label>
                                    <input type="text" name="nobc" id="nobc" class="form-control" required="">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary">SAVE</button>
                                    <button type="button" class="btn btn-default" onclick="dispose(this);">Close</button>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@include('inventory/move/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function(){


    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        searching : false,
        paging : false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            url: $('#form_filter').attr('action'),
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "po_number": $('#po_number').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td',row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data:null, sortable: false, orderalbe: false, searcable: false},
            {data:'po_number', name:'po_number'},
            {data:'plan_ref_number', name:'plan_ref_number'},
            {data:'ctn', name:'ctn'},
            {data:'factory_id', name:'factory_id'},
            {data:'action', name:'action'},
        ],
    });

    table.on('preDraw', function() {
        Pace.start();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'get',
            url : $('#form_filter').attr('action'),
            data : {po_number:$('#po_number').val()},
            beforeSend:function(){

                loading_process();
            },
            success:function(response){
                $('#table-list').unblock();
                
                
                table.clear();
                table.draw();
            },
            error:function(response){
                myalert('error',response['responseJSON']);
            }

        });

    });

    $('#form-handover').submit(function(event){
        event.preventDefault();

        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'POST',
            url : $('#form-handover').attr('action'),
            data : {po_number:$('#txpo_number').val(),plan_ref_number:$('#txplan_ref').val(),factory_id:$('#txfactory').val(),nobc:$('#nobc').val()},
            beforeSend:function(){

                loading_process();
            },
            success:function(response){
               var data_response = response.data;
                if (data_response.status == 200) {
                    myalert('success', data_response.output);
                    $('#form_filter').submit();
                    $('#table-list').unblock();
                    dispose();
                }else{
                    myalert('error', data_response.output);
                }

                
            },
            error:function(response){
                var data_response = response.data;

                if (data_response.status == 422) {
                    myalert('error', data_response.output);
                }else{
                    myalert('error', data_response.output);
                }

            }

        });
    });


});

function setpo(e){
    var po = e.getAttribute('data-po');
    var plan_ref_number = e.getAttribute('data-plan_ref');
    var factory_id = e.getAttribute('data-factory');

    $('#txpo_number').val(po);
    $('#txplan_ref').val(plan_ref_number);
    $('#txfactory').val(factory_id);
    $('#modal_handover').modal('show');
}

function dispose(){
    $('#txpo_number').val('');
    $('#txplan_ref').val('');
    $('#nobc').val('');
    $('#txfactory').val('');
    $('#modal_handover').modal('hide');
}




</script>
@endsection