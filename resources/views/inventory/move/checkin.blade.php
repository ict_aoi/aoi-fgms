@extends('layouts.app', ['active' => 'handover_checkin'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">HAND OVER CHECK IN</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#"><i class="icon-home4 position-left"></i>Hand Over Check In</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <form action="{{ route('handover.ajaxCheckin') }}" id="form-checkin">
            @csrf
            <div class="row">
                <div class="col-md-10">
                    <input type="text" class="form-control input-new" id="inventory-checkin" name="inventoryCheckIn" placeholder="#Scan ID"></input>
                    <input type="hidden" id="_barcodeid"></input>
                </div>
                <div class="col-md-2">
                    <input type="text" id="count_scan" name="count_scan" value="0" style="text-align: center; font-size: 28px;"
                           class="form-control" readonly="readonly"
                           placeholder="">
                </div>
            </div>
            <br>
            
        </form>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="show-result">
                <thead>
                    <tr>
                        <th>BARCODE ID</th>
                        <th>PLAN NUMBER</th>
                        <th>PO NUMBER</th>
                        <th>DEPARTMENT</th>
                        <th>STATUS</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#inventory-checkout').focus();

   
    $('.input-new').keypress(function(event) {
        if(event.which == 13) {
            event.preventDefault();
            var barcodeid = $('#inventory-checkin').val();

            
            $('#_barcodeid').val(barcodeid);
            if($('#inventory-checkin').val() == '') {
                $('#inventory-checkin').val('');
                $('#inventory-checkin').focus();
                $('#_barcodeid').val('');
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: $('#form-checkin').attr('action'),
                data: {barcode_id: barcodeid},
                beforeSend: function() {
                    $('#inventory-checkin').val('');
                    $('#inventory-checkin').focus();
                    $('#_barcodeid').val('');
                },
                success: function(response){
                    $('#show-result > tbody').prepend(response);
                    var count_s = +$('#count_scan').val()+1;

                    $('#count_scan').val(count_s);
                },
                error: function(response){
                    if(response['status'] == 422) {
                        myalert('error', response['responseJSON']);
                    }
                    $('#inventory-checkin').val('');
                }
            });
        }
    });
});
</script>
@endsection
