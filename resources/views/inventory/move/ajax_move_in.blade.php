<tr>
    <td>{{ $data['barcode_id'] }}</td>
    <td>{{ $data['plan_ref_number'] }}</td>
    <td>{{ $data['po_number'] }}</td>
    <td><span class="label label-flat border-grey text-grey-600">{{ $data['department_to'] }}</span></td>
    <td>
        @if($data['status_to'] == 'onprogress')
        <span class="label label-primary">Onprogress</span>
        @elseif($data['status_to'] == 'completed')
        <span class="label label-success">Hand Over Checkin</span>
        @endif
    </td>
</tr>