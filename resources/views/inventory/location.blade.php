@extends('layouts.app', ['active' => 'inventoryarea'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">LOCATOR</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('area') }}"><i class="icon-stack2 position-left"></i> Area</a></li>
            <li class="active"><a href="#"> Locator</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection
 
@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="form-group" style="float:right">
            <button type="button" class="btn btn-xs btn-primary add_location">ADD LOCATION</button>
            <a href="{{route('area.printFG',['areaid'=>$_GET['areaid'] ])}}" target="_blank" class="btn btn-xs btn-success" id="print_fg">PRINT ALL RACK</a>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table_location">
                <thead>
                    <tr>
                        <th style="width:10px;">#</th>
                        <th>BARCODE</th>
                        <th>NAME</th>
                        <th>RACK</th>
                        <th style="width:10px;">ACTION</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- IMPORTANT LINK -->
<a href="{{ route('area.ajaxGetLocation',['areaid' => $_GET['areaid']]) }}" id="location_get_data"></a>
<!-- /IMPORTANT LINK -->
@endsection

@section('modal')

<!-- MODAL LOCATION -->
<div id="modal_add_new_location_" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <form class="form-horizontal" action="{{ route('area.ajaxAddLocation') }}" id="form-add-location">
          <div class="modal-content">
            <div class="modal-body">
                <div class="panel-body">
                    <fieldset>
                        <legend class="text-semibold">
                            <i class="icon-file-text2 position-left"></i>
                            <span id="title"> ADD NEW LOCATION</span> <!-- title -->
                        </legend>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Area:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" placeholder="Choose Area" value="{{ $_GET['areaname'] }}" readonly>
                                <input type="hidden" name="areaid" value="{{ $_GET['areaid'] }}">
                                <input type="hidden" name="areaname" value="{{ $_GET['areaname'] }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Rack:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="rack" placeholder="Input rack here" required>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
    </form>
  </div>
</div>
<!-- /MODAL LOCATION -->
@endsection

@section('js_extension')
<script type="text/javascript" src="{{url('js/plugins/tables/datatables/datatables.min.js')}}"></script>
@endsection

@section('js')
<script type="text/javascript">
$( document ).ready(function() {
    //datatables
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var url = $('#location_get_data').attr('href');

    var table = $('#table_location').DataTable({
        ajax: url,
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false  },
            {data: 'barcode', name: 'barcode'},
            {data: 'code', name: 'code'},
            {data: 'rack', name: 'rack'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
    //end of datatables

    //add area/edit
    $('.add_location').on('click',function(){
        $('#modal_add_new_location_').modal('show')
    });
    //end of add area

    //delete area
    $('#table_location').on('click', '.deleteLocation', function() {
        var id = $(this).data('locationid');
        if(id == 'kosong') {
            return false;
        }
        var token = $(this).data("token");
        bootbox.confirm("Are you sure delete this row ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax(
                {
                    url: "location/delete/"+id,
                    type: 'get',
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": token,
                    },
                    success: function ()
                    {
        				myalert('success','Data has been deleted');
                        console.log("it Work");
                        table.ajax.reload();

                    }
                });

                console.log("It failed");
            }
        });
    });
     $('#table_location').on('click','.ignore-click', function() {
        return false;
    });
    //end of delete area


    //add new location
    $('#form-add-location').submit(function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-add-location').attr('action'),
            data: $('#form-add-location').serialize(),
            success: function(response) {
                myalert('success', 'GOOD');
                $('#form-add-location').trigger("reset");
                $('#modal_add_new_location_').trigger('toggle');
                table.ajax.reload();
            },
            error: function(response) {
                myalert('error', 'NOT GOOD');
            }
        })
    });
});

</script>
@endsection
