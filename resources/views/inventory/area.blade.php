@extends('layouts.app', ['active' => 'inventoryarea'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">AREA</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('area') }}"><i class="icon-stack2 position-left"></i> Area</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="form-group" style="float:right">
            <button type="button" class="btn btn-xs btn-primary add_area">ADD AREA</button>
            <a href="{{ route('area.printAllLocator') }}" target="_blank" class="btn btn-xs btn-success" id="print_all_locator">PRINT BARCODE LOCATOR</a>
        </div>
    </div>
    <div class="panel-body">
        <table class="table datatable-save-state" id="table_area">
            <thead>
                <tr>
                    <th style="width:10px;">#</th>
                    <th>NAME</th>
                    <th style="width:10px;">ACTION</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<!-- IMPORTANT LINK -->
<a href="{{ route('area.ajaxGetData') }}" id="area_get_data"></a>
<!-- /IMPORTANT LINK -->
@endsection

@section('modal')

<!-- MODAL ADD AREA -->
<div id="modal_add_new_area_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('area.ajaxAddArea') }}" id="form-add-area">
            <div class="modal-content">
              <div class="modal-body">
                  <div class="panel-body">
                      <fieldset>
                          <legend class="text-semibold">
                              <i class="icon-file-text2 position-left"></i>
                              <span id="title"> ADD NEW AREA</span> <!-- title -->
                          </legend>

                          <div class="form-group">
                              <label class="col-lg-3 control-label">Area Name:</label>
                              <div class="col-lg-9">
                                  <input type="text" class="form-control" name="areaname" placeholder="Input area name here" style="text-transform:uppercase">
                              </div>
                          </div>
                      </fieldset>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL ADD AREA -->
@endsection

@section('js_extension')
<script type="text/javascript" src="{{url('js/plugins/tables/datatables/datatables.min.js')}}"></script>
@endsection

@section('js')
<script type="text/javascript">
$( document ).ready(function() {
    //datatables
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var url = $('#area_get_data').attr('href');

    var table = $('#table_area').DataTable({
        ajax: url,
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false  },
            {data: 'name', name: 'name'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
    //end of datatables

    //add area/edit
    $('.add_area').on('click',function(){
        $('#modal_add_new_area_').modal('show')
    });
    //end of add area

    //delete area
    $('#table_area').on('click', '.deleteArea', function() {
        var id = $(this).data('areaid');
        if(id == 'kosong') {
            return false;
        }
        var token = $(this).data("token");
        bootbox.confirm("Are you sure delete this row ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax(
                {
                    url: "area/delete-area/"+id,
                    type: 'get',
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": token,
                    },
                    success: function ()
                    {
                    	myalert('success','Data has been deleted');
                        console.log("it Work");
                        table.ajax.reload();

                    }
                });

                console.log("It failed");
            }
        });
    });
     $('#table_area').on('click','.ignore-click', function() {
        return false;
    });
    //end of delete area


    //add new location
    $('#form-add-area').submit(function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-add-area').attr('action'),
            data: $('#form-add-area').serialize(),
            success: function(response) {
                myalert('success','GOOD');
                $('#form-add-area').trigger("reset");
                $('#modal_add_new_area_').trigger('toggle');
                table.ajax.reload();
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        })
    });
});

</script>
<script>
$(".save-data").on("click", function () {
        submitData();
    });
    function submitData()
    {
        var fd = $("#main-form").serializeArray();
        console.log(fd);
        if ($("#main-form").valid()) {
            $.ajax({
                url:"{{url('/area/update/{id}')}}",
                type: 'POST',
                url: apiurl,
                data: fd,
                beforeSend: function () {
                    $('.panel-body').block({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });
                },
                success: function (data) {
                        console.log(data);
                        if(data.response.Status == 0)
                        {
                            new PNotify({
                                title: 'success!',
                                text: 'Data berhasil disimpan ke server',
                                addclass: 'bg-success'
                            });
                            $('.panel-body').unblock();
                        }
                        else
                        {
                            if(data.response.Message)
                            {
                                $.each(data.response.Message, function(i, v) {
                                    var msg = '<label class="validation-error-label" for="'+i+'">'+v+'</label>';
                                    $("input[name="+i+"],select[name="+i+"]").after(msg);
                                });
                                var keys = Object.keys(data.response.Message);
                                $('input[name="'+keys[0]+'"]').focus();
                            }
                            new PNotify({
                                title: 'Failed!',
                                text:  'data Failed to store',
                                addclass: 'bg-danger'
                            });
                            $('.panel-body').unblock();
                        }
                },
                error: function (xhr) {
                    console.log(xhr);
                    new PNotify({
                                title: 'Failed!',
                                text:  xhr,
                                addclass: 'bg-danger'
                            });
                    $('.panel-body').unblock();
                },
                dataType: 'json'
            });
        } else {
            new PNotify({
                title: 'Validasi Error!',
                text: 'Isian data masih ada yang kurang, silahkan perbaiki.',
                addclass: 'bg-danger'
            });
        }
    };
</script>
@endsection
