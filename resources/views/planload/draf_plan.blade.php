@extends('layouts.app', ['active' => 'drafplanload'])

@section('page_header')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="position-left"></i> <span class="text-semibold">DRAF PLAN LOAD</span></h4>
                <p class="position-left"></p>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component">
            <ul class="breadcrumb">
                <li class="active"><a href="#" id="form_invoice"><i class="icon-location4 position-left"></i> Draf Plan
                        Load</a></li>
            </ul>
        </div>
    </div>
    <!-- /page header -->
@endsection

@section('content')
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="col-md-10">
                <form action="{{ route('planload.getDataDrafPlan') }}" id="form_filter">
                    @csrf
                    <div class="form-group ">
                        <label><b>Choose Date</b></label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                            <input type="text" class="form-control daterange-basic" name="date" id="date">
                            <div class="input-group-btn">
                                <button type="submit" id="btnFilter" class="btn btn-primary">Filter</button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="radio-inline"><input type="radio" name="radio_status" value="lc"
                                checked="">Filter by LC Date</label>
                        <label class="radio-inline"><input type="radio" name="radio_status" value="podd">Filter by PODD
                            Check</label>
                        <label class="radio-inline"><input type="radio" name="radio_status" value="psdd">Filter by
                            Stastical Date</label>
                    </div>
                </form>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <button style="margin-top: 25px;" class="btn btn-success" id="btn-mdup"><span
                            class="icon-file-upload2"></span> Upload</button>
                    <a href="{{ route('planload.templateUpload') }}" type="button" style="margin-top: 25px;"
                        class="btn btn-default" id="btn-dwtemplate"><span class="icon-file-download2"></span> Download</a>
                    <!-- <button style="margin-top: 25px;" class="btn btn-default" id="btn-dwtemplate"><span class="icon-file-download2"></span> Upload</button> -->
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-3">
                    <label><b>Brand</b></label>
                    <select class="form-control" id="fil_brand">
                        <option>--Filter Brand--</option>
                    </select>
                </div>
                <div class="col-lg-3">
                    <label><b>Document Type</b></label>
                    <select class="form-control" id="fil_doctype">
                        <option>--Filter Document Type--</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="table-responsive">
                    <table class="table datatable-save-state" id="table-list">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>PO</th>
                                <th width="100px">Plan Ref</th>
                                <th>Plan Stuffing</th>
                                <th>Job</th>
                                <th>Art. No.</th>
                                <th>Season</th>
                                <th>Style</th>
                                <th>Gender</th>
                                <th>New QTY</th>
                                <th>PL QTY</th>
                                <th>LC Date</th>
                                <th>Order Type</th>
                                <th class="fil-doc">Doc. Type</th>
                                <th>CRD</th>
                                <th>PSDD</th>
                                <th>PODD</th>
                                <th>PSFD</th>
                                <th>Cust. Order No.</th>
                                <th>Cust. No</th>
                                <th>Country</th>
                                <th>Stuff. Place</th>
                                <th class="fil-brand">Brand</th>
                                <th>CTN</th>
                                <th>CBM</th>
                                <th>GW</th>
                                <th>NW</th>
                                <th>Sports Ctg.</th>
                                <th>Product Type.</th>
                                <th>Age Group</th>
                                <th>Remark</th>
                                <th>Fact</th>
                                <th>Stuf. Place</th>
                                <th>Fact. Code</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <a href="{{ route('planload.exportDraf') }}" id="exportDraf"></a>
@endsection

@section('modal')
    <!-- modal upload -->
    <div id="modal_upload_" class="modal fade" role="dialog">
        <form class="form-horizontal" action="{{ route('planload.uploadDraf') }}" id="form-upload" method="POST"
            enctype="multipart/form-data">
            @csrf
            <div class="modal-dialog">
                <div class="modal-content modal-lg">
                    <div class="modal-body">
                        <div class="panel-body">
                            <fieldset>
                                <legend class="text-semibold">
                                    <h4 id="title_upload"><i class="icon-upload4 position-left"></i>UPLOAD DRAF PLAN
                                        LOAD<span id='title_po'></span></h4>

                                </legend>
                                <!-- upload excel -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">File Excel :</label>
                                    <div class="col-lg-9">
                                        <input type="file" class="file-styled" name="file_upload" id="file"
                                            required>
                                        <span class="help-block" id="excel_po"></span>
                                        <span class="help-block">Accepted formats: xls xlsx.</span>
                                    </div>
                                </div>

                                <div id="verify_data">

                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Upload</button>
                    </div>
                </div>
            </div>
        </form>
    </div>


    <!-- modal remark -->
    <div id="modal_remark" class="modal fade" role="dialog">
        <form class="form-horizontal" action="{{ route('planload.setRemark') }}" id="form-remark" method="POST"
            enctype="multipart/form-data">
            @csrf
            <div class="modal-dialog">
                <div class="modal-content modal-lg">
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="row form-group">
                                <label><b>Remark Order</b></label>
                                <textarea name="rm_order" id="rm_order" class="form-control"></textarea>
                                <input type="text" name="txpo" id="txpo" hidden="">
                                <input type="text" name="txplan" id="txplan" hidden="">
                            </div>
                            <div class="row form-group">
                                <label><b>Remark Exim</b></label>
                                <textarea name="rm_exim" id="rm_exim" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="close" onclick="chide();">Close</button>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@include('planload/_js_index')
@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            var url = $('#form_filter').attr('action');
            $.extend($.fn.dataTable.defaults, {
                stateSave: true,
                autoWidth: false,
                autoLength: false,
                processing: true,
                serverSide: false,
                dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: {
                        'first': 'First',
                        'last': 'Last',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    }
                }
            });

            var _token = $("input[name='_token']").val();
            var table = $('#table-list').DataTable({
                buttons: [{
                    text: 'Export to Excel',
                    className: 'btn btn-sm bg-success exportExcel',
                    action: function(e, dt, node, config) {
                        var columns = table.settings().init().columns;
                        var current_order = table.order();

                        var column_name = columns[current_order[0][0]].name;
                        var direction = current_order[0][1];
                        var filter = table.search();
                        window.location.href = $('#exportDraf').attr('href') +
                            '?date=' + $('#date').val() +
                            '&radio=' + $('input[name=radio_status]:checked').val() +
                            '&brand=' + $('#fil_brand').val() +
                            '&doctype=' + $('#fil_doctype').val();
                    }
                }],
                ajax: {
                    url: url,
                    type: 'get',
                    beforeSend: () => {


                    },
                    data: function(d) {
                        return $.extend({}, d, {
                            "radio": $('input[name=radio_status]:checked').val(),
                            "date": $('#date').val(),
                            "_token": _token
                        });
                    },
                    complete: () => {
                        // $("#btnFilter").removeAttr("disabled");
                    },
                    error: () => {
                        // $("#btnFilter").removeAttr("disabled");

                    }
                },
                fnCreatedRow: function(row, data, index) {
                    var info = table.page.info();
                    var value = index + 1 + info.start;
                    $("#btnFilter").removeAttr("disabled");
                    $('td', row).eq(0).html(value);
                    $('td', row).eq(2).css('min-width', '100px');
                },
                columnDefs: [{
                    className: 'dt-center'
                }],
                columns: [{
                        data: null,
                        sortable: false,
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'po_number',
                        name: 'po_number'
                    },
                    {
                        data: 'plan_ref_number',
                        name: 'plan_ref_number'
                    },
                    {
                        data: 'plan_stuffing',
                        name: 'plan_stuffing'
                    },
                    {
                        data: 'job',
                        name: 'job'
                    },
                    {
                        data: 'buyer_item',
                        name: 'buyer_item'
                    },
                    {
                        data: 'season',
                        name: 'season'
                    },
                    {
                        data: 'style',
                        name: 'style'
                    },
                    {
                        data: 'gender',
                        name: 'gender'
                    },
                    {
                        data: 'new_qty',
                        name: 'new_qty'
                    },
                    {
                        data: 'item_qty',
                        name: 'item_qty'
                    },
                    {
                        data: 'kst_lcdate',
                        name: 'kst_lcdate'
                    },
                    {
                        data: 'order_type',
                        name: 'order_type'
                    },
                    {
                        data: 'document_type',
                        name: 'document_type'
                    },
                    {
                        data: 'crd',
                        name: 'crd'
                    },
                    {
                        data: 'po_stat_date_adidas',
                        name: 'po_stat_date_adidas'
                    },
                    {
                        data: 'podd_check',
                        name: 'podd_check'
                    },
                    {
                        data: 'psfd',
                        name: 'psfd'
                    },
                    {
                        data: 'customer_order_number',
                        name: 'customer_order_number'
                    },
                    {
                        data: 'customer_number',
                        name: 'customer_number'
                    },
                    {
                        data: 'country_name',
                        name: 'country_name'
                    },
                    {
                        data: 'stuffing_place_update',
                        name: 'stuffing_place_update'
                    },
                    {
                        data: 'trademark',
                        name: 'trademark'
                    },
                    {
                        data: 'ctn',
                        name: 'ctn'
                    },
                    {
                        data: 'cbm',
                        name: 'cbm'
                    },
                    {
                        data: 'gross',
                        name: 'gross'
                    },
                    {
                        data: 'net',
                        name: 'net'
                    },
                    {
                        data: 'sports_ctg',
                        name: 'sports_ctg'
                    },
                    {
                        data: 'product_type',
                        name: 'product_type'
                    },
                    {
                        data: 'age_group',
                        name: 'age_group'
                    },
                    {
                        data: 'remarks',
                        name: 'remarks'
                    },
                    {
                        data: 'factory_id',
                        name: 'factory_id'
                    },
                    {
                        data: 'stuffing_place_update',
                        name: 'stuffing_place_update'
                    },
                    {
                        data: 'factory_code',
                        name: 'factory_code'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    }
                ],
            });
            //end of datatables

            // table.columns().every( function () {
            //     var that = this;
            //     that.search('').draw();
            // });

            table.on('preDraw', function() {
                    Pace.start();
                })
                .on('draw.dt', function() {
                    $('#table-list').unblock();
                    Pace.stop();
                });

            $('#form_filter').submit(function(event) {
                event.preventDefault();
                $('#btnFilter').prop('disabled', true)
                // dateRange = $('#date').val();
                // const [startDateStr, endDateStr] = dateRange.split(' - ');
                // const startDate = new Date(startDateStr);
                // const endDate = new Date(endDateStr);
                // const timeDifference = endDate - startDate;

                // const daysDifference = timeDifference / (1000 * 3600 * 24);
                // if (daysDifference > 93) {
                //     myalert('error', 'The date is too long');
                //     $("#btnFilter").removeAttr("disabled");
                //     return false;
                // }

                loading_process();
                table.clear();
                table.ajax.reload();
                ajaxFilBrand($('#date').val(), $('input[name=radio_status]:checked').val());
            });

            $('input[name=radio_status]').on('change', function(event) {
                event.preventDefault();

                loading_process();
                table.clear();
                table.draw();
                ajaxFilBrand($('#date').val(), $('input[name=radio_status]:checked').val());
                ajaxFilDoc($('#date').val(), $('input[name=radio_status]:checked').val(), $('#fil_brand')
                    .val());
            });




            $('#btn-mdup').on('click', function() {
                $('#modal_upload_').modal('show');
            });

            $('#form-upload').submit(function(event) {
                event.preventDefault();


                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',
                    url: $('#form-upload').attr('action'),
                    contentType: false,
                    processData: false,
                    data: new FormData(this),
                    beforeSend: function() {
                        $('#form-upload .modal-body').block({
                            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Uploading File</span>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#fff',
                                width: 'auto',
                                '-webkit-border-radius': 2,
                                '-moz-border-radius': 2,
                                backgroundColor: '#333'
                            }
                        });
                    },
                    success: function(response) {
                        var data_response = response.data;

                        if (data_response.status == 200) {
                            var field = data_response.output.toString();
                            myalert('success', field + " field to upload !!!");
                        } else {
                            myalert('error', response);
                        }
                        $('#form-upload .modal-body').unblock();
                        $('#modal_upload_').modal('hide');

                    },
                    error: function(response) {
                        $('#form-upload .modal-body').unblock();
                        myalert('error', response);
                        console.log(response);
                    }
                });

            });

            $('#form-remark').submit(function(event) {
                event.preventDefault();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: $('#form-remark').attr('action'),
                    type: "POST",
                    data: {
                        "po_number": $('#txpo').val(),
                        "plan_ref": $('#txplan').val(),
                        "remark_exim": $('#rm_exim').val(),
                        "remark_order": $('#rm_order').val(),
                        "_token": _token,
                    },
                    success: function(response) {
                        myalert('success', response);
                        chide();
                        $('#form_filter').submit();
                    },
                    error: function(response) {
                        if (response['status'] == 422) {
                            myalert('error', response);
                        }
                    }
                });
            });

            // $('#btn-dwtemplate').on('click',function(){
            //     $.ajaxSetup({
            //         headers: {
            //             'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            //         }
            //     });
            //     $.ajax({
            //         url:"{{ route('planload.templateUpload') }}",
            //         type: "GET",
            //         success: function(response){
            //             console.log(response);
            //         },
            //         error: function(response) {
            //             if(response['status'] == 422) {
            //                 myalert('error',response);
            //             }
            //         }
            //     });
            // });


            $('#fil_brand').on('change', function() {
                var fbrand = $(this).val();
                var len = $('#table-list tbody tr').length;

                if (len > 0) {

                    loading_process();

                    ajaxFilDoc($('#date').val(), $('input[name=radio_status]:checked').val(), $(
                        '#fil_brand').val());
                    table
                        .columns('.fil-brand')
                        .search(fbrand)
                        .draw();

                }
            });

            $('#fil_doctype').on('change', function() {
                var fdoc = $(this).val();
                var len = $('#table-list tbody tr').length;

                if (len > 0) {

                    loading_process();


                    table
                        .columns('.fil-doc')
                        .search(fdoc)
                        .draw();

                }
            });


        });

        function remark(e) {
            var po = e.getAttribute('data-po');
            var plan = e.getAttribute('data-plan');
            $('#txpo').val(po);
            $('#txplan').val(plan);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'get',
                url: "{{ route('planload.getRemark') }}",
                data: {
                    po: po,
                    plan: plan
                },
                success: function(response) {

                    $('#rm_order').val(response.order);
                    $('#rm_exim').val(response.exim);


                }
            });
            $('#modal_remark').modal('show');
        }

        function chide() {
            $('#rm_order').val('');
            $('#rm_exim').val('');
            $('#modal_remark').modal('hide');
        }

        function deleteDraf(e) {
            var po = e.getAttribute('data-po');
            var plan = e.getAttribute('data-plan');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'get',
                url: "{{ route('planload.deletePoDraf') }}",
                data: {
                    po: po,
                    plan: plan
                },
                beforeSend: function() {
                    loading_process();
                },
                success: function(response) {
                    myalert('success', response);

                    $('#form_filter').submit();
                },
                error: function(response) {
                    if (response['status'] == 422) {
                        myalert('error', response);
                    }
                }
            });

        }

        function ajaxFilBrand(date, radio) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'get',
                url: "{{ route('planload.ajaxFilBrand') }}",
                data: {
                    date: date,
                    radio: radio
                },
                success: function(response) {
                    // console.log(response);
                    var brand = response.brand;
                    // console.log(brand);
                    
                    $('#fil_brand').empty();
                    $('#fil_brand').append('<option value="">--Filter Brand--</option>');
                    $(Object.keys(brand)).each(function(index, i){
                        $('#fil_brand').append('<option value="' + brand[i]['trademark'] + '">' + brand[i][
                            'trademark'
                        ] + '</option>');
                    })
                    // for (var i = 0; i < brand.length; i++) {

                    //     $('#fil_brand').append('<option value="' + brand[i]['trademark'] + '">' + brand[i][
                    //         'trademark'
                    //     ] + '</option>');
                    // }
                },
                error: function(response) {
                    console.log(response);
                }
            });
        }

        function ajaxFilDoc(date, radio, brand) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'get',
                url: "{{ route('planload.ajaxDocType') }}",
                data: {
                    date: date,
                    radio: radio,
                    brand: brand
                },
                success: function(response) {

                    var doctype = response.doctype;

                    $('#fil_doctype').empty();
                    $('#fil_doctype').append('<option value="">--Filter Document Type--</option>');
                    $(Object.keys(doctype)).each(function(index, i){
                        // $('#fil_brand').append('<option value="' + brand[i]['trademark'] + '">' + brand[i][
                        //     'trademark'
                        // ] + '</option>');
                        $('#fil_doctype').append('<option value="' + doctype[i]['document_type'] + '">' +
                            doctype[i]['document_type'] + '</option>');
                    })
                    // for (var i = 0; i < doctype.length; i++) {

                    //     $('#fil_doctype').append('<option value="' + doctype[i]['document_type'] + '">' +
                    //         doctype[i]['document_type'] + '</option>');
                    // }
                },
                error: function(response) {
                    console.log(response);
                }
            });
        }
    </script>
@endsection
