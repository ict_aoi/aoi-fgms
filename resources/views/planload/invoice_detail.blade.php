@extends('layouts.app', ['active' => 'invdetail'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">INVOICE DETAIL</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#" id="form_invoice"><i class="icon-location4 position-left"></i> Invoice Detail</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('planload.getDetailInvoice') }}" id="form_filter">
            @csrf
            <div class="form-group" id="filter_by_invoice">
                <label><b>Invoice Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="invoice_no" id="invoice_no">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden"  id="filter_by_orderno">
                <label><b>PO Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="po_number" id="po_number" >
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_stuffing">
                <label><b>Plan Stuffing</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="planstuff" id="planstuff">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="invoice">Filter by Invoice Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="po">Filter by PO Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="plan">Filter by Plan Stuffing</label>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row {{ Auth::user()->admin_role !== 1 ? 'hidden' : '' }}">
            <div class="col-md-2"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->factory_id }}" {{ $val->factory_id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Invoice</th>
                        <th>PO Number</th>
                        <th>Plan Stuffing</th>
                        <th>Stuffing Place</th>
                        <th>SO</th>
                        <th>FWD</th>
                        <th>Ship Mode</th>
                        <th>Cust. No.</th>
                        <th>Destination</th>
                        <th>QTY</th>
                        <th>CTN</th>
                        <th>CBM</th>
                        <th>GW</th>
                        <th>NW</th>
                        <th>Remark</th>                                           
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('planload.exportDetailInvoice') }}" id="exportDetailInvoice"></a>
@endsection

@section('modal')
<div id="modal_remark" class="modal fade" role="dialog">
    <form class="form-horizontal" action="{{ route('planload.setRemarkDetInv') }}" id="form-remark" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content modal-lg">
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="row form-group">
                            <div class="col-md-4">
                                <label><b>Invoice :</b></label>
                                <label id="lb_inv"></label>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4">
                                <label><b>Remark Order</b></label>
                                <div class="rem_or" id="rem_or"></div>
                            </div>
                            <div class="col-md-4">
                                <label><b>Remark Exim</b></label>
                                <div class="rem_ex" id="rem_ex"></div>
                            </div>
                        </div>
                        <!-- <div class="row form-group">
                            <label><b>Remark Exim</b></label>
                            <textarea name="rm_exim" id="rm_exim" class="form-control"></textarea>
                        </div> -->
                        <div class="row form-group">
                            <label><b>Remark Container</b></label>
                            <select class="form-control" id="rm_cont">
                                <option value="">-Remark Container-</option>
                                <option value="Container 40FT">Container 40FT</option>
                                <option value="Container 60FT">Container 60FT</option>
                                <option value="Container 40HC">Container 40HC</option>
                                <option value="Container 60HC">Container 60HC</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="close" onclick="chide();" >Close</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection

@include('planload/_js_index')

@section('js')
<script type="text/javascript">
$(document).ready(function(){
    var url = $('#form_filter').attr('action');
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#exportDetailInvoice').attr('href')
                                            + '?po_number=' + $('#po_number').val()
                                            + '&invoice=' + $('#invoice_no').val()
                                            + '&exfact=' + $('#planstuff').val()
                                            + '&factory_id=' + $('#factory_id').val()
                                            + '&radio=' + $('input[name=radio_status]:checked').val();
                }
            }
       ],
        ajax: {
            url: url,
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "radio": $('input[name=radio_status]:checked').val(),
                    "po_number": $('#po_number').val(),
                    "invoice": $('#invoice_no').val(),
                    "exfact": $('#planstuff').val(),
                    "factory_id": $('#factory_id').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'invoice', name: 'invoice'},
            {data: 'po_number', name: 'po_number'},
            {data: 'plan_stuffing', name: 'plan_stuffing'},
            {data: 'stuffing_place_update', name: 'stuffing_place_update'},
            {data: 'so', name: 'so'},
            {data: 'fwd', name: 'fwd'},
            {data: 'shipmode', name: 'shipmode'},
            {data: 'customer_number', name: 'customer_number'},
            {data: 'destination', name: 'destination'},
            {data: 'item_qty', name: 'item_qty'},
            {data: 'pkg_count', name: 'pkg_count'},
            {data: 'cbm', name: 'cbm'},
            {data: 'gross', name: 'gross'},
            {data: 'net', name: 'net'},
            {data: 'remark', name: 'remark'},
            {data: 'action', name: 'action'}
        ],
    });
    //end of datatables

    $('input[type=radio][name=radio_status]').change(function() {
      
        if (this.value == 'invoice') {
            if($('#filter_by_invoice').hasClass('hidden')) {
                $('#filter_by_invoice').removeClass('hidden');
            }

            $('#filter_by_orderno').addClass('hidden');
            $('#filter_by_stuffing').addClass('hidden');
            $('#filter_by_podd').addClass('hidden');

        }else if (this.value == 'po') {
            if($('#filter_by_orderno').hasClass('hidden')) {
                $('#filter_by_orderno').removeClass('hidden');
            }

            $('#filter_by_invoice').addClass('hidden');
            $('#filter_by_stuffing').addClass('hidden');
            $('#filter_by_podd').addClass('hidden');

        }else if (this.value == 'plan') {
            if($('#filter_by_stuffing').hasClass('hidden')) {
                $('#filter_by_stuffing').removeClass('hidden');
            }

            $('#filter_by_invoice').addClass('hidden');
            $('#filter_by_orderno').addClass('hidden');
            $('#filter_by_podd').addClass('hidden');

        }
        
    });

    table.on('preDraw', function() {
        Pace.start();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        loading_process();
        table.clear();
        table.draw();
    });

    $('#factory_id').on('change',function(){
        loading_process();
        table.clear();
        table.draw();
    });


    $('#table-list').on('change','.exfact_unfilled',function(){
        var invoice = $(this).data('invoice');
        var exfact = $(this).val();
        setTimeout(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: "{{ route('planload.updateExfact') }}",
                data: {invoice:invoice,exfact:exfact},
                beforeSend:function(){
                    loading_process();
                },
                success: function(response){
                    $('#table-list').unblock();
                    console.log(response);
                },
                error: function(response) {
                    return false;
                }
            });
        }, 10000);
       
    });

    $('#form-remark').submit(function(event){
        event.preventDefault();
        var invoice = $('#txinvoice').val();
      
   
        var remark_cont = $('#rm_cont').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url:$('#form-remark').attr('action'),
            type: "POST",
            data:{
                "invoice": invoice,
                "remark_cont": remark_cont,                           
                "_token": _token,
            },
            success: function(response){
               myalert('success',response);
                chide();
               $('#form_filter').submit();
            },
            error: function(response) {
                if(response['status'] == 422) {
                    myalert('error',response);
                }
            }
        });
    });


});

function cancelInv(e){
    var invoice = e.getAttribute('data-invoice');
    bootbox.confirm("Are you sure cancel invoice "+invoice+" ?",function(result){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: "{{ route('planload.cancelInvoice')}}",
                data: {invoice:invoice},
                success: function(response){
                    myalert('success',"Cancel invoice success . . . !!!");
                    $('#form_filter').submit();
                },
                error: function(response) {
                    myalert('error',"Cancel invoice Field . . . !!!");
                    return false;
                }
            });
    });    
}

function remark(e){
    var invoice = e.getAttribute('data-invoice');
  

 
    $('#lb_inv').text(invoice);
    $('#txinvoice').val(invoice);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url: "{{ route('planload.getRemarkDtInv') }}",
        data: {invoice:invoice},
        success: function(response){
           // $('#rm_order').val(response.remark_order);
           // $('#rm_exim').val(response.remark_exim);
           $('#rm_cont').val(response.remark_cont);

           var rempo = response.remarkpo;
           var order = "";
           var remex = "";
           for (var i = 0; i < rempo.length; i++) {
               if (rempo[i]['remark_order']!=null) {
                    order = rempo[i]['po_number']+" ("+rempo[i]['remark_order']+") <br>"+order;
               }

               if (rempo[i]['remark_exim']!=null) {
                    remex = rempo[i]['po_number']+" ("+rempo[i]['remark_exim']+") <br>"+remex;
               }
           }


           document.getElementById("rem_or").innerHTML=order;
           document.getElementById("rem_ex").innerHTML=remex;

        },
        error: function(response) {
            console.log(response)
            return false;
        }
    });
    $('#modal_remark').modal('show');
}

function chide(){
    $('#rm_order').val('');
    $('#rm_exim').val('');
    $('#rm_cont').val('');
    $('#lb_po').text('');
    $('#lb_plan').text('');
    $('#lb_inv').text('');
    $('#modal_remark').modal('hide');
}
</script>
@endsection