@extends('layouts.app', ['active' => 'editdraf'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">EDIT PO DRAF</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#" id="form_invoice"><i class="icon-location4 position-left"></i> Edit PO Draf</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('planload.getDataPoDraf') }}" id="form_filter">
            @csrf
            <div class="form-group"  id="filter_by_orderno">
                <label><b>PO Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="po_number" id="po_number" >
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>           
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>PO Number</th>
                        <th width="156px">Plan Ref. No.</th>
                        <th width="160px">MDD</th>
                        <th width="79px">Subsidary</th>
                        <th width="175px">Ship Mode</th>
                        <th>FWD</th>
                        <th>PSFD</th>
                        <th>WHS</th>
                        <th>Priority</th>
                        <th>Remark</th>                                          
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('modal')
<!-- modal remark -->
<div id="modal_remark" class="modal fade" role="dialog">
    <form class="form-horizontal" action="{{ route('planload.setRemark') }}" id="form-remark" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content modal-lg">
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="row form-group">
                            <label><b>Remark Order</b></label>
                            <textarea name="rm_order" id="rm_order" class="form-control"></textarea>
                            <input type="text" name="txpo" id="txpo" hidden="">
                            <input type="text" name="txplan" id="txplan" hidden="">
                        </div>
                        <div class="row form-group">
                            <label><b>Remark Exim</b></label>
                            <textarea name="rm_exim" id="rm_exim" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="close" onclick="chide();">Close</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@include('planload/_js_index_2')

@section('js')
<script type="text/javascript">
$(document).ready(function(){
    var url = $('#form_filter').attr('action');
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        searching:false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            url: url,
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "po_number": $('#po_number').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(2).css('min-width','156px');
            $('td', row).eq(3).css('min-width','160px');
            $('td', row).eq(4).css('min-width','79px');
            $('td', row).eq(5).css('min-width','175px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'po_number', name: 'po_number'},
            {data: 'plan_ref_number', name: 'plan_ref_number'},
            {data: 'mdd', name: 'mdd'},
            {data: 'subsidary', name: 'subsidary'},
            {data: 'shipmode', name: 'shipmode'},
            {data: 'fwd', name: 'fwd'},
            {data: 'psfd', name: 'psfd'},
            {data: 'whs_code', name: 'whs_code'},
            {data: 'priority_code', name: 'priority_code'},
            {data: 'remark', name: 'remark'},
            {data: 'action', name: 'action'}
        ],
    });

    table.on('preDraw', function() {
        Pace.start();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        loading_process();
        table.clear();
        table.draw();
    });


    $('#form-remark').submit(function(event){
        event.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url:$('#form-remark').attr('action'),
            type: "POST",
            data:{
                "po_number": $('#txpo').val(),  
                "plan_ref": $('#txplan').val(),
                "remark_exim": $('#rm_exim').val(), 
                "remark_order": $('#rm_order').val(),                           
                "_token": _token,
            },
            success: function(response){
               myalert('success',response);
                chide();
               $('#form_filter').submit();
            },
            error: function(response) {
                if(response['status'] == 422) {
                    myalert('error',response);
                }
            }
        });
    });

    $('#table-list').on('blur','.planref_unfilled',function(){
        var id = $(this).data('id');
        var po = $(this).data('po');
        var plan_ref = $(this).data('plan');
        var newplan = $(this).val();

       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{route('planload.updatePlanRef')}}",
            data: {id:id, newplan:newplan, po_number:po, plan_ref_number:plan_ref},
            beforeSend:function(){
                loading_process();
            },
            success: function(response){
                $('#table-list').unblock();
                $('#planref_' + po).html(response);
                //$('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });
    });

    $('#table-list').on('blur','.mdd_unfilled',function(){
        var id = $(this).data('id');
        var po = $(this).data('po');
        var plan_ref = $(this).data('plan');
        var mdd = $(this).val();

       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{route('planload.updateMdd')}}",
            data: {id:id, mdd:mdd, po_number:po, plan_ref_number:plan_ref},
            beforeSend:function(){
                loading_process();
            },
            success: function(response){
                $('#table-list').unblock();
                $('#mdd_' + po).html(response);
                //$('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });
    });

    $('#table-list').on('blur','.subsidary_unfilled',function(){
        var id = $(this).data('id');
        var po = $(this).data('po');
        var plan_ref = $(this).data('plan');
        var subsidary = $(this).val();

       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{route('planload.updateSubsidary')}}",
            data: {id:id, subsidary:subsidary, po_number:po, plan_ref_number:plan_ref},
            beforeSend:function(){
                loading_process();
            },
            success: function(response){
                $('#table-list').unblock();
                $('#subsidary_' + po).html(response);
                //$('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });
    });

    $('#table-list').on('blur','.sm_unfilled',function(){
        var id = $(this).data('id');
        var po = $(this).data('po');
        var plan_ref = $(this).data('plan');
        var shipmode = $(this).val();

       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{route('planload.updateShipmode')}}",
            data: {id:id, shipmode:shipmode, po_number:po, plan_ref_number:plan_ref},
            beforeSend:function(){
                loading_process();
            },
            success: function(response){
                $('#table-list').unblock();
                $('#sm_' + po).html(response);
                //$('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });
    });

    $('#table-list').on('blur','.fwd_unfilled',function(){
        var id = $(this).data('id');
        var po = $(this).data('po');
        var plan_ref = $(this).data('plan');
        var fwd = $(this).val();

       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{route('planload.updateFwd')}}",
            data: {id:id, fwd:fwd, po_number:po, plan_ref_number:plan_ref},
            beforeSend:function(){
                loading_process();
            },
            success: function(response){
                $('#table-list').unblock();
                $('#fwd_' + po).html(response);
                //$('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });
    });

    $('#table-list').on('blur','.psfd_unfilled',function(){
        var id = $(this).data('id');
        var po = $(this).data('po');
        var plan_ref = $(this).data('plan');
        var psfd = $(this).val();

       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{route('planload.updatePsfd')}}",
            data: {id:id, psfd:psfd, po_number:po, plan_ref_number:plan_ref},
            beforeSend:function(){
                loading_process();
            },
            success: function(response){
                $('#table-list').unblock();
                $('#psfd_' + po).html(response);
                //$('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });
    });

    $('#table-list').on('blur','.whs_unfilled',function(){
        var id = $(this).data('id');
        var po = $(this).data('po');
        var plan_ref = $(this).data('plan');
        var whs = $(this).val();

       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{route('planload.updateWhs')}}",
            data: {id:id, whs:whs, po_number:po, plan_ref_number:plan_ref},
            beforeSend:function(){
                loading_process();
            },
            success: function(response){
                $('#table-list').unblock();
                $('#whs_' + po).html(response);
                //$('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });
    });

    $('#table-list').on('blur','.prio_unfilled',function(){
        var id = $(this).data('id');
        var po = $(this).data('po');
        var plan_ref = $(this).data('plan');
        var priority_code = $(this).val();

       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{route('planload.updatePriority')}}",
            data: {id:id, priority_code:priority_code, po_number:po, plan_ref_number:plan_ref},
            beforeSend:function(){
                loading_process();
            },
            success: function(response){
                $('#table-list').unblock();
                $('#prio_' + po).html(response);
                //$('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });
    });

});


function remark(e){
    var po = e.getAttribute('data-po');
    var plan = e.getAttribute('data-plan');
    $('#txpo').val(po);
    $('#txplan').val(plan);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }); 
    $.ajax({
        type: 'get',
        url: "{{route('planload.getRemark')}}",
        data: {po:po, plan:plan},
        success: function(response){
            
           $('#rm_order').val(response.order);
            $('#rm_exim').val(response.exim);
            

        }
    });
    $('#modal_remark').modal('show');
}

function chide(){
    $('#rm_order').val('');
    $('#rm_exim').val('');
    $('#modal_remark').modal('hide');
}

function deleteDraf(e){
    var po = e.getAttribute('data-po');
    var plan = e.getAttribute('data-plan');
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }); 
    $.ajax({
        type: 'get',
        url: "{{route('planload.deletePoDraf')}}",
        data: {po:po, plan:plan},
        beforeSend: function() {
            loading_process();
        },
        success: function(response){
           myalert('success',response);
           
           $('#form_filter').submit();
        },
        error: function(response) {
            if(response['status'] == 422) {
                myalert('error',response);
            }
        }
    });
  
}
</script>
@endsection