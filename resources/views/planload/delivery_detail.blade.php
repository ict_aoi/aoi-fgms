@extends('layouts.app', ['active' => 'detaildelivery'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">DELIVERY ORDER DETAIL</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#" id="form_invoice"><i class="icon-location4 position-left"></i> Delivery Order Detail</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('planload.getDetailDelivery') }}" id="form_filter">
            @csrf
            <div class="form-group" id="filter_by_delivery">
                <label><b>Delivery Number</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="delivery_no" id="delivery_no">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_stuffing">
                <label><b>Plan Stuffing</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="planstuff" id="planstuff">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="do">Filter by Delivery Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="plan">Filter by Plan Stuffing</label>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row ">
            <div class="col-md-2 {{ Auth::user()->admin_role !== 1 ? 'hidden' : '' }}"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->factory_id }}" {{ $val->factory_id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-7"></div>
            <div class="col-lg-3">
                <button class="btn btn-primary" id="exportTrucking" style="margin-top: 19px;">Export Trucking</button>
                <button class="btn btn-success" id="exportExcel" style="margin-top: 19px;">Export to Excel</button>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="table-responsive">
                <table class="table datatable-save-state" id="table-list">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>DO Number</th>
                            <th>Invoice</th>
                            <th>Ex Fact</th>
                            <th>Whs</th>
                            <th>Fwd</th>
                            <th>Shipmode</th>
                            <th>Setting</th>
                            <th>Police No.</th>
                            <th>Driver</th>
                            <th>Expedisi</th>
                            <th>CBM</th>
                            <th>CTN</th>
                            <th>QTY</th>                                       
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<a href="{{ route('planload.exportDetailDelivery') }}" id="exportDeliveryDetail"></a>
<a href="{{ route('planload.exportTruckDetail') }}" id="exportTruckingDetail"></a>
@endsection

@section('modal')
<!-- modal setting truck -->
<div id="modal_setDO" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-lg">
          
                <div class="modal-body">
                    <div class="panel-body">
                      <div class="form-group">
                          <div class="row">
                              <div class="col-lg-4">
                                  <label><b>Delivery Number</b></label>
                                  <input type="text" name="mddo" id="mddo" class="form-control">
                                  <input type="text" name="mdid" id="mdid" hidden="">
                                  <input type="text" name="mdinv" id="mdinv" hidden="">
                              </div>
                              <div class="col-lg-4">
                                  <label><b>No. Police</b></label>
                                  <input type="text" name="mdnopol" id="mdnopol" class="form-control">
                              </div>
                              <div class="col-lg-4">
                                  <label><b>Driver</b></label>
                                  <input type="text" name="mddriver" id="mddriver" class="form-control">
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-lg-4">
                                  <label><b>No. Telp</b></label>
                                  <input type="text" name="mdnotelp" id="mdnotelp" class="form-control">
                              </div>
                              <div class="col-lg-4">
                                  <label><b>Expedisi</b></label>
                                  <select id="mdexpedisi" class="form-control">
                                      <option value="">--Choosse Expedisi--</option>
                                      <option value="BESTINDO">BESTINDO</option>
                                      <option value="WINARA JAYA">WINARA JAYA</option>
                                      <option value="CITRA LINTAS">CITRA LINTAS</option>
                                      <option value="BKS">BKS</option>
                                      <option value="POS INDONESIA">POS INDONESIA</option>
                                      <option value="FEDEX">FEDEX</option>
                                      <option value="RPX">RPX</option>
                                      <option value="DHL">DHL</option>
                                      <option value="GREEN AIR PACIFIC">GREEN AIR PACIFIC</option>
                                      <option value="MITRA BARU TRANS">MITRA BARU TRANS</option>
                                      <option value="ALIRA JAYA TRANSPORTASI">ALIRA JAYA TRANSPORTASI</option>
                                      <option value="INDOTIRTA">INDOTIRTA</option>
                                      <option value="AOI">AOI</option>
                                      <option value="LISA GLOBAL TRANS">LISA GLOBAL TRANS</option>
                                      <option value="INTERNASIONAL FORTUNA EKSPRESINDO">INTERNASIONAL FORTUNA EKSPRESINDO</option>
                                      <option value="PT. KUEHNE NAGEL INDONESIA">PT. KUEHNE NAGEL INDONESIA</option>
                                      <option value="PT. QUALIS INDONESIA">PT. QUALIS INDONESIA</option>
                                  </select>
                              </div>
                              <div class="col-lg-4">
                                  <label><b>Type</b></label>
                                  <select id="mdtype" class="form-control">
                                      <option value="">--Choosse Type--</option>
                                      <option value="GRANDMAX">GRANDMAX</option>
                                      <option value="LCL">LCL</option>
                                      <option value="PICK UP">PICK UP</option>
                                      <option value="CDD">CDD</option>
                                      <option value="FUSO">FUSO</option>
                                      <option value="TRONTON">TRONTON</option>
                                      <option value="BUILD UP">BUILD UP</option>
                                      <option value="BUILD UP JUMBO">BUILD UP JUMBO</option>
                                      <option value="WING BOX">WING BOX</option>
                                      <option value="CONTAINER 40FT">CONTAINER 40FT</option>
                                      <option value="CONTAINER 20FT">CONTAINER 20FT</option>
                                      <option value="CONTAINER 40HC">CONTAINER 40HC</option>
                                      <option value="CONTAINER 45HC">CONTAINER 45HC</option>
                                      <!-- <option value="CONTAINER 20HC">CONTAINER 20HC</option> -->
                                  </select>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-lg-4">
                                  <label><b>No. Container</b></label>
                                  <input type="text" name="mdnocont" id="mdnocont" class="form-control">
                                  <label style="font-size: 10px; color: grey;">*optional untuk container</label>
                              </div>
                              <div class="col-lg-5"></div>
                              <div class="col-lg-3">
                                <button type="button" class="btn btn-primary" id="updateDO" style="margin-top: 25px;">Update</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-top: 25px;" >Close</button>
                             </div>
                          </div>
                      </div> 
                    </div>
                    <hr>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table datatable-save-state" id="table-truck">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>No. Police</th>
                                        <th>Driver</th>
                                        <th>Expedisi</th>
                                        <th>Type</th>
                                        <th>No. Telp</th>                                       
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

<!-- modal setting after stuffing -->
<div id="modal_setting_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-lg">
          
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="row form-group">
                            <div class="row">
                                <div class="col-lg-1">
                                    <label><b>No. DO</b></label>
                                </div>
                                <div class="col-lg-3">
                                    <label id="txdo"><b></b></label>
                                </div>

                                <div class="col-lg-1">
                                    <label><b>Forwarder</b></label>
                                </div>
                                <div class="col-lg-3">
                                    <label id="txfwd"><b></b></label>
                                </div>

                                <div class="col-lg-1">
                                    <label><b>Driver</b></label>
                                </div>
                                <div class="col-lg-3">
                                    <label id="txdriver"><b></b></label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-1">
                                    <label><b>Stuffing</b></label>
                                </div>
                                <div class="col-lg-3">
                                    <label id="txexfact"><b></b></label>
                                </div>

                                <div class="col-lg-1">
                                    <label><b>Shipmode</b></label>
                                </div>
                                <div class="col-lg-3">
                                    <label id="txship"><b></b></label>
                                </div>

                                <div class="col-lg-1">
                                    <label><b>Expedisi</b></label>
                                </div>
                                <div class="col-lg-3">
                                    <label id="txexpedisi"><b></b></label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-1">
                                    <label><b>Warehouse</b></label>
                                </div>
                                <div class="col-lg-3">
                                    <label id="txwhs"><b></b></label>
                                </div>

                                <div class="col-lg-1">
                                    <label><b>No. Pol.</b></label>
                                </div>
                                <div class="col-lg-3">
                                    <label id="txnopol"><b></b></label>
                                </div>

                                <div class="col-lg-1">
                                    <label><b>Type</b></label>
                                </div>
                                <div class="col-lg-3">
                                    <label id="txtype"><b></b></label>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row form-group">
                            <div class="row">
                                <label><b>Seal Number</b></label>
                                <input type="text" name="tx_seal" class="form-control" id="tx_seal" placeholder="Seal Number" required="">
                                <input type="text" name="txid" id="txid" hidden="">
                            </div>
                            <br>
                            <div class="row">
                                <label><b>PIC Seal</b></label>
                                <input type="text" name="tx_picseal" class="form-control" id="tx_picseal" placeholder="PIC Seal" required="">
                            </div>
                            <br>
                            <div class="row">
                                <div class="row">
                                    <label><b>Stuffing Finish</b></label>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label>Date</label>
                                        <input type="date" name="dt_stuff" class="form-control " id="dt_stuff" required="">
                                    </div>
                                    <div class="col-lg-2">
                                        <label>Hour</label>
                                        <input type="Number" name="hr_stuff" class="form-control " id="hr_stuff" required="" min="0" max="23">
                                    </div>
                                    <div class="col-lg-2">
                                        <label>Minute</label>
                                        <input type="Number" name="mn_stuff" class="form-control " id="mn_stuff" required="" min="0" max="59">
                                    </div>
                                </div>
                                
                            </div>
                            <br>
                            <div class="row">
                                <div class="row">
                                    <label><b>Out Factory</b></label>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label>Date</label>
                                        <input type="date" name="dt_out" class="form-control " id="dt_out" required="">
                                    </div>
                                    <div class="col-lg-2">
                                        <label>Hour</label>
                                        <input type="Number" name="hr_out" class="form-control " id="hr_out" required="" min="0" max="23">
                                    </div>
                                    <div class="col-lg-2">
                                        <label>Minute</label>
                                        <input type="Number" name="mn_out" class="form-control " id="mn_out" required="" min="0" max="59">
                                    </div>
                                </div>
                                
                            </div>
                            <br>
                            <div class="row">
                                <div class="row">
                                    <label><b>In Warehouse</b></label>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label>Date</label>
                                        <input type="date" name="dt_in" class="form-control " id="dt_in" required="">
                                    </div>
                                    <div class="col-lg-2">
                                        <label>Hour</label>
                                        <input type="Number" name="hr_in" class="form-control " id="hr_in" required="" min="0" max="23">
                                    </div>
                                    <div class="col-lg-2">
                                        <label>Minute</label>
                                        <input type="Number" name="mn_in" class="form-control " id="mn_in" required="" min="0" max="59">
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <label><b>Remark</b></label>
                                <textarea class="form-control" id="tx_remark" name="tx_remark"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="save">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

        </div>
    </div>
</div>
@endsection
@include('planload/_js_index_2')
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $('input[type=radio][name=radio_status]').on('change',function(){
        if (this.value=='do') {
            if($('#filter_by_delivery').hasClass('hidden')) {
                $('#filter_by_delivery').removeClass('hidden');
            }

            $('#filter_by_stuffing').addClass('hidden');
        }else{
            if($('#filter_by_stuffing').hasClass('hidden')) {
                $('#filter_by_stuffing').removeClass('hidden');
            }

            $('#filter_by_delivery').addClass('hidden');
        }

        loading_process();
        table.clear();
        table.draw();
    });

    var url = $('#form_filter').attr('action');
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            url: url,
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "radio": $('input[name=radio_status]:checked').val(),
                    "do": $('#delivery_no').val(),
                    "exfact": $('#planstuff').val(),
                    "factory_id":$('#factory_id').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(1).css('max-width', '150px');
            $('td', row).eq(2).css('max-width', '110px');
            $('td', row).eq(3).css('min-width', '110px');
            $('td', row).eq(4).css('min-width', '150px');
            $('td', row).eq(5).css('min-width', '50px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'do_number', name: 'do_number'},
            {data: 'invoice', name: 'invoice'},
            {data: 'plan_stuffing', name: 'plan_stuffing'},
            {data: 'whs_id', name: 'whs_id'},
            {data: 'fwd', name: 'fwd'},
            {data: 'shipmode', name: 'shipmode'},
            {data: 'setting', name: 'setting'},
            {data: 'nopol', name: 'nopol'},
            {data: 'driver', name: 'driver'},
            {data: 'expedisi', name: 'expedisi'},
            {data: 'cbm', name: 'cbm'},
            {data: 'ctn', name: 'ctn'},
            {data: 'qty', name: 'qty'},
            {data: 'action', name: 'action'}
        ],
    });

    table.on('preDraw', function() {
        Pace.start();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        loading_process();
        table.clear();
        table.draw();
    });

    $('#factory_id').on('change',function(event){
        event.preventDefault();

        loading_process();
        table.clear();
        table.draw();
    });


    $('#save').on('click',function(){
       

        var id = $('#txid').val();
        var seal = $('#tx_seal').val();
        var picseal = $('#tx_picseal').val();
        var dt_stuff = $('#dt_stuff').val();
        var hr_stuff = $('#hr_stuff').val();
        var mn_stuff = $('#mn_stuff').val();

        var dt_in = $('#dt_in').val();
        var hr_in = $('#hr_in').val();
        var mn_in = $('#mn_in').val();

        var dt_out = $('#dt_out').val();
        var hr_out = $('#hr_out').val();
        var mn_out = $('#mn_out').val();

        var remark = $('#tx_remark').val();
        
        
        if (seal==='' || picseal==='' || dt_stuff==='' || hr_stuff==='' || mn_stuff==='' || dt_out==='' || hr_out==='' || mn_out==='') {
            myalert('error', 'Update field, Please check Seal Number/ Stuffing Finish/ Out Factory ! ! !');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : "{{ route('planload.updateDelivery') }}",
            data :{id:id,
                seal:seal,
                picseal:picseal,
                dt_stuff:dt_stuff,
                hr_stuff:hr_stuff,
                mn_stuff:mn_stuff,
                dt_in:dt_in,
                hr_in:hr_in,
                mn_in:mn_in,
                dt_out:dt_out,
                hr_out:hr_out,
                mn_out:mn_out,
                remark:remark },
            success: function(response) {
                var response = response.data;

                if (response.status==200) {
                    myalert('success',response.output);

                    $('#modal_setting_ .modal-body').unblock();
                    $('#modal_setting_').modal('hide');
                }else{
                    myalert('error',response.output);
                    $('#modal_setting_ .modal-body').unblock();
                }

                $('#form_filter').submit();

                $('#modal_setting_ .modal-body').unblock();
            },
            error: function(response) {
                $('#modal_setting_ .modal-body').unblock();
                console.log(response);
            }
        });

    });

    //export to excel
    $('#exportExcel').on('click',function(){
        window.location.href = $('#exportDeliveryDetail').attr('href')
                                            + '?radio=' + $('input[name=radio_status]:checked').val()
                                            + '&factory_id=' + $('#factory_id').val()
                                            + '&exfact=' + $('#planstuff').val()
                                            + '&do=' + $('#delivery_no').val();
    });

    $('#exportTrucking').on('click',function(){
        window.location.href = $('#exportTruckingDetail').attr('href')
                                            + '?radio=' + $('input[name=radio_status]:checked').val()
                                            + '&factory='+$('#factory_id').val()
                                            + '&exfact=' + $('#planstuff').val()
                                            + '&do=' + $('#delivery_no').val();
    });


    $('#updateDO').on('click',function(event){
        event.preventDefault();

        if ($('#mddo').val().trim()=="" ||$('#mdnopol').val().trim()=="" ||$('#mddriver').val().trim()=="" ||$('#mdexpedisi').val().trim()=="" ||$('#mdtype').val().trim()=="" ||$('#mdnotelp').val().trim()=="") {
            myalert('error','Delivery Number / No. Police / Driver / Expedisi / Type / No. Telp required ! ! !');

            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : "{{ route('planload.updateDO') }}",
            data :{
                iddomd:$('#mdid').val(),
                invmd:$('#mdinv').val(),
                do_numbermd:$('#mddo').val(),
                mdexpedisi:$('#mdexpedisi').val(),
                mdnopol:$('#mdnopol').val(),
                mdtype:$('#mdtype').val(),
                mddriver:$('#mddriver').val(),
                mdnotlp:$('#mdnotelp').val(),
                mdnocont:$('#mdnocont').val()
            },
            success: function(response) {

                var response = response.data;
                if (response.status==200) {
                    $('#mdid').val('');
                    $('#mdinv').val('');
                    $('#mddo').val('');
                    $('#mdexpedisi').val('');
                    $('#mdnopol').val('');
                    $('#mdtype').val('');
                    $('#mddriver').val('');
                    $('#mdnotelp').val('');

                    $('#modal_setDO').unblock();
                    $('#form_filter').submit();
                    $('#modal_setDO').modal('hide');

                    myalert('success',response.output);
                }

                $('#form_filter').submit();
            },
            error: function(response) {
                var response = response.data;
                if (response.status==422) {
                    $('#modal_setDO').unblock();
                    myalert('error',response.output);
                }
                console.log(response);
            }
        });

    });

    
});

function setting(e){
    var id =e.getAttribute('data-id');
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{ route('planload.getSetting') }}",
        data :{id:id},
        success: function(response) {

            var data = response.data;
            
            $('#txdo').text(data.do_number);
            $('#txexfact').text(data.plans);
            $('#txwhs').text(data.whs);
            $('#txfwd').text(data.fwd);
            $('#txship').text(data.ship);
            $('#txnopol').text(data.nopol);
            $('#txdriver').text(data.driver);
            $('#txexpedisi').text(data.expedisi);
            $('#txtype').text(data.type);
            $('#tx_seal').val(data.seal_number);
            $('#tx_remark').val(data.remark);
         
            $('#dt_stuff').val(data.finish_dt);
            $('#hr_stuff').val(data.finish_hr);
            $('#mn_stuff').val(data.finish_mn);

            $('#dt_in').val(data.in_dt);
            $('#hr_in').val(data.in_hr);
            $('#mn_in').val(data.in_mn);

            $('#dt_out').val(data.out_dt);
            $('#hr_out').val(data.out_hr);
            $('#mn_out').val(data.out_mn);


            $('#txid').val(id);


            $('#modal_setting_').modal('show');
        },
        error: function(response) {
            console.log(response);
        }
    });

    

}

function cancelDo(e){
    var id =e.getAttribute('data-id');
    var do_number =e.getAttribute('data-do');

    bootbox.confirm("Are you sure cancel Delivery Number "+do_number+" ?",function(result){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: "{{ route('planload.cancelDO') }}",
                data: {id:id,do:do_number},
                beforeSend:function(){
                    loading_process();
                },
                success: function(response){
                    var response = response.data;

                    if (response.status==200) {
                        myalert('success',response.output);
                    }else{
                        myalert('error',response.output);
                    }
                    $('#form_filter').submit();
                },
                error: function(response) {
                    myalert('error',response);
                    $('#table-list').unblock();
                    return false;
                }
            });
    }); 

}

function setDotruck(e){
    var id =e.getAttribute('data-id');
    var no_do =e.getAttribute('data-do');
    var inv =e.getAttribute('data-inv');

    var _token = $("input[name='_token']").val();
    var tableT = $('#table-truck').DataTable({
        processing: true,
        serverSide: true,
        paging:false,
        scrollY:"400px",
        scrollCollapse: true,
        destroy:true,
        ajax: {
            url: "{{ route('planload.getTruck') }}",
            type: 'get'
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableT.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(1).css('max-width', '100px');
            $('td', row).eq(2).css('max-width', '110px');
            $('td', row).eq(3).css('max-width', '140px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'nopol', name: 'nopol'},
            {data: 'driver', name: 'driver'},
            {data: 'expedisi', name: 'expedisi'},
            {data: 'type', name: 'type'},
            {data: 'notelp', name: 'notelp'},
            {data: 'action', name: 'action'}
        ],
    });

    $('#mdid').val(id);
    $('#mdinv').val(inv);
    document.getElementById("mddo").placeholder = no_do;
    $('#modal_setDO').modal('show');
}

function setTruck(e){
    var nopol =e.getAttribute('data-nopol');
    var type =e.getAttribute('data-type');
    var driver =e.getAttribute('data-driver');
    var notelp =e.getAttribute('data-notelp');
    var expedisi =e.getAttribute('data-expedisi');

    $('#mdnopol').val(nopol);
    $('#mdtype').val(type);
    $('#mddriver').val(driver);
    $('#mdnotelp').val(notelp);
    $('#mdexpedisi').val(expedisi);
}
</script>
@endsection
