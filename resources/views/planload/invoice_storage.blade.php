@extends('layouts.app', ['active' => 'invstorage'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">INVOICE STORAGE</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#" id="form_invoice"><i class="icon-location4 position-left"></i> Invoice Storage</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<!-- filter podd -->
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('planload.getDataInvStrg') }}" id="form_filter">
            @csrf
            <div class="form-group">
                <label>PODD CHECK</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date" id="date">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- set invoice & filter set -->
<div class="panel panel-flat">
    <div class="panel-body">
        <!-- data-invoice -->
        <div class="row form-group">
            <div class="col-lg-2">
                <label><b>Plan Stuffing</b></label>
                <input type="text" name="exfact" id="exfact" class="form-control pickadate" readonly="" required="">
            </div>
            <div class="col-lg-2">
                <label><b>Invoice Number</b></label>
                <input type="text" name="no_inv" id="no_inv" class="form-control" required="" value="{{$maxinv}}">
            </div>
            <div class="col-lg-2">
                <label><b>SO</b></label>
                <input type="text" name="so" id="so" class="form-control" required="">
            </div>
            <div class="col-lg-2">
                <label><b>Remark Container</b></label>
                <select id="rem_cont" name="rem_cont" class="form-control">
                    <option value="">--Remark Container--</option>
                    <option value="Container 20FT">Container 20FT</option>
                    <option value="Container 40FT">Container 40FT</option>
                    <option value="Container 40HC">Container 40HC</option>
                    <option value="Container 45HC">Container 45HC</option>
                </select>
            </div>
            <div class="col-lg-3">
                <div class="row form-group">
                    <div class="col-md-4">
                        <label><b>CBM</b></label>
                        <input type="text" name="cbm" id="cbm" class="form-control" readonly="">
                    </div>
                    <div class="col-md-4">
                        <label><b>CTN</b></label>
                        <input type="text" name="ctn" id="ctn" class="form-control" readonly="">
                    </div>
                    <div class="col-md-4">
                        <label><b>QTY</b></label>
                        <input type="text" name="qty" id="qty" class="form-control" readonly="">
                        <input type="text" name="gw" id="gw" hidden="">
                        <input type="text" name="nw" id="nw" hidden="">
                    </div>
                </div>
                <div class="row form-group">
                    <span class="help-block info-check">*No Checked</span>
                </div>                
            </div>
            <div class="col-lg-1">
                <input type="text" name="h_custno" id="h_custno" hidden="">
                <input type="text" name="h_fwd" id="h_fwd" hidden="" hidden="">
                <input type="text" name="h_country" id="h_country" hidden="">
                <input type="text" name="h_shipmode" id="h_shipmode" hidden="">
                <!-- <input type="text" name="h_psdd" id="h_psdd" hidden=""> -->
                <input type="text" name="h_mdd" id="h_mdd"  hidden="">
                <!-- <input type="text" name="h_crd" id='h_crd' hidden=""> -->
                <button style="margin-top: 25px;" class="btn btn-primary" id="btn_save" type="button">Create Inovice</button>
            </div>
        </div>

        <!-- filter -->
        <div class="row">
            <div class="form-group">
                <div class="col-md-1"></div>
                <div class="col-md-2">
                    <label for="search_cust">Filter Customer :</label>
                    <select id="filter-customer" class="form-control" name="f_cust" >
                        <option value="">--Filter Customer--</option>
                    </select>
                </div>
                <div class="col-md-2 mdd-filter">
                    <label for="search_mdd">Filter MDD :</label>
                    <select id="filter-mdd" class="form-control" name="f_mdd" >
                        <option value="">--Filter MDD--</option>
                    </select>
                </div>
                <div class="col-md-2 psfd-filter">
                    <label for="search_psfd">Filter PSFD :</label>
                   <select id="filter-psfd" class="form-control" >
                        <option value="">--Filter PSFD--</option>
                    </select>
                </div>
                <div class="col-md-2 priority-filter">
                    <label for="search_priority">Filter Priority :</label>
                    <select id="filter-priority" class="form-control" >
                        <option value="">--Filter Priority--</option>
                    </select>
                </div>
                <div class="col-md-2 whs-filter">
                    <label for="search_whs ">Filter WHS :</label>
                    <select id="filter-whs" class="form-control" >
                        <option value="">--Filter WHS--</option>
                    </select>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- table po -->
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ORDER TYPE</th>
                        <th>PO Number</th>
                        <th width="125px">CRD</th>
                        <th width="125px">PSDD</th>
                        <th>STYLE</th>
                        <th class="fil-cust">CUST. NO.</th>
                        <th>COUNTRY</th>
                        <th class="fil-mdd" width="125px" >MDD</th>
                        <th width="175px">SHIPMODE</th>
                        <th width="175px">FWD</th>
                        <th>NEW QTY</th>
                        <th>QTY PL</th>
                        <th>CTN</th>
                        <th>CBM</th>
                        <th>GW</th>
                        <th>NW</th>
                        <th class="fil-priority">PRIORITY</th>
                        <th class="fil-whs">WHS</th>
                        <th>SUBSIDARY</th>
                        <th class="fil-psfd" width="125px">PSFD</th>
                        <th>REMARK</th>
                        <th>BRAND</th>
                        <th>FACT. CODE</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('modal')
<!-- modal remark -->
<div id="modal_remark" class="modal fade" role="dialog">
    <form class="form-horizontal" action="{{ route('planload.setRemark') }}" id="form-remark" >
        @csrf
        <div class="modal-dialog">
            <div class="modal-content modal-lg">
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="row form-group">
                            <label><b>Remark Order</b></label>
                            <textarea name="rm_order" id="rm_order" class="form-control"></textarea>
                            <input type="text" name="id" id="id" hidden="">
                            <input type="text" name="txpo" id="txpo" hidden="">
                            <input type="text" name="txplan" id="txplan" hidden="">
                        </div>
                        <div class="row form-group">
                            <label><b>Remark Exim</b></label>
                            <textarea name="rm_exim" id="rm_exim" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="close" onclick="chide();">Close</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection



@include('planload/_js_index_2')

@section('js')
<script type="text/javascript">
$(document).ready(function(){
    var url = $('#form_filter').attr('action');
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: true,
        autoLength: false,
        processing: true,
        serverSide: true,
        paging:false,
        scrollY:"400px",
        scrollX:true,
        // scrollCollapse: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            url: url,
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "date": $('#date').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            // $('td', row).eq(0).html(value);
            $('td', row).eq(3).css('min-width', '125px');
            $('td', row).eq(4).css('min-width', '125px');
            $('td', row).eq(8).css('min-width', '125px');
            $('td', row).eq(9).css('min-width', '175px');
            $('td', row).eq(10).css('min-width', '175px');
            $('td', row).eq(11).css('min-width', '175px');
            $('td', row).eq(20).css('min-width', '125px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            // {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'checkbox', name: 'checkbox',sortable: false, orderable: false, searchable: false},
            {data: 'order_type', name: 'order_type'},
            {data: 'po_number', name: 'po_number'},
            {data: 'crd', name: 'crd'},
            {data: 'po_stat_date_adidas', name: 'po_stat_date_adidas'},
            {data: 'style', name: 'style'},
            {data: 'customer_number', name: 'customer_number'},
            {data: 'country_name', name: 'country_name'},
            {data: 'mdd', name: 'mdd'},
            {data: 'shipmode', name: 'shipmode'},
            {data: 'fwd', name: 'fwd'},
            {data: 'new_qty', name: 'new_qty'},
            {data: 'item_qty', name: 'item_qty'},
            {data: 'ctn', name:'ctn'}, 
            {data: 'cbm', name:'cbm'}, 
            {data: 'gross', name:'gross'}, 
            {data: 'net', name:'net'},
            {data: 'priority_code', name: 'priority_code'},
            {data: 'whs_code', name: 'whs_code'},
            {data: 'subsidary', name: 'subsidary'},
            {data: 'psfd', name: 'psfd'}, 
            {data: 'remark', name:'remark'},
            {data: 'trademark', name:'trademark'},
            {data: 'factory_code', name:'factory_code'}, 
            {data: 'action', name:'action'}
        ],
    });

    table.columns().every( function () {
        var that = this;
        that.search('').draw();
    });


    

    $('#filter-customer').on('change',function(){
        var scust = $(this).val();
        var len = $('#table-list tbody tr').length;
       
        if(len > 0){
            
            loading_process();

            ajaxfilterMdd();

            table
            .columns('.fil-cust')
            .search(scust)
            .draw();
            
        }
    });

    $('#filter-mdd').on('change',function(){
        var smdd = $(this).val();
        var len = $('#table-list tbody tr').length;

        if(len > 0){
       
            loading_process();

            ajaxfilterPsfd();

            table
            .columns( '.fil-mdd' )
            .search( smdd )
            .draw();

            
        }
    });

    $('#filter-psfd').on('change',function(){
        var spsfd = $(this).val();
        var len = $('#table-list tbody tr').length;

        if(len > 0){
       
            loading_process();

            ajaxfilterPriority();

            table
            .columns( '.fil-psfd' )
            .search( spsfd )
            .draw();

            
        }
    });


    $('#filter-priority').on('change',function(){
        var sprio = $(this).val();
        var len = $('#table-list tbody tr').length;

        if(len > 0){
       
            loading_process();

            ajaxfilterWhs();

            table
            .columns( '.fil-priority' )
            .search( sprio )
            .draw();

            
        }
    });

    $('#filter-whs').on('change',function(){
        var swhs = $(this).val();
        var len = $('#table-list tbody tr').length;

        if(len > 0){
       
            loading_process();
            table
            .columns( '.fil-whs' )
            .search( swhs )
            .draw();

           
        }
    });


    table.on('preDraw', function() {
        Pace.start();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        loading_process();

        ajaxfilterCust();

        table.clear();
        table.draw();

    });
    


    function calculate() {

        var cbm = 0;
        var ctn = 0;
        var qty = 0;
        var gw = 0;
        var nw = 0;

        var info = '*No Checked';

        var arr = [];
        var po_ = [];
        var arrs = [];

        $('.poplan:checked').each(function(e, i) {
            arr.push({
                'cbm':$(this).data('cbm'),
                'ctn':$(this).data('ctn'),
                'qty':$(this).data('qty'),
                'gw':$(this).data('gw'),
                'nw':$(this).data('nw'),
            });
            arrs.push({
                'psfd': $(this).data('psfd'),
                'mdd': $(this).data('mdd'),
                // 'psdd': $(this).data('psdd'),
                'customer': $(this).data('customer'),
                'destination': $(this).data('country'),
                'fwd': $(this).data('fwd'),
                'shipmode': $(this).data('ship'),
                // 'crd': $(this).data('crd'),
            });
        });

        //cek psfd1 dll
        $.each(arrs, function(e, i) {
            var checkboxes = document.getElementsByName('selector[]');
            
            if(i['psfd'] === ''){
                    myalert('error', 'PSFD  must be filled..!')
               
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = false;
                        }
                    }
            }

          

            // if(i['psdd'] === ''){
            //         myalert('error', 'PSDD must be filled..!')
            
            //         for (var i = 0; i < checkboxes.length; i++) {
            //             if (checkboxes[i].type == 'checkbox') {
            //                 checkboxes[i].checked = false;
            //             }
            //         }
            // }

            if(i['customer'] === ''){
                    myalert('error', 'Customer number must be filled..!')
            
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = false;
                        }
                    }
            }


            if(i['destination'] === ''){
                    myalert('error', 'Destination must be filled..!')
            
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = false;
                        }
                    }
            }

            // penomoran invoice
            if (e >= 0) {
                $('#exfact').removeAttr('readonly');
                $('#so').removeAttr('readonly');
                $('#remark_cont').removeAttr('readonly');
                $('#exfact').val('');
                $('#so').val('');
                $('#remark_cont').val('');
                // cek mdd, statistical date, lc date, customer number, fwd, destination
                if (e == 0){
                    $('#h_mdd').val(i['mdd']);
                    // $('#h_psdd').val(i['statistical']);
                    $('#h_custno').val(i['customer']);
                    $('#h_fwd').val(i['fwd']);
                    $('#h_country').val(i['destination']);
                    $('#h_shipmode').val(i['shipmode']);
                    // $('#h_crd').val(i['crd']);
                }else{
                    
                    if (i['customer'] != $('#h_custno').val() || i['fwd'] != $('#h_fwd').val() || i['destination'] != $('#h_country').val() || i['shipmode'] != $('#h_shipmode').val() ) {

                        myalert('error', 'customer/fwd/destination/shipmode must be same..!')
               
                        for (var i = 0; i < checkboxes.length; i++) {
                            if (checkboxes[i].type == 'checkbox') {
                                checkboxes[i].checked = false;
                            }
                        }
                    }
                }
            }else{
                $('#exfact').attr('readonly', true);
                $('#so').attr('readonly', true);
                $('#remark_cont').attr('readonly', true);
                $('#exfact').val('');
                $('#so').val('');
                $('#remark_cont').val('');
            }

        });

        for (var i=0; i<arr.length; i++){
            cbm += parseFloat(arr[i]['cbm']);
            ctn += parseFloat(arr[i]['ctn']);
            qty += parseFloat(arr[i]['qty']);
            gw += parseFloat(arr[i]['gw']);
            nw += parseFloat(arr[i]['nw']);
        }


        $('.poplan:checked').each(function(e, i) {
           

            po_.push($(this).data('po'));
        });

        // info
        var vals = "";

        for (var j=0; j<po_.length; j++){
                vals +=", "+po_[j];
        }

        if (vals) vals = vals.substring(1);


        if (arr.length > 0) {
            info = '*'+arr.length+' Plan Ref Number Checked ( '+vals+' )';
        }

        $('.info-check').html(info);

        if(isNaN(cbm)){
            $('#cbm').val(0);
        }else{
            $('#cbm').val(cbm.toFixed(3));
        }


        if(isNaN(ctn)){
            $('#ctn').val(0);
        }else{
            $('#ctn').val(ctn);
        }

        if(isNaN(qty)){
            $('#qty').val(0);
        }else{
            $('#qty').val(qty);
        }

        if(isNaN(gw)){
            $('#gw').val(0);
        }else{
            $('#gw').val(gw.toFixed(3));
        }

        if(isNaN(nw)){
            $('#nw').val(0);
        }else{
            $('#nw').val(nw.toFixed(3));
        }

    }

    $('div').delegate('input:checkbox', 'click', calculate);

    $('#btn_save').on('click',function(){

        var data = [];

        var planstuffing = $('#exfact').val();
        var invoice_no = $('#no_inv').val();
        var so = $('#so').val();
        var remark_cont = $('#rem_cont').val();
        var cbm = $('#cbm').val();
        var ctn = $('#ctn').val();
        var qty = $('#qty').val();
        var cust_no = $('#h_custno').val();
        var fwd = $('#h_fwd').val();
        var country = $('#h_country').val();
        var shipmode = $('#h_shipmode').val();
        var mdd = $('#h_mdd').val();
        var gw = $('#gw').val();
        var nw = $('#nw').val();

        $('.poplan:checked').each(function() {
            data.push({
                id: $(this).val(),
                ponumber: $(this).data('po'),
                planref: $(this).data('planref')
            });
        });
        
        if (data.length>0) {

            bootbox.confirm("Are you sure grouping invoice ?", function(result){
                if (result) {

                    // if(so.length != 10 || invoice_no.length != 7){
                    //     myalert('error', 'SO must 10 character OR Inovice No mus 7 character . . .!!');
                    //     return false;
                    // }

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type:'post',
                        url : "{{ route('planload.createInvoice') }}",
                        data : {data:data,invoice_no:invoice_no,so:so,remark_cont:remark_cont,cbm:cbm,ctn:ctn,qty:qty,cust_no,cust_no,fwd:fwd,country:country,shipmode:shipmode,mdd:mdd,planstuffing:planstuffing,gw:gw,nw:nw},
                        success:function(response){
                            $('#table-list').unblock();
                            $('#exfact').val('');
                            $('#no_inv').val('');
                            $('#so').val('');
                            $('#rem_cont').val('');
                            $('#cbm').val(0);
                            $('#ctn').val(0);
                            $('#qty').val(0);
                            $('#gw').val(0);
                            $('#nw').val(0);
                            $('#h_custno').val('');
                            $('#h_shipmode').val('');
                            $('#h_country').val('');
                            $('#h_fwd').val('');
                            // $('#h_psdd').val('');
                            $('#h_mdd').val('');
                            $('.info-check').val('*No Checked');
                             myalert('success', 'Invoice created succesfully..!');
                            loading_process();
                            table.draw();

                        },
                        error:function(response){
                            if(response['status'] == 422) {
                                myalert('error', 'Oopss Something wrong..!');
                                console.log(response);
                            }

                        }
                    });
                }
            });
        }

    });


    $('#form-remark').submit(function(event){
        event.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url:$('#form-remark').attr('action'),
            type: "POST",
            data:{
                "po_number": $('#txpo').val(),  
                "plan_ref": $('#txplan').val(),
                "remark_exim": $('#rm_exim').val(), 
                "remark_order": $('#rm_order').val(),                           
                "_token": _token,
            },
            success: function(response){
                console.log(response);
               myalert('success',response);
                chide();
               $('#form_filter').submit();
            },
            error: function(response) {
                if(response['status'] == 422) {
                    myalert('error',response);
                }
            }
        });
    });


    $('#table-list').on('blur','.mdd_unfilled',function(){
        var id = $(this).data('id');
        var po = $(this).data('po');
        var plan_ref = $(this).data('plan');
        var mdd = $(this).val();

       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{route('planload.updateMdd')}}",
            data: {id:id, mdd:mdd, po_number:po, plan_ref_number:plan_ref},
            beforeSend:function(){
                loading_process();
            },
            success: function(response){
                $('#table-list').unblock();
                $('#mdd_' + po).html(response);
                //$('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });
    });

    $('#table-list').on('blur','.fwd_unfilled',function(){
        var id = $(this).data('id');
        var po = $(this).data('po');
        var plan_ref = $(this).data('plan');
        var fwd = $(this).val();
       
       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{route('planload.updateFwd')}}",
            data: {id:id, fwd:fwd, po_number:po, plan_ref_number:plan_ref},
            beforeSend:function(){
                loading_process();
            },
            success: function(response){
                $('#table-list').unblock();
                $('#fwd_' + po).html(response);
                //$('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });
    });

    $('#table-list').on('blur','.prio_unfilled',function(){
        var id = $(this).data('id');
        var po = $(this).data('po');
        var plan_ref = $(this).data('plan');
        var priority_code = $(this).val();
       
       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{route('planload.updatePriority')}}",
            data: {id:id, priority_code:priority_code, po_number:po, plan_ref_number:plan_ref},
            beforeSend:function(){
                loading_process();
            },
            success: function(response){
                $('#table-list').unblock();
                $('#prio_' + po).html(response);
                //$('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });
    });

    $('#table-list').on('blur','.sm_unfilled',function(){
        var id = $(this).data('id');
        var po = $(this).data('po');
        var plan_ref = $(this).data('plan');
        var shipmode = $(this).val();
       
       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "{{route('planload.updateShipmode')}}",
            data: {id:id, shipmode:shipmode, po_number:po, plan_ref_number:plan_ref},
            beforeSend:function(){
                loading_process();
            },
            success: function(response){
                $('#table-list').unblock();
                $('#sm_' + po).html(response);
                //$('#form_filter').submit();
            },
            error: function(response) {
                return false;
            }
        });
    });


});
// enddocs

function ajaxfilterCust(){
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : "{{route('planload.ajaxGetCustomer')}}",
            data :{date:$('#date').val()},
            success: function(response) {
                
                var cust = response.cust;
               
                $('#filter-customer').empty();
                $('#filter-customer').append('<option value="">--Filter Customer--</option>');
                for (var i = 0; i < cust.length; i++) {
                 
                   $('#filter-customer').append('<option value="'+cust[i]['customer_number']+'">'+cust[i]['customer_number']+'</option>');
                }
            },
            error: function(response) {
                console.log(response);
            }
        });
}
function ajaxfilterMdd(){
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : "{{route('planload.ajaxGetMdd')}}",
            data :{date:$('#date').val(),custno:$('#filter-customer').val()},
            success: function(response) {
             
                var mdd = response.mdd;
               
                $('#filter-mdd').empty();
                $('#filter-mdd').append('<option value="">--Filter MDD--</option>');
                for (var i = 0; i < mdd.length; i++) {
                 
                   $('#filter-mdd').append('<option value="'+mdd[i]['mdd']+'">'+mdd[i]['mdd']+'</option>');
                }
            },
            error: function(response) {
                console.log(response);
            }
        });
}
function ajaxfilterPsfd(){
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : "{{route('planload.ajaxGetPsfd')}}",
            data :{date:$('#date').val(),custno:$('#filter-customer').val(),mdd:$('#filter-mdd').val()},
            success: function(response) {
             
                var psfd = response.psfd;
               
                $('#filter-psfd').empty();
                $('#filter-psfd').append('<option value="">--Filter PSFD--</option>');
                for (var i = 0; i < psfd.length; i++) {
                 
                   $('#filter-psfd').append('<option value="'+psfd[i]['psfd']+'">'+psfd[i]['psfd']+'</option>');
                }
            },
            error: function(response) {
                console.log(response);
            }
        });
}
 
function ajaxfilterPriority(){
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : "{{route('planload.ajaxGetPriority')}}",
            data :{date:$('#date').val(),custno:$('#filter-customer').val(),mdd:$('#filter-mdd').val(),psfd:$('#filter-psfd').val()},
            success: function(response) {
                
                var priority = response.priority;
              
                $('#filter-priority').empty();
                $('#filter-priority').append('<option value="">--Filter Priority--</option>');
                for (var i = 0; i < priority.length; i++) {
               
                   $('#filter-priority').append('<option value="'+priority[i]['priority_code']+'">'+priority[i]['priority_code']+'</option>');
                }
            },
            error: function(response) {
                console.log(response);
            }
        });
}


function ajaxfilterWhs(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{route('planload.ajaxGetWhs')}}",
        data :{date:$('#date').val(),custno:$('#filter-customer').val(),mdd:$('#filter-mdd').val(),psfd:$('#filter-psfd').val(),priority:$('#filter-priority').val()},
        success: function(response) {
            
            var whs = response.whs;
          
            $('#filter-whs').empty();
            $('#filter-whs').append('<option value="">--Filter WHS--</option>');
            for (var i = 0; i < whs.length; i++) {
           
               $('#filter-whs').append('<option value="'+whs[i]['whs_code']+'">'+whs[i]['whs_code']+'</option>');
            }
        },
        error: function(response) {
            console.log(response);
        }
    });
}

function toggle(source) {
    checkboxes = document.getElementsByName('selector[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = source.checked;
    }

}

function remarkInv(e){
    var po = e.getAttribute('data-po');
    var planref = e.getAttribute('data-plan');
    var id = e.getAttribute('data-id');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{route('planload.invGetRemark')}}",
        data :{id:id},
        success: function(response) {
            var remark = response.remark;

            
            $('#id').val(id);
            $('#txpo').val(po);
            $('#txplan').val(planref);
            $('#rm_order').val(remark['remark_order']);
            $('#rm_exim').val(remark['remark_exim']);
            $('#modal_remark').modal('show');
        },
        error: function(response) {
            console.log(response);
        }
    });

}

function chide(){
    $('#rm_order').val('');
    $('#rm_exim').val('');
    $('#modal_remark').modal('hide');
}

function deletePlan(e){
    var po = e.getAttribute('data-po');
    var plan = e.getAttribute('data-plan');
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }); 
    $.ajax({
        type: 'get',
        url: "{{route('planload.deletePoDraf')}}",
        data: {po:po, plan:plan},
        beforeSend: function() {
            loading_process();
        },
        success: function(response){
           myalert('success',response);
           
           $('#form_filter').submit();
        },
        error: function(response) {
            if(response['status'] == 422) {
                myalert('error',response);
            }
        }
    });
}
</script>
@endsection