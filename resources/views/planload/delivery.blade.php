@extends('layouts.app', ['active' => 'deliveryorder'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold"> DELIVERY ORDER</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#" id="form_invoice"><i class="icon-location4 position-left"></i> Delivery Order</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group">
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-2">
                    <label><b>Draf Number</b></label>
                    <input type="text" name="do_number" id="do_number" class="form-control" placeholder="Delivery Number" hidden="" value="{{$draf}}" readonly="">
                </div>

                <div class="col-lg-2">
                    <label><b>Warehouse</b></label>
                    <select class="form-control" id="whs" name="whs" required="">
                        <option value="">--Choosse Warehouse--</option>
                        <?php foreach ($whs as $w) {
                        ?>
                            <option value="{{ $w->id }}">{{ $w->whs_name.' - '.$w->address }}</option>
                        <?php 
                        } ?>
                    </select>
                </div>
                <div class="col-lg-2">
                    <label><b>Remark</b></label>
                    <input type="text" name="remark" id="remark" class="form-control" placeholder="Remark" onkeyup="this.value = this.value.toUpperCase();">
                </div>
                <div class="col-lg-2">
                    <div class="row form-group">
                        <div class="col-lg-6">
                            <label><b>CBM</b></label>
                            <input type="text" name="cbm_val" id="cbm_val" class="form-control" readonly="">
                        </div>
                        <div class="col-lg-6">
                            <label><b>CTN</b></label>
                            <input type="text" name="ctn_val" id="ctn_val" class="form-control" readonly="">
                            <input type="text" name="qty_val" id="qty_val" hidden="" >
                            <input type="text" name="gw_val" id="gw_val" hidden="">
                            <input type="text" name="nw_val" id="nw_val" hidden="">

                        </div>
                    </div>
                </div>               
                <div class="col-lg-1">
                    <button style="margin-top: 25px;" class="btn btn-primary" id="btn_save" name="btn_save"><span class="icon-quill4"></span> Save</button>
                </div>
            </div>
            <div class="row">
                <div></div>
                <span style="margin-left: 20px;" class="help-block info-check">*No Checked</span>
                <input type="text" name="h_plan" id="h_plan" hidden="" >
                <input type="text" name="h_ship" id="h_ship" hidden="">
                <input type="text" name="h_fwd" id="h_fwd" hidden="">
            </div>
        </div>
        <hr>
        <div class="row form-group">
            <div class="col-lg-2">
                
            </div>
            <div class="col-lg-3">
                <label><b>Filter Plan Stuffing</b></label>
                <select class="form-control" id="fil_plan">
                    <option value="">--Filter Plan Stuffing--</option>
                    <?php foreach ($planstuffing as $pl) {
                    ?>
                            <option value="{{ $pl->plan_stuffing }}">{{ $pl->plan_stuffing }}</option>
                    <?php 
                    } ?>
                </select>
            </div>
            <div class="col-lg-3">
                <label><b>Filter Shipmode</b></label>
                <select class="form-control" id="fil_ship">
                    <option value="">--Filter Shipmode--</option>
                </select>
            </div>
            <div class="col-lg-3">
                <label><b>Filter FWD</b></label>
                <select class="form-control" id="fil_fwd">
                    <option value="">--Filter Forwarder--</option>
                </select>
            </div>
        </div>
        <div class="row form-group">
            <div class="table-responsive">
                <table class="table datatable-save-state" id="table-list">
                    <thead>
                        <th>#</th>
                        <th>Invoice</th>
                        <th class="fil-plan">Plan Stuffing</th>
                        <th>Remark Cont.</th>
                        <th width="150px" style="word-wrap: break-word;" >PO</th>
                        <th>SO</th>
                        <th class="fil-ship">Shipmode</th>
                        <th class="fil-fwd">FWD</th>
                        <th>Destination</th>
                        <th>Cust. NO.</th>
                        <th>CBM</th>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


@include('planload/_js_index_2')
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            url: "{{ route('planload.getDataDO') }}",
            type: 'get'
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            // $('td', row).eq(0).html(value);
            $('td', row).eq(4).css('max-width', '150px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            // {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'checkbox', name: 'checkbox',sortable: false, orderable: false, searchable: false},
            {data: 'invoice', name: 'invoice'},
            {data: 'plan_stuffing', name: 'plan_stuffing'},
            {data: 'remark_cont', name: 'remark_cont'},
            {data: 'po_number', name: 'po_number'},
            {data: 'so', name: 'so'},
            {data: 'shipmode', name: 'shipmode'},
            {data: 'fwd', name: 'fwd'},
            {data: 'destination', name: 'destination'},
            {data: 'customer_number', name: 'customer_number'},
            {data: 'cbm', name: 'cbm'}
        ],
    });
    //end of datatables

    table.columns().every( function () {
        var that = this;
        that.search('').draw();
    });


    table.on('preDraw', function() {
        Pace.start();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    $('#fil_plan').on('change',function(){
        var splan = $(this).val();
        var len = $('#table-list tbody tr').length;

        if (len > 0) {
            loading_process();
            ajaxGetShip();
            table
            .columns('.fil-plan')
            .search(splan)
            .draw();
        }
    });

    $('#fil_ship').on('change',function(){
        var sship = $(this).val();
        var len = $('#table-list tbody tr').length;

        if (len > 0) {
            loading_process();
             ajaxGetFwd();
            table
            .columns('.fil-ship')
            .search(sship)
            .draw();
        }
    });

    $('#fil_fwd').on('change',function(){
        var sfwd = $(this).val();
        var len = $('#table-list tbody tr').length;

        if (len > 0) {
            loading_process();
             
            table
            .columns('.fil-fwd')
            .search(sfwd)
            .draw();
        }
    });


    //calculate check invoice

    function calculate(){
        var cbm = 0;
        var ctn = 0;
        var qty = 0;
        var gw = 0;
        var nw = 0;

        var info = '*No Checked';

        var arr = [];
        var inv_ = [];
        var arrs = [];

        $('.invplan:checked').each(function(e,i){
            arr.push({
                'cbm':$(this).data('cbm'),
                'ctn':$(this).data('ctn'),
                'qty':$(this).data('qty'),
                'gw':$(this).data('gw'),
                'nw':$(this).data('nw')
            });

            arrs.push({
                'plan':$(this).data('plan'),
                'ship':$(this).data('ship'),
                'fwd':$(this).data('fwd')
            });

        });

        // invplanchecked


        // cek arrs
        $.each(arrs,function(e,i){
            var checkboxes = document.getElementsByName('selector[]');

            if (i['plan']==='') {
                myalert('error', 'Plan Stuffing  must be filled..!')

                for (var i = 0; i < checkedboxes.length; i++) {
                        if (checkedboxes[i].type=='checkbox') {
                            checkedboxes[i].checked = false;

                            clean();
                        }
                }
            }
            // plan

            if (i['ship']==='') {
                myalert('error', 'Shipmode  must be filled..!')

                for (var i = 0; i < checkedboxes.length; i++) {
                        if (checkedboxes[i].type=='checkbox') {
                            checkedboxes[i].checked = false;

                            clean();
                        }
                }
            }
            // shipmode

            if (i['fwd']==='') {
                myalert('error', 'Forwarder  must be filled..!')

                for (var i = 0; i < checkedboxes.length; i++) {
                        if (checkedboxes[i].type=='checkbox') {
                            checkedboxes[i].checked = false;

                            clean();
                        }
                }
            }
            // fwd


            //set inv do & validasi
            if (e>=0) {
                if (e==0) {
                    $('#h_plan').val(i['plan']);
                    $('#h_ship').val(i['ship']);
                    $('#h_fwd').val(i['fwd']);
                    
                }else{
                    if (i['plan']!=$('#h_plan').val() || i['fwd']!=$('#h_fwd').val() || i['ship']!=$('#h_ship').val() ) {
                         myalert('error', 'Plan Stuffing/Fwd/Shipmode must be same..!')

                         for (var i = 0; i < checkboxes.length; i++) {
                            if (checkboxes[i].type == 'checkbox') {
                                checkboxes[i].checked = false;
                                
                                clean();
                            }
                        }
                         
                    }
                }
            }
            
        });
        // cek arrs

        // sum cbm,qty,ctn,gw,nm
        for (var i = 0; i < arr.length; i++) {
            cbm += parseFloat(arr[i]['cbm']);
            ctn += parseFloat(arr[i]['ctn']);
            qty += parseFloat(arr[i]['qty']);
            gw += parseFloat(arr[i]['gw']);
            nw += parseFloat(arr[i]['nw']);
        }
        // sum cbm,qty,ctn,gw,nm

        // set invoice info
        $('.invplan:checked').each(function(e,i){
            inv_.push($(this).data('invoice'));
        });

        var vals = "";

        for (var j = 0; j < inv_.length; j++) {
            vals +=", "+inv_[j];
        }

        if (vals) vals = vals.substring(1);

        if (arr.length>0) {
            info = '* '+arr.length+' Invoice Checked ('+vals+')';
        }

        $('.info-check').html(info);
        // set invoice info

        // set val cbm,ctn,qty
        if (isNaN(cbm)) {
            $('#cbm_val').val(0);
        }else{
            $('#cbm_val').val(cbm);
        }

        if (isNaN(ctn)) {
            $('#ctn_val').val(0);
        }else{
            $('#ctn_val').val(ctn);
        }

        if (isNaN(qty)) {
            $('#qty_val').val(0);
        }else{
            $('#qty_val').val(qty);
        }

        if (isNaN(gw)) {
            $('#gw_val').val(0);
        }else{
            $('#gw_val').val(gw);
        }

        if (isNaN(nw)) {
            $('#nw_val').val(0);
        }else{
            $('#nw_val').val(nw);
        }
        // set val cbm,ctn,qty

    }
    // calculate

    $('div').delegate('input:checkbox', 'click', calculate);


    $('#btn_save').on('click',function(){
        
        if ($('#whs').val()=='' || $('#type').val()=='' || $('#do_number').val().trim()=='') {
            myalert('error', 'DO Number / Warehouse must be filled . . .!!!')
            return false;
        }

        var data = [];

        var do_number = $('#do_number').val();
        var warehouse = $('#whs').val();
        var remark = $('#remark').val();
        var cbm = $('#cbm_val').val();
        var ctn = $('#ctn_val').val();
        var qty = $('#qty_val').val();
        var gw = $('#gw_val').val();
        var nw = $('#nw_val').val();
        var plan = $('#h_plan').val();
        var ship = $('#h_ship').val();
        var fwd = $('#h_fwd').val();


        $('.invplan:checked').each(function() {
            data.push({
                id: $(this).val(),
                invoice: $(this).data('invoice')
            });
        });

        if (data.length>0) {
            bootbox.confirm("Are you sure grouping truck ? ", function(result){
                if (result) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type:'post',
                        url : "{{ route('planload.createDO') }}",
                        data : {data:data,warehouse:warehouse,remark:remark,cbm:cbm,ctn:ctn,qty:qty,gw:gw,nw:nw,plan:plan,ship:ship,fwd:fwd,do_number:do_number},
                        beforeSend:function(){
                            loading_process();
                        },
                        success:function(response){
                            var data_response = response.data;
                            if (data_response.status==200) {
                                myalert('success',data_response.output);
                                

                                window.location.reload();
                                
                                
                            }else{
                                myalert('error',data_response.output);
                                $('#table-list').unblock();
                            }
                        },
                        error:function(response){
                            console.log(response);
                            if(data_response.status == 422) {
                                myalert('error', data_response.output);
                                console.log(response);
                            }

                        }
                    });
                }
            });
        }else{
            myalert('error','select invoice first . . !');
        }

    });

});

function clean(){
    $('#h_plan').val('');
    $('#h_ship').val('');
    $('#h_fwd').val('');
}

function ajaxGetShip(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{ route('planload.ajaxGetShip') }}",
        data :{plan:$('#fil_plan').val()},
        success: function(response) {
            
            var ship = response.ship;

            $('#fil_ship').empty();
            $('#fil_ship').append('<option value="">--Filter Shipmode--</option>');
            for (var i = 0; i < ship.length; i++) {
           
               $('#fil_ship').append('<option value="'+ship[i]['shipmode']+'">'+ship[i]['shipmode']+'</option>');
            }
        },
        error: function(response) {
            console.log(response);
        }
    });
}

function ajaxGetFwd(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{ route('planload.ajaxGetFwd') }}",
        data :{plan:$('#fil_plan').val(),shipmode:$('#fil_ship').val()},
        success: function(response) {
            
            var fwd = response.fwd;

            $('#fil_fwd').empty();
            $('#fil_fwd').append('<option value="">--Filter Forwarder--</option>');
            for (var i = 0; i < fwd.length; i++) {
           
               $('#fil_fwd').append('<option value="'+fwd[i]['fwd']+'">'+fwd[i]['fwd']+'</option>');
            }
        },
        error: function(response) {
            console.log(response);
        }
    });
}

function toggle(source) {
    checkboxes = document.getElementsByName('selector[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = source.checked;
    }

}

function ajaxGetWhs(fwd){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{ route('planload.ajaxGetWhsAdd') }}",
        data :{fwd:fwd},
        success: function(response) {
            
            var whs = response.whs;

            $('#whs').empty();
            $('#whs').append('<option value="">--Choose Warehouse--</option>');
            for (var i = 0; i < whs.length; i++) {
           
               $('#whs').append('<option value="'+whs[i]['id']+'">'+whs[i]['whs_name']+' - '+whs[i]['address']+'</option>');
            }
        },
        error: function(response) {
            console.log(response);
        }
    });
}


</script>
@endsection