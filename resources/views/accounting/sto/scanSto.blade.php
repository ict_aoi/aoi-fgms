@extends('layouts.app', ['active' => 'scan_sto'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">SCAN STO</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#" id="form_invoice"><i class="icon-location4 position-left"></i> Scan STO</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-4">
                <label>Factory</label>
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->factory_id }}" {{ $val->factory_id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-4">
                <label>STO Location</label>
                <select class="form-control" name="location" id="location">
                    <option value="metal">--Select Location--</option>
                </select>
                <input type="text" name="locat" id="locat" class="form-control hidden">
            </div>
            <div class="col-lg-4">
                <label>Remark</label>
                <input type="text" name="remark" id="remark" class="form-control" placeholder="Remark">
            </div>
        </div>
        <hr>
        <div class="row">
            <form action="{{ route('accSto.checkScan') }}" id="form-scan">
                <div class="col-lg-8">
                    <label>Barcode ID</label>
                    <input type="text" name="barcode_id" id="barcode_id" class="form-control barcode_id" placeholder="#scan in here" required="">
                </div>
                <div class="col-lg-4">
                    <label style="color: white;">count</label>
                    <input type="text" name="count_scan" id="count_scan" value="0" style="text-align: center; font-size: 28px;" class="form-control" readonly="">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="show-list">
                <thead>
                    <tr>
                        <th width="10%">BARCODE ID</th>
                        <th width="8%">PO NUMBER</th>
                        <th width="8%">PLAN REF.</th>
                        <th width="6%">BRAND</th>
                        <th width="8%">STYLE</th>
                        <th width="7%">ART. NO.</th>
                        <th width="12%">CUST. SIZE</th>
                        <th width="12%">MANF. SIZE</th>
                        <th width="4%">QTY</th>
                        <th>LOCATION</th>
                        <th>ACTUAL</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@include('accounting/_js_index_2')
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $('#barcode_id').focus();

    $('#factory_id').change(function(event){
        event.preventDefault();
        $('#locat').val('');

        getLocation();
    });

    $(window).on('load',function(){
        getLocation();
    });

    $('#location').change(function(){
        var seloc = $('#location').val();

        $('#locat').val(seloc);
    });

    
    $('.barcode_id').keypress(function(event){
        if (event.which==13) {
            event.preventDefault();

            var barcode_id = $('#barcode_id').val();
            var locat = $('#locat').val();
            var factory_id = $('#factory_id').val();

            if (locat=='' ) {
                myalert('error', 'Location cannot empty !!!');
            }else if(barcode_id=='' ){
                myalert('error', 'Barcode cannot empty !!!');
            }else{
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',
                    url: $('#form-scan').attr('action'),
                    data: {barcode_id: $('#barcode_id').val(),locat:locat,factory_id:factory_id,remark:$('#remark').val()},
                    success: function(response){
                        $('#show-list').prepend(response);
                        var count_s = +$('#count_scan').val()+1;

                        $('#count_scan').val(count_s);
                        $('#barcode_id').val('');
                    },
                    error: function(response){
                        // console.log(response);
                        if(response['status'] == 422) {
                            myalert('error', response['responseJSON']);
                        }
                        $('#barcode_id').val('');
                    }
                });
            }
        }
    });
    

});

function getLocation(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{ route('accSto.getLocator') }}",
        data :{factory_id:$('#factory_id').val()},
        success: function(response) {
            
            var locs = response.locators;
            $('#location').empty();
            $('#location').append('<option value="">--Select Location--</option>');
            $('#location').append('<option value="metal">Metal Detector</option>');
            $('#location').append('<option value="loading">Loading</option>');
            $('#location').append('<option value="qcinspect">QC Inspect</option>');
            for (var i = 0; i < locs.length; i++) {
           
               $('#location').append('<option value="'+locs[i]['barcode']+'">'+locs[i]['code']+'</option>');
            }
        },
        error: function(response) {
            console.log(response);
        }
    });

    
}
</script>
@endsection