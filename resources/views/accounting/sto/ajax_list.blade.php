@foreach($data as $dt)
<tr>
	<td>{{$dt['barcode_id']}}</td>
	<td>{{$dt['po_number']}}</td>
	<td>{{$dt['plan_ref_number']}}</td>
	<td>{{$dt['trademark']}}</td>
	<td>{{$dt['upc']}}</td>
	<td>{{$dt['buyer_item']}}</td>
	<td>{{$dt['customer_size']}}</td>
	<td>{{$dt['manufacturing_size']}}</td>
	<td>{{$dt['inner_pack']}}</td>
	<td>{{$dt['locator']}}</td>
	<td>{{$dt['locator_sto']}}</td>
	<td>
		<?php 
			if ($dt['is_cancel_order']==false) {
				echo '<span class="label label-success">Active</span>';
			}else{
				echo '<span class="label label-danger">Cancel/Reduce</span>';
			}
		 ?>
	</td>
</tr>
@endforeach