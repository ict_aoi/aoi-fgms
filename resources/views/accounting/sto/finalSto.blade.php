@extends('layouts.app', ['active' => 'final_sto'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">REPORT FINAL STO</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="#" id="form_invoice"><i class="icon-location4 position-left"></i> Report Final STO</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('accSto.ajaxGetDataFinal') }}" id="form-filter">
            <div class="row form-group">
                <label>Select Date</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range" required>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row {{ Auth::user()->admin_role !== 1 ? 'hidden' : '' }}">
            <div class="col-md-2"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->factory_id }}" {{ $val->factory_id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Barcode ID</th>
                        <th>Style</th>
                        <th>Art. No.</th>
                        <th>Brand</th>
                        <th>PO Number</th>
                        <th>Plan Ref.</th>
                        <th>Cust. Size</th>
                        <th>Manuf. Size</th>
                        <th>Qty</th>
                        <th>FGMS Loc.</th>
                        <th>STO Loc.</th>
                        <th>Date STO</th>
                        <th>Remark</th>
                        <th>Status</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('accSto.exportFinalSto') }}" id="exportReportFinalSto"></a>
@endsection
@include('report/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    // var columns = table.settings().init().columns;
                    // var current_order = table.order();

                    // var column_name = columns[current_order[0][0]].name;
                    // var direction = current_order[0][1];
                    // var filter = table.search();
                    window.location.href = $('#exportReportFinalSto').attr('href')
                                            + '?date_range=' + $('#date_range').val()
                                            + '&factory_id=' + $('#factory_id').val();
                }
            }
       ],
        ajax: {
            url: $('#form-filter').attr('action'),
            type: 'get',
            data: function (d) {
                return $.extend({},d,{
                    "date_range": $('#date_range').val(),
                    "factory_id": $('#factory_id').val(),
                    "filterby": $('.dataTables_filter input').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ], 
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'barcode_id', name: 'barcode_id'},
            {data: 'upc', name: 'upc'},
            {data: 'buyer_item', name: 'buyer_item'},
            {data: 'trademark', name: 'trademark'},
            {data: 'po_number', name: 'po_number'},
            {data: 'plan_ref_number', name: 'plan_ref_number'},
            {data: 'customer_size', name: 'customer_size', sortable: false, orderable: false, searchable: false},
            {data: 'manufacturing_size', name: 'manufacturing_size', sortable: false, orderable: false, searchable: false},
            {data: 'inner_pack', name: 'inner_pack'},
            {data: 'current_loc', name: 'current_loc'},
            {data: 'sto_loc', name: 'sto_loc'},
            {data: 'sto_date', name: 'sto_date', sortable: false, orderable: false, searchable: false},
            {data: 'remark_sto', name: 'remark_sto', sortable: false, orderable: false, searchable: false},
            {data: 'order_status', name: 'order_status', sortable: false, orderable: false, searchable: false}
        ],
    });

    table
    .on( 'preDraw', function () {
        Pace.start();
        loading_process();
    } )
    .on( 'draw.dt', function () {
        Pace.stop();
        $('#table-list').unblock();
    } );

    $('#form-filter').submit(function(event){
        

        event.preventDefault();

        //check location
        if($('#date_range').val() == '') {
            alert('Please select date range first');
            return false;
        }

        table.draw();
    });

    $('#factory_id').on('change', function() {
        table.draw();
    });

    $('#date_range').on('change', function(){
        date = $(this).val();
    });
});
</script>
@endsection