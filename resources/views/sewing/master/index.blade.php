@extends('layouts.app', ['active' => 'mastersewing'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">SEWING</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('sewing.master') }}"><i class="icon-stack2 position-left"></i> Sewing</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
	<div class="panel panel-flat">
	    <div class="panel-heading">
	        <div class="form-group">
	            <button type="button" class="btn btn-xs btn-primary add_sewing">Add Sewing <span class="icon-plus2"></span></button>
	        </div>
	    </div>
	    <div class="panel-body">
            <div class="table-responsive">
    	        <table class="table datatable-save-state" id="table_sewing">
    	            <thead>
    	                <tr>
    	                    <th style="width:10px;">#</th>
    	                    <th>NAME</th>
    	                    <th>DESCRIPTION</th>
    	                    <th>PROUD MAX CAPACITY</th>
    	                    <th>NUMBER EMPLOYEE</th>
                            <th>IP ADDRESS</th>
    	                    <th style="width:10px;">ACTION</th>
    	                </tr>
    	            </thead>
    	        </table>
            </div>
	    </div>
	</div>

	<!-- IMPORTANT LINK -->
	<a href="{{ route('sewing.ajaxGetData') }}" id="sewing_get_data"></a>
	<!-- /IMPORTANT LINK -->
@endsection

@section('modal')
<!-- MODAL ADD SEWING -->
<div id="modal_add_new_sewing_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('sewing.addsewing') }}" id="form-add-sewing">
            <div class="modal-content">
              	<div class="modal-body">
                  	<div class="panel-body loader-area">
                      	<fieldset>
                          	<legend class="text-semibold">
                              	<i class="icon-file-text2 position-left"></i>
                              	<span id="title"> ADD NEW SEWING</span> <!-- title -->
                          	</legend>

                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Name:</label>
                              	<div class="col-lg-9">
                                  	<input type="text" class="form-control" name="name" placeholder="Type Name">
                              	</div>
                          	</div>
                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Description:</label>
                              	<div class="col-lg-9">
                                  	<input type="text" class="form-control" name="description" placeholder="Type Description">
                              	</div>
                          	</div>
                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Prod Max Capacity:</label>
                              	<div class="col-lg-9">
                                  	<input type="text" class="form-control" name="prod_max_capacity" placeholder="Type Prod Max Capacity">
                              	</div>
                          	</div>
                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Number Employee:</label>
                              	<div class="col-lg-9">
                                  	<input type="text" class="form-control" name="number_employees" placeholder="Type Number Employee">
                              	</div>
                          	</div>
                            <div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">IP Address:</label>
                              	<div class="col-lg-9">
                                  	<input type="text" class="form-control" name="ip_address" placeholder="Type IP Address">
                              	</div>
                          	</div>
                          	<hr>
                          	<div class="form-group text-center">
                          		<button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                          		<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                          	</div>
                      	</fieldset>
                  	</div>
              	</div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL ADD SEWING -->
@endsection

@section('js')
<script type="text/javascript" src="{{url('js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script type="text/javascript">
$( document ).ready(function() {
    //datatables
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var url = $('#sewing_get_data').attr('href');
    var table = $('#table_sewing').DataTable({
        ajax: url,
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false  },
            {data: 'name', name: 'name'},
            {data: 'description', name: 'description'},
            {data: 'prod_max_capacity', name: 'prod_max_capacity'},
            {data: 'number_employees', name: 'number_employees'},
            {data: 'ip_address', name: 'ip_address'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
    //end of datatables

    //add sewing
    $('.add_sewing').on('click',function(){
        $('#modal_add_new_sewing_').modal('show')
    });
    //end of add sewing

    //delete sewing
    $("#table_sewing").on("click", ".deleteSewing", function() {
        event.preventDefault();
        var id = $(this).data('sewingid');
        if(id == 'kosong') {
            return false;
        }
        var token = $(this).data("token");
        bootbox.confirm("Are you sure delete this row ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "master/delete-sewing/"+id,
                    type: "GET",
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": token,
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $(".loader-area").unblock();
                    },
                    success: function () {
                        myalert('success','Data has been deleted');
                        table.ajax.reload();
                    }
                });
            }
        });
    });
    //end of delete sewing

    //add new location
    $('#form-add-sewing').submit(function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-add-sewing').attr('action'),
            data: $('#form-add-sewing').serialize(),
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
            	$('.loader-area').unblock();
            },
            success: function(response) {
                myalert('success','GOOD');
                $('#form-add-sewing').trigger("reset");
                $('#modal_add_new_sewing_').trigger('toggle');
                table.ajax.reload();
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        })
    });
});

</script>
@endsection
