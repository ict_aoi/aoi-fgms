@extends('layouts.app', ['active' => 'mastersewing'])

@section('content')
<br>
<div class="breadcrumb-line breadcrumb-line-component">
    <ul class="breadcrumb">
        <li><a href="{{ route('sewing.master') }}"><i class="icon-home2 position-left"></i>Sewing</a></li>
    </ul>
</div>

<section class="panel">
	<div class="panel-body loader-area">
        <form action="{{ route('sewing.update') }}" id="main-form" method="POST" class="form-horizontal">
        {{ csrf_field() }}
            <input type="text" name="id" class="hidden" value="{{ $sewing->id }}" readonly>
            <div class="form-group">
                <label class="col-sm-2 control-label text-semibold text-uppercase">Name :</label>
                <div class="col-sm-5">
                    <input type="text" name="name" placeholder="Area Name" class="form-control" id="name" value="{{ $sewing->name }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-labe text-semibold text-uppercase">Description :</label>
                <div class="col-sm-5">
                    <input type="text" name="description" placeholder="Area Name" class="form-control" id="description" value="{{ $sewing->description }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-labe text-semibold text-uppercase">Prod Max Capacity :</label>
                <div class="col-sm-5">
                    <input type="text" name="prod_max_capacity" placeholder="Area Name" class="form-control" id="prod_max_capacity" value="{{ $sewing->prod_max_capacity }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-labe text-semibold text-uppercase">Number Employees :</label>
                <div class="col-sm-5">
                    <input type="text" name="number_employees" placeholder="Area Name" class="form-control" id="number_employees" value="{{ $sewing->number_employees }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-labe text-semibold text-uppercase">IP Address :</label>
                <div class="col-sm-5">
                    <input type="text" name="ip_address" placeholder="IP Address" class="form-control" id="ip_address" value="{{ $sewing->ip_address }}">
                </div>
            </div>
            <hr>
            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-6">
                    <div class="text-center loader-area">
                        <button type="button" class="btn btn-success save-data" name="update">SAVE <i class="icon-floppy-disk position-right"></i></button>
                        <a class="btn btn-default" href="javascript:history.back()">Close <i class="icon-reload-alt position-right"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
@endsection

@section('js')
<script type="text/javascript" src="{{url('js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function () {

    var fd = $("#main-form").serializeArray();
    var url = $("#main-form").attr("href");
    $(".save-data").on('click', function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#main-form').attr('action'),
            data: $('#main-form').serialize(),
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $('.loader-area').unblock();
            },
            success: function(response) {
                console.log(response);
                myalert('success','GOOD');
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        })
    });

});
</script>
@endsection
