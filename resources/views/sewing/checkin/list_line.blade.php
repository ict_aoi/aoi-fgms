<div class="table-responsive">
<table class="table" id="table-line">
    <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <!-- <th>Prod Max Capacity</th>
            <th>Employees Number</th> -->
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $key => $val)
            <tr>
                <td>{{ $val->name }}</td>
                <td>{{ $val->description }}</td>
                <td>
                    <button type="button" id="choose-line"
                            data-id="{{ $val->id }}"
                            data-name="{{ $val->name }}"
                            class="btn btn-default">
                            SELECT
                    </button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

</div>
<script type="text/javascript">
    var table = $('#table-line').DataTable({
        "lengthChange": false,
        "pageLength": 5
    });
</script>
