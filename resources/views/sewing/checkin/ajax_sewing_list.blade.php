<tr>
    <td>{{ $data['barcode_package'] }}</td>
    <td>{{ $plan_ref_number }}</td>
    <td>{{ $po_number }}</td>
    <td><span class="label label-flat border-grey text-grey-600">{{ $data['department_to'] }}</span></td>
    <td>{{ $linename }}</td>
    <td>
        @if($data['status_to'] == 'onprogress')
        <span class="label label-primary">ON PROGRESS</span>
        @elseif($data['status_to'] == 'rejected')
        <span class="label label-danger">REJECTED</span>
        @elseif($data['status_to'] == 'cancel')
        <span class="label label-default">CANCEL</span>
        @endif
    </td>
    <td>{{ $data['created_at'] }}</td>
</tr>
