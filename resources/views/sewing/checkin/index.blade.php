@extends('layouts.app', ['active' => 'sewingcheckin'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">SEWING CHECK IN</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('sewing.checkin') }}"><i class="icon-home4 position-left"></i>Sewing Check In</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <form action="{{ route('sewing.ajaxSetStatusCheckin') }}" id="form-checkin">
            @csrf
            <div class="row">
                <div class="col-md-10">
                    <input type="text" class="form-control input-new" id="sewing-checkin" name="sewingCheckin" placeholder="#Scan ID"></input>
                    <input type="hidden" id="_barcodeid"></input>
                    <input type="hidden" id="_status"></input>
                </div>
                <div class="col-md-2">
                    <input type="text" id="count_scan" name="count_scan" value="0" style="text-align: center; font-size: 28px;"
                           class="form-control" readonly="readonly"
                           placeholder="">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="onprogress">ON PROGRESS</label>
                        <label class="radio-inline"><input type="radio" name="radio_status" value="rejected">REJECTED</label>
                        <label class="radio-inline"><input type="radio" name="radio_status" value="cancel">CANCEL</label>
                    </div>
                </div>
                <div class="col-md-1 col-md-offset-5">
                    <input type="text" id="line" name="line"
                           class="form-control" readonly="readonly"
                           placeholder="#LINE">
                </div>
                <input type="hidden" id="line_id" name="line_id"></input>
            </div>
            <div class="row">

            </div>
        </form>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="show-result">
                <thead>
                    <tr>
                        <th>BARCODE ID</th>
                        <th>PLAN NUMBER</th>
                        <th>PO NUMBER</th>
                        <th>DEPARTMENT</th>
                        <th>LINE</th>
                        <th>STATUS</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('sewing.ajaxSetStatusCheckin') }}" id="set-status-checkin"></a>
<a href="{{ route('sewing.ajaxGetLine') }}" id="get-line"></a>
@endsection

@section('modal')
<div id="modal_line_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-md">
            <div class="panel-heading">
              <div class="pull-right">
                <button type="button" class="btn btn-small btn-default" data-dismiss="modal"><i class="fa fa-times" style="font-size:16px;"></i></button>
              </div>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <fieldset>
                        <legend class="text-semibold">
                            <h4 id="title-line-number"><i class="position-left"></i>Choose Line Number</h4>
                        </legend>

                        <div id="list-line" class="col-lg-12">
                            <!-- template pilihan -->
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#sewing-checkin').focus();

    //scan package
    $('.input-new').keypress(function(event) {
        if(event.which == 13) {
            event.preventDefault();
            var barcodeid = $('#sewing-checkin').val();
            var status = $('input[name=radio_status]:checked').val();

            $('#_barcodeid').val(barcodeid);
            $('#_status').val(status);

            var lineid = $('#line_id').val();
            var linename = $('#line').val();

            //check apakah search bar sudah diisi atau belum
            if($('#sewing-checkin').val() == '') {
                $('#sewing-checkin').val('');
                $('#sewing-checkin').focus();
                $('#_barcodeid').val('');
                $('#_status').val('');
                return false;
            }

            //check line number
            if(lineid == '') {
                $('#sewing-checkin').val('');
                $('#sewing-checkin').focus();
                $('#_barcodeid').val('');
                $('#_status').val('');
                myalert('error', 'Please Choose Line First!');
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: $('#set-status-checkin').attr('href'),
                data: {barcodeid: barcodeid, status: status, lineid: lineid, linename:linename},
                beforeSend: function() {
                    $('#sewing-checkin').val('');
                    $('#sewing-checkin').focus();
                    $('#_barcodeid').val('');
                    $('#_status').val('');
                },
                success: function(response){
                    $('#show-result > tbody').prepend(response);
                    var count_s = +$('#count_scan').val()+1;

                    $('#count_scan').val(count_s);
                },
                error: function(response){
                    if(response['status'] == 422) {
                        myalert('error', response['responseJSON']);
                    }
                    $('#sewing-checkin').val('');
                }
            });
        }
    });

    //choose line number
    $('#line').on('click',function(){
        $('#modal_line_').modal('show');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url: $('#get-line').attr('href'),
            beforeSend: function() {
                $('#modal_line_ > .modal-content').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response){
                $('#modal_line_ > .modal-content').unblock();
                $('#list-line').html(response);
            }
        });
    });

    //
    $('#list-line').on('click','#choose-line', function(){
        var id = $(this).data('id');
        var name = $(this).data('name');
        $('#line_id').val(id);
        $('#line').val(name);
        $('#modal_line_').modal('hide');
        $('.input-new').focus();
    })
});
</script>
@endsection
