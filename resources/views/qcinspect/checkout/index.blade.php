@extends('layouts.app', ['active' => 'qcinspectcompleted'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">QC INSPECT COMPLETED</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('qcinspect.checkout') }}"><i class="icon-home4 position-left"></i> QC Inspect Completed</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <form action="{{ route('qcinspect.ajaxSetStatusCheckout') }}" id="form-checkout">
            @csrf
            <div class="row">
                <div class="col-md-10">
                    <input type="text" class="form-control input-new" id="qcinspect-checkout" name="qcinspectCheckout" placeholder="#Scan ID"></input>
                    <input type="hidden" id="_barcodeid"></input>
                    <input type="hidden" id="_status"></input>
                </div>
                <div class="col-md-2">
                    <input type="text" id="count_scan" name="count_scan" value="0" style="text-align: center; font-size: 28px;"
                           class="form-control" readonly="readonly"
                           placeholder="">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="completed">RELEASED</label>
                        <label class="radio-inline"><input type="radio" name="radio_status" value="rejected">REJECTED</label>
                        <label class="radio-inline"><input type="radio" name="radio_status" value="cancel">CANCEL</label>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="show-result">
                <thead>
                    <tr>
                        <th>BARCODE ID</th>
                        <th>PO NUMBER</th>
                        <th>DEPARTMENT</th>
                        <th>STATUS</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('qcinspect.ajaxSetStatusCheckout') }}" id="set-status-checkout"></a>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#qcinspect-checkout').focus();

    //get barcode from scanner
    // function getBarcodeStatus(value, length) {
    //     var data = new Array();
    //     var scan_id = value;
    //     var scan_length = scan_id.length;
    //     var barcode_length = parseInt(scan_length) - parseInt(length);
    //     var barcodeid = '';
    //
    //     for(var i = barcode_length; i < scan_length; i++) {
    //         barcodeid += scan_id[i];
    //     }
    //
    //     if(scan_length <= length && scan_length > 0) {
    //         var status = 'completed';
    //     }
    //     else if(scan_length > length) {
    //         var current_status = '';
    //         var current_length = parseInt(scan_length) - parseInt(length);
    //         for(var i = length + 1; i < current_length; i++) {
    //             current_status += scan_id[i];
    //         }
    //
    //         if(current_status == 'completed') {
    //             var status = 'rejected';
    //         }
    //         else if(current_status == 'rejected') {
    //             var status = 'completed_all';
    //         }
    //         else if(current_status == 'completed_all') {
    //             var status = 'rejected_all';
    //         }
    //         else if(current_status == 'rejected_all') {
    //             var status = 'cancel';
    //         }
    //         else if(current_status == 'cancel') {
    //             var status = 'completed';
    //         }
    //         else {
    //             var status = current_status;
    //         }
    //         console.log(status);
    //     }
    //     else {
    //         $('#qcinspect-checkout').val('');
    //         return false;
    //     }
    //
    //     data[0] = barcodeid;
    //     data[1] = status;
    //     return data;
    // }

    //scan package
    $('.input-new').keypress(function(event) {
        if(event.which == 13) {
            event.preventDefault();
            var barcodeid = $('#qcinspect-checkout').val();
            var status = $('input[name=radio_status]:checked').val();

            $('#_barcodeid').val(barcodeid);
            $('#_status').val(status);

            //cancel
            if($('#qcinspect-checkout').val() == '') {
                $('#qcinspect-checkout').val('');
                $('#qcinspect-checkout').focus();
                $('#_barcodeid').val('');
                $('#_status').val('');
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: $('#set-status-checkout').attr('href'),
                data: {barcodeid: barcodeid, status: status},
                beforeSend: function() {
                    $('#qcinspect-checkout').val('');
                    $('#qcinspect-checkout').focus();
                    $('#_barcodeid').val('');
                    $('#_status').val('');
                },
                success: function(response){
                    $('#show-result > tbody').prepend(response);
                    var count_s = $('#show-result > tbody > tr').length;

                    $('#count_scan').val(count_s);
                },
                error: function(response){
                    if(response['status'] == 422) {
                        myalert('error',response['responseJSON']);
                    }
                    $('#qcinspect-checkout').val('');
                }
            });
        }
    });
});
</script>
@endsection
