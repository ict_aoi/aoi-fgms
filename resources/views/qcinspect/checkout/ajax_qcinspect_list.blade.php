@foreach($data as $key => $value)
<tr>
    <td>{{ $value['barcode_package'] }}</td>
    <td>{{ $po_number }}</td>
    <td><span class="label label-flat border-grey text-grey-600">{{ $value['department_to'] }}</span></td>
    <td>
        @if($value['status_to'] == 'rejected')
        <span class="label label-danger">REJECTED</span>
        @elseif($value['status_to'] == 'completed')
        <span class="label label-success">COMPLETED</span>
        @elseif($value['status_to'] == 'cancel')
        <span class="label label-success">CANCEL</span>
        @endif
    </td>
    <td>{{ $value['created_at'] }}</td>
</tr>
@endforeach
