<tr>
    <td>{{ $data['barcode_package'] }}</td>
    <td>{{ $po_number }}</td>
    <td><span class="label label-flat border-grey text-grey-600">{{ $data['department_to'] }}</span></td>
    <td>
        <span class="label label-primary">ON PROGRESS</span>
    </td>
    <!-- locator name -->
    <td>{{ $rak }}</td>
    <td>{{ $data['created_at'] }}</td>
</tr>
