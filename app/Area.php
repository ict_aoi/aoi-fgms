<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
	protected $table = 'areas';
	protected $fillable = ['id','name','user_id','warehouse','created_at','updated_at','deleted_at'];
    protected $guarded = ['id'];
    protected $dates = ['created_at','updated_at','deleted_at'];
}
