<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    protected $fillable = [
        'id','name', 'nik', 'email', 'password', 'warehouse', 'division', 'factory_id', 'factory_name', 'created_at', 'updated_at','deleted_at'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
