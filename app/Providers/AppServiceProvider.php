<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $user = \Auth::user();

        // $get_request_carton = DB::table('request_carton')
        //                 ->join('line', 'request_carton.line_id', '=', 'line.id')
        //                 ->where('request_carton.status_sewing', 'request')
        //                 // ->where('line.factory_id', $user->factory_id)
        //                 ->wherenull('request_carton.deleted_at')
        //                 ->wherenull('line.deleted_at')
        //                 ->count();

        view()->share(['total_request' => 0, 'user' => $user]);

        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
