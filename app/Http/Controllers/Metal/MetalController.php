<?php

namespace App\Http\Controllers\Metal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

class MetalController extends Controller
{
    // operator metal
    public function scanMetal(){
        return view('metal.scan.scan_metal');
    }

    public function ajaxScanMetal(Request $request){
        $barcodeid = trim($request->barcodeid);
        $cek_move = DB::table('package')->where('barcode_id',$barcodeid)->first();
        if(isset($cek_move) && $cek_move->is_move_to_fgms_new == true){
            return response()->json("Package sudah dipindah di FGMS BARU",422);

        }

        $cek = $this->cekPackage($barcodeid);

        
   
        if ($cek!=null && $cek->metal_status==false) {
           if ($cek->current_department=="sewing" && $cek->current_status="completed") {
               $datret = array(
                        'barcode_id'=>$cek->barcode_id,
                        'package_number'=>$cek->package_number,
                        'po_number'=>$cek->po_number,
                        'plan_ref_number'=>$cek->plan_ref_number,
                        'upc'=>$cek->upc,
                        'buyer_item'=>$cek->buyer_item,
                        'customer_size'=>$cek->customer_size,
                        'manufacturing_size'=>$cek->manufacturing_size,
                        'inner_pack'=>$cek->inner_pack,
                        'plan_stuffing'=>$cek->plan_stuffing,
                        'line_id'=>$cek->id,
                        'line_name'=>$cek->description,
                        'product_category_detail'=>$cek->product_category_detail,
                        'factory_id'=>Auth::user()->factory_id,
                        'created_at'=>carbon::now(),
                        'status'=>'checkin',
                        'operator'=>auth::user()->id,
                        'updated_at'=>Carbon::now()
                    );

               try {
                DB::begintransaction();
                   DB::table('metal_finding')->insert($datret);
                   DB::table('package')->where('barcode_id',$cek->barcode_id)->update(['metal_status'=>true,'updated_at'=>Carbon::now()]);
                DB::commit();
               } catch (Exception $ex) {
                   db::rollback();
                    $message = $ex->getMessage();
                    ErrorHandler::db($message);
               }

               return view('metal.scan._ajax_list')->with('data',$datret);
                
            }else{
                return response()->json("Package location ".$cek->current_department." status ".$cek->current_status." !!!",422);
            } 
        }else if ($cek!=null && $cek->metal_status==false) {
            return response()->json("Metal Status Is Quarantine ! ! !",422);
        }else{
            return response()->json("Package Not Found ! ! !",422);
        }

        
    }

    private function cekPackage($barcodeid){
        $data = DB::table('po_summary')
                    ->join('package_detail',function($join){
                        $join->on('po_summary.po_number','=','package_detail.po_number');
                        $join->on('po_summary.plan_ref_number','=','package_detail.plan_ref_number');
                    })
                    ->join('package','package_detail.scan_id','=','package.scan_id')
                    ->join('package_movements','package.barcode_id','=','package_movements.barcode_package')
                    ->leftjoin('po_stuffing',function($join){
                        $join->on('po_summary.po_number','=','po_stuffing.po_number');
                        $join->on('po_summary.plan_ref_number','=','po_stuffing.plan_ref_number');
                    })
                    ->leftjoin('line','package_movements.line_id','=','line.id')
                    ->leftjoin('po_master_sync','po_summary.po_number','=','po_master_sync.po_number')
                    ->whereNull('po_summary.deleted_at')
                    ->whereNull('package_detail.deleted_at')
                    ->whereNull('package.deleted_at')
                    ->whereNull('package_movements.deleted_at')
                    ->whereNull('po_stuffing.deleted_at')
                    ->where('package_movements.is_canceled',false)
                    ->where('package.barcode_id',$barcodeid)
                    ->where('po_summary.factory_id',auth::user()->factory_id)
                    ->orderBy('package_movements.created_at','desc')
                    ->select('package.current_department','package.current_status','package.metal_status','package_movements.department_from','package_movements.department_to','package_movements.status_to','package_movements.status_from','po_summary.po_number','po_summary.plan_ref_number','po_summary.upc','package_detail.buyer_item','package_detail.manufacturing_size','package_detail.customer_size','package_detail.inner_pack','package.barcode_id','package.package_number','po_stuffing.plan_stuffing','line.id','line.description','po_master_sync.product_category_detail')->first();

        return $data;
    }

    public function listScanMetal(){
        return view('metal.list_metal');
    }

    public function ajaxListScanMetal(){
        $data = DB::table('metal_finding')->wherenull('deleted_at')->where('factory_id',Auth::user()->factory_id)->whereIn('status',['checkin','metal found'])->orderby('created_at','desc');

        return Datatables::of($data)
                            ->editColumn('pso',function($data){
                                if (isset($data->pso_verify)) {
                                    $upso = DB::table('users')->where('id',$data->pso_verify)->first();

                                    return $upso->name;
                                }else{
                                    return "-";
                                }
                            })
                            ->setRowAttr([
                                'style' => function($data)
                                {
                                    if ($data->status=='checkin') {
                                        return  'background-color: #e6b800;';
                                    }else if ($data->status=='metal found'){
                                         return  'background-color: #e62e00;';
                                    }
                                    
                                    
                                },
                            ])
                            ->rawColumns(['pso'])->make(true);
    }

    // operator metal

    // pso metal
    public function psoVerify(){
        return view('metal.verification_pso');
    }

    public function ajaxListVerify(){
        $data = DB::table('metal_finding')->wherenull('deleted_at')->where('factory_id',Auth::user()->factory_id)->whereIn('status',['checkin','metal found'])->orderby('created_at','desc');

        return Datatables::of($data)
                            ->editColumn('created_at',function($data){
                                return date_format(date_create($data->created_at),'d-M-Y H:i:s');
                            })
                            ->editColumn('pso',function($data){
                                if (isset($data->pso_verify)) {
                                    $upso = DB::table('users')->where('id',$data->pso_verify)->first();

                                    return $upso->name;
                                }else{
                                    return "-";
                                }
                            })
                            ->editColumn('status',function($data){
                                return '<button class="btn btn-link btn-pop" id="btn-pop" data-id="'.$data->barcode_id.'">'.strtoupper($data->status).'</button>';
                            })
                            ->setRowAttr([
                                'style' => function($data)
                                {
                                    if ($data->status=='checkin') {
                                        return  'background-color: #e6b800;';
                                    }else if ($data->status=='metal found'){
                                         return  'background-color: #e62e00;';
                                    }
                                    
                                    
                                },
                            ])
                            ->rawColumns(['pso','status','created_at'])->make(true);
    }

    public function getMetFind(Request $request){
        $barcode_id = $request->barcode_id;

        $get = DB::table('metal_finding')
                    ->leftjoin('users','metal_finding.pso_verify','=','users.id')
                    ->wherenull('metal_finding.deleted_at')
                    ->where('metal_finding.barcode_id',$barcode_id)
                    ->first();


        $data = array(
                'barcode_id'=>$get->barcode_id,
                'po_number'=>$get->po_number,
                'upc'=>$get->upc,
                'buyer_item'=>$get->buyer_item,
                'product_category_detail'=>$get->product_category_detail,
                'line_name'=>$get->line_name,
                'customer_size'=>$get->customer_size,
                'manufacturing_size'=>$get->manufacturing_size,
                'inner_pack'=>$get->inner_pack,
                'package_number'=>$get->package_number,
                'plan_stuffing'=>$get->plan_stuffing,
                'created_at'=>date_format(date_create($get->created_at),'d-M-Y H:i:s'),
                'pso_verify'=>$get->name,
                'status'=>$get->status,
                'remark'=>$get->remark,
                'note'=>$get->note,
                'loct'=>$get->loct_defect,
                'uccno'=>$get->uccno
            );



        return response()->json(['data'=>$data],200);
    }


    public function changeStatus(Request $request){
        $barcode_id = trim($request->barcode_id);
        $status = trim($request->cekstatus);
        $data = $request->data;
        $no_ucc = trim($request->no_ucc);

        // dd($note);
        $cek = DB::table('metal_finding')->where('barcode_id',$barcode_id)->whereNull('deleted_at')->first();
        $cekdet = DB::table('detail_metal_finding')->where('barcode_id',$barcode_id)->whereNull('deleted_at')->count();


        try {
           DB::begintransaction();

                

                if ($status=='metal found') {
                    $up = array(
                                'status'=>$status,
                                'pso_verify'=>auth::user()->id,
                                'verify_date'=>carbon::now(),
                                'updated_at'=>Carbon::now(),
                                'uccno'=>$no_ucc
                            );
                    DB::table('metal_finding')->where('barcode_id',$barcode_id)->update($up);

                    if ($cekdet==0) {
                        $this->insertDetail($data,$barcode_id);
                    }
                }else if ($status=='pass') {
                    $up = array(
                                'status'=>$status,
                                'pso_release'=>auth::user()->id,
                                'release_date'=>carbon::now(),
                                'pso_verify'=>isset($cek->pso_verify) ? $cek->pso_verify : auth::user()->id,
                                'verify_date'=>isset($cek->verify_date) ? $cek->verify_date : carbon::now(),
                                'updated_at'=>Carbon::now(),
                                'uccno'=>$no_ucc
                            );
                    DB::table('metal_finding')->where('barcode_id',$barcode_id)->update($up);
                    DB::table('package')->where('barcode_id',$barcode_id)->update(['metal_status'=>false]);
                       if ($cekdet==0) {
                            $this->insertDetail($data,$barcode_id);
                        }
                }else{
                     throw new \Exception('Status barcode is '.strtoupper($cek->status).' cannot update ! ! !');
                }

                $data_response = [
                            'status' => 200,
                            'output' => "Change status success ! ! !"
                          ]; 
           DB::commit();
           
        } catch (Exception $ex) {
             DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Change status field ! ! !'
                          ];
        }

        return response()->json(['data'=>$data_response]);
    }

    private function insertDetail($arrs,$barcodeid){


        try {
            DB::begintransaction();
                foreach ($arrs as $dt) {
                    $ins = array(
                        'barcode_id'=>$barcodeid,
                        'metfind'=>$dt['metfind'],
                        'note'=>$dt['note'],
                        'loct'=>$dt['loct'],
                        'qty'=>$dt['qty'],
                        'created_at'=>carbon::now()
                    );

                    DB::table('detail_metal_finding')->insert($ins);
                }
            DB::commit();
        } catch (Exception $ex) {
             DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
    }
    // pso metal
}
