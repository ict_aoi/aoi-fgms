<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use DB;
use DataTables;
use Carbon\Carbon;
use Excel;
use Rap2hpoutre\FastExcel\FastExcel;

class RekonController extends Controller
{
    public function __construct(){
        ini_set('max_execution_time', 1800);
    }
    
	// inventory start


    public function inventoryIn(){
    	return view('report/rekon/inventory');
    }

    public function getDataInventory(Request $req){
        DB::statement('REFRESH MATERIALIZED VIEW ns_rekon_inv');

    	$date_range = explode('-', preg_replace('/\s+/', '', $req->date));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('ns_rekon_inv')
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkin', [$range['from'], $range['to']]);
                });
        return DataTables::of($data)
        					->editColumn('location',function($data){
        						if ($data->current_department=='inventory'&&$data->current_status=='onprogress') {
        							return "METAL DETECTOR";
        						}else if ($data->current_department=='inventory'&&$data->current_status=='completed') {
        							return "RACK";
        						}else if ($data->current_department=='qcinspect') {
        							return "QC INSPECT";
        						}else if ($data->current_department=='shipping'&&$data->current_status=='onprogress') {
        							return "LOADING";
        						}else if ($data->current_department=='shipping'&&$data->current_status=='completed') {
        							return "SHIPMENT";
        						}else if ($data->current_department=='preparation') {
        							return "BACK TO SEWING";
        						}else if ($data->current_department=='sewing') {
        							return "BACK TO SEWING REFILLING";
        						}
        					})
        					->make(true);
    }

    public function exportInventory(Request $request){
        DB::statement('REFRESH MATERIALIZED VIEW ns_rekon_inv');
    	$date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );
        $current_date = Carbon::now()->format('Y-m-d h:i:s');
        $filterby = $request->filterby;

        $data = DB::table('ns_rekon_inv')
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkin', [$range['from'], $range['to']]);
                });

        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('brand', 'like', '%'.$filterby.'%');
                    });
        }

        if($request->orderby != 'undefined') {
            $data = $data->orderBy($request->orderby, $request->direction);
        }

       

        $filename = "Rekon_Inventory_".$range['from']."_until_".$range['to']."_orderBy_".$request->orderby."_direction_".$request->direction."_filterby_".$request->filterby;
        $i = 1;
        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }
        
        if ($data_result->count()>0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use ($i,$current_date){
                     if ($row->current_department=='inventory'&&$row->current_status=='onprogress') {
                     $locat = "METAL DETECTOR";
                 }else if ($row->current_department=='inventory'&&$row->current_status=='completed') {
                     $locat = "RACK";
                 }else if ($row->current_department=='qcinspect') {
                     $locat = "QC INSPECT";
                 }else if ($row->current_department=='shipping'&&$row->current_status=='onprogress') {
                     $locat = "LOADING";
                 }else if ($row->current_department=='shipping'&&$row->current_status=='completed') {
                     $locat = "SHIPMENT";
                 }else if ($row->current_department=='preparation') {
                     $locat = "BACK TO SEWING";
                 }else if ($row->current_department=='sewing') {
                     $locat = "BACK TO SEWING REFILLING";
                 }

                 return
                 [
                    '#'=>$row->no ,
                    'Barcode'=>$row->barcode_id ,
                    'Style'=>$row->upc ,
                    'Article'=>$row->buyer_item ,
                    'PO Number'=>$row->po_number ,
                    'Plan Ref'=>$row->plan_ref_number ,
                    'Brand'=>$row->brand ,
                    'Product Ctg.'=>$row->product_category_detail ,
                    'Customer Size'=>$row->customer_size ,
                    'Manufacturing Size'=>$row->manufacturing_size ,
                    'Qty Pack'=>$row->qty_pack ,
                    'Location'=>$locat ,
                    'CheckIn'=>$row->checkin ,
                    'Item Code'=>$row->item_code ,
                    'Factory'=>$row->factory_id ,
                    'Qty Ratio'=>$row->inner_pack ,
                    'Date Balance'=>$current_date
                 ];

            });
        }else{
            return response()->json('no data', 422);
        }

   //      return Excel::create($filename,function($excel) use ($data){
   //      	$excel->sheet('inventory',function($sheet) use ($data){
   //      		$sheet->setCellValue('A1','#');
			// 	$sheet->setCellValue('B1','Barcode');
			// 	$sheet->setCellValue('C1','Style');
			// 	$sheet->setCellValue('D1','Article');
			// 	$sheet->setCellValue('E1','PO Number');
			// 	$sheet->setCellValue('F1','Plan Ref');
			// 	$sheet->setCellValue('G1','Brand');
			// 	$sheet->setCellValue('H1','Product Ctg.');
			// 	$sheet->setCellValue('I1','Customer Size');
			// 	$sheet->setCellValue('J1','Manufacturing Size');
			// 	$sheet->setCellValue('K1','Qty Pack');
			// 	$sheet->setCellValue('L1','Location');
			// 	$sheet->setCellValue('M1','Check In');
			// 	$sheet->setCellValue('N1','Item Code');
			// 	$sheet->setCellValue('O1','Factory');
			// 	$sheet->setCellValue('P1','Qty Ratio');
			// 	$sheet->setCellValue('Q1','Date Balance');

			// 	$i =1;

			// 	foreach ($data as $key => $lv) {
			// 		$rw = $i+1;

			// 		if ($lv->current_department=='inventory'&&$lv->current_status=='onprogress') {
			// 			$locat = "METAL DETECTOR";
			// 		}else if ($lv->current_department=='inventory'&&$lv->current_status=='completed') {
			// 			$locat = "RACK";
			// 		}else if ($lv->current_department=='qcinspect') {
			// 			$locat = "QC INSPECT";
			// 		}else if ($lv->current_department=='shipping'&&$lv->current_status=='onprogress') {
			// 			$locat = "LOADING";
			// 		}else if ($lv->current_department=='shipping'&&$lv->current_status=='completed') {
			// 			$locat = "SHIPMENT";
			// 		}else if ($lv->current_department=='preparation') {
			// 			$locat = "BACK TO SEWING";
			// 		}else if ($lv->current_department=='sewing') {
			// 			$locat = "BACK TO SEWING REFILLING";
			// 		}

			// 		$sheet->setCellValue('A'.$rw,$i);
			// 		$sheet->setCellValue('B'.$rw,'="'.$lv->barcode_id.'"');
			// 		$sheet->setCellValue('C'.$rw,$lv->upc);
			// 		$sheet->setCellValue('D'.$rw,$lv->buyer_item);
			// 		$sheet->setCellValue('E'.$rw,'="'.$lv->po_number.'"');
			// 		$sheet->setCellValue('F'.$rw,'="'.$lv->plan_ref_number.'"');
			// 		$sheet->setCellValue('G'.$rw,$lv->brand);
			// 		$sheet->setCellValue('H'.$rw,$lv->product_category_detail);
			// 		$sheet->setCellValue('I'.$rw,$lv->customer_size);
			// 		$sheet->setCellValue('J'.$rw,$lv->manufacturing_size);
			// 		$sheet->setCellValue('K'.$rw,$lv->qty_pack);
			// 		$sheet->setCellValue('L'.$rw,$locat);
			// 		$sheet->setCellValue('M'.$rw,$lv->checkin);
			// 		$sheet->setCellValue('N'.$rw,$lv->item_code);
			// 		$sheet->setCellValue('O'.$rw,$lv->factory_id);
			// 		$sheet->setCellValue('P'.$rw,$lv->inner_pack);
			// 		$sheet->setCellValue('Q'.$rw,Carbon::now()->format('Y-m-d h:i:s'));

			// 		$i++;
			// 	}

   //      	});
			// $excel->setActiveSheetIndex(0);
   //      })->export('xlsx');
    } 

    // inventory end

    //shipment start
    public function shipmentOut(){
    	return view('report/rekon/shipment');
    }

    public function getDataShipment(Request $req){
        DB::statement('REFRESH MATERIALIZED VIEW ns_rekon_ship');
        $date_range = explode('-', preg_replace('/\s+/', '', $req->date));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('ns_rekon_ship')
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkout', [$range['from'], $range['to']]);
                });
        return DataTables::of($data)
                            ->editColumn('location',function($data){
                                if ($data->current_department=='inventory'&&$data->current_status=='onprogress') {
                                    return "METAL DETECTOR";
                                }else if ($data->current_department=='inventory'&&$data->current_status=='completed') {
                                    return "RACK";
                                }else if ($data->current_department=='qcinspect') {
                                    return "QC INSPECT";
                                }else if ($data->current_department=='shipping'&&$data->current_status=='onprogress') {
                                    return "LOADING";
                                }else if ($data->current_department=='shipping'&&$data->current_status=='completed') {
                                    return "SHIPMENT";
                                }else if ($data->current_department=='preparation') {
                                    return "BACK TO SEWING";
                                }else if ($data->current_department=='sewing') {
                                    return "BACK TO SEWING REFILLING";
                                }
                            })
                            ->make(true);
    }

    public function exportShipment(Request $request){
        DB::statement('REFRESH MATERIALIZED VIEW ns_rekon_ship');
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );
        $current_date = Carbon::now()->format('Y-m-d h:i:s');
        $filterby = $request->filterby;
      
        $data = DB::table('ns_rekon_ship')
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkout', [$range['from'], $range['to']]);
                });
 
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('brand', 'like', '%'.$filterby.'%');
                    });
        }

        if($request->orderby != 'undefined') {
            $data = $data->orderBy($request->orderby, $request->direction);
        }

        // $data = $data->get();

        $filename = "Rekon_Shipment_".$range['from']."_until_".$range['to']."_orderBy_".$request->orderby."_direction_".$request->direction."_filterby_".$request->filterby;
        

        $i = 1;
        $data_result = $data->get();

        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }
        
        if ($data_result->count()>0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use ($i,$current_date){
                     if ($row->current_department=='inventory'&&$row->current_status=='onprogress') {
                     $locat = "METAL DETECTOR";
                 }else if ($row->current_department=='inventory'&&$row->current_status=='completed') {
                     $locat = "RACK";
                 }else if ($row->current_department=='qcinspect') {
                     $locat = "QC INSPECT";
                 }else if ($row->current_department=='shipping'&&$row->current_status=='onprogress') {
                     $locat = "LOADING";
                 }else if ($row->current_department=='shipping'&&$row->current_status=='completed') {
                     $locat = "SHIPMENT";
                 }else if ($row->current_department=='preparation') {
                     $locat = "BACK TO SEWING";
                 }else if ($row->current_department=='sewing') {
                     $locat = "BACK TO SEWING REFILLING";
                 }

                 return
                 [
                    '#'=>$row->no ,
                    'Barcode'=>$row->barcode_id ,
                    'Style'=>$row->upc ,
                    'Article'=>$row->buyer_item ,
                    'PO Number'=>$row->po_number ,
                    'Plan Ref'=>$row->plan_ref_number ,
                    'Brand'=>$row->brand ,
                    'Product Ctg.'=>$row->product_category_detail ,
                    'Customer Size'=>$row->customer_size ,
                    'Manufacturing Size'=>$row->manufacturing_size ,
                    'Qty Pack'=>$row->qty_pack ,
                    'Location'=>$locat ,
                    'CheckOut'=>$row->checkout ,
                    'Item Code'=>$row->item_code ,
                    'Factory'=>$row->factory_id ,
                    'Qty Ratio'=>$row->inner_pack ,
                    'Date Balance'=>$current_date
                 ];

            });
        }else{
            return response()->json('no data', 422);
        }

       
    }

    //shipment end

    // back to sewing start
    public function backToSewing(){
        return view('report/rekon/backtosewing');
    }

    public function getDataBackToSewing(Request $req){
        DB::statement('REFRESH MATERIALIZED VIEW ns_rekon_backsewing');
        $date_range = explode('-', preg_replace('/\s+/', '', $req->date));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('ns_rekon_backsewing')
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkout', [$range['from'], $range['to']]);
                });
        return DataTables::of($data)
                ->editColumn('location',function($data){
                    if ($data->current_department=='inventory'&&$data->current_status=='onprogress') {
                        return "METAL DETECTOR";
                    }else if ($data->current_department=='inventory'&&$data->current_status=='completed') {
                        return "RACK";
                    }else if ($data->current_department=='qcinspect') {
                        return "QC INSPECT";
                    }else if ($data->current_department=='shipping'&&$data->current_status=='onprogress') {
                        return "LOADING";
                    }else if ($data->current_department=='shipping'&&$data->current_status=='completed') {
                        return "SHIPMENT";
                    }else if ($data->current_department=='preparation') {
                        return "BACK TO SEWING";
                    }else if ($data->current_department=='sewing') {
                        return "BACK TO SEWING REFILLING";
                    }
                })
                ->make(true);
    }

    public function exportBackToSewing(Request $request){
        DB::statement('REFRESH MATERIALIZED VIEW ns_rekon_backsewing');
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );
        $filterby = $request->filterby;

        $data = DB::table('ns_rekon_backsewing')
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkout', [$range['from'], $range['to']]);
                });

        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('brand', 'like', '%'.$filterby.'%');
                    });
        }

        if($request->orderby != 'undefined') {
            $data = $data->orderBy($request->orderby, $request->direction);
        }

        $data = $data->get();

        $filename = "Rekon_BackToSewing_".$range['from']."_until_".$range['to']."_orderBy_".$request->orderby."_direction_".$request->direction."_filterby_".$request->filterby;
        

        return Excel::create($filename,function($excel) use ($data){
            $excel->sheet('backtosewing',function($sheet) use ($data){
                $sheet->setCellValue('A1','#');
                $sheet->setCellValue('B1','Barcode');
                $sheet->setCellValue('C1','Style');
                $sheet->setCellValue('D1','Article');
                $sheet->setCellValue('E1','PO Number');
                $sheet->setCellValue('F1','Plan Ref');
                $sheet->setCellValue('G1','Brand');
                $sheet->setCellValue('H1','Product Ctg.');
                $sheet->setCellValue('I1','Customer Size');
                $sheet->setCellValue('J1','Manufacturing Size');
                $sheet->setCellValue('K1','Qty Pack');
                $sheet->setCellValue('L1','Location');
                $sheet->setCellValue('M1','Check Out');
                $sheet->setCellValue('N1','Item Code');
                $sheet->setCellValue('O1','Factory');
                $sheet->setCellValue('P1','Qty Ratio');
                $sheet->setCellValue('Q1','Date Balance');

                $i =1;

                foreach ($data as $key => $lv) {
                    $rw = $i+1;

                    if ($lv->current_department=='inventory'&&$lv->current_status=='onprogress') {
                        $locat = "METAL DETECTOR";
                    }else if ($lv->current_department=='inventory'&&$lv->current_status=='completed') {
                        $locat = "RACK";
                    }else if ($lv->current_department=='qcinspect') {
                        $locat = "QC INSPECT";
                    }else if ($lv->current_department=='shipping'&&$lv->current_status=='onprogress') {
                        $locat = "LOADING";
                    }else if ($lv->current_department=='shipping'&&$lv->current_status=='completed') {
                        $locat = "SHIPMENT";
                    }else if ($lv->current_department=='preparation') {
                        $locat = "BACK TO SEWING";
                    }else if ($lv->current_department=='sewing') {
                        $locat = "BACK TO SEWING REFILLING";
                    }

                    $sheet->setCellValue('A'.$rw,$i);
                    $sheet->setCellValue('B'.$rw,'="'.$lv->barcode_id.'"');
                    $sheet->setCellValue('C'.$rw,$lv->upc);
                    $sheet->setCellValue('D'.$rw,$lv->buyer_item);
                    $sheet->setCellValue('E'.$rw,'="'.$lv->po_number.'"');
                    $sheet->setCellValue('F'.$rw,'="'.$lv->plan_ref_number.'"');
                    $sheet->setCellValue('G'.$rw,$lv->brand);
                    $sheet->setCellValue('H'.$rw,$lv->product_category_detail);
                    $sheet->setCellValue('I'.$rw,$lv->customer_size);
                    $sheet->setCellValue('J'.$rw,$lv->manufacturing_size);
                    $sheet->setCellValue('K'.$rw,$lv->qty_pack);
                    $sheet->setCellValue('L'.$rw,$locat);
                    $sheet->setCellValue('M'.$rw,$lv->checkout);
                    $sheet->setCellValue('N'.$rw,$lv->item_code);
                    $sheet->setCellValue('O'.$rw,$lv->factory_id);
                    $sheet->setCellValue('P'.$rw,$lv->inner_pack);
                    $sheet->setCellValue('Q'.$rw,Carbon::now()->format('Y-m-d h:i:s'));

                    $i++;
                }

            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }

    // back to sewing end

    // start balance
    public function balanceStock(){
        return view('report/rekon/balance');
    }

    public function getDataBalanceStock(Request $req){
        DB::statement('REFRESH MATERIALIZED VIEW ns_rekon_balance');
        $date = Carbon::parse($req->date)->format('Y-m-d');
        if (!empty(trim($req->date))) {
           $data = DB::table('ns_rekon_balance')
                ->where('date_balance',$date);
        }else{
            $yes = Carbon::now()->subDay(1)->format('Y-m-d');
            $data = DB::table('v_ns_rekon_balance_new')
                ->where('date_balance',$yes);
        }
        
        return DataTables::of($data)
                ->make(true);
    }

    public function exportBalanceStock(Request $request){
        DB::statement('REFRESH MATERIALIZED VIEW ns_rekon_balance');
        $date = trim($request->date_range);
    
        $data = DB::table('ns_rekon_balance');
        $filterby = $request->filterby;
        
        if (!empty($date)) {
            $data = $data->where('date_balance',$date);
        }else{
            $yes = Carbon::now()->subDay(1)->format('Y-m-d');
            $data = $data->where('date_balance',$yes);
        }

        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('brand', 'like', '%'.$filterby.'%');
                    });
        }

        if($request->orderby != 'undefined') {
            $data = $data->orderBy($request->orderby, $request->direction);
        }


        $filename = "Rekon_BalanceStock_".$date."_orderBy_".$request->orderby."_direction_".$request->direction."_filterby_".$request->filterby;

        $i=1;
        $data_result = $data->get();
        $current_date =Carbon::now()->format('Y-m-d h:i:s');

        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }

        if ($data_result->count()>0) {
            return(new FastExcel($data_result))->download($filename.'.xlsx',function ($row) use ($i,$current_date){

                return [
                    '#'=>$row->no,
                    'Barcode'=>$row->barcode_id,
                    'Style'=>$row->style,
                    'Article'=>$row->buyer_item,
                    'PO Number'=>$row->po_number,
                    'Plan Ref'=>$row->plan_ref_number,
                    'Brand'=>$row->trademark,
                    'Product Ctg.'=>$row->product_category_detail,
                    'Customer Size'=>$row->customer_size,
                    'Manufacturing Size'=>$row->manufacturing_size,
                    'Qty Pack'=>$row->qty_pack,
                    'Loc. Balance'=>$row->balance_location,
                    'Check In'=>$row->checkin,
                    'Item Code'=>$row->item_code,
                    'Factory'=>$row->factory_id,
                    'Qty Ratio'=>$row->qty_ratio,
                    'Loc. Status'=>$row->status_location,
                    'Date Balance'=>$row->date_balance
                ];
            });
        }else{
            return response()->json('no data', 422);
        }
        
    }

    // balance end

    // start delete pl 

    public function deletePackingList(){
        return view('report/rekon/delete_packinglist');
    }

    public function getDataDeletePL(Request $req){
        DB::statement('REFRESH MATERIALIZED VIEW ns_rekon_delete');
        $date_range = explode('-', preg_replace('/\s+/', '', $req->date));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('ns_rekon_delete')
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkout', [$range['from'], $range['to']]);
                });
        return DataTables::of($data)
                ->editColumn('location',function($data){
                    if ($data->current_department=='inventory'&&$data->current_status=='onprogress') {
                        return "METAL DETECTOR";
                    }else if ($data->current_department=='inventory'&&$data->current_status=='completed') {
                        return "RACK";
                    }else if ($data->current_department=='qcinspect') {
                        return "QC INSPECT";
                    }else if ($data->current_department=='shipping'&&$data->current_status=='onprogress') {
                        return "LOADING";
                    }else if ($data->current_department=='shipping'&&$data->current_status=='completed') {
                        return "SHIPMENT";
                    }else if ($data->current_department=='preparation') {
                        return "BACK TO SEWING";
                    }else if ($data->current_department=='sewing') {
                        return "BACK TO SEWING REFILLING";
                    }
                })
                ->make(true);
    }

    public function exportDeletePL(Request $request){
        DB::statement('REFRESH MATERIALIZED VIEW ns_rekon_delete');
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );
        $filterby = $request->filterby;

        $data = DB::table('ns_rekon_delete')
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkout', [$range['from'], $range['to']]);
                });

        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('brand', 'like', '%'.$filterby.'%');
                    });
        }

        if($request->orderby != 'undefined') {
            $data = $data->orderBy($request->orderby, $request->direction);
        }

        $data = $data->get();

        $filename = "Rekon_Delete_PL_".$range['from']."_until_".$range['to']."_orderBy_".$request->orderby."_direction_".$request->direction."_filterby_".$request->filterby;
        

        return Excel::create($filename,function($excel) use ($data){
            $excel->sheet('backtosewing',function($sheet) use ($data){
                $sheet->setCellValue('A1','#');
                $sheet->setCellValue('B1','Barcode');
                $sheet->setCellValue('C1','Style');
                $sheet->setCellValue('D1','Article');
                $sheet->setCellValue('E1','PO Number');
                $sheet->setCellValue('F1','Plan Ref');
                $sheet->setCellValue('G1','Brand');
                $sheet->setCellValue('H1','Product Ctg.');
                $sheet->setCellValue('I1','Customer Size');
                $sheet->setCellValue('J1','Manufacturing Size');
                $sheet->setCellValue('K1','Qty Pack');
                $sheet->setCellValue('L1','Location');
                $sheet->setCellValue('M1','Check Out');
                $sheet->setCellValue('N1','Item Code');
                $sheet->setCellValue('O1','Factory');
                $sheet->setCellValue('P1','Qty Ratio');
                $sheet->setCellValue('Q1','Date Balance');

                $i =1;

                foreach ($data as $key => $lv) {
                    $rw = $i+1;

                    if ($lv->current_department=='inventory'&&$lv->current_status=='onprogress') {
                        $locat = "METAL DETECTOR";
                    }else if ($lv->current_department=='inventory'&&$lv->current_status=='completed') {
                        $locat = "RACK";
                    }else if ($lv->current_department=='qcinspect') {
                        $locat = "QC INSPECT";
                    }else if ($lv->current_department=='shipping'&&$lv->current_status=='onprogress') {
                        $locat = "LOADING";
                    }else if ($lv->current_department=='shipping'&&$lv->current_status=='completed') {
                        $locat = "SHIPMENT";
                    }else if ($lv->current_department=='preparation') {
                        $locat = "BACK TO SEWING";
                    }else if ($lv->current_department=='sewing') {
                        $locat = "BACK TO SEWING REFILLING";
                    }

                    $sheet->setCellValue('A'.$rw,$i);
                    $sheet->setCellValue('B'.$rw,'="'.$lv->barcode_id.'"');
                    $sheet->setCellValue('C'.$rw,$lv->upc);
                    $sheet->setCellValue('D'.$rw,$lv->buyer_item);
                    $sheet->setCellValue('E'.$rw,'="'.$lv->po_number.'"');
                    $sheet->setCellValue('F'.$rw,'="'.$lv->plan_ref_number.'"');
                    $sheet->setCellValue('G'.$rw,$lv->brand);
                    $sheet->setCellValue('H'.$rw,$lv->product_category_detail);
                    $sheet->setCellValue('I'.$rw,$lv->customer_size);
                    $sheet->setCellValue('J'.$rw,$lv->manufacturing_size);
                    $sheet->setCellValue('K'.$rw,$lv->qty_pack);
                    $sheet->setCellValue('L'.$rw,$locat);
                    $sheet->setCellValue('M'.$rw,$lv->checkout);
                    $sheet->setCellValue('N'.$rw,$lv->item_code);
                    $sheet->setCellValue('O'.$rw,$lv->factory_id);
                    $sheet->setCellValue('P'.$rw,$lv->inner_pack);
                    $sheet->setCellValue('Q'.$rw,Carbon::now()->format('Y-m-d h:i:s'));

                    $i++;
                }

            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }

    public function rekonAll(){
        return view('report/rekon/rekon_all');
    }

    public function rekonInv(){
        try {
          $path = storage_path('exports/Rekon_Inventory.xlsx');
          return response()->download($path);
        } catch (Exception $e) {
          $e->getMessage();
        }
    }

    public function rekonShip(){
        try {
          $path = storage_path('exports/Rekon_Shipment.xlsx');
          return response()->download($path);
        } catch (Exception $e) {
          $e->getMessage();
        }
    }

    public function rekonBts(){
        try {
          $path = storage_path('exports/Rekon_Backtosewing.xlsx');
          return response()->download($path);
        } catch (Exception $e) {
          $e->getMessage();
        }
    }

    public function rekonBalc(){
        try {
          $path = storage_path('exports/Rekon_Balancestock.xlsx');
          return response()->download($path);
        } catch (Exception $e) {
          $e->getMessage();
        }
    }

    public function rekonDelpl(){
        try {
          $path = storage_path('exports/Rekon_Delete_PL.xlsx');
          return response()->download($path);
        } catch (Exception $e) {
          $e->getMessage();
        }
    }

}
