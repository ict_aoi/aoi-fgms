<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

use Rap2hpoutre\FastExcel\FastExcel;

class BapbController extends Controller
{
    public function index(){
    	 $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();
    	return view('report/bapb/index')->with('factory',$factory);
    }

    public function getDataBapb(Request $request){
    	$factory_id=$request->factory_id;
    	$filterby = $request->filterby;
    	$date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d')
        );
    
    	 $data = DB::table('v_ns_bapb')
                ->where('factory_id', $factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkout', [$range['from'], $range['to']]);
                });

        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_size', 'like', '%'.$filterby.'%');
                                // ->orWhere('location', 'like', '%'.$filterby.'%');
                    });
        }

        return DataTables::of($data)
        				->make(true);

    }

    public function exportBapb(Request $request){
    	$orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $current_date = date_format(Carbon::now(), 'd/m/Y');
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d')
        );

        $data = DB::table('v_ns_bapb')
                ->where('factory_id', $factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkout', [$range['from'], $range['to']]);
                });

        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_size', 'like', '%'.$filterby.'%');
                                // ->orWhere('location', 'like', '%'.$filterby.'%');
                    });
        }

        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('po_number', 'asc')
                        ->orderBy('barcode_id', 'asc');
        }

        $get_factory = DB::table('factory')
                        ->where('factory_id', $factory_id)
                        ->wherenull('deleted_at')
                        ->first();

        $filename = $get_factory->factory_name.'_report_BAPB_' . date_format(Carbon::now(), 'Y-m-d')
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        $i=1;
        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }

        if ($data_result->count() > 0) {
        return (new FastExcel($data_result))->download($filename.'.xlsx', function ($row) use ($i, $current_date) {

                    return 
                            [
                                '#' => $row->no, 
                                'Barcode ID' => $row->barcode_id, 
                                'PO. Number' => $row->po_number,
                                'Season'=> $row->season, 
                                'Brand' => $row->trademark,  
                                'Style' => $row->upc, 
                                'Art. No.' => $row->buyer_item, 
                                'Prod. Ctg' => $row->product_category_detail, 
                                'Cust. Size' => $row->customer_size, 
                                'Man. Size' => $row->manufacturing_size,
                                'Qty' => $row->inner_pack,
                                'Check Out'=>$row->checkout,
                            ];
        });
     }else {
         return response()->json('no data', 422);
     }
    }
}
