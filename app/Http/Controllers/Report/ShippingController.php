<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

use Rap2hpoutre\FastExcel\FastExcel;

class ShippingController extends Controller
{
    public function index() {
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('report/shipping/index')->with('factory', $factory);
    }

    public function shippingIn() {
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('report/shipping/list_in')->with('factory', $factory);
    }

    public function shippingCompleted() {
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('report/shipping/list_completed')->with('factory', $factory);
    }

    public function getDataShipping(Request $request) {
        $filterby = $request->filterby;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('v_report_shipping')
                ->where('factory_id', Auth::user()->factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkin', [$range['from'], $range['to']])
                            ->orWhereBetween('checkout', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_size', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
                ->editColumn('prd_ctg',function($data){
                    

                    return $data->product_category_detail;
                })
               ->editColumn('dimension', function($data) {
                   $length = $data->length * 1000;
                   $width = $data->width * 1000;
                   $height = $data->height * 1000;
                   return $length . 'x' .$width . 'x' .$height . ' (MM)';
               })
               ->rawColumns(['prd_ctg','dimension'])
               ->make(true);
    }

    // loading area
    public function getDataShippingIn(Request $request) {
        // $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        // $range = array(
        //     'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
        //     'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        // );
        $data = DB::table('package_movements')
                ->select(
                    'package_movements.barcode_package',
                    'package_detail.customer_size',
                    'package_detail.length',
                    'package_detail.width',
                    'package_detail.height',
                    'package_detail.unit_2',
                    'po_summary.po_number'
                )
                ->join('package', 'package.barcode_id','=','package_movements.barcode_package')
                ->join('package_detail', 'package_detail.scan_id','=','package.scan_id')
                ->join('po_summary', 'package_detail.po_number','=','po_summary.po_number')
                ->where('is_canceled', false)
                ->where('package.current_department', 'shipping')
                ->where('package.current_status', 'onprogress')
                ->where('package_detail.factory_id', Auth::user()->factory_id)
                ->whereNull('package.deleted_at')
                ->whereNull('package_detail.deleted_at')
                ->whereNull('po_summary.deleted_at')
                ->groupBy(
                    'package_movements.barcode_package',
                    'package_detail.customer_size',
                    'package_detail.length',
                    'package_detail.width',
                    'package_detail.height',
                    'package_detail.unit_2',
                    'po_summary.po_number'
                );

        return Datatables::of($data)
               ->editColumn('dimension', function($data) {
                   return $data->length . 'x' .$data->width . 'x' .$data->height . ' ('.$data->unit_2.')';
               })
               ->editColumn('checkin', function($data) {
                   $type = 'checkin';
                   $checkin = $this->getTimeIn($type, $data->barcode_package);
                   return $checkin ? $checkin : '-';
               })
               // ->editColumn('checkout', function($data){
               //     $type = 'checkout';
               //     $checkout = $this->getTimeIn($type, $data->barcode_package);
               //     return $checkout ? $checkout : '-';
               // })
               ->make(true);
    }

    // stuffing completed per po
    public function getDataShippingCompleted(Request $request) {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('v_ad_shipping_completed')
                    ->where('factory_id', $factory_id)
                    ->WhereBetween('checkout', [$range['from'], $range['to']]);

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->Where('po_number', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
               ->editColumn('brand',function($data){
                    $brn = DB::table('po_summary')
                                ->select('trademark')
                                ->where('po_number',$data->po_number)
                                ->where('plan_ref_number',$data->plan_ref_number)
                                ->whereNull('deleted_at')
                                ->first();
                    return $brn->trademark;
               })
               ->editColumn('checkout', function($data) {
                   $date_checkout = date('d-m-Y', strtotime($data->checkout) );
                   return $date_checkout;
               })
               ->make(true);
    }

    //checkin, completed, or checkout
    public function getTime($type, $barcode, $from, $to) {
        $typelist = array(
            'checkin', 'checkout'
        );

        $data = DB::table('package_movements')
                     ->select('package_movements.created_at')
                     ->join('package', 'package.barcode_id','=','package_movements.barcode_package')
                     ->join('package_detail', 'package_detail.scan_id','=','package.scan_id')
                     ->join('po_summary', 'package_detail.po_number','=','po_summary.po_number')
                     ->where('barcode_package', $barcode)
                     ->whereBetween('package_movements.created_at', [$from, $to])
                     ->where('is_canceled', false)
                     ->whereNull('package.deleted_at')
                     ->whereNull('package_detail.deleted_at')
                     ->whereNull('po_summary.deleted_at');

        if($type == $typelist[0]) {
            $data = $data->where('department_from', 'inventory')
                         ->where('department_to', 'shipping')
                         ->where('status_from', 'completed')
                         ->where('status_to', 'onprogress');

        }
        elseif($type == $typelist[1]) {
            $data = $data->where('department_from', 'shipping')
                          ->where('department_to', 'shipping')
                          ->where('status_from', 'onprogress')
                          ->where('status_to', 'completed');
        }
        else {
            return false;
        }

        $data = $data->orderBy('package_movements.created_at','desc')->first();

        if(!$data) {
            return false;
        }
        return $data->created_at;
    }

    //checkin, completed, or checkout
    public function getTimeIn($type, $barcode) {
        $typelist = array(
            'checkin', 'checkout'
        );

        $data = DB::table('package_movements')
                     ->select('package_movements.created_at')
                     ->join('package', 'package.barcode_id','=','package_movements.barcode_package')
                     ->join('package_detail', 'package_detail.scan_id','=','package.scan_id')
                     ->join('po_summary', 'package_detail.po_number','=','po_summary.po_number')
                     ->where('barcode_package', $barcode)
                     ->where('is_canceled', false)
                     ->whereNull('package.deleted_at')
                     ->whereNull('package_detail.deleted_at')
                     ->whereNull('po_summary.deleted_at');

        if($type == $typelist[0]) {
            $data = $data->where('department_from', 'inventory')
                         ->where('department_to', 'shipping')
                         ->where('status_from', 'completed')
                         ->where('status_to', 'onprogress');

        }
        elseif($type == $typelist[1]) {
            $data = $data->where('department_from', 'shipping')
                          ->where('department_to', 'shipping')
                          ->where('status_from', 'onprogress')
                          ->where('status_to', 'completed');
        }
        else {
            return false;
        }

        $data = $data->orderBy('package_movements.created_at','desc')->first();

        if(!$data) {
            return false;
        }
        return $data->created_at;
    }

    public function exportShipping(Request $request) {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        //
        $data = DB::table('v_report_shipping')
                ->where('factory_id', $factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkin', [$range['from'], $range['to']])
                            ->orWhereBetween('checkout', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_size', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('checkin', 'asc')
                        ->orderBy('checkout', 'asc');
        }

        //naming file
        $get_factory = DB::table('factory')
                        ->where('factory_id', $factory_id)
                        ->wherenull('deleted_at')
                        ->first();
        // dd($factory_id);

        $filename = $get_factory->factory_name.'_report_shipping_' . $range['from'] . '_until_' . $range['to']
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        $i = 1;

        
        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
         }
         if ($data_result->count() > 0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx', function ($row) use ($i) {
                        //dimension
                        $length = $row->length * 1000;
                        $width = $row->width * 1000;
                        $height = $row->height * 1000;
                        $dimension = $length . 'x' .$width . 'x' .$height . ' (MM)';

                        
                        
                        //
                        return 
                                [
                                    '#' => $row->no, 
                                    'Barcode ID' => $row->barcode_id, 
                                    'PO Number' => $row->po_number,
                                    'Brand'=>$row->brand,
                                    'Style'=>$row->style,
                                    'Art. No'=>$row->buyer_item,
                                    'Prod. Category'=>$row->product_category_detail, 
                                    'Size' => $row->customer_size, 
                                    'Dimension (L x W x H (satuan))' => $dimension,
                                    'Qty' => $row->inner_pack,
                                    'Check In' => $row->checkin, 
                                    'Check Out' => $row->checkout 
                                ];
            });
         }else {
             return response()->json('no data', 422);
         }

     //   return response()->json('Success exporting', 200);
    }

    ///
    public function exportShippingIn(Request $request) {

        $filename = 'report_shipping_in_'.Carbon::now();
        $i = 1;
        $data = DB::table('package_movements')
                ->select(
                    'package_movements.barcode_package',
                    'package_detail.customer_size',
                    'package_detail.length',
                    'package_detail.width',
                    'package_detail.height',
                    'package_detail.unit_2',
                    'po_summary.po_number'
                )
                ->join('package', 'package.barcode_id','=','package_movements.barcode_package')
                ->join('package_detail', 'package_detail.scan_id','=','package.scan_id')
                ->join('po_summary', 'package_detail.po_number','=','po_summary.po_number')
                ->where('is_canceled', false)
                ->where('package.current_department', 'shipping')
                ->where('package.current_status', 'onprogress')
                ->where('package_detail.factory_id', Auth::user()->factory_id)
                ->whereNull('package.deleted_at')
                ->whereNull('package_detail.deleted_at')
                ->whereNull('po_summary.deleted_at')
                ->groupBy(
                    'package_movements.barcode_package',
                    'package_detail.customer_size',
                    'package_detail.length',
                    'package_detail.width',
                    'package_detail.height',
                    'package_detail.unit_2',
                    'po_summary.po_number'
                )
                ->orderBy('po_summary.po_number', 'asc')
                ->orderBy('package_movements.barcode_package', 'asc');

        $export = \Excel::create($filename, function($excel) use ($data, $i) {
            $excel->sheet('report', function($sheet) use($data, $i) {
                $sheet->appendRow(array(
                    '#', 'Barcode ID', 'PO Number', 'Size',  'QTY', 'Dimension (L x W x H (satuan))',
                    'Check In'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i)
                {
                    foreach ($rows as $row)
                    {
                        //checkin
                        $checkin = $this->getTimeIn('checkin', $row->barcode_package);
                        $data_checkin = $checkin ? $checkin : '-';

                        //completed
                        $completed = $this->getTimeIn('completed', $row->barcode_package);
                        $data_completed = $completed ? $completed : '-';

                        //checkout
                        // $checkout = $this->getTimeIn('checkout', $row->barcode_package);
                        // $data_checkout = $checkout ? $checkout : '-';

                        //dimension
                        $dimension = $row->length . 'x' .$row->width . 'x' .$row->height . ' ('.$row->unit_2.')';

                        //
                        $sheet->appendRow(array(
                            $i++, '="'.$row->barcode_package.'"', $row->po_number, $row->customer_size,$row->inner_pack, $dimension,
                            $data_checkin
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }

    //
    public function exportShippingCompleted(Request $request) {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        //
        $data = DB::table('v_shipping_completed')
                    ->where('factory_id', $factory_id)
                    ->WhereBetween('checkout', [$range['from'], $range['to']]);

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                            $query->where('po_number', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('checkout', 'asc');
        }

        //naming file
        $get_factory = DB::table('factory')
                        ->where('factory_id', $factory_id)
                        ->wherenull('deleted_at')
                        ->first();

        $filename = $get_factory->factory_name.'_report_stuffing_' . $range['from'] . '_until_' . $range['to']
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        $i = 1;

        // $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
        //     $excel->sheet('report', function($sheet) use($data, $i, $range) {
        //         $sheet->appendRow(array(
        //             '#', 'Date', 'PO Number', 'Cust.No', 'QTY', 'CBM', 'CTN', 'Total CTN', 'Balance'
        //         ));
        //         $data->chunk(100, function($rows) use ($sheet, $i, $range)
        //         {
        //             foreach ($rows as $row)
        //             {
        //                 //
        //                 $sheet->appendRow(array(
        //                     $i++, '="'.date('d-m-Y', strtotime($row->checkout)).'"', $row->po_number, $row->customer_number, $row->v_total_item, $row->v_cbm, $row->ctn, $row->total_ctn, $row->balance
        //                 ));
        //             }
        //         });
        //     });
        // })->download('xlsx');

        // return response()->json('Success exporting', 200);
        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
         }
         if ($data_result->count() > 0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx', function ($row) use ($i) {
                        //
                            
                        return 
                                [
                                    '#' => $row->no, 
                                    'Date' => date('d-m-Y', strtotime($row->checkout)), 
                                    'PO Number' => $row->po_number,
                                    'Brand'=> $row->trademark,
                                    'Art. No'=> $row->buyer_item,
                                    'Cust.No' => $row->customer_number, 
                                    'CTN' => $row->ctn, 
                                    'Total CTN' => $row->total_ctn, 
                                    'Balance' => $row->balance
                                ];
            });
         }else {
             return response()->json('no data', 422);
         }
    }
}
