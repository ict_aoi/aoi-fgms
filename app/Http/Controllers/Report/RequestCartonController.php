<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

class RequestCartonController extends Controller
{

    public function listrequest() {
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('report/requestcarton/list_request')->with('factory', $factory);
    }

    public function getDataListRequest(Request $request) {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('v_report_request_receive')
                ->where('factory_id', $factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('request_at', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('description', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number_concat', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
               // ->editColumn('request_at', function($data) {
               //     $date_request = date('d-m-Y H:i:s', strtotime($data->request_at) );
               //     return $date_request;
               // })
               ->make(true);
    }

    public function exportListRequest(Request $request) {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        //
        $data = DB::table('v_report_request_receive')
                ->where('factory_id', $factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('request_at', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_number', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('request_at', 'asc');
        }

        //naming file
        $get_factory = DB::table('factory')
                        ->where('factory_id', $factory_id)
                        ->wherenull('deleted_at')
                        ->first();

        $filename = $get_factory->factory_name.'_report_request_carton_receive' . $range['from'] . '_until_' . $range['to']
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
            $excel->sheet('report', function($sheet) use($data, $i, $range) {
                $sheet->appendRow(array(
                    '#', 'Date', 'Line', 'PO', 'Size', 'CTN', 'Packing', 'Sewing'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i, $range)
                {
                    foreach ($rows as $row)
                    {
                        if ($row->info !== null) {
                            $info = $row->info;
                        }else{
                            $info = '-';
                        }
                        // date('d-m-Y', strtotime($row->request_at))
                        $sheet->appendRow(array(
                            $i++, $row->request_at, $row->description, $row->po_number_concat, $row->manufacturing_size_concat, $row->carton_qty_concat, $row->done, $row->receive
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }

    // list request detail carton
    public function listrequestdetail() {
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('report/requestcarton/list_request_detail')->with('factory', $factory);
    }

    public function getDataListRequestDetail(Request $request) {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('v_report_request_receive_detail')
                ->where('factory_id', $factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('request_at', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('description', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
               // ->editColumn('request_at', function($data) {
               //     $date_request = date('d-m-Y H:i:s', strtotime($data->request_at) );
               //     return $date_request;
               // })
               ->editColumn('dimension', function($data) {
                   $length = $data->length * 1000;
                   $width = $data->width * 1000;
                   $height = $data->height * 1000;
                   return $length . 'x' .$width . 'x' .$height . ' (MM)';
               })
               ->editColumn('carton_qty', function($data) {
                   if ($data->all_size == 1) {
                        return $data->pkg_count;
                   }else{
                        return $data->carton_qty;
                   }
               })
               ->make(true);
    }

    public function exportListRequestDetail(Request $request) {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        //
        $data = DB::table('v_report_request_receive_detail')
                ->where('factory_id', $factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('request_at', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('description', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('request_at', 'asc');
        }

        //naming file
        $get_factory = DB::table('factory')
                        ->where('factory_id', $factory_id)
                        ->wherenull('deleted_at')
                        ->first();

        $filename = $get_factory->factory_name.'_report_request_receive_detail' . $range['from'] . '_until_' . $range['to']
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
            $excel->sheet('report', function($sheet) use($data, $i, $range) {
                $sheet->appendRow(array(
                    '#', 'Date', 'Line', 'PO', 'Size', 'Dimension', 'Pkg Code', 'Qty', 'Packing', 'Sewing'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i, $range)
                {
                    foreach ($rows as $row)
                    {
                       //dimension
                        $length = $row->length * 1000;
                        $width = $row->width * 1000;
                        $height = $row->height * 1000;
                        $dimension = $length . 'x' .$width . 'x' .$height . ' (MM)';

                        //carton qty
                        if ($row->all_size == 1) {
                            $qty = $row->pkg_count;
                       }else{
                            $qty = $row->carton_qty;
                       }

                        // date('d-m-Y', strtotime($row->request_at))
                        $sheet->appendRow(array(
                            $i++, $row->request_at, $row->description, $row->po_number, $row->manufacturing_size, $dimension, $row->pkg_code, $qty, $row->done, $row->receive
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }

    public function reportRequestCarton(){
         $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();
        return view('report/requestcarton/request_carton')->with('factory',$factory);
    }

    public function getReportRequestCarton(Request $request){
            $filterby = $request->filterby;
            $factory_id = $request->factory_id;
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            
            $data = DB::table('v_ns_request_carton')
                    ->where('factory_id', $factory_id)
                    ->where(function($query) use ($range) {
                        $query->whereBetween('request_at', [$range['from'], $range['to']]);
                    })
                    ->orderBy('request_at','asc');

            //jika filterby tidak kosong
            if(!empty($filterby)) {
                $data = $data->where(function($query) use ($filterby) {
                            $query->where('po_number', 'like', '%'.$filterby.'%')
                                    ->orWhere('description', 'like', '%'.$filterby.'%');
                        });
            }

            return Datatables::of($data)
                            ->addColumn('done',function($data){
                                 $dne = DB::table('request_movement')
                                                ->select('created_at')
                                                ->where('status_packing_to','done')
                                                ->where('request_id',$data->id)
                                                ->first();
                                if ($dne!=null) {
                                    return $dne->created_at;
                                }else{
                                    return '';
                                }
                            })
                            ->addColumn('receive',function($data){
                                $rcv = DB::table('request_movement')
                                                ->select('created_at')
                                                ->where('status_sewing_to','receive')
                                                ->where('request_id',$data->id)
                                                ->first();
                                if ($rcv!=null) {
                                    return $rcv->created_at;
                                }else{
                                    return '';
                                }
                            })
                            ->rawColumns(['done','receive'])
                            ->make(true);
    }

    public function exportRequestCarton(Request $request){
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        //
        $data = DB::table('v_ns_request_carton')
                ->where('factory_id', $factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('request_at', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('description', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('request_at', 'asc');
        }

        //naming file
        $get_factory = DB::table('factory')
                        ->where('factory_id', $factory_id)
                        ->wherenull('deleted_at')
                        ->first();

        $filename = $get_factory->factory_name.'_report_request_carton_' . $range['from'] . '_until_' . $range['to']
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
            $excel->sheet('report', function($sheet) use($data, $i, $range) {
                $sheet->appendRow(array(
                    '#', 'Date', 'Line', 'PO', 'Size', 'Qty', 'Packing', 'Sewing'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i, $range)
                {
                    foreach ($rows as $row)
                    {
                        $done = DB::table('request_movement')
                                                ->where('status_packing_to','done')
                                                ->where('request_id',$row->id)
                                                ->first();
                        if ($done!=null) {
                            $cdone = $done->created_at;
                        }else{
                            $cdone='';
                        }

                        $receive = DB::table('request_movement')
                                                ->where('status_sewing_to','receive')
                                                ->where('request_id',$row->id)
                                                ->first();
                        if ($receive!=null) {
                            $creceive = $receive->created_at;
                        }else{
                            $creceive='';
                        }

                        // date('d-m-Y', strtotime($row->request_at))
                        $sheet->appendRow(array(
                            $i++, $row->request_at, $row->line, $row->po_number, $row->manufacturing_size,$row->carton_qty, $cdone, $creceive
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }
}