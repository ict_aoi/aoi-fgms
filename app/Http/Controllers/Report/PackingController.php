<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

class PackingController extends Controller
{
    public function index() {
        return view('report/packing/index');;
    }

    // package calculate
    public function packingcalculate() {
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('report/packing/calculate')->with('factory', $factory);
    }

    public function getDataPacking(Request $request) {
        $filterby = $request->filterby;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('v_report_packing')
                ->where('factory_id', Auth::user()->factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('upload_date', [$range['from'], $range['to']])
                            ->orWhereBetween('print_date', [$range['from'], $range['to']])
                            ->orWhereBetween('checkout', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_size', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
               ->editColumn('dimension', function($data) {
                   $length = $data->length * 1000;
                   $width = $data->width * 1000;
                   $height = $data->height * 1000;
                   return $length . 'x' .$width . 'x' .$height . ' (MM)';
               })
               ->make(true);
    }

    public function exportPacking(Request $request) {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        //
        $data = DB::table('v_report_packing')
                ->where('factory_id', Auth::user()->factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('upload_date', [$range['from'], $range['to']])
                            ->orWhereBetween('print_date', [$range['from'], $range['to']])
                            ->orWhereBetween('checkout', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_size', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('upload_date', 'asc')
                        ->orderBy('print_date', 'asc')
                        ->orderBy('checkout', 'asc');
        }

        //naming file
        $filename = 'report_packing_' . $range['from'] . '_until_' . $range['to']
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
            $excel->sheet('report', function($sheet) use($data, $i, $range) {
                $sheet->appendRow(array(
                    '#', 'Barcode ID', 'Plan Number', 'PO Number', 'Size', 'Dimension (L x W x H (satuan))',
                    'Upload Date', 'Print Date', 'Check Out'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i, $range)
                {
                    foreach ($rows as $row)
                    {
                        //dimension
                        $length = $row->length * 1000;
                        $width = $row->width * 1000;
                        $height = $row->height * 1000;
                        $dimension = $length . 'x' .$width . 'x' .$height . ' (MM)';

                        //
                        $sheet->appendRow(array(
                            $i++, '="'.$row->barcode_id.'"', $row->plan_ref_number, $row->po_number, $row->customer_size, $dimension,
                            $row->upload_date, $row->print_date, $row->checkout
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }

    // package calculate
    public function getDataPackingCalculate(Request $request) {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $date_range_podd = explode('-', preg_replace('/\s+/', '', $request->date_range_podd));
        $range_podd = array(
            'from' => date_format(date_create($date_range_podd[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range_podd[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('v_package_detail_calculate')
                ->where('factory_id', $factory_id);


        if ($request->radio_status == 'po') {
            $data = $data->where('po_number', $request->po);
        }elseif ($request->radio_status == 'lc') {
            $data = $data->where(function($query) use ($range) {
                        $query->whereBetween('kst_lcdate', [$range['from'], $range['to']]);
                    });
        }elseif ($request->radio_status == 'invoice') {
            $data = $data->where('invoice', $request->invoice);
        }elseif ($request->radio_status == 'podd') {
            $data = $data->leftjoin('po_summary_sync','po_summary_sync.po_number','=','v_package_detail_calculate.po_number')
                            ->where(function($query) use ($range_podd){
                                $query->whereBetween('po_summary_sync.podd_check',[$range_podd['from'],$range_podd['to']]);
                            });
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('plan_ref_number', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%');
                    });
        }


        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
                return '<input type="checkbox" class="clplanref" name="selector[]" id="Inputselector" data-po="'.$data->po_number.'" data-planrefnumber="'.$data->plan_ref_number.'" data-cbm="'.$data->cbm.'" value="'.$data->cbm.'" data-invoice="'.$data->invoice.'">';
            })
            ->rawColumns(['action', 'checkbox'])
            ->make(true);
    }

    public function exportPackingCalculate(Request $request) {
         $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $date_range_podd = explode('-', preg_replace('/\s+/', '', $request->date_range_podd));
        $range_podd = array(
            'from' => date_format(date_create($date_range_podd[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range_podd[1]), 'Y-m-d 23:59:59')
        );

        

        //
        $data = DB::table('v_package_detail_calculate')
                ->where('factory_id', $factory_id);

        if ($request->radio_status == 'po') {
            $data = $data->where('po_number', $request->po);
        }elseif ($request->radio_status == 'lc') {
            $data = $data->where(function($query) use ($range) {
                        $query->whereBetween('kst_lcdate', [$range['from'], $range['to']]);
                    });
        }elseif ($request->radio_status == 'invoice') {
            $data = $data->where('invoice', $request->invoice);
        }elseif ($request->radio_status == 'podd') {
            $data = $data ->select('v_package_detail_calculate.po_number as po_number','v_package_detail_calculate.plan_ref_number as plan_ref_number','v_package_detail_calculate.invoice as invoice','v_package_detail_calculate.dateordered as dateordered','v_package_detail_calculate.datepromised as datepromised','v_package_detail_calculate.total_gross as total_gross','v_package_detail_calculate.total_net as total_net','v_package_detail_calculate.total_ctn as total_ctn','v_package_detail_calculate.cbm as cbm','v_package_detail_calculate.total_item_qty as total_item_qty')->leftjoin('po_summary_sync','po_summary_sync.po_number','=','v_package_detail_calculate.po_number')->whereBetween('po_summary_sync.podd_check',[$range_podd['from'],$range_podd['to']]);
        }

      

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('plan_ref_number', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('po_number', 'asc');
        }

        //naming file
        $get_factory = DB::table('factory')
                        ->where('factory_id', $factory_id)
                        ->wherenull('deleted_at')
                        ->first();

        if ($request->radio_status == 'po') {
            $filename = $get_factory->factory_name. '_report_packing_calculate' . '_po_' . $request->po
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        }elseif ($request->radio_status == 'lc') {
            $filename = $get_factory->factory_name. '_report_packing_calculate' . $range['from'] . '_until_' . $range['to']
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        }elseif ($request->radio_status == 'invoice'){
            $filename = $get_factory->factory_name. '_report_packing_calculate' . '_invoice_' . $request->invoice
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        }elseif ($request->radio_status == 'podd') {
            
            $filename = $get_factory->factory_name. '_report_packing_calculate' . $range_podd['from'] . '_until_' . $range_podd['to']
                    . '_orderby_' . $orderby. '_' . $direction . '_filterby_' . $filterby;
        }
        $i = 1;
        
        $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
            $excel->sheet('report', function($sheet) use($data, $i, $range) {
                $sheet->appendRow(array(
                    '#', 'Plan Number', 'PO Number', 'Invoice', 'Ordered Date', 'Promised Date',
                    'Gross', 'Net', 'CBM', 'CTN', 'Qty'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i, $range)
                {
                    foreach ($rows as $row)
                    {
                        //
                        $sheet->appendRow(array(
                            $i++, $row->plan_ref_number, $row->po_number, $row->invoice, $row->dateordered, $row->datepromised,
                            $row->total_gross, $row->total_net, $row->cbm, $row->total_ctn, $row->total_item_qty
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }#endexport
}
