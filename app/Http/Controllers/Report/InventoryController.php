<?php 

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

use Rap2hpoutre\FastExcel\FastExcel;

class InventoryController extends Controller
{
    public function __construct(){
        ini_set('max_execution_time', 1800);
    }
    
    public function index() {
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('report/inventory/index')->with('factory', $factory);
    }

    public function inventoryPo() {
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('report/inventory/list_po')->with('factory', $factory);
    }

    public function backtosewing() {
        return view('report/inventory/back_to_sewing');
    }

    // back to sewing
    public function getDataBackTosewing(Request $request) {
        $filterby = $request->filterby;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('v_back_to_sewing')
                ->where('factory_id', Auth::user()->factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('created_at', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_package', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_size', 'like', '%'.$filterby.'%');
                                // ->orWhere('location', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
               ->editColumn('dimension', function($data) {
                   $length = $data->length * 1000;
                   $width = $data->width * 1000;
                   $height = $data->height * 1000;
                   return $length . 'x' .$width . 'x' .$height . ' (MM)';
               })
               ->editColumn('created_at', function($data) {
                   $date_checkin = date('d-m-Y', strtotime($data->created_at) );
                   return $date_checkin;
               })
               ->make(true);
    }

    public function exportBackToSewing(Request $request) {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        //
        $data = DB::table('v_back_to_sewing')
                ->where('factory_id', Auth::user()->factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('created_at', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_package', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_size', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('created_at', 'asc');
                        // ->orderBy('movement_date', 'asc')
                        // ->orderBy('checkout', 'asc');
        } 

        //naming file
        $filename = 'report_bongkaran_' . $range['from'] . '_until_' . $range['to']
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
            $excel->sheet('report', function($sheet) use($data, $i, $range) {
                $sheet->appendRow(array(
                    '#', 'Barcode ID', 'PO Number', 'Size', 'Dimension (L x W x H (satuan))', 'Date',
                    'Item Qty'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i, $range)
                {
                    foreach ($rows as $row)
                    {
                        //dimension
                        $length = $row->length * 1000;
                        $width = $row->width * 1000;
                        $height = $row->height * 1000;
                        $dimension = $length . 'x' .$width . 'x' .$height . ' (MM)';

                        //
                        $sheet->appendRow(array(
                            $i++, '="'.$row->barcode_id.'"', $row->po_number, $row->customer_size, $dimension, $row->created_at, $row->item_qty
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }

    public function getDataInventory(Request $request) {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('v_inventory_in')
                ->where('factory_id', $factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkin', [$range['from'], $range['to']]);
                            // ->orWhereBetween('movement_date', [$range['from'], $range['to']])
                            // ->orWhereBetween('checkout', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_size', 'like', '%'.$filterby.'%');
                                // ->orWhere('location', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
                ->editColumn('prd_ctg',function($data){
                    $gp = DB::table('po_summary_sync')
                                ->select('product_category_detail')
                                ->where('po_number',$data->po_number)
                                ->first();

                    $gp2 = DB::table('po_master_sync')
                                ->select('product_category_detail')
                                ->where('po_number',$data->po_number)
                                ->first();

                    if (isset($gp2->product_category_detail)) {
                        $return = $gp2->product_category_detail;
                    }else if (isset($gp->product_category_detail)) {
                        $return = $gp->product_category_detail;
                    }else{
                        $return = '-';
                    }
                    return $return;
                }) 
               ->editColumn('dimension', function($data) {
                   $length = $data->length * 1000;
                   $width = $data->width * 1000;
                   $height = $data->height * 1000;
                   return $length . 'x' .$width . 'x' .$height . ' (MM)';
               })
               ->editColumn('checkin', function($data) {
                   $date_checkin = date('d-m-Y H:i:s', strtotime($data->checkin) );
                   return $date_checkin;
               })
               ->addColumn('line',function($data){
                    $gL = DB::table('v_cek_carton_line')->select('description')->where('barcode_package',$data->barcode_id)->first();
                    return  !empty($gL->description) ? $gL->description :'-';
               })
               ->make(true);

    }

    // public function exportInventory(Request $request) {
    //     $orderby = $request->orderby;
    //     $direction = $request->direction;
    //     $filterby = $request->filterby;
    //     $factory_id = $request->factory_id;
    //     $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
    //     $range = array(
    //         'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
    //         'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
    //     );

    //     //
    //     $data = DB::table('v_inventory_in')
    //             ->where('factory_id', $factory_id)
    //             ->where(function($query) use ($range) {
    //                 $query->whereBetween('checkin', [$range['from'], $range['to']]);
    //                         // ->orWhereBetween('movement_date', [$range['from'], $range['to']])
    //                         // ->orWhereBetween('checkout', [$range['from'], $range['to']]);
    //             });

    //     //jika filterby tidak kosong
    //     if(!empty($filterby)) {
    //         $data = $data->where(function($query) use ($filterby) {
    //                     $query->where('barcode_id', 'like', '%'.$filterby.'%')
    //                             ->orWhere('po_number', 'like', '%'.$filterby.'%')
    //                             ->orWhere('customer_size', 'like', '%'.$filterby.'%');
    //                 });
    //     }

    //     //jika orderby tidak undefined
    //     if($orderby != 'undefined') {
    //         $data = $data->orderBy($orderby, $direction);
    //     }
    //     else {
    //         $data = $data->orderBy('checkin', 'asc');
    //                     // ->orderBy('movement_date', 'asc')
    //                     // ->orderBy('checkout', 'asc');
    //     }

    //     //naming file
    //     // $filename = 'report_inventory_' . $range['from'] . '_until_' . $range['to']
    //     //             . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
    //     //naming file
    //     $get_factory = DB::table('factory')
    //                     ->where('factory_id', $factory_id)
    //                     ->wherenull('deleted_at')
    //                     ->first();

    //     $filename = $get_factory->factory_name. '_report_inventory_' . $range['from'] . '_until_' . $range['to']
    //                 . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;

    //     $i = 1;

    //     $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
    //         $excel->sheet('report', function($sheet) use($data, $i, $range) {
    //             $sheet->appendRow(array(
    //                 '#', 'Barcode ID', 'PO Number', 'Style', 'Art. No', 'Prod. Category', 'Size', 'Dimension (L x W x H (satuan))', 'Qty', 'FROM', 'Check In', 'Line'
    //             ));
    //             $data->chunk(100, function($rows) use ($sheet, $i, $range)
    //             {
    //                 foreach ($rows as $row)
    //                 {
    //                     //dimension
    //                     $length = $row->length * 1000;
    //                     $width = $row->width * 1000;
    //                     $height = $row->height * 1000;
    //                     $dimension = $length . 'x' .$width . 'x' .$height . ' (MM)';

    //                     $gdt = DB::table('po_summary_sync')
    //                                 ->select('style','product_category_detail')
    //                                 ->where('po_number',$row->po_number)
    //                                 ->first();

    //                     $gdt2 = DB::table('po_master_sync')
    //                                 ->select('style','product_category_detail')
    //                                 ->where('po_number',$row->po_number)
    //                                 ->first();

    //                     if (isset($gdt2->product_category_detail)) {
    //                         $prd = $gdt2->product_category_detail;
    //                     }else if (isset($gdt->product_category_detail)) {
    //                         $prd = $gdt->product_category_detail;
    //                     }else{
    //                         $prd = "-";
    //                     }
                                
    //                     $gL = DB::table('v_cek_carton_line')->select('description')->where('barcode_package',$row->barcode_id)->first();

    //                     $sheet->appendRow(array(
    //                         $i++,
    //                         '="'.$row->barcode_id.'"', 
    //                         $row->po_number, 
    //                         $row->upc,
    //                         $row->buyer_item, 
    //                         $prd, 
    //                         $row->customer_size, 
    //                         $dimension, 
    //                         $row->inner_pack, 
    //                         $row->department_from, 
    //                         date('d-m-Y H:i:s', strtotime($row->checkin)), 
    //                         !empty($gL->description) ? $gL->description : "-"
    //                     ));
    //                 }
    //             });
    //         });
    //     })->download('xlsx');

    //     return response()->json('Success exporting', 200);
    // }


    public function exportInventory(Request $request) {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        //
        $data = DB::table('v_inventory_in')
                ->where('factory_id', $factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkin', [$range['from'], $range['to']]);
                            // ->orWhereBetween('movement_date', [$range['from'], $range['to']])
                            // ->orWhereBetween('checkout', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_size', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('checkin', 'asc');
                        // ->orderBy('movement_date', 'asc')
                        // ->orderBy('checkout', 'asc');
        }

        //naming file
        // $filename = 'report_inventory_' . $range['from'] . '_until_' . $range['to']
        //             . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        //naming file
        $get_factory = DB::table('factory')
                        ->where('factory_id', $factory_id)
                        ->wherenull('deleted_at')
                        ->first();

        $filename = $get_factory->factory_name. '_report_inventory_' . $range['from'] . '_until_' . $range['to']
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;

        $i = 1;

        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }

        if ($data_result->count()>0) {
            return ( new FastExcel($data_result))->download($filename.'.xlsx',function($row) use ($i){

                    $prd = DB::table('po_master_sync')
                                    ->select('style','product_category_detail')
                                    ->where('po_number',$row->po_number)
                                    ->first();

                    $length = $row->length * 1000;
                    $width = $row->width * 1000;
                    $height = $row->height * 1000;
                    $dimension = $length . 'x' .$width . 'x' .$height . ' (MM)';

                    $gL = DB::table('v_cek_carton_line')->select('description')->where('barcode_package',$row->barcode_id)->first();
                return 
                    [
                        '#'=>$row->no,
                        'Barcode ID'=>$row->barcode_id,
                        'PO Number'=>$row->po_number,
                        'Style'=>$row->upc,
                        'Art. No'=>$row->buyer_item,
                        'Prod. Category'=>!empty($prd->product_category_detail) ? $prd->product_category_detail : "-",
                        'Customer Size'=>$row->customer_size,
                        'Manufacturing Size'=>$row->manufacturing_size,
                        'Dimension (L x W x H (satuan))'=>$dimension, 
                        'Qty'=>$row->inner_pack,
                        'From'=>$row->department_from,
                        'Check In'=>date('d-m-Y H:i:s', strtotime($row->checkin)),
                        'Line'=>!empty($gL->description) ? $gL->description : "-"
                    ];
            });
        }else{
             return response()->json('no data', 422);
        }
       
    }

    public function getDataInventoryPo(Request $request) {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('v_inventory_in_perpo')
                ->where('factory_id', $factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkin', [$range['from'], $range['to']]);
                            // ->orWhereBetween('movement_date', [$range['from'], $range['to']])
                            // ->orWhereBetween('checkout', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_number', 'like', '%'.$filterby.'%');
                                // ->orWhere('location', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
               // ->editColumn('dimension', function($data) {
               //     $length = $data->length * 1000;
               //     $width = $data->width * 1000;
               //     $height = $data->height * 1000;
               //     return $length . 'x' .$width . 'x' .$height . ' (MM)';
               // })
               ->editColumn('checkin', function($data) {
                   $date_checkin = date('d-m-Y', strtotime($data->checkin) );
                   return $date_checkin;
               })
               ->make(true);
    }

    public function exportInventoryPo(Request $request) {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        //
        $data = DB::table('v_inventory_in_perpo')
                ->where('factory_id', $factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkin', [$range['from'], $range['to']]);
                            // ->orWhereBetween('movement_date', [$range['from'], $range['to']])
                            // ->orWhereBetween('checkout', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_number', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('checkin', 'asc');
                        // ->orderBy('movement_date', 'asc')
                        // ->orderBy('checkout', 'asc');
        }

        //naming file
        // $filename = 'report_inventory_perpo' . $range['from'] . '_until_' . $range['to']
        //             . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;

        //naming file
        $get_factory = DB::table('factory')
                        ->where('factory_id', $factory_id)
                        ->wherenull('deleted_at')
                        ->first();

        $filename = $get_factory->factory_name.'_report_inventory_perpo' . $range['from'] . '_until_' . $range['to']
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;

        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
            $excel->sheet('report', function($sheet) use($data, $i, $range) {
                $sheet->appendRow(array(
                    '#', 'PO Number', 'CUST.O', 'FROM', 'CTN', 'Check In'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i, $range)
                {
                    foreach ($rows as $row)
                    {
                        //dimension
                        $length = $row->length * 1000;
                        $width = $row->width * 1000;
                        $height = $row->height * 1000;
                        $dimension = $length . 'x' .$width . 'x' .$height . ' (MM)';

                        //
                        $sheet->appendRow(array(
                            $i++, $row->po_number, $row->customer_number, $row->department_from, $row->ctn, date('d-m-Y', strtotime($row->checkin))
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }

    

}
