<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

use Rap2hpoutre\FastExcel\FastExcel;

class StockController extends Controller
{
    public function index()
    {
        $factory = DB::table('factory')
            ->select('factory_id', 'factory_name')
            ->wherenull('deleted_at')
            ->get();

        return view('report/stock/index')->with('factory', $factory);
    }

    public function getDatastock(Request $request)
    {
        $factory_id = $request->factory_id;
        $data = DB::table('package')
            ->select(
                'package.barcode_id',
                'package_detail.inner_pack',
                'package_detail.customer_size',
                'package_detail.length',
                'package_detail.width',
                'package_detail.height',
                'package_detail.unit_2',
                'package_detail.is_cancel_order',
                'package_detail.buyer_item',
                'po_summary.po_number',
                'po_summary.upc',
                'po_summary.trademark',
                'package.created_at',
                'package.updated_at',
                'package_movements.barcode_package',
                'package_movements.created_at AS checkin',
                'locators.code',
                'package_detail.manufacturing_size'
            )
            ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
            // ->join('po_summary', 'package_detail.po_number','=','po_summary.po_number')
            ->join('po_summary', function ($join) {
                $join->on('package_detail.po_number', '=', 'po_summary.po_number');
                $join->on('package_detail.plan_ref_number', '=', 'po_summary.plan_ref_number');
            })
            ->leftjoin('package_movements', 'package_movements.barcode_package', '=', 'package.barcode_id')
            ->leftjoin('locators', 'locators.barcode', '=', 'package_movements.barcode_locator')
            ->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('current_department', 'inventory')
                        ->where('current_status', 'completed');
                })
                    ->orWhere(function ($query) {
                        $query->where('current_department', 'shipping')
                            ->where('current_status', 'onprogress');
                    });
            })
            ->where('package_detail.factory_id', $factory_id)
            // ->where(function($query){
            //     $query->orWhere('package_detail.is_cancel_order',null);
            //     $query->orWhere('package_detail.is_cancel_order',false);
            // })
            ->where('is_canceled', false)
            ->whereNull('package.deleted_at')
            ->whereNull('package_detail.deleted_at')
            ->whereNull('po_summary.deleted_at')
            ->whereNull('package_movements.deleted_at')
            ->groupBy(
                'package.barcode_id',
                'package_detail.inner_pack',
                'package_detail.customer_size',
                'package_detail.length',
                'package_detail.width',
                'package_detail.height',
                'package_detail.unit_2',
                'package_detail.is_cancel_order',
                'package_detail.buyer_item',
                'po_summary.po_number',
                'po_summary.upc',
                'po_summary.trademark',
                'locators.code',
                'package.created_at',
                'package.updated_at',
                'package_movements.created_at',
                'package_movements.barcode_package',
                'package_detail.manufacturing_size'
            )
            ->orderBy('po_summary.po_number', 'asc')
            ->orderBy('package.created_at', 'asc');

        return Datatables::of($data)
            ->addColumn('dimension', function ($data) {
                return $data->length . 'x' . $data->width . 'x' . $data->height . ' (' . $data->unit_2 . ')';
            })
            ->editColumn('info_order', function ($data) {
                // cek remarks po

                if ($data->is_cancel_order == true) {
                    return '<span class="badge badge-danger position-right">Cancel/Reduce</span>';
                } else {
                    return '<span class="badge badge-success position-right">Active</span>';
                }
            })
            ->addColumn('product_category_detail', function ($data) {
                $get_summary_sync = DB::table('po_summary_sync')
                    ->select('po_number', 'product_category_detail')
                    ->where('po_number', $data->po_number)
                    ->first();

                $get_master_sync = DB::table('po_master_sync')
                    ->select('po_number', 'product_category_detail')
                    ->where('po_number', $data->po_number)
                    ->first();

                if (isset($get_master_sync->product_category_detail)) {
                    $prod = $get_master_sync->product_category_detail;
                } else if (isset($get_summary_sync->product_category_detail)) {
                    $prod = $get_summary_sync->product_category_detail;
                } else {
                    $prod = "-";
                }

                return $prod;
            })
            ->editColumn('buyer_item', function ($data) {
                $art =  isset($data->buyer_item) ? $data->buyer_item : '-';
                return $art;
            })
            ->editColumn('code', function ($data) {
                if (empty($data->code)) {
                    return 'LOADING';
                } else {
                    return $data->code;
                }
            })
            ->rawColumns(['info_order', 'dimension', 'product_category_detail', 'buyer_item', 'code'])
            ->make(true);
    }

    public function exportStock(Request $request)
    {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $current_date = date_format(Carbon::now(), 'd/m/Y');

        $i = 1;
        $data = DB::table('package')
            ->select(
                'package.barcode_id',
                'package_detail.inner_pack',
                'package_detail.customer_size',
                'package_detail.length',
                'package_detail.width',
                'package_detail.height',
                'package_detail.unit_2',
                'package_detail.is_cancel_order',
                'package_detail.buyer_item',
                'po_summary.po_number',
                'po_summary.upc',
                'po_summary.trademark',
                'po_summary.bp_name',
                'package.created_at',
                'package.updated_at',
                'package_movements.barcode_package',
                'package_movements.created_at AS checkin',
                'locators.code',
                'package_detail.manufacturing_size',
                'package_detail.is_cancel_order'
            )
            ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
            // ->join('po_summary', 'package_detail.po_number','=','po_summary.po_number')
            ->join('po_summary', function ($join) {
                $join->on('package_detail.po_number', '=', 'po_summary.po_number');
                $join->on('package_detail.plan_ref_number', '=', 'po_summary.plan_ref_number');
            })
            ->leftjoin('package_movements', 'package_movements.barcode_package', '=', 'package.barcode_id')
            ->leftjoin('locators', 'locators.barcode', '=', 'package_movements.barcode_locator')
            ->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('current_department', 'inventory')
                        ->where('current_status', 'completed');
                })
                    ->orWhere(function ($query) {
                        $query->where('current_department', 'shipping')
                            ->where('current_status', 'onprogress');
                    });
            })
            ->where('package_detail.factory_id', $factory_id)
            // ->where(function($query){
            //     $query->orWhere('package_detail.is_cancel_order',null);
            //     $query->orWhere('package_detail.is_cancel_order',false);
            // })
            ->where('is_canceled', false)
            ->whereNull('package.deleted_at')
            ->whereNull('package_detail.deleted_at')
            ->whereNull('po_summary.deleted_at')
            ->whereNull('package_movements.deleted_at')
            ->groupBy(
                'package.barcode_id',
                'package_detail.inner_pack',
                'package_detail.customer_size',
                'package_detail.length',
                'package_detail.width',
                'package_detail.height',
                'package_detail.unit_2',
                'package_detail.is_cancel_order',
                'package_detail.buyer_item',
                'po_summary.po_number',
                'po_summary.upc',
                'po_summary.trademark',
                'po_summary.bp_name',
                'locators.code',
                'package.created_at',
                'package.updated_at',
                'package_movements.created_at',
                'package_movements.barcode_package',
                'package_detail.manufacturing_size',
                'package_detail.is_cancel_order'
            );


        //jika filterby tidak kosong
        if (!empty($filterby)) {
            $data = $data->where(function ($query) use ($filterby) {
                $query->where('package.barcode_id', 'like', '%' . $filterby . '%')
                    ->orWhere('po_summary.po_number', 'like', '%' . $filterby . '%')
                    ->orWhere('package_detail.customer_size', 'like', '%' . $filterby . '%')
                    ->orWhere('locators.code', 'like', '%' . $filterby . '%');
            });
        }

        //jika orderby tidak undefined
        if ($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        } else {
            $data = $data->orderBy('locators.code', 'asc')
                ->orderBy('po_summary.po_number', 'asc');
        }

        //naming file
        $get_factory = DB::table('factory')
            ->where('factory_id', $factory_id)
            ->wherenull('deleted_at')
            ->first();

        $filename = $get_factory->factory_name . '_report_stock_' . date_format(Carbon::now(), 'Y-m-d')
            . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        // naufal_1
        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }
        if ($data_result->count() > 0) {
            return (new FastExcel($data_result))->download($filename . '.xlsx', function ($row) use ($i, $current_date) {
                //dimension
                $length = isset($row->length) ? ($row->length * 1000) : 0;
                $width =  isset($row->width) ? ($row->width * 1000) : 0;
                $height = isset($row->height) ? ($row->height * 1000) : 0;
                $dimension = $length . 'x' . $width . 'x' . $height . ' (MM)';
                //
                // cek remarks po
                $get_summary_sync = DB::table('po_summary_sync')
                    ->select('po_number', 'product_category_detail')
                    ->where('po_number', $row->po_number)
                    ->first();

                $get_master_sync = DB::table('po_master_sync')
                    ->select('po_number', 'product_category_detail', 'po_grade')
                    ->where('po_number', $row->po_number)
                    ->first();

                if (isset($get_summary_sync->product_category_detail)) {
                    $prod = $get_summary_sync->product_category_detail;
                } else if (isset($get_master_sync->product_category_detail)) {
                    $prod = $get_master_sync->product_category_detail;
                } else {
                    $prod = "-";
                }

                if ($row->is_cancel_order == true) {
                    $info_order = 'Cancel/Reduce';
                } else {
                    $info_order = 'Active';
                }

                return
                    [
                        '#' => $row->no,
                        'Barcode ID' => $row->barcode_id,
                        'Style' => $row->upc,
                        'Art. No.' => $row->buyer_item,
                        'PO Number' => $row->po_number,
                        'Brand' => $row->trademark,
                        'Product Category' => $prod,
                        'Cust.Size' => $row->customer_size,
                        'Man.Size' => $row->manufacturing_size,
                        'Dimension (L x W x H (satuan))' => $dimension,
                        'Qty' => $row->inner_pack,
                        'Location' => !empty($row->code) ? $row->code : 'LOADING',
                        'Checkin' => $row->checkin,
                        'Date (d/m/y)' => $current_date,
                        'Remarks' => $info_order,
                        'PO Grade' => isset($get_master_sync->po_grade) ? $get_master_sync->po_grade : "-",
                        'Business Partner' => $row->bp_name
                    ];
            });
        } else {
            return response()->json('no data', 422);
        }
    }

    // ending balance
    public function endingBalance()
    {
        $factory = DB::table('factory')
            ->select('factory_id', 'factory_name')
            ->wherenull('deleted_at')
            ->get();

        return view('report/stock/ending_balance')->with('factory', $factory);
    }

    public function getDatastockBalance(Request $request)
    {
        $factory_id = $request->factory_id;

        if ($request->radio_status != 'current') {
            //$date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            // $date_range = Carbon::parse($request->date_range)->format('Y-m-d');
            // $date_range1 = explode(',',$request->date_range);
            // $date_md = Carbon::parse($date_range1[0])->format('m-d');
            // $date_range = $date_range1[1].'-'.$date_md;
            $date_r = Carbon::createFromFormat('d F, Y', $request->date_range)->format('Y-m-d');
            // $range = array(
            //     'from' => date_format(date_create($date_range), 'Y-m-d 00:00:00'),
            //     'to' => date_format(date_create($date_range), 'Y-m-d 23:59:59')
            // );

            $range = array(
                'from' => date_format(date_create($date_r), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_r), 'Y-m-d 23:59:59')
            );



            $data = DB::table('end_balance')
                ->select(DB::raw('DISTINCT ON (barcode_id) barcode_id, upc, po_number,  customer_size, manufacturing_size, inner_pack, length, width, height, unit_2, created_at, updated_at, checkin, code,  current_department, current_status, factory_id, date_balance, description'))
                ->where(function ($query) use ($range) {
                    $query->whereBetween('date_balance', [$range['from'], $range['to']]);
                })
                ->where('factory_id', $factory_id)
                ->orderBy('barcode_id', 'asc');
        } else {

            // $data = DB::table('package')
            //         ->select(
            //             'package.barcode_id',
            //             'package_detail.inner_pack',
            //             'package_detail.customer_size',
            //             'package_detail.length',
            //             'package_detail.width',
            //             'package_detail.height',
            //             'package_detail.unit_2',
            //             'po_summary.po_number',
            //             'po_summary.upc',
            //             'package.created_at',
            //             'package.updated_at',
            //             'package_movements.barcode_package',
            //             'package_movements.created_at AS checkin',
            //             'locators.code',
            //             'package_detail.manufacturing_size',
            //             'po_summary_sync.product_category',
            //             'po_summary_sync.product_category_detail',
            //             'package.current_department',
            //             'package.current_status',
            //             'line.description'
            //         )
            //         ->join('package_detail', 'package_detail.scan_id','=','package.scan_id')
            //         // ->join('po_summary', 'package_detail.po_number','=','po_summary.po_number')
            //         ->join('po_summary', function($join) {
            //             $join->on('package_detail.po_number','=','po_summary.po_number');
            //             $join->on('package_detail.plan_ref_number','=','po_summary.plan_ref_number');
            //         })
            //         ->leftjoin('po_summary_sync', 'po_summary_sync.po_number', '=', 'po_summary.po_number')
            //         ->leftjoin('package_movements', 'package_movements.barcode_package','=','package.barcode_id')
            //         ->leftjoin('locators','locators.barcode','=','package_movements.barcode_locator')
            //         ->leftjoin('line','line.id','=','package_movements.line_id')
            //         ->where(function($query) {
            //             $query->where(function($query) {
            //                 $query->where('current_department', 'inventory')
            //                     ->where('current_status', 'onprogress');
            //             })
            //                 ->orWhere(function($query) {
            //                 $query->where('current_department', 'inventory')
            //                 ->where('current_status', 'completed');
            //             })
            //             ->orWhere(function($query) {
            //                 $query->where('current_department', 'qcinspect')
            //                 ->where('current_status', 'onprogress');
            //             })
            //             ->orWhere(function($query) {
            //                 $query->where('current_department', 'qcinspect')
            //                 ->where('current_status', 'completed');
            //             })
            //             ->orWhere(function($query) {
            //                 $query->where('current_department', 'shipping')
            //                 ->where('current_status', 'onprogress');
            //             })
            //             ->orWhere(function($query) {
            //                 $query->where('current_department', 'sewing')
            //                 ->where('current_status', 'completed');
            //             });

            //         })
            //         ->where('package_detail.factory_id', $factory_id)
            //         ->where('is_canceled',false)
            //         ->whereNull('package.deleted_at')
            //         ->whereNull('package_detail.deleted_at')
            //         ->whereNull('po_summary.deleted_at')
            //         ->whereNull('package_movements.deleted_at')
            //         ->groupBy(
            //             'package.barcode_id',
            //             'package_detail.inner_pack',
            //             'package_detail.customer_size',
            //             'package_detail.length',
            //             'package_detail.width',
            //             'package_detail.height',
            //             'package_detail.unit_2',
            //             'po_summary.po_number',
            //             'po_summary.upc',
            //             'locators.code',
            //             'package.created_at',
            //             'package.updated_at',
            //             'package_movements.created_at',
            //             'package_movements.barcode_package',
            //             'package_detail.manufacturing_size',
            //             'po_summary_sync.product_category',
            //             'po_summary_sync.product_category_detail',
            //             'package.current_department',
            //             'package.current_status',
            //             'line.description'
            //             )
            //         ->orderBy('po_summary.po_number','asc')
            //         ->orderBy('package.created_at','asc');

            $data = DB::table('v_ns_current_balance')
                ->where('factory_id', $factory_id)
                ->orderBy('po_number', 'asc')
                ->orderBy('created_at', 'asc')
                ->orderBy('barcode_id', 'asc');
        }

        return Datatables::of($data)
            ->addColumn('art_no', function ($data) {
                $art = DB::table('package_detail')
                    ->select('buyer_item')
                    ->join('package', 'package.scan_id', '=', 'package_detail.scan_id')
                    ->where('package.barcode_id', $data->barcode_id)
                    ->whereNull('package.deleted_at')->first();
                return $art->buyer_item;
            })
            ->addColumn('brand', function ($data) {
                $brd = DB::table('po_summary')
                    ->select('trademark')
                    ->where('po_number', $data->po_number)
                    ->whereNull('deleted_at')->first();

                return $brd->trademark;
            })
            ->editColumn('dimension', function ($data) {
                return $data->length . 'x' . $data->width . 'x' . $data->height . ' (' . $data->unit_2 . ')';
            })
            ->editColumn('code', function ($data) {
                if ($data->current_department == 'inventory') {
                    if ($data->current_status == 'completed') {
                        return $data->code;
                    } else {
                        return 'Metal Detector';
                    }
                } elseif ($data->current_department == 'shipping') {
                    return 'LOADING';
                } elseif ($data->current_department == 'qcinspect') {
                    return 'QC Inspect';
                } elseif ($data->current_department == 'sewing') {
                    return $data->description ? $data->description : 'LINE';
                }
            })
            ->addColumn('date_balance', function ($data) {
                if (isset($data->date_balance)) {
                    return Carbon::parse($data->date_balance)->format('d/m/Y');
                } else {
                    return Carbon::now()->format('d/m/Y');
                }
            })
            ->make(true);
    }

    public function exportStockBalance(Request $request)
    {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $current_date = date_format(Carbon::now(), 'd/m/Y');

        if ($request->radio_status != 'current') {


            $date_r = Carbon::createFromFormat('d F, Y', $request->date_range)->format('Y-m-d');
            $range = array(
                'from' => date_format(date_create($date_r), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_r), 'Y-m-d 23:59:59')
            );

            $data = DB::table('end_balance')
                ->select(DB::raw('DISTINCT ON (barcode_id) barcode_id, upc, po_number,  customer_size, manufacturing_size, inner_pack, length, width, height, unit_2, created_at, updated_at, checkin, code, current_department, current_status, factory_id, date_balance, description'))
                ->where(function ($query) use ($range) {
                    $query->whereBetween('date_balance', [$range['from'], $range['to']]);
                })
                ->where('factory_id', $factory_id)
                ->orderBy('barcode_id', 'asc');

            if (!empty($filterby)) {
                $data = $data->where(function ($query) use ($filterby) {
                    $query->where('barcode_id', 'like', '%' . $filterby . '%')
                        ->orWhere('po_number', 'like', '%' . $filterby . '%')
                        ->orWhere('customer_size', 'like', '%' . $filterby . '%');
                    // ->orWhere('code', 'like', '%'.$filterby.'%');
                });
            }

            //jika orderby tidak undefined
            if ($orderby != 'undefined') {
                $data = $data->orderBy($orderby, $direction);
            } else {
                $data = $data->orderBy('po_number', 'asc');
            }
        } else {


            $data = DB::table('v_ns_current_balance')
                ->where('factory_id', $factory_id)
                ->orderBy('po_number', 'asc')
                ->orderBy('created_at', 'asc');

            if (!empty($filterby)) {
                $data = $data->where(function ($query) use ($filterby) {
                    $query->where('barcode_id', 'like', '%' . $filterby . '%')
                        ->orWhere('po_number', 'like', '%' . $filterby . '%')
                        ->orWhere('customer_size', 'like', '%' . $filterby . '%');
                    // ->orWhere('code', 'like', '%'.$filterby.'%');
                });
            }

            //jika orderby tidak undefined
            if ($orderby != 'undefined') {
                $data = $data->orderBy($orderby, $direction);
            } else {
                $data = $data->orderBy('po_number', 'asc');
            }
        }


        //jika filterby tidak kosong


        //naming file
        $get_factory = DB::table('factory')
            ->where('factory_id', $factory_id)
            ->wherenull('deleted_at')
            ->first();

        if ($request->radio_status != 'current') {
            $c_date = Carbon::parse($request->date_range)->format('d_m_Y');
        } else {
            $c_date = date_format(Carbon::now(), 'd_m_Y');
        }

        $filename = $get_factory->factory_name . '_report_stock_end_balance_' . $c_date
            . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        $i = 1;





        // by naufal

        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }

        if ($data_result->count() > 0) {
            return (new FastExcel($data_result))->download($filename . '.xls', function ($row) use ($i, $current_date) {
                $length = $row->length * 1000;
                $width = $row->width * 1000;
                $height = $row->height * 1000;
                $dimension = $length . 'x' . $width . 'x' . $height . ' (MM)';

                $cb = DB::table('v_ns_barcode_balance')->where('barcode_id', $row->barcode_id)->first();

                if (isset($cb->is_cancel_order) && $cb->is_cancel_order == true) {
                    $info_order = 'Cancel';
                } else {
                    $info_order = 'Active';
                }

                if ($row->current_department == 'inventory' && ($row->current_status == 'onprogress' || $row->current_status == 'rejected')) {
                    $loc = 'Metal Detector';
                } else if ($row->current_department == 'inventory' && $row->current_status == 'completed') {
                    $loc = $row->code;
                } else if ($row->current_department == 'qcinspect') {
                    $loc = 'QC Inspect';
                } else if ($row->current_department == 'shipping') {
                    $loc = 'LOADING';
                } else if ($row->current_department == 'sewing') {
                    $loc = $row->description != null ? $row->description : "";
                }


                return
                    [
                        '#' => $row->no,
                        'Barcode ID' => $row->barcode_id,
                        'Style' => $row->upc,
                        'Art. No.' => isset($cb->buyer_item) ? $cb->buyer_item : '',
                        'PO Number' => $row->po_number,
                        'Brand' => isset($cb->trademark) ? $cb->trademark : '',
                        'Product Category' => isset($cb->product_category_detail) ? $cb->product_category_detail : '',
                        'Cust.Size' => $row->customer_size,
                        'Man.Size' => $row->manufacturing_size,
                        'Dimension (L x W x H (satuan))' => $dimension,
                        'Qty' => $row->inner_pack,
                        'Location' => $loc,
                        'Checkin' => $row->checkin,
                        'Date (d/m/y)' => $current_date,
                        'Remarks' => $info_order
                    ];
            });
        } else {
            return response()->json('no data', 422);
        }
    }



    public function stockCancel()
    {
        $factory = DB::table('factory')
            ->select('factory_id', 'factory_name')
            ->wherenull('deleted_at')
            ->get();

        return view('report/stock/stock_cancel')->with('factory', $factory);
    } #stockCancel


    public function getDatastockCancel(Request $request)
    {
        $factory_id = $request->factory_id;
        $data = DB::table('package')
            ->select(
                'package.barcode_id',
                'package_detail.inner_pack',
                'package_detail.customer_size',
                'package_detail.length',
                'package_detail.width',
                'package_detail.height',
                'package_detail.unit_2',
                'package_detail.is_cancel_order',
                'po_summary.po_number',
                'po_summary.upc',
                'package.created_at',
                'package.updated_at',
                'package_movements.barcode_package',
                'package_movements.created_at AS checkin',
                'locators.code',
                'package_detail.manufacturing_size'
            )
            ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
            // ->join('po_summary', 'package_detail.po_number','=','po_summary.po_number')
            ->join('po_summary', function ($join) {
                $join->on('package_detail.po_number', '=', 'po_summary.po_number');
                $join->on('package_detail.plan_ref_number', '=', 'po_summary.plan_ref_number');
            })
            ->leftjoin('package_movements', 'package_movements.barcode_package', '=', 'package.barcode_id')
            ->leftjoin('locators', 'locators.barcode', '=', 'package_movements.barcode_locator')
            ->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('current_department', 'inventory')
                        ->where('current_status', 'completed');
                })
                    ->orWhere(function ($query) {
                        $query->where('current_department', 'shipping')
                            ->where('current_status', 'onprogress');
                    });
            })
            ->where('package_detail.factory_id', $factory_id)
            ->where('package_detail.is_cancel_order', true)
            ->where('is_canceled', false)
            ->whereNull('package.deleted_at')
            ->whereNull('package_detail.deleted_at')
            ->whereNull('po_summary.deleted_at')
            ->whereNull('package_movements.deleted_at')
            ->groupBy(
                'package.barcode_id',
                'package_detail.inner_pack',
                'package_detail.customer_size',
                'package_detail.length',
                'package_detail.width',
                'package_detail.height',
                'package_detail.unit_2',
                'package_detail.is_cancel_order',
                'po_summary.po_number',
                'po_summary.upc',
                'locators.code',
                'package.created_at',
                'package.updated_at',
                'package_movements.created_at',
                'package_movements.barcode_package',
                'package_detail.manufacturing_size'
            )
            ->orderBy('po_summary.po_number', 'asc')
            ->orderBy('package.created_at', 'asc');

        return Datatables::of($data)
            ->editColumn('dimension', function ($data) {
                return $data->length . 'x' . $data->width . 'x' . $data->height . ' (' . $data->unit_2 . ')';
            })
            ->editColumn('code', function ($data) {
                if (empty($data->code)) {
                    return 'LOADING';
                } else {
                    return $data->code;
                }
            })
            ->make(true);
    } #getDatastockCancel

    public function exportStockCancel(Request $request)
    {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $current_date = date_format(Carbon::now(), 'd/m/Y');

        //naming file
        $get_factory = DB::table('factory')
            ->where('factory_id', $factory_id)
            ->wherenull('deleted_at')
            ->first();

        $filename = $get_factory->factory_name . '_report_stock_cancel_' . $current_date
            . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        $i = 1;
        $data = DB::table('package')
            ->select(
                'package.barcode_id',
                'package_detail.inner_pack',
                'package_detail.customer_size',
                'package_detail.length',
                'package_detail.width',
                'package_detail.height',
                'package_detail.unit_2',
                'package_detail.is_cancel_order',
                'po_summary.po_number',
                'po_summary.upc',
                'package.created_at',
                'package.updated_at',
                'package_movements.barcode_package',
                'package_movements.created_at AS checkin',
                'locators.code',
                'package_detail.manufacturing_size',
                'package_detail.buyer_item',
                'po_summary.trademark'
            )
            ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
            // ->join('po_summary', 'package_detail.po_number','=','po_summary.po_number')
            ->join('po_summary', function ($join) {
                $join->on('package_detail.po_number', '=', 'po_summary.po_number');
                $join->on('package_detail.plan_ref_number', '=', 'po_summary.plan_ref_number');
            })
            ->leftjoin('package_movements', 'package_movements.barcode_package', '=', 'package.barcode_id')
            ->leftjoin('locators', 'locators.barcode', '=', 'package_movements.barcode_locator')
            ->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('current_department', 'inventory')
                        ->where('current_status', 'completed');
                })
                    ->orWhere(function ($query) {
                        $query->where('current_department', 'shipping')
                            ->where('current_status', 'onprogress');
                    });
            })
            ->where('package_detail.factory_id', $factory_id)
            ->where('package_detail.is_cancel_order', true)
            ->where('is_canceled', false)
            ->whereNull('package.deleted_at')
            ->whereNull('package_detail.deleted_at')
            ->whereNull('po_summary.deleted_at')
            ->whereNull('package_movements.deleted_at')
            ->groupBy(
                'package.barcode_id',
                'package_detail.inner_pack',
                'package_detail.customer_size',
                'package_detail.length',
                'package_detail.width',
                'package_detail.height',
                'package_detail.unit_2',
                'package_detail.is_cancel_order',
                'po_summary.po_number',
                'po_summary.upc',
                'locators.code',
                'package.created_at',
                'package.updated_at',
                'package_movements.created_at',
                'package_movements.barcode_package',
                'package_detail.manufacturing_size',
                'package_detail.buyer_item',
                'po_summary.trademark'
            );


        //jika filterby tidak kosong
        if (!empty($filterby)) {
            $data = $data->where(function ($query) use ($filterby) {
                $query->where('package.barcode_id', 'like', '%' . $filterby . '%')
                    ->orWhere('po_summary.po_number', 'like', '%' . $filterby . '%')
                    ->orWhere('package_detail.customer_size', 'like', '%' . $filterby . '%')
                    ->orWhere('locators.code', 'like', '%' . $filterby . '%');
            });
        }

        //jika orderby tidak undefined
        if ($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        } else {
            $data = $data->orderBy('locators.code', 'asc')
                ->orderBy('po_summary.po_number', 'asc');
        }

        $export = \Excel::create($filename, function ($excel) use ($data, $i, $current_date) {
            $excel->sheet('report', function ($sheet) use ($data, $i, $current_date) {
                $sheet->appendRow(array(
                    '#', 'Barcode ID', 'Style', 'Art. No', 'PO Number', 'Brand', 'Product Ctg.', 'Cust.Size', 'Man.Size', 'Dimension (L x W x H (satuan))', 'Qty', 'Location', 'Checkin', 'Date (d/m/y)'
                ));
                $data->chunk(100, function ($rows) use ($sheet, $i, $current_date) {

                    foreach ($rows as $row) {
                        $product_ctg_sum = DB::table('po_summary_sync')->select('product_category_detail')->where('po_number', $row->po_number)->first();

                        $product_ctg_mas = DB::table('po_master_sync')->select('product_category_detail')->where('po_number', $row->po_number)->first();
                        //dimension
                        $length = $row->length * 1000;
                        $width = $row->width * 1000;
                        $height = $row->height * 1000;
                        $dimension = $length . 'x' . $width . 'x' . $height . ' (MM)';

                        $sheet->appendRow(array(
                            $i++, '="' . $row->barcode_id . '"', $row->upc, $row->buyer_item, $row->po_number, $row->trademark, !empty($product_ctg_mas->product_category_detail) ? $product_ctg_mas->product_category_detail : $product_ctg_sum->product_category_detail, $row->customer_size, $row->manufacturing_size, $dimension, $row->inner_pack, !empty($row->code) ? $row->code : 'LOADING', $row->checkin, $current_date
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    } #exportStockCancel


    // stock balance acc by naufal
    public function stockBalanceAcc()
    {
        $factory = DB::table('factory')
            ->select('factory_id', 'factory_name')
            ->wherenull('deleted_at')
            ->get();
        return view('report/stock/stock_balance_acc')->with('factory', $factory);
    }

    public function getStockBalanceAcc(Request $request)
    {
        $factory_id = $request->factory_id;

        if ($request->radio_status != 'current') {

            $date_r = Carbon::createFromFormat('d F, Y', $request->date_range)->format('Y-m-d');


            $range = array(
                'from' => date_format(date_create($date_r), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_r), 'Y-m-d 23:59:59')
            );



            $data = DB::table('end_balance')
                ->select(DB::raw('DISTINCT ON (barcode_id) barcode_id, upc, po_number, customer_size, manufacturing_size, inner_pack, length, width, height, unit_2, created_at, updated_at, checkin, code,  current_department, current_status, factory_id, date_balance, description'))
                ->where(function ($query) use ($range) {
                    $query->whereBetween('date_balance', [$range['from'], $range['to']]);
                })
                ->where('factory_id', $factory_id)
                ->where(function ($query) {
                    $query->where('current_department', 'inventory')
                        ->orWhere('current_department', 'qcinspect')
                        ->orWhere('current_department', 'shipping');
                })
                ->orderBy('barcode_id', 'asc');
        } else {



            $data = DB::table('v_ns_current_balance')
                ->where('factory_id', $factory_id)
                ->where(function ($query) {
                    $query->where('current_department', 'inventory')
                        ->orWhere('current_department', 'qcinspect')
                        ->orWhere('current_department', 'shipping');
                })
                ->orderBy('po_number', 'asc')
                ->orderBy('created_at', 'asc')
                ->orderBy('barcode_id', 'asc');
        }

        return Datatables::of($data)
            ->addColumn('art_no', function ($data) {
                $art = DB::table('package_detail')
                    ->select('buyer_item')
                    ->join('package', 'package.scan_id', '=', 'package_detail.scan_id')
                    ->where('package.barcode_id', $data->barcode_id)
                    ->whereNull('package.deleted_at')->first();
                return isset($art->buyer_item)? $art->buyer_item : '-';
            })
            ->addColumn('brand', function ($data) {
                $brd = DB::table('po_summary')
                    ->select('trademark')
                    ->where('po_number', $data->po_number)
                    ->whereNull('deleted_at')->first();

                return isset($brd->trademark)?$brd->trademark : '-';
            })
            ->editColumn('dimension', function ($data) {
                return $data->length . 'x' . $data->width . 'x' . $data->height . ' (' . $data->unit_2 . ')';
            })
            ->editColumn('code', function ($data) {
                if ($data->current_department == 'inventory') {
                    if ($data->current_status == 'completed') {
                        return $data->code;
                    } else {
                        return 'Metal Detector';
                    }
                } elseif ($data->current_department == 'shipping') {
                    return 'LOADING';
                } elseif ($data->current_department == 'qcinspect') {
                    return 'QC Inspect';
                } elseif ($data->current_department == 'sewing') {
                    return $data->description != null ? $data->description : 'LINE';
                }
            })
            ->addColumn('status_inspection', function ($data) {
                $package = DB::table('package')->where('barcode_id', $data->barcode_id)->first();
                $po_number = DB::table('package_detail')->where('scan_id', $package->scan_id)->pluck('po_number');
                $scan_id = DB::table('package_detail')->whereIn('po_number', $po_number)->pluck('scan_id');
                $package = DB::table('package')->whereIn('scan_id', $scan_id)->where('status_qcinspect', 'completed')->exists();
                if ($package) {
                    return 'Yes';
                }
                return 'No';
            })
            ->addColumn('inventory_in',function($data){
                return DB::table('package_movements')->where('barcode_package',$data->barcode_id)->where('department_to', 'inventory')->orderBy('created_at','asc')->first()->created_at ?? '-';
            })
            ->addColumn('tgl_inspection', function ($data) {
                $package = DB::table('package')->where('barcode_id', $data->barcode_id)->first();
                // return $package->scan_id;
                $po_number = DB::table('package_detail')->where('scan_id', $package->scan_id)->pluck('po_number');
                // return $po_number;
                $scan_id = DB::table('package_detail')->whereIn('po_number', $po_number)->pluck('scan_id');
                // return $scan_id;
                $barcode_id = DB::table('package')->whereIn('scan_id', $scan_id)->pluck('barcode_id');
                // return $barcode_id;
                $package_movement = DB::table('package_movements')->whereIn('barcode_package', $barcode_id)->where('department_from', 'qcinspect')->max('created_at');
                return $package_movement;


                // $scan_id = DB::table('package')->where('barcode_id', $data->barcode_id)->first();
                // $po_number = DB::table('package_detail')->where('scan_id', $package->scan_id)->first();
                // $scan_id = DB::table('package_detail')->where('po_number', $po_number->po_number)->pluck('scan_id');
                // $barcode_id = DB::table('package')->whereIn('scan_id', $scan_id)->where('status_qcinspect', 'completed')->pluck('barcode_id');
                // $package_movement = DB::table('package_movements')->whereIn('barcode_package', $barcode_id)->where('department_from', 'qcinspect')->where('status_to', 'completed')->max('created_at');

                // return $package_movement;

            })
            ->addColumn('date_balance', function ($data) {
                if (isset($data->date_balance)) {
                    return Carbon::parse($data->date_balance)->format('d/m/Y');
                } else {
                    return Carbon::now()->format('d/m/Y');
                }
            })
            ->make(true);
    }

    public function exportStockBalanceAcc(Request $request)
    {
        ini_set('max_execution_time', -1);
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $current_date = date_format(Carbon::now(), 'd/m/Y');

        if ($request->radio_status != 'current') {


            $date_r = Carbon::createFromFormat('d F, Y', $request->date_range)->format('Y-m-d');
            $range = array(
                'from' => date_format(date_create($date_r), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_r), 'Y-m-d 23:59:59')
            );

            $barcode_move = DB::table('adn_barcode_is_move_fgms_new')->pluck('barcode_id');
            // return $barcode_move;
            $data = DB::table('end_balance')
                ->select(DB::raw('DISTINCT ON (barcode_id) barcode_id, upc, po_number, customer_size, manufacturing_size, inner_pack, length, width, height, unit_2, created_at, updated_at, checkin, code, current_department, current_status, factory_id, date_balance, description'))
                ->where(function ($query) use ($range) {
                    $query->whereBetween('date_balance', [$range['from'], $range['to']]);
                })
                ->where('factory_id', $factory_id)
                ->where(function ($query) {
                    $query->where('current_department', 'inventory')
                        ->orWhere('current_department', 'qcinspect')
                        ->orWhere('current_department', 'shipping');
                })
                ->whereNotIn('barcode_id', $barcode_move)
                // ->get();
                ->orderBy('barcode_id', 'asc');
                // return count($data);

            if (!empty($filterby)) {
                $data = $data->where(function ($query) use ($filterby) {
                    $query->where('barcode_id', 'like', '%' . $filterby . '%')
                        ->orWhere('po_number', 'like', '%' . $filterby . '%')
                        ->orWhere('customer_size', 'like', '%' . $filterby . '%');
                    // ->orWhere('code', 'like', '%'.$filterby.'%');
                });
            }

            //jika orderby tidak undefined
            if ($orderby != 'undefined') {
                $data = $data->orderBy($orderby, $direction);
            } else {
                $data = $data->orderBy('po_number', 'asc');
            }
        } else {


            $data = DB::table('v_ns_current_balance')
                ->where('factory_id', $factory_id)
                ->where(function ($query) {
                    $query->where('current_department', 'inventory')
                        ->orWhere('current_department', 'qcinspect')
                        ->orWhere('current_department', 'shipping');
                })
                ->orderBy('barcode_id', 'asc');

            if (!empty($filterby)) {
                $data = $data->where(function ($query) use ($filterby) {
                    $query->where('barcode_id', 'like', '%' . $filterby . '%')
                        ->orWhere('po_number', 'like', '%' . $filterby . '%')
                        ->orWhere('customer_size', 'like', '%' . $filterby . '%');
                    // ->orWhere('code', 'like', '%'.$filterby.'%');
                });
            }

            //jika orderby tidak undefined
            if ($orderby != 'undefined') {
                $data = $data->orderBy($orderby, $direction);
            } 
        }


        //jika filterby tidak kosong


        //naming file
        $get_factory = DB::table('factory')
            ->where('factory_id', $factory_id)
            ->wherenull('deleted_at')
            ->first();

        if ($request->radio_status != 'current') {
            $c_date = Carbon::parse($request->date_range)->format('d_m_Y');
        } else {
            $c_date = date_format(Carbon::now(), 'd_m_Y');
        }

        $filename = $get_factory->factory_name . '_stock_balance_' . $c_date
            . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        $i = 1;
        $tamp = collect();





        // by naufal_1
        $data_result = $data->chunk(1000, function ($chunk) use ($tamp, $i) {
            foreach ($chunk as $result) {
                $result->no = $i++;
                $tgl_inspection = DB::table('v_ad_check_status_inspection')->where('barcode_id', $result->barcode_id)->first();
                $result->inventory_in =  DB::table('package_movements')->where('barcode_package',$result->barcode_id)->where('department_to', 'inventory')->orderBy('created_at','asc')->first()->created_at ?? '-';
                if (isset($tgl_inspection) && $tgl_inspection->tgl_inspection != '' && $tgl_inspection->tgl_inspection != null) {
                    $result->status_inspection = 'Yes';
                    $result->tgl_inspection = $tgl_inspection->tgl_inspection;
                } else {
                    $result->status_inspection = 'No';
                    $result->tgl_inspection = NULL;
                }
                $length = $result->length * 1000;
                $width = $result->width * 1000;
                $height = $result->height * 1000;
                $dimension = $length . 'x' . $width . 'x' . $height . ' (MM)';

                $cb = DB::table('v_ns_barcode_balance')->where('barcode_id', $result->barcode_id)->first();

                if (isset($cb->is_cancel_order) && $cb->is_cancel_order == true) {
                    $info_order = 'Cancel';
                } else {
                    $info_order = 'Active';
                }

                if ($result->current_department == 'inventory' && ($result->current_status == 'onprogress' || $result->current_status == 'rejected')) {
                    $loc = 'Metal Detector';
                } else if ($result->current_department == 'inventory' && $result->current_status == 'completed') {
                    $loc = $result->code;
                } else if ($result->current_department == 'qcinspect') {
                    $loc = 'QC Inspect';
                } else if ($result->current_department == 'shipping') {
                    $loc = 'LOADING';
                } else if ($result->current_department == 'sewing') {
                    $loc = $result->description != null ? $result->description : "";
                }
                $result->dimension = $dimension;
                $result->remark = $info_order;
                $result->location = $loc;
                $result->art_no = isset($cb->buyer_item) ? $cb->buyer_item : '';
                $result->brand = isset($cb->trademark) ? $cb->trademark : '';
                $result->product_category = isset($cb->product_category_detail) ? $cb->product_category_detail : '';


                // return response()->json($result);

                $tamp = $tamp->push($result);
            }
        });
        // $data_result = $data->get();

        // $all_barcode = collect($data_result)->pluck('barcode_id');
        // $all_inspection = DB::table('v_ad_check_status_inspection')->whereIn('barcode_id', $all_barcode)->get();
        // $all_cb = DB::table('v_ns_barcode_balance')->whereIn('barcode_id', $all_barcode)->get();

        // $data_chunk = array_chunk($data_result->toArray(), 1000);


        // $tamp = collect();
        // foreach ($data_chunk as $key => $chunk) {
        //     foreach ($chunk as $result) {
        //         $result->no = $i++;
        //         $tgl_inspection = DB::table('v_ad_check_status_inspection')->where('barcode_id', $result->barcode_id)->first();
        //         if (isset($tgl_inspection) && $tgl_inspection->tgl_inspection != '' && $tgl_inspection->tgl_inspection != null) {
        //             $result->status_inspection = 'Yes';
        //             $result->tgl_inspection = $tgl_inspection->tgl_inspection;
        //         } else {
        //             $result->status_inspection = 'No';
        //             $result->tgl_inspection = NULL;
        //         }
        //         $length = $result->length * 1000;
        //         $width = $result->width * 1000;
        //         $height = $result->height * 1000;
        //         $dimension = $length . 'x' . $width . 'x' . $height . ' (MM)';

        //         $cb = DB::table('v_ns_barcode_balance')->where('barcode_id', $result->barcode_id)->first();

        //         if (isset($cb->is_cancel_order) && $cb->is_cancel_order == true) {
        //             $info_order = 'Cancel';
        //         } else {
        //             $info_order = 'Active';
        //         }

        //         if ($result->current_department == 'inventory' && ($result->current_status == 'onprogress' || $result->current_status == 'rejected')) {
        //             $loc = 'Metal Detector';
        //         } else if ($result->current_department == 'inventory' && $result->current_status == 'completed') {
        //             $loc = $result->code;
        //         } else if ($result->current_department == 'qcinspect') {
        //             $loc = 'QC Inspect';
        //         } else if ($result->current_department == 'shipping') {
        //             $loc = 'LOADING';
        //         } else if ($result->current_department == 'sewing') {
        //             $loc = $result->description != null ? $result->description : "";
        //         }
        //         $result->dimension = $dimension;
        //         $result->remark = $info_order;
        //         $result->location = $loc;
        //         $result->art_no = isset($cb->buyer_item) ? $cb->buyer_item : '';
        //         $result->brand = isset($cb->trademark) ? $cb->trademark : '';
        //         $result->product_category = isset($cb->product_category_detail) ? $cb->product_category_detail : '';


        //         // return response()->json($result);

        //         $tamp = $tamp->push($result);
        //     }
        //     // if($key == 0){
        //     //     break;
        //     // }

        // }
        // return $tamp;
        $data_result = $tamp;

        if ($data_result->count() > 0) {
            return (new FastExcel($data_result))->download($filename . '.xls', function ($row) use ($i, $current_date) {
                // $length = $row->length * 1000;
                // $width = $row->width * 1000;
                // $height = $row->height * 1000;
                // $dimension = $length . 'x' . $width . 'x' . $height . ' (MM)';

                // $cb = DB::table('v_ns_barcode_balance')->where('barcode_id', $row->barcode_id)->first();

                // if (isset($cb->is_cancel_order) && $cb->is_cancel_order == true) {
                //     $info_order = 'Cancel';
                // } else {
                //     $info_order = 'Active';
                // }

                // if ($row->current_department == 'inventory' && ($row->current_status == 'onprogress' || $row->current_status == 'rejected')) {
                //     $loc = 'Metal Detector';
                // } else if ($row->current_department == 'inventory' && $row->current_status == 'completed') {
                //     $loc = $row->code;
                // } else if ($row->current_department == 'qcinspect') {
                //     $loc = 'QC Inspect';
                // } else if ($row->current_department == 'shipping') {
                //     $loc = 'LOADING';
                // } else if ($row->current_department == 'sewing') {
                //     $loc = $row->description != null ? $row->description : "";
                // }


                return
                    [
                        '#' => $row->no,
                        'Barcode ID' => $row->barcode_id,
                        'Style' => $row->upc,
                        'Art. No.' => $row->art_no,
                        'PO Number' => $row->po_number,
                        'Brand' => $row->brand,
                        'Product Category' => $row->product_category,
                        'Cust.Size' => $row->customer_size,
                        'Man.Size' => $row->manufacturing_size,
                        'Dimension (L x W x H (satuan))' => $row->dimension,
                        'Qty' => $row->inner_pack,
                        'Location' => $row->location,
                        'Checkin' => $row->checkin,
                        'Inventory In' => $row->inventory_in,
                        'Date (d/m/y)' => $current_date,
                        'Remarks' => $row->remark,
                        'Status Inspection' => $row->status_inspection,
                        'Tanggal Inspection' => $row->tgl_inspection
                    ];
            });
        } else {
            return response()->json('no data', 422);
        }
    }
}
