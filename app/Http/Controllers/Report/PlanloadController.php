<?php

namespace App\Http\Controllers\Report;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;
use Rap2hpoutre\FastExcel\FastExcel;
use Excel;


class PlanloadController extends Controller
{
    public function reportInvoice(){
    	$factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();
    	return view('report/planload/report_invoice')->with('factory',$factory);
    }

    public function getReportInvoice(Request $request){
    	$radio = $request->radio;
        $po_number = $request->po_number;
        $invoice = $request->invoice;
        $exfact = $request->exfact;
        $podd = $request->podd;
        $factory = $request->factory_id;

        $date_exfact = explode('-', preg_replace('/\s+/', '', $request->exfact));
        $range_exfact = array(
            'from' => date_format(date_create($date_exfact[0]), 'Y-m-d'),
            'to' => date_format(date_create($date_exfact[1]), 'Y-m-d')
        );

        $date_podd = explode('-', preg_replace('/\s+/', '', $request->podd));
        $range_podd = array(
            'from' => date_format(date_create($date_podd[0]), 'Y-m-d'),
            'to' => date_format(date_create($date_podd[1]), 'Y-m-d')
        );
     
        $data = DB::table('v_ns_draf_planload')
                ->where('factory_check',$factory)
                ->where('invoice','<>',null);

        switch ($radio) {
            case 'invoice':
                    $data = $data->where('invoice',$invoice);
                break;
            case 'po':
                    $data = $data->where('po_number',$po_number);
                break;
            case 'plan':
                    $data = $data->whereBetween('plan_stuffing',[$range_exfact['from'],$range_exfact['to']]);
                break;
            case 'pdcheck':
            		$data = $data->whereBetween('podd_check',[$range_podd['from'],$range_podd['to']]);
                break;
        }

        $data = $data->orderby('plan_stuffing','asc')
                        ->orderby('invoice','asc')
                        ->orderby('po_number','asc');

        return DataTables::of($data)
                ->addColumn('so',function($data){
                    $so = DB::table('invoice')
                                ->where('invoice',$data->invoice)
                                ->wherenull('deleted_at')
                                ->first();

                    return $so->so;
                })
                ->addColumn('remark',function($data){
                    $rcont = DB::table('invoice')
                                ->where('invoice',$data->invoice)
                                ->wherenull('deleted_at')
                                ->first();

                    return $rcont->remark_cont;
                })
                ->rawColumns(['so','remark'])
                ->make(true);
    }



    public function exportReportInvoice2(Request $request){
        $radio = $request->radio;
        $po_number = $request->po_number;
        $invoice = $request->invoice;
        $exfact = $request->exfact;
        $podd = $request->podd;
        $factory = $request->factory;
        $setname='';
     
        $date_exfact = explode('-', preg_replace('/\s+/', '', $request->exfact));
        $range_exfact = array(
            'from' => date_format(date_create($date_exfact[0]), 'Y-m-d'),
            'to' => date_format(date_create($date_exfact[1]), 'Y-m-d')
        );

        $date_podd = explode('-', preg_replace('/\s+/', '', $request->podd));
        $range_podd = array(
            'from' => date_format(date_create($date_podd[0]), 'Y-m-d'),
            'to' => date_format(date_create($date_podd[1]), 'Y-m-d')
        );        

        $data = DB::table('v_ns_draf_planload')
                ->where('factory_check',$factory)
                ->where('invoice','<>',null);

        switch ($radio) {
            case 'invoice':
                    $data = $data->where('invoice',$invoice);
                    $setname = "search_by_invoice_".$invoice;
                break;
            case 'po':
                    $data = $data->where('po_number',$po_number);
                    $setname = "search_by_po_number_".$po_number;
                break;
            case 'plan':
                    $data = $data->whereBetween('plan_stuffing',[$range_exfact['from'],$range_exfact['to']]);
                    $setname = "search_by_plan_stuffing_".$range_exfact['from']."-".$range_exfact['to'];
                break;
            case 'pdcheck':
                    $data = $data->whereBetween('podd_check',[$range_podd['from'],$range_podd['to']]);
                    $setname = "search_by_plan_podd_check_".$range_podd['from']."-".$range_podd['to'];
                break;
        }
        
        $data = $data->orderby('plan_stuffing','asc')
                        ->orderby('fwd','asc')
                        ->orderby('invoice','asc')
                        ->orderby('po_number','asc')->get();


         $get_factory = DB::table('factory')
                        ->where('factory_id', $factory)
                        ->wherenull('deleted_at')
                        ->first();
       
        $filename = $get_factory->factory_name.'_report_invoice_'.$setname;
  
        return Excel::create($filename,function($excel) use ($data){
                $excel->sheet('active',function($sheet) use ($data){
                    $sheet->setCellValue('A1','#');
                    $sheet->setCellValue('B1','Plan Stuffing');
                    $sheet->setCellValue('C1','Style');
                    $sheet->setCellValue('D1','PO Number');
                    $sheet->setCellValue('E1','CRD');
                    $sheet->setCellValue('F1','PSDD');
                    $sheet->setCellValue('G1','MDD');
                    $sheet->setCellValue('H1','Destination');
                    $sheet->setCellValue('I1','Cust. No.');
                    $sheet->setCellValue('J1','Shipmode');
                    $sheet->setCellValue('K1','Invoice');
                    $sheet->setCellValue('L1','FWD');
                    $sheet->setCellValue('M1','SO');
                    $sheet->setCellValue('N1','PODD');
                    $sheet->setCellValue('O1','CBM');
                    $sheet->setCellValue('P1','QTY');
                    $sheet->setCellValue('Q1','CTN');
                    $sheet->setCellValue('R1','GW');
                    $sheet->setCellValue('S1','NW');
                    $sheet->setCellValue('T1','Remark Exim');
                    $sheet->setCellValue('U1','Remark Container');
                    $sheet->setCellValue('V1','Remark Order');
                    $sheet->setCellValue('W1','Priority');
                    $sheet->setCellValue('X1','WHS');
                    $sheet->setCellValue('Y1','Subsidary');
                    $sheet->setCellValue('Z1','Art. No.');
                    $sheet->setCellValue('AA1','Order Type');              
                    $sheet->setCellValue('AB1','PSFD');
                    $sheet->setCellValue('AC1','LC Date');
                    $sheet->setCellValue('AD1','Plan Ref. No.');
                    $sheet->setCellValue('AE1','Factory');
                    
                    
                    

                    $i = 1;
                        
                    foreach ($data as $key => $dt) {
              
                        $inv = DB::table('invoice')->where('invoice',$dt->invoice)->wherenull('deleted_at')->first();
                        $rw = $i+1;

                        $sheet->setCellValue('A'.$rw,$i);
                        $sheet->setCellValue('B'.$rw,carbon::parse($dt->plan_stuffing)->format('d-M-Y'));
                        $sheet->setCellValue('C'.$rw,$dt->style);
                        $sheet->setCellValue('D'.$rw,$dt->po_number);
                        $sheet->setCellValue('E'.$rw,carbon::parse($dt->crd)->format('d-M-Y'));
                        $sheet->setCellValue('F'.$rw,carbon::parse($dt->po_stat_date_adidas)->format('d-M-Y'));
                        $sheet->setCellValue('G'.$rw,carbon::parse($dt->mdd)->format('d-M-Y'));
                        $sheet->setCellValue('H'.$rw,$dt->country_name);
                        $sheet->setCellValue('I'.$rw,$dt->customer_number);
                        $sheet->setCellValue('J'.$rw,$dt->shipmode);
                        $sheet->setCellValue('K'.$rw,$dt->invoice);
                        $sheet->setCellValue('L'.$rw,$dt->fwd);
                        $sheet->setCellValue('M'.$rw,$inv->so);
                        $sheet->setCellValue('N'.$rw,carbon::parse($dt->podd_check)->format('d-M-Y'));
                        $sheet->setCellValue('O'.$rw,(float)round($dt->cbm,3));
                        $sheet->setCellValue('P'.$rw,$dt->item_qty);
                        $sheet->setCellValue('Q'.$rw,$dt->ctn);
                        $sheet->setCellValue('R'.$rw,(float)round($dt->gross,3));
                        $sheet->setCellValue('S'.$rw,(float)round($dt->net,3));
                        $sheet->setCellValue('T'.$rw,$dt->remark_exim);
                        $sheet->setCellValue('U'.$rw,$inv->remark_cont);
                        $sheet->setCellValue('V'.$rw,$dt->remark_order);
                        $sheet->setCellValue('W'.$rw,$dt->priority_code);
                        $sheet->setCellValue('X'.$rw,$dt->whs_code);
                        $sheet->setCellValue('Y'.$rw,$dt->subsidary);
                        $sheet->setCellValue('Z'.$rw,$dt->buyer_item);
                        $sheet->setCellValue('AA'.$rw,$dt->order_type);
                        $sheet->setCellValue('AB'.$rw,carbon::parse($dt->psfd)->format('d-M-Y'));
                        $sheet->setCellValue('AC'.$rw,carbon::parse($dt->kst_lcdate)->format('d-M-Y'));
                        $sheet->setCellValue('AD'.$rw,'="'.$dt->plan_ref_number.'"');
                        $sheet->setCellValue('AE'.$rw,'="'.$dt->factory_id.'"');
                        
                        $i++;

                    }

                    $sheet->setColumnFormat(array(
                        'P'=>'0.00',
                        'S'=>'0.00',
                        'T'=>'0.00'
                    ));



                });
                $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }
    
    
}



