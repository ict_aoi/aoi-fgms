<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

use Rap2hpoutre\FastExcel\FastExcel;

class MetalController extends Controller
{
    public function metalReport(){
        $factory = DB::table('factory')->whereNull('deleted_at')->get();

        return view('report.metal.metal_finding')->with('factory',$factory);
    }

    public function ajaxGetData(Request $request){
        $factory_id = $request->factory_id;

        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );


        $data = DB::table('ns_metal_report_detail')->where('factory_id',$factory_id)->whereBetween('created_at',[$range['from'],$range['to']])->orderby('created_at','desc');

       

        return Datatables::of($data)
                            ->editColumn('created_at',function($data){
                                return date_format(date_create($data->created_at),'d-M-Y').'<br>'.date_format(date_create($data->created_at),'H:i:s');
                            })
                            ->editColumn('status',function($data){
                                // return strtoupper($data->status);

                                if ($data->status=='checkin') {
                                    return '<span class="badge badge-danger">CHECKIN</span>';
                                }else if ($data->status=='metal found') {
                                    return '<span class="badge badge-warning">Metal Found</span>';
                                }else if ($data->status=='pass') {
                                    return '<span class="badge badge-success">PASS</span>';
                                }else{
                                    return '<span class="badge badge-default">ERROR</span>';
                                }
                            })
                            ->addColumn('pack_plan',function($data){
                                $pp = DB::table('package_detail')->where('po_number',$data->po_number)->where('plan_ref_number',$data->plan_ref_number)->whereNull('deleted_at')->groupBy('po_number')->groupBy('plan_ref_number')->select(DB::raw('sum(item_qty) as qty'))->first();

                                return $pp->qty;
                            })
                            ->editColumn('operator',function($data){
                                return $this->getUser($data->operator);
                            })
                            ->editColumn('pso_verify',function($data){
                                return $this->getUser($data->pso_verify);
                            })
                            ->editColumn('pso_release',function($data){
                                return $this->getUser($data->pso_release);
                            })
                            ->editColumn('verify_date',function($data){
                              
                                return date_format(date_create($data->verify_date),'d-M-Y').'<br>'.date_format(date_create($data->verify_date),'H:i:s');
                            })
                            ->editColumn('release_date',function($data){
                         
                                return date_format(date_create($data->release_date),'d-M-Y').'<br>'.date_format(date_create($data->release_date),'H:i:s');
                            })
                            ->rawColumns(['pack_plan','created_at','status','oprator','pso_release','pso_verify','release_date','verify_date'])->make(true);
    }

    public function exportMetal(Request $request){
        $factory_id = $request->factory_id;
        $filter = trim($request->filterby);
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );


        $data = DB::table('ns_metal_report_detail')->where('factory_id',$factory_id)->whereBetween('created_at',[$range['from'],$range['to']])->orderby('created_at','desc');

        if (!empty($filter)) {
            $data = $data->where('po_number',$filter)
                            ->orwhere('upc',$filter)
                            ->orwhere('buyer_item',$filter)
                            ->orwhere('barcode_id',$filter)
                            ->orWhere('line_name',$filter)
                            ->orWhere('status',$filter);
        }

         $get_factory = DB::table('factory')
                        ->where('factory_id', $factory_id)
                        ->wherenull('deleted_at')
                        ->first();
        $i= 1;

        $filename = $get_factory->factory_name.'_report_metal_finding_' .$range['from'].'_'.$range['to'];

        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }

        if ($data_result->count() > 0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx', function ($row) use ($i) {
                        //dimension
                            
                         

                        $pp = DB::table('package_detail')
                                        ->join('po_summary',function($join){
                                            $join->on('package_detail.po_number','=','po_summary.po_number');
                                            $join->on('package_detail.plan_ref_number','=','po_summary.plan_ref_number');
                                        })->where('po_summary.po_number',$row->po_number)
                                        ->where('po_summary.plan_ref_number',$row->plan_ref_number)
                                        ->whereNull('po_summary.deleted_at')
                                        ->groupBy('po_summary.po_number')
                                        ->groupBy('po_summary.plan_ref_number')
                                        ->groupBy('po_summary.trademark')
                                        ->groupBy('po_summary.country')
                                        ->select('po_summary.po_number','po_summary.plan_ref_number','po_summary.trademark','po_summary.country',DB::raw('sum(package_detail.item_qty) as qty'))->first();
                        
                        return 
                                [
                                    '#' => $row->no, 
                                    'Metal Detector Date'=>date_format(date_create($row->created_at),'d-m-Y H:i:s'),
                                    'Barcode ID' => $row->barcode_id,
                                    'PO Number' => $row->po_number,
                                    'Buyer' => isset($pp->trademark) ? $pp->trademark : "",
                                    'Style' => $row->upc,
                                    'Product Category' => $row->product_category_detail,
                                    'Art. No.'=> $row->buyer_item, 
                                    'Destination'=>isset($pp->country) ? $pp->country : "", 
                                    'Package No.' => $row->package_number,
                                    'UCC No.'=> $row->uccno,
                                    'Cust.Size' => $row->customer_size, 
                                    'Man.Size' => $row->manufacturing_size, 
                                    'Qty Pack. Plan'=> isset($pp->qty) ? $pp->qty : 0,
                                    'Inner Pack' => $row->inner_pack,
                                    'Status'=>strtoupper($row->status),
                                    'Operator' => isset($row->operator) ? $this->getUser($row->operator) : "",
                                    'Verified By' => isset($row->pso_verify) ? $this->getUser($row->pso_verify) : "",
                                    'Verified At' => isset($row->verify_date) ? $row->verify_date : "",
                                    'Released By' => isset($row->pso_release) ? $this->getUser($row->pso_release) : "",
                                    'Released At' => isset($row->release_date) ? $row->release_date : "",
                                    'Metal Find.'=>$row->metfind,
                                    'Note'=>$row->note,
                                    'Location'=>$row->loct,
                                    'Find Qty'=>$row->qty,
                                    'Line'=>$row->line_name
                                    
                                ];
            });
         }else {
             return response()->json('no data', 422);
         }


    }


   

    private function getUser($id){
        $user = DB::table('users')->where('id',$id)->first();

        return $user->name;
    }
}
