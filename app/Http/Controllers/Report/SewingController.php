<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

use Rap2hpoutre\FastExcel\FastExcel;

class SewingController extends Controller
{
    public function index() {
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('report/sewing/index')->with('factory', $factory);
    }

    public function getDataSewing(Request $request) {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('v_report_sewing')
                ->select(DB::raw('v_report_sewing.*, po_summary.upc'))
                ->join('po_summary', function ($join)
                {
                    $join->on('po_summary.po_number', '=', 'v_report_sewing.po_number')
                            ->on('po_summary.plan_ref_number', '=', 'v_report_sewing.plan_ref_number');
                })
                ->where('v_report_sewing.factory_id', $factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('v_report_sewing.checkin', [$range['from'], $range['to']])
                            ->orWhereBetween('v_report_sewing.completed', [$range['from'], $range['to']])
                            ->orWhereBetween('v_report_sewing.checkout', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('v_report_sewing.barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('v_report_sewing.po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('v_report_sewing.line', 'ilike', '%'.$filterby.'%')
                                ->orWhere('v_report_sewing.customer_size', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
               ->editColumn('dimension', function($data) {
                   $length = $data->length * 1000;
                   $width = $data->width * 1000;
                   $height = $data->height * 1000;
                   return $length . 'x' .$width . 'x' .$height . ' (MM)';
               })
               ->make(true);
    }

    public function exportSewing(Request $request) {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        //
        $data = DB::table('v_report_sewing')
                ->select(DB::raw('v_report_sewing.*, po_summary.upc'))
                ->join('po_summary', function ($join)
                {
                    $join->on('po_summary.po_number', '=', 'v_report_sewing.po_number')
                            ->on('po_summary.plan_ref_number', '=', 'v_report_sewing.plan_ref_number');
                })
                ->where('v_report_sewing.factory_id', $factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('v_report_sewing.checkin', [$range['from'], $range['to']])
                            ->orWhereBetween('v_report_sewing.completed', [$range['from'], $range['to']])
                            ->orWhereBetween('v_report_sewing.checkout', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('v_report_sewing.barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('v_report_sewing.po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('v_report_sewing.line', 'ilike', '%'.$filterby.'%')
                                ->orWhere('v_report_sewing.customer_size', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('v_report_sewing.checkin', 'asc')
                        ->orderBy('v_report_sewing.completed', 'asc')
                        ->orderBy('v_report_sewing.checkout', 'asc');
        }

        //naming file
        $get_factory = DB::table('factory')
                        ->where('factory_id', $factory_id)
                        ->wherenull('deleted_at')
                        ->first();

        $filename = $get_factory->factory_name. '_report_sewing_' . $range['from'] . '_until_' . $range['to']
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        $i = 1;

        // $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
        //     $excel->sheet('report', function($sheet) use($data, $i, $range) {
        //         $sheet->appendRow(array(
        //             '#', 'Barcode ID', 'Style', 'PO Number', 'Size', 'Dimension (L x W x H (satuan))',
        //             'Check In', 'Completed', 'Check Out', 'Qty', 'Line'
        //         ));
        //         $data->chunk(100, function($rows) use ($sheet, $i, $range)
        //         {
        //             foreach ($rows as $row)
        //             {
        //                 //dimension
        //                 $length = $row->length * 1000;
        //                 $width = $row->width * 1000;
        //                 $height = $row->height * 1000;
        //                 $dimension = $length . 'x' .$width . 'x' .$height . ' (MM)';

        //                 //
        //                 $sheet->appendRow(array(
        //                     $i++, '="'.$row->barcode_id.'"', $row->upc, $row->po_number, $row->customer_size, $dimension,
        //                     $row->checkin, $row->completed, $row->checkout, $row->inner_pack, $row->line
        //                 ));
        //             }
        //         });
        //     });
        // })->download('xls');
        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
         }
         if ($data_result->count() > 0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx', function ($row) use ($i) {
                        //dimension
                        $length = $row->length * 1000;
                        $width = $row->width * 1000;
                        $height = $row->height * 1000;
                        $dimension = $length . 'x' .$width . 'x' .$height . ' (MM)';
                        //
                        return 
                                [
                                    '#' => $row->no, 
                                    'Barcode ID' => $row->barcode_id, 
                                    'Style' => $row->upc, 
                                    'PO Number' => $row->po_number, 
                                    'Size' => $row->customer_size, 
                                    'Dimension (L x W x H (satuan))' => $dimension,
                                    'Check In' => $row->checkin, 
                                    'Completed' => $row->completed, 
                                    'Check Out' => $row->checkout, 
                                    'Qty' => $row->inner_pack, 
                                    'Line' => $row->line,
                                    'Remark'=>$row->description
                                ];
            });
         }else {
             return response()->json('no data', 422);
         }
    }



    // sewing completed
    public function sewingOut() {
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('report/sewing/list_out')->with('factory',$factory);
    }

    public function ajaxGetDataSewingOut(Request $request) {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('v_sewing_completed')
                ->select(DB::raw('v_sewing_completed.*, po_summary.upc, po_summary.trademark'))
                ->join('po_summary', function ($join)
                {
                    $join->on('po_summary.po_number', '=', 'v_sewing_completed.po_number')
                            ->on('po_summary.plan_ref_number', '=', 'v_sewing_completed.plan_ref_number');
                })
                ->where('v_sewing_completed.factory_id', $factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('v_sewing_completed.completed', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('v_sewing_completed.barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('v_sewing_completed.po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('v_sewing_completed.customer_size', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
               ->addColumn('brand',function($data){
                    // $brand = DB::table('po_summary')
                    //                 ->select('trademark')
                    //                 ->where('po_number',$data->po_number)
                    //                 ->where('plan_ref_number',$data->plan_ref_number)
                    //                 ->wherenull('deleted_at')
                    //                 ->first();
                    return $data->trademark;
               })
               ->editColumn('dimension', function($data) {
                   $length = $data->length * 1000;
                   $width = $data->width * 1000;
                   $height = $data->height * 1000;
                   return $length . 'x' .$width . 'x' .$height . ' (MM)';
               })
               ->make(true);
    }

    public function exportSewingOut(Request $request) {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        //
        $data = DB::table('v_sewing_completed')
                ->select(DB::raw('v_sewing_completed.*, po_summary.upc, po_summary.trademark'))
                ->join('po_summary', function ($join)
                {
                    $join->on('po_summary.po_number', '=', 'v_sewing_completed.po_number')
                            ->on('po_summary.plan_ref_number', '=', 'v_sewing_completed.plan_ref_number');
                })
                ->where('v_sewing_completed.factory_id', $factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('v_sewing_completed.completed', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('v_sewing_completed.barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('v_sewing_completed.po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('v_sewing_completed.customer_size', 'like', '%'.$filterby.'%');
                    });
        }

        $get_factory = DB::table('factory')
                        ->where('factory_id', $factory_id)
                        ->wherenull('deleted_at')
                        ->first();

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);

            //naming file
            $filename = $get_factory->factory_name.'_report_sewing_completed_' . $range['from'] . '_until_' . $range['to']
            . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        }
        else {
            $data = $data->orderBy('v_sewing_completed.completed', 'asc');

            //naming file
            $filename = $get_factory->factory_name.'_report_sewing_completed_' . $range['from'] . '_until_' . $range['to'];
        }

        
        $i = 1;

        // $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
        //     $excel->sheet('report', function($sheet) use($data, $i, $range) {
        //         $sheet->appendRow(array(
        //             '#', 'Barcode ID', 'PO Number', 'Size', 'Dimension (L x W x H (satuan))',
        //             'Completed', 'Item Qty'
        //         ));
        //         $data->chunk(100, function($rows) use ($sheet, $i, $range)
        //         {
        //             foreach ($rows as $row)
        //             {
        //                 //dimension
        //                 $length = $row->length * 1000;
        //                 $width = $row->width * 1000;
        //                 $height = $row->height * 1000;
        //                 $dimension = $length . 'x' .$width . 'x' .$height . ' (MM)';

        //                 //
        //                 $sheet->appendRow(array(
        //                     $i++, '="'.$row->barcode_id.'"', $row->po_number, $row->customer_size, $dimension,
        //                     $row->completed, $row->inner_pack
        //                 ));
        //             }
        //         });
        //     });
        // })->download('xlsx');

        // return response()->json('Success exporting', 200);
        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
         }
         if ($data_result->count() > 0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx', function ($row) use ($i) {
                        //dimension
                        $length = $row->length * 1000;
                        $width = $row->width * 1000;
                        $height = $row->height * 1000;
                        $dimension = $length . 'x' .$width . 'x' .$height . ' (MM)';
                        //
                        // $brand = DB::table('po_summary')
                        //                 ->select('trademark')
                        //                 ->where('po_number',$row->po_number)
                        //                 ->where('plan_ref_number',$row->plan_ref_number)
                        //                 ->wherenull('deleted_at')->first();

                        return 
                                [
                                    '#' => $row->no, 
                                    'Barcode ID' => $row->barcode_id,
                                    'PO Number' => $row->po_number, 
                                    'Style' => $row->upc, 
                                    'Brand'=>$row->trademark,
                                    'Art. No'=>$row->buyer_item,
                                    'Size' => $row->customer_size, 
                                    'Dimension (L x W x H (satuan))' => $dimension,
                                    'Completed' => $row->completed,
                                    'Qty' => $row->inner_pack,
                                    'Line'=> $row->line
                                ];
            });
         }else {
             return response()->json('no data', 422);
         }
    }
}
