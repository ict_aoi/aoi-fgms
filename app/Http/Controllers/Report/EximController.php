<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

class EximController extends Controller
{
    //
     // list delivery order
     public function listdeliveryorder() {
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('report/exim/list_delivery_orders')->with('factory', $factory);
    }

    public function getDataListDelivery(Request $request) {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $status = $request->radio_status;
        $po = $request->po;

        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $date_plan = explode('-', preg_replace('/\s+/', '', $request->date_plan));
        $range_plan = array(
            'from' => date_format(date_create($date_plan[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_plan[1]), 'Y-m-d 23:59:59')
        );

        // $data = DB::table('v_report_delivery_orders')
        //         ->where('factory_id', $factory_id)
        //         ->whereNull('deleted_at')
        //         ->where(function($query) use ($range) {
        //             $query->whereBetween('created_at', [$range['from'], $range['to']]);
        //         });

        $data = DB::table('v_report_delivery_orders')
                    ->where('factory_id',$factory_id)
                    ->wherenull('deleted_at');

            if ($status=='exfact') {
                $data = $data->where(function($query) use ($range){
                    $query->whereBetween('created_at',[$range['from'],$range['to']]);
                });
            } elseif ($status=='planstuffing') {
                $data = $data->where(function($query) use ($range_plan){
                    $query->whereBetween('plan_stuffing',[$range_plan['from'],$range_plan['to']]);
                });
            } elseif ($status=='pono') {
                 $data = $data->where('orderno',$po);
            }
            

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('invoice', 'like', '%'.$filterby.'%')
                                ->orWhere('orderno', 'like', '%'.$filterby.'%');
                    });
        }

        $data = $data->orderBy('invoice');

        return Datatables::of($data)
                ->editColumn('created_at', function($data){
                    return Carbon::parse($data->created_at)->format('d/m/Y');
                })
                ->addColumn('jam', function($data){
                    return Carbon::parse($data->created_at)->format('H:i');
                })
                ->editColumn('crd', function($data){
                    return Carbon::parse($data->crd)->format('d/m/Y');
                })
                ->editColumn('po_stat_date_adidas', function($data){
                    return Carbon::parse($data->po_stat_date_adidas)->format('d/m/Y');
                })
               ->make(true);
    }

    public function exportListDelivery(Request $request) {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        //
        $data = DB::table('v_report_delivery_orders')
                ->where('factory_id', $factory_id)
                ->whereNull('deleted_at')
                ->where(function($query) use ($range) {
                    $query->whereBetween('created_at', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('invoice', 'like', '%'.$filterby.'%')
                                ->orWhere('orderno', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('invoice', 'asc');
        }

        //naming file
        $get_factory = DB::table('factory')
                        ->where('factory_id', $factory_id)
                        ->wherenull('deleted_at')
                        ->first();

        $filename = $get_factory->factory_name.'_report_delivery_order' . $range['from'] . '_until_' . $range['to']
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
            $excel->sheet('report', function($sheet) use($data, $i, $range) {
                $sheet->appendRow(array(
                    '#', 'Ex-Fact Date', 'No.Truck', 'Nama Sopir', 'Jam', 'No.SJ', 'Style', 'Factory', 'Qty', 'CRD', 'PSDD', 'PO', 'Destination', 'cust.no', 'Shipmode', 'Invoice', 'FWD', 'SO', 'CTN'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i, $range)
                {
                    foreach ($rows as $row)
                    {
                        // date('d-m-Y', strtotime($row->request_at))
                        $sheet->appendRow(array(
                            $i++, Carbon::parse($row->created_at)->format('d/m/Y'), $row->nopol, $row->drivers_name, Carbon::parse($row->created_at)->format('H:i'), $row->delivery_number, $row->style, $row->factory_name, $row->new_qty, Carbon::parse($row->crd)->format('d/m/Y'), Carbon::parse($row->po_stat_date_adidas)->format('d/m/Y'), $row->orderno, $row->country_name, $row->customer_number, $row->shipmode, $row->invoice, $row->fwd, $row->so, $row->total_ctn
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }
}
