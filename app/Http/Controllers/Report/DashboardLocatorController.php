<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

class DashboardLocatorController extends Controller
{
    public function index() {
        $areas = DB::table('areas')
                    ->select('id', 'name')
                    ->where('factory_id', Auth::user()->factory_id)
                    ->whereNull('deleted_at')
                    ->get();
        return view('report/dashboardlocator/index')->with('areas', $areas);
    }

    public function getListLocator(Request $request) {
        $areaid = $request->areaid;
        $locators = DB::table('locators')
                        ->select('barcode', 'code', 'rack')
                        ->where('area_id', $areaid)
                        ->where('factory_id', Auth::user()->factory_id)
                        ->whereNull('deleted_at')
                        ->get();
        return view('report/dashboardlocator/list_locator')->with('locators', $locators);
    }

    public function getDataDashboardLocator(Request $request) {
        if(isset($request->locator)) {
            $locator = $request->locator;
            $data = DB::table('v_dashboard_locator')
                        ->where('factory_id', Auth::user()->factory_id)
                        ->where('rack', $locator);
            
        }
        else {
            $data = array();
        }

        return Datatables::of($data)
                        ->editColumn('actual_carton', function($data) {
                            return $data->actual_carton . '('.$data->actual_carton_all_rack.')';
                        })
                        ->editColumn('psdd', function($data) {
                            if (empty($data->psdd)) {
                                $psdd = '-';
                            }else{
                                $psdd = Carbon::createFromFormat('Y-m-d', $data->psdd);
                                $psdd = date_format($psdd, 'd/m/Y');
                            }
                            return $psdd;
                        })
                        ->editColumn('psd', function($data) {
                            $string = '';
                            if(empty($data->psd)) {
                                $string .= '<div id="psd_'.$data->po_number.'">
                                                <input type="date"
                                                    data-ponumber="'.$data->po_number.'"
                                                    data-psd="'.$data->psd.'"
                                                    class="psd_unfilled"
                                                    value="'.$data->psd.'">
                                                </input>
                                            </div>';
                            }
                            else {
                                $psd_db = date_format(date_create_from_format('Y-m-d H:i:s', $data->psd), 'Y-m-d');
                                $psd_view = date_format(date_create_from_format('Y-m-d H:i:s', $data->psd), 'd/m/Y');
                                $string .= '<div id="psd_'.$data->po_number.'">
                                                <span class="psd_filled" data-ponumber="'.$data->po_number.'" data-psd="'.$psd_db.'">
                                                '.$psd_view.'
                                                </span>
                                                </div>';
                            }
                            return $string;
                        })
                        ->editColumn('pfcd', function($data) {
                            if (empty($data->psdd)) {
                                $pfcd = '-';
                            }else{
                                $pfcd = Carbon::createFromFormat('Y-m-d', $data->psdd)->addDays(1);
                                $pfcd = date_format($pfcd, 'd/m/Y');
                            }
                            return $pfcd;
                        })
                        ->editColumn('balance', function($data) {
                            return $data->balance;
                        })
                        ->editColumn('remark', function($data) {
                            return $data->remark;
                        })
                        ->editColumn('final_inspect', function($data) {
                            if(empty($data->final_inspect)) {
                                $final_inspect = '-';
                            }
                            else {
                                $final_inspect = $data->final_inspect;
                            }
                            return $final_inspect;
                        })
                        ->rawColumns(['final_inspect', 'psd'])
                        // ->with('actual_sum', sum($data->actual_carton))
                        ->make(true);
    }

    public function exportDashboardLocator(Request $request) {
        if(isset($request->locator) && !empty($request->locator)) {
            $orderby = $request->orderby;
            $direction = $request->direction;
            $filterby = $request->filterby;
            $locator = $request->locator;
            $current_date = date_format(Carbon::now(), 'd/m/Y');
            $filename = 'report_dashboard_locator_' . $locator . '_' . $current_date;
            $i = 1;

            //
            $data = DB::table('v_dashboard_locator')
                        ->where('factory_id', Auth::user()->factory_id)
                        ->where('rack', $locator);

            //jika filterby tidak kosong
            if(!empty($filterby)) {
                $data = $data->where(function($query) use ($filterby) {
                            $query->where('po_number', 'like', '%'.$filterby.'%')
                                    ->orWhere('sharing_rack', 'like', '%'.$filterby.'%')
                                    ->orWhere('final_inspect', 'like', '%'.$filterby.'%');
                        });
            }

            //jika orderby tidak undefined
            if($orderby != 'undefined') {
                $data = $data->orderBy($orderby, $direction);
            }
            else {
                $data = $data->orderBy('po_number', 'asc');
            }
        }
        else {
            return response()->json('Please select rack', 422);
        }

        $export = \Excel::create($filename, function($excel) use ($data, $i) {
            $excel->sheet('report', function($sheet) use($data, $i) {
                $sheet->appendRow(array(
                    '#', 'PO', 'PSDD', 'PSD', 'PFCD',
                    'Need Carton', 'Actual Carton', 'Balance',
                    'Final Inspect', 'Remark', 'Rack', 'Sharing Rack'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i)
                {
                    foreach ($rows as $row)
                    {
                        if (empty($psdd)) {
                            $psdd = null;
                            $pfcd = null;
                        }else{
                            $psdd = date_format(Carbon::createFromFormat('Y-m-d', $row->psdd), 'd/m/Y');
                            $pfcd = date_format(Carbon::createFromFormat('Y-m-d', $row->psdd)->addDays(1), 'd/m/Y');
                        }

                        
                        if(empty($row->psd)) {
                            $psd = null;
                        }
                        else {
                            $psd = date_format(date_create($row->psd), 'd/m/Y');
                        }
                        $sheet->appendRow(array(
                            $i++, $row->po_number, $psdd,$psd,$pfcd, $row->need_carton,
                            $row->actual_carton. '('. $row->actual_carton_all_rack .')',
                            $row->balance, $row->final_inspect, $row->remark, $row->rack, $row->sharing_rack
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }

    public function updatePSD(Request $request) {
        $ponumber = $request->ponumber;
        if(empty($request->psd)) {
            $psd = null;
        }
        else {
            $psd =  date_format(date_create($request->psd), 'Y-m-d'); //for database
        }

        //update
        try {
            DB::table('po_summary')
                ->where('po_number', $ponumber)
                ->whereNull('deleted_at')
                ->update([
                    'psd' => $psd,
                    'updated_at' => Carbon::now()
                ]);
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }

        if(empty($psd)) {
            $html = '<input type="date"
                        data-ponumber="'.$ponumber.'"
                        data-psd="'.$psd.'"
                        class="psd_unfilled"
                        value="'.$psd.'">
                        </input>';
        }
        else {
            $html = '<span class="psd_filled"
                            data-ponumber="'.$ponumber.'"
                            data-psd="'.$psd.'">
                            '. date_format(date_create($request->psd), 'd/m/Y') .'
                            </span>';
        }

        return response()->json($html, 200);
    }

    // update invoice
     public function updateInvoice(Request $request) {
        $data = $request->data;

        if(empty($request->invoice)) {
            $invoice = null;
        }
        else {
            $invoice =  $request->invoice; //for database
        }

        //update
        try {
            foreach ($data as $key => $value) {
                DB::table('po_summary')
                ->where('po_number', $value['ponumber'])
                ->where('plan_ref_number', $value['planrefnumber'])
                ->where('factory_id', Auth::user()->factory_id)
                ->whereNull('deleted_at')
                ->update([
                    'invoice' => $invoice,
                    'updated_at' => Carbon::now()
                ]);
            }
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }


        // return response()->json($html, 200);
    }

    public function resultDashboard(Request $request) {
        $locator = $request->locator;
        $sum_rack = DB::table('v_dashboard_locator')
                        ->where('rack', $locator)
                        ->where('factory_id', Auth::user()->factory_id)
                        ->sum('actual_carton');


        return response()->json(['sum_rack' => $sum_rack]);
    }
}
