<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

class InventoryController extends Controller
{
    public function index() {
        return view('report/inventory/index');;
    }

    public function getDataInventory(Request $request) {
        $filterby = $request->filterby;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('v_report_inventory')
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkin', [$range['from'], $range['to']])
                            ->orWhereBetween('movement_date', [$range['from'], $range['to']])
                            ->orWhereBetween('checkout', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_size', 'like', '%'.$filterby.'%')
                                ->orWhere('location', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
               ->editColumn('dimension', function($data) {
                   $length = $data->length * 1000;
                   $width = $data->width * 1000;
                   $height = $data->height * 1000;
                   return $length . 'x' .$width . 'x' .$height . ' (MM)';
               })
               ->make(true);
    }

    public function exportInventory(Request $request) {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        //
        $data = DB::table('v_report_inventory')
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkin', [$range['from'], $range['to']])
                            ->orWhereBetween('movement_date', [$range['from'], $range['to']])
                            ->orWhereBetween('checkout', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_size', 'like', '%'.$filterby.'%')
                                ->orWhere('location', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('checkin', 'asc')
                        ->orderBy('movement_date', 'asc')
                        ->orderBy('checkout', 'asc');
        }

        //naming file
        $filename = 'report_inventory_' . $range['from'] . '_until_' . $range['to']
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
            $excel->sheet('report', function($sheet) use($data, $i, $range) {
                $sheet->appendRow(array(
                    '#', 'Barcode ID', 'PO Number', 'Size', 'Dimension (L x W x H (satuan))',
                    'Location', 'Check In', 'Movement Date', 'Check Out'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i, $range)
                {
                    foreach ($rows as $row)
                    {
                        //dimension
                        $length = $row->length * 1000;
                        $width = $row->width * 1000;
                        $height = $row->height * 1000;
                        $dimension = $length . 'x' .$width . 'x' .$height . ' (MM)';

                        //
                        $sheet->appendRow(array(
                            $i++, '="'.$row->barcode_id.'"', $row->po_number, $row->customer_size, $dimension,
                            $row->location, $row->checkin, $row->movement_date, $row->checkout
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }
}
