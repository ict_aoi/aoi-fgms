<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

class QcinspectController extends Controller
{
    public function index() {
        return view('report/qcinspect/index');;
    }

    public function getDataQcinspect(Request $request) {
        $filterby = $request->filterby;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('v_report_qcinspect')
                ->where('factory_id', Auth::user()->factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkin', [$range['from'], $range['to']])
                            ->orWhereBetween('completed', [$range['from'], $range['to']])
                            ->orWhereBetween('checkout', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_size', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
               ->addColumn('brand',function($data){
                    $brand = DB::table('po_summary')
                                    ->select('trademark')
                                    ->where('po_number',$data->po_number)
                                    ->where('plan_ref_number',$data->plan_ref_number)
                                    ->wherenull('deleted_at')
                                    ->first();
                    return $brand->trademark;
               })
               ->editColumn('dimension', function($data) {
                   $length = $data->length * 1000;
                   $width = $data->width * 1000;
                   $height = $data->height * 1000;
                   return $length . 'x' .$width . 'x' .$height . ' (MM)';
               })
               ->make(true);
    }

    public function exportQcinspect(Request $request) {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        //
        $data = DB::table('v_report_qcinspect')
                ->where('factory_id', Auth::user()->factory_id)
                ->where(function($query) use ($range) {
                    $query->whereBetween('checkin', [$range['from'], $range['to']])
                            ->orWhereBetween('completed', [$range['from'], $range['to']])
                            ->orWhereBetween('checkout', [$range['from'], $range['to']]);
                });

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('customer_size', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('checkin', 'asc')
                        ->orderBy('completed', 'asc')
                        ->orderBy('checkout', 'asc');
        }

        //naming file
        $filename = 'report_qcinspect_' . $range['from'] . '_until_' . $range['to']
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
            $excel->sheet('report', function($sheet) use($data, $i, $range) {
                $sheet->appendRow(array(
                    '#', 'Barcode ID', 'PO Number', 'Brand', 'Art. No', 'Size', 'Dimension (L x W x H (satuan))',
                    'Check In', 'Completed', 'Check Out'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i, $range)
                {
                    foreach ($rows as $row)
                    {
                        $brand = DB::table('po_summary')
                                        ->select('trademark')
                                        ->where('po_number',$row->po_number)
                                        ->where('plan_ref_number',$row->plan_ref_number)
                                        ->wherenull('deleted_at')->first();
                        //dimension
                        $length = $row->length * 1000;
                        $width = $row->width * 1000;
                        $height = $row->height * 1000;
                        $dimension = $length . 'x' .$width . 'x' .$height . ' (MM)';

                        //
                        $sheet->appendRow(array(
                            $i++, '="'.$row->barcode_id.'"', $row->po_number, $brand->trademark, $row->buyer_item, $row->customer_size, $dimension,
                            $row->checkin, $row->completed, $row->checkout
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }
}
