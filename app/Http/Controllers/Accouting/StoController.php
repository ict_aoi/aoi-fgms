<?php

namespace App\Http\Controllers\Accouting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

use Rap2hpoutre\FastExcel\FastExcel;

class StoController extends Controller
{
    public function scanSto(){
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('accounting/sto/scanSto')->with('factory', $factory);
    }

    public function getLocator(Request $request){
        $factory_id = $request->factory_id;

        // $data = DB::table('locators')->where('factory_id',$factory_id)->whereNull('deleted_at')->select('barcode','code')->get();

        $data = DB::table('areas')->join('locators','areas.id','=','locators.area_id')
                                    ->where('areas.factory_id',$factory_id)
                                    ->whereNull('areas.deleted_at')
                                    ->whereNull('locators.deleted_at')
                                    ->select('locators.code','locators.barcode')
                                    ->orderBy('locators.code','asc')->get();

        return response()->json(['locators'=>$data],200);
    }

    public function checkScan(Request $request){
        $barcode_id = $request->barcode_id;
        $locat = $request->locat;
        $factory_id = $request->factory_id;
        $remark = $request->remark;
        $dt_scan = array();
        $ip_address = $request->ip();

        try {
            $cekSto = $this->cekSto($barcode_id);
            
            if ($cekSto==false) {
                
                $data = DB::table('po_summary')
                            ->join('package_detail',function($join){
                                $join->on('po_summary.po_number','=','package_detail.po_number');
                                $join->on('po_summary.plan_ref_number','=','package_detail.plan_ref_number');
                            })
                            ->join('package','package_detail.scan_id','=','package.scan_id')
                            ->leftjoin('package_movements','package.barcode_id','=','package_movements.barcode_package')
                            ->leftjoin('line','package_movements.line_id','=','line.id')
                            ->where('package.barcode_id',$barcode_id)
                            ->where('po_summary.factory_id',$factory_id)
                            ->whereNull('po_summary.deleted_at')
                            ->whereNull('package_detail.deleted_at')
                            ->whereNull('package.deleted_at')
                            ->whereNull('package_movements.deleted_at')
                            ->where('package_movements.is_canceled',false)
                            ->select('po_summary.po_number','po_summary.plan_ref_number','po_summary.upc','po_summary.trademark','po_summary.factory_id','package_detail.buyer_item','package_detail.customer_size','package_detail.manufacturing_size','package_detail.inner_pack','package_detail.is_cancel_order','package.barcode_id','package.current_department','package.current_status','package_movements.barcode_locator','line.description','po_summary.season')->first();
                            // $this->cekSto($barcode_id);
                if (isset($data->barcode_id)) {
                    db::begintransaction();

                    switch ($locat) {
                        case 'metal':
                            $actual = "Metal Detector";
                            break;

                        case 'loading':
                            $actual = "Loading";
                            break;

                        case 'qcinspect':
                            $actual = "QC Inspect";
                            break;
                        
                        default:
                            $actual = $locat;
                            break;
                    }

                    if ($data->current_department=='inventory' && $data->current_status=='onprogress') {
                        $dt_locator = "Metal Detector";
                    }else if ($data->current_department=='inventory' && $data->current_status=='completed') {
                        $dt_locator = $data->barcode_locator;
                    }else if ($data->current_department=='qcinspect') {
                        $dt_locator = "QC Inspect";
                    }else if ($data->current_department=='shipping' && $data->current_status=='onprogress') {
                        $dt_locator = "Loading";
                    }else if ($data->current_department=='sewing') {
                        $dt_locator = $data->description;
                    }else if ($data->current_department=='preparation') {
                        $dt_locator = 'Preparation';
                    }

                    $scan = array(
                        'barcode_id'=>$data->barcode_id,
                        'po_number'=>$data->po_number,
                        'plan_ref_number'=>$data->plan_ref_number,
                        'upc'=>$data->upc,
                        'buyer_item'=>$data->buyer_item,
                        'customer_size'=>$data->customer_size,
                        'manufacturing_size'=>$data->manufacturing_size,
                        'inner_pack'=>$data->inner_pack,
                        'locator'=>$dt_locator,
                        'locator_sto'=>$actual,
                        'trademark'=>$data->trademark,
                        'remark'=>$remark,
                        'factory_id'=>$data->factory_id,
                        'user_id'=>Auth::user()->id,
                        'created_at'=>Carbon::now(),
                        'is_cancel_order'=>$data->is_cancel_order,
                        'ip_address'=>$ip_address,
                        'season'=>$data->season
                    );
                    $dt_scan[]=$scan;

                    DB::table('sto_accounting')->insert($scan);
                    db::commit();

                    return view('accounting/sto/ajax_list')->with('data',$dt_scan);
                }else{
                    $err = 'Barcode '.$barcode_id.' not found ! ! !';

                    return response()->json($err,422);
                }
            }else{
                $err = 'Barcode '.$barcode_id.' already STO ! ! !';

                return response()->json($err,422);
            }

        } catch (Exception $er) {
            DB::rollback();
            $message = $er->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }

        
        
    }


    private function cekSto($barcode_id){
        $now = date_format(Carbon::now(),'Y-m-d');
        $cek = DB::table('sto_accounting')->where('barcode_id',$barcode_id)->whereDate('created_at',$now)->whereNull('deleted_at')->exists();

        return $cek;
    }


    public function reportSto(){
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('accounting/sto/reportSto')->with('factory', $factory);
    }

    public function ajaxGetDataSto(Request $request){
        $factory_id = $request->factory_id;
        $filterby = $request->filterby;

        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('sto_accounting')
                ->where(function($query) use ($range){
                    $query->whereBetween('created_at',[$range['from'],$range['to']]);
                })
                ->where('factory_id',$factory_id)
                ->whereNull('deleted_at')
                ->orderBy('po_number','asc')
                ->orderBy('barcode_id','asc');

        if (!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby){
                $query->where('barcode_id','like','%'.$filterby.'%')
                        ->orWhere('po_number','like','%'.$filterby.'%')
                        ->orWhere('plan_ref_number','like','%'.$filterby.'%')
                        ->orWhere('upc','like','%'.$filterby.'%')
                        ->orWhere('buyer_item','like','%'.$filterby.'%')
                        ->orWhere('trademark','like','%'.$filterby.'%');
            });
        }

        return DataTables::of($data)
                        ->addColumn('sto_by',function($data){
                            $user = DB::table('users')->where('id',$data->user_id)->select('name','nik')->first();
                            
                            return $user->name.' ('.$user->nik.')';
                        })
                        ->addColumn('sto_date',function($data){
                            return date_format(date_create($data->created_at),'d-m-Y');
                        })
                        ->addColumn('order_status',function($data){
                            // switch ($data->is_cancel_order) {
                            //     case false:
                            //         $return = '<span class="label label-success">Active</span>';
                            //         break;

                            //     case false:
                            //         $return = '<span class="label label-danger">Cancel/Reduce</span>';
                            //         break;
                            // }

                            if ($data->is_cancel_order==false) {
                                return  '<span class="label label-success">Active</span>';
                            }else{
                                return  '<span class="label label-danger">Cancel/Reduce<</span>';
                            }

                            
                        })
                        ->rawColumns(['sto_by','sto_date','order_status'])->make(true);
    }

    public function exportGetDataSto(Request $request){
        $factory_id = $request->factory_id;
        $filterby = $request->filterby;
        $orderby = $request->orderby;
        $direction = $request->direction;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $factory = DB::table('factory')->where('factory_id',$factory_id)->whereNull('deleted_at')->first();

        $data = DB::table('sto_accounting')
                ->where(function($query) use ($range){
                    $query->whereBetween('created_at',[$range['from'],$range['to']]);
                })
                ->where('factory_id',$factory_id)
                ->whereNull('deleted_at');
                // ->orderBy('po_number','asc')
                // ->orderBy('barcode_id','asc');

        if (!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby){
                $query->where('barcode_id','like','%'.$filterby.'%')
                        ->orWhere('po_number','like','%'.$filterby.'%')
                        ->orWhere('plan_ref_number','like','%'.$filterby.'%')
                        ->orWhere('upc','like','%'.$filterby.'%')
                        ->orWhere('buyer_item','like','%'.$filterby.'%')
                        ->orWhere('trademark','like','%'.$filterby.'%');
            });

            $filtername = '_filterby_'.$filterby;
        }else{
            $data=$data;
            $filtername = '';
        }

        if ($orderby != 'undefined') {
            $data_result = $data->orderBy($orderby,$direction)->get();
            $ordername = '_orderby_'.$orderby.'_'.$direction.'_';
        }else{
            $data_result = $data->orderBy('po_number','asc')
                        ->orderBy('barcode_id','asc')->get();
            $ordername = '';
        }

        $i = 1;
        $filename = $factory->factory_name.'_ReportScanSTO_'.$range['from'].'_until_'.$range['to'].$ordername.$filtername;

        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }

        if ($data_result->count()>0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use ($i){
                    $user = DB::table('users')->where('id',$row->user_id)->select('name','nik')->first();

                    if ($row->is_cancel_order==true) {
                        $dt_order = "Cancel/Reduce";
                    }else{
                        $dt_order = "Active";
                    }

                    if ($row->locator==$row->locator_sto) {
                        $check = "Yes";
                    }else{
                        $check = "No";
                    }
                return[
                    '#'=>$row->no,
                    'Barcode id'=>$row->barcode_id,
                    'Season'=>$row->season,
                    'Style'=>$row->upc,
                    'Art. No.'=>$row->buyer_item,
                    'Brand'=>$row->trademark,
                    'PO Number'=>$row->po_number,
                    'Plan Ref.'=>$row->plan_ref_number,
                    'Cust. Size'=>$row->customer_size,
                    'Manuf. Size'=>$row->manufacturing_size,
                    'Qty'=>$row->inner_pack,
                    'FGMS Location'=>$row->locator,
                    'STO Location'=>$row->locator_sto,
                    'STO By'=>$user->name.' ('.$user->nik.')',
                    'STO Date'=>date_format(date_create($row->created_at),'d-m-Y'),
                    'Remark'=>$row->remark,
                    'Order Status'=>$dt_order,
                    'STO Check'=>$check
                ];
            });
        }else{
            return response()->json('no data',422);
        }
    }


    public function finalReportSto(){
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('accounting/sto/finalSto')->with('factory', $factory);
    }


    private function processCekInStock($factory_id,$date_from,$date_to){
        

        $all_array = array();

        $dt_stock = DB::table('po_summary')
                            ->join('package_detail',function($join){
                                $join->on('po_summary.po_number','=','package_detail.po_number');
                                $join->on('po_summary.plan_ref_number','=','package_detail.plan_ref_number');
                            })
                            ->join('package','package_detail.scan_id','=','package.scan_id')
                            ->leftjoin('package_movements','package.barcode_id','=','package_movements.barcode_package')
                            ->leftjoin('line','package_movements.line_id','=','line.id')
                            ->where(function ($query){
                                $query->where(function($query){
                                    $query->where('package.current_department','inventory')->where('package.current_status','onprogress');
                                })
                                ->orWhere(function($query){
                                    $query->where('package.current_department','inventory')->where('package.current_status','completed');
                                })
                                ->orWhere(function($query){
                                    $query->where('package.current_department','shipping')->where('package.current_status','onprogress');
                                })
                                ->orWhere('package.current_department','qcinspect');
                            })
                            ->where('po_summary.factory_id',$factory_id)
                            ->whereNull('po_summary.deleted_at')
                            ->whereNull('package_detail.deleted_at')
                            ->whereNull('package.deleted_at')
                            ->whereNull('package_movements.deleted_at')
                            ->where('package_movements.is_canceled',false)
                            ->select('po_summary.po_number','po_summary.plan_ref_number','po_summary.upc','po_summary.trademark','po_summary.factory_id','package_detail.buyer_item','package_detail.customer_size','package_detail.manufacturing_size','package_detail.inner_pack','package_detail.is_cancel_order','package.barcode_id','package.current_department','package.current_status','package_movements.barcode_locator','line.description','po_summary.season')->get();
        

            foreach ($dt_stock as $stock) {
                $dt_sto = DB::table('sto_accounting')
                        ->whereBetween('created_at',[$date_from,$date_to])
                        ->whereNull('deleted_at')->where('barcode_id',$stock->barcode_id)->first();
                if ($stock->current_department=='inventory' && $stock->current_status=='onprogress') {
                    $packloct = "Metal Detector";
                }else if ($stock->current_department=='inventory' && $stock->current_status=='completed') {
                    $packloct = $stock->barcode_locator;
                }else if ($stock->current_department=='shipping' && $stock->current_status=='onprogress') {
                    $packloct = "Loading";
                }else if ($stock->current_department=='qcinspect') {
                    $packloct = "QC Inspect";
                }else if ($stock->current_department=='sewing') {
                    $packloct = $stock->description;
                }else if ($stock->current_department=='preparation') {
                    $packloct = "Preparation";
                }else{
                    $packloct = "Undefined";
                }

                

                if ($dt_sto!=null) {
                    if ($dt_sto->locator_sto==$dt_sto->locator) {
                        $sto_status = "Sama";
                    }else{
                        $sto_status = "Berbeda";
                    }

                    $user = DB::table('users')->where('id',$dt_sto->user_id)->first();

                    $dt_array = array(
                        'barcode_id'=>$stock->barcode_id,
                        'upc'=>$stock->upc,
                        'season'=>$stock->season,
                        'buyer_item'=>$stock->buyer_item,
                        'trademark'=>$stock->trademark,
                        'po_number'=>$stock->po_number,
                        'plan_ref_number'=>$stock->plan_ref_number,
                        'customer_size'=>$stock->customer_size,
                        'manufacturing_size'=>$stock->manufacturing_size,
                        'inner_pack'=>$stock->inner_pack,
                        'current_loc'=>$packloct,
                        'fgms_loc'=>$dt_sto->locator,
                        'sto_loc'=>$dt_sto->locator_sto,
                        'remark_sto'=>$dt_sto->remark,
                        'order_status'=>$stock->is_cancel_order,
                        'sto_check'=>"Sudah STO  ",
                        'sto_status'=>$sto_status,
                        'sto_date'=>date_format(date_create($dt_sto->created_at),'d-m-Y'),
                        'sto_by'=>$user->name." (".$user->nik.")"
                    );
                }else{
                    $dt_array = array(
                        'barcode_id'=>$stock->barcode_id,
                        'upc'=>$stock->upc,
                        'season'=>$stock->season,
                        'buyer_item'=>$stock->buyer_item,
                        'trademark'=>$stock->trademark,
                        'po_number'=>$stock->po_number,
                        'plan_ref_number'=>$stock->plan_ref_number,
                        'customer_size'=>$stock->customer_size,
                        'manufacturing_size'=>$stock->manufacturing_size,
                        'inner_pack'=>$stock->inner_pack,
                        'current_loc'=>$packloct,
                        'fgms_loc'=>"-",
                        'sto_loc'=>"-",
                        'remark_sto'=>"-",
                        'order_status'=>$stock->is_cancel_order,
                        'sto_check'=>"Belum STO ",
                        'sto_status'=>"-",
                        'sto_date'=>"-",
                        'sto_by'=>"-"
                    );
                }

                $all_array[]=$dt_array;
            }

        return $all_array;
    }

    private function processCekNoStock($factory_id,$date_from,$date_to){
        $all_array = array();
        $dt_sto = DB::table('sto_accounting')->whereBetween('created_at',[$date_from,$date_to])->where('factory_id',$factory_id)->whereNull('deleted_at')->get();

            foreach ($dt_sto as $sto) {
                $cekLoc = DB::table('po_summary')
                                ->join('package_detail',function($join){
                                    $join->on('po_summary.po_number','=','package_detail.po_number');
                                    $join->on('po_summary.plan_ref_number','=','package_detail.plan_ref_number');
                                })
                                ->join('package','package_detail.scan_id','=','package.scan_id')
                                ->leftjoin('package_movements','package.barcode_id','=','package_movements.barcode_package')
                                ->leftjoin('line','package_movements.line_id','=','line.id')
                                ->where(function ($query){
                                    $query->where('package.current_department','preparation')
                                    ->orWhere('package.current_department','sewing')
                                    ->orWhere(function($query){
                                        $query->where('package.current_department','shipping')->where('package.current_status','completed');
                                    });
                                })
                                ->where('po_summary.factory_id',$factory_id)
                                ->whereNull('po_summary.deleted_at')
                                ->where('package.barcode_id',$sto->barcode_id)
                                ->whereNull('package_detail.deleted_at')
                                ->whereNull('package.deleted_at')
                                ->whereNull('package_movements.deleted_at')
                                ->where('package_movements.is_canceled',false)
                                ->select('po_summary.po_number','po_summary.plan_ref_number','po_summary.upc','po_summary.trademark','po_summary.factory_id','package_detail.buyer_item','package_detail.customer_size','package_detail.manufacturing_size','package_detail.inner_pack','package_detail.is_cancel_order','package.barcode_id','package.current_department','package.current_status','package_movements.barcode_locator','line.description','po_summary.season')->first();

                    if ($cekLoc!=null) {

                        if ($cekLoc->current_department=='preparation') {
                            $packloct = "Preparation";
                        }else if ($cekLoc->current_department=='sewing') {
                            $packloct = "Sewing";
                        }else if ($cekLoc->current_department=='inventory' && $cekLoc->current_status=='onprogress') {
                            $packloct = "Metal Detector";
                        }else if ($cekLoc->current_department=='inventory' && $cekLoc->current_status=='completed') {
                            $packloct = $cekLoc->barcode_locator;
                        }else if ($cekLoc->current_department=='qcinspect') {
                            $packloct = "QC Inspect";
                        }else if ($cekLoc->current_department=='shipping' && $cekLoc->current_status=='onprogress') {
                            $packloct = "Loading";
                        }else if ($cekLoc->current_department=='shipping' && $cekLoc->current_status=='completed') {
                            $packloct = "Shipping";
                        }else{
                            $packloct = "Undefined";
                        }

                        $user = DB::table('users')->where('id',$sto->user_id)->first();

                        if ($sto->locator_sto==$sto->locator) {
                            $sto_status = "Sama";
                        }else{
                            $sto_status = "Berbeda";
                        }

                        $dt_array = array(
                            'barcode_id'=>$sto->barcode_id,
                            'upc'=>$sto->upc,
                            'season'=>$sto->season,
                            'buyer_item'=>$sto->buyer_item,
                            'trademark'=>$sto->trademark,
                            'po_number'=>$sto->po_number,
                            'plan_ref_number'=>$sto->plan_ref_number,
                            'customer_size'=>$sto->customer_size,
                            'manufacturing_size'=>$sto->manufacturing_size,
                            'inner_pack'=>$sto->inner_pack,
                            'current_loc'=>$packloct,
                            'fgms_loc'=>$sto->locator,
                            'sto_loc'=>$sto->locator_sto,
                            'remark_sto'=>$sto->remark,
                            'order_status'=>$sto->is_cancel_order,
                            'sto_check'=>"Sudah STO ",
                            'sto_status'=>$sto_status,
                            'sto_date'=>date_format(date_create($sto->created_at),'d-m-Y'),
                            'sto_by'=>$user->name." (".$user->nik.")"
                        );

                        $all_array[]=$dt_array;
                    }
            }

        return $all_array;
    }



    public function ajaxGetDataFinal(Request $request){
        $factory_id = $request->factory_id;
        $data = array();
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );
        
        $checkInstock = $this->processCekInStock($factory_id,$range['from'],$range['to']);
        foreach ($checkInstock as $instock) {
            $dt_array_in = array(
                'barcode_id'=>$instock['barcode_id'],
                'season'=>$instock['season'],
                'upc'=>$instock['upc'],
                'buyer_item'=>$instock['buyer_item'],
                'trademark'=>$instock['trademark'],
                'po_number'=>$instock['po_number'],
                'plan_ref_number'=>$instock['plan_ref_number'],
                'customer_size'=>$instock['customer_size'],
                'manufacturing_size'=>$instock['manufacturing_size'],
                'inner_pack'=>$instock['inner_pack'],
                'current_loc'=>$instock['current_loc'],
                'fgms_loc'=>$instock['fgms_loc'],
                'sto_loc'=>$instock['sto_loc'],
                'remark_sto'=>$instock['remark_sto'],
                'order_status'=>$instock['order_status'],
                'sto_check'=>$instock['sto_check'],
                'sto_status'=>$instock['sto_status'],
                'sto_date'=>$instock['sto_date'],
                'sto_by'=>$instock['sto_by']
            );

            $data[]=$dt_array_in;
        }


        $checkNostock = $this->processCekNoStock($factory_id,$range['from'],$range['to']);
        foreach ($checkNostock as $nostock) {
            $dt_array_no = array(
                'barcode_id'=>$nostock['barcode_id'],
                'season'=>$nostock['season'],
                'upc'=>$nostock['upc'],
                'buyer_item'=>$nostock['buyer_item'],
                'trademark'=>$nostock['trademark'],
                'po_number'=>$nostock['po_number'],
                'plan_ref_number'=>$nostock['plan_ref_number'],
                'customer_size'=>$nostock['customer_size'],
                'manufacturing_size'=>$nostock['manufacturing_size'],
                'inner_pack'=>$nostock['inner_pack'],
                'current_loc'=>$nostock['current_loc'],
                'fgms_loc'=>$nostock['fgms_loc'],
                'sto_loc'=>$nostock['sto_loc'],
                'remark_sto'=>$nostock['remark_sto'],
                'order_status'=>$nostock['order_status'],
                'sto_check'=>$nostock['sto_check'],
                'sto_status'=>$nostock['sto_status'],
                'sto_date'=>$nostock['sto_date'],
                'sto_by'=>$nostock['sto_by']
            );

            $data[]=$dt_array_no;
        }
        
             

        return DataTables::of($data)
                                ->editColumn('order_status',function($data){
                                    if ($data['order_status']==false) {
                                        return  '<span class="label label-success">Active</span>';
                                    }else{
                                        return  '<span class="label label-danger">Cancel/Reduce</span>';
                                    }
                                })
                                ->rawColumns(['order_status'])
                                ->make(true);

    }

    public function exportFinalSto(Request $request){
        $factory_id = $request->factory_id;
        $data = array();
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );
        
        $i = 1;
        $checkInstock = $this->processCekInStock($factory_id,$range['from'],$range['to']);
        foreach ($checkInstock as $instock) {
            $dt_array_in = array(
                'barcode_id'=>$instock['barcode_id'],
                'season'=>$instock['season'],
                'upc'=>$instock['upc'],
                'buyer_item'=>$instock['buyer_item'],
                'trademark'=>$instock['trademark'],
                'po_number'=>$instock['po_number'],
                'plan_ref_number'=>$instock['plan_ref_number'],
                'customer_size'=>$instock['customer_size'],
                'manufacturing_size'=>$instock['manufacturing_size'],
                'inner_pack'=>$instock['inner_pack'],
                'current_loc'=>$instock['current_loc'],
                'fgms_loc'=>$instock['fgms_loc'],
                'sto_loc'=>$instock['sto_loc'],
                'remark_sto'=>$instock['remark_sto'],
                'order_status'=>$instock['order_status'],
                'sto_check'=>$instock['sto_check'],
                'sto_status'=>$instock['sto_status'],
                'sto_date'=>$instock['sto_date'],
                'sto_by'=>$instock['sto_by']
            );

            $data[]=$dt_array_in;
        }


        $checkNostock = $this->processCekNoStock($factory_id,$range['from'],$range['to']);
        foreach ($checkNostock as $nostock) {
            $dt_array_no = array(
                'barcode_id'=>$nostock['barcode_id'],
                'season'=>$nostock['season'],
                'upc'=>$nostock['upc'],
                'buyer_item'=>$nostock['buyer_item'],
                'trademark'=>$nostock['trademark'],
                'po_number'=>$nostock['po_number'],
                'plan_ref_number'=>$nostock['plan_ref_number'],
                'customer_size'=>$nostock['customer_size'],
                'manufacturing_size'=>$nostock['manufacturing_size'],
                'inner_pack'=>$nostock['inner_pack'],
                'current_loc'=>$nostock['current_loc'],
                'fgms_loc'=>$nostock['fgms_loc'],
                'sto_loc'=>$nostock['sto_loc'],
                'remark_sto'=>$nostock['remark_sto'],
                'order_status'=>$nostock['order_status'],
                'sto_check'=>$nostock['sto_check'],
                'sto_status'=>$nostock['sto_status'],
                'sto_date'=>$nostock['sto_date'],
                'sto_by'=>$nostock['sto_by']
            );

            $data[]=$dt_array_no;
        }

        $factory = DB::table('factory')->where('factory_id',$factory_id)->whereNull('deleted_at')->first();

    
        $filename = $factory->factory_name."_ReportFinalSto_".$range['from']."_until_".$range['to'];


        $export = \Excel::create($filename,function($excel) use ($data,$i){
            $excel->sheet('report',function($sheet) use ($data,$i){
                $sheet->appendRow(array(
                    '#',
                    'Barcode ID',
                    'Season',
                    'Style',
                    'Art. No.',
                    'Brand',
                    'PO Number',
                    'Plan Ref.',
                    'Cust. Size',
                    'Manf. Size',
                    'Qty',
                    'Current Loc.',
                    'Fgms Loc.',
                    'STO Loc.',
                    'Remark STO',
                    'STO Check',
                    'STO Status',
                    'STO Date',
                    'Order Status',
                    'STO By'
                ));
                foreach ($data as $row) {
                    if ($row['order_status']==true) {
                        $is_cancel_order = "Cancel/Reduce";
                    }else{
                        $is_cancel_order = "Active";
                    }

                    $sheet->appendRow(array(
                        $i++,
                        '="'.$row['barcode_id'].'"',
                        $row['season'],
                        $row['upc'],
                        $row['buyer_item'],
                        $row['trademark'],
                        $row['po_number'],
                        '="'.$row['plan_ref_number'].'"',
                        $row['customer_size'],
                        $row['manufacturing_size'],
                        $row['inner_pack'],
                        $row['current_loc'],
                        $row['fgms_loc'],
                        $row['sto_loc'],
                        $row['remark_sto'],
                        $row['sto_check'],
                        $row['sto_status'],
                        $row['sto_date'],
                        $is_cancel_order,
                        $row['sto_by']
                    ));
                }
            });
        })->download('xlsx');
    }
}
