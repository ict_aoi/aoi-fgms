<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        $nik  = request()->get('nik');
        $fieldName = 'nik';
        request()->merge([$fieldName => $nik]);
        return $fieldName;
    }
    /**
    * Validate the user login.
    * @param Request $request
    */
   public function login(Request $request){
        $this->validate(
            $request,
            [
                'nik' => 'required|string',
                'password' => 'required|string',
            ],
            [
                'nik.required' => 'NIK is required',
                'password.required' => 'Password is required',
            ]
        );
        if(auth()->attempt(['nik'=>$request->nik,'password'=>$request->password])){

            if(auth()->user()->deleted_at!==null){
                Auth::logout();
                return redirect()->back()
                    ->withInput($request->only($this->username(), 'remember'))
                    ->withErrors([
                        'password' => 'Your account has not yet been activated',
                    ]);
            }
            return redirect(route('dashboard'));
        }else {
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    'password' => trans('auth.failed'),
                ]);
        }
    }
    // protected function validateLogin(Request $request)
    // {
    //     $this->validate(
    //         $request,
    //         [
    //             'nik' => 'required|string',
    //             'password' => 'required|string',
    //         ],
    //         [
    //             'nik.required' => 'NIK is required',
    //             'password.required' => 'Password is required',
    //         ]
    //     );
    // }
    /**
    * @param Request $request
    * @throws ValidationException
    */

    protected function authenticated(Request $request, $user)
    {
        $getRoles = \DB::table('role_user')
                        ->select('name')
                        ->join('roles', 'roles.id', '=', 'role_user.role_id')
                        ->where('user_id', $user->id)
                        ->first();

        if($getRoles) {
            if(strtolower($getRoles->name) == 'storing') {
                return redirect()->route('inventory.placement');
            }
            elseif(strtolower($getRoles->name) == 'receiving') {
                return redirect()->route('inventory.checkin');
            }
        }
        return redirect()->route('dashboard');
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $request->session()->put('login_error', trans('auth.failed'));


        // throw ValidationException::withMessages(
        //     [
        //         'error' => [trans('auth.failed')],
        //     ]
        // );


        // if ( ! User::where('nik', $request->nik)->first() ) {
        //     return redirect()->back()
        //         ->withInput($request->only($this->username(), 'remember'))
        //         ->withErrors([
        //             $this->username() => trans('auth.failed'),
        //         ]);
        // }

        // if ( ! User::where('username', $request->username)->where('password', bcrypt($request->password))->first() ) {
        //     return redirect()->back()
        //         ->withInput($request->only($this->username(), 'remember'))
        //         ->withErrors([
        //             'password' => trans('auth.failed'),
        //         ]);
        // }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([
                'password' => trans('auth.failed'),
            ]);

    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }
}
