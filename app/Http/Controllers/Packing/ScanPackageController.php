<?php

namespace App\Http\Controllers\Packing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use DataTables;
use Carbon\Carbon;

class ScanPackageController extends Controller
{
    public function index()
    {
        return view('packing/scan_package/index');
    }

    //set status package
    public function setStatusPackage(Request $request) {
        $barcodeid = trim($request->barcodeid);
        $current_department = 'preparation';
        $current_status = strtolower(trim($request->status));

        //check apakah package siap di checkout
        $check = DB::table('package')
                 ->where('barcode_id', $barcodeid)
                 ->where('is_prepared', true)
                 ->where('is_print_barcode', true)
                 ->where('is_print_shippingmark', true)
                 ->where('current_status', 'onprogress')
                 ->where('current_department', 'preparation')
                 ->whereNull('deleted_at')
                 ->first();

        if(!$check) {
            return response()->json('Package not found or is not ready, please checked.', 422);
        }

        //data package movement
        $data = array(
            'barcode_id' => $barcodeid,
            'current_department' => $current_department,
            'current_status' => $current_status,
            'created_at' => Carbon::now()
        );

        try {
            db::beginTransaction();

            //update status
            DB::table('package')
            ->where('barcode_id', $barcodeid)
            ->whereNull('deleted_at')
            ->update([
                'current_department' => $current_department,
                'current_status' => $current_status,
                'status_preparation' => $current_status,
                'updated_at' => Carbon::now()
            ]);

            //insert movement history
            DB::table('package_movement')->insert($data);
            db::commit();
        }
        catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return view('packing/scan_package/ajax_package_list')->with('data', $data);

    }
}
