<?php

namespace App\Http\Controllers\Packing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

class PackageDetailController extends Controller
{
    protected $api_url =  "http://mdware.aoi.co.id/api/packing/customer-information?";
    protected $parameter = array(
        'lc_date_from',
        'lc_date_to',
        'po_number',
        'poreference'
    );
 
    public function index(){
        return view('packing/package_detail/index');
    }

    public function getData(Request $request) {
        $data = DB::table('package_detail')
                    ->where([
                        ['po_number', $request->po_number]
                    ])
                    ->whereNull('deleted_at')
                    ->orderBy('scan_id', 'asc');

        return Datatables::of($data)
               ->make(true);
    }

    // package preparation
    public function preparation(){
        return view('packing/package_detail/preparation');
    }

    public function getDataPreparation(Request $request) {
      $po_number = $request->po_number;


        $data = DB::table('v_ns_po_preparation')
                    ->where('factory_id', Auth::user()->factory_id);

        if ($po_number != '') {
          $data = $data->where('po_number', 'like', '%'.$po_number.'%');
        }else{
          $data = $data->where('po_number', $po_number);
        }

        $data = $data->orderBy('upload_date', 'asc');

        return Datatables::of($data)
              ->addColumn('btn_action', function($data){
                  return '<input type="button" class="btn btn-default btn-delete" data-po="'.$data->po_number.'" data-planrefnumber="'.$data->plan_ref_number.'" value="Delete" onclick="delete_plan(this);" >';
              })
              ->rawColumns(['action', 'btn_action'])
              ->make(true);
    }

    public function deleteDataPreparation(Request $request) {
      $po_number = $request->po_number;
      $plan_ref_number = $request->plan_ref_number;
      $key_id = $po_number."-".Carbon::now()->format('YmdHis');

      // cek invoice exim
      $cek_invoice = DB::table('po_stuffing')
                        ->where('po_number',$po_number)
                        ->where('plan_ref_number',$plan_ref_number)
                        ->whereNotNull('invoice')
                        ->whereNull('deleted_at')
                        ->exists();

      if ($cek_invoice) {
          return response()->json('Invoice already created, please contact the Exim Department to cancel before..!', 422);
      }

     

        $userid = Auth::user()->id;
        $data_history_user = [
                'deleted_from' => $userid,
                'deleted_date' => Carbon::now()
              ];


        $list_scanid = [];
        $list_bc = [];
        $data_po = [];
        $data_movement = [];
        $data_package_detail = [];
        $data_package = [];
        $data_package_detail_rasio = [];
        $is_costco=false;
        $data_integrate_cancel = [];

        // cek apakah po sudah ada yg discan ?
        $detail_package = DB::table('package_detail')
                        ->where('po_number', $po_number)
                        ->where('plan_ref_number', $plan_ref_number)
                        ->where('factory_id', Auth::user()->factory_id)
                        ->whereNull('deleted_at')
                        ->get();

        foreach ($detail_package as $key => $value) {
          $list_scanid[] = $value->scan_id;
          // $data[] = $value->scan_id;
        }

        $cek_scan = DB::table('package')
                    ->where('current_department', '<>', 'preparation')
                    ->whereIn('scan_id', $list_scanid)
                    ->wherenull('deleted_at')
                    ->get();

        if (count($cek_scan) !== 0) {
          throw new \Exception('po number are still transactions');
        }

        // get data po
        $get_po = DB::table('po_summary')
                      ->where('po_number', $po_number)
                      ->where('plan_ref_number', $plan_ref_number)
                      ->get();

        foreach ($get_po as $keyy => $value_) {
                  $detail['po_number'] = $value_->po_number;
                  $detail['plan_ref_number'] = $value_->plan_ref_number;
                  $detail['created_at'] = $value_->created_at;
                  $detail['updated_at'] = $value_->updated_at;
                  $detail['deleted_at'] = $value_->deleted_at;
                  $detail['dateordered'] = $value_->dateordered;
                  $detail['datepromised'] = $value_->datepromised;
                  $detail['grandtotal'] = $value_->grandtotal;
                  $detail['bp_name'] = $value_->bp_name;
                  $detail['bp_code'] = $value_->bp_code;
                  $detail['phone'] = $value_->phone;
                  $detail['fax'] = $value_->fax;
                  $detail['address'] = $value_->address;
                  $detail['city'] = $value_->city;
                  $detail['postal'] = $value_->postal;
                  $detail['country'] = $value_->country;
                  $detail['kst_lcdate'] = $value_->kst_lcdate;
                  $detail['kst_statisticaldate'] = $value_->kst_statisticaldate;
                  $detail['trademark'] = $value_->trademark;
                  $detail['made_in'] = $value_->made_in;
                  $detail['country_of_origin'] = $value_->country_of_origin;
                  $detail['commodities_type'] = $value_->commodities_type;
                  $detail['persen_inventory'] = $value_->persen_inventory;
                  $detail['persen_qcinspect'] = $value_->persen_qcinspect;
                  $detail['sample_size_qc'] = $value_->sample_size_qc;
                  $detail['completed_qty_qc'] = $value_->completed_qty_qc;
                  $detail['customer_order_number'] = $value_->customer_order_number;
                  $detail['remark'] = $value_->remark;
                  $detail['psd'] = $value_->psd;
                  $detail['factory_id'] = $value_->factory_id;
                  $detail['persen_shipping'] = $value_->persen_shipping;
                  $detail['is_integrate'] = $value_->is_integrate;
                  $detail['integration_date'] = $value_->integration_date;
                  $detail['m_locator_id'] = $value_->m_locator_id;
                  $detail['is_complete'] = $value_->is_complete;
                  $detail['invoice'] = $value_->invoice;
                  $detail['deleted_from'] = $userid;
                  $detail['upc'] = $value_->upc;
                  $detail['season'] = $value_->season;
                  $detail['deleted_date'] = Carbon::now();
                  $detail['key_id'] = $key_id;
                  $is_costco = $value_->is_costco;
                  $data_po[] = $detail;
        }

        // data package detail
        $get_package_detail = DB::table('package_detail')
                              ->whereIn('scan_id', $list_scanid)
                              // ->where('factory_id', Auth::user()->factory_id)
                              ->whereNull('deleted_at')
                              ->get();

        foreach ($get_package_detail as $key2 => $value2) {
                  $pdetail['scan_id'] = $value2->scan_id;
                  $pdetail['po_number'] = $value2->po_number;
                  $pdetail['package_range'] = $value2->package_range;
                  $pdetail['package_from'] = $value2->package_from;
                  $pdetail['package_to'] = $value2->package_to;
                  $pdetail['serial_from'] = $value2->serial_from;
                  $pdetail['serial_to'] = $value2->serial_to;
                  $pdetail['pack_instruction_code'] = $value2->pack_instruction_code;
                  $pdetail['line'] = $value2->line;
                  $pdetail['sku'] = $value2->sku;
                  $pdetail['buyer_item'] = $value2->buyer_item;
                  $pdetail['model_number'] = $value2->model_number;
                  $pdetail['customer_size'] = $value2->customer_size;
                  $pdetail['customer_number'] = $value2->customer_number;
                  $pdetail['manufacturing_size'] = $value2->manufacturing_size;
                  $pdetail['technical_print_index'] = $value2->technical_print_index;
                  $pdetail['gps_three_digit_size'] = $value2->gps_three_digit_size;
                  $pdetail['service_identifier'] = $value2->service_identifier;
                  $pdetail['packing_mode'] = $value2->packing_mode;
                  $pdetail['item_qty'] = $value2->item_qty;
                  $pdetail['inner_pack'] = $value2->inner_pack;
                  $pdetail['inner_pkg_count'] = $value2->inner_pkg_count;
                  $pdetail['pkg_count'] = $value2->pkg_count;
                  $pdetail['net_net'] = $value2->net_net;
                  $pdetail['net'] = $value2->net;
                  $pdetail['gross'] = $value2->gross;
                  $pdetail['unit_1'] = $value2->unit_1;
                  $pdetail['length'] = $value2->length;
                  $pdetail['width'] = $value2->width;
                  $pdetail['height'] = $value2->height;
                  $pdetail['created_at'] = $value2->created_at;
                  $pdetail['updated_at'] = $value2->updated_at;
                  $pdetail['deleted_at'] = $value2->deleted_at;
                  $pdetail['r'] = $value2->r;
                  $pdetail['pkg_code'] = $value2->pkg_code;
                  $pdetail['unit_2'] = $value2->unit_2;
                  $pdetail['rasio'] = $value2->rasio;
                  $pdetail['factory_id'] = $value2->factory_id;
                  $pdetail['plan_ref_number'] = $value2->plan_ref_number;
                  $pdetail['key_id'] = $key_id;
                  $data_package_detail[] = $pdetail;
        }

        // get data package
        $get_data_package = DB::table('package')
                            ->whereIn('scan_id', $list_scanid)
                            ->whereNull('deleted_at')
                            ->get();

        foreach ($get_data_package as $key3 => $value3) {
                  $package['barcode_id'] = $value3->barcode_id;
                  $package['scan_id'] = $value3->scan_id;
                  $package['package_number'] = $value3->package_number;
                  $package['serial_number'] = $value3->serial_number;
                  $package['created_at'] = $value3->created_at;
                  $package['updated_at'] = $value3->updated_at;
                  $package['deleted_at'] = $value3->deleted_at;
                  $package['current_department'] = $value3->current_department;
                  $package['current_status'] = $value3->current_status;
                  $package['status_preparation'] = $value3->status_preparation;
                  $package['status_sewing'] = $value3->status_sewing;
                  $package['status_inventory'] = $value3->status_inventory;
                  $package['status_qcinspect'] = $value3->status_qcinspect;
                  $package['is_printed'] = $value3->is_printed;
                  $package['status_shipping'] = $value3->status_shipping;
                  $package['locator_sewing'] = $value3->locator_sewing;
                  $package['locator_inventory'] = $value3->locator_inventory;
                  $package['is_integrate'] = $value3->is_integrate;
                  $package['integration_date'] = $value3->integration_date;
                  $package['is_complete'] = $value3->is_complete;
                  $package['is_integrate_out'] = $value3->is_integrate_out;
                  $package['integration_date_out'] = $value3->integration_date_out;

                  $data_package[] = $package;
                  $list_bc[] = $value3->barcode_id;

                  if ($value3->is_integrate==true) {
                      $pc['scan_id']= $value3->scan_id;
                      $pc['created_at']=Carbon::now();
                      $pc['barcode_id']=$value3->barcode_id;

                      $data_integrate_cancel[]=$pc;

                  }
        }

        // get data mavement
        $get_data_movement = DB::table('package_movements')
                            ->whereIn('barcode_package', $list_bc)
                            ->get();

        foreach ($get_data_movement as $key4 => $value4) {
                $detailmovement['id'] = $value4->id;
                $detailmovement['barcode_package'] = $value4->barcode_package;
                $detailmovement['barcode_locator'] = $value4->barcode_locator;
                $detailmovement['department_from'] = $value4->department_from;
                $detailmovement['department_to'] = $value4->department_to;
                $detailmovement['status_from'] = $value4->status_from;
                $detailmovement['status_to'] = $value4->status_to;
                $detailmovement['user_id'] = $value4->user_id;
                $detailmovement['created_at'] = $value4->created_at;
                $detailmovement['description'] = $value4->description;
                $detailmovement['deleted_at'] = $value4->deleted_at;
                $detailmovement['is_canceled'] = $value4->is_canceled;
                $detailmovement['line_id'] = $value4->line_id;
                // $detailmovement['locator_sewing'] = $value4->locator_sewing;
                // $detailmovement['locator_inventory'] = $value4->locator_inventory;
                $detailmovement['ip_address'] = $value4->ip_address;
                $data_movement[] = $detailmovement;
        }

          if ($is_costco==true) {
          $get_data_rasio = DB::table('package_detail_rasio')
                            ->where('po_number_key',$po_number)
                            ->get();

              foreach ($get_data_rasio as $key5 => $value5) {
                    $dtrasio['plan_ref_number']= $value5->plan_ref_number;
                    $dtrasio['po_number']= $value5->po_number;
                    $dtrasio['line']= $value5->line;
                    $dtrasio['buyer_item']= $value5->buyer_item;
                    $dtrasio['model_number']= $value5->model_number;
                    $dtrasio['customer_size']= $value5->customer_size;
                    $dtrasio['technical_print_index']= $value5->technical_print_index;
                    $dtrasio['service_identifier']= $value5->service_identifier;
                    $dtrasio['packing_mode']= $value5->packing_mode;
                    $dtrasio['item_qty']= $value5->item_qty;
                    $dtrasio['inner_pack']= $value5->inner_pack;
                    $dtrasio['factory_id']= $value5->factory_id;
                    $dtrasio['manufacturing_size']= $value5->manufacturing_size;
                    $dtrasio['deleted_from']= $userid;
                    $dtrasio['po_number_key']= $value5->po_number_key;
                    $dtrasio['deleted_date']= Carbon::now();
                    $dtrasio['created_at']= $value5->created_at;
                    $data_package_detail_rasio[]=$dtrasio;

                }   
        }

        try {
          DB::beginTransaction();
           if ($is_costco==true) {
                DB::table('history_po_summary')->insert($data_po);
                DB::table('history_package_detail')->insert($data_package_detail);
                DB::table('history_package')->insert($data_package);
                DB::table('history_package_movements')->insert($data_movement);
                DB::table('history_package_detail_rasio')->insert($data_package_detail_rasio);

                if (!empty($data_integrate_cancel)) {
                    DB::table('package_cancel_integration')->insert($data_integrate_cancel);
                }
                // delete
                DB::table('package_movements')
                    ->whereIn('barcode_package', $list_bc)
                    ->delete();

                DB::table('package')
                    ->whereIn('scan_id', $list_scanid)
                    ->whereNull('deleted_at')
                    ->delete();

                DB::table('package_detail')
                    ->whereIn('scan_id', $list_scanid)
                    ->whereNull('deleted_at')
                    ->delete();

                DB::table('po_summary')
                    ->where('po_number', $po_number)
                    ->where('plan_ref_number', $plan_ref_number)
                    ->delete();

                DB::table('package_detail_rasio')
                    ->where('po_number_key',$po_number)
                    ->delete();

          }else{
                // insert history
              DB::table('history_po_summary')->insert($data_po);
              DB::table('history_package_detail')->insert($data_package_detail);
              DB::table('history_package')->insert($data_package);
              DB::table('history_package_movements')->insert($data_movement);

              if (!empty($data_integrate_cancel)) {
                    DB::table('package_cancel_integration')->insert($data_integrate_cancel);
                }

              // delete
              DB::table('package_movements')
                  ->whereIn('barcode_package', $list_bc)
                  ->delete();

              DB::table('package')
                  ->whereIn('scan_id', $list_scanid)
                  ->whereNull('deleted_at')
                  ->delete();

              DB::table('package_detail')
                  ->whereIn('scan_id', $list_scanid)
                  ->whereNull('deleted_at')
                  ->delete();

              DB::table('po_summary')
                  ->where('po_number', $po_number)
                  ->where('plan_ref_number', $plan_ref_number)
                  ->delete();
          }  


            $data_response = [
              'status' => 200,
              'po_number' => $po_number,
              'output' => 'Data has been deleted'
            ];

            //


              DB::commit();
        } catch (Exception $ex) {
              db::rollback();
              $message = $ex->getMessage();
              ErrorHandler::db($message);

              $data_response = [
                'status' => 422,
                'po_number' => $po_number,
                'output' => 'Oops something wrong..!!'
              ];
        }

        return response()->json(['data' => $data_response]);
      
    }

    // po cancel
    public function packagecancel(){
      return view('packing/package_detail/package_cancel');
  }

  public function getDataCancel(Request $request) {
    $po_number = $request->po_number;


      $data = DB::table('po_summary')
                  ->select('po_number', 'customer_order_number', 'country','plan_ref_number')
                  ->where('factory_id', Auth::user()->factory_id)
                  ->whereNull('deleted_at')
                  ->groupBy('po_number', 'customer_order_number', 'country','plan_ref_number');

      if ($po_number != '') {
        $data = $data->where('po_number', 'like', '%'.$po_number.'%');
      }else{
        $data = $data->where('po_number', $po_number);
      }

      // $data = $data->orderBy('upload_date', 'asc');

      return Datatables::of($data)
            ->addColumn('btn_action', function($data){
                return '<input type="button" class="btn btn-default btn-delete" data-po="'.$data->po_number.'" data-plan="'.$data->plan_ref_number.'" value="Delete" onclick="delete_plan(this);" >';
            })
            ->rawColumns(['action', 'btn_action'])
            ->make(true);
  }

  public function deleteDataCancel(Request $request) {
    $po_number = $request->po_number;
    $plan_ref_number = $request->plan_ref_number;
    $key_id = $po_number."-".Carbon::now()->format('YmdHis');

    // cek invoice exim
    $cek_invoice = DB::table('po_stuffing')
                        ->where('po_number',$po_number)
                        ->where('plan_ref_number',$plan_ref_number)
                        ->whereNotNull('invoice')
                        ->whereNull('deleted_at')
                        ->exists();
       
    if ($cek_invoice) {
        return response()->json('Invoice already created, please contact the Exim Department to cancel before..!', 422);
    }

    $userid = Auth::user()->id;
    $data_history_user = [
            'deleted_from' => $userid,
            'deleted_date' => Carbon::now()
          ];


    $list_scanid = [];
    $list_bc = [];
    $data_po = [];
    $data_movement = [];
    $data_package_detail = [];
    $data_package = [];
    $data_integrate_cancel = [];
    $is_costco =false;


    // get list scan id ?
    $detail_package = DB::table('package_detail')
                    ->where('po_number', $po_number)
                    ->where('plan_ref_number', $plan_ref_number)
                    ->where('factory_id', Auth::user()->factory_id)
                    ->whereNull('deleted_at')
                    ->get();

    foreach ($detail_package as $key => $value) {
      $list_scanid[] = $value->scan_id;
      // $data[] = $value->scan_id;
    }

    // cek apakah po udah ada yg stuffing ?
    $cek_scan = DB::table('package')
                ->where('current_department', 'shipping')
                ->where('current_status', 'completed')
                ->whereIn('scan_id', $list_scanid)
                ->wherenull('deleted_at')
                ->get();

    // if (count($cek_scan) > 0) {
    //   throw new \Exception('po number are still shipment');
    // }

    // get data po
    $get_po = DB::table('po_summary')
                  ->where('po_number', $po_number)
                  ->where('plan_ref_number', $plan_ref_number)
                  ->get();

    foreach ($get_po as $keyy => $value_) {
              $detail['po_number'] = $value_->po_number;
              $detail['plan_ref_number'] = $value_->plan_ref_number;
              $detail['created_at'] = $value_->created_at;
              $detail['updated_at'] = $value_->updated_at;
              $detail['deleted_at'] = $value_->deleted_at;
              $detail['dateordered'] = $value_->dateordered;
              $detail['datepromised'] = $value_->datepromised;
              $detail['grandtotal'] = $value_->grandtotal;
              $detail['bp_name'] = $value_->bp_name;
              $detail['bp_code'] = $value_->bp_code;
              $detail['phone'] = $value_->phone;
              $detail['fax'] = $value_->fax;
              $detail['address'] = $value_->address;
              $detail['city'] = $value_->city;
              $detail['postal'] = $value_->postal;
              $detail['country'] = $value_->country;
              $detail['kst_lcdate'] = $value_->kst_lcdate;
              $detail['kst_statisticaldate'] = $value_->kst_statisticaldate;
              $detail['trademark'] = $value_->trademark;
              $detail['made_in'] = $value_->made_in;
              $detail['country_of_origin'] = $value_->country_of_origin;
              $detail['commodities_type'] = $value_->commodities_type;
              $detail['persen_inventory'] = $value_->persen_inventory;
              $detail['persen_qcinspect'] = $value_->persen_qcinspect;
              $detail['sample_size_qc'] = $value_->sample_size_qc;
              $detail['completed_qty_qc'] = $value_->completed_qty_qc;
              $detail['customer_order_number'] = $value_->customer_order_number;
              $detail['remark'] = $value_->remark;
              $detail['psd'] = $value_->psd;
              $detail['factory_id'] = $value_->factory_id;
              $detail['persen_shipping'] = $value_->persen_shipping;
              $detail['is_integrate'] = $value_->is_integrate;
              $detail['integration_date'] = $value_->integration_date;
              $detail['m_locator_id'] = $value_->m_locator_id;
              $detail['is_complete'] = $value_->is_complete;
              $detail['invoice'] = $value_->invoice;
              $detail['upc'] = $value_->upc;
              $detail['season'] = $value_->season;
              $detail['deleted_from'] = $userid;
              $detail['deleted_date'] = Carbon::now();
              $detail['key_id'] = $key_id;
              $is_costco = $value_->is_costco;
              $data_po[] = $detail;
    }

    // data package detail
    $get_package_detail = DB::table('package_detail')
                          ->whereIn('scan_id', $list_scanid)
                          // ->where('factory_id', Auth::user()->factory_id)
                          ->whereNull('deleted_at')
                          ->get();

    foreach ($get_package_detail as $key2 => $value2) {
              $pdetail['scan_id'] = $value2->scan_id;
              $pdetail['po_number'] = $value2->po_number;
              $pdetail['package_range'] = $value2->package_range;
              $pdetail['package_from'] = $value2->package_from;
              $pdetail['package_to'] = $value2->package_to;
              $pdetail['serial_from'] = $value2->serial_from;
              $pdetail['serial_to'] = $value2->serial_to;
              $pdetail['pack_instruction_code'] = $value2->pack_instruction_code;
              $pdetail['line'] = $value2->line;
              $pdetail['sku'] = $value2->sku;
              $pdetail['buyer_item'] = $value2->buyer_item;
              $pdetail['model_number'] = $value2->model_number;
              $pdetail['customer_size'] = $value2->customer_size;
              $pdetail['customer_number'] = $value2->customer_number;
              $pdetail['manufacturing_size'] = $value2->manufacturing_size;
              $pdetail['technical_print_index'] = $value2->technical_print_index;
              $pdetail['gps_three_digit_size'] = $value2->gps_three_digit_size;
              $pdetail['service_identifier'] = $value2->service_identifier;
              $pdetail['packing_mode'] = $value2->packing_mode;
              $pdetail['item_qty'] = $value2->item_qty;
              $pdetail['inner_pack'] = $value2->inner_pack;
              $pdetail['inner_pkg_count'] = $value2->inner_pkg_count;
              $pdetail['pkg_count'] = $value2->pkg_count;
              $pdetail['net_net'] = $value2->net_net;
              $pdetail['net'] = $value2->net;
              $pdetail['gross'] = $value2->gross;
              $pdetail['unit_1'] = $value2->unit_1;
              $pdetail['length'] = $value2->length;
              $pdetail['width'] = $value2->width;
              $pdetail['height'] = $value2->height;
              $pdetail['created_at'] = $value2->created_at;
              $pdetail['updated_at'] = $value2->updated_at;
              $pdetail['deleted_at'] = $value2->deleted_at;
              $pdetail['r'] = $value2->r;
              $pdetail['pkg_code'] = $value2->pkg_code;
              $pdetail['unit_2'] = $value2->unit_2;
              $pdetail['rasio'] = $value2->rasio;
              $pdetail['factory_id'] = $value2->factory_id;
              $pdetail['plan_ref_number'] = $value2->plan_ref_number;
              $pdetail['key_id'] = $key_id;
              $data_package_detail[] = $pdetail;
    }

    // get data package
    $get_data_package = DB::table('package')
                        ->whereIn('scan_id', $list_scanid)
                        ->whereNull('deleted_at')
                        ->get();

    foreach ($get_data_package as $key3 => $value3) {
              $package['barcode_id'] = $value3->barcode_id;
              $package['scan_id'] = $value3->scan_id;
              $package['package_number'] = $value3->package_number;
              $package['serial_number'] = $value3->serial_number;
              $package['created_at'] = $value3->created_at;
              $package['updated_at'] = $value3->updated_at;
              $package['deleted_at'] = $value3->deleted_at;
              $package['current_department'] = $value3->current_department;
              $package['current_status'] = $value3->current_status;
              $package['status_preparation'] = $value3->status_preparation;
              $package['status_sewing'] = $value3->status_sewing;
              $package['status_inventory'] = $value3->status_inventory;
              $package['status_qcinspect'] = $value3->status_qcinspect;
              $package['is_printed'] = $value3->is_printed;
              $package['status_shipping'] = $value3->status_shipping;
              $package['locator_sewing'] = $value3->locator_sewing;
              $package['locator_inventory'] = $value3->locator_inventory;
              $package['is_integrate'] = $value3->is_integrate;
              $package['integration_date'] = $value3->integration_date;
              $package['is_complete'] = $value3->is_complete;
              $package['integration_date_out'] = $value3->integration_date_out;
              $package['is_complete'] = $value3->is_complete;

              $data_package[] = $package;
              $list_bc[] = $value3->barcode_id;

              if ($value3->is_integrate==true) {
                  $pc['scan_id']=$value3->scan_id;
                  $pc['created_at']=Carbon::now();
                  $pc['barcode_id']=$value3->barcode_id;

                  $data_integrate_cancel[]=$pc;

              }
    }

    // get data mavement
    $get_data_movement = DB::table('package_movements')
                        ->whereIn('barcode_package', $list_bc)
                        ->get();

    foreach ($get_data_movement as $key4 => $value4) {
            $detailmovement['id'] = $value4->id;
            $detailmovement['barcode_package'] = $value4->barcode_package;
            $detailmovement['barcode_locator'] = $value4->barcode_locator;
            $detailmovement['department_from'] = $value4->department_from;
            $detailmovement['department_to'] = $value4->department_to;
            $detailmovement['status_from'] = $value4->status_from;
            $detailmovement['status_to'] = $value4->status_to;
            $detailmovement['user_id'] = $value4->user_id;
            $detailmovement['created_at'] = $value4->created_at;
            $detailmovement['description'] = $value4->description;
            $detailmovement['deleted_at'] = $value4->deleted_at;
            $detailmovement['is_canceled'] = $value4->is_canceled;
            $detailmovement['line_id'] = $value4->line_id;
            // $detailmovement['locator_sewing'] = $value4->locator_sewing;
            // $detailmovement['locator_inventory'] = $value4->locator_inventory;
            $detailmovement['ip_address'] = $value4->ip_address;
            $data_movement[] = $detailmovement;
    }

    

    try {
        // insert history
        db::beginTransaction();

        if ($is_costco==true) {
                DB::table('history_po_summary')->insert($data_po);
                DB::table('history_package_detail')->insert($data_package_detail);
                DB::table('history_package')->insert($data_package);
                DB::table('history_package_movements')->insert($data_movement);
                DB::table('history_package_detail_rasio')->insert($data_package_detail_rasio);

                if (!empty($data_integrate_cancel)) {
                    DB::table('package_cancel_integration')->insert($data_integrate_cancel);
                }
                // delete
                DB::table('package_movements')
                    ->whereIn('barcode_package', $list_bc)
                    ->delete();

                DB::table('package')
                    ->whereIn('scan_id', $list_scanid)
                    ->whereNull('deleted_at')
                    ->delete();

                DB::table('package_detail')
                    ->whereIn('scan_id', $list_scanid)
                    ->whereNull('deleted_at')
                    ->delete();

                DB::table('po_summary')
                    ->where('po_number', $po_number)
                    ->where('plan_ref_number', $plan_ref_number)
                    ->delete();

                DB::table('package_detail_rasio')
                    ->where('po_number_key',$po_number)
                    ->delete();

          }else{
                // insert history
              DB::table('history_po_summary')->insert($data_po);
              DB::table('history_package_detail')->insert($data_package_detail);
              DB::table('history_package')->insert($data_package);
              DB::table('history_package_movements')->insert($data_movement);

              if (!empty($data_integrate_cancel)) {
                    DB::table('package_cancel_integration')->insert($data_integrate_cancel);
                }

              // delete
              DB::table('package_movements')
                  ->whereIn('barcode_package', $list_bc)
                  ->delete();

              DB::table('package')
                  ->whereIn('scan_id', $list_scanid)
                  ->whereNull('deleted_at')
                  ->delete();

              DB::table('package_detail')
                  ->whereIn('scan_id', $list_scanid)
                  ->whereNull('deleted_at')
                  ->delete();

              DB::table('po_summary')
                  ->where('po_number', $po_number)
                  ->where('plan_ref_number', $plan_ref_number)
                  ->delete();
          }


        $data_response = [
          'status' => 200,
          'po_number' => $po_number,
          'output' => 'Data has been deleted'
        ];

        //


          DB::commit();
    } catch (Exception $ex) {
          db::rollback();
          $message = $ex->getMessage();
          ErrorHandler::db($message);

          $data_response = [
            'status' => 422,
            'po_number' => $po_number,
            'output' => 'Oops something wrong..!!'
          ];
    }

    return response()->json(['data' => $data_response]);

  }

    // request carton
    public function requestcarton(){

        $count_waiting = $this->getQtyRequestCarton(Auth::user()->factory_id, 'Waiting');
        $count_onprogress = $this->getQtyRequestCarton(Auth::user()->factory_id, 'onprogress');
        $count_hold = $this->getQtyRequestCarton(Auth::user()->factory_id, 'hold');
        $count_done = $this->getQtyRequestCarton(Auth::user()->factory_id, 'done');


        return view('packing/package_detail/request_carton',
          [
              'count_waiting' => $count_waiting,
              'count_onprogress' => $count_onprogress,
              'count_hold' => $count_hold,
              'count_done' => $count_done
          ]);
    }

    public function getDataRequestCarton(Request $request) {

        if ($request->radio_status == 'waiting') {
          $st_packing = null;
        }else {
          $st_packing = $request->radio_status;
        }

        if ($request->radio_status == 'all') {
          $data = DB::table('v_request_carton')
                      ->where('status_sewing', 'request')
                      ->where('factory_id', Auth::user()->factory_id);
        }else{
          $data = DB::table('v_request_carton')
                      ->where('status_sewing', 'request')
                      ->where('status_packing', $st_packing)
                      ->where('factory_id', Auth::user()->factory_id);
        }


        $data = $data->orderBy('request_at', 'asc');


        return Datatables::of($data)
              ->addColumn('checkbox', function ($data) {
                      return '<input type="checkbox" class="clrequest" name="selector[]" id="Inputselector" data-po="'.$data->po_number_concat.'" data-manufaturingsize="'.$data->manufacturing_size_concat.'" data-customersize="'.$data->customer_size_concat.'" data-id="'.$data->id.'">';
              })
              ->addColumn('btn_reject', function($data){
                  if ($data->status_packing == null) {
                    $button = '<button class="btn btn-danger btn-xs btn-reject" data-po="'.$data->po_number_concat.'" data-manufaturingsize="'.$data->manufacturing_size_concat.'" data-customersize="'.$data->customer_size_concat.'" data-id="'.$data->id.'" data-line="'.$data->line_id.'" value="" onclick="reject_carton(this);" data-toggle="tooltip" title="Hold"><i class="icon icon-database-remove" style="font-size:0.85em;"></i></button>
                      <button class="btn btn-primary btn-xs btn-onprogress" data-po="'.$data->po_number_concat.'" data-manufaturingsize="'.$data->manufacturing_size_concat.'" data-customersize="'.$data->customer_size_concat.'" data-id="'.$data->id.'" data-line="'.$data->line_id.'" value="" onclick="onprogress_carton(this);" data-toggle="tooltip" title="On Progress"><i class="icon icon-upload" style="font-size:0.85em;"></i></button>';
                  }elseif ($data->status_packing == 'onprogress') {
                    $button = '<button class="btn btn-danger btn-xs btn-reject" data-po="'.$data->po_number_concat.'" data-manufaturingsize="'.$data->manufacturing_size_concat.'" data-customersize="'.$data->customer_size_concat.'" data-id="'.$data->id.'" data-line="'.$data->line_id.'" value="" onclick="reject_carton(this);" data-toggle="tooltip" title="Hold"><i class="icon icon-database-remove" style="font-size:0.85em;"></i></button>
                    <button class="btn btn-success btn-xs btn-done" data-po="'.$data->po_number_concat.'" data-manufaturingsize="'.$data->manufacturing_size_concat.'" data-customersize="'.$data->customer_size_concat.'" data-id="'.$data->id.'" data-line="'.$data->line_id.'" value="" onclick="done_carton(this);" data-toggle="tooltip" title="Done"><i class="icon icon-finish" style="font-size:0.85em;"></i></button>';
                  }elseif ($data->status_packing == 'hold') {
                    $button = '<button class="btn btn-primary btn-xs btn-onprogress" data-po="'.$data->po_number_concat.'" data-manufaturingsize="'.$data->manufacturing_size_concat.'" data-customersize="'.$data->customer_size_concat.'" data-id="'.$data->id.'" data-line="'.$data->line_id.'" value="" onclick="onprogress_carton(this);" data-toggle="tooltip" title="On Progress"><i class="icon icon-upload" style="font-size:0.85em;"></i></button>';
                  }elseif ($data->status_packing == 'done') {
                    $button = '<button class="btn btn-danger btn-xs btn-reject" data-po="'.$data->po_number_concat.'" data-manufaturingsize="'.$data->manufacturing_size_concat.'" data-customersize="'.$data->customer_size_concat.'" data-id="'.$data->id.'" data-line="'.$data->line_id.'" value="" onclick="reject_carton(this);" data-toggle="tooltip" title="Hold"><i class="icon icon-database-remove" style="font-size:0.85em;"></i></button>';
                  }
                  // for sewing
                  // <button class="btn btn-success btn-xs btn-receive" data-po="'.$data->po_number_concat.'" data-manufaturingsize="'.$data->manufacturing_size_concat.'" data-customersize="'.$data->customer_size_concat.'" data-id="'.$data->id.'" data-line="'.$data->line_id.'" value="" onclick="receive_carton(this);" data-toggle="tooltip" title="Receive Sewing"><i class="icon icon-download" style="font-size:0.85em;"></i></button>

                  return $button;

              })
              ->editColumn('request_at', function($data){
                  return date('d-m-Y H:i:s', strtotime($data->request_at));
              })
              ->editColumn('status_packing', function($data){
                    if ($data->status_packing == 'onprogress') {
                      return '<span class="badge badge-primary position-right">ON PROGRESS</span>';
                    }elseif ($data->status_packing == 'done') {
                      return '<span class="badge badge-success position-right">DONE</span>';
                    }elseif ($data->status_packing == 'hold') {
                      return '<span class="badge badge-danger position-right">HOLD</span>';
                    }else{
                      return '<span class="badge badge-default position-right">Waiting</span>';
                    }
              })
              ->editColumn('status_sewing', function($data){
                    if ($data->status_sewing == 'request') {
                      return '<span class="badge badge-primary position-right">REQUEST</span>';
                    }elseif ($data->status_sewing == 'receive') {
                      return '<span class="badge badge-success position-right">RECEIVED</span>';
                    }
              })
              ->editColumn('manufacturing_size_concat', function($data){
                    $ex = explode(';', $data->manufacturing_size_concat);

                    $st = '';

                    for($i=0; $i<count($ex); $i++){

                      if (strpos($ex[$i], ',') == true) {
                        $ex[$i] = 'ALL Size';
                      }

                      $st = $st. $ex[$i].'; ';

                    }
                    return $st;
              })
              ->editColumn('info', function($data){
                if ($data->status_packing=='hold') {
                  return $data->info;
                }else{
                  return '-';
                }
              })
              ->rawColumns(['action', 'checkbox', 'btn_reject', 'status_packing', 'status_sewing', 'manufacturing_size_concat'])
              ->make(true);
    }

     public function reject(Request $request) {
      $po_number = $request->po_number;
      $id = $request->id;
      $result = $request->result;

      $userid = Auth::user()->id;

     //check request carton
      $check = $this->_check_request($id);
      if(!$check) {
          $error = 'request not found';
          return response()->json($error,422);
      }

      if($check->status_sewing == 'receive') { //jika status sewing sudah di receive, maka tidak bisa reject
          $error = 'this request received';
          return response()->json($error,422);
      }

      $data_movement = [
          'request_id' => $id,
          'status_sewing_from' => $check->status_sewing,
          'status_packing_from' => $check->status_packing,
          'status_packing_to' => 'hold',
          'user_id' => Auth::user()->id,
          'created_at' => Carbon::now(),
          'description' => 'Holded by packing',
          'info' => $result
      ];


      try {

          //  request movement
          $last_data = DB::table('request_movement')
                      ->where('request_id', $id)
                      ->where('is_canceled', false)
                      ->wherenull('deleted_at')
                      ->first();

          if(!$last_data) {
                throw new \Exception('Request not found');
            }
            else {
                DB::table('request_movement')
                ->where('id', $last_data->id)
                ->update([
                    'deleted_at' => Carbon::now()
                ]);
            }

          // insert history
          DB::table('request_movement')->insert($data_movement);

          // update request carton
          DB::table('request_carton')
              ->where('id', $id)
              ->update([
                  'status_packing' => 'hold',
                  // 'info' => $result,
                  // 'deleted_at' => Carbon::now(),
                  'updated_at' => Carbon::now(),
              ]);

          // update detail request carton
          DB::table('request_carton_detail')
              ->where('request_id', $id)
              ->update([
                  'updated_at' => Carbon::now()
                  // 'deleted_at' => Carbon::now()
              ]);

          $data_response = [
            'status' => 200,
            'po_number' => $po_number,
            'output' => 'Data has been Holded'
          ];


            DB::commit();
      } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);

            $data_response = [
              'status' => 422,
              'po_number' => $po_number,
              'output' => 'Oops something wrong..!!'
            ];
      }

      return response()->json(['data' => $data_response]);

    }

    public function progress(Request $request) {
      $po_number = $request->po_number;
      $id = $request->id;
      $result = $request->result;

      $userid = Auth::user()->id;

     //check request carton
      $check = $this->_check_request($id);
      if(!$check) {
          $error = 'request not found';
          return response()->json($error,422);
      }

      $data_movement = [
          'request_id' => $id,
          'status_sewing_from' => $check->status_sewing,
          'status_packing_from' => $check->status_packing,
          'status_packing_to' => 'onprogress',
          'user_id' => Auth::user()->id,
          'created_at' => Carbon::now(),
          'description' => 'onprogress by packing',
      ];


      try {

          //  request movement
          $last_data = DB::table('request_movement')
                      ->where('request_id', $id)
                      ->where('is_canceled', false)
                      ->wherenull('deleted_at')
                      ->first();

          if(!$last_data) {
                throw new \Exception('Request not found');
            }
            else {
                DB::table('request_movement')
                ->where('id', $last_data->id)
                ->update([
                    'deleted_at' => Carbon::now()
                ]);
            }

          // insert history
          DB::table('request_movement')->insert($data_movement);

          // update request carton
          DB::table('request_carton')
              ->where('id', $id)
              ->update([
                  'status_packing' => 'onprogress',
                  'updated_at' => Carbon::now(),
              ]);

          // update detail request carton
          DB::table('request_carton_detail')
              ->where('request_id', $id)
              ->update([
                  'updated_at' => Carbon::now()
              ]);

          $data_response = [
            'status' => 200,
            'po_number' => $po_number,
            'output' => 'Good Job..!!'
          ];


            DB::commit();
      } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);

            $data_response = [
              'status' => 422,
              'po_number' => $po_number,
              'output' => 'Oops something wrong..!!'
            ];
      }

      return response()->json(['data' => $data_response]);

    }

    public function done(Request $request) {
      $po_number = $request->po_number;
      $id = $request->id;
      $result = $request->result;

      $userid = Auth::user()->id;

     //check request carton
      $check = $this->_check_request($id);
      if(!$check) {
          $error = 'request not found';
          return response()->json($error,422);
      }

      $data_movement = [
          'request_id' => $id,
          'status_sewing_from' => $check->status_sewing,
          // 'status_sewing_to' => 'receive',
          'status_packing_from' => $check->status_packing,
          'status_packing_to' => 'done',
          'user_id' => Auth::user()->id,
          'created_at' => Carbon::now(),
          'description' => 'done by packing',
      ];


      try {

          //  request movement
          $last_data = DB::table('request_movement')
                      ->where('request_id', $id)
                      ->where('is_canceled', false)
                      ->wherenull('deleted_at')
                      ->first();

          if(!$last_data) {
                throw new \Exception('Request not found');
            }
            else {
                DB::table('request_movement')
                ->where('id', $last_data->id)
                ->update([
                    'deleted_at' => Carbon::now()
                ]);
            }

          // insert history
          DB::table('request_movement')->insert($data_movement);

          // update request carton
          DB::table('request_carton')
              ->where('id', $id)
              ->update([
                  // 'status_sewing' => 'receive',
                  'status_packing' => 'done',
                  'updated_at' => Carbon::now(),
              ]);

          // update detail request carton
          DB::table('request_carton_detail')
              ->where('request_id', $id)
              ->update([
                  'updated_at' => Carbon::now()
              ]);

          $data_response = [
            'status' => 200,
            'po_number' => $po_number,
            'output' => 'Good Job..!!'
          ];


            DB::commit();
      } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);

            $data_response = [
              'status' => 422,
              'po_number' => $po_number,
              'output' => 'Oops something wrong..!!'
            ];
      }

      return response()->json(['data' => $data_response]);

    }


    // receive sewing
    public function receive(Request $request) {
      $po_number = $request->po_number;
      $id = $request->id;
      $result = $request->result;

      $userid = Auth::user()->id;

     //check request carton
      $check = $this->_check_request($id);
      if(!$check) {
          $error = 'request not found';
          return response()->json($error,422);
      }

      $data_movement = [
          'request_id' => $id,
          'status_sewing_from' => $check->status_sewing,
          'status_sewing_to' => 'receive',
          'status_packing_from' => $check->status_packing,
          // 'status_packing_to' => 'done',
          'user_id' => Auth::user()->id,
          'created_at' => Carbon::now(),
          'description' => 'receive by sewing',
      ];


      try {

          //  request movement
          $last_data = DB::table('request_movement')
                      ->where('request_id', $id)
                      ->where('is_canceled', false)
                      ->wherenull('deleted_at')
                      ->first();

          if(!$last_data) {
                throw new \Exception('Request not found');
            }
            else {
                DB::table('request_movement')
                ->where('id', $last_data->id)
                ->update([
                    'deleted_at' => Carbon::now()
                ]);
            }

          // insert history
          DB::table('request_movement')->insert($data_movement);

          // update request carton
          DB::table('request_carton')
              ->where('id', $id)
              ->update([
                  'status_sewing' => 'receive',
                  // 'status_packing' => 'done',
                  'updated_at' => Carbon::now(),
              ]);

          // update detail request carton
          DB::table('request_carton_detail')
              ->where('request_id', $id)
              ->update([
                  'updated_at' => Carbon::now()
              ]);

          $data_response = [
            'status' => 200,
            'po_number' => $po_number,
            'output' => 'Good Job..!!'
          ];


            DB::commit();
      } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);

            $data_response = [
              'status' => 422,
              'po_number' => $po_number,
              'output' => 'Oops something wrong..!!'
            ];
      }

      return response()->json(['data' => $data_response]);

    }

    // get qty request carton
    public function getQtyRequestCarton($factory_id, $status){
        $data = DB::table('request_carton')
                ->join('line', 'line.id', '=', 'request_carton.line_id')
                ->where('line.factory_id', $factory_id)
                ->wherenull('request_carton.deleted_at')
                ->wherenull('line.deleted_at');

        if ($status == 'Waiting') {
          return $data = $data->where('request_carton.status_sewing','request')->wherenull('request_carton.status_packing')->count();
        }elseif ($status == 'onprogress') {
          return $data = $data->where('request_carton.status_packing', 'onprogress')->count();
        }elseif ($status == 'hold') {
          return $data = $data->where('request_carton.status_packing', 'hold')->count();
        }elseif ($status == 'done') {
          return $data = $data->where('request_carton.status_packing', 'done')->count();
        }else{
          return 0;
        }

    }

    private function _check_request($request_id)
    {
      $check = DB::table('request_carton')
                ->where('id', $request_id)
                ->wherenull('deleted_at')
                ->first();

      if (!$check) {
        return false;
      }else{
        return $check;
      }

    }

    // transfer style by po
    public function transferstyle(){
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('packing/package_detail/transfer_style')->with('factory', $factory);
    }

    public function getDataTransfer(Request $request) {
        $factory_id = $request->factory_id;

        $data = DB::table('po_summary')
                    ->select('po_number', 'customer_order_number', 'country', 'kst_lcdate', 'kst_statisticaldate', 'upc', 'trademark')
                    ->wherenull('deleted_at')
                    ->where('factory_id', $factory_id)
                    ->orderBy('kst_statisticaldate')
                    ->groupBy('po_number', 'customer_order_number', 'country', 'kst_lcdate', 'kst_statisticaldate', 'upc', 'trademark');


        return Datatables::of($data)
              ->addColumn('btn_action', function($data){
                  return '<input type="button" class="btn btn-danger btn-transfer" data-po="'.$data->po_number.'" value="Transfer Style" onclick="transfer_style(this);" >';
              })
              ->rawColumns(['action', 'btn_action'])
              ->make(true);
    }

    public function transferDataStyle(Request $request) {
      $po_number = $request->po_number;

      $userid = Auth::user()->id;

       if(false === ($content = @file_get_contents($this->api_url . $this->parameter[3] . '=' . $po_number))){
            return 'Syncing error, please checked your network';
        }

      $json = json_decode($content, true);
      $data = array_chunk($json['data'], 1000, true);

        try {
            db::beginTransaction();
            foreach ($data as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    $data = array(
                        'upc' => $value2['upc'],
                        'trademark' => $value2['brand']
                    );
                    $data_style[] = $data;
                }
            }

            DB::table('po_summary')
                    ->where('po_number', $po_number)
                    ->update($data_style[0]);


          $data_response = [
            'status' => 200,
            'po_number' => $po_number,
            'output' => 'Data has been transfered'
          ];

          //

            DB::commit();
      } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);

            $data_response = [
              'status' => 422,
              'po_number' => $po_number,
              'output' => 'Oops something wrong..!!'
            ];
      }

      return response()->json(['data' => $data_response]);

    }

    // package rasio
    public function packagerasio(){
      $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('packing/package_detail/package_rasio')->with('factory', $factory);
    }

    public function getDataRasio(Request $request) {
        $factory_id = $request->factory_id;

        $data = DB::table('package_detail')
                    ->select('po_number', 'plan_ref_number', 'scan_id', 'customer_number', 'customer_size', 'manufacturing_size', 'factory_id')
                    ->where('manufacturing_size', 'like', '%,%')
                    ->where('factory_id', $factory_id)
                    ->whereNull('deleted_at')
                    ->orderBy('po_number')
                    ->get();



        return Datatables::of($data)
              ->editColumn('manufacturing_size', function($data){
                  return '<div id="ms_'.$data->scan_id.'">
                              <input type="text"
                                  data-ponumber="'.$data->po_number.'"
                                  data-planrefnumber="'.$data->plan_ref_number.'"
                                  data-scanid="'.$data->scan_id.'"
                                  class="form-control ms_unfilled"
                                  value="'.$data->manufacturing_size.'">
                              </input>
                          </div>';
              })
              ->rawColumns(['action', 'btn_action', 'manufacturing_size'])
              ->make(true);
    }

    public function updateManufacturingSize(Request $request) {
        $scan_id = $request->scan_id;
        $po_number = $request->po_number;
        $plan_ref_number = $request->plan_ref_number;
        if(empty($scan_id)) {
            $manufacturing_size = null;
        }
        else {
            $manufacturing_size =  $request->manufacturing_size; //for database
        }

        //update
        try {
            DB::table('package_detail')
                ->where('scan_id', $scan_id)
                ->whereNull('deleted_at')
                ->update([
                    'manufacturing_size' => $manufacturing_size
                ]);
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }


        $html = '<input type="text"
                      data-ponumber="'.$po_number.'"
                      data-planrefnumber="'.$plan_ref_number.'"
                      data-scanid="'.$scan_id.'"
                      class="form-control ms_unfilled"
                      value="'.$manufacturing_size.'">
                  </input>';

        return response()->json($html, 200);
    }


    // po sync
    public function posync() {

        return view('packing/package_detail/po_sync');
    }

    // 
    public function getDataPoSync(Request $request) {
      $filterby = $request->filterby;
      $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
      $range = array(
          'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
          'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
      );

      $factory_name = trim(Auth::user()->factory_name);

      if ($factory_name == 'AOI1') {
        $stuffing_place_update = 'Packing AOI 1';
      }elseif ($factory_name == 'AOI2') {
        $stuffing_place_update = 'Packing AOI 2';
      }

      $data = DB::table('v_plan_load_sync')
                  ->whereNotNull('stuffing_place_update');

      if ($request->radio_status == 'po_') {
          $data = $data->where('po_number', $request->po);
      }elseif ($request->radio_status == 'stat') {
          $data = $data->where(function($query) use ($range) {
                      $query->whereBetween('podd_check', [$range['from'], $range['to']]);
                  });
      }elseif ($request->radio_status == 'stats') { // for filter in packing list 
        $date_range2 = explode('-', preg_replace('/\s+/', '', $request->statdate));
        $range2 = array(
            'from' => date_format(date_create($date_range2[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range2[1]), 'Y-m-d 23:59:59')
        );

        $data = $data->where(function($query) use ($range2) {
                        $query->whereBetween('podd_check', [$range2['from'], $range2['to']]);
                    })
                    ->where('stuffing_place_update', $stuffing_place_update);
      }

      //jika filterby tidak kosong
      if(!empty($filterby)) {
          $data = $data->where(function($query) use ($filterby) {
                      $query->where('po_number', 'like', '%'.$filterby.'%')
                              ->orWhere('style', 'like', '%'.$filterby.'%')
                              ->orWhere('stuffing_place_update', 'like', '%'.$filterby.'%')
                              ->orWhere('status', 'like', '%'.$filterby.'%');
                  });
      }

      $data = $data->orderBy('podd_check');


      return Datatables::of($data)
              ->editColumn('crd', function($data)
                {
                  return Carbon::parse($data->crd)->format('d/m/Y');
                })
              ->editColumn('kst_lcdate', function($data)
                {
                  return Carbon::parse($data->kst_lcdate)->format('d/m/Y');
                })
              ->editColumn('po_stat_date_adidas', function($data)
                {
                  return Carbon::parse($data->po_stat_date_adidas)->format('d/m/Y');
                })
              ->editColumn('podd_check', function($data)
                {
                  if ($data->podd_check!=null) {
                    return Carbon::parse($data->podd_check)->format('d/m/Y');
                  }
                 
                })
              ->editColumn('sewing_place_update', function($data)
                {
                  if ($data->sewing_place_update== 'Packing AOI 1') {
                    $string = 'AOI 1';
                  }elseif ($data->sewing_place_update== 'Packing AOI 2') {
                    $string = 'AOI 2';
                  }else{
                    $string = '-';
                  }

                  return $string;

                })
              ->editColumn('stuffing_place_update', function($data)
                {
                  if ($data->stuffing_place_update== 'Packing AOI 1') {
                    $string = 'AOI 1';
                  }elseif ($data->stuffing_place_update== 'Packing AOI 2') {
                    $string = 'AOI 2';
                  }else{
                    $string = '-';
                  }

                  return $string;
                  
                })
              ->editColumn('status', function($data)
                {
                  // $is_exists = DB::table('po_summary')
                  //               ->where('po_number', $data->po_number)
                  //               ->whereNull('deleted_at')
                  //               ->exists();

                    if ($data->status == 'uploaded') {
                      return '<span class="badge badge-success position-right">Uploaded</span>';
                    }elseif ($data->status == 'waiting upload') {
                      return '<span class="badge badge-default position-right">Waiting Upload</span>';
                    }
                })
                ->editColumn('info_order', function($data)
                  {
                      if ($data->is_cancel_order == true) {
                        return '<span class="badge badge-danger position-right">Cancel/Re-Route</span>';
                      }else {
                        return '-';
                      }
                  })
              ->rawColumns(['status', 'sewing_place_update', 'stuffing_place_update', 'info_order'])
              ->make(true);
  }

  public function exportPoSync(Request $request) {
      $orderby = $request->orderby;
      $direction = $request->direction;
      $filterby = $request->filterby;
      $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
      $range = array(
          'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
          'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
      );

      //
      $data = DB::table('po_summary_sync');

      if ($request->radio_status == 'po_') {
          $data = $data->where('po_number', $request->po);
      }elseif ($request->radio_status == 'stat') {
          $data = $data->where(function($query) use ($range) {
                      $query->whereBetween('podd_check', [$range['from'], $range['to']]);
                  });
      }

      //jika filterby tidak kosong
      if(!empty($filterby)) {
          $data = $data->where(function($query) use ($filterby) {
                      $query->where('po_number', 'like', '%'.$filterby.'%')
                              ->orWhere('style', 'like', '%'.$filterby.'%');
                  });
      }

      //jika orderby tidak undefined
      if($orderby != 'undefined') {
          $data = $data->orderBy($orderby, $direction);
      }
      else {
          $data = $data->orderBy('po_number', 'asc');
      }

      //naming file
      if ($request->radio_status == 'po_') {
          $filename = 'po_sync' . '_po_' . $request->po
                  . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
      }elseif ($request->radio_status == 'stat') {
          $filename = 'po_sync_' . $range['from'] . '_until_' . $range['to']
                  . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
      }

      $i = 1;

      $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
          $excel->sheet('report', function($sheet) use($data, $i, $range) {
              $sheet->appendRow(array(
                  '#', 'PO Number', 'Job', 'Article', 'Season', 'Style', 'New Qty', 'CRD', 'LC Date', 'Order Type', 'Document Type', 'PO Stat Date', 'PODD Check', 'Cust.OrderNo', 'Cust.No', 'Country Name', 'Sewing Place Update', 'Stuffing Place Update', 'Remaks', 'Status', 'Info'
              ));
              $data->chunk(100, function($rows) use ($sheet, $i, $range)
              {
                  foreach ($rows as $row)
                  {
                    $is_exists = DB::table('po_summary')
                                  ->where('po_number', $row->po_number)
                                  ->whereNull('deleted_at')
                                  ->exists();

                      if ($is_exists) {
                        $status = 'Uploaded';
                      }else{
                        $status = 'Waiting Upload';
                      }

                      if ($row->is_cancel_order == true || $row->is_re_route == true) {
                        $info_order = 'Cancel/Re-Route';
                      }else {
                        $info_order = '-';
                      }
                      //
                      $sheet->appendRow(array(
                          $i++, $row->po_number, $row->job, $row->article, $row->season, $row->style, $row->new_qty, Carbon::parse($row->crd)->format('m/d/Y'), Carbon::parse($row->kst_lcdate)->format('m/d/Y'), $row->order_type, $row->document_type, Carbon::parse($row->po_stat_date_adidas)->format('m/d/Y'), Carbon::parse($row->podd_check)->format('m/d/Y'), $row->customer_order_number, $row->customer_number, $row->country_name, $row->sewing_place_update, $row->stuffing_place_update, $row->remark, $status, $info_order
                      ));
                  }
              });
          });
      })->download('xlsx');

      return response()->json('Success exporting', 200);
  }


  

  


}