<?php

namespace App\Http\Controllers\Packing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

class HistoryRevisiController extends Controller
{
    public function index(){
        return view('packing/history_revisi/index');
    }

    public function getData(Request $request) {
        $data = DB::table('history_revisi_pl')
                    ->where([
                        ['po_number', $request->po_number],
                        ['is_used', false]
                    ])
                    ->whereNotNull('deleted_at')
                    ->orderBy('deleted_at', 'desc');

        return Datatables::of($data)
               ->editColumn('dimension', function($data) {
                   $length = $data->length * 1000;
                   $width = $data->width * 1000;
                   $height = $data->height * 1000;
                   return $length . 'x' .$width . 'x' .$height . ' (MM)';
               })
               ->editColumn('current_department', function($data) {
                   $current_department = '<span class="label label-flat border-grey text-grey-600">'.$data->current_department.'</span>';
                   return $current_department;
               })
               ->editColumn('current_status', function($data) {
                   if($data->current_status == 'onprogress') {
                       $current_status = '<td><span class="label label-primary" id="current_status_'.$data->barcode_id.'">ON PROGRESS</span></td>';
                   }
                   elseif($data->current_status == 'completed') {
                       $current_status = '<td><span class="label label-success" id="current_status_'.$data->barcode_id.'">COMPLETED</span></td>';
                   }
                   else {
                       $current_status = '<td><span class="label label-danger" id="current_status_'.$data->barcode_id.'">REJECTED</span></td>';
                   }
                   return $current_status;
               })
               ->rawColumns(['barcode_id', 'current_status', 'current_department'])
               ->make(true);
    }


}
