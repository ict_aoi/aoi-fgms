<?php

namespace App\Http\Controllers\Packing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use DataTables;
use Carbon\Carbon;

class InputPackageController extends Controller
{
    public function index()
    {
        $list_po = DB::table('po_summary')
                   ->select('po_number')
                   ->whereNull('deleted_at')
                   ->get();
        return view('packing/input_package/index')->with('list_po',$list_po);
    }

    public function ajaxSubmit(Request $request) {
        $po_number = trim($request->po_number);
        $from = $request->from;
        $to = $request->to;
        $serial_from = $request->serial_from;
        $serial_to = $request->serial_to;

        //data package
        for($i = $from; $i <= $to; $i++) {
            $data['barcode_id'] = $this->random_code($i);
            $data['scan_id'] = trim($request->scan_id);
            $data['package_number'] = $i;
            $data['serial_number'] = $serial_from++;
            $data['current_department'] = 'preparation';
            $data['current_status'] = 'onprogress';
            $data['status_preparation'] = 'onprogress';
            $data['created_at'] = Carbon::now();
            $data_package[] = $data;

            //data package movement
            $data_m = array(
                'barcode_id' => $data['barcode_id'],
                'current_department' => $data['current_department'],
                'current_status' => $data['current_status'],
                'created_at' => $data['created_at']
            );
            $data_movement[] = $data_m;
        }

        //data package detail
        $detail['scan_id'] = trim($request->scan_id);
        $detail['po_number'] = $po_number;
        $detail['package_range'] = $request->range;
        $detail['package_from'] = $request->from;
        $detail['package_to'] = $request->to;
        $detail['serial_from'] = $request->serial_from;
        $detail['serial_to'] = $request->serial_to;
        $detail['pack_instruction_code'] = $request->pack_instruction_code;
        $detail['line'] = $request->line;
        $detail['sku'] = $request->sku;
        $detail['buyer_item'] = $request->buyer_item;
        $detail['model_number'] = $request->model_number;
        $detail['customer_size'] = $request->customer_size;
        $detail['customer_number'] = $request->customer_number;
        $detail['manufacturing_size'] = $request->manufacturing_size;
        $detail['technical_print_index'] = $request->technical_print_index;
        $detail['gps_three_digit_size'] = $request->gps_three_digit_size;
        $detail['service_identifier'] = $request->service_identifier;
        $detail['packing_mode'] = $request->packing_mode;
        $detail['item_qty'] = $request->item_qty;
        $detail['inner_pack'] = $request->inner_pack;
        $detail['inner_pkg_count'] = $request->inner_pkg_count;
        $detail['pkg_count'] = $request->pkg_count;
        $detail['net_net'] = $request->net_net;
        $detail['net'] = $request->net;
        $detail['gross'] = $request->gross;
        $detail['unit_1'] = $request->unit_1;
        $detail['length'] = $request->length;
        $detail['width'] = $request->width;
        $detail['height'] = $request->height;
        $detail['r'] = $request->r_;
        $detail['pkg_code'] = $request->pkg_code;
        $detail['unit_2'] = $request->unit_2;
        $detail['created_at'] = Carbon::now();
        $data_package_detail[] = $detail;

        try {
            DB::begintransaction();

            //insert package detail
            DB::table('package_detail')->insert($data_package_detail);

            //insert package
            DB::table('package')->insert($data_package);

            //insert package movement
            DB::table('package_movement')->insert($data_movement);

            //update status po_summary
            DB::table('po_summary')->where('po_number', $po_number)
                                   ->update(
                                     ['updated_at' => Carbon::now()]
                                   );

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function random_code($string){
        return '3'.Carbon::now()->format('ymdhis').$string;
    }
}
