<?php

namespace App\Http\Controllers\Packing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class InputPackingPlanController extends Controller
{
    public function index()
    {
        return view('packing/input_packing_plan/index');
    }

    public function ajaxSubmit(Request $request) {
        $data_packing_plan = array([
            'plan_ref_number' => trim($request->plan_ref_number),
            'buyer_name' => $request->buyer_name,
            'buyer_address' => $request->buyer_address,
            'buyer_city' => $request->buyer_city,
            'buyer_postalcode' => $request->buyer_postalcode,
            'buyer_country' => $request->buyer_country,
            'buyer_phone' => $request->buyer_phone,
            'buyer_fax' => $request->buyer_fax,
            'destination_name' => $request->destination_name,
            'destination_address' => $request->destination_address,
            'destination_postalcode' => $request->destination_postalcode,
            'destination_country' => $request->destination_country,
            'created_at' => Carbon::now()
        ]);

        $data_po_summary = array([
            'po_number' => trim($request->po_number),
            'plan_ref_number' => trim($request->plan_ref_number),
            'created_at' => Carbon::now()
        ]);

        try {
            DB::begintransaction();
            //insert packing plan
            DB::table('packing_plan')->insert($data_packing_plan);

            //insert po
            DB::table('po_summary')->insert($data_po_summary);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
    }
}
