<?php

namespace App\Http\Controllers\Packing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use DataTables;
use Carbon\Carbon;

class SewingPlanController extends Controller
{
    public function index()
    {
        return view('packing/sewing_plan/index');
    }

    public function getData() {
        $data_test = (object)[];
        $data_test->po_number = 'test123';
        $data_test->start = '01-03-2018';
        $data_test->end = '01-03-2018';
        $data_test->qty = '10';
        $data[] = $data_test;

        return Datatables::of($data)
                ->addColumn('action', function($data) {
                    return view('_action', [
                                'model' => $data,
                                'edit' => route('packing.sewingPlan',
                                                ['po_number'  => $data->po_number]),
                                'delete' => route('packing.sewingPlan',
                                                ['po_number'  => $data->po_number])
                                ]);
                    })->make(true);
    }
}
