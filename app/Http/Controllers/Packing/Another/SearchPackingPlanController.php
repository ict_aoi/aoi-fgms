<?php

namespace App\Http\Controllers\Packing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use DataTables;

class SearchPackingPlanController extends Controller
{
    public function index()
    {
        return view('packing/search_packing_plan/index');
    }
}
