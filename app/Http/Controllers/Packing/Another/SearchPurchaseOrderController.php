<?php

namespace App\Http\Controllers\Packing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use DataTables;

class SearchPurchaseOrderController extends Controller
{
    public function index()
    {
        return view('packing/search_purchase_order/index');
    }

    public function getData(Request $request) {
        $ponumber = $request->ponumber;
        $data = DB::table('po_summary')
                ->where('po_number', $ponumber)
                ->whereNull('deleted_at')
                ->first();
        if(!$data) {
            return response()->json('Data not found',404);
        }
        $data->total_package = DB::table('package')
                                 ->join('package_detail','package_detail.scan_id',
                                        '=','package.scan_id')
                                 ->where('po_number', $ponumber)
                                 ->whereNull('package.deleted_at')
                                 ->count();

        return view('packing/search_purchase_order/ajax_po')->with('data',$data);
    }
}
