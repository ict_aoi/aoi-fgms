<?php

namespace App\Http\Controllers\Packing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use DataTables;

class InputPurchaseOrderController extends Controller
{
    public function index()
    {
        return view('packing/input_purchase_order/index');
    }
}
