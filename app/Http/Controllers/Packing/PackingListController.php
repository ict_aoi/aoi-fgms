<?php

namespace App\Http\Controllers\Packing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

class PackingListController extends Controller
{
    protected $api_url =  "http://mdware.aoi.co.id/api/packing/customer-information?";
    protected $parameter = array(
        'lc_date_from',
        'lc_date_to',
        'po_number'
    );

    public function index()
    {
        return view('packing/packing_list/index');
    }

    public function modalUpload()
    { //gak kepake
        return view('packing/packing_list/modal_upload');
    }

    public function getData(Request $request)
    {
        // if($request->date_range) {
        //     $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        //     $from = date_format(date_create($date_range[0]), 'Y-m-d 00:00:00');
        //     $to = date_format(date_create($date_range[1]), 'Y-m-d 23:59:59');
        //     $data = DB::table('sync_plan')
        //             ->whereBetween('dateordered', [$from, $to])
        //             ->orderBy('dateordered','asc')
        //             ->orderBy('datepromised','asc');
        // }
        // else {
        //     $data = DB::table('sync_plan')
        //             ->orderBy('dateordered','asc')
        //             ->orderBy('datepromised','asc');
        // }
        //
        // return Datatables::of($data)
        //         ->editColumn('total_package', function($data) {
        //             $ponumber = $this->removePrefix(trim($data->documentno));
        //             $total_package    = DB::table('package')
        //                                 ->join('package_detail','package_detail.scan_id','=','package.scan_id')
        //                                 ->where('po_number', $ponumber)
        //                                 ->whereNull('package.deleted_at')
        //                                 ->count();
        //             if($total_package == 0) {
        //                 $total_package = '-';
        //             }
        //             return $total_package;
        //         })
        //        ->addColumn('action', function($data) {
        //            $ponumber = $this->removePrefix(trim($data->documentno));
        //            return view('_action', [
        //                        'model' => $data,
        //                        'upload' => route('packing.modalUpload',
        //                                         ['po_number' => $ponumber]),
        //                        'detail' => route('packing.viewPackageDetail',
        //                                         ['po_number'  => $ponumber])
        //                     ]);
        //         })->make(true);

        //api

        if ($request->radio_status) {
            //filtering
            if ($request->radio_status == 'po') {
                if (empty($request->po_number)) {
                    $po_number = 'null';
                } else {
                    $po_number = $request->po_number;
                }

                $params =  $this->api_url . $this->parameter[2] . '=' . $po_number;
            } elseif ($request->radio_status == 'lc') {
                $counted_day = 0;
                if ($request->date_range) {
                    $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
                    $from = date_format(date_create($date_range[0]), 'Y-m-d');
                    $to = date_format(date_create($date_range[1]), 'Y-m-d');
                    //check date range
                    $date_to = Carbon::parse($date_range[0]);
                    $date_from = Carbon::parse($date_range[1]);
                    $counted_day = $date_to->diffInDays($date_from);
                } else {
                    $from = date_format(Carbon::now(), 'Y-m-d');
                    $to = date_format(Carbon::now(), 'Y-m-d');
                }

                if ($counted_day > 30) {
                    return response()->json('Date range should be less than 30 days', 422);
                }

                $params =  $this->api_url . $this->parameter[0] . '=' . $from . '&' . $this->parameter[1] . '=' . $to;
            }

            //check if po has been uploaded or not
            $check_po = DB::table('po_summary')
                ->select('po_number')
                ->whereNull('deleted_at')
                // ->where('po_number', 'like', '%'.trim($request->po_number).'%')
                ->where('po_number', trim($request->po_number))
                ->where('factory_id', Auth::user()->factory_id)
                ->count();


            if ($check_po > 0) {
                $dataxx = DB::table('po_summary')
                    // ->where('po_number', 'like', '%'.trim($request->po_number).'%')
                    ->whereNull('deleted_at')
                    ->where('po_number', trim($request->po_number))
                    ->where('factory_id', Auth::user()->factory_id)
                    ->orderBy('created_at', 'asc')
                    ->get();
                $data = json_decode(json_encode($dataxx), true);
            } else {
                //call api
                $json = json_decode(file_get_contents($params), true);
                $data = $json['data'];
            }


            return Datatables::of($data)
                ->editColumn('poreference', function ($data) {
                    return isset($data['poreference']) ? trim($data['poreference']) : trim($data['po_number']);
                })
                ->editColumn('plan_ref_number', function ($data) {
                    // return isset($data['plan_ref_number']) ? trim($data['plan_ref_number']) : '-';

                    $planref = isset($data['plan_ref_number']) ? trim($data['plan_ref_number']) : '-';
                    $orderno = isset($data['poreference']) ? trim($data['poreference']) : trim($data['po_number']);
                    if ($planref == '-') {
                        return  $planref;
                    } else {
                        return '<div id="planref_' . $orderno . '">
                                <input type="text"
                                    data-ponumber="' . $orderno . '"
                                    data-planrefnumber="' . $planref . '"
                                    class="form-control planref_unfilled"
                                    value="' . $planref . '">
                                </input>
                            </div>';
                    }
                })
                ->editColumn('total_package', function ($data) {

                    // $ponumber = $this->removePrefix(trim($data['documentno']));
                    $ponumber = isset($data['poreference']) ? trim($data['poreference']) : trim($data['po_number']);
                    $plan_ref_number = isset($data['plan_ref_number']) ? trim($data['plan_ref_number']) : '0';
                    // $customer_order_number = isset($data['kst_orderno']) ? $data['kst_orderno'] : $data['customer_order_number'];
                    if (isset($data['kst_orderno'])) {
                        $customer_order_number = $data['kst_orderno'];
                    } else {
                        if (isset($data['customer_order_number'])) {
                            $customer_order_number = $data['customer_order_number'];
                        } else {
                            $customer_order_number = null;
                        }
                    }
                    $total_package    = DB::table('package')
                        ->select('barcode_id')
                        ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
                        ->where('po_number', $ponumber)
                        ->where('plan_ref_number', $plan_ref_number)
                        ->where('factory_id', Auth::user()->factory_id)
                        ->whereNull('package.deleted_at')
                        ->count();
                    if ($total_package == 0) {
                        $total_package = '-';
                    }

                    // $trademark = isset($data['brand']) ? trim($data['brand']) : trim($data['trademark']);

                    if (isset($data['brand']) != null) {
                        $trademark = $data['brand'];
                    } else {
                        if (isset($data['trademark']) != null) {
                            $trademark = $data['trademark'];
                        } else {
                            $trademark = '-';
                        }
                    }

                    if ($data['recycle'] == 'Y') {
                        $isrecycle = 1;
                    } else {
                        $isrecycle = 0;
                    }

                    $oldpo = $ponumber;

                    $returned_value = '<span id="total_package_' . $ponumber . '">' . $total_package . '</span>
                                           <input type="hidden" id="buyer_name_' . $ponumber . '" value="' . $data['bp_name'] . '">
                                           <input type="hidden" id="destination_address_' . $ponumber . '" value="' . $data['address'] . '">
                                           <input type="hidden" id="destination_city_' . $ponumber . '" value="' . $data['city'] . '">
                                           <input type="hidden" id="destination_postal_' . $ponumber . '" value="' . $data['postal'] . '">
                                           <input type="hidden" id="destination_country_' . $ponumber . '" value="' . $data['country'] . '">
                                           <input type="hidden" id="dateordered_' . $ponumber . '" value="' . $data['dateordered'] . '">
                                           <input type="hidden" id="datepromised_' . $ponumber . '" value="' . $data['datepromised'] . '">
                                           <input type="hidden" id="grandtotal_' . $ponumber . '" value="' . $data['grandtotal'] . '">
                                           <input type="hidden" id="bp_code_' . $ponumber . '" value="' . $data['bp_code'] . '">
                                           <input type="hidden" id="phone_' . $ponumber . '" value="' . $data['phone'] . '">
                                           <input type="hidden" id="fax_' . $ponumber . '" value="' . $data['fax'] . '">
                                           <input type="hidden" id="kst_lcdate_' . $ponumber . '" value="' . $data['kst_lcdate'] . '">
                                           <input type="hidden" id="kst_statisticaldate_' . $ponumber . '" value="' . $data['kst_statisticaldate'] . '">
                                           <input type="hidden" id="trademark_' . $ponumber . '" value="' . $trademark . '">
                                           <input type="hidden" id="made_in_' . $ponumber . '" value="INDONESIA">
                                           <input type="hidden" id="country_of_origin_' . $ponumber . '" value="INDONESIA">
                                           <input type="hidden" id="commodities_type_' . $ponumber . '" value="GARMENT">
                                           <input type="hidden" id="customer_order_number_' . $ponumber . '" value="' . $customer_order_number . '">
                                           <input type="hidden" id="remark_' . $ponumber . '" value="' . $data['remark'] . '">
                                           <input type="hidden" id="upc_' . $ponumber . '" value="' . $data['upc'] . '">
                                           <input type="hidden" id="season_' . $ponumber . '" value="' . $data['season'] . '">
                                           <input type="hidden" id="oldpo_' . $ponumber . '" value="' . $oldpo . '">
                                           <input type="hidden" id="recycle_' . $ponumber . '" value="' . $isrecycle . '">
                                          ';

                    return $returned_value;
                })
                ->addColumn('action', function ($data) {
                    // $ponumber = trim($data->po_number);
                    $ponumber = isset($data['poreference']) ? trim($data['poreference']) : trim($data['po_number']);
                    $plan_ref_number = isset($data['plan_ref_number']) ? trim($data['plan_ref_number']) : '';

                    //check if po has been uploaded or not
                    $check = DB::table('po_summary')
                        ->where('po_number', $ponumber)
                        ->whereNull('deleted_at')
                        ->where('factory_id', Auth::user()->factory_id)
                        // ->whereNull('deleted_at')
                        ->first();

                    $cplanR = DB::table('package_detail_rasio')
                        ->select('plan_ref_number')
                        ->where('plan_ref_number', $plan_ref_number)
                        ->first();

                    if (!$check) {
                        return view('_action', [
                            'model' => $data,
                            'upload' => route(
                                'packing.modalUpload',
                                [
                                    'po_number'  => $ponumber
                                ]
                            ),
                            'detail' => route(
                                'packing.viewPackageDetail',
                                [
                                    'po_number'  => $ponumber,
                                    'plan_ref_number'  => $plan_ref_number
                                ]
                            )
                        ]);
                    } else {
                        if (isset($data['is_costco']) == 'true' && !$cplanR) {
                            return view('_action', [
                                'model' => $data,
                                'detail' => route(
                                    'packing.viewPackageDetail',
                                    [
                                        'po_number'  => $ponumber,
                                        'plan_ref_number'  => $plan_ref_number
                                    ]
                                ),
                                'revisi' => route(
                                    'packing.getCurrentPackingList',
                                    [
                                        'po_number'  => $ponumber,
                                        'plan_ref_number'  => $plan_ref_number
                                    ]
                                ),
                                'costco' => route(
                                    'packing.getCostco',
                                    [
                                        'po_number'  => $ponumber,
                                        'plan_ref_number'  => $plan_ref_number
                                    ]
                                ),
                                'editpl' => route(
                                    'packing.editPl',
                                    [
                                        'po_number'  => $ponumber,
                                        'plan_ref_number'  => $plan_ref_number
                                    ]
                                ),
                            ]);
                        } else if (isset($data['is_costco']) == 'true' && is_null($cplanR) == false) {
                            return view('_action', [
                                'model' => $data,
                                'detail' => route(
                                    'packing.viewPackageDetail',
                                    [
                                        'po_number'  => $ponumber,
                                        'plan_ref_number'  => $plan_ref_number
                                    ]
                                ),
                                'revisi' => route(
                                    'packing.getCurrentPackingList',
                                    [
                                        'po_number'  => $ponumber,
                                        'plan_ref_number'  => $plan_ref_number
                                    ]
                                ),
                                'detcostco' => route(
                                    'packing.viewCostcoDetail',
                                    [
                                        'po_number'  => $ponumber,
                                        'plan_ref_number'  => $plan_ref_number
                                    ]
                                ),
                                'editpl' => route(
                                    'packing.editPl',
                                    [
                                        'po_number'  => $ponumber,
                                        'plan_ref_number'  => $plan_ref_number
                                    ]
                                ),
                            ]);
                        } else {
                            return view('_action', [
                                'model' => $data,
                                'detail' => route(
                                    'packing.viewPackageDetail',
                                    [
                                        'po_number'  => $ponumber,
                                        'plan_ref_number'  => $plan_ref_number
                                    ]
                                ),
                                'revisi' => route(
                                    'packing.getCurrentPackingList',
                                    [
                                        'po_number'  => $ponumber,
                                        'plan_ref_number'  => $plan_ref_number
                                    ]
                                ),
                                'editpl' => route(
                                    'packing.editPl',
                                    [
                                        'po_number'  => $ponumber,
                                        'plan_ref_number'  => $plan_ref_number
                                    ]
                                ),
                            ]);
                        }
                    }
                })
                ->addColumn('is_completed', function ($data) {
                    $ponumber = isset($data['poreference']) ? trim($data['poreference']) : trim($data['po_number']);
                    $plan_ref_number = isset($data['plan_ref_number']) ? trim($data['plan_ref_number']) : '0';

                    $get_package = DB::table('package')
                        ->select('barcode_id')
                        ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
                        ->where('po_number', $ponumber)
                        ->where('plan_ref_number', $plan_ref_number)
                        ->where('factory_id', Auth::user()->factory_id)
                        ->where('is_printed', true)
                        ->whereNull('package.deleted_at')
                        ->count();

                    if ($get_package > 0) {
                        $is_completed = '<span class="label label-success label-rounded">
                                                <i class="icon-checkmark2"></i>
                                            </span><span class="badge badge-success position-right">' . $get_package . '</span>';
                    } else {
                        $is_completed = '<span class="label label-default label-rounded">
                                                <i class="icon-cross3"></i>
                                            </span>';
                    }

                    return $is_completed;
                })
                ->rawColumns(['plan_ref_number', 'total_package', 'action', 'is_completed'])
                ->make(true);
        } else {
            $data = array();
            return Datatables::of($data)
                ->editColumn('total_package', function ($data) {
                    return null;
                })
                ->addColumn('action', function ($data) {
                    return null;
                })
                ->rawColumns(['total_package', 'action'])
                ->make(true);
        }
    }

    public function updatePlanref(Request $request)
    {

        $po_number = $request->po_number;
        $planref = $request->planref;
        $new_plan_ref = $request->new_plan_ref;

        try {
            db::beginTransaction();
            $cek_invoice = DB::table('po_stuffing')
                ->where('po_number', $po_number)
                ->where('plan_ref_number', $planref)
                ->whereNotNull('invoice')
                ->whereNull('deleted_at')
                ->exists();

            if ($cek_invoice) {
                return response()->json('Invoice already created, please contact the Exim Department to cancel before..!', 422);
            }

            DB::table('po_summary')
                ->where('po_number', $po_number)
                ->where('plan_ref_number', $planref)
                ->whereNull('deleted_at')
                ->update(['plan_ref_number' => $new_plan_ref, 'updated_at' => Carbon::now()]);
            DB::table('package_detail')
                ->where('po_number', $po_number)
                ->where('plan_ref_number', $planref)
                ->whereNull('deleted_at')
                ->update(['plan_ref_number' => $new_plan_ref, 'updated_at' => Carbon::now()]);

            db::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        $html = '<input type="text" id="planref_' . $request->po_number . '" data-id="' . $request->id . '" data-po="' . $request->po_number . '" data-plan="' . $request->plan_ref_number . '" class="form-control planref_unfilled" value="' . $request->plan_ref_number . '">';

        return response()->json($html, 200);
    }

    //verify upload excel
    public function uploadExcel(Request $request)
    {
        if (substr($request->po_number, 0, 2) == 'SO') {
            $ponumber = explode("-", trim($request->po_number))[1];
        } else {
            $ponumber = trim($request->po_number);
        }

        $plan_ref_number = trim($request->plan_ref_number);

        if ($plan_ref_number == '') {
            return response()->json('Plan Ref Number not found', 422);
        }

        if ($request->costco == 'on') {
            $costco = true;
        } else {
            $costco = false;
        }

        $oldpo = $request->oldpo;

        //data po_summary
        $data_po_summary = array(
            'po_number' => $ponumber,
            'plan_ref_number' => $plan_ref_number,
            'dateordered' => $request->dateordered,
            'datepromised' => $request->datepromised,
            'grandtotal' => $request->grandtotal,
            'bp_name' => $request->bp_name,
            'bp_code' => $request->bp_code,
            'phone' => $request->phone,
            'fax' => $request->fax,
            'address' => $request->address,
            'city' => $request->city,
            'postal' => $request->postal,
            'country' => $request->country,
            'kst_lcdate' => $request->kst_lcdate,
            'kst_statisticaldate' => $request->kst_statisticaldate,
            'trademark' => $request->trademark,
            'country_of_origin' => $request->country_of_origin,
            'made_in' => $request->made_in,
            'commodities_type' => $request->commodities_type,
            'customer_order_number' => $request->customer_order_number,
            'remark' => $request->remark,
            'upc' => $request->upc,
            'is_costco' => $costco,
            'factory_id' => Auth::user()->factory_id,
            'created_at' => Carbon::now(),
            'old_po' => $oldpo,
            'season' => $request->season,
            'recycle' => isset($request->recycle) == 1 ? true : false
        );

        //
        $results = array();
        if ($request->hasFile('file_upload')) {
            $extension = \File::extension($request->file_upload->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls") {
                $path = $request->file_upload->getRealPath();
                $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
            }
        }



        $data_package = array();
        $data_movement = array();
        $data_package_detail = array();
        $count_ponumber = 0;
        $count_inserted_data = 0;
        foreach ($datax as $key => $value) {
            if ($value != "") {


                //check apakah header unit sudah diubah menjadi unit1 dan unit2
                if (!isset($value->unit1) || !isset($value->unit2)) {
                    return response()->json('Header `unit1` dan `unit2` tidak ditemukan', 422);
                }
                $po_number = trim($value->po);
                if ($po_number != $ponumber) {
                    // return response()->json('Cek PO Number pada packinglist',422);
                    continue;
                }

                //check apakah kolom from, to, scan_id ada valuenya tidak
                if ($value->from == '') {
                    return response()->json('Kolom `from` harus terisi', 422);
                }

                if ($value->to == '') {
                    return response()->json('Kolom `to` harus terisi', 422);
                }

                if ($value->scan_id == '') {
                    return response()->json('Kolom `scan id` harus terisi', 422);
                }

                if ($value->qty_per_pkginner_pack == '' || $value->qty_per_pkginner_pack == 0 || $value->item_qty == '' || $value->item_qty == 0) {
                    return response()->json('Inner pack & item qty tidak boleh kosong / 0', 422);
                }

                if ($this->_scan_id_check($value->scan_id) == false) {
                    return response()->json('Check scan id packinglist, Scan Id sudah ada', 422);
                }

                $count_ponumber++;
                $from = $value->from;
                $to = $value->to;
                $serial_from = $value->serial_from;
                $serial_to = $value->serial_to;
                $description = 'upload package';
                $department = 'preparation';
                $status = 'onprogress';

                //data package
                for ($i = $from; $i <= $to; $i++) {
                    $count_inserted_data++;

                    $data['barcode_id'] = $this->random_code($i);
                    $data['scan_id'] = trim($value->scan_id);
                    $data['package_number'] = $i;
                    $data['serial_number'] = $serial_from++;
                    $data['current_department'] = 'preparation';
                    $data['current_status'] = 'onprogress';
                    $data['status_preparation'] = 'onprogress';
                    $data['created_at'] = Carbon::now();
                    $data_package[] = $data;

                    //data package movement
                    $data_m = array(
                        'barcode_package' => $data['barcode_id'],
                        'department_to' => $department,
                        'status_to' => $status,
                        'user_id' => Auth::user()->id,
                        'description' => $description,
                        'created_at' => Carbon::now()
                    );
                    $data_movement[] = $data_m;
                }

                //data package detail
                $detail['scan_id'] = trim($value->scan_id);
                $detail['po_number'] = $po_number;
                $detail['plan_ref_number'] = $plan_ref_number;
                $detail['package_range'] = $value->range;
                $detail['package_from'] = $value->from;
                $detail['package_to'] = $value->to;
                $detail['serial_from'] = $value->serial_from;
                $detail['serial_to'] = $value->serial_to;
                $detail['pack_instruction_code'] = $value->pack_instruction_code;
                $detail['line'] = $value->line;
                $detail['sku'] = $value->sku;
                $detail['buyer_item'] = trim($value->buyer_item);
                $detail['model_number'] = $value->model_number;
                $detail['customer_size'] = trim($value->customer_size);
                $detail['customer_number'] = $value->customer_number;
                $detail['manufacturing_size'] = trim($value->manufacturing_size);
                $detail['technical_print_index'] = $value->technical_print_index;
                $detail['gps_three_digit_size'] = $value->gps_three_digit_size;
                $detail['service_identifier'] = $value->vasshas_g01_service_identifier;
                $detail['packing_mode'] = $value->vasshas_l15_packing_mode;
                $detail['item_qty'] = str_replace(array('.', ','), '', $value->item_qty);
                $detail['inner_pack'] = $value->qty_per_pkginner_pack;
                $detail['inner_pkg_count'] = $value->inner_pkg_count;
                $detail['pkg_count'] = $value->pkg_count;
                $detail['net_net'] = $value->net_net;
                $detail['net'] = $value->net;
                $detail['gross'] = $value->gross;
                $detail['unit_1'] = $value->unit1;
                $detail['length'] = $value->l;
                $detail['width'] = $value->w;
                $detail['height'] = $value->h;
                $detail['r'] = $value->r;
                $detail['pkg_code'] = $value->pkg_code;
                $detail['unit_2'] = $value->unit2;
                $detail['rasio'] = $this->_rasio_check($value->manufacturing_size);
                $detail['created_at'] = Carbon::now();
                $detail['factory_id'] = Auth::user()->factory_id;
                $detail['old_po'] = $oldpo;
                $data_package_detail[] = $detail;
            }
        }

        $getPackageTo = array();
        $getPackageFrom = array();

        foreach ($data_package_detail as $key_ => $val_) {
            $getPackageTo[$key] = $val_['package_to'];
            $getPackageFrom[$key] = $val_['package_from'];
        }

        //
        if ($count_ponumber == 0) {
            return response()->json('PO Number not found', 422);
        }

        try {
            DB::begintransaction();

            //check apakah po dan plan ref number pernah diupload
            $check = DB::table('po_summary')
                ->where('po_number', $ponumber)
                ->where('plan_ref_number', $plan_ref_number)
                ->where('factory_id', Auth::user()->factory_id)
                ->whereNull('deleted_at')
                ->first();

            if ($check) {
                throw new \Exception('Plan Ref Number already exists');
            } else {
                //insert status po_summary
                DB::table('po_summary')->insert($data_po_summary);

                //insert package detail
                DB::table('package_detail')->insert($data_package_detail);

                //insert package
                DB::table('package')->insert($data_package);

                //insert package movement
                DB::table('package_movements')->insert($data_movement);
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json($count_inserted_data, 200);
    }

    //revisi excel
    public function revisiExcel(Request $request)
    {
        if (substr($request->po_number, 0, 2) == 'SO') {
            $ponumber = explode("-", trim($request->revisi_po_number))[1];
        } else {
            $ponumber = trim($request->revisi_po_number);
        }

        $plan_ref_number = trim($request->revisi_plan_ref_number);
        $plan_ref_number_new = trim($request->plan_ref_number_new);

        $cekpo = DB::table('po_summary')
            ->where('po_number', $ponumber)
            ->where('plan_ref_number', $plan_ref_number_new)
            ->where('factory_id', Auth::user()->factory_id)
            ->first();
        if ($cekpo != null) {
            return response()->json('Plan Ref. Number was already', 422);
        }

        $oldpo = $request->revisi_oldpo;
        //data po_summary
        $data_po_summary = array(
            'po_number' => $ponumber,
            'plan_ref_number' => $plan_ref_number_new,
            'dateordered' => $request->revisi_dateordered,
            'datepromised' => $request->revisi_datepromised,
            'grandtotal' => $request->revisi_grandtotal,
            'bp_name' => $request->revisi_bp_name,
            'bp_code' => $request->revisi_bp_code,
            'phone' => $request->revisi_phone,
            'fax' => $request->revisi_fax,
            'address' => $request->revisi_address,
            'city' => $request->revisi_city,
            'postal' => $request->revisi_postal,
            'country' => $request->revisi_country,
            'kst_lcdate' => $request->revisi_kst_lcdate,
            'kst_statisticaldate' => $request->revisi_kst_statisticaldate,
            'trademark' => $request->revisi_trademark,
            'country_of_origin' => $request->revisi_country_of_origin,
            'made_in' => $request->revisi_made_in,
            'commodities_type' => $request->revisi_commodities_type,
            'customer_order_number' => $request->revisi_customer_order_number,
            'remark' => $request->revisi_remark,
            'upc' => $request->revisi_upc,
            'factory_id' => Auth::user()->factory_id,
            'created_at' => Carbon::now(),
            'old_po' => $oldpo,
            'season' => $request->revisi_season,
            'recycle' => isset($request->revisi_recycle) == 1 ? true : false
        );



        //file excel
        $filename = '';
        $results = array();
        if ($request->hasFile('revisi_file_upload')) {
            $extension = \File::extension($request->revisi_file_upload->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls") {
                $path = $request->revisi_file_upload->getRealPath();
                $filename = $request->revisi_file_upload->getClientOriginalName();
                $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
            }
        }

        //ambil data current packinglist


        $data_package_detail = array(); //data moved to table revisi_pl_history
        $data_package = array(); //data inserted to table package_detail
        $data_insert_package = array(); //data inserted to table package
        $data_movement = array(); //data inserted to table package_movements
        // $list_scanid = array(); //list scan_id for delete
        $ddmmyy = date_format(Carbon::now(), "d_m_Y");
        $count_inserted_data = 0;
        $count_ponumber = 0;

        foreach ($datax as $key => $value) {
            if (!isset($value->unit1) || !isset($value->unit2)) {
                return response()->json('Header `unit1` dan `unit2` tidak ditemukan', 422);
            }
            $po_number = trim($value->po);
            if ($po_number != $ponumber) {
                // return response()->json('Cek PO Number pada packinglist',422);
                continue;
            }

            //check apakah kolom from, to, scan_id ada valuenya tidak
            if ($value->from == '') {
                return response()->json('Kolom `from` harus terisi', 422);
            }

            if ($value->to == '') {
                return response()->json('Kolom `to` harus terisi', 422);
            }

            if ($value->scan_id == '') {
                return response()->json('Kolom `scan id` harus terisi', 422);
            }

            if ($value->qty_per_pkginner_pack == '' || $value->qty_per_pkginner_pack == 0 || $value->item_qty == '' || $value->item_qty == 0) {
                return response()->json('Inner pack & item qty tidak boleh kosong / 0', 422);
            }

            if ($this->_scan_id_check($value->scan_id) == false) {
                return response()->json('Check scan id packinglist, Scan Id sudah ada', 422);
            }

            $detail['scan_id'] = trim($value->scan_id);
            $detail['po_number'] = $po_number;
            $detail['plan_ref_number'] = $plan_ref_number_new;
            $detail['package_range'] = $value->range;
            $detail['package_from'] = $value->from;
            $detail['package_to'] = $value->to;
            $detail['serial_from'] = $value->serial_from;
            $detail['serial_to'] = $value->serial_to;
            $detail['pack_instruction_code'] = $value->pack_instruction_code;
            $detail['line'] = $value->line;
            $detail['sku'] = $value->sku;
            $detail['buyer_item'] = trim($value->buyer_item);
            $detail['model_number'] = $value->model_number;
            $detail['customer_size'] = trim($value->customer_size);
            $detail['customer_number'] = $value->customer_number;
            $detail['manufacturing_size'] = trim($value->manufacturing_size);
            $detail['technical_print_index'] = $value->technical_print_index;
            $detail['gps_three_digit_size'] = $value->gps_three_digit_size;
            $detail['service_identifier'] = $value->vasshas_g01_service_identifier;
            $detail['packing_mode'] = $value->vasshas_l15_packing_mode;
            $detail['item_qty'] = str_replace(array('.', ','), '', $value->item_qty);
            $detail['inner_pack'] = $value->qty_per_pkginner_pack;
            $detail['inner_pkg_count'] = $value->inner_pkg_count;
            $detail['pkg_count'] = $value->pkg_count;
            $detail['net_net'] = $value->net_net;
            $detail['net'] = $value->net;
            $detail['gross'] = $value->gross;
            $detail['unit_1'] = $value->unit1;
            $detail['length'] = $value->l;
            $detail['width'] = $value->w;
            $detail['height'] = $value->h;
            $detail['r'] = $value->r;
            $detail['pkg_code'] = $value->pkg_code;
            $detail['unit_2'] = $value->unit2;
            $detail['rasio'] = $this->_rasio_check($value->manufacturing_size);
            $detail['created_at'] = Carbon::now();
            $detail['factory_id'] = Auth::user()->factory_id;
            $detail['old_po'] = $oldpo;
            $data_package_detail[] = $detail;

            $count_ponumber++;
            $from = $value->from;
            $to = $value->to;
            $serial_from = $value->serial_from;
            $serial_to = $value->serial_to;
            $description = 'upload package';
            $department = 'preparation';
            $status = 'onprogress';

            for ($i = $from; $i <= $to; $i++) {
                $count_inserted_data++;
                $data['barcode_id'] = $this->random_code($i);
                $data['scan_id'] = trim($value->scan_id);
                $data['package_number'] = $i;
                $data['serial_number'] = $serial_from++;
                $data['current_department'] = $department;
                $data['current_status'] = $status;
                $data['status_preparation'] = $status;
                $data['created_at'] = Carbon::now();
                $data_package[] = $data;

                $data_m = array(
                    'barcode_package' => $data['barcode_id'],
                    'department_to' => $department,
                    'status_to' => $status,
                    'user_id' => Auth::user()->id,
                    'description' => $description,
                    'created_at' => Carbon::now()
                );
                $data_movement[] = $data_m;
            }
        }


        try {

            DB::begintransaction();
            $check = DB::table('po_summary')
                ->where('po_number', $ponumber)
                ->where('plan_ref_number', $plan_ref_number_new)
                ->where('factory_id', Auth::user()->factory_id)
                ->whereNull('deleted_at')
                ->first();

            if ($check) {
                throw new \Exception('Plan Ref Number already exists');
            } else {
                //insert status po_summary
                DB::table('po_summary')->insert($data_po_summary);

                //insert package detail
                DB::table('package_detail')->insert($data_package_detail);

                //insert package
                DB::table('package')->insert($data_package);

                //insert package movement
                DB::table('package_movements')->insert($data_movement);
            }


            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        //get total package
        $total_package = DB::table('package')
            ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
            ->where('po_number', $ponumber)
            ->whereNull('package.deleted_at')
            ->whereNull('package_detail.deleted_at')
            ->count();
        return response()->json($total_package, 200);
    }

    //get current packing list
    public function getCurrentPackingList(Request $request)
    {
        $ponumber = $request->ponumber;
        $data = DB::table('package_detail')
            ->where('po_number', $ponumber)
            ->where('factory_id', Auth::user()->factory_id)
            ->whereNull('deleted_at')
            ->get();

        return view('packing/packing_list/current_packing_list')->with('data', $data);
    }

    //get detail of po number, hanya digunakan jika data utk menampilkan packing list menggunakan table sync.
    public function getDetail(Request $request)
    {
        $po_number = $this->addPrefix(trim($request->po_number));
        $data = DB::table('sync_plan')
            ->where('documentno', $po_number)
            ->first();

        return response()->json($data, 200);
    }

    //add prefix, ex: string -> SO-string-0
    public function addPrefix($string)
    {
        $new_string = array('SO-', $string, '-0');
        $new_string = implode($new_string);
        return $new_string;
    }

    //remove prefix, ex: SO-string-0 -> string
    public function removePrefix($string)
    {
        $new_string = explode("-", $string)[1];
        return $new_string;
    }

    //HALAMAN VIEW PACKAGE
    public function viewPackage(Request $request)
    {
        $po_number = trim($request->po_number);
        $plan_ref_number = trim($request->plan_ref_number);
        return view('packing/packing_list/detail')->with('po_number', $po_number)
            ->with('plan_ref_number', $plan_ref_number);
    }

    //HALAMAN VIEW PACKAGE WITH PLAN REF NUMBER
    public function viewPackagePlanRefNumber(Request $request)
    {
        $po_number = trim($request->po_number);
        $plan_ref_number = trim($request->plan_ref_number);
        return view('packing/packing_list/detail_plan')->with('po_number', $po_number)
            ->with('plan_ref_number', $plan_ref_number);
    }

    //DATA FOR DATATABLES VIEW PACKAGE
    public function viewPackageData(Request $request)
    {
        $ponumber = trim($request->po_number);
        $plan_ref_number = trim($request->plan_ref_number);
        $data     = DB::table('package')
            ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
            ->where('po_number', $ponumber)
            ->where('plan_ref_number', $plan_ref_number)
            ->where('factory_id', Auth::user()->factory_id)
            ->whereNull('package.deleted_at')
            ->orderBy('package.package_number', 'asc');


        return Datatables::of($data)
            ->editColumn('barcode_id', function ($data) {
                $detail = '<a href="#" class="modal_package"
                                    [detail]
                                   </a>';
                return $data->barcode_id . ' ' . $detail;
            })
            ->editColumn('is_printed', function ($data) {
                if ($data->is_printed) {
                    $iscompleted = '<span class="label label-success label-rounded"
                                            id="completed_' . $data->barcode_id . '">
                                            <i class="icon-checkmark2"></i>
                                           </span>';
                } else {
                    $iscompleted = '<span class="label label-default label-rounded"
                                            id="completed_' . $data->barcode_id . '">
                                            <i class="icon-cross3"></i>
                                           </span>';
                }
                return $iscompleted;
            })
            ->editColumn('dimension', function ($data) {
                $dimension = $data->length . ' x ' . $data->width . ' x ' . $data->height . ' (' . $data->unit_2 . ')';
                return $dimension;
            })
            ->addColumn('action', function ($data) {
                return view('_action', [
                    'model' => $data,
                    'button_mode' => true,
                    'is_printed' => route('packing.ajaxSetCompleted', [
                        'barcodeid' => $data->barcode_id,
                        'current_is_printed' => $data->is_printed
                    ])
                ]);
            })
            ->rawColumns(['barcode_id', 'is_printed', 'action'])
            ->make(true);
    }

    //set package as completed
    public function setCompleted(Request $request)
    {
        $barcodeid = trim($request->barcode_id_shippingmark);
        $current_ = $request->_current_is_printed;
        $department_from = 'preparation';
        $department_to = 'preparation';
        $status_to = 'completed';
        $template = $request->template_shippingmark;
        $description = 'print barcode and shipping mark (template: ' . $template . ')';


        // cek customer order egypt = 856010
        $check_po = DB::table('po_summary')
            ->select('bp_code', 'country')
            ->where('po_number', $request->po_number)
            ->where('plan_ref_number', $request->plan_ref_number)
            ->whereNull('deleted_at')
            ->first();

        if ($check_po->bp_code == '856010') {
            if ($template != 'sm_egypt_a5') {
                return response()->json('please choose another template', 422);
            }
        }

        //check status package dan is printed
        $check = DB::table('package')
            ->select('current_status', 'is_printed')
            ->where('barcode_id', $barcodeid)
            ->whereNull('package.deleted_at')
            ->first();
        if (!$check) {
            return response()->json('Something wrong, system cannot proccess this request', 422);
        } else {
            //jika data sudah di print sebelumnya, maka tidak perlu update database
            if ($check->is_printed === TRUE) {
                return response()->json('GOOD', 200);
            }
            $status_from = $check->current_status;
        }

        //data movement
        $data = array(
            'barcode_package' => $barcodeid,
            'department_from' => $department_from,
            'department_to' => $department_to,
            'status_from' => $status_from,
            'status_to' => $status_to,
            'user_id' => Auth::user()->id,
            'description' => $description,
            'created_at' => Carbon::now()
        );

        try {
            db::beginTransaction();

            //update is_printed
            DB::table('package')
                ->where('barcode_id', $barcodeid)
                ->whereNull('deleted_at')
                ->update([
                    'current_department' => $department_to,
                    'current_status' => $status_to,
                    'status_preparation' => $status_to,
                    'is_printed' => true,
                    'updated_at' => Carbon::now()
                ]);

            //update deleted_at on the last package movement based on package
            $checkifexist = DB::table('package_movements')
                ->where('barcode_package', $barcodeid)
                ->where('is_canceled', false)
                ->whereNull('deleted_at')
                ->first();
            if ($checkifexist) {
                DB::table('package_movements')
                    ->where('barcode_package', $barcodeid)
                    ->where('is_canceled', false)
                    ->whereNull('deleted_at')
                    ->update([
                        'deleted_at' => Carbon::now()
                    ]);
            }

            //insert package movement
            DB::table('package_movements')->insert($data);
            db::commit();
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json('GOOD', 200);
    }

    //set all package as completed
    public function setCompletedAll(Request $request)
    {
        $filter_package = $request->filter_package;
        $ponumber = trim($request->po_number);
        $plan_ref_number = trim($request->plan_ref_number);
        $template = $request->template_shippingmark;
        $size_template = $request->size_template;
        $department_from = 'preparation';
        $department_to = 'preparation';
        $status_to = 'completed';
        $description = 'print barcode and shipping mark (template: ' . $template . ')';
        $data_movement = array();

        // cek customer order egypt = 856010
        $check_po = DB::table('po_summary')
            ->select('bp_code', 'country')
            ->where('po_number', $request->po_number)
            ->where('plan_ref_number', $request->plan_ref_number)
            ->whereNull('deleted_at')
            ->first();

        if ($check_po->bp_code == '856010') {
            if ($template != 'sm_egypt_a5') {
                return response()->json('please choose another template', 422);
            }
        }

        //
        $data = DB::table('package')
            ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
            ->join('po_summary', function ($join) {
                $join->on('po_summary.po_number', '=', 'package_detail.po_number');
                $join->on('po_summary.plan_ref_number', '=', 'package_detail.plan_ref_number');
            })
            ->where('package_detail.po_number', $ponumber)
            ->where('package_detail.plan_ref_number', $plan_ref_number)
            ->where('package_detail.factory_id', Auth::user()->factory_id);


        //jika tempplate sm adidas maka tambahkan filtering berdasarkan size package;
        if ($template == 'sm_adidas_apparel_fcopy_a5_standar' || $template == 'sm_adidas_apparel_fcopy_a4_standar' || $template == 'costco_korea_a5' || $template == 'costco_korea_a4') {
            if ($size_template == 'l') {
                $data = $data->where('package_detail.height', '>=', '0.2');
            } elseif ($size_template == 'm') {
                // $data = $data->where('package_detail.height', '=', '0.125');
                $data = $data->whereBetween('package_detail.height', ['0.125', '0.159']);
            } elseif ($size_template == 's') {
                $data = $data->where('package_detail.height', '=', '0.083');
            }
        }

        //jika filter package di checklist, maka tambahkan filter berdasarkan package_number
        if ($filter_package) {
            $filter_from = $request->filter_from;
            $filter_to = $request->filter_to;
            $data = $data->whereBetween('package_number', [$filter_from, $filter_to]);
        }

        $data = $data->whereNull('package.deleted_at')
            ->whereNull('package_detail.deleted_at')
            ->whereNull('po_summary.deleted_at')
            ->get();


        foreach ($data as $key => $value) {
            $data[$key]->carton_weight = $value->gross - $value->net;
            $barcodeid = $value->barcode_id;
            $status_from = $value->current_status;

            if ($value->is_printed === TRUE) {
                continue;
            }

            //data movement
            $data_m = array(
                'barcode_package' => $barcodeid,
                'department_from' => $department_from,
                'department_to' => $department_to,
                'status_from' => $status_from,
                'status_to' => $status_to,
                'user_id' => Auth::user()->id,
                'description' => $description,
                'created_at' => Carbon::now()
            );

            $data_movement[] = $data_m;
        }

        try {
            db::beginTransaction();

            foreach ($data_movement as $key => $value) {
                $barcodeid = $value['barcode_package'];

                //update is_printed
                DB::table('package')
                    ->where('barcode_id', $barcodeid)
                    ->whereNull('deleted_at')
                    ->update([
                        'current_department' => $department_to,
                        'current_status' => $status_to,
                        'status_preparation' => $status_to,
                        'is_printed' => true,
                        'updated_at' => Carbon::now()
                    ]);

                //update deleted_at on the last package movement based on package
                $checkifexist = DB::table('package_movements')
                    ->where('barcode_package', $barcodeid)
                    ->where('is_canceled', false)
                    ->whereNull('deleted_at')
                    ->first();
                if ($checkifexist) {
                    DB::table('package_movements')
                        ->where('barcode_package', $barcodeid)
                        ->where('is_canceled', false)
                        ->whereNull('deleted_at')
                        ->update([
                            'deleted_at' => Carbon::now()
                        ]);
                }
            }

            //insert package movement
            DB::table('package_movements')->insert($data_movement);
            db::commit();
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json('GOOD', 200);
    }

    //print preview
    public function printPreview(Request $request)
    {
        ini_set('max_execution_time', 9999);
        ini_set('memory_limit', '128M');
        // $size_template = $request->size_template; disable sementara
        $size_template = 'l';
        $font_size_template = '50';
        $multi_font_size = '50';
        $filename = 'packing_list_po_number_#';
        $filter_package = $request->filter_package;

        $data = DB::table('package')
            ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
            ->join('po_summary', function ($join) {
                $join->on('po_summary.po_number', '=', 'package_detail.po_number');
                $join->on('po_summary.plan_ref_number', '=', 'package_detail.plan_ref_number');
            });
        // return $data->get();
        // ->join('v_ns_po_sync','po_summary.po_number','=','v_ns_po_sync.po_number');
        // ->where('package_detail.factory_id', Auth::user()->factory_id);
        // ->join('po_summary', 'po_summary.po_number', '=', 'package_detail.po_number');
        //
        if (isset($request->template_shippingmark) && !empty($request->template_shippingmark)) {
            $template = $request->template_shippingmark;
            $template_list = array(
                'australia',
                'new_zealand',
                'sld_ship_a5_address_usa_sld',
                'sm_adidas_apparel_fcopy_a5_standar',
                'sm_adidas_sld_apparel_a5_fcopy_usa_sld',
                'sm_china',
                'sm_egypt_a5',
                'uk',
                'sm_adidas_apparel_fcopy_a4_standar',
                'costco_template_ship',
                'sm_deadlock',
                'new_balance',
                'trigerlabel2',
                'egyptoc_a5',
                'egyptoc_a4',
                'sm_adidas_sld_apparel_a4_fcopy_usa_sld',
                'hummel_main',
                'hummel_side',
                'hummel_main_a4',
                'hummel_side_a4',
                'puma_main_italy',
                'puma_main_standar',
                'puma_side_standar',
                'puma_side_thailand',
                'puma_side_usa',
                'puma_side',
                'puma_a5',
                'costco_korea_a5',
                'costco_korea_a4',
                'puma_main_a5',
                'puma_main_a4',
                'puma_side_a5',
                'puma_side_a4',
                'puma_korea_main_a5',
                'puma_korea_side_a5',
                'puma_japan_main_a5',
                'puma_japan_side_a5',
                'puma_thailand_main_a5',
                'puma_thailand_side_a5',
                'puma_korea_main2_a5',
                'puma_korea_side2_a5',
            );
        } else {
            return response()->json('Something wrong A', 422);
        }

        // cek customer order egypt = 856010
        $check_po = DB::table('po_summary')
            ->select('bp_code', 'country')
            ->where('po_number', $request->po_number)
            ->where('plan_ref_number', $request->plan_ref_number)
            ->whereNull('deleted_at')
            ->first();

        if ($check_po->bp_code == '856010') {
            if ($template != $template_list[6]) {
                return response()->json('Something wrong, system cannot proccess this request template', 422);
            }
        }

        //
        if (isset($request->barcode_id) && !empty($request->barcode_id)) {
            $barcodeid = $request->barcode_id;
            $data = $data->where('package.barcode_id', $barcodeid);
        } else {
            $barcodeid = null;
            //jika template sm apparel maka pilah2
            if ($template == $template_list[3] || $template == $template_list[8] || $template == $template_list[27] || $template == $template_list[28]) {
                $size_template = $request->size_template;
                if ($size_template == 'l') {
                    $data = $data->where('package_detail.height', '>=', '0.2');
                } elseif ($size_template == 'm') {
                    // $data = $data->where('package_detail.height', '=', '0.125');
                    $data = $data->whereBetween('package_detail.height', ['0.125', '0.159']);
                } elseif ($size_template == 's') {
                    $data = $data->where('package_detail.height', '=', '0.083');
                }
            }
        }

        //filter
        if ($filter_package) {
            $from = $request->from;
            $to = $request->to;
            $data = $data->whereBetween('package_number', [$from, $to]);
        }

        //
        if (isset($request->po_number) && !empty($request->po_number)) {
            $ponumber = $request->po_number;
            $plan_ref_number = $request->plan_ref_number;

            $data = $data->where('package_detail.po_number', $ponumber)
                ->where('package_detail.plan_ref_number', $plan_ref_number)
                ->where('package_detail.factory_id', Auth::user()->factory_id)
                ->whereNull('package.deleted_at')
                ->whereNull('package_detail.deleted_at')
                ->whereNull('po_summary.deleted_at')
                ->orderBy('package.package_number', 'asc')
                ->get();

            if (empty($barcodeid)) {
                $filename = 'packing_list_po_number_#' . $ponumber . '.pdf';
                // $total_package = count($data);
            } else {
                $filename = 'packing_list_po_number_#' . $ponumber . '_barcodeid_' . $barcodeid . '.pdf';
            }

            $total_package = DB::table('package')
                ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
                // ->join('po_summary', 'po_summary.po_number', '=', 'package_detail.po_number')
                ->join('po_summary', function ($join) {
                    $join->on('po_summary.po_number', '=', 'package_detail.po_number');
                    $join->on('po_summary.plan_ref_number', '=', 'package_detail.plan_ref_number');
                })
                ->where('package_detail.po_number', $ponumber)
                ->where('package_detail.plan_ref_number', $plan_ref_number)
                ->where('package_detail.factory_id', Auth::user()->factory_id)
                ->whereNull('package.deleted_at')
                ->whereNull('package_detail.deleted_at')
                ->whereNull('po_summary.deleted_at')
                ->count();

            if ($total_package == 0) {
                return response()->json('Something wrong B', 422);
            }

            foreach ($data as $key => $value) {
                $data[$key]->carton_weight = $value->gross - $value->net;
                $data[$key]->total_package = $total_package;
            }
        } else {
            return response()->json('Something wrong C', 422);
        }


        //config layout
        $orientation = 'landscape';
        $papersize = 'a5';
        if (
            $template == $template_list[0] ||
            $template == $template_list[1] ||
            $template == $template_list[2] ||
            $template == $template_list[7] ||
            $template == $template_list[8] ||
            $template == $template_list[15] ||
            $template == $template_list[16] ||
            $template == $template_list[17] ||
            $template == $template_list[20] ||
            $template == $template_list[21] ||
            $template == $template_list[22] ||
            $template == $template_list[23] ||
            $template == $template_list[24] ||
            $template == $template_list[25] ||
            $template == $template_list[28] ||
            $template == $template_list[30] ||
            $template == $template_list[32] ||
            $template == $template_list[36] 
        ) {
            $orientation = 'portrait';
        }

        if (
            $template == $template_list[5] ||
            $template == $template_list[7] ||
            $template == $template_list[8] ||
            $template == $template_list[15] ||
            $template == $template_list[18] ||
            $template == $template_list[19] ||
            $template == $template_list[28] ||
            $template == $template_list[30] ||
            $template == $template_list[32]
        ) {
            $papersize = 'a4';
        }

        if (
            $template == $template_list[0] ||
            $template == $template_list[1]
        ) {
            $papersize = 'a4';
        }

        if ($template == $template_list[9] || $template == $template_list[11] || $template == $template_list[12] || $template == $template_list[14]) {
            $papersize = 'a4';
            $orientation = 'portrait';
        }

        if ($template == $template_list[3] || $template == $template_list[8] || $template == $template_list[26] || $template == $template_list[27] || $template == $template_list[28]) {
            $size_template = $request->size_template;
            if ($size_template == 'm') {
                $papersize = 'a4';
                $orientation = 'portrait';
                $template = 'sm_adidas_apparel_fcopy125';
                $multi_font_size = $request->multi_font_size;
            } elseif ($size_template == 's') {
                $papersize = 'a4';
                $orientation = 'portrait';
                $template = 'sm_adidas_083';
                $multi_font_size = $request->multi_font_size;
            } else {
                $font_size_template = $request->font_size_template;
                $multi_font_size = $request->multi_font_size;
            }
        }
        if ($template == $template_list[27] || $template == $template_list[28]) {
            $size_template = $request->size_template;
            if ($size_template == 'm') {
                $papersize = 'a4';
                $orientation = 'portrait';
                $template = 'costco_korea125';
                $multi_font_size = $request->multi_font_size;
            } elseif ($size_template == 's') {
                $papersize = 'a4';
                $orientation = 'portrait';
                $template = 'costco_korea_083';
                $multi_font_size = $request->multi_font_size;
            } else {
                $font_size_template = $request->font_size_template;
                $multi_font_size = $request->multi_font_size;
            }
        }

        //template china
        if ($template == $template_list[5]) {
            $size_template = $request->size_template;
            if ($size_template == 'm') {
                $papersize = 'a4';
                $orientation = 'portrait';
                $template = 'sm_china_m';
            } elseif ($size_template == 's') {
                $papersize = 'a4';
                $orientation = 'portrait';
                $template = 'sm_china_s';
            } else {
                $font_size_template = $request->font_size_template;
            }
        }

        //template hummel main hummel side
        if (
            $template == $template_list[16] ||
            $template == $template_list[17] ||
            $template == $template_list[18] ||
            $template == $template_list[19] ||
            $template == $template_list[20] ||
            $template == $template_list[21] ||
            $template == $template_list[22] ||
            $template == $template_list[23] ||
            $template == $template_list[24] ||
            $template == $template_list[25] ||
            $template == $template_list[29] ||
            $template == $template_list[30] ||
            $template == $template_list[31] ||
            $template == $template_list[32] ||
            $template == $template_list[33] ||
            $template == $template_list[34] ||
            $template == $template_list[35] ||
            $template == $template_list[36] ||
            $template == $template_list[37] ||
            $template == $template_list[38] ||
            $template == $template_list[39] || 
            $template == $template_list[40] 
        ) {
            $multi_font_size = $request->multi_font_size;
            $font_size_template = $request->font_size_template;
        }
        //show barcode ?
        if (isset($request->barcode_show) && $request->barcode_show == 'false') {
            $showbarcode = null;
        } else {
            $showbarcode = true;
        }

        // show total package
        if (isset($request->package_show) && $request->package_show == 'false') {
            $showpackage = null;
        } else {
            $showpackage = true;
        }

        //isbgrade
        if (isset($request->isbgrade) && $request->isbgrade == 'true') {
            $isbgrade = true;
        } else {
            $isbgrade = null;
        }

        // return $papersize;
        // return $font_size_template;
        // return $data;
        // return view(
        //     'packing.packing_list.template.' . $template,
        //     [
        //         'data' => $data,
        //         'total_package' => $total_package,
        //         'size_template' => $size_template,
        //         'font_size_template' => $font_size_template,
        //         'multi_font_size' => $multi_font_size,
        //         'showbarcode' => $showbarcode,
        //         'showpackage' => $showpackage,
        //         'isbgrade' => $isbgrade,
        //         'preview' => 0,
        //     ]
        // );

        // return $orientation;

        $pdf = \PDF::loadView(
            'packing.packing_list.template.' . $template,
            [
                'data' => $data,
                'total_package' => $total_package,
                'size_template' => $size_template,
                'font_size_template' => $font_size_template,
                'multi_font_size' => $multi_font_size,
                'showbarcode' => $showbarcode,
                'showpackage' => $showpackage,
                'isbgrade' => $isbgrade,
            ]
        )
            ->setPaper($papersize, $orientation)
            ->setOptions([
                'isPhpEnabled' => true,
                'unit' => 'mm',
            ]);
        // return $pdf->stream();
        return $pdf->stream($filename);
    }

    //generate barcode
    public function generateBarcode(Request $request)
    {
        $barcodeid = trim($request->barcodeid);
        return view('packing/packing_list/print_barcode')->with('barcodeid', $barcodeid);
    }

    //view template
    public function viewTemplate(Request $request)
    {
        $template = $request->template;
        if (isset($request->barcode_id)) {
            $data = DB::table('package_detail')->select(
                'package.barcode_id',
                'package.package_number',
                'package_detail.buyer_item',
                'package_detail.customer_number',
                'package_detail.customer_size',
                'package_detail.gross',
                'package_detail.height',
                'package_detail.item_qty',
                'package_detail.inner_pack',
                'package_detail.length',
                'package_detail.manufacturing_size',
                'package_detail.net',
                'package_detail.po_number',
                'package_detail.unit_1',
                'package_detail.unit_2',
                'package_detail.width',
                'po_summary.address',
                'po_summary.city',
                'po_summary.country_of_origin',
                'po_summary.country',
                'po_summary.fax',
                'po_summary.made_in',
                'po_summary.phone',
                'po_summary.postal',
                'po_summary.trademark',
                'po_summary.customer_order_number',
                'po_summary.commodities_type',
                'po_summary.remark',
                'po_summary.upc',
                'po_summary.season',
                'po_summary.plan_ref_number',
                'po_summary.bp_name',
                'package_detail.package_from',
                'package_detail.package_to',
                'package_detail.scan_id',
                'package_detail.rasio',
                'package_detail.service_identifier',
                'package_detail.packing_mode',
                'package_detail.pkg_count',
                'package_detail.technical_print_index',
                'package_detail.gps_three_digit_size',
                'package_detail.model_number'
            )->join('package', 'package.scan_id', '=', 'package_detail.scan_id')
                ->join('po_summary', function ($join) {
                    $join->on('po_summary.po_number', '=', 'package_detail.po_number');
                    $join->on('po_summary.plan_ref_number', '=', 'package_detail.plan_ref_number');
                })
                // ->join('po_summary', 'po_summary.po_number','=','package_detail.po_number')
                ->where('barcode_id', $request->barcode_id)
                ->where('package_detail.factory_id', Auth::user()->factory_id)
                ->whereNull('package_detail.deleted_at')
                ->whereNull('package.deleted_at')
                ->whereNull('po_summary.deleted_at')
                ->get();

            $ponumber = $data[0]->po_number;
            $total_package = DB::table('package')
                ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
                ->join('po_summary', function ($join) {
                    $join->on('po_summary.po_number', '=', 'package_detail.po_number');
                    $join->on('po_summary.plan_ref_number', '=', 'package_detail.plan_ref_number');
                })
                // ->join('po_summary', 'po_summary.po_number', '=', 'package_detail.po_number')
                ->where('package_detail.po_number', $ponumber)
                ->where('package_detail.factory_id', Auth::user()->factory_id)
                ->whereNull('package.deleted_at')
                ->whereNull('package_detail.deleted_at')
                ->whereNull('po_summary.deleted_at')
                ->count();

            foreach ($data as $key => $value) {
                $data[$key]->carton_weight = $value->gross - $value->net;
                $data[$key]->total_package = $total_package;
            }
        }

        if (isset($request->barcode_id)) {
            return view('packing/packing_list/template/' . $template, compact('data'));
        } else {
            return view('packing/packing_list/template/' . $template);
        }
    }

    //Random barcode
    static function random_code($string)
    {
        $string = str_pad($string, 5, "0", STR_PAD_LEFT);
        return Carbon::now()->format('ymdhis') . $string;
    }

    //SYNC DARI API YG DIKASIH AOI
    public function syncPackingList()
    {
        $lc_date = '2017-02-03';
        // $list_datasync = array();
        // $first_lcdate = Carbon::parse('2014-07-31');
        // $current_lcdate = Carbon::now();
        // $total_days = $current_lcdate->diffInDays($first_lcdate);
        // for($i = 0; $i < $total_days; $i++) {
        //     if(false === ($content = @file_get_contents($this->api_url . $this->parameter[0] . '=' . $first_lcdate->addDays(1)))){
        //         return 'Syncing error, please checked your network';
        //     }
        //
        //     $json = json_decode($content, true);
        //     $data = array_chunk($json['data'], 1000, true);
        //
        //     array_push($list_datasync, $data);
        //     echo $i;
        // }
        // dd($list_datasync);

        if (false === ($content = @file_get_contents($this->api_url . $this->parameter[0] . '=' . $lc_date))) {
            return 'Syncing error, please checked your network';
        }

        $json = json_decode($content, true);
        $data = array_chunk($json['data'], 1000, true);
        dd($data);

        try {
            db::beginTransaction();
            foreach ($data as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    $data = array(
                        'c_order_id' => $value2['c_order_id'],
                        'c_bpartner_id' => $value2['c_bpartner_id'],
                        'c_bpartner_location_id' => $value2['c_bpartner_location_id'],
                        'c_location_id' => $value2['c_location_id'],
                        'c_country_id' => $value2['c_country_id'],
                        'documentno' => $value2['documentno'],
                        'dateordered' => $value2['dateordered'],
                        'datepromised' => $value2['datepromised'],
                        'grandtotal' => $value2['grandtotal'],
                        'bp_name' => $value2['bp_name'],
                        'bp_code' => $value2['bp_code'],
                        'phone' => $value2['phone'],
                        'fax' => $value2['fax'],
                        'address' => $value2['address'],
                        'city' => $value2['city'],
                        'postal' => $value2['postal'],
                        'country' => $value2['country'],
                        'kst_lcdate' => $value2['kst_lcdate'],
                        'kst_statisticaldate' => $value2['kst_statisticaldate'],
                        'trademark' => 'adidas',
                        'country_of_origin' => 'indonesia',
                        'made_in' => 'indonesia',
                        'commodities_type' => 'garment',
                        'customer_order_number' => $value2['kst_orderno'],
                        'created_at' => Carbon::now()
                    );
                    $sync[] = $data;
                }
                DB::table('po_summary')->insert($sync);
            }
            db::commit();
        } catch (Exception $e) {
            db::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    /*private function array2xml($array, $xml = false){
        if($xml === false){
            $xml = new SimpleXMLElement('<PurchaseOrder xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="PurchaseOrderv1_0.xsd"/>');
        }
        foreach($array as $key => $value){
            if(is_array($value)){
                array2xml($value, $xml->addChild($key));
            }else{
                $xml->addChild($key, $value);
            }
        }
        return $xml->asXML();
    }
*/

    //TES XML
    /*public function xmlList() {

        $test_array = array (
              'POPurpose' => 'PO Creation',
              'PONumber' => '0123456789',
              'POVersionNumber' => '0',
              'POResponseVersionNumber' => '0',
              'messageGenerationDateTime' => '2018-06-11T12:00:00+08:00',
              'latestUpdateDateTime' => '2018-06-11T11:00:00+08:00',
              'originalPOIssueDate' => '2018-06-11',
              'adidasPODownloadDate' => '2018-06-10',
              'currency' => 'USD',
              'paymentTerms' => 'Payment 30 days after invoice date',
              'headerRemarks' => 'Check markings on cases, there was a problem with past orders. This is general information only, not to be processed by your system.',
              'sender' => array (
                'senderName' => 'T1ABC',
                'senderPerson' => 'Sam Jones',
              ),
              'recipient' => 'T2ABCD',
              'buyer' => array(
                'buyerName' => 'T1 Supplier Company Limited',
                'buyerAddressLine1' => '48/F, Kodak House, 123 Java Road, North Point',
              ),

            );

        $content_xml = $this->array2xml($test_array);

        return response($content_xml)
        ->withHeaders([
            'Content-Type' => 'text/xml'
        ]);

    }*/

    public function getCostco(Request $request)
    {
        $ponum = trim($request->po_number);
        $planref = trim($request->plan_ref_number);

        return view('packing/packing_list/upload_costco')->with(['po_number' => $ponum, 'plan_ref_number' => $planref]);
    }

    public function uploadCostco(Request $request)
    {

        $ponum = trim($request->po_number);
        $planref = trim($request->plan_ref_number);

        $results = array();
        if ($request->hasFile('file_upload')) {
            $extension = \File::extension($request->file_upload->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls") {
                $path = $request->file_upload->getRealPath();

                $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
            }
        }

        $data_package_detail_rasio = array();

        foreach ($datax as $key => $value) {
            $det['po_number_key'] = $ponum;
            $det['po_number'] = $value->po;
            $det['plan_ref_number'] = $planref;
            $det['line'] = $value->line;
            $det['buyer_item'] = $value->buyer_item;
            $det['model_number'] = $value->model_number;
            $det['customer_size'] = $value->customer_size;
            $det['manufacturing_size'] = $value->manufacturing_size;
            $det['technical_print_index'] = $value->technical_print_index;
            $det['gps_three_digit_size'] = $value->gps_three_digit_size;
            $det['service_identifier'] = $value->vasshas_g01_service_identifier;
            $det['packing_mode'] = $value->vasshas_l15_packing_mode;
            $det['item_qty'] = str_replace(array('.', ','), '', $value->item_qty);
            $det['inner_pack'] = $value->qty_per_pkginner_pack;
            $det['created_at'] = Carbon::now();
            $det['factory_id'] = Auth::user()->factory_id;
            $data_package_detail_rasio[] = $det;
        }





        try {
            DB::begintransaction();
            $cekrasio = DB::table('po_summary')
                ->where('plan_ref_number', $planref)
                ->where('factory_id', Auth::user()->factory_id)
                ->whereNull('deleted_at')
                ->count();

            if ($cekrasio > 0) {

                DB::table('package_detail_rasio')->insert($data_package_detail_rasio);
            } else {
                throw new \Exception('Costco cannot upload');
            }

            DB::commit();
        } catch (Exception $er) {
            DB::rollback();
            $message = $er->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json('GOOD', 200);
    }

    public function viewCostcoDetail(Request $request)
    {
        $plan_ref_number = $request->plan_ref_number;
        $po_number = $request->po_number;

        return view('packing/packing_list/detail_costco')
            ->with('po_number_key', $po_number)
            ->with('plan_ref_number', $plan_ref_number);
    }

    public function getCostcoDetail(Request $request)
    {
        $plan = trim($request->plan_ref_number);
        $pokey = trim($request->po_number_key);

        $check = DB::table('package_detail_rasio')
            ->where('plan_ref_number', $plan)
            ->where('factory_id', Auth::user()->factory_id)
            ->count();

        if ($check > 0) {
            $data = DB::table('package_detail_rasio')
                ->where('plan_ref_number', $plan)
                ->where('factory_id', Auth::user()->factory_id)
                ->get();
        }

        return Datatables::of($data)
            ->addIndexColumn()
            ->editColumn('po_number_key', function ($data) {
                return $data->po_number_key;
            })
            ->editColumn('po_number', function ($data) {
                return $data->po_number;
            })
            ->editColumn('buyer_item', function ($data) {
                return $data->buyer_item;
            })
            ->editColumn('costumer_size', function ($data) {
                return $data->customer_size;
            })
            ->editColumn('item_qty', function ($data) {
                return $data->item_qty;
            })
            ->editColumn('inner_pack', function ($data) {
                return $data->inner_pack;
            })
            ->rawColumns(['po_number_key', 'po_number', 'buyer_item', 'customer_size', 'item_qty', 'inner_pack'])
            ->make(true);
    }

    public function packingListCancel()
    {
        return view('packing/packing_list/cancel_order');
    }

    public function getDataCancel(Request $request)
    {


        //api
        if ($request->radio_status) {
            //filtering
            if ($request->radio_status == 'po') {
                if (empty($request->po_number)) {
                    $po_number = 'null';
                } else {
                    $po_number = $request->po_number;
                }

                $params =  $this->api_url . $this->parameter[2] . '=' . $po_number;
            } elseif ($request->radio_status == 'lc') {
                $counted_day = 0;
                if ($request->date_range) {
                    $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
                    $from = date_format(date_create($date_range[0]), 'Y-m-d');
                    $to = date_format(date_create($date_range[1]), 'Y-m-d');
                    //check date range
                    $date_to = Carbon::parse($date_range[0]);
                    $date_from = Carbon::parse($date_range[1]);
                    $counted_day = $date_to->diffInDays($date_from);
                } else {
                    $from = date_format(Carbon::now(), 'Y-m-d');
                    $to = date_format(Carbon::now(), 'Y-m-d');
                }

                if ($counted_day > 30) {
                    return response()->json('Date range should be less than 30 days', 422);
                }

                $params =  $this->api_url . $this->parameter[0] . '=' . $from . '&' . $this->parameter[1] . '=' . $to;
            }

            //check if po has been uploaded or not
            $check_po = DB::table('po_summary')
                ->select('po_number')
                ->where('po_number', 'like', '%' . trim($request->po_number) . '%')
                ->where('factory_id', Auth::user()->factory_id)
                ->count();


            if ($check_po > 0) {
                $dataxx = DB::table('po_summary')
                    ->where('po_number', 'like', '%' . trim($request->po_number) . '%')
                    ->where('factory_id', Auth::user()->factory_id)
                    ->orderBy('deleted_at', 'asc')
                    ->get();
                $data = json_decode(json_encode($dataxx), true);
            } else {
                //call api
                $json = json_decode(file_get_contents($params), true);
                $data = $json['data'];
            }


            return Datatables::of($data)
                ->editColumn('poreference', function ($data) {
                    return isset($data['poreference']) ? trim($data['poreference']) : trim($data['po_number']);
                })
                ->editColumn('plan_ref_number', function ($data) {
                    return isset($data['plan_ref_number']) ? trim($data['plan_ref_number']) : '-';
                })
                ->editColumn('total_package', function ($data) {

                    // $ponumber = $this->removePrefix(trim($data['documentno']));
                    $ponumber = isset($data['poreference']) ? trim($data['poreference']) : trim($data['po_number']);
                    $plan_ref_number = isset($data['plan_ref_number']) ? trim($data['plan_ref_number']) : '0';
                    // $customer_order_number = isset($data['kst_orderno']) ? $data['kst_orderno'] : $data['customer_order_number'];
                    if (isset($data['kst_orderno'])) {
                        $customer_order_number = $data['kst_orderno'];
                    } else {
                        if (isset($data['customer_order_number'])) {
                            $customer_order_number = $data['customer_order_number'];
                        } else {
                            $customer_order_number = null;
                        }
                    }

                    $total_package    = DB::table('package')
                        ->select('barcode_id')
                        ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
                        ->where('po_number', $ponumber)
                        ->where('plan_ref_number', $plan_ref_number)
                        ->where('factory_id', Auth::user()->factory_id)
                        ->whereNull('package.deleted_at')
                        ->count();
                    if ($total_package == 0) {
                        $total_package = '-';
                    }

                    if (isset($data['brand']) != null) {
                        $trademark = $data['brand'];
                    } else {
                        if (isset($data['trademark']) != null) {
                            $trademark = $data['trademark'];
                        } else {
                            $trademark = '-';
                        }
                    }

                    if ($data['recycle'] == 'Y') {
                        $isrecycle = 1;
                    } else {
                        $isrecycle = 0;
                    }

                    $returned_value = '<span id="total_package_' . $ponumber . '">' . $total_package . '</span>
                                           <input type="hidden" id="buyer_name_' . $ponumber . '" value="' . $data['bp_name'] . '">
                                           <input type="hidden" id="destination_address_' . $ponumber . '" value="' . $data['address'] . '">
                                           <input type="hidden" id="destination_city_' . $ponumber . '" value="' . $data['city'] . '">
                                           <input type="hidden" id="destination_postal_' . $ponumber . '" value="' . $data['postal'] . '">
                                           <input type="hidden" id="destination_country_' . $ponumber . '" value="' . $data['country'] . '">
                                           <input type="hidden" id="dateordered_' . $ponumber . '" value="' . $data['dateordered'] . '">
                                           <input type="hidden" id="datepromised_' . $ponumber . '" value="' . $data['datepromised'] . '">
                                           <input type="hidden" id="grandtotal_' . $ponumber . '" value="' . $data['grandtotal'] . '">
                                           <input type="hidden" id="bp_code_' . $ponumber . '" value="' . $data['bp_code'] . '">
                                           <input type="hidden" id="phone_' . $ponumber . '" value="' . $data['phone'] . '">
                                           <input type="hidden" id="fax_' . $ponumber . '" value="' . $data['fax'] . '">
                                           <input type="hidden" id="kst_lcdate_' . $ponumber . '" value="' . $data['kst_lcdate'] . '">
                                           <input type="hidden" id="kst_statisticaldate_' . $ponumber . '" value="' . $data['kst_statisticaldate'] . '">
                                           <input type="hidden" id="trademark_' . $ponumber . '" value="' . $trademark . '">
                                           <input type="hidden" id="made_in_' . $ponumber . '" value="INDONESIA">
                                           <input type="hidden" id="country_of_origin_' . $ponumber . '" value="INDONESIA">
                                           <input type="hidden" id="commodities_type_' . $ponumber . '" value="GARMENT">
                                           <input type="hidden" id="customer_order_number_' . $ponumber . '" value="' . $customer_order_number . '">
                                           <input type="hidden" id="remark_' . $ponumber . '" value="' . $data['remark'] . '">
                                           <input type="hidden" id="upc_' . $ponumber . '" value="' . $data['upc'] . '">
                                           <input type="hidden" id="recycle_' . $ponumber . '" value="' . $isrecycle . '">
                                          ';

                    return $returned_value;
                })
                ->addColumn('action', function ($data) {
                    // $ponumber = trim($data->po_number);
                    $ponumber = isset($data['poreference']) ? trim($data['poreference']) : trim($data['po_number']);
                    $plan_ref_number = isset($data['plan_ref_number']) ? trim($data['plan_ref_number']) : '';

                    //check if po has been uploaded or not
                    $check = DB::table('po_summary')
                        ->where('po_number', $ponumber)
                        ->where('factory_id', Auth::user()->factory_id)
                        // ->whereNull('deleted_at')
                        ->first();
                    $cplanR = DB::table('package_detail_rasio')
                        ->select('plan_ref_number')
                        ->where('plan_ref_number', $plan_ref_number)
                        ->first();

                    if (!$check) {
                        return view('_action', [
                            'model' => $data,
                            'upload' => route(
                                'packing.modalUploadCancel',
                                [
                                    'po_number'  => $ponumber
                                ]
                            ),
                            'detail' => route(
                                'packing.viewPackageDetail',
                                [
                                    'po_number'  => $ponumber,
                                    'plan_ref_number'  => $plan_ref_number
                                ]
                            )
                        ]);
                    } else {

                        return view('_action', [
                            'model' => $data,
                            // 'upload' => route('packing.modalUpload', ['po_number' => $ponumber]),
                            // 'upload' => $routeUpload,
                            'detail' => route(
                                'packing.viewPackageDetail',
                                [
                                    'po_number'  => $ponumber,
                                    'plan_ref_number'  => $plan_ref_number
                                ]
                            )
                        ]);
                    }
                })
                ->addColumn('is_completed', function ($data) {
                    $ponumber = isset($data['poreference']) ? trim($data['poreference']) : trim($data['po_number']);
                    $plan_ref_number = isset($data['plan_ref_number']) ? trim($data['plan_ref_number']) : '0';

                    $get_package = DB::table('package')
                        ->select('barcode_id')
                        ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
                        ->where('po_number', $ponumber)
                        ->where('plan_ref_number', $plan_ref_number)
                        ->where('factory_id', Auth::user()->factory_id)
                        ->where('is_printed', true)
                        ->whereNull('package.deleted_at')
                        ->count();

                    if ($get_package > 0) {
                        $is_completed = '<span class="label label-success label-rounded">
                                                <i class="icon-checkmark2"></i>
                                            </span><span class="badge badge-success position-right">' . $get_package . '</span>';
                    } else {
                        $is_completed = '<span class="label label-default label-rounded">
                                                <i class="icon-cross3"></i>
                                            </span>';
                    }

                    return $is_completed;
                })
                ->rawColumns(['total_package', 'action', 'is_completed'])
                ->make(true);
        } else {
            $data = array();
            return Datatables::of($data)
                ->editColumn('total_package', function ($data) {
                    return null;
                })
                ->addColumn('action', function ($data) {
                    return null;
                })
                ->rawColumns(['total_package', 'action'])
                ->make(true);
        }
    }

    public function uploadExcelCancel(Request $request)
    {
        if (substr($request->po_number, 0, 2) == 'SO') {
            $ponumber = explode("-", trim($request->po_number))[1];
        } else {
            $ponumber = trim($request->po_number);
        }

        $plan_ref_number = trim($request->plan_ref_number);

        if ($plan_ref_number == '') {
            return response()->json('Plan Ref Number not found', 422);
        }
        $oldpo = $request->oldpo;
        //data po_summary
        $data_po_summary = array(
            'po_number' => $ponumber,
            'plan_ref_number' => $plan_ref_number,
            'dateordered' => $request->dateordered,
            'datepromised' => $request->datepromised,
            'grandtotal' => $request->grandtotal,
            'bp_name' => $request->bp_name,
            'bp_code' => $request->bp_code,
            'phone' => $request->phone,
            'fax' => $request->fax,
            'address' => $request->address,
            'city' => $request->city,
            'postal' => $request->postal,
            'country' => $request->country,
            'kst_lcdate' => $request->kst_lcdate,
            'kst_statisticaldate' => $request->kst_statisticaldate,
            'trademark' => $request->trademark,
            'country_of_origin' => $request->country_of_origin,
            'made_in' => $request->made_in,
            'commodities_type' => $request->commodities_type,
            'customer_order_number' => $request->customer_order_number,
            'remark' => $request->remark,
            'upc' => $request->upc,
            'factory_id' => Auth::user()->factory_id,
            'created_at' => Carbon::now(),
            'is_cancel_order' => true,
            'old_po' => $oldpo,
            'season' => $request->season,
            'recycle' => isset($request->recycle) == 1 ? true : false
        );

        //
        $results = array();
        if ($request->hasFile('file_upload')) {
            $extension = \File::extension($request->file_upload->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls") {
                $path = $request->file_upload->getRealPath();
                $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();

                $rows = \Excel::load($request->file_upload->getPathName(), function ($path) {
                })->get();

                $totalRows = $rows->count();
            }
        }

        $data_package = array();
        $data_movement = array();
        $data_package_detail = array();
        $count_ponumber = 0;
        $count_inserted_data = 0;
        $j = 0;
        foreach ($datax as $key => $value) {
            $j++;
            //check apakah header unit sudah diubah menjadi unit1 dan unit2
            if (!isset($value->unit1) || !isset($value->unit2)) {
                return response()->json('Header `unit1` dan `unit2` tidak ditemukan', 422);
            }
            $po_number = trim($value->po);
            if ($po_number != $ponumber) {
                // return response()->json('Cek PO Number pada packinglist',422);
                continue;
            }

            //check apakah kolom from, to ada valuenya tidak
            if ($value->from == '') {
                return response()->json('Kolom `from` harus terisi', 422);
            }

            if ($value->to == '') {
                return response()->json('Kolom `to` harus terisi', 422);
            }

            if ($value->scan_id == '') {
                return response()->json('Kolom `scan id` harus terisi', 422);
            }

            if ($value->qty_per_pkginner_pack == '' || $value->qty_per_pkginner_pack == 0 || $value->item_qty == '' || $value->item_qty == 0) {
                return response()->json('Inner pack & item qty tidak boleh kosong / 0', 422);
            }

            if ($this->_scan_id_check($value->scan_id) == false) {
                return response()->json('Check scan id packinglist, Scan Id Sudah ada', 422);
            }

            // cek package detail apakah po & size ada
            $check_package_posize = DB::table('package_detail')
                ->where('po_number', $po_number)
                ->where('manufacturing_size', $value->manufacturing_size)
                ->whereNull('deleted_at')
                ->exists();

            if ($check_package_posize) {
                return response()->json('Packing List belum revisi', 422);
            }

            $count_ponumber++;
            $from = $value->from;
            $to = $value->to;
            $serial_from = $value->serial_from;
            $serial_to = $value->serial_to;
            $description = 'upload package';
            $department = 'preparation';
            $status = 'onprogress';

            //data package detail
            // $detail['scan_id'] = trim($value->scan_id);
            if (!empty($value->scan_id)) {
                $detail['scan_id'] = trim($value->scan_id);
            } else {
                $detail['scan_id'] = $this->random_code_scan($po_number, $value->manufacturing_size, $j);
            }

            $detail['po_number'] = $po_number;
            $detail['plan_ref_number'] = $plan_ref_number;
            $detail['package_range'] = $value->range;
            $detail['package_from'] = $value->from;
            $detail['package_to'] = $value->to;
            $detail['serial_from'] = $value->serial_from;
            $detail['serial_to'] = $value->serial_to;
            $detail['pack_instruction_code'] = $value->pack_instruction_code;
            $detail['line'] = $value->line;
            $detail['sku'] = $value->sku;
            $detail['buyer_item'] = trim($value->buyer_item);
            $detail['model_number'] = $value->model_number;
            $detail['customer_size'] = trim($value->customer_size);
            $detail['customer_number'] = $value->customer_number;
            $detail['manufacturing_size'] = trim($value->manufacturing_size);
            $detail['technical_print_index'] = $value->technical_print_index;
            $detail['gps_three_digit_size'] = $value->gps_three_digit_size;
            $detail['service_identifier'] = $value->vasshas_g01_service_identifier;
            $detail['packing_mode'] = $value->vasshas_l15_packing_mode;
            $detail['item_qty'] = str_replace(array('.', ','), '', $value->item_qty);
            $detail['inner_pack'] = $value->qty_per_pkginner_pack;
            $detail['inner_pkg_count'] = $value->inner_pkg_count;
            $detail['pkg_count'] = $value->pkg_count;
            $detail['net_net'] = $value->net_net;
            $detail['net'] = $value->net;
            $detail['gross'] = $value->gross;
            $detail['unit_1'] = $value->unit1;
            $detail['length'] = $value->l;
            $detail['width'] = $value->w;
            $detail['height'] = $value->h;
            $detail['r'] = $value->r;
            $detail['pkg_code'] = $value->pkg_code;
            $detail['unit_2'] = $value->unit2;
            $detail['rasio'] = $this->_rasio_check($value->manufacturing_size);
            $detail['created_at'] = Carbon::now();
            $detail['factory_id'] = Auth::user()->factory_id;
            $detail['old_po'] = $oldpo;
            $data_package_detail[] = $detail;

            //data package
            for ($i = $from; $i <= $to; $i++) {
                $count_inserted_data++;

                $data['barcode_id'] = $this->random_code($i);
                if (!empty($value->scan_id)) {
                    $data['scan_id'] = trim($value->scan_id);
                    $data['serial_number'] = $serial_from++;
                } else {
                    $data['scan_id'] = $detail['scan_id'];
                    $data['serial_number'] = $i;
                }

                $data['package_number'] = $i;
                $data['current_department'] = 'preparation';
                $data['current_status'] = 'onprogress';
                $data['status_preparation'] = 'onprogress';
                $data['created_at'] = Carbon::now();
                $data_package[] = $data;

                //data package movement
                $data_m = array(
                    'barcode_package' => $data['barcode_id'],
                    'department_to' => $department,
                    'status_to' => $status,
                    'user_id' => Auth::user()->id,
                    'description' => $description,
                    'created_at' => Carbon::now(),
                    'ip_address' => \Request::ip()
                );
                $data_movement[] = $data_m;
            }
        }

        $getPackageTo = array();
        $getPackageFrom = array();

        foreach ($data_package_detail as $key_ => $val_) {
            $getPackageTo[$key] = $val_['package_to'];
            $getPackageFrom[$key] = $val_['package_from'];
        }

        //
        if ($count_ponumber == 0) {
            return response()->json('PO Number not found', 422);
        }

        try {
            DB::begintransaction();

            //check apakah po dan plan ref number pernah diupload
            $check = DB::table('po_summary')
                ->where('po_number', $ponumber)
                ->where('plan_ref_number', $plan_ref_number)
                ->where('factory_id', Auth::user()->factory_id)
                ->whereNull('deleted_at')
                ->first();

            if ($check) {
                throw new \Exception('Plan Ref Number already exists');
            } else {
                //insert status po_summary
                DB::table('po_summary')->insert($data_po_summary);

                //insert package detail
                DB::table('package_detail')->insert($data_package_detail);

                //insert package
                DB::table('package')->insert($data_package);

                //insert package movement
                DB::table('package_movements')->insert($data_movement);
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json($count_inserted_data, 200);
    } #uploadExcelCancel

    public function revisiExcelCancel(Request $request)
    {
        if (substr($request->po_number, 0, 2) == 'SO') {
            $ponumber = explode("-", trim($request->revisi_po_number))[1];
        } else {
            $ponumber = trim($request->revisi_po_number);
        }

        $plan_ref_number = trim($request->revisi_plan_ref_number);
        $plan_ref_number_new = trim($request->plan_ref_number_new);

        $cekpo = DB::table('po_summary')
            ->where('po_number', $ponumber)
            ->where('plan_ref_number', $plan_ref_number_new)
            ->where('factory_id', Auth::user()->factory_id)
            ->first();
        if ($cekpo != null) {
            return response()->json('Plan Ref. Number was already', 422);
        }

        $oldpo = $request->revisi_oldpo;
        //data po_summary
        $data_po_summary = array(
            'po_number' => $ponumber,
            'plan_ref_number' => $plan_ref_number_new,
            'dateordered' => $request->revisi_dateordered,
            'datepromised' => $request->revisi_datepromised,
            'grandtotal' => $request->revisi_grandtotal,
            'bp_name' => $request->revisi_bp_name,
            'bp_code' => $request->revisi_bp_code,
            'phone' => $request->revisi_phone,
            'fax' => $request->revisi_fax,
            'address' => $request->revisi_address,
            'city' => $request->revisi_city,
            'postal' => $request->revisi_postal,
            'country' => $request->revisi_country,
            'kst_lcdate' => $request->revisi_kst_lcdate,
            'kst_statisticaldate' => $request->revisi_kst_statisticaldate,
            'trademark' => $request->revisi_trademark,
            'country_of_origin' => $request->revisi_country_of_origin,
            'made_in' => $request->revisi_made_in,
            'commodities_type' => $request->revisi_commodities_type,
            'customer_order_number' => $request->revisi_customer_order_number,
            'remark' => $request->revisi_remark,
            'upc' => $request->revisi_upc,
            'factory_id' => Auth::user()->factory_id,
            'created_at' => Carbon::now(),
            'is_cancel_order' => true,
            'old_po' => $oldpo,
            'season' => $request->revisi_season,
            'recycle' => isset($request->revisi_recycle) == 1 ? true : false
        );



        //file excel
        $filename = '';
        $results = array();
        if ($request->hasFile('revisi_file_upload')) {
            $extension = \File::extension($request->revisi_file_upload->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls") {
                $path = $request->revisi_file_upload->getRealPath();
                $filename = $request->revisi_file_upload->getClientOriginalName();
                $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
            }
        }

        //ambil data current packinglist


        $data_package_detail = array(); //data moved to table revisi_pl_history
        $data_package = array(); //data inserted to table package_detail
        $data_insert_package = array(); //data inserted to table package
        $data_movement = array(); //data inserted to table package_movements
        // $list_scanid = array(); //list scan_id for delete
        $ddmmyy = date_format(Carbon::now(), "d_m_Y");
        $count_inserted_data = 0;
        $count_ponumber = 0;

        foreach ($datax as $key => $value) {
            if (!isset($value->unit1) || !isset($value->unit2)) {
                return response()->json('Header `unit1` dan `unit2` tidak ditemukan', 422);
            }
            $po_number = trim($value->po);
            if ($po_number != $ponumber) {
                // return response()->json('Cek PO Number pada packinglist',422);
                continue;
            }

            //check apakah kolom from, to, scan_id ada valuenya tidak
            if ($value->from == '') {
                return response()->json('Kolom `from` harus terisi', 422);
            }

            if ($value->to == '') {
                return response()->json('Kolom `to` harus terisi', 422);
            }

            if ($value->scan_id == '') {
                return response()->json('Kolom `scan id` harus terisi', 422);
            }

            if ($value->qty_per_pkginner_pack == '' || $value->qty_per_pkginner_pack == 0 || $value->item_qty == '' || $value->item_qty == 0) {
                return response()->json('Inner pack & item qty tidak boleh kosong / 0', 422);
            }

            if ($this->_scan_id_check($value->scan_id) == false) {
                return response()->json('Check scan id packinglist, Scan Id sudah ada', 422);
            }

            $detail['scan_id'] = trim($value->scan_id);
            $detail['po_number'] = $po_number;
            $detail['plan_ref_number'] = $plan_ref_number_new;
            $detail['package_range'] = $value->range;
            $detail['package_from'] = $value->from;
            $detail['package_to'] = $value->to;
            $detail['serial_from'] = $value->serial_from;
            $detail['serial_to'] = $value->serial_to;
            $detail['pack_instruction_code'] = $value->pack_instruction_code;
            $detail['line'] = $value->line;
            $detail['sku'] = $value->sku;
            $detail['buyer_item'] = trim($value->buyer_item);
            $detail['model_number'] = $value->model_number;
            $detail['customer_size'] = trim($value->customer_size);
            $detail['customer_number'] = $value->customer_number;
            $detail['manufacturing_size'] = trim($value->manufacturing_size);
            $detail['technical_print_index'] = $value->technical_print_index;
            $detail['gps_three_digit_size'] = $value->gps_three_digit_size;
            $detail['service_identifier'] = $value->vasshas_g01_service_identifier;
            $detail['packing_mode'] = $value->vasshas_l15_packing_mode;
            $detail['item_qty'] = str_replace(array('.', ','), '', $value->item_qty);
            $detail['inner_pack'] = $value->qty_per_pkginner_pack;
            $detail['inner_pkg_count'] = $value->inner_pkg_count;
            $detail['pkg_count'] = $value->pkg_count;
            $detail['net_net'] = $value->net_net;
            $detail['net'] = $value->net;
            $detail['gross'] = $value->gross;
            $detail['unit_1'] = $value->unit1;
            $detail['length'] = $value->l;
            $detail['width'] = $value->w;
            $detail['height'] = $value->h;
            $detail['r'] = $value->r;
            $detail['pkg_code'] = $value->pkg_code;
            $detail['unit_2'] = $value->unit2;
            $detail['rasio'] = $this->_rasio_check($value->manufacturing_size);
            $detail['created_at'] = Carbon::now();
            $detail['factory_id'] = Auth::user()->factory_id;
            $detail['is_cancel_order'] = true;
            $detail['old_po'] = $oldpo;
            $data_package_detail[] = $detail;

            $count_ponumber++;
            $from = $value->from;
            $to = $value->to;
            $serial_from = $value->serial_from;
            $serial_to = $value->serial_to;
            $description = 'upload package';
            $department = 'preparation';
            $status = 'onprogress';

            for ($i = $from; $i <= $to; $i++) {
                $count_inserted_data++;
                $data['barcode_id'] = $this->random_code($i);
                $data['scan_id'] = trim($value->scan_id);
                $data['package_number'] = $i;
                $data['serial_number'] = $serial_from++;
                $data['current_department'] = $department;
                $data['current_status'] = $status;
                $data['status_preparation'] = $status;
                $data['created_at'] = Carbon::now();
                $data_package[] = $data;

                $data_m = array(
                    'barcode_package' => $data['barcode_id'],
                    'department_to' => $department,
                    'status_to' => $status,
                    'user_id' => Auth::user()->id,
                    'description' => $description,
                    'created_at' => Carbon::now()
                );
                $data_movement[] = $data_m;
            }
        }


        try {

            DB::begintransaction();
            $check = DB::table('po_summary')
                ->where('po_number', $ponumber)
                ->where('plan_ref_number', $plan_ref_number_new)
                ->where('factory_id', Auth::user()->factory_id)
                ->whereNull('deleted_at')
                ->first();

            if ($check) {
                throw new \Exception('Plan Ref Number already exists');
            } else {
                //insert status po_summary
                DB::table('po_summary')->insert($data_po_summary);

                //insert package detail
                DB::table('package_detail')->insert($data_package_detail);

                //insert package
                DB::table('package')->insert($data_package);

                //insert package movement
                DB::table('package_movements')->insert($data_movement);
            }


            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        //get total package
        $total_package = DB::table('package')
            ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
            ->where('po_number', $ponumber)
            ->whereNull('package.deleted_at')
            ->whereNull('package_detail.deleted_at')
            ->count();
        return response()->json($total_package, 200);
    } #revisiExcelCancel







    public function editPl(Request $request)
    {
        $po_number = $request->po_number;
        $plan_ref_number = $request->plan_ref_number;

        return view('packing/packing_list/edit_packinglist')->with('po_number', $po_number)
            ->with('plan_ref_number', $plan_ref_number);
    }

    public function getPackingList(Request $request)
    {
        $po_number = $request->po_number;
        $plan_ref_number = $request->plan_ref_number;
        $data = DB::table('po_summary')
            ->join('package_detail', function ($join) {
                $join->on('package_detail.po_number', '=', 'po_summary.po_number');
                $join->on('package_detail.plan_ref_number', '=', 'po_summary.plan_ref_number');
            })
            ->where('po_summary.po_number', $po_number)
            ->where('po_summary.plan_ref_number', $plan_ref_number)
            ->whereNull('po_summary.deleted_at')
            ->whereNull('package_detail.deleted_at');
        return DataTables::of($data)
            ->editColumn('buyer_item', function ($data) {
                return '<div id="buyer' . $data->scan_id . '">
                                    <input type="text"
                                        data-scanid="' . $data->scan_id . '"
                                        class="form-control buyer_unfilled"
                                        value="' . $data->buyer_item . '">
                                    </input>
                                </div>';
            })
            ->editColumn('customer_size', function ($data) {
                return '<div id="customer' . $data->scan_id . '">
                                    <input type="text"
                                        data-scanid="' . $data->scan_id . '"
                                        class="form-control customer_unfilled"
                                        value="' . $data->customer_size . '">
                                    </input>
                                </div>';
            })
            ->editColumn('manufacturing_size', function ($data) {
                return '<div id="manufacturing' . $data->scan_id . '">
                                    <input type="text"
                                        data-scanid="' . $data->scan_id . '"
                                        class="form-control manufacturing_unfilled"
                                        value="' . $data->manufacturing_size . '">
                                    </input>
                                </div>';
            })
            ->rawColumns(['buyer_item', 'customer_size', 'manufacturing_size'])
            ->make(true);
    }

    public function editBuyerItem(Request $request)
    {
        try {
            DB::beginTransaction();
            DB::table('package_detail')->where('scan_id', $request->scan_id)->update(['buyer_item' => trim($request->buyer_item)]);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function editCustomerSize(Request $request)
    {
        try {
            DB::beginTransaction();
            DB::table('package_detail')->where('scan_id', $request->scan_id)->update(['customer_size' => trim($request->customer_size)]);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function editManufacturingSize(Request $request)
    {
        try {
            DB::beginTransaction();
            DB::table('package_detail')->where('scan_id', $request->scan_id)->update(['manufacturing_size' => trim($request->manufacturing_size)]);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function sync_PL()
    {
        $data = DB::table('v_cek_item_scan_id')->whereNull('nscd')->get();
        $dinsert = [];
        try {
            DB::begintransaction();

            foreach ($data as $dt) {
                $pc['scan_id'] = $dt->scan_id;
                $pc['manufacturing_size'] = $dt->manufacturing_size;
                $pc['buyer_item'] = $dt->buyer_item;
                $pc['inner_pack'] = $dt->inner_pack;
                $pc['po_number'] = $dt->po_number;
                $pc['factory_id'] = $dt->factory_id;
                $pc['item_code'] = $dt->item_code;
                $pc['is_cancel_order'] = $dt->is_cancel_order;
                $pc['style'] = $dt->style;
                $dinsert[] = $pc;
            }

            DB::table('package_detail_item')->insert($dinsert);
            DB::commit();
        } catch (Exception $er) {
            db::rollback();
            $message = $er->getMessage();
            ErrorHandler::db($message);
        }
    }

    private function _scan_id_check($scanid)
    {
        $scan = DB::table('package_detail')->where('scan_id', trim($scanid))->whereNull('deleted_at')->first();

        if ($scan == null) {
            $permission = true;
        } else {
            $permission = false;
        }

        return $permission;
    }

    private function _rasio_check($manufacturing)
    {
        if (preg_match('[,]', $manufacturing)) {
            $rasio = true;
        } else {
            $rasio = false;
        }

        return $rasio;
    }

    private function _old_po($documentno, $ponumber)
    {
        $old = explode('-', $documentno)[1];
        if ($old != $ponumber) {
            $ret = $old;
        } else {
            $ret = '-';
        }
        return $ret;
    }
}
