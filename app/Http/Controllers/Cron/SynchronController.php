<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;

class SynchronController extends Controller
{
    public function __construct(){
        ini_set('max_execution_time', 1800);
    }
    //cron master po
  public function poMasterSync(){
      $from = date(Carbon::now()->subDays(7)->format('Y-m-d 00:00:00'));
      $to = date(Carbon::now()->addDays(250)->format('Y-m-d 00:00:00'));
      try {
           db::begintransaction();
                  // from erp 
            $erp = DB::connection('erp')->table('bw_fgms_check_shipment')
                      ->where(function ($query) use ($from,$to){
                          $query->whereBetween('podd_check',[$from,$to])
                                ->orwhereBetween('po_stat_date_adidas',[$from,$to])
                                ->orwhereBetween('crd',[$from,$to])
                                ->orwhereBetween('lc_date',[$from,$to]);
                      })
                      ->where('new_qty','<>',0)
                      ->whereIn('document_type',
                                    [
                                      'Sales Order Development FOC',
                                      'Sales Order Subcont',
                                      'Sales Order NEW BALANCE',
                                      'Sales Order Local',
                                      'Sales Order B Grade ADIDAS',
                                      'Sales Order Mizuno',
                                      'Sales Order PUMA',
                                      'Shipment Sample Subcon',
                                      'Sales Order ADIDAS',
                                      'Sales Order Other',
                                      'FOB'
                                ])
                      //->whereIn('po_number',['0128885049','0128885179','0128885187','0128885192'])
                      ->whereNotNull('style')
                      ->whereNotNull('brand')
                      ->get();
            // $pomaster = DB::table('po_master_sync');
            foreach ($erp as $erp) {


              $expo = "";
              $erpo = str_replace('^','',$erp->po_number);
              $cekpo = DB::table('po_master_sync')->where('po_number',$erpo)->first();
              $cekso = DB::table('po_master_sync')->where('so',str_replace('^','', $erp->so))->first();
            
                if ($erp->remark=='Voided') {
                  $cancel = true;
                }elseif ($erp->is_cancelorder=='Y') {
                  $cancel = true;
                }else{
                  $cancel = false;
                }

                // $expo = explode("-",$erp->so)[1];
                $expo= !empty($cekso->po_number) ? $cekso->po_number : $erpo;
                if ($expo==$erpo) {
                  $old_po = "-";
                }else{
                  $dup = array(
                    'is_cancel_order'=>true,
                    'update_at'=>Carbon::now()
                  );
                  DB::table('po_master_sync')->where('po_number',$expo)->where('is_edit',false)->update($dup);
                  $old_po=$expo;
                }
              
              if ($cekpo==null) {
              
                
                $dtin = array(
                    'po_number'=>$erpo,
                  'crd'=>$erp->crd,
                  'po_stat_date_adidas'=>$erp->po_stat_date_adidas,
                  'podd_check'=>$erp->podd_check,
                  'job'=>$erp->job,
                  'season'=>$erp->season,
                  'kst_lcdate'=>$erp->lc_date,
                  'product_category'=>$erp->product_category,
                  'product_category_detail'=>$erp->product_category_detail,
                  'order_type'=>$erp->order_type,
                  'document_type'=>$erp->document_type,
                  'customer_number'=>$erp->customer_order,
                  'customer_order_number'=>$erp->customer_order_number,
                  'sewing_place_update'=>$erp->sewing_place,
                  'stuffing_place_update'=>$erp->stuffing_place_update,
                  'country_name'=>$erp->country_name,
                  'so'=>str_replace('^','', $erp->so),
                  'pd'=>$erp->pd,
                  'remark'=>$erp->remark,
                  'is_reroute'=>$erp->is_reroute,
                  'is_cancel_order'=>$cancel,
                  'created_at'=>Carbon::now(),
                  'style'=>$erp->style,
                  'new_qty'=>(int)$erp->new_qty,
                  'brand'=>$erp->brand,
                  'old_po'=>$old_po,
                  'is_edit'=>false,
                  'factory_code'=>$erp->factorycode,
                  'uom'=>$erp->uom,
                  'po_grade'=>$erp->po_grade
                );

                  
                  DB::table('po_master_sync')->insert($dtin);
                  
              }elseif (($cekpo!=null)&&($cekpo->is_cancel_order==false)){
                
                

                $dtin = array(
                  'crd'=>$erp->crd,
                  'po_stat_date_adidas'=>$erp->po_stat_date_adidas,
                  'podd_check'=>$erp->podd_check,
                  'job'=>$erp->job,
                  'season'=>$erp->season,
                  'kst_lcdate'=>$erp->lc_date,
                  'product_category'=>$erp->product_category,
                  'product_category_detail'=>$erp->product_category_detail,
                  'order_type'=>$erp->order_type,
                  'document_type'=>$erp->document_type,
                  'customer_number'=>$erp->customer_order,
                  'customer_order_number'=>$erp->customer_order_number,
                  'sewing_place_update'=>$erp->sewing_place,
                  'stuffing_place_update'=>$erp->stuffing_place_update,
                  'country_name'=>$erp->country_name,
                  'so'=>str_replace('^','', $erp->so),
                  'pd'=>$erp->pd,
                  'remark'=>$erp->remark,
                  'is_reroute'=>$erp->is_reroute,
                  'is_cancel_order'=>$cancel,
                  'update_at'=>Carbon::now(),
                  'style'=>$erp->style,
                  'brand'=>$erp->brand,
                  'old_po'=>$old_po,
                  'is_edit'=>false,
                  'factory_code'=>$erp->factorycode,
                  'uom'=>$erp->uom,
                  'new_qty'=>(int)$erp->new_qty,
                  'po_grade'=>$erp->po_grade

                );

                  DB::table('po_master_sync')->where('po_number',$erpo)->where('is_edit',false)->update($dtin);
                
              }

            }
        db::commit();
      } catch (Exception $ex) {
          DB::rollback();
              $message = $ex->getMessage();
              ErrorHandler::db($message);
      }
  }
  // end cron master po_number

  // cron remark cancel 
  public function remarkCancel(){
      $from = date(Carbon::now()->subDays(200)->format('Y-m-d 00:00:00'));
      $to = date(Carbon::now()->addDays(30)->format('Y-m-d 00:00:00'));
      
      $datapo = DB::table('po_master_sync')
                        ->whereBetween('kst_lcdate',[$from,$to])
                        ->orderBy('kst_lcdate','asc')
                        ->get();

        if ($datapo->count()>0) {
            try {
                DB::begintransaction();
                    foreach ($datapo as $dt) {
                          $remark = array('is_cancel_order'=>$dt->is_cancel_order,'updated_at'=>Carbon::now());
                          DB::table('po_summary')->where('po_number',$dt->po_number)->where('is_cancel_order',false)->update($remark);
                          DB::table('package_detail')->where('po_number',$dt->po_number)->where('is_cancel_order',false)->update($remark);
                    }

                DB::commit();
            } catch (Exception $ex) {
              DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            }
        }
  }




  // public function getDate(){
  //   $submonth = date(Carbon::now()->subDays(1)->format('Y-m-d'));
  //   $monthname = date_format(date_create($submonth),"F");
  //   $now = date_create(date_format(Carbon::now(),'Y-m-d h:i:s'));

  //   $from = date(date_create($submonth)->format('Y-m-01'));
  //   $to = date(date_create($submonth)->format('Y-m-t'));
    
   
    
  //     // $path = storage_path('exports/');

  //     // $file = new Filesystem;
  //     // $file->cleanDirectory($path);

  //     try {
  //       $this->rekInventory($from,$to,$now);
  //       $this->rekShipping($from,$to,$now);
  //       $this->rekBacktosewing($from,$to,$now);
  //       $this->rekBalanceStock($from,$to,$now);
  //       $this->rekDeletePL($from,$to,$now);
  //     } catch (Exception $e) {
  //       $e->getMessage();
  //     }
    
  // }


  private function tanggal(){
    $submonth = date(Carbon::now()->subDays(1)->format('Y-m-d'));
    $monthname = date_format(date_create($submonth),"F");
    $now = date_create(date_format(Carbon::now(),'Y-m-d h:i:s'));

    $from = date(date_create($submonth)->format('Y-m-01'));
    $to = date(date_create($submonth)->format('Y-m-t'));
    
   
    return $date= array('from'=>'2023-05-01','to'=>'2023-05-31','now'=>'2023-06-01');
    
  }


  public function rekInventory(){
    DB::statement('REFRESH MATERIALIZED VIEW ns_rekon_inv');
      
      $from = $this->tanggal()['from'];
      $to = $this->tanggal()['to'];
      $now = $this->tanggal()['now'];

      $path = storage_path('exports/Rekon_Inventory');

      $file = new Filesystem;
      $file->cleanDirectory($path);

      $data = DB::table('ns_rekon_inv')
                  ->whereBetween('checkin',[$from,$to])
                  ->get();
      $file_name = "Rekon_Inventory";

      $i =1;

      $export = \Excel::create($file_name,function($excel) use ($data,$i,$now){
        $excel->sheet('inventory',function($sheet) use ($data,$i,$now){
              $sheet->appendRow(array(
              '#',
              'Barcode',
              'Style',
              'Article',
              'PO Number',
              'Plan Ref.',
              'Brand',
              'Prod. Ctg.',
              'Cust. Size',
              'Man. Size',
              'Qty Pack',
              'Location',
              'CheckIn',
              'Item Code',
              'Factory',
              'Qty Ratio',
              'Date Balance'
            ));

            foreach ($data as $dt) {

              if ($dt->current_department=='inventory'&&$dt->current_status=='onprogress') {
                $locat = "METAL DETECTOR";
              }else if ($dt->current_department=='inventory'&&$dt->current_status=='completed') {
                $locat = "RACK";
              }else if ($dt->current_department=='qcinspect') {
                $locat = "QC INSPECT";
              }else if ($dt->current_department=='shipping'&&$dt->current_status=='onprogress') {
                $locat = "LOADING";
              }else if ($dt->current_department=='shipping'&&$dt->current_status=='completed') {
                $locat = "SHIPMENT";
              }else if ($dt->current_department=='preparation') {
                $locat = "BACK TO SEWING";
              }else if ($dt->current_department=='sewing') {
                $locat = "BACK TO SEWING REFILLING";
              }

                $sheet->appendRow(array(
                  $i++,
                  '="'.$dt->barcode_id.'"',
                  $dt->upc,
                  $dt->buyer_item,
                  '="'.$dt->po_number.'"',
                  '="'.$dt->plan_ref_number.'"',
                  $dt->brand,
                  $dt->product_category_detail,
                  $dt->customer_size,
                  $dt->manufacturing_size,
                  $dt->qty_pack,
                  $locat,
                  $dt->checkin,
                  $dt->item_code,
                  $dt->factory_id,
                  $dt->inner_pack,
                  $now
                  ));
            }
        });
          
      })->store('xlsx');
  }

  public function rekShipping(){
    DB::statement('REFRESH MATERIALIZED VIEW ns_rekon_ship');
     
      $from = $this->tanggal()['from'];
      $to = $this->tanggal()['to'];
      $now = $this->tanggal()['now'];

      $path = storage_path('exports/Rekon_Shipment');

      $file = new Filesystem;
      $file->cleanDirectory($path);

      $data = DB::table('ns_rekon_ship')
                  ->whereBetween('checkout',[$from,$to])
                  ->get();
      $file_name = "Rekon_Shipment";

      $i =1;

      $export = \Excel::create($file_name,function($excel) use ($data,$i,$now){
        $excel->sheet('shipment',function($sheet) use ($data,$i,$now){
              $sheet->appendRow(array(
              '#',
              'Barcode',
              'Style',
              'Article',
              'PO Number',
              'Plan Ref.',
              'Brand',
              'Prod. Ctg.',
              'Cust. Size',
              'Man. Size',
              'Qty Pack',
              'Location',
              'Checkout',
              'Item Code',
              'Factory',
              'Qty Ratio',
              'Date Balance'
            ));

            foreach ($data as $dt) {

              if ($dt->current_department=='inventory'&&$dt->current_status=='onprogress') {
                $locat = "METAL DETECTOR";
              }else if ($dt->current_department=='inventory'&&$dt->current_status=='completed') {
                $locat = "RACK";
              }else if ($dt->current_department=='qcinspect') {
                $locat = "QC INSPECT";
              }else if ($dt->current_department=='shipping'&&$dt->current_status=='onprogress') {
                $locat = "LOADING";
              }else if ($dt->current_department=='shipping'&&$dt->current_status=='completed') {
                $locat = "SHIPMENT";
              }else if ($dt->current_department=='preparation') {
                $locat = "BACK TO SEWING";
              }else if ($dt->current_department=='sewing') {
                $locat = "BACK TO SEWING REFILLING";
              }

                $sheet->appendRow(array(
                  $i++,
                  '="'.$dt->barcode_id.'"',
                  $dt->upc,
                  $dt->buyer_item,
                  '="'.$dt->po_number.'"',
                  '="'.$dt->plan_ref_number.'"',
                  $dt->brand,
                  $dt->product_category_detail,
                  $dt->customer_size,
                  $dt->manufacturing_size,
                  $dt->qty_pack,
                  $locat,
                  $dt->checkout,
                  $dt->item_code,
                  $dt->factory_id,
                  $dt->inner_pack,
                  $now
                  ));
            }
        });
          
      })->store('xlsx');
  }

  public function rekBacktosewing(){
    DB::statement('REFRESH MATERIALIZED VIEW ns_rekon_backsewing');
      
      $from = $this->tanggal()['from'];
      $to = $this->tanggal()['to'];
      $now = $this->tanggal()['now'];

      $path = storage_path('exports/Rekon_Backtosewing');

      $file = new Filesystem;
      $file->cleanDirectory($path);

      $data = DB::table('ns_rekon_backsewing')
                  ->whereBetween('checkout',[$from,$to])
                  ->get();
      $file_name = "Rekon_Backtosewing";

      $i =1;

      $export = \Excel::create($file_name,function($excel) use ($data,$i,$now){
        $excel->sheet('backtosewing',function($sheet) use ($data,$i,$now){
              $sheet->appendRow(array(
              '#',
              'Barcode',
              'Style',
              'Article',
              'PO Number',
              'Plan Ref.',
              'Brand',
              'Prod. Ctg.',
              'Cust. Size',
              'Man. Size',
              'Qty Pack',
              'Location',
              'Checkout',
              'Item Code',
              'Factory',
              'Qty Ratio',
              'Date Balance'
            ));

            foreach ($data as $dt) {

              if ($dt->current_department=='inventory'&&$dt->current_status=='onprogress') {
                $locat = "METAL DETECTOR";
              }else if ($dt->current_department=='inventory'&&$dt->current_status=='completed') {
                $locat = "RACK";
              }else if ($dt->current_department=='qcinspect') {
                $locat = "QC INSPECT";
              }else if ($dt->current_department=='shipping'&&$dt->current_status=='onprogress') {
                $locat = "LOADING";
              }else if ($dt->current_department=='shipping'&&$dt->current_status=='completed') {
                $locat = "SHIPMENT";
              }else if ($dt->current_department=='preparation') {
                $locat = "BACK TO SEWING";
              }else if ($dt->current_department=='sewing') {
                $locat = "BACK TO SEWING REFILLING";
              }

                $sheet->appendRow(array(
                  $i++,
                  '="'.$dt->barcode_id.'"',
                  $dt->upc,
                  $dt->buyer_item,
                  '="'.$dt->po_number.'"',
                  '="'.$dt->plan_ref_number.'"',
                  $dt->brand,
                  $dt->product_category_detail,
                  $dt->customer_size,
                  $dt->manufacturing_size,
                  $dt->qty_pack,
                  $locat,
                  $dt->checkout,
                  $dt->item_code,
                  $dt->factory_id,
                  $dt->inner_pack,
                  $now
                  ));
            }
        });
          
      })->store('xlsx');
  }

  public function rekBalanceStock(){
    DB::statement('REFRESH MATERIALIZED VIEW ns_rekon_balance');
   
       $from = $this->tanggal()['from'];
      $to = $this->tanggal()['to'];
      $now = $this->tanggal()['now'];
      // dd($from,$to,$now);
      $path = storage_path('exports/Rekon_Balancestock');

      $file = new Filesystem;
      $file->cleanDirectory($path);

      $date = date_format(date_create($to),'Y-m-d');

      $data = DB::table('ns_rekon_balance')
                  ->where('date_balance',$date)
                  ->get();

      $file_name = "Rekon_Balancestock";

      $i =1;

      $export = \Excel::create($file_name,function($excel) use ($data,$i,$now){
        $excel->sheet('balancestock',function($sheet) use ($data,$i,$now){
              $sheet->appendRow(array(
              '#',
              'Barcode',
              'Style',
              'Article',
              'PO Number',
              'Plan Ref.',
              'Brand',
              'Prod. Ctg.',
              'Cust. Size',
              'Man. Size',
              'Qty Pack',
              'Location',
              'Checkout',
              'Item Code',
              'Factory',
              'Qty Ratio',
              'Loc. Status',
              'Date Balance'
            ));

            foreach ($data as $dt) {


                $sheet->appendRow(array(
                  $i++,
                  '="'.$dt->barcode_id.'"',
                  $dt->style,
                  $dt->buyer_item,
                  '="'.$dt->po_number.'"',
                  '="'.$dt->plan_ref_number.'"',
                  $dt->trademark,
                  $dt->product_category_detail,
                  $dt->customer_size,
                  $dt->manufacturing_size,
                  $dt->qty_pack,
                  $dt->balance_location,
                  $dt->checkin,
                  $dt->item_code,
                  $dt->factory_id,
                  $dt->qty_ratio,
                  $dt->status_location,
                  $dt->date_balance
                  ));
            }
        });
          
      })->store('xlsx');
  }

  public function rekDeletePL(){

      DB::statement('REFRESH MATERIALIZED VIEW ns_rekon_delete');
      
       $from = $this->tanggal()['from'];
      $to = $this->tanggal()['to'];
      $now = $this->tanggal()['now'];

      $path = storage_path('exports/Rekon_Delete_PL');

      $file = new Filesystem;
      $file->cleanDirectory($path);

      // $date = date_format(date_create($to),'Y-m-d');
      $data = DB::table('ns_rekon_delete')
                  ->whereBetween('checkout',[$from,$to])
                  ->get();
      $file_name = "Rekon_Delete_PL";

      $i =1;

      $export = \Excel::create($file_name,function($excel) use ($data,$i,$now){
        $excel->sheet('deletepackinglist',function($sheet) use ($data,$i,$now){
              $sheet->appendRow(array(
              '#',
              'Barcode',
              'Style',
              'Article',
              'PO Number',
              'Plan Ref.',
              'Brand',
              'Prod. Ctg.',
              'Cust. Size',
              'Man. Size',
              'Qty Pack',
              'Location',
              'Checkout',
              'Item Code',
              'Factory',
              'Qty Ratio',
              'Date Balance'
            ));

            foreach ($data as $dt) {

              if ($dt->current_department=='inventory'&&$dt->current_status=='onprogress') {
                $locat = "METAL DETECTOR";
              }else if ($dt->current_department=='inventory'&&$dt->current_status=='completed') {
                $locat = "RACK";
              }else if ($dt->current_department=='qcinspect') {
                $locat = "QC INSPECT";
              }else if ($dt->current_department=='shipping'&&$dt->current_status=='onprogress') {
                $locat = "LOADING";
              }else if ($dt->current_department=='shipping'&&$dt->current_status=='completed') {
                $locat = "SHIPMENT";
              }else if ($dt->current_department=='preparation') {
                $locat = "BACK TO SEWING";
              }else if ($dt->current_department=='sewing') {
                $locat = "BACK TO SEWING REFILLING";
              }

                $sheet->appendRow(array(
                  $i++,
                  '="'.$dt->barcode_id.'"',
                  $dt->upc,
                  $dt->buyer_item,
                  '="'.$dt->po_number.'"',
                  '="'.$dt->plan_ref_number.'"',
                  $dt->brand,
                  $dt->product_category_detail,
                  $dt->customer_size,
                  $dt->manufacturing_size,
                  $dt->qty_pack,
                  $locat,
                  $dt->checkout,
                  $dt->item_code,
                  $dt->factory_id,
                  $dt->inner_pack,
                  $now
                  ));
            }
        });
          
      })->store('xlsx');
  }

  // end cron remark cancel

  // po master sync manual
  public function syncpo($po_number){
    
      try {
           db::begintransaction();
                  // from erp 
            $erp = DB::connection('erp')->table('bw_fgms_check_shipment')
                      ->where('new_qty','<>',0)
                      ->whereIn('document_type',
                                    [
                                      'Sales Order Development FOC',
                                      'Sales Order Subcont',
                                      'Sales Order NEW BALANCE',
                                      'Sales Order Local',
                                      'Sales Order B Grade ADIDAS',
                                      'Sales Order Mizuno',
                                      'Sales Order PUMA',
                                      'Shipment Sample Subcon',
                                      'Sales Order ADIDAS'
                                ])
                      ->where('po_number',$po_number)
                      ->whereNotNull('style')
                      ->whereNotNull('brand')
                      ->get();
            // $pomaster = DB::table('po_master_sync');
            foreach ($erp as $erp) {


              $expo = "";
              $erpo = str_replace('^','',$erp->po_number);
              $cekpo = DB::table('po_master_sync')->where('po_number',$erpo)->first();
              $cekso = DB::table('po_master_sync')->where('so',str_replace('^','', $erp->so))->first();
            
                if ($erp->remark=='Voided') {
                  $cancel = true;
                }elseif ($erp->is_cancelorder=='Y') {
                  $cancel = true;
                }else{
                  $cancel = false;
                }

                // $expo = explode("-",$erp->so)[1];
                $expo= !empty($cekso->po_number) ? $cekso->po_number : $erpo;
                if ($expo==$erpo) {
                  $old_po = "-";
                }else{
                  $dup = array(
                    'is_cancel_order'=>true,
                    'update_at'=>Carbon::now()
                  );
                  DB::table('po_master_sync')->where('po_number',$expo)->where('is_edit',false)->update($dup);
                  $old_po=$expo;
                }
              
              if ($cekpo==null) {
              
                
                $dtin = array(
                    'po_number'=>$erpo,
                  'crd'=>$erp->crd,
                  'po_stat_date_adidas'=>$erp->po_stat_date_adidas,
                  'podd_check'=>$erp->podd_check,
                  'job'=>$erp->job,
                  'season'=>$erp->season,
                  'kst_lcdate'=>$erp->lc_date,
                  'product_category'=>$erp->product_category,
                  'product_category_detail'=>$erp->product_category_detail,
                  'order_type'=>$erp->order_type,
                  'document_type'=>$erp->document_type,
                  'customer_number'=>$erp->customer_order,
                  'customer_order_number'=>$erp->customer_order_number,
                  'sewing_place_update'=>$erp->sewing_place,
                  'stuffing_place_update'=>$erp->stuffing_place_update,
                  'country_name'=>$erp->country_name,
                  'so'=>str_replace('^','', $erp->so),
                  'pd'=>$erp->pd,
                  'remark'=>$erp->remark,
                  'is_reroute'=>$erp->is_reroute,
                  'is_cancel_order'=>$cancel,
                  'created_at'=>Carbon::now(),
                  'style'=>$erp->style,
                  'new_qty'=>(int)$erp->new_qty,
                  'brand'=>$erp->brand,
                  'old_po'=>$old_po,
                  'is_edit'=>false,
                  'factory_code'=>$erp->factorycode,
                  'uom'=>$erp->uom
                );

                  
                  DB::table('po_master_sync')->insert($dtin);
                  
              }elseif (($cekpo!=null)&&($cekpo->is_cancel_order==false)){
                
                

                $dtin = array(
                  'crd'=>$erp->crd,
                  'po_stat_date_adidas'=>$erp->po_stat_date_adidas,
                  'podd_check'=>$erp->podd_check,
                  'job'=>$erp->job,
                  'season'=>$erp->season,
                  'kst_lcdate'=>$erp->lc_date,
                  'product_category'=>$erp->product_category,
                  'product_category_detail'=>$erp->product_category_detail,
                  'order_type'=>$erp->order_type,
                  'document_type'=>$erp->document_type,
                  'customer_number'=>$erp->customer_order,
                  'customer_order_number'=>$erp->customer_order_number,
                  'sewing_place_update'=>$erp->sewing_place,
                  'stuffing_place_update'=>$erp->stuffing_place_update,
                  'country_name'=>$erp->country_name,
                  'so'=>str_replace('^','', $erp->so),
                  'pd'=>$erp->pd,
                  'remark'=>$erp->remark,
                  'is_reroute'=>$erp->is_reroute,
                  'is_cancel_order'=>$cancel,
                  'update_at'=>Carbon::now(),
                  'style'=>$erp->style,
                  'brand'=>$erp->brand,
                  'old_po'=>$old_po,
                  'is_edit'=>false,
                  'factory_code'=>$erp->factorycode,
                  'uom'=>$erp->uom,
                  'new_qty'=>(int)$erp->new_qty

                );

                  DB::table('po_master_sync')->where('po_number',$erpo)->where('is_edit',false)->update($dtin);
                
              }

            }
        db::commit();
      } catch (Exception $ex) {
          DB::rollback();
              $message = $ex->getMessage();
              ErrorHandler::db($message);
      }
  }
  // po master sync manual

}
