<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use DataTables;
use Carbon\Carbon;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Session;
use Auth;
use App\Area;
use App\Locator;

class AreaController extends Controller
{
    public function index() {
        return view('inventory/area');
    }

    public function ajaxGetData(Request $request) {
        $factory_id = Auth::user()->factory_id;

        if ($request->ajax()){

            $data = DB::table('areas')
                        ->select('id','name','user_id','warehouse','factory_id')
                        ->where('is_active', true)
                        ->where('factory_id', $factory_id)
                        ->whereNull('deleted_at')
                        ->orderBy('name', 'asc');

            return DataTables::of($data)
                    ->addColumn('action', function($data) {
                        return view('_action', [
                                    'model' => $data,
                                    'location' => route('area.location',[
                                        'areaid'   => $data->id,
                                        'areaname' => strtoupper($data->name)
                                    ]),
                                    'edit' => route('area.edit', $data->id),
                                    'areaid' => $data->id,
                                    'delete_area' => route('area.delete', $data->id)]);

                    })
                   ->make(true);
        }
    }

    public function update(Request $request){
        $data = $request->all();
        $area = DB::table('areas')
                        ->where('id', $data['id'])
                        ->update([
                            'name' => $data['name'],
                        ]);

        return redirect('inventory/area');
    }

    //return index location
    public function viewLocation() {
        return view('inventory/location');
    }

    //add new area
    public function ajaxAddArea(Request $request) {
        $this->validate($request, [
            'areaname' => 'required'
        ]);

        $data = array([
            'name' => strtoupper($request->areaname),
            'user_id' => Auth::user()->id,
            'warehouse' => Auth::user()->warehouse,
            'factory_id' => Auth::user()->factory_id,
            'is_active' => true,
            'created_at' => Carbon::now()
        ]);

        try {
            DB::table('areas')->insert($data);
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

    }

    //edit area
    public function edit($id){
        $area = Area::find($id);
            return view('inventory/edit_area', compact('areas'))
                ->with('area',$area);

    }

    //edit location
    public function editLocation($id){
        $location = Locator::find($id);
            return view('inventory/edit_location', compact('locators'))
                ->with('location',$location);

    }

    public function updateLocation(Request $request){
        $this->validate($request, [
            'rack' => 'numeric'
        ]);

        $data = $request->all();
        $location = DB::table('locators')
                        ->where('id', $data['id'])
                        ->update([
                            'code' => $data['code'],
                            'rack' => $data['rack']
                            ]);

        return redirect('inventory/area');
    }

    //get data location based on area id
    public function ajaxGetLocation(Request $request) {
        $area_id = $request->areaid;
        $data = DB::table('locators')
                ->where('area_id', $area_id)
                ->whereNull('deleted_at')
                ->orderBy('rack','asc');

        return DataTables::of($data)
                ->addColumn('action', function($data) {
                    return view('_action', [
                                'model' => $data,
                                'print_rack'=>route('area.rackFG',['id'=>$data->id]),
                                'edit' => route('location.edit', $data->id),
                                'locationid' => $data->id,
                                'delete_location' => route('location.delete', $data->id)]);

                })
               ->make(true);
    }

    //add new location
    public function ajaxAddLocation(Request $request) {
        $areaid = $request->areaid;
        $areaname = $request->areaname;
        $rack = strtoupper($request->rack);
        $code = strtoupper($areaname).'-'.$rack;

        $this->validate($request, [
            'rack' => 'required|numeric'
        ]);

        $data = array(
            'code' => $code,
            'area_id' => $areaid,
            'rack' => $rack,
            'is_active' => true,
            'created_at' => Carbon::now(),
            'factory_id' => Auth::user()->factory_id,
            'barcode' => $this->random_code($code)
        );

        try {
            DB::table('locators')->insert($data);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function destroy(Request $request)
    {
        $id = $request->id;
        $area = Area::find($id);
        $area->deleted_at = carbon::now();
        $area->save();
    }

    public function destroyLocation(Request $request){
        $id = $request->id;
        $area = Locator::find($id);
        $area->deleted_at = carbon::now();
        $area->save();
    }

    static function random_code($string){
        return 'R'.$string;
    }

    //print all locator
    public function printAllLocator() {
        $data = DB::table('areas')
                    ->select('locators.barcode','locators.code')
                    ->join('locators', function($join) {
                        $join->on('locators.area_id', '=', 'areas.id');
                        $join->on('locators.factory_id', '=', 'areas.factory_id');
                    })
                    // ->join('locators', 'locators.area_id', '=', 'areas.id')
                    ->where('areas.factory_id', Auth::user()->factory_id)
                    ->whereNull('areas.deleted_at')
                    ->whereNull('locators.deleted_at')
                    ->get();

        $papersize = 'a4';
        $orientation = 'portrait';
        $pdf = \PDF::loadView('inventory.print_locator',
                                [
                                    'data' => $data
                                ])
                       ->setPaper($papersize, $orientation);

        return $pdf->stream('print_all_locator');
    }

    public function printFG(Request $request){
        $area_id = $request->areaid;

       $data = DB::table('areas')
                    ->select('locators.barcode','locators.code')
                    ->join('locators', function($join) {
                        $join->on('locators.area_id', '=', 'areas.id');
                        $join->on('locators.factory_id', '=', 'areas.factory_id');
                    })
                    // ->join('locators', 'locators.area_id', '=', 'areas.id')
                    ->where('areas.factory_id', Auth::user()->factory_id)
                    ->where('areas.id',$area_id)
                    ->whereNull('areas.deleted_at')
                    ->whereNull('locators.deleted_at')
                    ->get();


        $papersize = 'a4';
        $orientation = 'portrait';
        $pdf = \PDF::loadView('inventory.print_fg',
                                [
                                    'data' => $data
                                ])
                       ->setPaper($papersize, $orientation);

        return $pdf->stream('print_area');

    }

    public function printRack(Request $request){
        $id = $request->id;

         $data = DB::table('areas')
                    ->select('locators.barcode','locators.code')
                    ->join('locators', function($join) {
                        $join->on('locators.area_id', '=', 'areas.id');
                        $join->on('locators.factory_id', '=', 'areas.factory_id');
                    })
                    // ->join('locators', 'locators.area_id', '=', 'areas.id')
                    ->where('areas.factory_id', Auth::user()->factory_id)
                    ->where('locators.id',$id)
                    ->whereNull('areas.deleted_at')
                    ->whereNull('locators.deleted_at')
                    ->get();


        $papersize = 'A4';
        $orientation = 'portrait';
        $pdf = \PDF::loadView('inventory.print_rack',
                                [
                                    'data' => $data
                                ])
                       ->setPaper($papersize, $orientation);

        return $pdf->stream('print_rack');
    }
}
