<?php 
namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

/**
 * 
 */
class MoveController extends Controller
{
	
	public function checkout(){
		return view('inventory/move/checkout');
	}

	public function ajaxCheckout(Request $request){
		$barcode_id = $request->barcode_id;
		$ip = $request->ip();
		$fac = auth::user()->factory_id;
		$check = $this->checkpackage($barcode_id);

		if (!isset($check)) {
			return response()->json('Package not found ! ! !',422);
		}

		if ($check->current_department!='inventory' && $check!='completed') {
			return response()->json('Package Last Location '.$check->current_department.' status '.$check->current_status.' ! ! !',422);
		}

		if ($check->factory_id!=$fac) {
			return response()->json('Package Location in AOI '.$check->factory_id,422);
		}

		try {
			DB::begintransaction();
				$query = $this->_packageMovement($barcode_id,'checkout',$ip);
				if ($query) {
					$incancel = array(
						'scan_id'=>$check->scan_id,
						'barcode_id'=>$check->barcode_id,
						'is_integrate'=>false,
						'integration_date'=>null,
						'created_at'=>Carbon::now()
					);

					DB::table('package_cancel_integration')->insert($incancel);

					$data = array(
						'barcode_id'=>$check->barcode_id,
						'plan_ref_number'=>$check->plan_ref_number,
						'po_number'=>$check->po_number,
						'department_to'=>"handover",
						'status_to'=>"onprogress"
					);
				}
			DB::commit();
		} catch (Exception $ex) {
			 db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
		}

		return view('inventory/move/ajax_move_out')->with('data', $data);
		
	}


	public function checkin(){
		return view('inventory/move/checkin');
	}

	public function ajaxCheckin(Request $request){
		$barcode_id = $request->barcode_id;
		$ip = $request->ip();
		$fac = auth::user()->factory_id;
		$check = $this->checkpackage($barcode_id);

		$cek_move = DB::table('package')->where('barcode_id',$barcodeid)->first();
        if(isset($cek_move) && $cek_move->is_move_to_fgms_new == true){
            return response()->json("Package sudah dipindah di FGMS BARU",422);

        }

		if (!isset($check)) {
			return response()->json('Package not found ! ! !',422);
		}
	
		if ($check->current_department!='handover' && $check!='onprogress') {
			return response()->json('Package Last Location '.$check->current_department.' status '.$check->current_status.' ! ! !',422);
		}

		if ($fac!=$check->factory_id) {
			return response()->json('Package Location in AOI '.$check->factory_id,422);
		}

		try {
			DB::begintransaction();
				$query = $this->_packageMovement($barcode_id,'checkin',$ip);
				if ($query) {
					$data = array(
						'barcode_id'=>$check->barcode_id,
						'plan_ref_number'=>$check->plan_ref_number,
						'po_number'=>$check->po_number,
						'department_to'=>"inventory",
						'status_to'=>"onprogress"
					);
				}
			DB::commit();
		} catch (Exception $ex) {
			 db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
		}

		return view('inventory/move/ajax_move_in')->with('data', $data);
		
	}

	public function index(){
		return view('inventory/move/index');
	}

	public function ajaxGetPo(Request $request){
		$po_number = $request->po_number;

		$data = DB::table('po_summary')
						->join('package_detail',function($join){
							$join->on('po_summary.po_number','=','package_detail.po_number');
							$join->on('po_summary.plan_ref_number','=','package_detail.plan_ref_number');
						})
						->where('po_summary.po_number',$po_number)
						->where('po_summary.factory_id',auth::user()->factory_id)
						->whereNull('po_summary.deleted_at')
						->whereNull('package_detail.deleted_at')
						->groupBy('po_summary.po_number')
						->groupBy('po_summary.plan_ref_number')
						->groupBy('po_summary.factory_id')
						->select('po_summary.po_number','po_summary.plan_ref_number','po_summary.factory_id');

		return DataTables::of($data)
						->addColumn('ctn',function($data){
							$ctn = $this->_checkCtn($data->po_number,$data->plan_ref_number,$data->factory_id,'all');
							return $ctn;
						})
						->editColumn('factory_id',function($data){
							switch ($data->factory_id) {
								case 1:
									$to = 2;
									break;

								case 2:
									$to = 1;
									break;
								
							}

							return '<span class="badge badge-primary">From AOI '.$data->factory_id.'</span> <span class="badge badge-success"> TO AOI '.$to.'</span>';
						})
						->addColumn('action',function($data){
							return '<button class="btn btn-warning" data-po="'.$data->po_number.'" data-plan_ref="'.$data->plan_ref_number.'" data-factory="'.$data->factory_id.'" onclick="setpo(this);">Setting</button>';
						})
						->rawColumns(['ctn','factory_id','action'])
						->make(true);
	}

	public function setHandover(Request $request){
		$po_number = $request->po_number;
		$plan_ref_number = $request->plan_ref_number;
		$from = $request->factory_id;
		$nobc = $request->nobc;

		switch ($from) {
			case 1:
				$to = 2;
				break;

			case 2:
				$to = 1;
				break;
		}

		$all = $this->_checkCtn($po_number,$plan_ref_number,$from,'all');
		$hand = $this->_checkCtn($po_number,$plan_ref_number,$from,'handover');
		

		if ($all!=$hand) {
			return response()->json("Package not scan handover all ! ! !",422);
		}

		try {
			DB::begintransaction();
				$changepo = DB::table('po_summary')->where('po_number',$po_number)->where('plan_ref_number',$plan_ref_number)->whereNull('deleted_at')->update(['factory_id'=>$to,'updated_at'=>Carbon::now()]);
				$changepd = DB::table('package_detail')->where('po_number',$po_number)->where('plan_ref_number',$plan_ref_number)->whereNull('deleted_at')->update(['factory_id'=>$to,'updated_at'=>Carbon::now()]);

				if ($changepo && $changepd) {
					$inhand = array(
						'po_number'=>$po_number,
						'plan_ref_number'=>$plan_ref_number,
						'factory_from'=>$from,
						'factory_to'=>$to,
						'bc_number'=>$nobc,
						'user_id'=>Auth::user()->id,
						'created_at'=>Carbon::now()
					);
					DB::table('handover')->insert($inhand);
				}
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => "Handover setting Success ! ! !"
                          ];
		} catch (Exception $ex) {
			db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => "Handover setting field ! ! !"
                          ];
		}

		return response()->json(['data'=>$data_response]);
	}



	private function _packageMovement($barcode_id,$status,$ip){
		$lastmove = DB::table('package_movements')->where('barcode_package',$barcode_id)->whereNull('deleted_at')->where('is_canceled',false)->first();
		switch (auth::user()->factory_id) {
			case 1:
				$fac_to = 2;
				break;
			case 2:
				$fac_to = 1;
				break;
		
		}

		switch ($status) {
			case 'checkout':
				$up_pack = array(
						'current_department'=>"handover",
						'current_status'=>"onprogress",
						'status_inventory'=>null,
						'is_integrate'=>false,
						'integration_date'=>null,
						'updated_at'=>Carbon::now()
					);
				$in_move = array(
					'barcode_package'=>$barcode_id ,
					'department_from'=>$lastmove->department_to,
					'department_to'=>"handover",
					'status_from'=>$lastmove->status_to,
					'status_to'=>"onprogress",
					'user_id'=>auth::user()->id,
					'created_at'=>Carbon::now(),
					'description'=>"checkout packing AOI ".auth::user()->factory_id." to AOI ".$fac_to,
					'is_canceled'=>false,
					'ip_address'=>$ip
				);



				break;

			case 'checkin':
				$up_pack = array(
						'current_department'=>"inventory",
						'current_status'=>"onprogress",
						'status_inventory'=>"onprogress",
						'updated_at'=>Carbon::now()
					);
				$in_move = array(
					'barcode_package'=>$barcode_id ,
					'department_from'=>$lastmove->department_to,
					'department_to'=>"inventory",
					'status_from'=>$lastmove->status_to,
					'status_to'=>"onprogress",
					'user_id'=>auth::user()->id,
					'created_at'=>Carbon::now(),
					'description'=>"checkin packing AOI ".$fac_to." from AOI ".auth::user()->factory_id,
					'is_canceled'=>false,
					'ip_address'=>$ip
				);
				
				break;
		}

		try {
			DB::begintransaction();
				DB::table('package')->where('barcode_id',$barcode_id)->update($up_pack);
				DB::table('package_movements')->where('barcode_package',$barcode_id)->update(['deleted_at'=>Carbon::now()]);
				DB::table('package_movements')->insert($in_move);
			DB::commit();
		} catch (Exception $ex) {
			 db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
		}

		return true;
	}

	private function checkpackage($barcode_id){
		$check = DB::table('package_detail')
							->join('package','package_detail.scan_id','=','package.scan_id')
							->join('package_movements','package_movements.barcode_package','=','package.barcode_id')
							->whereNull('package_detail.deleted_at')
							->whereNull('package.deleted_at')
							->whereNull('package_movements.deleted_at')
							->where('package_movements.is_canceled',false)
							->where('package.barcode_id',$barcode_id)
							// ->where('package_detail.factory_id',auth::user()->factory_id)
							->select('package_detail.scan_id','package_detail.buyer_item','package_detail.customer_size','package_detail.manufacturing_size','package_detail.inner_pack','package.barcode_id','package.current_department','package.current_status','package_movements.barcode_locator','package_detail.factory_id','package_detail.plan_ref_number','package_detail.po_number')
							->first();

		return $check;
	}

	private function _checkCtn($po_number,$plan_ref_number,$factory,$cek){
		$data = DB::table('package_detail')
				->join('package','package.scan_id','=','package_detail.scan_id')
				->whereNull('package.deleted_at')
				->whereNull('package_detail.deleted_at')
				->where('package_detail.po_number',$po_number)
				->where('package_detail.plan_ref_number',$plan_ref_number)
				->where('package_detail.factory_id',$factory);
				
		if ($cek=='handover') {
			$data = $data->where('package.current_department','handover')
						->where('package.current_status','onprogress');
		}elseif($cek=='all'){
			$data = $data;
		}

		$data = $data->groupBy('package_detail.po_number')
				->groupBy('package_detail.plan_ref_number')
				->groupBy('package_detail.factory_id')
				->count('package.barcode_id');

		return $data;

	}

	
}
 ?>