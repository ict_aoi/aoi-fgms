<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

class InventoryController extends Controller
{
    // halaman checkin
    public function index() {
        return view('inventory/checkin/index');
    }

    //halaman placement
    public function placement() {
        return view('inventory/placement/index');
    }

    //halaman placement check
    public function placementCheck() {
        return view('inventory/placement/check');
    }

    /*
    Constraint :
        yang boleh di scan onprogress
        a. department :sewing, status :completed
        b. department :qcinspect, status :completed (completed all)
        c. department :shipping, status :onprogress

        yang boleh di scan rejected
        a. department :sewing, status :completed

        yang boleh di scan cancel
        a. department :inventory, status :onprogress
        b. department :sewing, status :rejected
    */
    public function setStatusCheckin(Request $request) {
        $type = 'checkin';
        $barcodeid = trim($request->barcodeid);
        $status = strtolower(trim($request->status));

        $cek_move = DB::table('package')->where('barcode_id',$barcodeid)->first();
        if(isset($cek_move) && $cek_move->is_move_to_fgms_new == true){
            return response()->json("Package sudah dipindah di FGMS BARU",422);

        }

        //allowed status
        if($status !== 'onprogress' && $status !== 'rejected' && $status !== 'cancel') {
            return response()->json('Status unidentified', 422);
        }

        //check if package can be scanned or not
        $check = $this->_check_scan($barcodeid, $status, $type);
        if(!$check) {
            $error = $this->_show_error($barcodeid);
            return response()->json($error,422);
            // $error = true;
            // if($status == 'cancel') {
            //     $suggestion = 'This package cannot be canceled';
            // }
            // else {
            //     $suggestion = 'This package cannot be scanned here';
            // }
            //
            // $po_number = DB::table('package')
            //              ->select('po_number')
            //              ->join('package_detail', 'package.scan_id', '=','package_detail.scan_id')
            //              ->where('package.barcode_id', $barcodeid)
            //              ->whereNull('package.deleted_at')
            //              ->whereNull('package_detail.deleted_at')
            //              ->first();
            //
            // if(!$po_number) {
            //     $po_number = '-';
            // }
            // else {
            //     $po_number = $po_number->po_number;
            // }
            // return view('inventory/checkin/ajax_inventory_list')->with('barcodeid', $barcodeid)
            //                                                     ->with('po_number', $po_number)
            //                                                     ->with('error', $error)
            //                                                     ->with('suggestion', $suggestion)
            //                                                     ->with('created_at', Carbon::now());
        }

        //prepare data for table package and package_movements
        if($status == 'onprogress' || $status == 'rejected') {

            if ($check->metal_status==true) {
                return response()->json('Carton on lock metal findding ! ! !', 422);
            }else{
                $department_from = $check->current_department;
                $status_from = $check->current_status;
                $status_to = $status;

                if($status_to == 'onprogress') {
                    $department_to = 'inventory';
                    $description = 'accepted on checkin inventory';
                }
                elseif($status_to == 'rejected') {
                    $department_to = 'sewing';
                    $description = 'rejected on checkin inventory';
                }

                //data package movement
                $data = array(
                    'barcode_package' => $barcodeid,
                    'department_from' => $department_from,
                    'department_to' => $department_to,
                    'status_from' => $status_from,
                    'status_to' => $status_to,
                    'user_id' => Auth::user()->id,
                    'description' => $description,
                    'created_at' => Carbon::now()
                );

                //rack suggestion
                $rack_suggestion = $this->getSuggestion($barcodeid, $check->po_number);
            }
            
        }
        elseif($status == 'cancel') {
            $department_to = 'sewing';
            $status_to = 'completed';

            //data for view
            $data = array(
                'barcode_package' => $barcodeid,
                'department_to' => null,
                'status_to' => 'cancel',
                'created_at' => Carbon::now()
            );

            //rack null array
            $rack_suggestion = array();
        }

        //do database action
        try {
            DB::beginTransaction();

            //query for table package_movements and package
            $query_movement = $this->_query_movement($barcodeid, $status, $type, $data);
            if(!$query_movement) {
                throw new \Exception('Movement error');
            }
            $query_package = $this->_query_package($barcodeid, $status, $type, $department_to, $status_to);
            if(!$query_package) {
                throw new \Exception('Package error');
            }

            //jika package berasal dari sewing completed, update persen_inventory pada table po_summary
            if(($check->current_department == 'sewing' && $check->current_status == 'completed')
                || ($check->current_department == 'inventory' && $check->current_status == 'onprogress')) {
                $count_package_inventory = DB::table('package')
                                               ->join('package_detail', 'package_detail.scan_id','=','package.scan_id')
                                               ->where('po_number', $check->po_number)
                                               ->where('plan_ref_number', $check->plan_ref_number)
                                               ->whereNotNull('status_inventory')
                                               ->whereNull('package.deleted_at')
                                               ->whereNull('package_detail.deleted_at')
                                               ->count();

                $count_total_package_in_po = DB::table('package')
                                               ->join('package_detail', 'package_detail.scan_id','=','package.scan_id')
                                               ->where('po_number', $check->po_number)
                                               ->where('plan_ref_number', $check->plan_ref_number)
                                               ->whereNull('package.deleted_at')
                                               ->whereNull('package_detail.deleted_at')
                                               ->count();

                $persen = ($count_package_inventory/$count_total_package_in_po) * 100;
                $persen = number_format($persen, 2);
                $update_persen_inventory = DB::table('po_summary')
                                               ->where('po_number', $check->po_number)
                                               ->where('plan_ref_number', $check->plan_ref_number)
                                               ->whereNull('deleted_at')
                                               ->update([
                                                   'persen_inventory' => $persen
                                               ]);
            }

            DB::commit();
        }
        catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return view('inventory/checkin/ajax_inventory_list')->with('data', $data)
                                                            ->with('rack_suggestion', $rack_suggestion)
                                                            ->with('po_number', $check->po_number)
                                                            ->with('plan_ref_number', $check->plan_ref_number);
    }

    /* placement*
    Constraint :
        yang boleh di scan placement
        a. department :inventory, status :onprogress
        b. department :inventory, status :rejected
        c. department :inventory, status :completed
    */
    public function checkout(Request $request) {
        $type = 'checkout';
        $rack = $request->rack;
        $barcode_locator = $request->barcode_rack;
        $barcodeid = trim($request->barcodeid);
        $status = strtolower(trim($request->status));
        $radio_status = strtolower(trim($request->radio_status));

        $cek_move = DB::table('package')->where('barcode_id',$barcodeid)->first();
        if(isset($cek_move) && $cek_move->is_move_to_fgms_new == true){
            return response()->json("Package sudah dipindah di FGMS BARU",422);

        }

        //allowed status
        if($status !== 'completed') {
            return response()->json('Status unidentified', 422);
        }

        //check if package can be scanned or not
        $check = $this->_check_scan($barcodeid, $status, $type, $barcode_locator, $radio_status);
        if(!$check) {
            $error = $this->_show_error($barcodeid);
            return response()->json($error,422);
        }

        //prepare data for table package and package_movements
        if ($radio_status == 'onprogress') {
            $department_from = $check->current_department;
            $status_from = $check->current_status;
            $status_to = $status;
            $department_to = 'inventory';
            if($check->current_status == 'onprogress' || $check->current_status == 'rejected') {
                $description = 'placed to rack';
            }
            elseif($check->current_status == 'completed') {
                $description = 'moving from rack to rack';
            }
        }elseif ($radio_status == 'sendsewing') {
            $barcode_locator = '';
            $department_from = $check->current_department;
            $status_from = $check->current_status;
            $status_to = $status;
            $department_to = 'preparation';
            $description = 'back to sewing';
        }

        //data package movement
        $data_move = array(
            'barcode_package' => $barcodeid,
            'barcode_locator' => $barcode_locator,
            'department_from' => $department_from,
            'department_to' => $department_to,
            'status_from' => $status_from,
            'status_to' => $status_to,
            'user_id' => Auth::user()->id,
            'description' => $description,
            'created_at' => Carbon::now()
        );

        //rack suggestion
        $rack_suggestion = $this->getSuggestion($barcodeid, $check->po_number);

        //do database action
        try {
            DB::beginTransaction();

            //query for table package_movements and package
            $query_movement = $this->_query_movement($barcodeid, $status, $type, $data_move, $radio_status);
            if(!$query_movement) {
                throw new \Exception('Movement error');
            }
            $query_package = $this->_query_package($barcodeid, $status, $type, $department_to, $status_to, $radio_status);
            if(!$query_package) {
                throw new \Exception('Package error');
            }

            DB::commit();
        }
        catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return view('inventory/placement/ajax_inventory_list')
                ->with('data', $check)
                ->with('movement', $data_move)
                ->with('rack', $rack)
                ->with('rack_suggestion', $rack_suggestion);
    }

    // check stock
    public function checkstock(Request $request) {
        $type = 'checkout';
        $rack = $request->rack;
        $barcode_locator = $request->barcode_rack;
        $barcodeid = trim($request->barcodeid);
        $status = strtolower(trim($request->status));
        $radio_status = strtolower(trim($request->radio_status));

        $cek_move = DB::table('package')->where('barcode_id',$barcodeid)->first();
        if(isset($cek_move) && $cek_move->is_move_to_fgms_new == true){
            return response()->json("Package sudah dipindah di FGMS BARU",422);

        }

        //allowed status
        if($status !== 'completed') {
            return response()->json('Status unidentified', 422);
        }

        //check if package can be scanned or not
        // $check = $this->_check_scan($barcodeid, $status, $type, $barcode_locator, $radio_status);
        // if(!$check) {
            $check = $this->_show_placement($barcodeid, $barcode_locator);
            return response()->json($check,422);
        // }

        //prepare data for table package and package_movements
        // if ($radio_status == 'onprogress') {
        //     $department_from = $check->current_department;
        //     $status_from = $check->current_status;
        //     $status_to = $status;
        //     $department_to = 'inventory';
        //     if($check->current_status == 'onprogress' || $check->current_status == 'rejected') {
        //         $description = 'placed to rack';
        //     }
        //     elseif($check->current_status == 'completed') {
        //         $description = 'moving from rack to rack';
        //     }
        // }elseif ($radio_status == 'sendsewing') {
        //     $barcode_locator = '';
        //     $department_from = $check->current_department;
        //     $status_from = $check->current_status;
        //     $status_to = $status;
        //     $department_to = 'preparation';
        //     $description = 'back to sewing';
        // }

        // //data package movement
        // $data_move = array(
        //     'barcode_package' => $barcodeid,
        //     'barcode_locator' => $barcode_locator,
        //     'department_from' => $department_from,
        //     'department_to' => $department_to,
        //     'status_from' => $status_from,
        //     'status_to' => $status_to,
        //     'user_id' => Auth::user()->id,
        //     'description' => $description,
        //     'created_at' => Carbon::now()
        // );

        // //rack suggestion
        // $rack_suggestion = $this->getSuggestion($barcodeid, $check->po_number);

        // //do database action
        // try {
        //     DB::beginTransaction();

        //     //query for table package_movements and package
        //     $query_movement = $this->_query_movement($barcodeid, $status, $type, $data_move, $radio_status);
        //     if(!$query_movement) {
        //         throw new \Exception('Movement error');
        //     }
        //     $query_package = $this->_query_package($barcodeid, $status, $type, $department_to, $status_to, $radio_status);
        //     if(!$query_package) {
        //         throw new \Exception('Package error');
        //     }

        //     DB::commit();
        // }
        // catch (Exception $ex) {
        //     db::rollback();
        //     $message = $ex->getMessage();
        //     ErrorHandler::db($message);
        // }

        return view('inventory/placement/ajax_inventory_list')
                ->with('data', $check)
                ->with('movement', $data_move)
                ->with('rack', $rack)
                ->with('rack_suggestion', $rack_suggestion);
    }

    //get suggestion rack based on other package with same ponumber
    public function getSuggestion($barcodeid, $ponumber) {
        $locators = DB::table('package')
                        ->select('barcode_locator', 'locators.code')
                        ->join('package_movements', 'package.barcode_id', '=', 'package_movements.barcode_package')
                        ->join('package_detail', 'package.scan_id', '=','package_detail.scan_id')
                        ->join('locators', 'package_movements.barcode_locator', '=', 'locators.barcode')
                        ->where('package_detail.po_number', $ponumber)
                        ->where('package_movements.barcode_package', '!=', $barcodeid)
                        ->where('package_movements.is_canceled', false)
                        ->whereNull('package.deleted_at')
                        ->whereNull('package_movements.deleted_at')
                        ->whereNull('package_detail.deleted_at')
                        ->whereNotNull('package_movements.barcode_locator')
                        ->groupBy('barcode_locator', 'locators.code')
                        ->get();

        return $locators;
    }

    //get locator
    public function getLocator(Request $request) {
        $barcode = $request->barcodeid;
        $locators = DB::table('locators')
                    ->select('code')
                    ->where('barcode', $barcode)
                    ->whereNull('deleted_at')
                    ->first();

        if(!$locators) {
            return response()->json('RACK not found.', 422);
        }

        return response()->json($locators->code,200);
    }

    //check if package can be scanned or not
    private function _check_scan($barcodeid, $check_status, $type, $barcode_rack=null, $radio_status='onprogress') {
        if($type == 'checkin') {
            if($check_status !== 'onprogress' && $check_status !== 'rejected' && $check_status !== 'cancel') {
                return false;
            }
        }
        elseif($type == 'checkout') {
            if($check_status !== 'completed') {
                return false;
            }
        }

        //proses check
        if($check_status == 'onprogress' || $check_status == 'rejected') {
            if($check_status == 'onprogress') {
                $check = DB::table('package')
                         ->select('current_department', 'current_status', 'po_number', 'plan_ref_number','metal_status')
                         ->join('package_detail', 'package.scan_id', '=','package_detail.scan_id')
                         ->where('package.barcode_id', $barcodeid)
                         ->where(function($query) {
                             $query->where(function($query) {
                                 $query->where('current_department', 'sewing')
                                       ->where('current_status', 'completed');
                             })
                             ->orWhere(function($query) {
                                 $query->where('current_department', 'qcinspect')
                                       ->where('current_status', 'completed');
                             })
                             ->orWhere(function($query) {
                                 $query->where('current_department', 'shipping')
                                       ->where('current_status', 'onprogress');
                             });
                         })
                         ->whereNull('package.deleted_at')
                         ->whereNull('package_detail.deleted_at')
                         ->first();
            }
            else {
                $check = DB::table('package')
                         ->select('current_department', 'current_status', 'po_number', 'plan_ref_number')
                         ->join('package_detail', 'package.scan_id', '=','package_detail.scan_id')
                         ->where('barcode_id', $barcodeid)
                         ->where('current_department', 'sewing')
                         ->where('current_status', 'completed')
                         ->whereNull('package.deleted_at')
                         ->whereNull('package_detail.deleted_at')
                         ->first();
            }
            if(!$check) {
                return false;
            }
            else {
                //khusus qcinspect completed, dicek lagi apakah qcinspect sudah completed all
                if($check->current_department == 'qcinspect' && $check->current_status == 'completed') {
                    $checkdata = DB::table('po_summary')
                                     ->select('persen_qcinspect')
                                     ->where('po_number', $check->po_number)
                                     ->where('plan_ref_number', $check->plan_ref_number)
                                     ->whereNull('po_summary.deleted_at')
                                     ->first();
                    if($checkdata->persen_qcinspect < 100) {
                        return false;
                    }
                }
                return $check;
            }
        }
        elseif($check_status == 'completed') {
            if ($radio_status == 'sendsewing') {
                $check = DB::table('package')
                         ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                         ->leftJoin('package_movements', 'package.barcode_id', '=', 'package_movements.barcode_package')
                         ->where('barcode_id', $barcodeid)
                         ->where('current_department', 'inventory')
                         // ->where(function($query) use ($barcode_rack) {
                         //     $query->where('barcode_locator', '<>', $barcode_rack)
                         //           ->orWhereNull('barcode_locator');
                         // })
                         ->whereNull('package.deleted_at')
                         ->whereNull('package_detail.deleted_at')
                         ->whereNull('package_movements.deleted_at')
                         ->first();
            }else{


                $check = DB::table('package')
                         ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                         ->leftJoin('package_movements', 'package.barcode_id', '=', 'package_movements.barcode_package')
                         ->where('barcode_id', $barcodeid)
                         ->where('current_department', 'inventory')
                         ->where(function($query) use ($barcode_rack) {
                             $query->where('barcode_locator', '<>', $barcode_rack)
                                   ->orWhereNull('barcode_locator');
                         })
                         ->whereNull('package.deleted_at')
                         ->whereNull('package_detail.deleted_at')
                         ->whereNull('package_movements.deleted_at')
                         ->first();

            }

            if(!$check) {
                return false;
            }
            else {
                return $check;
            }
        }
        elseif($check_status == 'cancel') {
            $check = DB::table('package')
                     ->select('current_department', 'current_status', 'po_number', 'plan_ref_number')
                     ->join('package_detail', 'package.scan_id', '=','package_detail.scan_id')
                     ->where('barcode_id', $barcodeid)
                     ->where(function($query) {
                         $query->where(function($query) {
                             $query->where('current_department', 'sewing')
                                   ->where('current_status', 'rejected');
                         })
                         ->orWhere(function($query) {
                             $query->where('current_department', 'inventory')
                                   ->where('current_status', 'onprogress');
                         });

                     })
                     ->whereNull('package.deleted_at')
                     ->whereNull('package_detail.deleted_at')
                     ->first();

            if(!$check) {
                return false;
            }
            else {
                return $check;
            }
        }
        else {
            return false;
        }
    }

    //show error
    private function _show_error($barcodeid) {
        $error = '';
        $check_current_status = $this->_get_current_status($barcodeid);
        if($check_current_status) {
            if($check_current_status->current_status == 'onprogress') {
                $check_current_status->current_status = 'in';
            }
            elseif($check_current_status->current_status == 'completed') {
                $check_current_status->current_status = 'out';
            }
            $error .= 'Last Scanned (' . $check_current_status->barcode_id . ') : '
                        . $check_current_status->current_department . ' - '
                        . $check_current_status->current_status;
        }
        else {
            $error = 'Barcode not found on system';
        }

        return $error;
    }

    //show check placement
    private function _show_placement($barcodeid, $barcode_locator) {
        $error = '';
        $check_current_status = $this->_get_current_status($barcodeid);
        if($check_current_status) {
            if($check_current_status->current_status == 'onprogress') {
                $check_current_status->current_status = 'in';
            }
            elseif($check_current_status->current_status == 'completed') {
                $check_current_status->current_status = 'out';
            }

            if ($check_current_status->current_department == 'inventory' && $check_current_status->current_status == 'out')
            {
                $data = DB::table('package_movements')
                    ->select('package_movements.barcode_package','package_movements.barcode_locator', 'locators.code')
                    ->join('locators', 'locators.barcode', '=', 'package_movements.barcode_locator')
                    ->where('package_movements.barcode_package', $barcodeid)
                    ->where('package_movements.department_to', 'inventory')
                    ->where('package_movements.status_to', 'completed')
                    ->where('package_movements.is_canceled', false)
                    ->first();

                if ($data->barcode_locator == $barcode_locator) {
                    $error .= 'Placement is TRUE';
                }else{
                    $error .= 'Placement is FALSE ( '.$data->code.' )';
                }
            }else{

                $error .= 'Last Scanned (' . $check_current_status->barcode_id . ') : '
                        . $check_current_status->current_department . ' - '
                        . $check_current_status->current_status;
            }
        }
        else {
            $error = 'Barcode not found on system';
        }

        return $error;
    }

    //get current status
    private function _get_current_status($barcodeid) {
        $data = DB::table('package')
                    ->select('barcode_id','current_department', 'current_status')
                    ->where('barcode_id', $barcodeid)
                    ->whereNull('package.deleted_at')
                    ->first();
        return $data;
    }

    //query movement
    private function _query_movement($barcodeid, $check_status, $type, $data_movement = null, $radio_status='onprogress') {

        if($check_status == 'onprogress' || $check_status == 'rejected' || $check_status == 'completed') {

            try {
                DB::beginTransaction();

                //update deleted_at on the last package movement
                /*$last_data = DB::table('package_movements')
                            ->where('barcode_package', $barcodeid)
                            ->where('is_canceled', false)
                            ->whereNull('deleted_at')
                            ->first();
                if(!$last_data) {
                    throw new \Exception('Package not found');
                }
                else {
                    $update_m =  DB::table('package_movements')
                                ->where('id', $last_data->id)
                                ->update([
                                    'deleted_at' => Carbon::now()
                                ]);

                    if ($update_m == 0) {
                        throw new \Exception('the last package nothing updated');
                    }
                }*/

                // tes update deleted at baru
                //update deleted_at on the last package movement
                $last_data = DB::table('package_movements')
                            ->where('barcode_package', $barcodeid)
                            ->where('is_canceled', false)
                            ->whereNull('deleted_at')
                            ->get()
                            ->toArray();
                foreach ($last_data as $key => $value) {
                    if(!$value) {
                        throw new \Exception('Package not found');
                    }
                    else {
                        $update_m =  DB::table('package_movements')
                                    ->where('id', $value->id)
                                    ->update([
                                        'deleted_at' => Carbon::now()
                                    ]);

                        if ($update_m == 0) {
                            throw new \Exception('the last package nothing updated');
                        }
                    }
                }

                //insert movement history
                $insert = DB::table('package_movements')->insert($data_movement);

                if ($insert) {
                    // cek apakah insert double ?
                    $cek_insert = DB::table('package_movements')
                                    ->where('barcode_package', $barcodeid)
                                    ->where('is_canceled', false)
                                    ->whereNull('deleted_at')
                                    ->count();

                    if ($cek_insert > 1) {
                        throw new \Exception('data double inserted');
                    }
                }

                //
                if ($radio_status == 'sendsewing') {

                    DB::table('package_movements')
                    ->where('barcode_package', $barcodeid)
                    // ->where('status_to', '<>', 'onprogress')
                    ->where('is_canceled', false)
                    ->whereNotNull('department_from')
                    ->whereNotNull('deleted_at')
                    ->update([
                        'deleted_at' => Carbon::now(),
                        'is_canceled' => true
                    ]);

                    // update status in db sewing
                    DB::connection('line')->table('folding_package_barcode')
                                        ->where('barcode_id', $barcodeid)
                                        ->update([
                                                'status' => 'back to sewing',
                                                'complete_at' => null,
                                                'complete_by' => null,
                                                'backto_sewing' => true
                                        ]);

                }
                // else{


                // }

                DB::commit();
            }
            catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return true;
        }
        elseif($check_status == 'cancel') {
            try {
                DB::beginTransaction();

                /*
                    - get second last data from table package_movements
                    - get last data from table package_movements
                    - update second last data, deleted_at = null
                    - update last data, deleted_at = now() and is_canceled = true
                */
                $second_last_data = DB::table('package_movements')
                                        ->where('barcode_package', $barcodeid)
                                        ->where('is_canceled', false)
                                        ->whereNotNull('deleted_at')
                                        ->orderBy('created_at', 'desc')
                                        ->first();

                $last_data = DB::table('package_movements')
                                 ->where('barcode_package', $barcodeid)
                                 ->where('is_canceled', false)
                                 ->whereNull('deleted_at')
                                 ->first();

                $ceklsd = DB::table('package_movements')
                    ->where('id', $second_last_data->id)
                    ->update([
                        'deleted_at' => null
                    ]);

                $cekld = DB::table('package_movements')
                    ->where('id', $last_data->id)
                    ->update([
                        'is_canceled' => true,
                        'deleted_at' => Carbon::now()
                    ]);

                if ($ceklsd && $cekld) {
                    $countpm = DB::table('package_movements')
                                ->where('barcode_package',$barcodeid)
                                ->whereNull('deleted_at')
                                ->where('is_canceled',false)
                                ->count();
                    if ($countpm != 1) {
                        throw new \Exception('data inserted error');
                    }
                }

                DB::commit();
            }
            catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return true;
        }
        else {
            return false;
        }
    }

    //query package
    private function _query_package($barcodeid, $check_status, $type, $department, $status, $radio_status='onprogress') {

        if($type == 'checkin') {
            /*
                jika status onprogress, update column 'status_inventory' = onprogress
                jika status rejected, update column 'status_sewing' = rejected,
                jika status cancel, update column 'status_sewing' = completed && 'status_inventory' = null
            */
            if($check_status == 'onprogress') {
                $update_clause = array(
                    'current_department' => $department,
                    'current_status' => $status,
                    'status_inventory' => 'onprogress',
                    'updated_at' => Carbon::now()
                );
            }
            elseif($check_status == 'rejected') {
                $update_clause = array(
                    'current_department' => $department,
                    'current_status' => $status,
                    'status_sewing' => 'rejected',
                    'updated_at' => Carbon::now()
                );
            }
            elseif($check_status == 'cancel') {
                $update_clause = array(
                    'current_department' => $department,
                    'current_status' => $status,
                    'status_inventory' => null,
                    'status_sewing' => 'completed',
                    'updated_at' => Carbon::now()
                );
            }
            else {
                return false;
            }
        }
        elseif($type == 'checkout') {
            /*
                update column 'status_inventory' = completed
            */
           if ($radio_status == 'sendsewing') {

               $update_clause = array(
                        'current_department' => 'preparation',
                        'current_status' => $status,
                        'status_inventory' => null,
                        'status_sewing' => null,
                        'status_qcinspect' => null,
                        'updated_at' => Carbon::now()
                    );
           }else{

               if ($department != 'preparation') {
                    $update_clause = array(
                        'current_department' => $department,
                        'current_status' => $status,
                        'status_inventory' => 'completed',
                        'updated_at' => Carbon::now()
                    );
               }else{
                    $update_clause = array(
                        'current_department' => $department,
                        'current_status' => $status,
                        'status_inventory' => null,
                        'status_sewing' => null,
                        'status_qcinspect' => null,
                        'updated_at' => Carbon::now()
                    );
               }
           }

        }
        else {
            return false;
        }

        try {
            DB::beginTransaction();

            DB::table('package')
                ->where('barcode_id', $barcodeid)
                ->whereNull('deleted_at')
                ->update($update_clause);

            DB::commit();
        }
        catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }

    //get area
    public function getArea() {
        $areas = DB::table('areas')
                 ->where('factory_id', Auth::user()->factory_id)
                 ->whereNull('areas.deleted_at')
                 ->get();

        return view('inventory/placement/listbox_area')->with('areas', $areas);
    }

    //get locator by area
    public function getLocatorByArea(Request $request) {
        $areaid = $request->areaid;
        $locators = DB::table('locators')
                    ->where('area_id', $areaid)
                    ->whereNull('deleted_at')
                    ->get();

        return view('inventory/placement/list_locators')->with('locators', $locators);
    }
}
