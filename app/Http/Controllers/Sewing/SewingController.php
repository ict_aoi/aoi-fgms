<?php

namespace App\Http\Controllers\Sewing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;
use App\Line;

class SewingController extends Controller
{
    // halaman checkin
    public function index()
    {
        // $validate_ip = $this->validateIp();
        //
        // if(!$validate_ip) {
        //     return response()->json(['error' => 'Not authorized.'],403);
        // }

        return view('sewing/checkin/index');
    }

    // halaman checkout
    public function checkout() {
        // $validate_ip = $this->validateIp();
        //
        // if(!$validate_ip) {
        //     return response()->json(['error' => 'Not authorized.'],403);
        // }
        return view('sewing/checkout/index');
    }

    static function validateIp() {
        $roles = Auth::user()->roles[0]['name'];
        $ip = \Request::ip();

        if($roles != 'admin' && $roles != 'Admin') {
            //check ip - line
            $check = DB::table('line')
                        ->where('ip_address', $ip)
                        ->whereNull('deleted_at')
                        ->first();
            if(!$check) {
                return false;
            }
        }
        return true;
    }

    /*
    Constraint :
        yg boleh di scan onprogress
        a. department :preparation, status :completed

        yg boleh di scan rejected
        a. department :preparation, status :completed

        yg boleh di cancel
        a. department :sewing, status :onprogress
        b. department :preparation, status :rejected
    */
    public function setStatusCheckin(Request $request) {
        $type = 'checkin';
        $barcodeid = trim($request->barcodeid);
        $lineid = $request->lineid;
        $linename = $request->linename;
        $status = strtolower(trim($request->status));

        $cek_move = DB::table('package')->where('barcode_id',$barcodeid)->first();
        if(isset($cek_move) && $cek_move->is_move_to_fgms_new == true){
            return response()->json("Package sudah dipindah di FGMS BARU",422);

        }

        //allowed status
        if($status !== 'onprogress' && $status !== 'rejected' && $status !== 'cancel') {
            return response()->json('Status unidentified', 422);
        }

        //check if package can be scanned or not
        $check = $this->_check_scan($barcodeid, $status, $type);
        if(!$check) {
            $error = $this->_show_error($barcodeid);
            return response()->json($error,422);
        }

        //prepare data for table package and package_movements
        if($status == 'onprogress' || $status == 'rejected') {

            $department_from = $check->current_department;
            $status_from = $check->current_status;
            $status_to = $status;

            if($status_to == 'onprogress') {
                $department_to = 'sewing';
                $description = 'accepted on checkin sewing (by packing)';
            }
            elseif($status_to == 'rejected') {
                $department_to = 'preparation';
                $description = 'rejected on checkin sewing (by packing)';
            }

            //data package movement
            $data = array(
                'barcode_package' => $barcodeid,
                'line_id' => $lineid,
                'department_from' => $department_from,
                'department_to' => $department_to,
                'status_from' => $status_from,
                'status_to' => $status_to,
                'user_id' => Auth::user()->id,
                'description' => $description,
                'created_at' => Carbon::now(),
                'ip_address'=> $request->ip()
            );
        }
        elseif($status == 'cancel') {
            $department_to = 'preparation';
            $status_to = 'completed';
            //data for view
            $data = array(
                'barcode_package' => $barcodeid,
                'department_to' => null,
                'status_to' => 'cancel',
                'created_at' => Carbon::now(),
                'ip_address'=> $request->ip()
            );
        }

        //do database action
        try {
            DB::beginTransaction();

            //query for table package_movements and package
            $query_movement = $this->_query_movement($barcodeid, $status, $type, $data);
            if(!$query_movement) {
                throw new \Exception('Movement error');
            }
            $query_package = $this->_query_package($barcodeid, $status, $type, $department_to, $status_to);
            if(!$query_package) {
                throw new \Exception('Package error');
            }

            DB::commit();
        }
        catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return view('sewing/checkin/ajax_sewing_list')
                ->with('data', $data)
                ->with('linename', $linename)
                ->with('po_number', $check->po_number)
                ->with('plan_ref_number', $check->plan_ref_number);
    }

    /*
    Constraint :
        yg boleh di scan completed
        a. department :sewing, status :onprogress
        b. department :sewing, status :rejected

        yg boleh di scan cancel
        a. department :sewing, status :completed
    */
    public function setStatusCheckOut(Request $request) {
        $type = 'checkout';
        $barcodeid = trim($request->barcodeid);
        $status = strtolower(trim($request->status));

        $cek_move = DB::table('package')->where('barcode_id',$barcodeid)->first();
        if(isset($cek_move) && $cek_move->is_move_to_fgms_new == true){
            return response()->json("Package sudah dipindah di FGMS BARU",422);

        }

        //allowed status
        if($status !== 'completed' && $status !== 'cancel') {
            return response()->json('Status unidentified', 422);
        }

        //check if package can be scanned or not
        $check = $this->_check_scan($barcodeid, $status, $type);
        if(!$check) {
            $error = $this->_show_error($barcodeid);
            return response()->json($error,422);
        }

        //prepare data for table package and package_movements
        if($status == 'completed') {

            $department_from = $check->current_department;
            $status_from = $check->current_status;
            $status_to = $status;
            $department_to = 'sewing';
            $description = 'set as completed on sewing (by packing)';

            //data package movement
            $data = array(
                'barcode_package' => $barcodeid,
                'line_id' => $check->line_id,
                'department_from' => $department_from,
                'department_to' => $department_to,
                'status_from' => $status_from,
                'status_to' => $status_to,
                'user_id' => Auth::user()->id,
                'description' => $description,
                'created_at' => Carbon::now(),
                'ip_address'=> $request->ip()
            );
        }
        elseif($status == 'cancel') {
            $department_to = 'sewing';
            $status_to = 'onprogress';
            //data for view
            $data = array(
                'barcode_package' => $barcodeid,
                'department_to' => null,
                'status_to' => 'cancel',
                'created_at' => Carbon::now(),
                'ip_address'=> $request->ip()
            );
        }

        //do database action
        try {
            DB::beginTransaction();

            //query for table package_movements and package
            $query_movement = $this->_query_movement($barcodeid, $status, $type, $data);
            if(!$query_movement) {
                throw new \Exception('Movement error');
            }
            $query_package = $this->_query_package($barcodeid, $status, $type, $department_to, $status_to);
            if(!$query_package) {
                throw new \Exception('Package error');
            }

            DB::commit();
        }
        catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return view('sewing/checkout/ajax_sewing_list')
                ->with('data', $data)
                ->with('linename',$check->name)
                ->with('po_number', $check->po_number)
                ->with('plan_ref_number', $check->plan_ref_number);
    }

    //check if package can be scanned or not
    private function _check_scan($barcodeid, $check_status, $type) {
        if($type == 'checkin') {
            if($check_status !== 'onprogress' && $check_status !== 'rejected' && $check_status !== 'cancel') {
                return false;
            }
        }
        elseif($type == 'checkout') {
            if($check_status !== 'completed' && $check_status !== 'cancel') {
                return false;
            }
        }

        //proses check
        if($check_status == 'onprogress' || $check_status == 'rejected') {
            $check = DB::table('package')
                     ->select('current_department', 'current_status', 'po_number', 'plan_ref_number')
                     ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                     ->where('barcode_id', $barcodeid)
                     ->where('package_detail.factory_id', Auth::user()->factory_id)
                     ->where('current_department', 'preparation')
                     ->where('current_status', 'completed')
                     ->whereNull('package.deleted_at')
                     ->whereNull('package_detail.deleted_at')
                     ->first();

            if(!$check) {
                return false;
            }
            else {
                return $check;
            }
        }
        elseif($check_status == 'completed') {
            $check = DB::table('package')
                     ->select('current_department', 'current_status', 'po_number', 'line_id', 'name', 'plan_ref_number')
                     ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                     ->leftJoin('package_movements','package.barcode_id','=','package_movements.barcode_package')
                     ->leftJoin('line','package_movements.line_id','=','line.id')
                     ->where('barcode_id', $barcodeid)
                     ->where('package_detail.factory_id', Auth::user()->factory_id)
                     ->where('current_department', 'sewing')
                     ->where(function($query) {
                         $query->where('current_status', 'onprogress')
                               ->orWhere('current_status', 'rejected');
                     })
                     ->where('package_movements.is_canceled', false)
                     ->whereNull('package.deleted_at')
                     ->whereNull('package_detail.deleted_at')
                     ->whereNull('package_movements.deleted_at')
                     ->whereNull('line.deleted_at')
                     ->first();

            if(!$check) {
                return false;
            }
            else {
                return $check;
            }
        }
        elseif($check_status == 'cancel') {
            if($type == 'checkin') {
                $check = DB::table('package')
                         ->select('current_department', 'current_status', 'po_number', 'plan_ref_number')
                         ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                         ->where('barcode_id', $barcodeid)
                         ->where('package_detail.factory_id', Auth::user()->factory_id)
                         ->where(function($query) {
                             $query->where(function($query) {
                                 $query->where('current_department', 'preparation')
                                       ->where('current_status', 'rejected');
                             })
                             ->orWhere(function($query) {
                                 $query->where('current_department', 'sewing')
                                       ->where('current_status', 'onprogress');
                             });
                         })
                         ->whereNull('package.deleted_at')
                         ->whereNull('package_detail.deleted_at')
                         ->first();
            }
            elseif($type == 'checkout') {
                $check = DB::table('package')
                         ->select('current_department', 'current_status', 'po_number', 'line_id', 'name', 'plan_ref_number')
                         ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                         ->leftJoin('package_movements','package.barcode_id','=','package_movements.barcode_package')
                         ->leftJoin('line','package_movements.line_id','=','line.id')
                         ->where('barcode_id', $barcodeid)
                         ->where('package_detail.factory_id', Auth::user()->factory_id)
                         ->where('current_department', 'sewing')
                         ->where('current_status', 'completed')
                         ->where('package_movements.is_canceled', false)
                         ->whereNull('package.deleted_at')
                         ->whereNull('package_detail.deleted_at')
                         ->whereNull('package_movements.deleted_at')
                         ->whereNull('line.deleted_at')
                         ->first();
            }
            else {
                return false;
            }

            if(!$check) {
                return false;
            }
            else {
                return $check;
            }
        }
        else {
            return false;
        }
    }

    //show error
    private function _show_error($barcodeid) {
        $error = '';
        $check_current_status = $this->_get_current_status($barcodeid);
        if($check_current_status) {
            if($check_current_status->current_status == 'onprogress') {
                $check_current_status->current_status = 'in';
            }
            elseif($check_current_status->current_status == 'completed') {
                $check_current_status->current_status = 'out';
            }
            $error .= 'Last Scanned (' . $check_current_status->barcode_id . ') : '
                        . $check_current_status->current_department . ' - '
                        . $check_current_status->current_status;
        }
        else {
            $error = 'Barcode not found on system';
        }

        return $error;
    }

    //get current status
    private function _get_current_status($barcodeid) {
        $data = DB::table('package')
                    ->select('barcode_id','current_department', 'current_status')
                    ->where('barcode_id', $barcodeid)
                    ->whereNull('package.deleted_at')
                    ->first();
        return $data;
    }

    //query movement
    private function _query_movement($barcodeid, $check_status, $type, $data_movement = null) {

        if($check_status == 'onprogress' || $check_status == 'rejected' || $check_status == 'completed') {

            try {
                DB::beginTransaction();

                //update deleted_at on the last package movement
                /*$last_data = DB::table('package_movements')
                            ->where('barcode_package', $barcodeid)
                            ->where('is_canceled', false)
                            ->whereNull('deleted_at')
                            ->first();
                if(!$last_data) {
                    throw new \Exception('Package not found');
                }
                else {
                    DB::table('package_movements')
                    ->where('id', $last_data->id)
                    ->update([
                        'deleted_at' => Carbon::now()
                    ]);
                }*/

                //update deleted_at on the last package movement
                $last_data = DB::table('package_movements')
                            ->where('barcode_package', $barcodeid)
                            ->where('is_canceled', false)
                            ->whereNull('deleted_at')
                            ->get()
                            ->toArray();
                foreach ($last_data as $key => $value) {
                    if(!$value) {
                        throw new \Exception('Package not found');
                    }
                    else {
                        $update_m =  DB::table('package_movements')
                                    ->where('id', $value->id)
                                    ->update([
                                        'deleted_at' => Carbon::now()
                                    ]);

                        if ($update_m == 0) {
                            throw new \Exception('the last package nothing updated');
                        }
                    }
                }

                //insert movement history
                $insert = DB::table('package_movements')->insert($data_movement);

                if ($insert) {
                    // cek apakah insert double ?
                    $cek_insert = DB::table('package_movements')
                                    ->where('barcode_package', $barcodeid)
                                    ->where('is_canceled', false)
                                    ->whereNull('deleted_at')
                                    ->count();

                    if ($cek_insert > 1) {
                        throw new \Exception('data double inserted');
                    }
                }

                DB::commit();
            }
            catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return true;
        }
        elseif($check_status == 'cancel') {
            try {
                DB::beginTransaction();

                /*
                    - get second last data from table package_movements
                    - get last data from table package_movements
                    - update second last data, deleted_at = null
                    - update last data, deleted_at = now() and is_canceled = true
                */
                $second_last_data = DB::table('package_movements')
                                        ->where('barcode_package', $barcodeid)
                                        ->where('is_canceled', false)
                                        ->whereNotNull('deleted_at')
                                        ->orderBy('created_at', 'desc')
                                        ->first();

                $last_data = DB::table('package_movements')
                                 ->where('barcode_package', $barcodeid)
                                 ->where('is_canceled', false)
                                 ->whereNull('deleted_at')
                                 ->first();

                DB::table('package_movements')
                    ->where('id', $second_last_data->id)
                    ->update([
                        'deleted_at' => null
                    ]);

                DB::table('package_movements')
                    ->where('id', $last_data->id)
                    ->update([
                        'is_canceled' => true,
                        'deleted_at' => Carbon::now()
                    ]);

                DB::commit();
            }
            catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return true;
        }
        else {
            return false;
        }
    }

    //query package
    private function _query_package($barcodeid, $check_status, $type, $department, $status) {

        if($type == 'checkin') {
            /*
                jika status onprogress, update column 'status_sewing' = onprogress
                jika status rejected, update column 'status_preparation' = rejected,'is_printed' = false
                jika status cancel, update column 'status_preparation' = completed && 'status_sewing' = null,'is_printed' = true
            */
            if($check_status == 'onprogress') {
                $update_clause = array(
                    'current_department' => $department,
                    'current_status' => $status,
                    'status_sewing' => 'onprogress',
                    'updated_at' => Carbon::now()
                );
            }
            elseif($check_status == 'rejected') {
                $update_clause = array(
                    'current_department' => $department,
                    'current_status' => $status,
                    'status_preparation' => 'rejected',
                    'is_printed' => false,
                    'updated_at' => Carbon::now()
                );
            }
            elseif($check_status == 'cancel') {
                $update_clause = array(
                    'current_department' => $department,
                    'current_status' => $status,
                    'status_sewing' => null,
                    'status_preparation' => 'completed',
                    'is_printed' => true,
                    'updated_at' => Carbon::now()
                );
            }
            else {
                return false;
            }
        }
        elseif($type == 'checkout') {
            /*
                jika status completed, update column 'status_sewing' = completed
                jika status cancel, update column 'status_sewing' = onprogress
            */
            if($check_status == 'completed') {
                $update_clause = array(
                    'current_department' => $department,
                    'current_status' => $status,
                    'status_sewing' => 'completed',
                    'updated_at' => Carbon::now()
                );
            }
            elseif($check_status == 'cancel') {
                $update_clause = array(
                    'current_department' => $department,
                    'current_status' => $status,
                    'status_sewing' => 'onprogress',
                    'updated_at' => Carbon::now()
                );
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }

        try {
            DB::beginTransaction();

            DB::table('package')
                ->where('barcode_id', $barcodeid)
                ->whereNull('deleted_at')
                ->update($update_clause);

            DB::commit();
        }
        catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }

    //get line
    public function ajaxGetLine(){
        // implementasi folding line 1 - 22 AOI 2
        $data = DB::table('line')
                    ->where('factory_id', Auth::user()->factory_id)
                    // ->where('is_hidden', false)
                    ->whereNull('deleted_at')
                    ->get();
        return view('sewing/checkin/list_line')->with('data', $data);
    }


    //MASTER SEWING
    public function index2()
    {
        return view('sewing/master/index');
    }

    public function ajaxGetData(Request $request)
    {
        if ($request->ajax()){
            $sewing = DB::table('line')
                ->select('id', 'name', 'description', 'prod_max_capacity', 'number_employees', 'created_at','updated_at', 'ip_address')
                ->where('factory_id', Auth::user()->factory_id)
                ->wherenull('deleted_at')
                ->orderBy('created_at', 'desc')
                ->get();

            //datatables

            return DataTables::of($sewing)
                ->addColumn('action', function($sewing){ return view('_action', [
                        'edit' => route('sewing.edit', $sewing->id),
                        'sewingid' => $sewing->id,
                        'delete_sewing' => route('sewing.delete', $sewing->id)
                    ]);
                })
            ->make(true);
        }
    }

    //add new sewing-master
    public function addsewing(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'prod_max_capacity' => 'required|numeric',
            'number_employees' => 'required|numeric'
        ]);

        $data = array([
            'name' => $request->name,
            'description' => $request->description,
            'prod_max_capacity' => $request->prod_max_capacity,
            'number_employees' => $request->number_employees,
            'ip_address' => $request->ip_address,
            'factory_id' => Auth::user()->factory_id,
            'created_at' => Carbon::now()
        ]);

        try {
            DB::begintransaction();

            //insert table sewing
            DB::table('line')->insert($data);

            DB::commit();
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit sewing-master
    public function edit($id)
    {
        $sewing = Line::find($id);
        return view('sewing/master/edit', compact('sewing'))->with('sewing', $sewing);
    }

    //update sewing-master
    public function update(Request $request)
    {
        $data = $request->all();
        $sewing = DB::table('line')
                        ->where('id', $data['id'])
                        ->update([
                            'name' => $data['name'],
                            'description' => $data['description'],
                            'prod_max_capacity' => $data['prod_max_capacity'],
                            'number_employees' => $data['number_employees'],
                            'ip_address' => $data['ip_address'],
                        ]);

        return view('sewing.master.index');

    }

    //delete sewing-master
    public function destroy(Request $request)
    {
        $id = $request->id;
        $sewing = Line::find($id);
        $sewing->deleted_at = carbon::now();
        $sewing->save();

        return view('sewing/master/index');
    }

    //END OF MASTER SEWING
}
