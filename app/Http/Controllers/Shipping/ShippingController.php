<?php

namespace App\Http\Controllers\Shipping;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

class ShippingController extends Controller
{
    // halaman checkin
    public function index()
    {
        return view('shipping/checkin/index');
    }

    // halaman checkout
    public function checkout() {
        return view('shipping/checkout/index');
    }

    /*
    Constraint :
        - inventory >= 100%
        - qcinspect completed all

        yang boleh di scan onprogress
        a. department :inventory, status :completed

        yang boleh di scan rejected
        a. department :inventory, status :completed
    */
    public function setStatusCheckin(Request $request) {
        $type = 'checkin';
        $barcodeid = trim($request->barcodeid);
        $status = strtolower(trim($request->status));

        $cek_move = DB::table('package')->where('barcode_id',$barcodeid)->first();
        if(isset($cek_move) && $cek_move->is_move_to_fgms_new == true){
            return response()->json("Package sudah dipindah di FGMS BARU",422);

        }

        //allowed status
        if($status !== 'onprogress' && $status !== 'rejected' && $status !== 'cancel') {
            return response()->json('Status unidentified', 422);
        }

        //check if package can be scanned or not
        $check = $this->_check_scan($barcodeid, $status, $type);
        if(!$check) {
            // if($status == 'cancel') {
            //     return response()->json('This package cannot be canceled', 422);
            // }
            // else {
            //     return response()->json('This package cannot be scanned here', 422);
            // }
            $error = $this->_show_error($barcodeid);
            return response()->json($error,422);
        }

        //prepare data for table package and package_movements
        if($status == 'onprogress' || $status == 'rejected') {

            $department_from = $check->current_department;
            $status_from = $check->current_status;
            $status_to = $status;

            if($status_to == 'onprogress') {
                $department_to = 'shipping';
                $description = 'accepted on checkin shipping';
            }
            elseif($status_to == 'rejected') {
                $department_to = 'inventory';
                $description = 'rejected on checkin shipping';
            }

            //data package movement
            $data = array(
                'barcode_package' => $barcodeid,
                'department_from' => $department_from,
                'department_to' => $department_to,
                'status_from' => $status_from,
                'status_to' => $status_to,
                'user_id' => Auth::user()->id,
                'description' => $description,
                'created_at' => Carbon::now()
            );
        }
        elseif($status == 'cancel') {
            $department_to = 'inventory';
            $status_to = 'completed';
            //data for view
            $data = array(
                'barcode_package' => $barcodeid,
                'department_to' => null,
                'status_to' => 'cancel',
                'created_at' => Carbon::now()
            );
            $check->code = null;
        }

        //do database action
        try {
            DB::beginTransaction();

            //query for table package_movements and package
            $query_movement = $this->_query_movement($barcodeid, $status, $type, $data);
            if(!$query_movement) {
                throw new \Exception('Movement error');
            }
            $query_package = $this->_query_package($barcodeid, $status, $type, $department_to, $status_to);
            if(!$query_package) {
                throw new \Exception('Package error');
            }

            DB::commit();
        }
        catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return view('shipping/checkin/ajax_shipping_list')
                ->with('data', $data)
                ->with('rak', $check->code)
                ->with('po_number', $check->po_number)
                ->with('plan_ref_number', $check->plan_ref_number);
    }

    /*
    Constraint :
        yang boleh di scan completed
        a. department :shipping, status :onprogress

        yang boleh di scan cancel
        a. department :shipping, status :completed
    */
    public function setStatusCheckOut(Request $request) {
        $type = 'checkout';
        $barcodeid = trim($request->barcodeid);
        $status = strtolower(trim($request->status));

        $cek_move = DB::table('package')->where('barcode_id',$barcodeid)->first();
        if(isset($cek_move) && $cek_move->is_move_to_fgms_new == true){
            return response()->json("Package sudah dipindah di FGMS BARU",422);

        }

        //allowed status
        if($status !== 'completed' && $status !== 'cancel' && $status!=='sendsewing') {
            return response()->json('Status unidentified', 422);
        }

        //check if package can be scanned or not
        $check = $this->_check_scan($barcodeid, $status, $type);
        if(!$check) {
            $error = $this->_show_error($barcodeid);
            return response()->json($error,422);
        }

        //prepare data for table package and package_movements
        if($status == 'completed') {

            $department_from = $check->current_department;
            $status_from = $check->current_status;
            $status_to = $status;
            $department_to = 'shipping';
            $description = 'completed on shipping';

            //data package movement
            $data = array(
                'barcode_package' => $barcodeid,
                'department_from' => $department_from,
                'department_to' => $department_to,
                'status_from' => $status_from,
                'status_to' => $status_to,
                'user_id' => Auth::user()->id,
                'description' => $description,
                'created_at' => Carbon::now()
            );
        }
        elseif($status == 'cancel') {
            $department_to = 'shipping';
            $status_to = 'onprogress';
            //data for view
            $data = array(
                'barcode_package' => $barcodeid,
                'department_to' => null,
                'status_to' => 'cancel',
                'created_at' => Carbon::now()
            );
        }
        elseif ($status=='sendsewing') {
            $department_from = $check->current_department;
            $status_from = $check->current_status;
            $status_to = 'completed';
            $department_to = 'preparation';
            $description = 'back to sewing';

            $data = array(
                'barcode_package' => $barcodeid,
                'department_from' => $department_from,
                'department_to' => $department_to,
                'status_from' => $status_from,
                'status_to' => $status_to,
                'user_id' => Auth::user()->id,
                'description' => $description,
                'created_at' => Carbon::now()
            );
        }
        
        //do database action
        try {
            DB::beginTransaction();

            //query for table package_movements and package
            $query_movement = $this->_query_movement($barcodeid, $status, $type, $data);
            if(!$query_movement) {
                throw new \Exception('Movement error');
            }
            $query_package = $this->_query_package($barcodeid, $status, $type, $department_to, $status_to);
            if(!$query_package) {
                throw new \Exception('Package error');
            }

            DB::commit();
        }
        catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return view('shipping/checkout/ajax_shipping_list')
                ->with('data', $data)
                ->with('po_number', $check->po_number)
                ->with('plan_ref_number', $check->plan_ref_number);
    }

    //check apakah total package dari po tsb di inventory (atau yg sudah lewat inventory) sudah mencapai 100%
    public function checkPersenCompleted($barcodeid, $po_number, $plan_ref_number) {
        $persen_inventory = DB::table('po_summary')
                                ->select('persen_inventory')
                                ->where('po_number', $po_number)
                                ->where('plan_ref_number', $plan_ref_number)
                                ->where('factory_id', Auth::user()->factory_id)
                                ->whereNull('deleted_at')
                                ->first();

        if($persen_inventory->persen_inventory < 100) {
            $data = array(
                'po_number' => $po_number,
                'plan_ref_number' => $plan_ref_number,
                'persen' => $persen_inventory->persen_inventory,
                'status' => false
            );
        }
        else {
            $data = array(
                'po_number' => $po_number,
                'plan_ref_number' => $plan_ref_number,
                'persen' => $persen_inventory->persen_inventory,
                'status' => true
            );
        }
        return $data;
    }

    //check apakah qc inspect sudah completed all
    public function checkQcCompleted($po_number, $plan_ref_number) {
        $persen_qcinspect = DB::table('po_summary')
                                ->select('persen_qcinspect')
                                ->where('po_number', $po_number)
                                ->where('plan_ref_number', $plan_ref_number)
                                ->where('factory_id', Auth::user()->factory_id)
                                ->whereNull('deleted_at')
                                ->first();

        if($persen_qcinspect->persen_qcinspect < 100) {
            $data['status'] = false;
        }
        else {
            $data['status'] = true;
        }

        return $data;
    }

    //check if all package already on inventory or shipping
    public function checkInventoryAll($po_number) {
        //get qty package on this po
        $qty_po = DB::table('package')
                        ->join('package_detail','package.scan_id', '=','package_detail.scan_id')
                        ->where('po_number', $po_number)
                        ->where('package_detail.factory_id', Auth::user()->factory_id)
                        ->whereNull('package.deleted_at')
                        ->whereNull('package_detail.deleted_at')
                        ->count();

        $qty_inventory = DB::table('package')
                        ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                        ->where('po_number', $po_number)
                        ->where(function($query) {
                            $query->where('current_department', 'inventory')
                                  ->orWhere('current_department', 'shipping');
                        })
                        ->whereNull('package.deleted_at')
                        ->whereNull('package_detail.deleted_at')
                        ->count();

        if($qty_inventory == $qty_po) {
            $data['qty_inventory'] = $qty_inventory;
            $data['status'] = true;
        }
        else {
            $data['qty_inventory'] = $qty_inventory;
            $data['status'] = false;
        }

        return $data;
    }

    //check if package can be scanned or not
    private function _check_scan($barcodeid, $check_status, $type) {
        if($type == 'checkin') {
            if($check_status !== 'onprogress' && $check_status !== 'rejected' && $check_status !== 'cancel') {
                return false;
            }
        }
        elseif($type == 'checkout') {
            if($check_status !== 'completed' && $check_status !== 'cancel' && $check_status!=='sendsewing') {
                return false;
            }
        }

        //proses check
        if($check_status == 'onprogress' || $check_status == 'rejected') {
            $check = DB::table('package')
                     ->select('current_department', 'current_status', 'po_number', 'code', 'plan_ref_number')
                     ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                     ->leftJoin('package_movements','package.barcode_id','=','package_movements.barcode_package')
                     ->leftJoin('locators','package_movements.barcode_locator','=','locators.barcode')
                     ->where('barcode_id', $barcodeid)
                     ->where('package_detail.factory_id', Auth::user()->factory_id)
                     ->where('current_department', 'inventory')
                     ->where('current_status', 'completed')
                     ->where('package_movements.is_canceled', false)
                     ->whereNull('package.deleted_at')
                     ->whereNull('package_detail.deleted_at')
                     ->whereNull('package_movements.deleted_at')
                     ->whereNull('locators.deleted_at')
                     ->first();

            if(!$check) {
                return false;
            }
            else {
                //VALIDASI MAS BUDI
                // //check apakah inventory sudah 100%
                // $isReadyInv = $this->checkPersenCompleted($barcodeid, $check->po_number);
                // if(!$isReadyInv['status']) {
                //     // $data = array(
                //     //     'error' => 'Total Package dari Purchase Order #'.
                //     //                             $isReadyInv['po_number'].
                //     //                             ' belum mencapai 100% di Inventory'
                //     // );
                //     // return $data;
                //     return false;
                // }
                //
                // //check apakah qc_inspect sudah completed all;
                // $isCompletedQc = $this->checkQcCompleted($isReadyInv['po_number']);
                //
                // if($isCompletedQc['status'] == false) {
                //     // $data = array(
                //     //     'error' => 'QC Inspect should be completed all first'
                //     // );
                //     // return $data;
                //     return false;
                // }
                //
                // //check apakah semua barang termasuk dari qc inspect sudah dibalikkan ke inventory
                // $isInvAll = $this->checkInventoryAll($isReadyInv['po_number']);
                // if($isInvAll['status'] == false) {
                //     // $data = array(
                //     //     'error' => 'All package should be on inventory'
                //     // );
                //     // return $data;
                //     return false;
                // }
                return $check;
            }
        }
        elseif($check_status == 'completed') {
            //VALIDASI STUFFING : QC COMPLETED & INV COMPLETED
            $check = DB::table('package')
                     ->select('current_department', 'current_status', 'po_number', 'plan_ref_number')
                     ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                     ->where('barcode_id', $barcodeid)
                     ->where('package_detail.factory_id', Auth::user()->factory_id)
                     ->where('current_department', 'shipping')
                     ->where('current_status', 'onprogress')
                     ->whereNull('package.deleted_at')
                     ->whereNull('package_detail.deleted_at')
                     ->first();

            if(!$check) {
                return false;
            }
            else {
                $isReadyInv = $this->checkPersenCompleted($barcodeid, $check->po_number, $check->plan_ref_number);

                $isCompletedQc = $this->checkQcCompleted($isReadyInv['po_number'], $isReadyInv['plan_ref_number']);

                if($isCompletedQc['status'] == false) {
                    // $data = array(
                    //     'error' => 'QC Inspect should be completed all first'
                    // );
                    // return $data;
                    return false;
                }


                return $check;
            }


        }
        elseif($check_status == 'cancel') {
            if($type == 'checkin') {
                $check = DB::table('package')
                         ->select('current_department', 'current_status', 'po_number', 'plan_ref_number')
                         ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                         ->where('barcode_id', $barcodeid)
                         ->where('package_detail.factory_id', Auth::user()->factory_id)
                         ->where(function($query) {
                             $query->where(function($query) {
                                 $query->where('current_department', 'inventory')
                                       ->where('current_status', 'rejected');
                             })
                             ->orWhere(function($query) {
                                 $query->where('current_department', 'shipping')
                                       ->where('current_status', 'onprogress');
                             });
                         })
                         ->whereNull('package.deleted_at')
                         ->whereNull('package_detail.deleted_at')
                         ->first();
            }
            elseif($type == 'checkout') {
                $check = DB::table('package')
                         ->select('current_department', 'current_status', 'po_number', 'plan_ref_number')
                         ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                         ->where('barcode_id', $barcodeid)
                         ->where('package_detail.factory_id', Auth::user()->factory_id)
                         ->where('current_department', 'shipping')
                         ->where('current_status', 'completed')
                         ->whereNull('package.deleted_at')
                         ->whereNull('package_detail.deleted_at')
                         ->first();
            }
            else {
                return false;
            }

            if(!$check) {
                return false;
            }
            else {
                return $check;
            }
        }
        elseif ($check_status=='sendsewing') {
           $check = DB::table('package')
                         ->select('current_department', 'current_status', 'po_number', 'plan_ref_number')
                         ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                         ->where('barcode_id', $barcodeid)
                         ->where('package_detail.factory_id', Auth::user()->factory_id)
                         ->where('current_department', 'shipping')
                         ->where('current_status', 'onprogress')
                         ->whereNull('package.deleted_at')
                         ->whereNull('package_detail.deleted_at')
                         ->first();
            if (!$check) {
                return false;
            }else{
                return $check;
            }
        }
        else {
            return false;
        }


    }

    //show error
    private function _show_error($barcodeid) {
        $error = '';
        $check_current_status = $this->_get_current_status($barcodeid);
        if($check_current_status) {
            if($check_current_status->current_status == 'onprogress') {
                $check_current_status->current_status = 'in';
            }
            elseif($check_current_status->current_status == 'completed') {
                $check_current_status->current_status = 'out';
            }

            if ($check_current_status->current_department == 'shipping' && $check_current_status->current_status == 'in') {
                $info_status = $check_current_status->current_department . ' - '
                                . $check_current_status->current_status . ' / QC Inspect should be completed all';
            }else{
                $info_status = $check_current_status->current_department . ' - '
                        . $check_current_status->current_status;
            }
            $error .= 'Last Scanned (' . $check_current_status->barcode_id . ') : '
                        . $info_status;
        }
        else {
            $error = 'Barcode not found on system';
        }

        return $error;
    }

    //get current status
    private function _get_current_status($barcodeid) {
        $data = DB::table('package')
                    ->select('barcode_id','current_department', 'current_status')
                    ->where('barcode_id', $barcodeid)
                    ->whereNull('deleted_at')
                    ->first();
        return $data;
    }

    //query movement
    private function _query_movement($barcodeid, $check_status, $type, $data_movement = null) {
        
        if($check_status == 'onprogress' || $check_status == 'rejected' || $check_status == 'completed') {

            try {
                DB::beginTransaction();

                //update deleted_at on the last package movement
                $last_data = DB::table('package_movements')
                            ->where('barcode_package', $barcodeid)
                            ->where('is_canceled', false)
                            ->whereNull('deleted_at')
                            ->first();
                if(!$last_data) {
                    throw new \Exception('Package not found');
                }
                else {
                    DB::table('package_movements')
                    ->where('id', $last_data->id)
                    ->update([
                        'deleted_at' => Carbon::now()
                    ]);
                }

                //insert movement history
                $insert = DB::table('package_movements')->insert($data_movement);

                if ($insert) {
                    // cek apakah insert double ?
                    $cek_insert = DB::table('package_movements')
                                    ->where('barcode_package', $barcodeid)
                                    ->where('is_canceled', false)
                                    ->whereNull('deleted_at')
                                    ->count();

                    if ($cek_insert > 1) {
                        throw new \Exception('data double inserted');
                    }
                }
                DB::commit();
            }
            catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return true;
        }
        elseif($check_status == 'cancel') {
            try {
                DB::beginTransaction();

                /*
                    - get second last data from table package_movements
                    - get last data from table package_movements
                    - update second last data, deleted_at = null
                    - update last data, deleted_at = now() and is_canceled = true
                */
                $second_last_data = DB::table('package_movements')
                                        ->where('barcode_package', $barcodeid)
                                        ->where('is_canceled', false)
                                        ->whereNotNull('deleted_at')
                                        ->orderBy('created_at', 'desc')
                                        ->first();

                $last_data = DB::table('package_movements')
                                 ->where('barcode_package', $barcodeid)
                                 ->where('is_canceled', false)
                                 ->whereNull('deleted_at')
                                 ->first();


                $ceklsd = DB::table('package_movements')
                    ->where('id', $second_last_data->id)
                    ->update([
                        'deleted_at' => null
                    ]);

                $cekld = DB::table('package_movements')
                    ->where('id', $last_data->id)
                    ->update([
                        'is_canceled' => true,
                        'deleted_at' => Carbon::now()
                    ]);

                if ($ceklsd && $cekld) {
                    $countpm = DB::table('package_movements')
                                ->where('barcode_package',$barcodeid)
                                ->whereNull('deleted_at')
                                ->where('is_canceled',false)
                                ->count();
                    if ($countpm != 1) {
                        throw new \Exception('data inserted error');
                    }
                }

                DB::commit();
            }
            catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return true;
        }
        elseif ($check_status=='sendsewing') {
                // naufal
            try {
                DB::beginTransaction();
                    $last_data = DB::table('package_movements')
                                        ->where('barcode_package',$barcodeid)
                                        ->where('is_canceled',false)
                                        ->whereNull('deleted_at')
                                        ->first();
                    if(!$last_data) {
                        throw new \Exception('Package not found');
                    }
                    else {
                        DB::table('package_movements')
                        ->where('id', $last_data->id)
                        ->update([
                            'deleted_at' => Carbon::now()
                        ]);
                    }

                    $insert = DB::table('package_movements')->insert($data_movement);

                    if ($insert) {
                        // cek apakah insert double ?
                        $cek_insert = DB::table('package_movements')
                                        ->where('barcode_package', $barcodeid)
                                        ->where('is_canceled', false)
                                        ->whereNull('deleted_at')
                                        ->count();

                        if ($cek_insert > 1) {
                            throw new \Exception('data double inserted');
                        }
                    }


                    DB::table('package_movements')
                    ->where('barcode_package', $barcodeid)
                    // ->where('status_to', '<>', 'onprogress')
                    ->where('is_canceled', false)
                    ->whereNotNull('department_from')
                    ->whereNotNull('deleted_at')
                    ->update([
                        'deleted_at' => Carbon::now(),
                        'is_canceled' => true
                    ]);
                    
                    DB::connection('line')->table('folding_package_barcode')
                                        ->where('barcode_id', $barcodeid)
                                        ->update([
                                                'status' => 'back to sewing',
                                                'complete_at' => null,
                                                'complete_by' => null,
                                                'backto_sewing' => true
                                        ]);

                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }
            return true;
        }
        else {
            return false;
        }
    }

    //query package
    private function _query_package($barcodeid, $check_status, $type, $department, $status) {

        if($type == 'checkin') {
            /*
                jika status onprogress, update column 'status_shipping' = onprogress
                jika status rejected, update column 'status_inventory' = rejected
                jika status cancel, update column 'status_inventory' = completed && 'status_shipping' = null
            */
            if($check_status == 'onprogress') {
                $update_clause = array(
                    'current_department' => $department,
                    'current_status' => $status,
                    'status_shipping' => 'onprogress',
                    'updated_at' => Carbon::now()
                );
            }
            elseif($check_status == 'rejected') {
                $update_clause = array(
                    'current_department' => $department,
                    'current_status' => $status,
                    'status_inventory' => 'rejected',
                    'updated_at' => Carbon::now()
                );
            }
            elseif($check_status == 'cancel') {
                $update_clause = array(
                    'current_department' => $department,
                    'current_status' => $status,
                    'status_shipping' => null,
                    'status_inventory' => 'completed',
                    'updated_at' => Carbon::now()
                );
            }
            else {
                return false;
            }
        }
        elseif($type == 'checkout') {
            /*
                jika status completed, update column 'status_shipping' = completed
                jika status cancel, update column 'status_shipping' = onprogress
            */
            if($check_status == 'completed') {
                $update_clause = array(
                    'current_department' => $department,
                    'current_status' => $status,
                    'status_shipping' => 'completed',
                    'updated_at' => Carbon::now()
                );
            }
            elseif($check_status == 'cancel') {
                $update_clause = array(
                    'current_department' => $department,
                    'current_status' => $status,
                    'status_shipping' => 'onprogress',
                    'updated_at' => Carbon::now()
                );
            }elseif ($check_status == 'sendsewing') {
                // naufal
                $update_clause = array(
                    'current_department' => $department,
                    'current_status' => $status,
                    'status_shipping' => null,
                    'status_inventory' => null,
                    'status_sewing' => null,
                    'status_qcinspect' => null,
                    'updated_at' => Carbon::now()
                );

               
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }

        try {
            DB::beginTransaction();

            DB::table('package')
                ->where('barcode_id', $barcodeid)
                ->whereNull('deleted_at')
                ->update($update_clause);

            DB::commit();
        }
        catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }
}
