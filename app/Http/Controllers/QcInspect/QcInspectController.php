<?php

namespace App\Http\Controllers\QcInspect;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

class QcInspectController extends Controller
{
    // halaman checkin
    public function index()
    {
        return view('qcinspect/checkin/index');
    }

    // halaman completed
    public function checkout() {
        return view('qcinspect/checkout/index');
    }

    /*
    Constraint :
        yang boleh di scan onprogress
        a. department :inventory, status :completed
        b. total inventory >= 80%
        c. status_qcinspect != completed
    */
    public function setStatusCheckin(Request $request) {
        $type = 'checkin';
        $barcodeid = trim($request->barcodeid);
        $status = strtolower(trim($request->status));

        //allowed status
        if($status !== 'onprogress') {
            return response()->json('Status unidentified', 422);
        }

        //check if package can be scanned or not
        $check = $this->_check_scan($barcodeid, $status, $type);
        if(!$check) {
            $error = $this->_show_error($barcodeid);
            return response()->json($error,422);
        }

        //prepare data for table package and package_movements
        $department_from = $check->current_department;
        $status_from = $check->current_status;
        $status_to = $status;
        $department_to = 'qcinspect';
        $description = 'accepted on checkin qcinspect';

        //data package movement
        $data = array(
            'barcode_package' => $barcodeid,
            'department_from' => $department_from,
            'department_to' => $department_to,
            'status_from' => $status_from,
            'status_to' => $status_to,
            'user_id' => Auth::user()->id,
            'description' => $description,
            'created_at' => Carbon::now()
        );

        //do database action
        try {
            DB::beginTransaction();

            //query for table package_movements and package
            $query_movement = $this->_query_movement($barcodeid, $status, $type, $data);
            if(!$query_movement) {
                throw new \Exception('Movement error');
            }
            $query_package = $this->_query_package($barcodeid, $status, $type, $department_to, $status_to);
            if(!$query_package) {
                throw new \Exception('Package error');
            }

            DB::commit();
        }
        catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return view('qcinspect/checkin/ajax_qcinspect_list')
                ->with('data', $data)
                ->with('rak', $check->code)
                ->with('po_number', $check->po_number)
                ->with('plan_ref_number', $check->plan_ref_number);
    }

    /*
    Constraint :
        yang boleh di scan completed
        a. department :qcinspect, status :onprogress
        b. department :qcinspect, status :rejected

        yang boleh di scan rejected
        a. department :qcinspect, status :onprogress
        b. department :qcinspect, status :rejected
    */
    // public function setStatusCheckOut(Request $request) {
    //     $type = 'checkout';
    //     $barcodeid = trim($request->barcodeid);
    //     $status = strtolower(trim($request->status));
    //
    //     //allowed status
    //     if($status !== 'completed' && $status !== 'rejected') {
    //         return response()->json('Status unidentified', 422);
    //     }
    //
    //     //check if package can be scanned or not
    //     $check = $this->_check_scan($barcodeid, $status, $type);
    //     if(!$check) {
    //         return response()->json('This package cannot be scanned here', 422);
    //     }
    //
    //     //prepare data for table package and package_movements
    //     $department_from = $check->current_department;
    //     $status_from = $check->current_status;
    //     $status_to = $status;
    //
    //     if($status == 'completed' || $status == 'rejected') {
    //         if($status == 'completed') {
    //             $department_to = 'qcinspect';
    //             $description = 'completed on qcinspect';
    //         }
    //         else {
    //             $department_to = 'qcinspect';
    //             $description = 'rejected on qcinspect';
    //         }
    //
    //         //data package movement
    //         $data = array(
    //             array(
    //                 'barcode_package' => $barcodeid,
    //                 'department_from' => $department_from,
    //                 'department_to' => $department_to,
    //                 'status_from' => $status_from,
    //                 'status_to' => $status_to,
    //                 'user_id' => Auth::user()->id,
    //                 'description' => $description,
    //                 'created_at' => Carbon::now()
    //             )
    //         );
    //     }
    //     elseif($status == 'rejected') {
    //         $description = 'rejected all by qcinspect';
    //     }
    //     elseif($status == 'cancel') {
    //
    //     }
    //
    //     //do database action
    //     try {
    //         DB::beginTransaction();
    //
    //         if($status == 'completed' || $status == 'rejected') {
    //             //query for table package_movements and package
    //             $query_movement = $this->_query_movement($barcodeid, $status, $type, $data);
    //             if(!$query_movement) {
    //                 throw new \Exception('Movement error');
    //             }
    //             $query_package = $this->_query_package($barcodeid, $status, $type, $department_to, $status_to);
    //             if(!$query_package) {
    //                 throw new \Exception('Package error');
    //             }
    //         }
    //
    //         /*
    //             jika status completed, update persen_qcinspect di table po_summary
    //                                     jika persen_qcinspect == 100% maka
    //                                     set semua package pada po ini menjadi status_qcinspect = completed
    //                                     dan move package pada qcinspect yg berstatus != completed menjadi completed
    //             jika status rejected_all, move semua package kecuali pada department preparation ke status rejected
    //         */
    //         if($status == 'completed') {
    //             $update_persen_qc = $this->updatePersenQc($check->po_number);
    //             if(!$update_persen_qc) {
    //                 throw new \Exception('Persen qc error');
    //             }
    //             elseif($update_persen_qc >= 100) {
    //                 $update_status_qcinspect = DB::table('package')
    //                                                 ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
    //                                                 ->where('package_detail.po_number', $check->po_number)
    //                                                 ->whereNull('package.deleted_at')
    //                                                 ->whereNull('package_detail.deleted_at')
    //                                                 ->update([
    //                                                     'status_qcinspect' => 'completed',
    //                                                     'updated_at' => Carbon::now()
    //                                                 ]);
    //
    //                 $get_list_barcode = DB::table('package')
    //                                         ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
    //                                         ->where('package.current_department', 'qcinspect')
    //                                         ->where('package_detail.po_number', $check->po_number)
    //                                         ->whereNull('package.deleted_at')
    //                                         ->whereNull('package_detail.deleted_at')
    //                                         ->get();
    //                 foreach ($get_list_barcode as $key => $value) {
    //                     if($value->barcode_id == $barcodeid) {
    //                         continue;
    //                     }
    //
    //                     if($value->current_department != 'qcinspect') {
    //                         continue;
    //                     }
    //
    //                     if($value->current_department == 'qcinspect' && $value->current_status == 'completed') {
    //                         continue;
    //                     }
    //
    //                     $movement = array(
    //                         'barcode_package' => $value->barcode_id,
    //                         'department_from' =>  $value->current_department,
    //                         'department_to' => 'qcinspect',
    //                         'status_from' => $value->current_status,
    //                         'status_to' => 'completed',
    //                         'user_id' => Auth::user()->id,
    //                         'description' => $description,
    //                         'created_at' => Carbon::now()
    //                     );
    //                     $move = $this->_query_movement($value->barcode_id, 'completed', $type, $movement);
    //                     if(!$move) {
    //                         throw new \Exception('Movement error');
    //                     }
    //                     $query_package_again = $this->_query_package($value->barcode_id, 'completed', $type, $value->current_department, 'completed');
    //                     $data[] = $movement;
    //                 }
    //             }
    //         }
    //         elseif($status == 'rejected_all') {
    //             //update persen_qcinspect on table po_summary with 0
    //             //update completed_qty_qc on table po_summary with 0
    //             $update_persen_qc = DB::table('po_summary')
    //                                     ->where('po_number', $check->po_number)
    //                                     ->whereNull('deleted_at')
    //                                     ->update([
    //                                         'completed_qty_qc' => 0,
    //                                         'persen_qcinspect' => 0,
    //                                         'updated_at' => Carbon::now()
    //                                     ]);
    //
    //             $get_list_barcode = DB::table('package')
    //                                     ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
    //                                     ->where('package_detail.po_number', $check->po_number)
    //                                     ->whereNull('package.deleted_at')
    //                                     ->whereNull('package_detail.deleted_at')
    //                                     ->get();
    //
    //             //query movement again
    //             foreach ($get_list_barcode as $key => $value) {
    //                 if($value->current_department == 'preparation' || $value->current_department == 'shipping') {
    //                     continue;
    //                 }
    //                 $movement = array(
    //                     'barcode_package' => $value->barcode_id,
    //                     'department_from' =>  $value->current_department,
    //                     'department_to' => $value->current_department,
    //                     'status_from' => $value->current_status,
    //                     'status_to' => 'rejected',
    //                     'user_id' => Auth::user()->id,
    //                     'description' => $description,
    //                     'created_at' => Carbon::now()
    //                 );
    //
    //                 $move = $this->_query_movement($value->barcode_id, 'rejected', $type, $movement);
    //                 if(!$move) {
    //                     throw new \Exception('Movement error');
    //                 }
    //
    //                 $query_package_again = $this->_query_package($value->barcode_id, 'rejected', $type, $value->current_department, 'rejected');
    //                 if(!$query_package_again) {
    //                     throw new \Exception('Package error');
    //                 }
    //
    //                 $data[] = $movement;
    //             }
    //         }
    //
    //         DB::commit();
    //     }
    //     catch (Exception $ex) {
    //         db::rollback();
    //         $message = $ex->getMessage();
    //         ErrorHandler::db($message);
    //     }
    //
    //     return view('qcinspect/checkout/ajax_qcinspect_list')->with('data', $data);
    // }


    //test setstatuscheckout yang baru
    public function setStatusCheckOut(Request $request) {
        $type = 'checkout';
        $barcodeid = trim($request->barcodeid);
        $status = strtolower(trim($request->status));

        //allowed status
        if($status !== 'completed' && $status !== 'rejected' && $status !== 'cancel') {
            return response()->json('Status unidentified', 422);
        }

        //check if package can be scanned or not
        $check = $this->_check_scan($barcodeid, $status, $type);
        if(!$check) {
            $error = $this->_show_error($barcodeid);
            return response()->json($error,422);
        }

        //prepare data for table package_movements
        if($status == 'completed') {
            $description = 'completed all by qcinspect';
        }
        elseif($status == 'rejected') {
            $description = 'rejected all by qcinspect';
        }
        elseif($status == 'cancel') {
            $description = 'cancel all by qcinspect';
        }

        //do database action
        try {
            DB::beginTransaction();

            /*
                jika status released/completed ,
                                        set semua package pada po ini menjadi status_qcinspect = completed
                                        dan move package pada qcinspect yg berstatus != completed menjadi completed
                jika status rejected, move semua package kecuali pada department preparation ke status rejected
            */
            if($status == 'completed') {

                // cek apakah status terakhir ada rejected
                $get_list_rejected = DB::table('package')
                                            ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                                            ->where('package.current_department', 'qcinspect')
                                            ->where('package.current_status', 'rejected')
                                            ->where('package_detail.po_number', $check->po_number)
                                            ->where('plan_ref_number', $check->plan_ref_number)
                                            ->where('package_detail.factory_id', Auth::user()->factory_id)
                                            ->whereNull('package.deleted_at')
                                            ->whereNull('package_detail.deleted_at')
                                            ->count();
                if ($get_list_rejected > 0) {
                    throw new \Exception('rejected found');
                }else{

                    $update_persen_qc = $this->updatePersenQc($check->po_number, $check->plan_ref_number);
                    if(!$update_persen_qc) {
                        throw new \Exception('Persen qc error');
                    }
                    elseif($update_persen_qc >= 100) {
                        $update_status_qcinspect = DB::table('package')
                                                        ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                                                        ->where('package_detail.po_number', $check->po_number)
                                                        ->where('plan_ref_number', $check->plan_ref_number)
                                                        ->where('package_detail.factory_id', Auth::user()->factory_id)
                                                        ->whereNull('package.deleted_at')
                                                        ->whereNull('package_detail.deleted_at')
                                                        ->update([
                                                            'status_qcinspect' => 'completed',
                                                            'updated_at' => Carbon::now()
                                                        ]);

                        $get_list_barcode = DB::table('package')
                                                ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                                                ->where('package.current_department', 'qcinspect')
                                                ->where('package_detail.po_number', $check->po_number)
                                                ->where('plan_ref_number', $check->plan_ref_number)
                                                ->where('package_detail.factory_id', Auth::user()->factory_id)
                                                ->whereNull('package.deleted_at')
                                                ->whereNull('package_detail.deleted_at')
                                                ->get();
                        foreach ($get_list_barcode as $key => $value) {

                            $movement = array(
                                'barcode_package' => $value->barcode_id,
                                'department_from' =>  $value->current_department,
                                'department_to' => 'qcinspect',
                                'status_from' => $value->current_status,
                                'status_to' => 'completed',
                                'user_id' => Auth::user()->id,
                                'description' => $description,
                                'created_at' => Carbon::now()
                            );
                            $move = $this->_query_movement($value->barcode_id, 'completed', $type, $movement);
                            if(!$move) {
                                throw new \Exception('Movement error');
                            }
                            $query_package_again = $this->_query_package($value->barcode_id, 'completed', $type, $value->current_department, 'completed');
                            $data[] = $movement;
                        }
                    }
                }
            }
            elseif($status == 'rejected') {
                //update persen_qcinspect on table po_summary with 0
                //update completed_qty_qc on table po_summary with 0
                $update_persen_qc = DB::table('po_summary')
                                        ->where('po_number', $check->po_number)
                                        ->where('plan_ref_number', $check->plan_ref_number)
                                        ->where('factory_id', Auth::user()->factory_id)
                                        ->whereNull('deleted_at')
                                        ->update([
                                            'completed_qty_qc' => 0,
                                            'persen_qcinspect' => 0,
                                            'updated_at' => Carbon::now()
                                        ]);

                $get_list_barcode = DB::table('package')
                                        ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                                        ->where('package_detail.po_number', $check->po_number)
                                        ->where('package_detail.plan_ref_number', $check->plan_ref_number)
                                        ->where('package_detail.factory_id', Auth::user()->factory_id)
                                        ->where(function($query) {
                                             $query->where(function($query) {
                                                 $query->where('current_department', 'qcinspect')
                                                       ->where('current_status', 'onprogress');
                                             })
                                             ->orWhere(function($query) {
                                                 $query->where('current_department', 'inventory')
                                                       ->where('current_status', 'completed');
                                             });
                                         })
                                        ->whereNull('package.deleted_at')
                                        ->whereNull('package_detail.deleted_at')
                                        ->get();

                //query movement again
                foreach ($get_list_barcode as $key => $value) {
                    if($value->current_department == 'preparation' || $value->current_department == 'shipping') {
                        continue;
                    }
                    $movement = array(
                        'barcode_package' => $value->barcode_id,
                        'department_from' =>  $value->current_department,
                        'department_to' => $value->current_department,
                        'status_from' => $value->current_status,
                        'status_to' => 'rejected',
                        'user_id' => Auth::user()->id,
                        'description' => $description,
                        'created_at' => Carbon::now()
                    );

                    $move = $this->_query_movement($value->barcode_id, 'rejected', $type, $movement);
                    if(!$move) {
                        throw new \Exception('Movement error');
                    }

                    $query_package_again = $this->_query_package($value->barcode_id, 'rejected', $type, $value->current_department, 'rejected');
                    if(!$query_package_again) {
                        throw new \Exception('Package error');
                    }

                    if($value->current_department == 'qcinspect') {
                        $data[] = $movement;
                    }
                }
            }
            elseif ($status == 'cancel') {

                $get_list_barcode = DB::table('package')
                                        ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                                        ->where('package_detail.po_number', $check->po_number)
                                        ->where('package_detail.plan_ref_number', $check->plan_ref_number)
                                        ->where('package_detail.factory_id', Auth::user()->factory_id)
                                        ->where(function($query) {
                                             $query->where(function($query) {
                                                 $query->where('current_department', 'qcinspect')
                                                       ->where('current_status', 'rejected');
                                             })
                                             ->orWhere(function($query) {
                                                 $query->where('current_department', 'inventory')
                                                       ->where('current_status', 'rejected');
                                             });
                                         })
                                        ->whereNull('package.deleted_at')
                                        ->whereNull('package_detail.deleted_at')
                                        ->get();

                //query movement again
                foreach ($get_list_barcode as $key => $value) {
                    if($value->current_department == 'preparation' || $value->current_department == 'shipping') {
                        continue;
                    }
                    $movement = array(
                        'barcode_package' => $value->barcode_id,
                        'department_from' =>  $value->current_department,
                        'department_to' => $value->current_department,
                        'status_from' => $value->current_status,
                        'status_to' => 'cancel',
                        'user_id' => Auth::user()->id,
                        'description' => $description,
                        'created_at' => Carbon::now()
                    );

                    $move = $this->_query_movement($value->barcode_id, 'cancel', $type, $movement);
                    if(!$move) {
                        throw new \Exception('Movement error');
                    }

                    $query_package_again = $this->_query_package($value->barcode_id, 'cancel', $type, $value->current_department, 'cancel');
                    if(!$query_package_again) {
                        throw new \Exception('Package error');
                    }

                    if($value->current_department == 'qcinspect') {
                        $data[] = $movement;
                    }
                }

                $update_persen_qc = DB::table('po_summary')
                                        ->where('po_number', $check->po_number)
                                        ->where('plan_ref_number', $check->plan_ref_number)
                                        ->where('factory_id', Auth::user()->factory_id)
                                        ->whereNull('deleted_at')
                                        ->update([
                                            'completed_qty_qc' => 0,
                                            'persen_qcinspect' => 0,
                                            'updated_at' => Carbon::now()
                                        ]);
            }

            DB::commit();
        }
        catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return view('qcinspect/checkout/ajax_qcinspect_list')
                ->with('data', $data)
                ->with('po_number', $check->po_number)
                ->with('plan_ref_number', $check->plan_ref_number);
    }

    //check apakah total package dari po tsb di inventory (atau yg sudah lewat inventory) sudah mencapai 80%
    public function checkPersenCompleted($barcodeid, $po_number) {
        $total_package = DB::table('package')
                            ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                            ->where('package_detail.po_number', $po_number)
                            ->where('plan_ref_number', $check->plan_ref_number)
                            ->where('package_detail.factory_id', Auth::user()->factory_id)
                            ->whereNull('package.deleted_at')
                            ->whereNull('package_detail.deleted_at')
                            ->count();

        //package that already passed inventory placement
        $completed_package = DB::table('package')
                            ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                            ->where('package_detail.po_number', $po_number)
                            ->where('plan_ref_number', $check->plan_ref_number)
                            ->where('package_detail.factory_id', Auth::user()->factory_id)
                            ->whereNotNull('status_inventory')
                            ->whereNull('package.deleted_at')
                            ->whereNull('package_detail.deleted_at')
                            ->count();
        $persen_completed = ($completed_package / $total_package) * 100;

        if($persen_completed < 80) {
            $data = array(
                'po_number' => $po_number,
                'total_package' => $total_package,
                'completed_package' => $completed_package,
                'persen_completed' => $persen_completed,
                'status' => false
            );
            return $data;
        }

        $data = array(
            'po_number' => $po_number,
            'total_package' => $total_package,
            'completed_package' => $completed_package,
            'persen_completed' => $persen_completed,
            'status' => true
        );
        return $data;
    }

    //check if qc from this package is already completed
    public function checkQcIsCompleted($barcodeid) {
        $check = DB::table('package')
                     ->where('barcode_id', $barcodeid)
                     ->where('status_qcinspect', 'completed')
                     ->whereNull('package.deleted_at')
                     ->count();
        return $check;
    }

    //update persen qc di table po_summary (fungsi lama)
    // public function updatePersenQc($ponumber) {
    //     $sample_size = 0;
    //     //get total qty pada ponumber
    //     $total_qty = DB::table('package_detail')
    //                      ->where('po_number', $ponumber)
    //                      ->whereNull('deleted_at')
    //                      ->sum('item_qty');
    //
    //     if($total_qty == null || $total_qty <= 0) {
    //         return false;
    //     }
    //     elseif($total_qty < 200) {
    //         $sample_size = $total_qty;
    //     }
    //     else {
    //         //get sample size from tabel formula_qc
    //         $formula = DB::table('formula_qc')
    //                         ->where('load_size_from', '<=', $total_qty)
    //                         ->where('load_size_to', '>=', $total_qty)
    //                         ->whereNull('deleted_at')
    //                         ->first();
    //         if(!$formula) {
    //             return false;
    //         }
    //
    //         //hitung persen_qcinspect
    //         $sample_size = $formula->sample_size;
    //     }
    //
    //     //get current total innerpack yang sudah completed
    //     $total_innerpack = DB::table('package')
    //                             ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
    //                             ->where('po_number', $ponumber)
    //                             ->where('package.status_qcinspect', 'completed')
    //                             ->whereNull('package.deleted_at')
    //                             ->whereNull('package_detail.deleted_at')
    //                             ->sum('inner_pack');
    //
    //     $persen = ($total_innerpack/$sample_size) * 100;
    //     $persen = number_format($persen, 2);
    //
    //     //update persen_qcinspect pada table po_summary
    //     $update_persen = DB::table('po_summary')
    //                          ->where('po_number', $ponumber)
    //                          ->whereNull('deleted_at')
    //                          ->update([
    //                              'sample_size_qc' => $sample_size,
    //                              'completed_qty_qc' => $total_innerpack,
    //                              'persen_qcinspect' => $persen,
    //                              'updated_at' => Carbon::now()
    //                          ]);
    //
    //     return $persen;
    // }

    //update persen qc di table po_summary menjadi 100%
    public function updatePersenQc($ponumber, $plan_ref_number) {
        $sample_size = 0;
        //get total qty pada ponumber
        $total_qty = DB::table('package_detail')
                         ->where('po_number', $ponumber)
                         ->where('plan_ref_number', $plan_ref_number)
                         ->where('factory_id', Auth::user()->factory_id)
                         ->whereNull('deleted_at')
                         ->sum('item_qty');

        if($total_qty == null || $total_qty <= 0) {
            return false;
        }
        // elseif($total_qty < 200) {
            $sample_size = $total_qty;
        // }
        // else {
        //     //get sample size from tabel formula_qc
        //     $formula = DB::table('formula_qc')
        //                     ->where('load_size_from', '<=', $total_qty)
        //                     ->where('load_size_to', '>=', $total_qty)
        //                     ->whereNull('deleted_at')
        //                     ->first();
        //     if(!$formula) {
        //         return false;
        //     }

        //     //hitung persen_qcinspect
        //     $sample_size = $formula->sample_size;
        // }

        $persen = 100;
        $persen = number_format($persen, 2);

        //update persen_qcinspect pada table po_summary
        $update_persen = DB::table('po_summary')
                             ->where('po_number', $ponumber)
                             ->where('plan_ref_number', $plan_ref_number)
                             ->where('factory_id', Auth::user()->factory_id)
                             ->whereNull('deleted_at')
                             ->update([
                                 'sample_size_qc' => $sample_size,
                                 'persen_qcinspect' => $persen,
                                 'updated_at' => Carbon::now()
                             ]);

        return $persen;
    }

    //check if package can be scanned or not
    private function _check_scan($barcodeid, $check_status, $type) {
        if($type == 'checkin') {
            if($check_status !== 'onprogress') {
                return false;
            }
        }
        elseif($type == 'checkout') {
            if($check_status !== 'completed' && $check_status !== 'rejected' && $check_status !== 'cancel') {
                return false;
            }
        }

        //proses check
        if($check_status == 'onprogress') {
            $check = DB::table('package')
                        ->select('current_department', 'current_status', 'po_number', 'code', 'plan_ref_number')
                        ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                        ->leftJoin('package_movements','package.barcode_id','=','package_movements.barcode_package')
                        ->leftJoin('locators','package_movements.barcode_locator','=','locators.barcode')
                        ->where('package.barcode_id', $barcodeid)
                        ->where('package_detail.factory_id', Auth::user()->factory_id)
                        ->where('package.current_status', 'completed')
                        ->where('package.current_department', 'inventory')
                        ->where('package_movements.is_canceled', false)
                        ->whereNull('package.deleted_at')
                        ->whereNull('package_detail.deleted_at')
                        ->whereNull('package_movements.deleted_at')
                        ->whereNull('locators.deleted_at')
                        ->first();

            if(!$check) {
                return false;
            }
            else {
                //VALIDASI MAS BUDI
                // //check apakah total package dari po tsb di inventory sudah mencapai 80%
                // $isReady = $this->checkPersenCompleted($barcodeid, $check->po_number);
                // if(!$isReady['status']) {
                //     // $data = array(
                //     //     'error' => 'Total Package dari Purchase Order #'. $isReady['po_number'].
                //     //                 ' belum mencapai 80% di Inventory'
                //     // );
                //     // return $data;
                //     return false;
                // }
                //
                // //check apakah qc inspect sudah set all completed
                // $isQcCompleted = $this->checkQcIsCompleted($barcodeid);
                // if($isQcCompleted > 0) {
                //     // $data = array(
                //     //     'error' => 'Purchase Order #' . $isReady['po_number'].
                //     //                 ' has been set as completed, cannot check in here'
                //     // );
                //     // return $data;
                //     return false;
                // }

                return $check;
            }
        }
        elseif($check_status == 'completed') {
            $check = DB::table('package')
                     ->select('current_department', 'current_status', 'po_number', 'plan_ref_number')
                     ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
                     ->where('package.barcode_id', $barcodeid)
                     ->where('package_detail.factory_id', Auth::user()->factory_id)
                     ->where(function($query) {
                         $query->where('current_status', 'onprogress')
                               ->orWhere('current_status', 'rejected');
                     })
                     ->where('current_department', 'qcinspect')
                     ->whereNull('package.deleted_at')
                     ->whereNull('package_detail.deleted_at')
                     ->first();

            if(!$check) {
                return false;
            }
            else {
                return $check;
            }
        }
        elseif($check_status == 'rejected') {
            $check = DB::table('package')
                     ->select('current_department', 'current_status', 'po_number', 'plan_ref_number')
                     ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
                     ->where('package.barcode_id', $barcodeid)
                     ->where('package_detail.factory_id', Auth::user()->factory_id)
                     ->where(function($query) {
                         $query->where('current_status', 'onprogress')
                               ->orWhere('current_status', 'rejected');
                     })
                     ->where('current_department', 'qcinspect')
                     ->whereNull('package.deleted_at')
                     ->whereNull('package_detail.deleted_at')
                     ->first();

            if(!$check) {
                return false;
            }
            else {
                return $check;
            }
        }
        elseif($check_status == 'cancel') {
            $check = DB::table('package')
                     ->select('current_department', 'current_status', 'po_number', 'plan_ref_number')
                     ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
                     ->where('package.barcode_id', $barcodeid)
                     ->where('package_detail.factory_id', Auth::user()->factory_id)
                     ->where(function($query) {
                         $query->where('current_department', 'qcinspect')
                               ->where('current_status', 'rejected');
                     })
                     ->orWhere(function($query) {
                         $query->where('current_department', 'inventory')
                               ->where('current_status', 'rejected');
                     })
                     ->whereNull('package.deleted_at')
                     ->whereNull('package_detail.deleted_at')
                     ->first();

            if(!$check) {
                return false;
            }
            else {
                return $check;
            }
        }
        else {
            return false;
        }
    }

    //show error
    private function _show_error($barcodeid) {
        $error = '';
        $check_current_status = $this->_get_current_status($barcodeid);
        if($check_current_status) {
            if($check_current_status->current_status == 'onprogress') {
                $check_current_status->current_status = 'in';
            }
            elseif($check_current_status->current_status == 'completed') {
                $check_current_status->current_status = 'out';
            }
            $error .= 'Last Scanned (' . $check_current_status->barcode_id . ') : '
                        . $check_current_status->current_department . ' - '
                        . $check_current_status->current_status;
        }
        else {
            $error = 'Barcode not found on system';
        }

        return $error;
    }

    //get current status
    private function _get_current_status($barcodeid) {
        $data = DB::table('package')
                    ->select('barcode_id','current_department', 'current_status')
                    ->where('barcode_id', $barcodeid)
                    ->whereNull('package.deleted_at')
                    ->first();
        return $data;
    }

    //query movement
    private function _query_movement($barcodeid, $check_status, $type, $data_movement = null) {

        if($check_status == 'onprogress' || $check_status == 'rejected' || $check_status == 'completed') {

            try {
                DB::beginTransaction();

                //update deleted_at on the last package movement
                $last_data = DB::table('package_movements')
                            ->where('barcode_package', $barcodeid)
                            ->where('is_canceled', false)
                            ->whereNull('deleted_at')
                            ->first();
                if(!$last_data) {
                    throw new \Exception('Package not found');
                }
                else {
                    DB::table('package_movements')
                    ->where('id', $last_data->id)
                    ->update([
                        'deleted_at' => Carbon::now()
                    ]);
                }

                //insert movement history
                $insert = DB::table('package_movements')->insert($data_movement);

                if ($insert) {
                    // cek apakah insert double ?
                    $cek_insert = DB::table('package_movements')
                                    ->where('barcode_package', $barcodeid)
                                    ->where('is_canceled', false)
                                    ->whereNull('deleted_at')
                                    ->count();

                    if ($cek_insert > 1) {
                        throw new \Exception('data double inserted');
                    }
                }
                DB::commit();
            }
            catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return true;
        }
        elseif($check_status == 'cancel') {
            try {
                DB::beginTransaction();

                /*
                    - get second last data from table package_movements
                    - get last data from table package_movements
                    - update second last data, deleted_at = null
                    - update last data, deleted_at = now() and is_canceled = true
                */
                $second_last_data = DB::table('package_movements')
                                        ->where('barcode_package', $barcodeid)
                                        ->where('is_canceled', false)
                                        ->whereNotNull('deleted_at')
                                        ->orderBy('created_at', 'desc')
                                        ->first();

                $last_data = DB::table('package_movements')
                                 ->where('barcode_package', $barcodeid)
                                 ->where('is_canceled', false)
                                 ->whereNull('deleted_at')
                                 ->first();

                DB::table('package_movements')
                    ->where('id', $second_last_data->id)
                    ->update([
                        'deleted_at' => null
                    ]);

                DB::table('package_movements')
                    ->where('id', $last_data->id)
                    ->update([
                        'is_canceled' => true,
                        'deleted_at' => Carbon::now()
                    ]);

                DB::commit();
            }
            catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return true;
        }
        else {
            return false;
        }
    }

    //query package
    private function _query_package($barcodeid, $check_status, $type, $department, $status) {

        if($type == 'checkin') {
            /*
                update column 'status_qcinspect' = onprogress
            */
            $update_clause = array(
                'current_department' => $department,
                'current_status' => $status,
                'status_qcinspect' => 'onprogress',
                'updated_at' => Carbon::now()
            );
        }
        elseif($type == 'checkout') {
            /*
                jika status completed, update column 'status_qcinspect' = completed
                jika status rejected, update column 'status_qcinspect' = rejected
            */
            if($check_status == 'completed') {
                $update_clause = array(
                    'current_department' => $department,
                    'current_status' => $status,
                    'status_qcinspect' => 'completed',
                    'updated_at' => Carbon::now()
                );
            }
            elseif($check_status == 'rejected') {
                $update_clause = array(
                    'current_department' => $department,
                    'current_status' => $status,
                    'updated_at' => Carbon::now()
                );

                // if($department == 'sewing') {
                //     $update_clause['status_sewing'] = 'rejected';
                // }
                if($department == 'inventory') {
                    $update_clause['status_inventory'] = 'rejected';
                }
                elseif($department == 'qcinspect') {
                    $update_clause['status_qcinspect'] = 'rejected';
                }
            }
        elseif($check_status == 'cancel') {
                $update_clause = array(
                    'current_department' => $department,
                    'updated_at' => Carbon::now()
                );

                // if($department == 'sewing') {
                //     $update_clause['status_sewing'] = 'rejected';
                // }
                if($department == 'inventory') {
                    $update_clause['status_inventory'] = null;
                    $update_clause['current_status'] = 'completed';
                }
                elseif($department == 'qcinspect') {
                    $update_clause['status_qcinspect'] = null;
                    $update_clause['current_status'] = 'onprogress';
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }

        try {
            DB::beginTransaction();

            DB::table('package')
                ->where('barcode_id', $barcodeid)
                ->whereNull('deleted_at')
                ->update($update_clause);

            DB::commit();
        }
        catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }

}
