<?php

namespace App\Http\Controllers\Delivery;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

// use App\Export\DoExport;
// use Excel;


class DeliveryOrderController extends Controller
{
    public function index(){
    	return view('delivery/index');
    }

    public function getDataDO(Request $request){
    	$radio = $request->radio;
    	$stuffing = date_format(date_create($request->stuffing),'Y-m-d');
    	$delivery = $request->delivery;

    	$data = DB::table('delivery_order_new')->where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->where('is_draf',false);
    	if ($radio=='plan') {
    		$data = $data->where('plan_stuffing',$stuffing);
    	}else{
    		$data = $data->where('do_number',$delivery);
    	}

    	$data = $data->orderBy('do_number','asc');

    	return DataTables::of($data)
    						->editColumn('whs_id',function($data){
    							$whs = DB::table('master_warehouse')->where('id',$data->whs_id)->first();

    							return $whs->whs_name.' - '.$whs->address;
    						})
    						->addColumn('invoice',function($data){
    							$inv = DB::table('invoice')->where('do_number',$data->do_number)->whereNull('deleted_at')->groupBy('do_number')->select(DB::raw("string_agg(invoice.invoice, ', ') as invoice"))->first();
    							return $inv->invoice;
    						})
    						->addColumn('action',function($data){
    							$whs = DB::table('master_warehouse')->where('id',$data->whs_id)->first();
    							$getwhs = $whs->whs_name.' - '.$whs->address;
    							return '<button class="btn btn-success" 
    										data-id="'.$data->id.'"
    										data-do="'.$data->do_number.'"
    										data-driver="'.$data->driver.'"
    										data-nopol="'.$data->nopol.'"
    										data-whs ="'.$getwhs.'" 
    										onclick="_modalDO(this);"><span class="icon-printer2"></span></button>';
    						})
    						->make(true);
    }

    public function getdetailDO(Request $request){
    	$id = $request->id;
    	$do_number = $request->do;

    	
    	$data = DB::table('v_ns_draf_planload')
    					->join('invoice','v_ns_draf_planload.invoice','=','invoice.invoice')
    					->join('delivery_order_new','delivery_order_new.do_number','=','invoice.do_number')
    					->where('delivery_order_new.id',$id)
    					->whereNull('invoice.deleted_at')
    					->whereNull('delivery_order_new.deleted_at')
    					->select('v_ns_draf_planload.po_number','v_ns_draf_planload.customer_order_number','v_ns_draf_planload.ctn','v_ns_draf_planload.item_qty','v_ns_draf_planload.country_name','v_ns_draf_planload.style','delivery_order_new.whs_id');
    					;
    	return DataTables::of($data)
    						->addColumn('warehouse',function($data){
    							$whs = DB::table('master_warehouse')->where('id',$data->whs_id)->first();

    							return $whs->whs_name;
    						})
    						->rawColumns(['warehouse'])
    						->make(true);
    }

    public function exportDO(Request $request){
    	$id = $request->id;
    	$template = $request->template;
    	$factory = Auth::user()->factory_id;

        $maxrow = $request->maxrow;
    	
    	// return 'tes';
    	
    	$head = DB::table('delivery_order_new')
    				->join('master_warehouse','delivery_order_new.whs_id','=','master_warehouse.id')
    				->where('delivery_order_new.id',$id)->where('delivery_order_new.factory_id',$factory)->whereNull('delivery_order_new.deleted_at')->first();
		$get_invoice = DB::table('invoice')->where('do_number', $head->do_number)->where('factory_id', $factory)->pluck('invoice');
		$get_po_number = DB::table('po_stuffing')->whereIn('invoice', $get_invoice)->pluck('po_number');
    	$data = DB::table('ns_export_detaildo')
                        ->where('id',$id)
                        ->where('factory_id',$factory)
						->whereIn('po_number',$get_po_number)
                        ->get();
					// return response()->json($data);


        $fact = DB::table('factory')->where('factory_id',$factory)->first();

        $filename = $fact->factory_name."_DO_".$head->do_number;
        $legal = array(0, 0, 612.00, 1008.00);

    	$temp = "delivery.template.".$template;
    	$pdf = \PDF::loadView($temp,[
                                        'data'=>$data,
                                        'fact'=>$fact->address,
                                        'head'=>$head,
                                        'maxrow'=>$maxrow
                                    ])->setPaper($legal,'potrait');
        

    	return $pdf->stream($filename);

    	// return Excel::download(new DoExport($id,$factory_id,$template),$filename);

    }

}
