<?php
 
namespace App\Http\Controllers\Exim;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth; 
use DB;
use DataTables;
use Carbon\Carbon;
 
class EximController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function planload()
    {
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();
        
        $customers = DB::table('customers') 
                        ->select('id', 'customer_name', 'description_blade')
                        ->wherenull('deleted_at')
                        ->orderBy('id')
                        ->get();

        return view('exim/master_plan')->with(['factory' => $factory, 'customers' => $customers]);
    }

    // plan stuffing
    public function planstuffing()
    {
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        return view('exim/plan_stuffing')->with('factory', $factory);
    }

    // plan invoice
    public function planinvoice()
    {
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();
        
        $current_factory = DB::table('factory')
                            ->select('factory_id', 'factory_name', 'numbering_invoice', 'code_invoice', 'numbering_sj')
                            ->where('factory_id', Auth::user()->factory_id)
                            ->whereNull('deleted_at')
                            ->first();
        
        $invoice_start = $this->invoice_serial($current_factory->code_invoice, $current_factory->numbering_invoice);

        return view('exim/plan_invoice')->with([
                                                'factory' => $factory,
                                                'invoice_start' => $invoice_start
                                            ]);
    } 

    public function uploadexcelPlan(Request $request)
    {
        //
        $customer = $request->customer;
        $factory_id = null;

        if ($customer == '') {
            return response()->json('Customer not found', 422);
        }

        // gak jadi
        // if ($factory_id == '') {
        //     return response()->json('Factory not found', 422);
        // }

        if($request->hasFile('file_upload')){
            $extension = \File::extension($request->file_upload->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls") {
                $path = $request->file_upload->getRealPath();
                $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
                $headerRow = $datax->first()->keys()->toArray();
            }
        }

        // get template customer
        $data_customer = DB::table('customers') 
                            ->where('id', $customer)
                            ->whereNull('deleted_at')
                            ->first();
        
        $template_header = array_map('trim', explode(',', $data_customer->template_header_planload));

        // cek template apakah sudah sesuai
        // if($headerRow !== $template_header){
        //     return response()->json('Template not accepted', 422);
        // }

        $cek_header = count(array_intersect($template_header, $headerRow)) == count($template_header);

        if(!$cek_header){
            return response()->json('Template not accepted', 422);
        }

        $data_load = array();
        $data_movement = array();
        $data_history = array();
        $count_number = 0;

        foreach ($datax as $key => $value) {

             //check apakah kolom orderno, plan_ref ada valuenya tidak
            if ($value->orderno == '') {
                return response()->json('Kolom `orderno` harus terisi', 422);
            }
            
            if ($value->plan_ref == '') {
                return response()->json('Kolom `plan ref` harus terisi', 422);
            }

            // if ($value->sewing_place_update == '') {
            //     return response()->json('Kolom `sewing place update` harus terisi', 422);
            // }

            // if ($value->stuffing_place_update == '') {
            //     return response()->json('Kolom `stuffing place update` harus terisi', 422);
            // }

            // if ($value->sewing_place_update !== 'AOI1' && $value->sewing_place_update !== 'AOI2') {
            //     return response()->json('Kolom `sewing place update` Factory `AOI1 OR AOI2` tidak ditemukan', 422);
            // }

            // if ($value->stuffing_place_update !== 'AOI1' && $value->stuffing_place_update !== 'AOI2') {
            //     return response()->json('Kolom `stuffing place update` Factory `AOI1 OR AOI2` tidak ditemukan', 422);
            // }

            $count_number++;

            $plan_detail['orderno'] = trim($value->orderno);
            $plan_detail['plan_ref'] = trim($value->plan_ref);
            $plan_detail['old_po_re_route'] = $value->old_po_re_route;
            $plan_detail['sports_cats'] = $value->sports_cats;
            $plan_detail['product_type'] = $value->product_type;
            $plan_detail['age_group'] = $value->age_group;
            $plan_detail['gender'] = $value->gender;
            $plan_detail['mdd'] = $value->mdd;
            $plan_detail['priority_code'] = $value->priority_code;
            $plan_detail['subsidiary'] = $value->subsidiary;
            $plan_detail['whs_code'] = $value->whs_code;
            $plan_detail['shipmode'] = $value->shipmode;
            $plan_detail['fwd'] = $value->fwd;
            $plan_detail['psfd_1'] = $value->psfd1;
            $plan_detail['remarks_order'] = $value->remarks_order;
            $plan_detail['created_at'] = Carbon::now();
            $plan_detail['psfd_2'] = $value->psfd2;
            $plan_detail['customers_id'] = $customer;  
            $plan_detail['factory_id'] = $factory_id;  
            $plan_detail['pd'] = $value->pd;
            $plan_detail['remarks_psfd'] = $value->remarks_psfd;
            $data_plan_detail[] = $plan_detail;

            
            $plan_movement['orderno'] = trim($value->orderno);
            $plan_movement['plan_ref'] = trim($value->plan_ref);
            $plan_movement['user_id'] = Auth::user()->id;
            $plan_movement['created_at'] = Carbon::now();
            $data_movement[] = $plan_movement;

            // history
            $is_exist_load = DB::table('plan_load')
                        ->where('orderno', trim($value->orderno))
                        ->where('plan_ref', trim($value->plan_ref))
                        ->exists();

            if ($is_exist_load) {

                $get_data_load = DB::table('plan_load')
                        ->where('orderno', trim($value->orderno))
                        ->where('plan_ref', trim($value->plan_ref))
                        ->get();

                foreach ($get_data_load as $key1 => $value1) {
                        $d_history['orderno'] = $value1->orderno;
                        $d_history['poright'] = $value1->poright;
                        $d_history['plan_ref'] = $value1->plan_ref;
                        $d_history['old_po_re_route'] = $value1->old_po_re_route;
                        $d_history['sports_cats'] = $value1->sports_cats;
                        $d_history['product_type'] = $value1->product_type;
                        $d_history['age_group'] = $value1->age_group;
                        $d_history['gender'] = $value1->gender;
                        $d_history['mdd'] = $value1->mdd;
                        $d_history['mdd_new'] = $value1->mdd_new;
                        $d_history['priority_code'] = $value1->priority_code;
                        $d_history['subsidiary'] = $value1->subsidiary;
                        $d_history['whs_code'] = $value1->whs_code;
                        $d_history['shipmode'] = $value1->shipmode;
                        $d_history['fwd'] = $value1->fwd;
                        $d_history['psfd_1'] = $value1->psfd_1;
                        $d_history['gw'] = $value1->gw;
                        $d_history['nw'] = $value1->nw;
                        $d_history['ctn'] = $value1->ctn;
                        $d_history['qty'] = $value1->qty;
                        $d_history['invoice'] = $value1->invoice;
                        $d_history['so'] = $value1->so;
                        $d_history['plan_stuffing'] = $value1->plan_stuffing;
                        $d_history['actual_stuffing'] = $value1->actual_stuffing;
                        $d_history['remarks_order'] = $value1->remarks_order;
                        $d_history['qty_allocated'] = $value1->qty_allocated;
                        $d_history['is_integrate'] = $value1->is_integrate;
                        $d_history['integrate_date'] = $value1->integrate_date;
                        $d_history['is_stuffing'] = $value1->is_stuffing;
                        $d_history['created_at'] = $value1->created_at;
                        $d_history['updated_at'] = $value1->updated_at;
                        $d_history['deleted_at'] = Carbon::now();
                        $d_history['psfd_2'] = $value1->psfd_2;
                        $d_history['customers_id'] = $value1->customers_id;
                        $d_history['factory_id'] = $value1->factory_id;
                        $d_history['id'] = $value1->id;
                        $d_history['user_id'] = Auth::user()->id;
                        $d_history['pd'] = $value1->pd;
                        $d_history['remarks_psfd'] = $value1->remarks_psfd;
                        $data_history[] = $d_history;
                }
            }

        }


        try {
            DB::begintransaction();

            // update data if po / plan ref already exist
            foreach ($data_plan_detail as $key => $value) {
                
                $is_exist = DB::table('plan_load')
                            ->where('orderno', $value['orderno'])
                            ->where('plan_ref', $value['plan_ref'])
                            ->exists();
            
                
                if ($is_exist) {

                    // history plan load
                    DB::table('history_plan_load')->insert($data_history);
                    
                    // update master plan load
                    DB::table('plan_load')
                        ->where('orderno', $value['orderno'])
                        ->where('plan_ref', $value['plan_ref'])
                        ->update([
                                'orderno' => $value['orderno'],
                                'plan_ref' => $value['plan_ref'],
                                'old_po_re_route' => $value['old_po_re_route'],
                                'sports_cats' => $value['sports_cats'],
                                'product_type' => $value['product_type'],
                                'age_group' => $value['age_group'],
                                'gender' => $value['gender'],
                                'mdd' => $value['mdd'],
                                'priority_code' => $value['priority_code'],
                                'subsidiary' => $value['subsidiary'],
                                'whs_code' => $value['whs_code'],
                                'shipmode' => $value['shipmode'],
                                'fwd' => $value['fwd'],
                                'psfd_1' => $value['psfd_1'],
                                'remarks_order' => $value['remarks_order'],
                                'psfd_2' => $value['psfd_2'],
                                'customers_id' => $value['customers_id'],
                                'factory_id' => $value['factory_id'],
                                'pd' => $value['pd'],
                                'remarks_psfd' => $value['remarks_psfd'],
                                'updated_at' => Carbon::now()
                            ]);
                    
                         // update movement
                        DB::table('plan_load_movement')
                            ->where('orderno', $value['orderno'])
                            ->where('plan_ref', $value['plan_ref'])
                            ->whereNull('deleted_at')
                            ->update([
                                'deleted_at' => Carbon::now()
                            ]);

                }else {
                    // insert master plan load
                    DB::table('plan_load')->insert($data_plan_detail);
                }
            }

            // insert movement
            DB::table('plan_load_movement')->insert($data_movement);

            DB::commit();

        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

    }

    public function getplanload(Request $request)
    {
        $filterby = $request->filterby;

        $data = DB::table('plan_load_invoice')
                ->where('factory_id', Auth::user()->factory_id)
                ->whereNull('invoice')
                ->wherenull('deleted_at');
        
        if ($request->radio_status == 'stat') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range) {
                        $query->whereBetween('podd_check', [$range['from'], $range['to']]);
                    });

        }elseif ($request->radio_status == 'po_') {
            $data = $data->where('orderno', $request->po);
            
        }elseif ($request->radio_status == 'custno') {
            $data = $data->where('customer_number', $request->customer_number);

        }elseif ($request->radio_status == 'mdd') {
            $mdd = explode('-', preg_replace('/\s+/', '', $request->mdd));
            $range2 = array(
                'from' => date_format(date_create($mdd[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($mdd[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range2) {
                        $query->whereBetween('mdd', [$range2['from'], $range2['to']]);
                    });

        }elseif ($request->radio_status == 'lc') {
            $lc = explode('-', preg_replace('/\s+/', '', $request->lc));
            $range3 = array(
                'from' => date_format(date_create($lc[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($lc[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range3) {
                        $query->whereBetween('kst_lcdate', [$range3['from'], $range3['to']]);
                    });
        }

        // jika filterby tidak kosong
        if (!empty($filterby)) {
            $data = $data->where(function ($query) use ($filterby){
                        $query->where('orderno', 'like', '%'.$filterby.'%');
            });
        }

        $data = $data->orderby('podd_check');

        return Datatables::of($data)
                ->addColumn('checkbox', function ($data) {
                    return '<input type="checkbox" class="clplanref editor-active" name="selector[]" id="Inputselector" value="'.$data->id.'" data-po="'.$data->orderno.'" data-planrefnumber="'.$data->plan_ref.'" data-id="'.$data->id.'" data-invoice="'.$data->invoice.'" data-so="'.$data->so.'" data-planstuffing="'.$data->plan_stuffing.'" data-cbm="'.$data->cbm.'" data-mdd="'.$data->mdd.'" data-psfd1="'.$data->psfd_1.'" data-psfd2="'.$data->psfd_2.'"  data-statistical="'.$data->po_stat_date_adidas.'"  data-customer="'.$data->customer_number.'"  data-lc="'.$data->kst_lcdate.'" data-destination="'.$data->country_name.'" data-fwd="'.$data->fwd.'" data-shipmode="'.$data->shipmode.'" >';
                })
                ->editColumn('shipmode', function($data){
                    return '<div id="sp_'.$data->id.'">
                                <input type="text"
                                    data-id="'.$data->id.'"
                                    data-ponumber="'.$data->orderno.'"
                                    data-planrefnumber="'.$data->plan_ref.'"
                                    class="form-control sp_unfilled"
                                    value="'.$data->shipmode.'">
                                </input>
                            </div>';
                })
                ->editColumn('crd', function($data){
                    if (!empty($data->crd)) {
                        return Carbon::parse($data->crd)->format('Y-m-d');
                    }else{
                        return '';
                    }
                })
                ->editColumn('po_stat_date_adidas', function($data){
                    if (!empty($data->po_stat_date_adidas)) {
                        return Carbon::parse($data->po_stat_date_adidas)->format('Y-m-d');
                    }else{
                        return '';
                    }
                })
                // ->editColumn('mdd', function($data){

                    // if (empty($data->mdd)) {
                    //     $string = '<div id="mdd_'.$data->id.'">
                    //                 <input type="date"
                    //                     data-ponumber="'.$data->orderno.'"
                    //                     data-planrefnumber="'.$data->plan_ref_number.'"
                    //                     class="form-control mdd_unfilled"
                    //                     value="'.$data->mdd.'">
                    //                 </input>
                    //             </div>';
                    // }else {
                //    $string = Carbon::parse($data->mdd)->format('m/d/Y');
                        // $string = '<div id="mdd_'.$data->id.'">
                        //             <input type="date"
                        //                 data-id="'.$data->id.'"
                        //                 data-ponumber="'.$data->orderno.'"
                        //                 data-planrefnumber="'.$data->plan_ref.'"
                        //                 class="form-control mdd_unfilled"
                        //                 value="'.$data->mdd.'">
                        //             </input>
                        //         </div>';
                    // }
                //     return $string;
                // })
                // ->editColumn('remarks_order', function($data){
                //     return '<div id="ro_'.$data->id.'">
                //                 <input type="text"
                //                     data-id="'.$data->id.'"
                //                     data-ponumber="'.$data->orderno.'"
                //                     data-planrefnumber="'.$data->plan_ref.'"
                //                     class="form-control ro_unfilled"
                //                     value="'.$data->remarks_order.'">
                //                 </input>
                //             </div>';
                // })
                ->addColumn('remarks',function($data){
                    return '<input type="button" name="remarks" id="remarks" value="Remarks" class="btn btn-warning" data-orderno="'.$data->orderno.'" data-planref="'.$data->plan_ref.'"  onclick="remarks(this);">';


                })
                ->editColumn('psfd_1', function($data){
                    return '<div id="psfd_1_'.$data->id.'">
                                <input type="date"
                                    data-id="'.$data->id.'"
                                    data-ponumber="'.$data->orderno.'"
                                    data-planrefnumber="'.$data->plan_ref.'"
                                    class="form-control psfd_1_unfilled"
                                    value="'.$data->psfd_1.'">
                                </input>
                            </div>';
                })
                ->editColumn('psfd_2', function($data){
                    return '<div id="psfd_2_'.$data->id.'">
                                <input type="date"
                                    data-id="'.$data->id.'"
                                    data-ponumber="'.$data->orderno.'"
                                    data-planrefnumber="'.$data->plan_ref.'"
                                    class="form-control psfd_2_unfilled"
                                    value="'.$data->psfd_2.'">
                                </input>
                            </div>';
                })
                ->addColumn('status', function($data){
                    $is_exists = DB::table('po_summary')
                                    ->where('po_number', $data->orderno)
                                    ->whereNull('deleted_at')
                                    ->exists();

                    if ($is_exists) {
                        return '<span class="badge badge-success position-right">Uploaded</span>';
                    }else{
                        return '<span class="badge badge-default position-right">Waiting Upload</span>';
                    }
                })
                ->addColumn('counter', function($data){
                    return '0';
                })
                ->rawColumns(['action', 'checkbox', 'shipmode', 'remarks', 'psfd_1', 'psfd_2', 'status', 'counter'])
                ->make(true);
    }

    public function exportplanload(Request $request)
    {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;

        $current_date = date_format(Carbon::now(), 'd/m/Y');

        $data = DB::table('plan_load')
                ->wherenull('deleted_at');

        // jika filterby tidak kosong
        if (!empty($filterby)) {
            $data = $data->where(function ($query) use ($filterby){
                        $query->where('orderno', 'like', '%'.$filterby.'%');
            });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('crd');
        }

        // $data = $data->orderby('crd');
        $filename = 'plan_load_' . $filterby;

        $i = 1;

        $export = \Excel::create($filename, function ($excel) use ($data, $i, $current_date){
            $excel->sheet('report', function($sheet) use ($data, $i, $current_date){
                $sheet->appendRow(array(
                    'LC Date', 'Orderno', 'Plan Reff', 'Style', 'CRD', 'Destination', 'Ship Mode', 'FWD', 'GW', 'NW', 'CBM', 'CTN', 'QTY'
                ));
                $data->chunk(100, function ($rows) use ($sheet, $i, $current_date){
                    foreach ($rows as $row) {
                        $sheet->appendRow(array(
                            $row->lc_date, $row->orderno, $row->plan_ref, $row->style, $row->crd, $row->destination, $row->shipmode, $row->fwd, $row->gw, $row->nw, $row->cbm, $row->ctn, $row->qty
                        ));
                    }
                });
            });
        })->download('xlsx');
    }

    // sync plan load to table plan invoice
    static function planloadexim()
    {
        $get_plan_load = DB::table('v_plan_load_invoice')
                            ->whereNull('deleted_at')
                            ->get();
        
        try {
            
            DB::beginTransaction();
                foreach ($get_plan_load as $key => $value) {
                    $exist_plan_invoice = DB::table('plan_load_invoice')
                                            ->where('orderno', $value->orderno)
                                            ->where('plan_ref', $value->plan_ref)
                                            ->exists();
                    if ($exist_plan_invoice) {
                        DB::table('plan_load_invoice')
                            ->where('orderno', $value->orderno)
                            ->where('plan_ref', $value->plan_ref)
                            ->update([
                                    'poright' => $value->poright,
                                    'old_po_re_route' => $value->old_po_re_route,
                                    'sports_cats' => $value->sports_cats,
                                    'product_type' => $value->product_type,
                                    'age_group' => $value->age_group,
                                    'gender' => $value->gender,
                                    'mdd' => $value->mdd,
                                    'mdd_new' => $value->mdd_new,
                                    'priority_code' => $value->priority_code,
                                    'subsidiary' => $value->subsidiary,
                                    'whs_code' => $value->whs_code,
                                    'shipmode' => $value->shipmode,
                                    'fwd' => $value->fwd,
                                    'psfd_1' => $value->psfd_1,
                                    'gw' => $value->gw,
                                    'nw' => $value->nw,
                                    'ctn' => $value->ctn,
                                    'qty' => $value->qty,
                                    'invoice' => $value->invoice,
                                    'so' => $value->so,
                                    'plan_stuffing' => $value->plan_stuffing,
                                    'actual_stuffing' => $value->actual_stuffing,
                                    'remarks_order' => $value->remarks_order,
                                    'qty_allocated' => $value->qty_allocated,
                                    'is_integrate' => $value->is_integrate,
                                    'integrate_date' => $value->integrate_date,
                                    'is_stuffing' => $value->is_stuffing,
                                    'created_at' => $value->created_at,
                                    'updated_at' => $value->updated_at,
                                    'deleted_at' => $value->deleted_at,
                                    'psfd_2' => $value->psfd_2,
                                    'customers_id' => $value->customers_id,
                                    'id' => $value->id,
                                    'factory_id' => $value->factory_id,
                                    'total_ctn' => $value->total_ctn,
                                    'total_inner_pack' => $value->total_inner_pack,
                                    'total_item_qty' => $value->total_item_qty,
                                    'ctn_count' => $value->ctn_count,
                                    'total_net_net' => $value->total_net_net,
                                    'total_net' => $value->total_net,
                                    'total_gross' => $value->total_gross,
                                    'cbm' => $value->cbm,
                                    'po_number' => $value->po_number,
                                    'job' => $value->job,
                                    'article' => $value->article,
                                    'season' => $value->season,
                                    'style' => $value->style,
                                    'color' => $value->color,
                                    'new_qty' => $value->new_qty,
                                    'crd' => $value->crd,
                                    'po_stat_date_adidas' => $value->po_stat_date_adidas,
                                    'podd_check' => $value->podd_check,
                                    'customer_order_number' => $value->customer_order_number,
                                    'customer_number' => $value->customer_number,
                                    'country_name' => $value->country_name,
                                    'sewing_place_update' => $value->sewing_place_update,
                                    'stuffing_place_update' => $value->stuffing_place_update,
                                    'remark' => $value->remark,
                                    'order_type' => $value->order_type,
                                    'kst_lcdate' => $value->kst_lcdate,
                                    'document_type' => $value->document_type,
                                    'pd' => $value->pd,
                                    'remarks_exim' => $value->remarks_exim,
                                    'remarks_psfd' => $value->remarks_psfd,
                                    'date_sync' => Carbon::now(),
                                    'user_sync' => Auth::user()->id
                            ]);
                    }else{
                        DB::table('plan_load_invoice')
                            ->insert([
                                    'orderno' => $value->orderno,
                                    'poright' => $value->poright,
                                    'plan_ref' => $value->plan_ref,
                                    'old_po_re_route' => $value->old_po_re_route,
                                    'sports_cats' => $value->sports_cats,
                                    'product_type' => $value->product_type,
                                    'age_group' => $value->age_group,
                                    'gender' => $value->gender,
                                    'mdd' => $value->mdd,
                                    'mdd_new' => $value->mdd_new,
                                    'priority_code' => $value->priority_code,
                                    'subsidiary' => $value->subsidiary,
                                    'whs_code' => $value->whs_code,
                                    'shipmode' => $value->shipmode,
                                    'fwd' => $value->fwd,
                                    'psfd_1' => $value->psfd_1,
                                    'gw' => $value->gw,
                                    'nw' => $value->nw,
                                    'ctn' => $value->ctn,
                                    'qty' => $value->qty,
                                    'invoice' => $value->invoice,
                                    'so' => $value->so,
                                    'plan_stuffing' => $value->plan_stuffing,
                                    'actual_stuffing' => $value->actual_stuffing,
                                    'remarks_order' => $value->remarks_order,
                                    'qty_allocated' => $value->qty_allocated,
                                    'is_integrate' => $value->is_integrate,
                                    'integrate_date' => $value->integrate_date,
                                    'is_stuffing' => $value->is_stuffing,
                                    'created_at' => $value->created_at,
                                    'updated_at' => $value->updated_at,
                                    'deleted_at' => $value->deleted_at,
                                    'psfd_2' => $value->psfd_2,
                                    'customers_id' => $value->customers_id,
                                    'id' => $value->id,
                                    'factory_id' => $value->factory_id,
                                    'total_ctn' => $value->total_ctn,
                                    'total_inner_pack' => $value->total_inner_pack,
                                    'total_item_qty' => $value->total_item_qty,
                                    'ctn_count' => $value->ctn_count,
                                    'total_net_net' => $value->total_net_net,
                                    'total_net' => $value->total_net,
                                    'total_gross' => $value->total_gross,
                                    'cbm' => $value->cbm,
                                    'po_number' => $value->po_number,
                                    'job' => $value->job,
                                    'article' => $value->article,
                                    'season' => $value->season,
                                    'style' => $value->style,
                                    'color' => $value->color,
                                    'new_qty' => $value->new_qty,
                                    'crd' => $value->crd,
                                    'po_stat_date_adidas' => $value->po_stat_date_adidas,
                                    'podd_check' => $value->podd_check,
                                    'customer_order_number' => $value->customer_order_number,
                                    'customer_number' => $value->customer_number,
                                    'country_name' => $value->country_name,
                                    'sewing_place_update' => $value->sewing_place_update,
                                    'stuffing_place_update' => $value->stuffing_place_update,
                                    'remark' => $value->remark,
                                    'order_type' => $value->order_type,
                                    'kst_lcdate' => $value->kst_lcdate,
                                    'document_type' => $value->document_type,
                                    'pd' => $value->pd,
                                    'remarks_exim' => $value->remarks_exim,
                                    'remarks_psfd' => $value->remarks_psfd,
                                    'date_sync' => Carbon::now(),
                                    'user_sync' => Auth::user()->id
                            ]);
                    }
                }
            DB::commit();
        } 
        catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json('transfer successfully..!');
    }
    // 

    public function getplanloadajax(Request $request)
    {
        $data = DB::table('plan_load_invoice')
                ->select('customer_number', 'mdd', 'kst_lcdate')
                ->where('factory_id', Auth::user()->factory_id)
                ->whereNull('invoice')
                ->wherenull('deleted_at');
        
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = $data->where(function($query) use ($range) {
                    $query->whereBetween('podd_check', [$range['from'], $range['to']]);
        });

        $data = $data->get()->toArray();

        $data_cust = array();

        //$result = array();
        foreach ($data as $element) {
            $data_cust[$element->customer_number][] = $element->customer_number;
        }
        ksort($data_cust);

        return response()->json(['data_cust' => $data_cust], 200);
    }

    public function getplanloadajaxmdd(Request $request)
    {
        $data = DB::table('plan_load_invoice')
                ->select('customer_number', 'mdd', 'kst_lcdate')
                ->where('factory_id', Auth::user()->factory_id)
                ->whereNull('invoice')
                ->wherenull('deleted_at');
        
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = $data->where(function($query) use ($range) {
                    $query->whereBetween('podd_check', [$range['from'], $range['to']]);
        });

        if ($request->search_cust != '') {
            $data = $data->where('customer_number', trim($request->search_cust));
        }

        $data = $data->get()->toArray();

        $data_mdd = array();

        //$result = array();
        foreach ($data as $element) {
            $data_mdd[$element->mdd][] = $element->mdd;
        }
        ksort($data_mdd);

        return response()->json(['data_mdd' => $data_mdd], 200);
    }

    public function getplanloadajaxlc(Request $request)
    {
        $data = DB::table('plan_load_invoice')
                ->select('customer_number', 'mdd', 'kst_lcdate')
                ->where('factory_id', Auth::user()->factory_id)
                ->whereNull('invoice')
                ->wherenull('deleted_at');
        
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = $data->where(function($query) use ($range) {
                    $query->whereBetween('podd_check', [$range['from'], $range['to']]);
        });

        if ($request->search_cust != '' || $request->search_mdd != '') {
            $data = $data->where('customer_number', $request->search_cust)
                        ->where('mdd', $request->search_mdd);
        }

        $data = $data->get()->toArray();

        $data_lc = array();

        //$result = array();
        foreach ($data as $element) {
            $data_lc[$element->kst_lcdate][] = $element->kst_lcdate;
        }
        ksort($data_lc);

        return response()->json(['data_lc' => $data_lc], 200);
    }

    public function getplanloadajaxpriority(Request $request)
    {
        $data = DB::table('plan_load_invoice')
                ->select('customer_number', 'mdd', 'kst_lcdate', 'priority_code', 'whs_code')
                // ->where('factory_id', Auth::user()->factory_id)
                ->whereNull('invoice')
                ->wherenull('deleted_at');
        
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = $data->where(function($query) use ($range) {
                    $query->whereBetween('podd_check', [$range['from'], $range['to']]);
        });

        if ($request->search_cust != '' || $request->search_mdd != '') {
            $data = $data->where('customer_number', trim($request->search_cust))
                        ->where('mdd', carbon::parse($request->search_mdd)->format('Y-m-d'))
                        ->where('psfd_1', carbon::parse($request->search_psfd)->format('Y-m-d'));
        }

        $data = $data->get()->toArray();

        $data_priority = array();

        //$result = array();
        foreach ($data as $element) {
            $data_priority[$element->priority_code][] = $element->priority_code;
        }
        ksort($data_priority);

        return response()->json(['data_priority' => $data_priority], 200);
    }

    public function getplanloadajaxpsfd(Request $request)
    {
        $data = DB::table('plan_load_invoice')
                ->select('customer_number', 'mdd', 'kst_lcdate', 'priority_code', 'whs_code','psfd_1')
                // ->where('factory_id', Auth::user()->factory_id)
                ->whereNull('invoice')
                ->wherenull('deleted_at');
        
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = $data->where(function($query) use ($range) {
                    $query->whereBetween('podd_check', [$range['from'], $range['to']]);
        });

        if ($request->search_cust != '' || $request->search_mdd != '') {
            $data = $data->where('customer_number', trim($request->search_cust))
                        ->where('mdd', carbon::parse($request->search_mdd)->format('Y-m-d'));
        }

        $data = $data->get()->toArray();

        $data_psfd = array();

        //$result = array();
        foreach ($data as $element) {
       
            $data_psfd[$element->psfd_1][] = $element->psfd_1;
        }
        ksort($data_psfd);

        return response()->json(['data_psfd' => $data_psfd], 200);
    }

    public function getplanloadajaxwhs(Request $request)
    {
        $data = DB::table('plan_load_invoice')
                ->select('customer_number', 'mdd', 'kst_lcdate', 'priority_code', 'whs_code')
                ->where('factory_id', Auth::user()->factory_id)
                ->whereNull('invoice')
                ->wherenull('deleted_at');
        
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = $data->where(function($query) use ($range) {
                    $query->whereBetween('podd_check', [$range['from'], $range['to']]);
        });

        if ($request->search_cust != '' || $request->search_mdd != '' || $request->search_priority) {
            $data = $data->where('customer_number', trim($request->search_cust))
                        ->where('mdd', carbon::parse($request->search_mdd)->format('Y-m-d'))
                        ->where('priority_code', trim($request->search_priority));
        }

        $data = $data->get()->toArray();

        $data_whs = array();

        //$result = array();
        foreach ($data as $element) {
            $data_whs[$element->whs_code][] = $element->whs_code;
        }
        ksort($data_whs);

        return response()->json(['data_whs' => $data_whs], 200);
    }

    // filtering delivery order
    public function getgroupinvoiceajaxplan(Request $request)
    {
        $data = DB::table('v_plan_load_current_shipment_2')
                ->select('plan_stuffing', 'shipmode', 'fwd', 'destination')
                ->where('factory_id', Auth::user()->factory_id)
                ->where('is_delivery', false)
                ->wherenull('deleted_at');
        
        // $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        // $range = array(
        //     'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
        //     'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        // );

        // $data = $data->where(function($query) use ($range) {
        //             $query->whereBetween('plan_stuffing', [$range['from'], $range['to']]);
        // });

        $data = $data->get()->toArray();

        $data_plan = array();

        //$result = array();
        foreach ($data as $element) {
            $data_plan[$element->plan_stuffing][] = $element->plan_stuffing;
        }
        ksort($data_plan);

        return response()->json(['data_plan' => $data_plan], 200);
    }

    public function getgroupinvoiceajaxshipmode(Request $request)
    {
        $data = DB::table('v_plan_load_current_shipment_2')
                ->select('plan_stuffing', 'shipmode', 'fwd', 'destination')
                ->where('factory_id', Auth::user()->factory_id)
                ->where('is_delivery', false)
                ->wherenull('deleted_at');
        
        if (!empty($request->plan_stuffing)) {

            $data = $data->where('plan_stuffing', Carbon::parse($request->plan_stuffing)->format('Y-m-d'));
        }

        $data = $data->get()->toArray();

        $data_shipmode = array();

        //$result = array();
        foreach ($data as $element) {
            $data_shipmode[$element->shipmode][] = $element->shipmode;
        }
        ksort($data_shipmode);

        return response()->json(['data_shipmode' => $data_shipmode], 200);
    }

    public function getgroupinvoiceajaxfwd(Request $request)
    {
        $data = DB::table('v_plan_load_current_shipment_2')
                ->select('plan_stuffing', 'shipmode', 'fwd', 'destination')
                ->where('factory_id', Auth::user()->factory_id)
                ->where('is_delivery', false)
                ->wherenull('deleted_at');
        
        if (!empty($request->plan_stuffing)) {

            $data = $data->where('plan_stuffing', Carbon::parse($request->plan_stuffing)->format('Y-m-d'));
        }

        if (!empty($request->shipmode)) {
            $data = $data->where('shipmode', trim($request->shipmode));
        }

        $data = $data->get()->toArray();

        $data_fwd = array();

        //$result = array();
        foreach ($data as $element) {
            $data_fwd[$element->fwd][] = $element->fwd;
        }
        ksort($data_fwd);

        return response()->json(['data_fwd' => $data_fwd], 200);
    }

    public function getgroupinvoiceajaxdestination(Request $request)
    {
        $data = DB::table('v_plan_load_current_shipment_2')
                ->select('plan_stuffing', 'shipmode', 'fwd', 'destination')
                ->where('factory_id', Auth::user()->factory_id)
                ->where('is_delivery', false)
                ->wherenull('deleted_at');
        
        if (!empty($request->plan_stuffing)) {

            $data = $data->where('plan_stuffing', Carbon::parse($request->plan_stuffing)->format('Y-m-d'));
        }

        if (!empty($request->shipmode)) {
            $data = $data->where('shipmode', trim($request->shipmode));
        }

        if (!empty($request->fwd)) {
            $data = $data->where('fwd', trim($request->fwd));
        }

        $data = $data->get()->toArray();

        $data_destination = array();

        //$result = array();
        foreach ($data as $element) {
            $data_destination[$element->destination][] = $element->destination;
        }
        ksort($data_destination);

        return response()->json(['data_destination' => $data_destination], 200);
    }

    // update plan
    public function updatePlanLoad(Request $request)
    {   
        $action = $request->action;
        $data = $request->data;
        
        $data_update = array();
        $data_header = array();

        $request->validate([
            'invoice' => 'required',
            'plan_stuffing' => 'required',
            'so' => 'required',
            'customer_number_hide' => 'required',
            'fwd' => 'required',
            'shipmode' => 'required',
            'is_cancel_invoice' => 'required',
        ]);

        // invoice
        $invoice =  $request->invoice; //for database

        // plan stuffing
        $date_plan = date_format(date_create($request->plan_stuffing), 'Y-m-d');
        $current_date = date_format(Carbon::now(), 'Y-m-d');

       // dilepas dulu
        // if ($date_plan < $current_date) {
        //     return response()->json('plan stuffing must be more than with current date', 422);
        // }
        
        $plan_stuffing =   $date_plan; //for database

        // so (shipment order)
        $so =  $request->so; //for database

        $remark_co = $request->remark_co;

        //header invoice
        $headers['invoice'] = $invoice;
        $headers['plan_stuffing'] = $plan_stuffing;
        $headers['so'] = $so;
        $headers['remark_container'] = $remark_co;
        $headers['created_at'] = Carbon::now();
        $headers['mdd'] = $request->mdd;
        $headers['kst_statisticaldate'] = $request->kst_statisticaldate;
        $headers['customer_number'] = $request->customer_number_hide;
        $headers['kst_lcdate'] = $request->kst_lcdate;
        $headers['fwd'] = $request->fwd;
        $headers['destination'] = $request->destination;
        $headers['shipmode'] = $request->shipmode;
        $headers['factory_id'] = Auth::user()->factory_id;
        $headers['user_id'] = Auth::user()->id;
        $headers['cbm'] = $request->cbm;
        $data_header = $headers;

        //detail
        $plans['invoice'] = $invoice;
        $plans['plan_stuffing'] = $plan_stuffing;
        $plans['so'] = $so;
        $plans['updated_at'] = Carbon::now();
        $data_update = $plans;

        $current_factory = DB::table('factory')
                            ->select('factory_id', 'factory_name', 'numbering_invoice', 'code_invoice', 'numbering_sj')
                            ->where('factory_id', Auth::user()->factory_id)
                            ->whereNull('deleted_at')
                            ->first();
        
          // pisah angka
          $pattern = '/([^0-9]+)/';
          $numbering_start_update = preg_replace($pattern,'',$invoice);
        
//        $numbering_start = $current_factory->numbering_invoice;

        //update
        try {
            // insert invoices
            DB::table('invoices')->insert($data_header);

            // update plan load
            foreach ($data as $key => $value) {

                DB::table('plan_load')
                        ->where('id', $value['id'])
                        ->whereNull('deleted_at')
                        ->update($data_update);
                
                DB::table('plan_load_invoice')
                        ->where('id', $value['id'])
                        ->whereNull('deleted_at')
                        ->update($data_update);
            }

            
            // jika dari invoice cancel
            if ($request->is_cancel_invoice == 1) {

                DB::table('cancel_invoices')
                        ->where('invoice', $invoice)
                        ->update([
                            'is_used' => true
                        ]);
            }else{
                
                // update numbering invoice factory
                DB::table('factory')
                ->where('factory_id', Auth::user()->factory_id)
                ->whereNull('deleted_at')
                ->update([
                    'numbering_invoice' => (int)$numbering_start_update+1
                ]);

            }

        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }

    }

    // update shipmode
    public function updateShipmode(Request $request)
    {
        $id = $request->id;
        $po_number = $request->po_number;
        $plan_ref_number = $request->plan_ref_number;

        if(empty($id)) {
            $shipmode = null;
        }
        else {
            $shipmode =  $request->shipmode;  //for database
        }

        //update
        try {
            DB::beginTransaction();
                DB::table('plan_load')
                    ->where('id', $id)
                    ->whereNull('deleted_at')
                    ->update([
                        'shipmode' => $shipmode
                    ]);
                
                DB::table('plan_load_invoice')
                    ->where('id', $id)
                    ->whereNull('deleted_at')
                    ->update([
                        'shipmode' => $shipmode
                    ]);
            DB::commit();
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }


        $html = '<input type="text"
                      data-id="'.$id.'"
                      data-ponumber="'.$po_number.'"
                      data-planrefnumber="'.$plan_ref_number.'"
                      class="form-control sp_unfilled"
                      value="'.$shipmode.'">
                  </input>';

        return response()->json($html, 200);
    }

    // update MDD
    public function updateMdd(Request $request)
    {
        $id = $request->id;
        $po_number = $request->po_number;
        $plan_ref_number = $request->plan_ref_number;

        if(empty($id)) {
            $mdd = null;
        }
        else {
            $mdd = Carbon::parse($request->mdd)->format('Y-m-d');  //for database
        }

        //update
        try {
            DB::beginTransaction();
                DB::table('plan_load')
                    ->where('id', $id)
                    ->whereNull('deleted_at')
                    ->update([
                        'mdd' => $mdd
                    ]);
                
                DB::table('plan_load_invoice')
                    ->where('id', $id)
                    ->whereNull('deleted_at')
                    ->update([
                        'mdd' => $mdd
                    ]);
            DB::commit();
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }


        $html = '<input type="date"
                      data-id="'.$id.'"
                      data-ponumber="'.$po_number.'"
                      data-planrefnumber="'.$plan_ref_number.'"
                      class="form-control mdd_unfilled"
                      value="'.$mdd.'">
                  </input>';

        return response()->json($html, 200);
    }

    // update remarks order
    public function updateRemark(Request $request)
    {
        $id = $request->id;
        $po_number = $request->po_number;
        $plan_ref_number = $request->plan_ref_number;

        if(empty($id)) {
            $remarks_order = null;
        }
        else {
            $remarks_order = $request->remarks_order;  //for database
        }

        //update
        try {
            DB::beginTransaction();
                
                DB::table('plan_load')
                    ->where('id', $id)
                    ->whereNull('deleted_at')
                    ->update([
                        'remarks_order' => $remarks_order
                    ]);
                
                DB::table('plan_load_invoice')
                ->where('id', $id)
                ->whereNull('deleted_at')
                ->update([
                    'remarks_order' => $remarks_order
                ]);

            DB::commit();
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }


        $html = '<input type="text"
                      data-id="'.$id.'"
                      data-ponumber="'.$po_number.'"
                      data-planrefnumber="'.$plan_ref_number.'"
                      class="form-control ro_unfilled"
                      value="'.$remarks_order.'">
                  </input>';

        return response()->json($html, 200);
    }

    // update psfd_1
    public function updatePsfdFrom(Request $request)
    {
        $id = $request->id;
        $po_number = $request->po_number;
        $plan_ref_number = $request->plan_ref_number;

        if(empty($id)) {
            $psfd_1 = null;
        }
        else {
            $psfd_1 = Carbon::parse($request->psfd_1)->format('Y-m-d');  //for database
        }

        //update
        try {
            DB::beginTransaction();
                DB::table('plan_load')
                    ->where('id', $id)
                    ->whereNull('deleted_at')
                    ->update([
                        'psfd_1' => $psfd_1
                    ]);

                DB::table('plan_load_invoice')
                    ->where('id', $id)
                    ->whereNull('deleted_at')
                    ->update([
                        'psfd_1' => $psfd_1
                    ]);

            DB::commit();
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }


        $html = '<input type="date"
                      data-id="'.$id.'"
                      data-ponumber="'.$po_number.'"
                      data-planrefnumber="'.$plan_ref_number.'"
                      class="form-control psfd_1_unfilled"
                      value="'.$psfd_1.'">
                  </input>';

        return response()->json($html, 200);
    }

    // update psfd_2
    public function updatePsfdTo(Request $request)
    {
        $id = $request->id;
        $po_number = $request->po_number;
        $plan_ref_number = $request->plan_ref_number;

        if(empty($id)) {
            $psfd_2 = null;
        }
        else {
            $psfd_2 = Carbon::parse($request->psfd_2)->format('Y-m-d');  //for database
        }

        //update
        try {
            DB::beginTransaction();

                DB::table('plan_load')
                    ->where('id', $id)
                    ->whereNull('deleted_at')
                    ->update([
                        'psfd_2' => $psfd_2
                    ]);
                
                DB::table('plan_load_invoice')
                    ->where('id', $id)
                    ->whereNull('deleted_at')
                    ->update([
                        'psfd_2' => $psfd_2
                    ]);

            DB::commit();
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }


        $html = '<input type="date"
                      data-id="'.$id.'"
                      data-ponumber="'.$po_number.'"
                      data-planrefnumber="'.$plan_ref_number.'"
                      class="form-control psfd_2_unfilled"
                      value="'.$psfd_2.'">
                  </input>';

        return response()->json($html, 200);
    }

    // plan stuffing
    public function getDataPlanStuffing(Request $request)
    {
        $filterby = $request->filterby;
        $radio_status = $request->radio_status;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('plan_load')
                ->wherenull('deleted_at');
        
        if ($radio_status == 'date') {
            $data = $data->where(function($query) use ($range){
                $query->whereBetween('plan_stuffing', [$range['from'], $range['to']]);
            });
        }elseif ($radio_status == 'invoice') {
            $data = $data->where('invoice', $request->invoice);
        }elseif ($radio_status == 'so') {
            $data = $data->where('so', $request->so);
        }

        // jika filterby tidak kosong
        if (!empty($filterby)) {
            $data = $data->where(function ($query) use ($filterby){
                        $query->where('orderno', 'like', '%'.$filterby.'%');
            });
        }

        $data = $data->orderby('invoice', 'asc');

        return Datatables::of($data)
                ->make(true);
    }

    public function exportplanstuffing(Request $request)
    {
        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::table('plan_load')
                ->wherenull('deleted_at');
        
        if ($radio_status == 'date') {
            $data = $data->where(function($query) use ($range){
                $query->whereBetween('plan_stuffing', [$range['from'], $range['to']]);
            });
        }elseif ($radio_status == 'invoice') {
            $data = $data->where('invoice', $request->invoice);
        }elseif ($radio_status == 'so') {
            $data = $data->where('so', $request->so);
        }

        // jika filterby tidak kosong
        if (!empty($filterby)) {
            $data = $data->where(function ($query) use ($filterby){
                        $query->where('orderno', 'like', '%'.$filterby.'%');
            });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('invoice');
        }

        $filename = 'plan_stuffing_' . $filterby;

        $i = 1;

        $export = \Excel::create($filename, function ($excel) use ($data, $i, $current_date){
            $excel->sheet('report', function($sheet) use ($data, $i, $current_date){
                $sheet->appendRow(array(
                    'Plan', 'Invoice', 'SO', 'Orderno', 'Plan Reff', 'Style', 'CRD', 'Destination', 'Ship Mode', 'FWD', 'GW', 'NW', 'CBM', 'CTN', 'QTY'
                ));
                $data->chunk(100, function ($rows) use ($sheet, $i, $current_date){
                    foreach ($rows as $row) {
                        $sheet->appendRow(array(
                            $row->plan_stuffing, $row->invoice, $row->so, $row->orderno, $row->plan_ref, $row->style, $row->crd, $row->destination, $row->shipmode, $row->fwd, $row->gw, $row->nw, $row->cbm, $row->ctn, $row->qty
                        ));
                    }
                });
            });
        })->download('xlsx');
    }

    //view template header
    public function viewTemplateHeader(Request $request) {
        $template = $request->template;
        $template_blade = $request->template_blade;

        $data = DB::table('customers')
                            ->where('id', $template)
                            ->whereNull('deleted_at')
                            ->first();
        
        $template = explode(',', $data->template_header_planload);

        return view('exim/template_header/header_plan')->with('template', $template);
    }


    //export template header
    public function viewTemplateHeaderExport(Request $request) {
        $template = $request->template;
        
        if($template == ''){
            return response()->json('Template not found', 422);
        }

        $array_template = DB::table('customers')
                            ->where('id', $template)
                            ->whereNull('deleted_at')
                            ->first();
        
        $filename = 'template_planload_'.$array_template->customer_name;
        $template_header = explode(',', $array_template->template_header_planload);
        
        $i=1;

        $export = \Excel::create($filename, function($excel) use ($template_header, $i, $array_template) {
            $excel->sheet('template', function($sheet) use($template_header, $i, $array_template) {
                $sheet->appendRow($template_header);
              
            });
        })->download('xlsx');


        return response()->json('Success exporting', 200);
        
    }

    // group invoice
    public function groupinvoice()
    {
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();
        
        $current_factory = DB::table('factory')
                            ->select('factory_id', 'factory_name', 'numbering_invoice', 'code_invoice', 'numbering_sj')
                            ->where('factory_id', Auth::user()->factory_id)
                            ->whereNull('deleted_at')
                            ->first();
        
        $sj_start = $this->sj_serial($current_factory->numbering_sj,$current_factory->code_invoice);

        return view('exim/group_invoice')->with([
                                                'factory' => $factory,
                                                'sj_start' => $sj_start
                                            ]);
    }

    public function getinvoice(Request $request)
    {
        $filterby = $request->filterby;

        $data = DB::table('v_plan_load_current_shipment_2')
                ->where('factory_id', Auth::user()->factory_id)
                ->where('is_delivery', false)
                ->wherenull('deleted_at');
        
        if ($request->radio_status == 'fw') {
            $data = $data->where('fwd', 'like', '%'.trim($request->fwd).'%');
        }elseif ($request->radio_status == 'ship') {
            $data = $data->where('shipmode', 'like', '%'.trim($request->shipmode).'%');
        }

        // jika filterby tidak kosong
        if (!empty($filterby)) {
            $data = $data->where(function ($query) use ($filterby){
                        $query->where('invoice', 'like', '%'.$filterby.'%')
                                ->orwhere('fwd', 'like', '%'.$filterby.'%')
                                ->orwhere('shipmode', 'like', '%'.$filterby.'%');
            });
        }

        $data = $data->orderby('invoice');

        return Datatables::of($data)
                ->addColumn('checkbox', function ($data) {
                    return '<input type="checkbox" class="clplanref" name="selector[]" id="Inputselector" value="'.$data->invoice.'" data-so="'.$data->so.'" data-planstuffing="'.$data->plan_stuffing.'" data-fwd="'.$data->fwd.'" data-destination="'.$data->destination.'" data-customer="'.$data->customer_number.'" data-shipmode="'.$data->shipmode.'" data-cbm="'.$data->cbm.'" data-totalbalance="'.$data->total_balance.'" >';
                })
                ->editColumn('remark_cont',function($data){
                    $gtrm = DB::table('invoices')
                                    ->select('remark_container')
                                    ->where('invoice',$data->invoice)
                                    ->whereNull('deleted_at')
                                    ->first();

                    return $gtrm->remark_container;
                })
                ->editColumn('plan_stuffing', function($data){
                   return '<div id="plan_'.$data->invoice.'">
                                <input  width="100px" type="date"
                                    data-id="'.$data->invoice.'"
                                    class="form-control plan_unfilled"
                                    value="'.$data->plan_stuffing.'">
                                </input>
                            </div>';
                })
                ->editColumn('so', function($data){
                   return '<div id="so_'.$data->invoice.'">
                                <input  width="150px" type="text"
                                    data-id="'.$data->invoice.'"
                                    class="form-control so_unfilled"
                                    value="'.$data->so.'">
                                </input>
                            </div>';
                })
                ->editColumn('shipmode', function($data){
                   return '<div id="ship_'.$data->invoice.'">
                                <input  width="170px" type="text"
                                    data-id="'.$data->invoice.'"
                                    class="form-control ship_unfilled"
                                    value="'.$data->shipmode.'">
                                </input>
                            </div>';
                })
                ->editColumn('fwd', function($data){
                   return '<div id="fwd_'.$data->invoice.'">
                                <input  width="150px" type="text"
                                    data-id="'.$data->invoice.'"
                                    class="form-control fwd_unfilled"
                                    value="'.$data->fwd.'">
                                </input>
                            </div>';
                })
                ->editColumn('total_balance', function($data) {
                    if ($data->total_balance == nulL) {
                        return '-';
                    }else{
                        return $data->total_balance;
                    }
                })
                ->addColumn('action', function($data){
                    return '<button type="button" id="btnCancel" class="btn btn-danger" data-invoice="'.$data->invoice.'" onclick="cancelInvoice(this);" title="Cancel Invoice"><i class="icon-cancel-circle2"></i></button>';
                })
                ->rawColumns(['action', 'checkbox', 'status', 'total_balance','plan_stuffing','so','shipmode','fwd'])
                ->make(true);
    }

    public function exportinvoice(Request $request)
    {
        $filterby = $request->filterby;
        $orderby = $request->orderby;
        $direction = $request->direction;

        $data = DB::table('v_plan_load_current_shipment_2')
                ->where('factory_id', Auth::user()->factory_id)
                ->where('is_delivery', false)
                ->wherenull('deleted_at');
        
        if ($request->radio_status == 'fw') {
            $data = $data->where('fwd', 'like', '%'.trim($request->fwd).'%');
            
        }elseif ($request->radio_status == 'ship') {
            $data = $data->where('shipmode', 'like', '%'.trim($request->shipmode).'%');

        }

        // jika filterby tidak kosong
        if (!empty($filterby)) {
            $data = $data->where(function ($query) use ($filterby){
                        $query->where('invoice', 'like', '%'.$filterby.'%')
                                ->orwhere('fwd', 'like', '%'.$filterby.'%')
                                ->orwhere('shipmode', 'like', '%'.$filterby.'%');
            });
        }

        //jika orderby tidak undefined
        // if($orderby != 'undefined') {
        //     $data = $data->orderBy($orderby, $direction);
        // }
        // else {
            $data = $data->orderBy('invoice');
        // }

        $filename = 'trucking_' . $filterby;

        $i = 1;

        $current_date = date_format(Carbon::now(), 'd/m/Y');

        $export = \Excel::create($filename, function ($excel) use ($data, $i, $current_date){
            $excel->sheet('report', function($sheet) use ($data, $i, $current_date){
                $sheet->appendRow(array(
                    'Plan', 'Invoice', 'SO', 'Shipmode', 'FWD', 'Cust No'
                ));
                $data->chunk(100, function ($rows) use ($sheet, $i, $current_date){
                    foreach ($rows as $row) {
                        $sheet->appendRow(array(
                            $row->plan_stuffing, $row->invoice, $row->so, $row->shipmode, $row->fwd, $row->customer_number
                        ));
                    }
                });
            });
        })->download('xlsx');
    }

    // update invoice
    public function updateInvoice(Request $request)
    {   
        $action = $request->action;
        $data = $request->data;
        
        $data_detail = array();
        $data_header = array();

        $request->validate([
            'delivery_number' => 'required',
            'nopol' => 'required',
            'drivers_name' => 'required',
            'drivers_phone' => 'required',
            'warehouse' => 'required'
        ]);

        //header delivery
        $headers['delivery_number'] = $request->delivery_number;
        $headers['nopol'] = $request->nopol;
        $headers['drivers_name'] = $request->drivers_name;
        $headers['drivers_phone'] = $request->drivers_phone;
        $headers['warehouse'] = $request->warehouse;
        $headers['factory_id'] = Auth::user()->factory_id;
        $headers['created_at'] = Carbon::now();
        $headers['user_id'] = Auth::user()->id;
        $data_header = $headers;

        $current_factory = DB::table('factory')
                            ->select('factory_id', 'factory_name', 'numbering_invoice', 'code_invoice', 'numbering_sj')
                            ->where('factory_id', Auth::user()->factory_id)
                            ->whereNull('deleted_at')
                            ->first();
        
        $numbering_start = $current_factory->numbering_sj;

        //update
        try {
            DB::begintransaction();
            
            // insert delivery
            DB::table('delivery_orders')->insert($data_header);

            // insert detail
            foreach ($data as $key => $value) {

                DB::table('delivery_order_details')->insert([
                                                    'delivery_number' => $request->delivery_number,
                                                    'invoice' => $value['invoice'],
                                                    'created_at' => Carbon::now()   
                                                 ]);
                
                // update invoice
                DB::table('invoices')->where('invoice', $value['invoice'])
                                        ->update([
                                            'is_delivery' => true
                                        ]);
            }

            // update numbering sj factory
            DB::table('factory')
                    ->where('factory_id', Auth::user()->factory_id)
                    ->whereNull('deleted_at')
                    ->update([
                        'numbering_sj' => (int)$numbering_start+1
                    ]);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

    }

    // cancel invoice
    public function cancelInvoice(Request $request)
    {   
        $invoice = $request->invoice;
        $data_invoice = array();

        $request->validate([
            'invoice' => 'required'
        ]);

        // get invoice
        $getInvoice = DB::table('invoices')
                        ->where('invoice', $invoice)
                        ->first();
        
        if ($getInvoice->is_delivery == true) {
            return response()->json('Invoice already delivery..!',422);
        }

        // 
        $ins['invoice'] = $getInvoice->invoice;
        $ins['so'] = $getInvoice->so;
        $ins['plan_stuffing'] = $getInvoice->plan_stuffing;
        $ins['created_at'] = $getInvoice->created_at;
        $ins['updated_at'] = $getInvoice->updated_at;
        $ins['deleted_at'] = Carbon::now();
        $ins['mdd'] = $getInvoice->mdd;
        $ins['kst_statisticaldate'] = $getInvoice->kst_statisticaldate;
        $ins['customer_number'] = $getInvoice->customer_number;
        $ins['kst_lcdate'] = $getInvoice->kst_lcdate;
        $ins['fwd'] = $getInvoice->fwd;
        $ins['destination'] = $getInvoice->destination;
        $ins['shipmode'] = $getInvoice->shipmode;
        $ins['is_delivery'] = $getInvoice->is_delivery;
        $ins['factory_id'] = $getInvoice->factory_id;
        $ins['user_id'] = $getInvoice->user_id;
        $ins['cbm'] = $getInvoice->cbm;
        $ins['user_by'] = Auth::user()->id;
        $data_invoice = $ins;

        try {

            // insert cancel invoice
            DB::table('cancel_invoices')->insert($data_invoice);

            // update plan load
            DB::table('plan_load')->where('invoice', $invoice)
                                    ->update([
                                        'invoice' => null,
                                        'so' => null,
                                        'plan_stuffing' => null
                                    ]);

            // update plan load invoice
            DB::table('plan_load_invoice')->where('invoice', $invoice)
                                ->update([
                                    'invoice' => null,
                                    'so' => null,
                                    'plan_stuffing' => null
                                ]);
                                            
            // delete invoices
            DB::table('invoices')->where('invoice', $invoice)
                                    ->delete();
            
           
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }

    }

    //get invoice cancel
    public function ajaxGetInvoiceCancel(){

        $data = DB::table('cancel_invoices')
                    ->select('invoice', 'so', 'plan_stuffing')
                    ->where('factory_id', Auth::user()->factory_id)
                    ->where('is_used', false)
                    ->orderby('invoice')
                    ->get();

        return view('exim/list_invoice_cancel')->with('data', $data);
    }

    public function invoiceSerial()
    {
        $current_factory = DB::table('factory')
                            ->select('factory_id', 'factory_name', 'numbering_invoice', 'code_invoice', 'numbering_sj')
                            ->where('factory_id', Auth::user()->factory_id)
                            ->whereNull('deleted_at')
                            ->first();
        
        $invoice_start = $this->invoice_serial($current_factory->code_invoice, $current_factory->numbering_invoice);

        return response()->json($invoice_start, 200);
    }

    public function sjSerial()
    {
        $current_factory = DB::table('factory')
                            ->select('factory_id', 'factory_name', 'numbering_invoice', 'code_invoice', 'numbering_sj')
                            ->where('factory_id', Auth::user()->factory_id)
                            ->whereNull('deleted_at')
                            ->first();
        
        $invoice_start = $this->sj_serial($current_factory->numbering_sj,$current_factory->code_invoice);

        return response()->json($invoice_start, 200);
    }

    // numbering invoice
    static function invoice_serial($code, $string){
        $string = str_pad($string, 6, "0", STR_PAD_LEFT);
        return $code.$string;
    }

    // numbering surat jalan
    static function sj_serial($string,$code){
        $string = str_pad($string, 6, "0", STR_PAD_LEFT);
        return $code.$string;
    }


    //by naufal
    public function detailInvoice(){
        $factory = DB::table('factory')
                ->select('factory_id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();
        return view('exim/detail_invoice')->with('factory',$factory);
    }

    public function getdDetailInvoice(Request $request){
        $radio = $request->radio;
        $invoice = $request->invoice;
        $orderno = $request->orderno;
        $planstuff = $request->planstuff;
        $podd = $request->podd;
        $factory_id = $request->factory_id;
       
        $date_range = explode('-', preg_replace('/\s+/', '', $planstuff));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $date_podd = explode('-', preg_replace('/\s+/', '', $podd));
        $range_podd = array(
            'from' => date_format(date_create($date_podd[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_podd[1]), 'Y-m-d 23:59:59')
        );
       

    
        if (!empty($orderno) || !empty($invoice)||!empty($planstuff)||!empty($podd)) {

            
                $data = DB::table('v_detail_invoice')
                        ->where('factory_id',$factory_id);

                if ($radio == "invoice") {
                    $data = $data->where('invoice','LIKE',$invoice.'%');
                }else if ($radio=="po"){
                    $data = $data->where('orderno','LIKE',$orderno.'%');
                }else if ($radio=="plan"){
                      $data = $data->whereBetween('plan_stuffing',[$range['from'],$range['to']]);
                }else if ($radio=="pdcheck"){
                    $data = $data->whereBetween('podd_check',[$range_podd['from'],$range_podd['to']]);
                }else{
                    $data= array();
                }

                

        }else{
            $data = DB::table('v_detail_invoice')
                        ->where('factory_id',$factory_id);
             
        }

        $data = $data->orderby('invoice','ASC')
                    ->orderBy('plan_stuffing','ASC')
                    ->orderby('fwd','Desc');


        


       return Datatables::of($data)
                ->editColumn('fwd', function($data){
                    $Damco ="";
                    $DHL ="";
                    $DGF ="";
                    $DSV ="";
                    $EXPEDITORS ="";
                    $FEDEX ="";
                    $KN ="";
                    $NAKU ="";
                    $Panalpina ="";
                    $ITG="";
                    $NO3PL="";
                    $set ="";
                    switch ($data->fwd) {
                        case 'Damco':
                            $Damco = "selected='selected'";
                            break;

                        case 'DHL':
                            $DHL = "selected='selected'";
                            break;

                        case 'DGF':
                            $DGF = "selected='selected'";
                            break;

                        case 'DSV Air & Sea Inc.':
                            $DSV = "selected='selected'";
                            break;

                        case 'EXPEDITORS':
                            $EXPEDITORS = "selected='selected'";
                            break;

                        case 'FEDEX':
                            $FEDEX = "selected='selected'";
                            break;

                        case 'KN':
                            $KN = "selected='selected'";
                            break;

                        case 'NAKU FREIGHT':
                            $NAKU = "selected='selected'";
                            break;

                        case 'Panalpina':
                            $Panalpina = "selected='selected'";
                            break;

                        case 'ITG':
                            $ITG = "selected='selected'";
                            break;

                        case 'NO3PL':
                            $NO3PL = "selected='selected'";
                            break;

                        default:
                            $set = "selected='selected'";
                            break;
                    }
                   
                    return '<select class="form-control fwd_unfilled" data-invoice="'.$data->invoice.'" data-fwd="'.$data->fwd.'">
                            <option value="" '.$set.'>--NOT SET--</option>
                            <option value="Damco" '.$Damco.'>Damco</option>
                            <option value="DHL" '.$DHL.'>DHL</option>
                            <option value="DGF" '.$DGF.'>DGF</option>
                            <option value="DSV Air & Sea Inc." '.$DSV.'>DSV Air & Sea Inc.</option>
                            <option value="EXPEDITORS" '.$EXPEDITORS.'>EXPEDITORS</option>
                            <option value="FEDEX" '.$FEDEX.'>FEDEX</option>
                            <option value="KN" '.$KN.'>KN</option>
                            <option value="NAKU FREIGHT" '.$NAKU.'>NAKU FREIGHT</option>
                            <option value="Panalpina" '.$Panalpina.'>Panalpina</option>
                            <option value="ITG" '.$ITG.'>ITG</option>
                            <option value="NO3PL" '.$NO3PL.'>NO3PL</option>
                    </select>';
                })
                ->editColumn('shipmode',function($data){
                    $truck ="";
                    $aircol ="";
                    $seacol ="";
                    $courcol ="";
                    $seaircol ="";
                    $airprep ="";
                    $atruckcol ="";
                    $atruckpre ="";
                    $x ="";

                    switch ($data->shipmode) {
                        case '09 - Truck':
                            $truck = "selected='selected'";
                            break;

                        case '10 - Air Collect':
                            $aircol = "selected='selected'";
                            break;

                        case '11 - Sea Collect':
                            $seacol = "selected='selected'";
                            break;

                        case '20 - Courier Collect':
                            $courcol = "selected='selected'";
                            break;

                        case '21 - Sea/Air Collect':
                            $seaircol = "selected='selected'";
                            break;

                        case '23 - Air Prepaid':
                            $airprep = "selected='selected'";
                            break;

                        case '40 - Air Truck Collect':
                            $atruckcol = "selected='selected'";
                            break;

                        case '41 - Air Truck Prepaid':
                            $atruckpre = "selected='selected'";
                            break;

                        default:
                            $x = "selected='selected'";
                            break;
                    }

                   
                    return '<select class="form-control ship_unfilled" data-invoice="'.$data->invoice.'" data-ship ="'.$data->shipmode.'">
                                <option value="" '.$x.'>--NOT SET--</option>
                                <option value="09 - Truck" '.$truck.'>09 - Truck</option>
                                <option value="10 - Air Collect" '.$aircol.'>10 - Air Collect</option>
                                <option value="11 - Sea Collect" '.$seacol.'>11 - Sea Collect</option>
                                <option value="20 - Courier Collect" '.$courcol.'>20 - Courier Collect</option>
                                <option value="21 - Sea/Air Collect" '.$seaircol.'>21 - Sea/Air Collect</option>
                                <option value="23 - Air Prepaid" '.$airprep.'>23 - Air Prepaid</option>
                                <option value="40 - Air Truck Collect" '.$atruckcol.'>40 - Air Truck Collect</option>
                                <option value="41 - Air Truck Prepaid" '.$atruckpre.'>41 - Air Truck Prepaid</option>
                    </select>';
                })
                ->editColumn('plan_ref', function($data){
                    return '<div id="planref_'.$data->orderno.'">
                                <input type="text"
                                    data-orderno="'.$data->orderno.'"
                                    class="form-control plenref_unfilled"
                                    value="'.$data->plan_ref.'">
                                </input>
                            </div>';
                })
                ->addColumn('remark',function($data){
                    return '<input type="button" class="btn btn-default btn-remark"  data-orderno="'.$data->orderno.'" data-invoice="'.$data->invoice.'" id="btn-remark" value="Remark" onclick="remark(this);">';
                })
                ->addColumn('action',function($data){
                    return '<input type="button" class="btn btn-danger btn-delete" data-id="'.$data->id.'" data-orderno="'.$data->orderno.'" data-invoice='.$data->invoice.' value="Delete" onclick="deletepo(this);">';
                })
                ->rawColumns(['plan_ref','action','remark','fwd','shipmode'])
                ->make(true);
    }

    public function exportDetailInvoice(Request $request){
       
        $radio = $request->radio;
        $invoice = $request->invoice;
        $orderno = $request->orderno;
        $orderby = $request->orderby;
        $planstuff = $request->planstuff;
        $podd = $request->podd;
        $factory_id = $request->factory_id;
       
        $date_range = explode('-', preg_replace('/\s+/', '', $planstuff));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $date_podd = explode('-', preg_replace('/\s+/', '', $podd));
        $range_podd = array(
            'from' => date_format(date_create($date_podd[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_podd[1]), 'Y-m-d 23:59:59')
        );

        if (!empty($orderno) || !empty($invoice)||!empty($planstuff)||!empty($podd)) {

            
                $data = DB::table('v_detail_invoice')
                        ->where('factory_id',$factory_id);


                 if ($radio == "invoice") {
                    $data = $data->where('invoice',$invoice);
                    $dt = "invoice";
                }else if ($radio=="po"){
                    $data = $data->where('orderno',$orderno);
                    $dt = "orderno";
                }else if ($radio=="plan"){
                      $data = $data->whereBetween('plan_stuffing',[$range['from'],$range['to']]);
                      $dt = "planstuffing";
                }else if ($radio=="pdcheck"){
                    $data = $data->whereBetween('podd_check',[$range_podd['from'],$range_podd['to']]);
                      $dt = "podd";
                }else{
                    $data= array();
                    $dt = "";

                }

            

        }else{
            $data = DB::table('v_detail_invoice')
                        ->where('plan_load_invoice.factory_id',$factory_id);
             
                      $dt = "";

        }#if
        
        
        $date = date_format( Carbon::now(),'d/m/Y');
        $filename = 'Detail_invoice_AOI'.$factory_id.'_'.$dt.'_'.$date;

        $i = 1;
        $data = $data->orderby('plan_stuffing','ASC');
        $export = \Excel::create($filename,function($excel) use($data,$i,$date){
                $excel->sheet('report',function($sheet) use ($data,$i,$date){
                    $sheet->appendRow(array(
                        '#','LC','Style','Factory','QTY','CRD','PSDD','MDD','Priority' ,'LOT. REF','Destination','CUST. NO.','Ship Mode','Invoice Number','FWD','SO','CTN','CBM','GROSS WEIGHT','NET WEIGHT','Remarks','Plan Ex Fact','WHS CODE','SUBSIDARY SHIP TO','Stuffing Place','Product Type','Age Group','Gender','PD','PSFD1','PSFD2','PO','PODD Check','Remark Cont.'
                    ));
                    $data->chunk(100,function($rows) use ($sheet,$i,$date){
                            foreach ($rows as $row) {
                               
                                switch ($row->factory_id) {
                                    case '1':
                                        $fct = "AOI 1";
                                        break;
                                    
                                    case '2':
                                        $fct = "AOI 2";
                                        break;
                                    }

                                    $slc = $row->order_type.' '.Carbon::parse($row->kst_lcdate)->format('d-M-Y');
                                    $remark_cont = DB::table('invoices')
                                                        ->where('invoice',$row->invoice)
                                                        ->whereNull('deleted_at')
                                                        ->first();
                                $sheet->appendRow(array(
                                    $i++, $slc, $row->style, $fct, $row->total_item_qty, Carbon::parse($row->crd)->format('d-M-Y'), Carbon::parse($row->po_stat_date_adidas)->format('d-M-Y'), Carbon::parse($row->mdd)->format('d-M-Y'), $row->priority_code, $row->orderno, $row->country_name, $row->customer_number, $row->shipmode, $row->invoice, $row->fwd, $row->so, $row->total_ctn, $row->cbm, $row->total_gross, $row->total_net, $row->remarks_exim, Carbon::parse($row->plan_stuffing)->format('d-M-Y'), $row->whs_code, $row->subsidiary, $row->stuffing_place_update, $row->product_type, $row->age_group, $row->gender,  Carbon::parse($row->pd)->format('d-M-Y'),  Carbon::parse($row->psfd_1)->format('d-M-Y'),  Carbon::parse($row->psfd_2)->format('d-M-Y'), $row->po_number, Carbon::parse($row->podd_check)->format('d-M-Y'), $row->remark_container
                                ));
                            }
                    });
                });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }


    // public function deletepo(Request $request){
    //     $orderno = $request->orderno;

    //     try {
    //         DB::table('plan_load')
    //             ->where('orderno',$orderno)
    //             ->update([
    //                 'deleted_at'=>Carbon::now()
    //             ]);
    //     } catch (\Exception $er) {
    //         $message = $er->getMessage();
    //         \ErrorHandler::db($message);
    //     }
    // }

    public function updateFwd(Request $request){
        // $ponumber = $request->ponumber;
        // $plan_ref_number = $request->plan_ref_number;
        $fwd = $request->fwd;
        $invoice = $request->invoice;

        try {
            DB::begintransaction();
                DB::table('invoices')
                        ->where('invoice',$invoice)
                        ->whereNull('deleted_at')
                        ->update([
                            'fwd'=>$fwd,
                            'updated_at'=>Carbon::now()
                        ]);

                DB::table('plan_load')
                    ->where('invoice',$invoice)
                    ->whereNull('deleted_at')
                    ->update([
                        'fwd'=>$fwd,
                        'updated_at'=>Carbon::now()
                    ]);
                    
                DB::table('plan_load_invoice')
                    ->where('invoice',$invoice)
                    ->whereNull('deleted_at')
                    ->update([
                        'fwd'=>$fwd,
                        'updated_at'=>Carbon::now()
                    ]);
           
            DB::commit();
        } catch (\Exception $er) {
            $message = $er->getMessage();
            \ErrorHandler::db($message);
        }

        $html='<input type="text"
                    data-id="'.$invoice.'"
                    class="form-control fwd_unfilled"
                    value="'.$fwd.'">
                </input>';      

        return response()->json($html, 200);
    }

    public function updatePlanStuffing(Request $request){
       
        $planstuffing = $request->planstuffing;
        $invoice = $request->invoice;

        try {
            DB::begintransaction();
                DB::table('invoices')
                        ->where('invoice',$invoice)
                        ->whereNull('deleted_at')
                        ->update([
                            'plan_stuffing'=>$planstuffing,
                            'updated_at'=>Carbon::now()
                        ]);
                DB::table('plan_load')
                    ->where('invoice',$invoice)
                    ->whereNull('deleted_at')
                    ->update([
                        'plan_stuffing'=>$planstuffing,
                        'updated_at'=>Carbon::now()
                    ]);

                DB::table('plan_load_invoice')
                    ->where('invoice',$invoice)
                    ->whereNull('deleted_at')
                    ->update([
                        'plan_stuffing'=>$planstuffing,
                        'updated_at'=>Carbon::now()
                    ]);
           
            DB::commit();
        } catch (\Exception $er) {
            $message = $er->getMessage();
            \ErrorHandler::db($message);
        }

        $html='<input type="text"
                    data-id="'.$invoice.'"
                    class="form-control plan_unfilled"
                    value="'.$planstuffing.'">
                </input>';      

        return response()->json($html, 200);
    }

    public function updateSo(Request $request){
       
        $so = $request->so;
        $invoice = $request->invoice;

        try {
            DB::beginTransaction();

                DB::table('invoices')
                        ->where('invoice',$invoice)
                        ->whereNull('deleted_at')
                        ->update([
                            'so' => $so,
                            'updated_at' => Carbon::now()
                        ]);

                DB::table('plan_load')
                    ->where('invoice',$invoice)
                    ->whereNull('deleted_at')
                    ->update([
                        'so' => $so,
                        'updated_at'=>Carbon::now()
                    ]);
                
                DB::table('plan_load_invoice')
                    ->where('invoice',$invoice)
                    ->whereNull('deleted_at')
                    ->update([
                        'so' => $so,
                        'updated_at'=>Carbon::now()
                    ]);
           
            DB::commit();
        } catch (\Exception $er) {
            $message = $er->getMessage();
            \ErrorHandler::db($message);
        }

        $html='<input type="text"
                    data-id="'.$invoice.'"
                    class="form-control so_unfilled"
                    value="'.$so.'">
                </input>';      

        return response()->json($html, 200);
    }

    public function updateShip(Request $request){
       
        $ship = $request->ship;
        $invoice = $request->invoice;

        try {
            DB::begintransaction();
                DB::table('invoices')
                        ->where('invoice',$invoice)
                        ->whereNull('deleted_at')
                        ->update([
                            'shipmode'=>$ship,
                            'updated_at'=>Carbon::now()
                        ]);
                DB::table('plan_load')
                    ->where('invoice',$invoice)
                    ->whereNull('deleted_at')
                    ->update([
                        'shipmode'=>$ship,
                        'updated_at'=>Carbon::now()
                    ]);
                
                DB::table('plan_load_invoice')
                    ->where('invoice',$invoice)
                    ->whereNull('deleted_at')
                    ->update([
                        'shipmode'=>$ship,
                        'updated_at'=>Carbon::now()
                    ]);
           
            DB::commit();
        } catch (\Exception $er) {
            $message = $er->getMessage();
            \ErrorHandler::db($message);
        }

        $html='<input type="text"
                    data-id="'.$invoice.'"
                    class="form-control ship_unfilled"
                    value="'.$ship.'">
                </input>';      

        return response()->json($html, 200);
    }

    public function updatePlanRef(Request $request){
       
        $plan_ref = $request->plan_ref;
        $orderno = $request->orderno;

        try {
            DB::begintransaction();
           
            DB::table('plan_load')
                ->where('orderno',$orderno)
                ->whereNull('deleted_at')
                ->update([
                    'plan_ref'=>$plan_ref,
                    'updated_at'=>Carbon::now()
                ]);

            DB::table('plan_load_invoice')
                ->where('orderno',$orderno)
                ->whereNull('deleted_at')
                ->update([
                    'plan_ref'=>$plan_ref,
                    'updated_at'=>Carbon::now()
                ]);
           
            DB::commit();
        } catch (\Exception $er) {
            $message = $er->getMessage();
            \ErrorHandler::db($message);
        }

        $html='<input type="text"
                    data-id="'.$orderno.'"
                    class="form-control so_unfilled"
                    value="'.$plan_ref.'">
                </input>';      

        return response()->json($html, 200);
    }

    public function deletePO(Request $request){
        $id = $request->id;

        $plan_load = DB::table('plan_load')
                    ->where('id',$id)
                    ->get();

            foreach ($plan_load as $rw ) {
                $delete_poinvoice = array(
                    'orderno'=>$rw->orderno,
                    'poright'=>$rw->poright,
                    'plan_ref'=>$rw->plan_ref,
                    'old_po_re_route'=>$rw->old_po_re_route,
                    'sports_cats'=>$rw->sports_cats,
                    'product_type'=>$rw->product_type,
                    'age_group'=>$rw->age_group,
                    'gender'=>$rw->gender,
                    'mdd'=>$rw->mdd,
                    'mdd_new'=>$rw->mdd_new,
                    'priority_code'=>$rw->priority_code,
                    'subsidiary'=>$rw->subsidiary,
                    'whs_code'=>$rw->whs_code,
                    'shipmode'=>$rw->shipmode,
                    'fwd'=>$rw->fwd,
                    'psfd_1'=>$rw->psfd_1,
                    'gw'=>$rw->gw,
                    'nw'=>$rw->nw,
                    'ctn'=>$rw->ctn,
                    'invoice'=>$rw->invoice,
                    'so'=>$rw->so,
                    'plan_stuffing'=>$rw->plan_stuffing,
                    'actual_stuffing'=>$rw->actual_stuffing,
                    'remarks_order'=>$rw->remarks_order,
                    'qty_allocated'=>$rw->qty_allocated,
                    'is_integrate'=>$rw->is_integrate,
                    'integrate_date'=>$rw->integrate_date,
                    'is_stuffing'=>$rw->is_stuffing,
                    'created_at'=>$rw->created_at,
                    'updated_at'=>$rw->updated_at,
                    'deleted_at'=>Carbon::now(),
                    'psfd_2'=>$rw->psfd_2,
                    'customers_id'=>$rw->customers_id,
                    'id'=>$rw->id,
                    'factory_id'=>$rw->factory_id,
                    'pd'=>$rw->pd,
                    'remarks_psfd'=>$rw->remarks_psfd,
                    'remarks_exim'=>$rw->remarks_exim,
                );
               
                $newplan_load = array(
                    'orderno'=>$rw->orderno,
                    'plan_ref'=>$rw->plan_ref,
                    'old_po_re_route'=>$rw->old_po_re_route,
                    'sports_cats'=>$rw->sports_cats,
                    'product_type'=>$rw->product_type,
                    'age_group'=>$rw->age_group,
                    'gender'=>$rw->gender,
                    'mdd'=>$rw->mdd,
                    'priority_code'=>$rw->priority_code,
                    'subsidiary'=>$rw->subsidiary,
                    'whs_code'=>$rw->whs_code,
                    'shipmode'=>$rw->shipmode,
                    'fwd'=>$rw->fwd,
                    'psfd_1'=>$rw->psfd_1,
                    'remarks_order'=>$rw->remarks_order,
                    'created_at'=>$rw->created_at,
                    'psfd_2'=>$rw->psfd_2,
                    'customers_id'=>$rw->customers_id,
                    'factory_id'=>$rw->factory_id,
                    'pd'=>$rw->pd,
                    'remarks_psfd'=>$rw->remarks_psfd,

                );

            }#endforeach

            try {
                DB::begintransaction();

                    DB::table('history_delete_poinvoice')->insert($delete_poinvoice);
                    DB::table('plan_load')->insert($newplan_load);
                    DB::table('plan_load')->where('id',$id)->delete();
                    DB::table('plan_load_invoice')->where('id',$id)->delete();

                DB::commit();
            } catch (Exception $er) {
                $message = $er->getMessage();
                \ErrorHandler($message);
            }
 
    }#deletePO

    public function detailDelivery(){
        return view('exim/detail_delivery');
    }#detailDelivery

    public function ajaxGetDelivery(Request $request){
        $radio = $request->radio;
        $no_delivery = $request->no_delivery;
        $no_invoice = $request->no_invoice;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );
        

        if (!empty($no_delivery)||!empty($no_invoice)||!empty($date_range)) {
            $data = DB::table('delivery_orders')
                        ->join('delivery_order_details','delivery_orders.delivery_number','=','delivery_order_details.delivery_number')
                        ->join('invoices','invoices.invoice','=','delivery_order_details.invoice')
                        ->where('delivery_orders.factory_id',Auth::user()->factory_id)
                        ->whereNull('delivery_orders.deleted_at')
                        ->whereNull('delivery_order_details.deleted_at');

                if ($radio=='delivery_num') {
                    $data = $data->where('delivery_orders.delivery_number',$no_delivery);
                }else if ($radio=='invoice_num'){
                    $data = $data->where('delivery_order_details.invoice',$no_invoice);
                }else if ($radio=='plan_stuff'){
                    $data = $data->whereBetween('invoices.plan_stuffing',[$range['from'],$range['to']]);
                }else{
                    $data = array();
                }
        }else{
            $data = array();
        }
        
        return  Datatables::of($data)
                    ->addColumn('orderno',function($data){
                            $po = DB::table('plan_load_invoice')
                                        ->select('orderno')
                                        ->where('invoice',$data->invoice)
                                        ->whereNull('deleted_at')
                                        ->get();
                            foreach ($po as $p) {
                                return $p->orderno;
                            }
                    })
                    ->addColumn('action',function($data){
                        return '<input type="button" class="btn btn-danger btn-delete" data-id="'.$data->invoice.'" data-delivery="'.$data->delivery_number.'" value="Delete" onclick="deleteinv(this);">';
                    })
                    ->rawColumns(['orderno','action'])
                    ->make(true);


    }#endajaxdelivery

    public function deleteInv(Request $request){
        $delivery_number = $request->delivery_number;
        $invoice = $request->invoice;

        try {
                DB::begintransaction();

                    DB::table('delivery_order_details')->where('delivery_number',$delivery_number)->update(['deleted_at'=>Carbon::now()]);
                    DB::table('delivery_orders')->where('delivery_number',$delivery_number)->delete();

                DB::commit();
            } catch (Exception $er) {
                $message = $er->getMessage();
                \ErrorHandler($message);
            }
    }

    public function getRemark(Request $request){
        $orderno = $request->orderno;
        $invoice = $request->invoice;
        
        $gexim = DB::table('plan_load_invoice')->where('orderno',$orderno)->whereNull('deleted_at')->first();
        $gcont = DB::table('invoices')->where('invoice',$invoice)->whereNull('deleted_at')->first();
        
    
         
        return response()->json(['exim'=>$gexim->remarks_exim,'cont'=>$gcont->remark_container],200);
    }

    public function setRemark(Request $request){
        $orderno = $request->orderno;
        $invoice = $request->invoice;
        $remark_exim = $request->remark_exim;
        $remark_cont = $request->remark_cont;

        try {
            DB::begintransaction();
                DB::table('plan_load_invoice')->where('orderno',$orderno)->whereNull('deleted_at')->update(['remarks_exim'=>$remark_exim,'updated_at'=>Carbon::now()]);
                DB::table('invoices')->where('invoice',$invoice)->whereNull('deleted_at')->update(['remark_container'=>$remark_cont,'updated_at'=>Carbon::now()]);
                
            DB::commit();
            return response()->json('Update Success . . .', 200);
        } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
                \ErrorHandler($message);
            return response()->json('Update Field . . . '.$message, 422);
        }
    }

    public function getRemarkPlan(Request $request){
        $getRem = DB::table('plan_load_invoice')->select('remarks_order','remarks_exim')->where('orderno',$request->orderno)->where('plan_ref',$request->plan_ref)->whereNull('deleted_at')->first();

        return response()->json(['rkorder'=>$getRem->remarks_order,'rkemxim'=>$getRem->remarks_exim],200);
    }

    public function setRemarkPlan(Request $request){
        $orderno = $request->orderno;
        $plan_ref = $request->plan_ref;
        $rem_order = $request->rm_order;
        $rem_exim = $request->rm_exim;

        try {
            db::beginTransaction();
                $dt_up = array(
                    'remarks_exim'=>$rem_exim,
                    'remarks_order'=>$rem_order,
                    'updated_at'=>Carbon::now()
                );

                
                DB::table('plan_load_invoice')
                        ->where('orderno',$orderno)
                        ->where('plan_ref',$plan_ref)
                        ->update($dt_up);
            return response()->json('Update Success . . .', 200);  
            db::commit();
            
        } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
                \ErrorHandler($message);
            return response()->json('Update Field . . . '.$message, 422);
        }
    }


    public function updateFwdinv(Request $request){
     
        $invoice = $request->invoice;
        $fwd = $request->fwd;
       
        try {
            DB::begintransaction();
            $dt = array('fwd'=>$fwd,'updated_at'=>carbon::now());
            DB::table('plan_load')->where('invoice',$invoice)->update($dt);
            DB::table('plan_load_invoice')->where('invoice',$invoice)->update($dt);
           
            DB::commit();
            return response()->json('Update Success . . .', 200);
        } catch (\Exception $er) {
            $message = $er->getMessage();
            \ErrorHandler::db($message);
            return response()->json('Update Field . . . '.$message, 422);
        }

    }

    public function updateShipinv(Request $request){
     
        $invoice = $request->invoice;
        $shipmode = $request->shipmode;
       
        try {
            DB::begintransaction();
            $dt = array('shipmode'=>$shipmode,'updated_at'=>carbon::now());
            DB::table('plan_load')->where('invoice',$invoice)->update($dt);
            DB::table('plan_load_invoice')->where('invoice',$invoice)->update($dt);
           
            DB::commit();
            return response()->json('Update Success . . .', 200);
        } catch (\Exception $er) {
            $message = $er->getMessage();
            \ErrorHandler::db($message);
            return response()->json('Update Field . . . '.$message, 422);
        }

    }


}