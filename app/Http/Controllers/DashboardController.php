<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Excel;
use Carbon\Carbon;

class DashboardController extends Controller
{
    //department list  = array (
    //  'preparation',
    //  'sewing',
    //  'inventory',
    //  'qcinspect',
    //  'shipping'
    //)

    //status list = array(
    //  'rejected',
    //  'onprogress',
    //  'completed'
    //)

    public function index(Request $request)
    {
        //
        $factory = DB::table('factory')
            ->select('factory_id', 'factory_name')
            ->wherenull('deleted_at')
            ->get();

        $factory_id = $request->factory_id ? $request->factory_id : Auth::user()->factory_id;

        $get_request_carton = DB::table('request_carton')
            ->join('line', 'request_carton.line_id', '=', 'line.id')
            ->where('request_carton.status_sewing', 'request')
            ->where('line.factory_id', $factory_id)
            ->wherenull('request_carton.deleted_at')
            ->wherenull('line.deleted_at')
            ->count();

        $total_request_change = $get_request_carton;


        $dt = Carbon::parse(Carbon::now());
        $mode = 'count';
        if ($request->frequency) {
            $frequency = strtolower($request->frequency);

            $data = array(
                'count_preparation' => $this->getAllQtyPackage($factory_id, $frequency, $mode, $dt, 'preparation'),
                'count_sewing' => $this->getAllQtyPackage($factory_id, $frequency, $mode, $dt, 'sewing'),
                'count_inventory' => $this->getAllQtyPackage($factory_id, $frequency, $mode, $dt, 'inventory'),
                'count_qcinspect' => $this->getAllQtyPackage($factory_id, $frequency, $mode, $dt, 'qcinspect'),
                'count_shipping' => $this->getAllQtyPackage($factory_id, $frequency, $mode, $dt, 'shipping'),
                'total_request_change' => $total_request_change
            );
            return response()->json($data);
        } else {
            $count_preparation = $this->getAllQtyPackage($factory_id, 'yearly', $mode, $dt, 'preparation');
            $count_sewing = $this->getAllQtyPackage($factory_id, 'yearly', $mode, $dt, 'sewing');
            $count_inventory = $this->getAllQtyPackage($factory_id, 'yearly', $mode, $dt, 'inventory');
            $count_qcinspect = $this->getAllQtyPackage($factory_id, 'yearly', $mode, $dt, 'qcinspect');
            $count_shipping = $this->getAllQtyPackage($factory_id, 'yearly', $mode, $dt, 'shipping');
            $total_request_change = $total_request_change;
        }


        return view('dashboard/index', [
            'count_preparation' => $count_preparation,
            'count_sewing' => $count_sewing,
            'count_inventory' => $count_inventory,
            'count_qcinspect' => $count_qcinspect,
            'count_shipping' => $count_shipping,
            'total_request_change' => $total_request_change,
            'factory' => $factory
        ]);
    }

    //get total number of package on each department
    public function getAllQtyPackage($factory_id, $frequency, $mode = 'get', $dt, $department = null, $roles = null, $po = null, $radio_status = 'po', $date_range_plan = null, $date_podd = null)
    {
        $frequency = strtolower($frequency);

        if ($mode == 'count') {
            $data = DB::table('package')
                ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                ->join('v_ns_dashboard', function ($join) {
                    $join->on('package_detail.po_number', '=', 'v_ns_dashboard.po_number');
                    $join->on('package_detail.plan_ref_number', '=', 'v_ns_dashboard.plan_ref_number');
                });
            // ->join('po_summary','package_detail.po_number','=','po_summary.po_number');
        } else {
            $data = DB::table('v_ns_dashboard');

            // if ($roles != 'qcinspect') {
            // if ($po != null && $po != '') {
            //     $data = $data->where('po_number', 'like', '%'.trim($po).'%');
            // }
            // }

            if ($radio_status != null) {
                if ($radio_status == 'po') {
                    $data = $data->where('po_number', 'like', '%' . trim($po) . '%');
                } elseif ($radio_status == 'plan') {
                    $date_range = explode('-', preg_replace('/\s+/', '', $date_range_plan));
                    $range = array(
                        'from' => Carbon::parse($date_range[0])->format('Y-m-d'),
                        'to' => Carbon::parse($date_range[1])->format('Y-m-d')
                    );

                    $data = $data->where(function ($query) use ($range) {
                        $query->whereBetween('plan_stuffing', [$range['from'], $range['to']]);
                    });
                } elseif ($radio_status == 'podd') {
                    $date_r_podd = explode('-', preg_replace('/\s+/', '', $date_podd));
                    $rangepodd = array(
                        'from' => Carbon::parse($date_r_podd[0])->format('Y-m-d'),
                        'to' => Carbon::parse($date_r_podd[1])->format('Y-m-d')
                    );

                    $data = $data->where(function ($query) use ($rangepodd) {
                        $query->whereBetween('podd_check', [$rangepodd['from'], $rangepodd['to']]);
                    });
                }
            }
        }

        if (!empty($department)) {
            // $data = $data->where('current_department', $department);
            if ($department == 'inventory') {
                $data = $data->where(function ($query) {
                    $query->where(function ($query) {
                        $query->where('current_department', 'inventory');
                    })
                        ->orWhere(function ($query) {
                            $query->where('current_department', 'shipping')
                                ->where('current_status', 'onprogress');
                        });
                });
            } elseif ($department == 'shipping') {
                $data = $data->where('current_department', 'shipping')
                    ->where('current_status', 'completed');
            } else {
                $data = $data->where('current_department', $department);
            }
        }

        if ($frequency == 'daily') {
            // if ($department == 'preparation') {
            //     $data = $data->where(function($query) {
            //                  $query->where(function($query) {
            //                      $query->whereRaw("EXTRACT(DAY FROM package.created_at) = ?",[$dt->day])
            //                              ->whereRaw("EXTRACT(MONTH FROM package.created_at) = ?",[$dt->month])
            //                              ->whereRaw("EXTRACT(YEAR FROM package.created_at) = ?",[$dt->year]);
            //                  })
            //                  ->orWhere(function($query) {
            //                      $query->whereRaw("EXTRACT(DAY FROM package.created_at) = ?",[$dt->day])
            //                              ->whereRaw("EXTRACT(MONTH FROM package.created_at) = ?",[$dt->month])
            //                              ->whereRaw("EXTRACT(YEAR FROM package.created_at) = ?",[$dt->year])
            //                              ->where('package.current_department', 'preparation')
            //                              ->where('package.current_status', 'completed');
            //                  })
            //                  ->orWhere(function($query) {
            //                      $query->where('current_department', 'shipping')
            //                            ->where('current_status', 'onprogress');
            //                  });
            //              })
            // }
            $data = $data->whereRaw("EXTRACT(DAY FROM v_ns_dashboard.created_at) = ?", [$dt->day])
                ->whereRaw("EXTRACT(MONTH FROM v_ns_dashboard.created_at) = ?", [$dt->month])
                ->whereRaw("EXTRACT(YEAR FROM v_ns_dashboard.created_at) = ?", [$dt->year]);
        } elseif ($frequency == 'weekly') {
            $data = $data->whereRaw("EXTRACT(WEEK FROM v_ns_dashboard.created_at) = ?", [$dt->weekOfYear])
                ->whereRaw("EXTRACT(YEAR FROM v_ns_dashboard.created_at) = ?", [$dt->year]);
        } elseif ($frequency == 'monthly') {
            $data = $data->whereRaw("EXTRACT(MONTH FROM v_ns_dashboard.created_at) = ?", [$dt->month])
                ->whereRaw("EXTRACT(YEAR FROM v_ns_dashboard.created_at) = ?", [$dt->year]);
        } elseif ($frequency == 'yearly') {
            $data = $data->whereRaw("EXTRACT(YEAR FROM v_ns_dashboard.created_at) >= ?", ['2018']);
        } else {
            if ($mode == 'count') {
                $data = 0;
            } else {
                $data = false;
            }
        }

        $data = $data->whereNull('v_ns_dashboard.deleted_at')
            ->where('v_ns_dashboard.factory_id', $factory_id);


        //jika department qcinspect maka ambil data yg status inventory diatas 80% saja
        // if($roles == 'qcinspect') {
        //     $data = $data->where('v_ns_dashboard.persen_inventory', '>=' , '80');
        // }

        if ($data !== 0 || $data !== false) {
            if ($mode == 'count') {
                $data = $data->whereNull('package.deleted_at')->count();
            } else {
                // if($roles == 'qcinspect') {
                //     $data = $data->orderBy('v_ns_dashboard.persen_qcinspect', 'asc');
                // }
                // else {
                $data = $data->orderBy('v_ns_dashboard.kst_statisticaldate', 'asc');
                // }
            }
        }


        return $data;
    }

    //DATA FOR DATATABLES INDEX
    public function getData(Request $request)
    {
        // return $request->all();
        $radio_status = $request->radio_status;
        $po = $request->po;
        $date_podd = $request->date_podd;
        $date_range_plan = $request->date_range_plan;
        $filter_click = $request->filter_click;
        $dt = Carbon::parse(Carbon::now());
        $mode = 'get';

        $factory_id = $request->factory_id ? $request->factory_id : Auth::user()->factory_id;

        //untuk filtering dashboard qcinspect
        $roles = \Auth::user()->roles[0]['name'];

        /* if ($roles == 'qcinspect') {
            //get data
            if($request->frequency) {
                $frequency = strtolower($request->frequency);
                $data = $this->getAllQtyPackage($factory_id, $frequency, $mode, $dt, null, $roles, $po);
            }
            else {
                $data = $this->getAllQtyPackage($factory_id, 'yearly', $mode, $dt, null, $roles, $po);
            }


            if($data === false) {
                return false;
            }

            //datatables
            return DataTables::of($data)
                    ->editColumn('total_package', function($data) {
                        $total_package = DB::table('package')
                                            ->join('package_detail','package_detail.scan_id','=','package.scan_id')
                                            ->where('po_number', $data->po_number)
                                            ->where('plan_ref_number', $data->plan_ref_number)
                                            ->where('factory_id', $data->factory_id)
                                            ->whereNull('package.deleted_at')
                                            ->count();
                        return $total_package;
                    })
                    ->editColumn('preparation', function($data) {
                        //total package
                        $total_package = DB::table('package')
                                            ->join('package_detail','package_detail.scan_id','=','package.scan_id')
                                            ->where('po_number', $data->po_number)
                                            ->where('plan_ref_number', $data->plan_ref_number)
                                            ->where('factory_id', $data->factory_id)
                                            ->whereNull('package.deleted_at')
                                            ->count();

                        //get qty package per department
                        $qty_preparation = $this->getQtyPackage($data->po_number, 'preparation', $data->plan_ref_number, $data->factory_id);

                        //get status package per department (completed / onprogress / waiting)
                        $status_preparation = $this->getStatusPerDepartment($data->po_number, $total_package, $qty_preparation, 'preparation', 'status_preparation', null, $data->plan_ref_number, $data->factory_id);

                        $status = $this->showStatus($data->factory_id, $status_preparation);

                        return $status;
                    })
                    ->editColumn('sewing', function($data) {
                        //total package
                        $total_package = DB::table('package')
                                            ->join('package_detail','package_detail.scan_id','=','package.scan_id')
                                            ->where('po_number', $data->po_number)
                                            ->where('plan_ref_number', $data->plan_ref_number)
                                            ->where('factory_id', $data->factory_id)
                                            ->whereNull('package.deleted_at')
                                            ->count();

                        //get qty package per department
                        $qty_sewing = $this->getQtyPackage($data->po_number, 'sewing', $data->plan_ref_number, $data->factory_id);

                        //get status package per department (completed / onprogress / waiting)
                        $status_sewing = $this->getStatusPerDepartment($data->po_number, $total_package, $qty_sewing, 'sewing', 'status_sewing', null, $data->plan_ref_number, $data->factory_id);
                        $status = $this->showStatus($data->factory_id, $status_sewing);
                        return $status;
                    })
                    ->editColumn('inventory', function($data) {
                        //total package
                        $total_package = DB::table('package')
                                            ->join('package_detail','package_detail.scan_id','=','package.scan_id')
                                            ->where('po_number', $data->po_number)
                                            ->where('plan_ref_number', $data->plan_ref_number)
                                            ->where('factory_id', $data->factory_id)
                                            ->whereNull('package.deleted_at')
                                            ->count();

                        //get qty package per department
                        $qty_inventory = $this->getQtyPackage($data->po_number, 'inventory', $data->plan_ref_number, $data->factory_id);

                        //get status package per department (completed / onprogress / waiting)
                        $status_inventory = $this->getStatusPerDepartment($data->po_number, $total_package, $qty_inventory, 'inventory', 'status_inventory', null, $data->plan_ref_number, $data->factory_id);
                        $status = $this->showStatus($data->factory_id, $status_inventory);
                        return $status;
                    })
                    ->editColumn('qcinspect', function($data) {
                        //total package
                        $total_package = DB::table('package')
                                            ->join('package_detail','package_detail.scan_id','=','package.scan_id')
                                            ->where('po_number', $data->po_number)
                                            ->where('plan_ref_number', $data->plan_ref_number)
                                            ->where('factory_id', $data->factory_id)
                                            ->whereNull('package.deleted_at')
                                            ->count();

                        //get qty package per department
                        $qty_qcinspect = $this->getQtyPackage($data->po_number, 'qcinspect', $data->plan_ref_number, $data->factory_id);

                        //get total qty from status_qcinspect = completed
                        $qty_qcinspect_completed = DB::table('package')
                                                       ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                                                       ->where('po_number', $data->po_number)
                                                       ->where('plan_ref_number', $data->plan_ref_number)
                                                       ->where('factory_id', $data->factory_id)
                                                       ->where('status_qcinspect', 'completed')
                                                       ->whereNull('package.deleted_at')
                                                       ->whereNull('package_detail.deleted_at')
                                                       ->count();

                        //get status package per department (completed / onprogress / waiting)
                        $status_qcinspect = $this->getStatusPerDepartment($data->po_number, $total_package, $qty_qcinspect, 'qcinspect', 'status_qcinspect', $qty_qcinspect_completed, $data->plan_ref_number, $data->factory_id);
                        $status = $this->showStatus($data->factory_id, $status_qcinspect, $qty_qcinspect);
                        return $status;
                    })
                    ->editColumn('shipping', function($data) {
                        //total package
                        $total_package = DB::table('package')
                                            ->join('package_detail','package_detail.scan_id','=','package.scan_id')
                                            ->where('po_number', $data->po_number)
                                            ->where('plan_ref_number', $data->plan_ref_number)
                                            ->where('factory_id', $data->factory_id)
                                            ->whereNull('package.deleted_at')
                                            ->count();

                        //get qty package per department
                        $qty_shipping = $this->getQtyPackage($data->po_number, 'shipping', $data->plan_ref_number, $data->factory_id);

                        //get total qty from status_qcinspect = completed
                        $qty_shipping_completed = DB::table('package')
                                                       ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                                                       ->where('po_number', $data->po_number)
                                                       ->where('plan_ref_number', $data->plan_ref_number)
                                                       ->where('factory_id', $data->factory_id)
                                                       ->where('status_shipping', 'completed')
                                                       ->whereNull('package.deleted_at')
                                                       ->whereNull('package_detail.deleted_at')
                                                       ->count();

                        //get status package per department (completed / onprogress / waiting)
                        $status_shipping = $this->getStatusPerDepartment($data->po_number, $total_package, $qty_shipping, 'shipping', 'status_shipping', $qty_shipping_completed, $data->plan_ref_number, $data->factory_id);
                        $status = $this->showStatus($data->factory_id, $status_shipping, $qty_shipping);
                        return $status;
                    })
                    ->addColumn('action', function($data) {
                        return view('_action', [
                                    'model' => $data,
                                    'view_package' => route('viewPackage',
                                                                [
                                                                    'po_number' => $data->po_number,
                                                                    'plan_ref_number' => $data->plan_ref_number
                                                                ]
                                                            )
                                    ]);
                    })
                    ->rawColumns(['total_package','preparation',
                                  'sewing','inventory','qcinspect','shipping',
                                  'action'])
                    ->make(true);
        }
*/

        if ($filter_click == 1) {
            //get data
            if ($request->frequency) {
                $frequency = strtolower($request->frequency);
                $data = $this->getAllQtyPackage($factory_id, $frequency, $mode, $dt, null, $roles, $po, $radio_status, $date_range_plan, $date_podd);
            } else {
                $data = $this->getAllQtyPackage($factory_id, 'yearly', $mode, $dt, null, $roles, $po, $radio_status, $date_range_plan, $date_podd);
            }


            if ($data === false) {
                return false;
            }
            // return $data->get();
            //datatables
            return DataTables::of($data)
                ->addColumn('po_number', function($data){
                    return $data->po_number; 
                })
                ->addColumn('order_status', function ($data) {
                    $getstatus = DB::table('po_summary')
                        ->where('po_number', $data->po_number)
                        ->where('plan_ref_number', $data->plan_ref_number)
                        ->where('factory_id', $data->factory_id)
                        ->whereNull('deleted_at')->first();
                    if (isset($getstatus) && $getstatus->is_cancel_order == true) {
                        $badge = '<span class="badge badge-danger position-right">CANCELED</span>';
                    } else {
                        $badge = '';
                    }

                    $grade = DB::table('po_master_sync')->where('po_number', $data->po_number)->select('po_grade')->first();

                    if (empty($grade)) {
                        $bgr = '';
                    } else {
                        if ($grade->po_grade != 'A') {
                            $bgr = '<span class="badge badge-warning position-right">' . $grade->po_grade . '</span>';
                        } else {
                            $bgr = '';
                        }
                    }

                    return $badge . ' ' . $bgr;
                })
                ->editColumn('brand', function ($data) {
                    $brand = DB::table('po_summary')
                        ->select('trademark')
                        ->where('po_number', $data->po_number)
                        ->where('plan_ref_number', $data->plan_ref_number)
                        ->wherenull('deleted_at')
                        ->first();

                    return (isset($brand->trademark))? $brand->trademark : '-';
                })
                ->editColumn('art_no', function ($data) {
                    // $brand = DB::table('package_detail')
                    //             ->select('buyer_item')
                    //             ->where('po_number',$data->po_number)
                    //             ->where('plan_ref_number',$data->plan_ref_number)
                    //             ->wherenull('deleted_at')
                    //             ->groupBy('buyer_item')
                    //             ->groupBy('po_number')
                    //             ->first();
                    $art = DB::table('v_ns_article')->where('po_number', $data->po_number)->where('plan_ref_number', $data->plan_ref_number)->first();
                    return (isset($art->art))? $art->art : '-';
                })
                ->editColumn('new_qty', function ($data) {
                    $qty = DB::table('package_detail')
                        ->where('po_number', $data->po_number)
                        ->where('plan_ref_number', $data->plan_ref_number)
                        ->whereNull('deleted_at')
                        ->sum('item_qty');
                    return $qty;
                })
                ->addColumn('qty_intg', function ($data) {
                    $qty_intg = DB::table('package')
                        ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
                        ->where('package_detail.po_number', $data->po_number)
                        ->where('package_detail.plan_ref_number', $data->plan_ref_number)
                        ->where('package.is_integrate', true)
                        ->where('package.is_integrate', '<>', null)
                        ->whereNull('package_detail.deleted_at')
                        ->whereNull('package.deleted_at')
                        ->count();
                    return $qty_intg;
                })
                ->editColumn('total_package', function ($data) {
                    $total_package = DB::table('package')
                        ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
                        ->where('po_number', $data->po_number)
                        ->where('plan_ref_number', $data->plan_ref_number)
                        ->where('factory_id', $data->factory_id)
                        ->whereNull('package.deleted_at')
                        ->whereNull('package_detail.deleted_at')
                        ->count();
                    return $total_package;
                })
                ->editColumn('preparation', function ($data) {
                    //total package
                    $total_package = DB::table('package')
                        ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
                        ->where('po_number', $data->po_number)
                        ->where('plan_ref_number', $data->plan_ref_number)
                        ->where('factory_id', $data->factory_id)
                        ->whereNull('package.deleted_at')
                        ->whereNull('package_detail.deleted_at')
                        ->count();

                    //get qty package per department
                    $qty_preparation = $this->getQtyPackage($data->po_number, 'preparation', $data->plan_ref_number, $data->factory_id);

                    //get status package per department (completed / onprogress / waiting)
                    $status_preparation = $this->getStatusPerDepartment($data->po_number, $total_package, $qty_preparation, 'preparation', 'status_preparation', null, $data->plan_ref_number, $data->factory_id);

                    $status = $this->showStatus($data->factory_id, $status_preparation);

                    return $status;
                })
                ->editColumn('sewing', function ($data) {
                    //total package
                    $total_package = DB::table('package')
                        ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
                        ->where('po_number', $data->po_number)
                        ->where('plan_ref_number', $data->plan_ref_number)
                        ->where('factory_id', $data->factory_id)
                        ->whereNull('package.deleted_at')
                        ->whereNull('package_detail.deleted_at')
                        ->count();

                    //get qty package per department
                    $qty_sewing = $this->getQtyPackage($data->po_number, 'sewing', $data->plan_ref_number, $data->factory_id);

                    //get status package per department (completed / onprogress / waiting)
                    $status_sewing = $this->getStatusPerDepartment($data->po_number, $total_package, $qty_sewing, 'sewing', 'status_sewing', null, $data->plan_ref_number, $data->factory_id);
                    $status = $this->showStatus($data->factory_id, $status_sewing);
                    return $status;
                })
                ->editColumn('inventory', function ($data) {
                    //total package
                    $total_package = DB::table('package')
                        ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
                        ->where('po_number', $data->po_number)
                        ->where('plan_ref_number', $data->plan_ref_number)
                        ->where('factory_id', $data->factory_id)
                        ->whereNull('package.deleted_at')
                        ->whereNull('package_detail.deleted_at')
                        ->count();

                    //get qty package per department
                    $qty_inventory = $this->getQtyPackage($data->po_number, 'inventory', $data->plan_ref_number, $data->factory_id);

                    //get status package per department (completed / onprogress / waiting)
                    $status_inventory = $this->getStatusPerDepartment($data->po_number, $total_package, $qty_inventory, 'inventory', 'status_inventory', null, $data->plan_ref_number, $data->factory_id);
                    $status = $this->showStatus($data->factory_id, $status_inventory);
                    return $status;
                })
                ->editColumn('qcinspect', function ($data) {
                    //total package
                    $total_package = DB::table('package')
                        ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
                        ->where('po_number', $data->po_number)
                        ->where('plan_ref_number', $data->plan_ref_number)
                        ->where('factory_id', $data->factory_id)
                        ->whereNull('package.deleted_at')
                        ->whereNull('package_detail.deleted_at')
                        ->count();

                    //get qty package per department
                    $qty_qcinspect = $this->getQtyPackage($data->po_number, 'qcinspect', $data->plan_ref_number, $data->factory_id);

                    //get total qty from status_qcinspect = completed
                    $qty_qcinspect_completed = DB::table('package')
                        ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                        ->where('po_number', $data->po_number)
                        ->where('plan_ref_number', $data->plan_ref_number)
                        ->where('factory_id', $data->factory_id)
                        ->where('status_qcinspect', 'completed')
                        ->whereNull('package.deleted_at')
                        ->whereNull('package_detail.deleted_at')
                        ->count();

                    //get status package per department (completed / onprogress / waiting)
                    $status_qcinspect = $this->getStatusPerDepartment($data->po_number, $total_package, $qty_qcinspect, 'qcinspect', 'status_qcinspect', $qty_qcinspect_completed, $data->plan_ref_number, $data->factory_id);
                    $status = $this->showStatus($data->factory_id, $status_qcinspect, $qty_qcinspect);
                    return $status;
                })
                ->editColumn('shipping', function ($data) {
                    //total package
                    $total_package = DB::table('package')
                        ->join('package_detail', 'package_detail.scan_id', '=', 'package.scan_id')
                        ->where('po_number', $data->po_number)
                        ->where('plan_ref_number', $data->plan_ref_number)
                        ->where('factory_id', $data->factory_id)
                        ->whereNull('package.deleted_at')
                        ->whereNull('package_detail.deleted_at')
                        ->count();

                    //get qty package per department
                    $qty_shipping = $this->getQtyPackage($data->po_number, 'shipping', $data->plan_ref_number, $data->factory_id);

                    //get total qty from status_qcinspect = completed
                    $qty_shipping_completed = DB::table('package')
                        ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                        ->where('po_number', $data->po_number)
                        ->where('plan_ref_number', $data->plan_ref_number)
                        ->where('factory_id', $data->factory_id)
                        ->where('status_shipping', 'completed')
                        ->whereNull('package.deleted_at')
                        ->whereNull('package_detail.deleted_at')
                        ->count();

                    //get status package per department (completed / onprogress / waiting)
                    $status_shipping = $this->getStatusPerDepartment($data->po_number, $total_package, $qty_shipping, 'shipping', 'status_shipping', $qty_shipping_completed, $data->plan_ref_number, $data->factory_id);
                    $status = $this->showStatus($data->factory_id, $status_shipping, $qty_shipping);
                    return $status;
                })
                ->editColumn('plan_stuffing', function ($data) {
                    if (empty($data->plan_stuffing) || !isset($data->plan_stuffing)) {
                        return '-';
                    } else {
                        return Carbon::parse($data->plan_stuffing)->format('d/m/Y');
                    }
                })
                ->addColumn('action', function ($data) {
                    // return view('_action', [
                    //             'model' => $data,
                    //             'view_package' => route('viewPackage',
                    //                                         [
                    //                                             'po_number' => $data->po_number,
                    //                                             'plan_ref_number' => $data->plan_ref_number
                    //                                         ]
                    //                                     )
                    //             ]);
                    $view_package = route(
                        'viewPackage',
                        [
                            'po_number' => $data->po_number,
                            'plan_ref_number' => $data->plan_ref_number
                        ]
                    );

                    return '<a type="button" class="btn btn-default" target="_blank" href="' . $view_package . '" title="View Package"><i class="icon-stack2"></a>';
                })
                ->rawColumns([
                    'order_status', 'total_package', 'preparation',
                    'sewing', 'inventory', 'qcinspect', 'shipping',
                    'action'
                ])
                ->make(true);
        } else {
            $data = array();
            return Datatables::of($data)
                ->addColumn('action', function ($data) {
                    return null;
                })
                ->rawColumns([
                    'total_package', 'preparation',
                    'sewing', 'inventory', 'qcinspect', 'shipping',
                    'action'
                ])
                ->make(true);
        }
    }

    public function showStatus($factory_id, $department, $qty_department = null)
    {
        if ($department['status'] == 'completed') {
            $data = '<span class="badge badge-success position-right">Completed</span>';
            if ($department['department'] == 'qcinspect' || $department['department'] == 'shipping') {
                if ($qty_department > 0) {
                    $data .= '<span class="badge badge-success position-right">' . $qty_department . '</span>';
                }
            } elseif ($department['department'] == 'inventory') {
                //get count package that already passed inventory placement
                $completed_package = DB::table('package')
                    ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                    ->where('package_detail.po_number', $department['po_number'])
                    ->where('package_detail.plan_ref_number', $department['plan_ref_number'])
                    ->where('package_detail.factory_id', $factory_id)
                    ->whereNotNull('status_inventory')
                    ->whereNull('package.deleted_at')
                    ->whereNull('package_detail.deleted_at')
                    ->count();

                if ($department['total_package'] == 0) {
                    $persenan = 0;
                } else {
                    $persenan = ($completed_package / $department['total_package']) * 100;
                    $persenan = number_format($persenan, 2);
                }
                $data .= '<span style="float:right">' . $persenan . ' %</span>';
            }
        } elseif ($department['status'] == 'waiting') {
            $data = '<span class="badge badge-default position-right">Waiting</span>';
            if ($department['department'] == 'inventory') {
                //get count package that already passed inventory placement
                $completed_package = DB::table('package')
                    ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                    ->where('package_detail.po_number', $department['po_number'])
                    ->where('package_detail.plan_ref_number', $department['plan_ref_number'])
                    ->where('package_detail.factory_id', $factory_id)
                    ->whereNotNull('status_inventory')
                    ->whereNull('package.deleted_at')
                    ->whereNull('package_detail.deleted_at')
                    ->count();

                if ($department['total_package'] == 0) {
                    $persenan = 0;
                } else {
                    $persenan = ($completed_package / $department['total_package']) * 100;
                    $persenan = number_format($persenan, 2);
                }
                $data .= '<span style="float:right">' . $persenan . ' %</span>';
            }
        } else {
            $data = '';
            if ($department['department'] != 'shipping') {
                $data .= '<span class="badge badge-danger position-right">' . $department['qty_rejected'] . '</span>';
            }
            $data .= '<span class="badge badge-primary position-right">' . $department['qty_onprogress'] . '</span>';
            $data .= '<span class="badge badge-success position-right">' . $department['qty_completed'] . '</span>';
            if ($department['department'] == 'inventory') {
                //get count package that already passed inventory placement
                $completed_package = DB::table('package')
                    ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                    ->where('package_detail.po_number', $department['po_number'])
                    ->where('package_detail.plan_ref_number', $department['plan_ref_number'])
                    ->where('package_detail.factory_id', $factory_id)
                    ->whereNotNull('status_inventory')
                    ->whereNull('package.deleted_at')
                    ->whereNull('package_detail.deleted_at')
                    ->count();

                if ($department['total_package'] == 0) {
                    $persenan = 0;
                } else {
                    $persenan = ($completed_package / $department['total_package']) * 100;
                    $persenan = number_format($persenan, 2);
                }
                $data .= '<span style="float:right">' . $persenan . ' %</span>';
            } elseif ($department['department'] == 'qcinspect') {
                $another_data = DB::table('po_summary')
                    ->where('po_number', $department['po_number'])
                    ->where('plan_ref_number', $department['plan_ref_number'])
                    ->where('factory_id', $factory_id)
                    ->whereNull('deleted_at')
                    ->first();
                if (!$another_data) {
                    $completed_qty_qc = 0;
                    $sample_size_qc = 0;
                    $persen_qcinspect = 0;
                } else {
                    $completed_qty_qc = $another_data->completed_qty_qc;
                    $sample_size_qc = $another_data->sample_size_qc;
                    $persen_qcinspect = number_format($another_data->persen_qcinspect, 2);
                }
                $data .= '<span style="float:right"> ' . $persen_qcinspect . ' %</span>';
            }
        }
        return $data;
    }

    //get qty package per department
    public function getQtyPackage($po_number, $department, $plan_ref_number = null, $factory_id)
    {
        $data = DB::table('package')
            ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
            ->where('po_number', $po_number)
            ->where('plan_ref_number', $plan_ref_number)
            ->where('factory_id', $factory_id)
            ->where('current_department', $department)
            ->whereNull('package.deleted_at')
            ->whereNull('package_detail.deleted_at')
            ->count();

        return $data;
    }

    //get status per department
    public function getStatusPerDepartment($po_number, $qty_po, $qty_department, $current_department, $department, $qty_department_completed = null, $plan_ref_number = null, $factory_id)
    {
        if ($qty_department > 0) {
            if (
                ($current_department == 'shipping' && $qty_department_completed == $qty_po) ||
                ($current_department == 'qcinspect' && $qty_department_completed == $qty_po)
            ) {
                $qty_completed = DB::table('package')
                    ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                    ->where('po_number', $po_number)
                    ->where('plan_ref_number', $plan_ref_number)
                    ->where('factory_id', $factory_id)
                    ->where('current_department', $current_department)
                    ->where('current_status', 'completed')
                    ->whereNull('package.deleted_at')
                    ->count();

                $data['qty_completed'] = $qty_completed;
                $data['qty_rejected'] = 0;
                $data['qty_onprogress'] = 0;
                $data['status'] = 'completed';
                $data['total_package'] = $qty_po;
                $data['department'] = $current_department;
                $data['po_number'] = $po_number;
                $data['plan_ref_number'] = $plan_ref_number;
            } else {
                $qty_completed = DB::table('package')
                    ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                    ->where('po_number', $po_number)
                    ->where('plan_ref_number', $plan_ref_number)
                    ->where('factory_id', $factory_id)
                    ->where('current_department', $current_department)
                    ->where('current_status', 'completed')
                    ->whereNull('package.deleted_at')
                    ->count();

                $qty_rejected = DB::table('package')
                    ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                    ->where('po_number', $po_number)
                    ->where('plan_ref_number', $plan_ref_number)
                    ->where('factory_id', $factory_id)
                    ->where('current_department', $current_department)
                    ->where('current_status', 'rejected')
                    ->whereNull('package.deleted_at')
                    ->count();

                $qty_onprogress = DB::table('package')
                    ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                    ->where('po_number', $po_number)
                    ->where('plan_ref_number', $plan_ref_number)
                    ->where('factory_id', $factory_id)
                    ->where('current_department', $current_department)
                    ->where('current_status', 'onprogress')
                    ->whereNull('package.deleted_at')
                    ->count();

                $data['status'] = 'onprogress';
                $data['qty_completed'] = $qty_completed;
                $data['qty_rejected'] = $qty_rejected;
                $data['qty_onprogress'] = $qty_onprogress;
                $data['total_package'] = $qty_po;
                $data['department'] = $current_department;
                $data['po_number'] = $po_number;
                $data['plan_ref_number'] = $plan_ref_number;
            }
        } else {
            $qty_completed = DB::table('package')
                ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
                ->where('po_number', $po_number)
                ->where('plan_ref_number', $plan_ref_number)
                ->where('factory_id', $factory_id)
                ->where($department, 'completed')
                ->whereNull('package.deleted_at')
                ->count();
            if ($qty_completed == $qty_po) {
                $data['qty_completed'] = $qty_completed;
                $data['qty_rejected'] = 0;
                $data['qty_onprogress'] = 0;
                $data['status'] = 'completed';
                $data['total_package'] = $qty_po;
                $data['department'] = $current_department;
                $data['po_number'] = $po_number;
                $data['plan_ref_number'] = $plan_ref_number;
            } else {
                $data['status'] = 'waiting';
                $data['total_package'] = $qty_po;
                $data['department'] = $current_department;
                $data['po_number'] = $po_number;
                $data['plan_ref_number'] = $plan_ref_number;
            }
        }

        return $data;
    }

    //HALAMAN VIEW PACKAGE
    public function viewPackage(Request $request)
    {
        $po_number = trim($request->po_number);
        $plan_ref_number = trim($request->plan_ref_number);
        return view('dashboard/detail')->with('po_number', $po_number)
            ->with('plan_ref_number', $plan_ref_number);
    }

    //DATA FOR DATATABLES VIEW PACKAGE
    public function viewPackageData(Request $request)
    {
        $ponumber = trim($request->po_number);
        $plan_ref_number = trim($request->plan_ref_number);
        $data     = DB::table('package')
            ->join('package_detail', 'package.scan_id', '=', 'package_detail.scan_id')
            ->leftjoin('package_movements', 'package.barcode_id', '=', 'package_movements.barcode_package')
            ->leftjoin('locators', 'package_movements.barcode_locator', '=', 'locators.barcode')
            ->leftjoin('line', 'package_movements.line_id', '=', 'line.id')
            ->where('package_detail.po_number', $ponumber)
            ->where('package_detail.plan_ref_number', $plan_ref_number)
            // ->where('package_detail.factory_id', Auth::user()->factory_id)
            ->where('package_movements.is_canceled', false)
            ->whereNull('package.deleted_at')
            ->whereNull('package_movements.deleted_at')
            ->whereNull('locators.deleted_at')
            ->whereNull('line.deleted_at')
            ->orderBy('package.barcode_id', 'asc')
            ->select('package.barcode_id', 'package.package_number', 'package.current_department', 'package.current_department', 'package.current_status', 'package_detail.customer_size', 'package_detail.manufacturing_size', 'package_detail.length', 'package_detail.width', 'package_detail.height', 'package_detail.inner_pack', 'package_detail.scan_id', 'locators.code', 'line.description', 'package_detail.unit_2', 'package_detail.net', 'package_detail.gross', 'package_detail.unit_1')
            ->get();
        // return $data;
        return Datatables::of($data)
            ->editColumn('barcode_id', function ($data) {
                $detail = '<a href="#" class="modal_package"
                                    [detail]
                                   </a>';
                return $data->barcode_id . ' ' . $detail;
            })
            ->editColumn('current_department', function ($data) {
                if ($data->current_department == 'sewing') {
                    $current_department = '<span class="label label-flat border-grey text-grey-600">' . $data->current_department . ' #' . $data->description . '</span>';
                } else {
                    $current_department = '<span class="label label-flat border-grey text-grey-600">' . $data->current_department . '</span>';
                }
                return $current_department;
            })
            ->editColumn('current_status', function ($data) {
                if ($data->current_status == 'onprogress') {
                    $current_status = '<td><span class="label label-primary" id="current_status_' . $data->barcode_id . '">ON PROGRESS</span></td>';
                } elseif ($data->current_status == 'completed') {
                    $current_status = '<td><span class="label label-success" id="current_status_' . $data->barcode_id . '">COMPLETED</span></td>';
                } else {
                    $current_status = '<td><span class="label label-danger" id="current_status_' . $data->barcode_id . '">REJECTED</span></td>';
                }
                return $current_status;
            })
            ->editColumn('net', function($data){
                return $data->net.' ('.$data->unit_1.')';
            })
            ->editColumn('gross', function($data){
                return $data->gross.' ('.$data->unit_1.')';
            })
            ->editColumn('dimension', function ($data) {
                $dimension = $data->length . ' x ' . $data->width . ' x ' . $data->height . ' (' . $data->unit_2 . ')';
                return $dimension;
            })
            ->editColumn('location', function ($data) {
                if ($data->code) {
                    $location = $data->code;
                } else {
                    $location = '-';
                }
                return $location;
            })
            // ->addColumn('action', function($data) {
            //      return view('_action', [
            //                  'model' => $data,
            //                  'print' => route('printBarcodePackage',
            //                                   [
            //                                       'po_number'  => $data->po_number,
            //                                       'barcode_id' => $data->barcode_id
            //                                   ]),
            //                  ]);
            // })
            ->rawColumns(['barcode_id', 'current_status', 'current_department', 'net', 'gross'])
            ->make(true);
    }

    //PRINT BARCODE PACKAGE
    public function printBarcodePackage(Request $request)
    {
    }

    // get count request carton
    public function getDataCarton(Request $request)
    {
        $factory_id = $request->factory_id ? $request->factory_id : Auth::user()->factory_id;

        $get_request_carton = DB::table('request_carton')
            ->join('line', 'request_carton.line_id', '=', 'line.id')
            ->where('request_carton.status_sewing', 'request')
            ->where('line.factory_id', $factory_id)
            ->wherenull('request_carton.deleted_at')
            ->wherenull('line.deleted_at')
            ->count();

        $total_request_change = $get_request_carton;

        //
        $count_waiting = $this->getQtyRequestCarton(Auth::user()->factory_id, 'Waiting');
        $count_onprogress = $this->getQtyRequestCarton(Auth::user()->factory_id, 'onprogress');
        $count_hold = $this->getQtyRequestCarton(Auth::user()->factory_id, 'hold');
        $count_done = $this->getQtyRequestCarton(Auth::user()->factory_id, 'done');

        return response()->json([
            'total_request_change' => $total_request_change,
            'count_waiting' => $count_waiting,
            'count_onprogress' => $count_onprogress,
            'count_hold' => $count_hold,
            'count_done' => $count_done
        ], 200);
    }

    // get qty request carton
    public function getQtyRequestCarton($factory_id, $status)
    {
        $data = DB::table('request_carton')
            ->join('line', 'line.id', '=', 'request_carton.line_id')
            ->where('line.factory_id', $factory_id)
            ->wherenull('request_carton.deleted_at')
            ->wherenull('line.deleted_at');

        if ($status == 'Waiting') {
            return $data = $data->wherenull('request_carton.status_packing')->count();
        } elseif ($status == 'onprogress') {
            return $data = $data->where('request_carton.status_packing', 'onprogress')->count();
        } elseif ($status == 'hold') {
            return $data = $data->where('request_carton.status_packing', 'hold')->count();
        } elseif ($status == 'done') {
            return $data = $data->where('request_carton.status_packing', 'done')->count();
        } else {
            return 0;
        }
    }

    //export dashboard
    public function dashboardExport(Request $request)
    {
        $radio = $request->radio;
        $param = $request->param;
        $data = DB::table('v_ns_export_dashboard')
            ->where('factory_id', $request->factory_id);
        // ->whereNull('deleted_at')
        // ->orderBy('po_number', 'asc');

        if ($radio == 'po') {
            $filby = 'filterby_po_' . trim($param);
            $data = $data->where('po_number', 'LIKE', '%' . trim($param) . '%');
        } else if ($radio == 'plan') {
            $date_range = explode('-', preg_replace('/\s+/', '', $param));
            $from = date_format(date_create($date_range[0]), 'Y-m-d');
            $to = date_format(date_create($date_range[1]), 'Y-m-d');
            $data = $data->whereBetween('plan_stuffing', [$from, $to]);
            $filby = 'filterby_planstuffing_' . $from . '-' . $to;
        } else if ($radio == 'podd') {
            $date_rangep = explode('-', preg_replace('/\s+/', '', $param));
            $fromp = date_format(date_create($date_rangep[0]), 'Y-m-d');
            $top = date_format(date_create($date_rangep[1]), 'Y-m-d');
            $data = $data->whereBetween('podd_check', [$fromp, $top]);
            $filby = 'filterby_podd_' . $fromp . '-' . $top;
        }

        $data = $data->orderBy('plan_stuffing', 'asc')
            ->orderBy('fwd', 'asc')
            ->orderBy('invoice', 'asc')
            ->orderBy('podd_check', 'asc');
        //naming file
        $get_factory = DB::table('factory')
            ->where('factory_id', $request->factory_id)
            ->wherenull('deleted_at')
            ->first();

        $filename = $get_factory->factory_name . '_dashboard_' . $filby;
        $i = 1;

        $export = \Excel::create($filename, function ($excel) use ($data, $i) {
            $excel->sheet('report', function ($sheet) use ($data, $i) {
                $sheet->appendRow(array(
                    'Plan Stuffing',
                    'PO',
                    'Plan Ref',
                    'Brand',
                    'Style',
                    'Art. NO',
                    'Invoice',
                    'PODD',
                    'Dest',
                    'Qty',
                    'FWD',
                    'Shipmode',
                    'Total Package',
                    'Preparation',
                    'Sewing Onprogress',
                    'Sewing Completed',
                    'Inventory',
                    'QC Inspect',
                    'Loading',
                    'Shipping'
                ));
                $data->chunk(100, function ($rows) use ($sheet, $i) {
                    foreach ($rows as $rw) {
                        $art = DB::table('v_ns_article')->where('po_number', $rw->po_number)->where('plan_ref_number', $rw->plan_ref_number)->first();
                        //
                        $sheet->appendRow(array(
                            $rw->plan_stuffing,
                            '="' . $rw->po_number . '"',
                            '="' . $rw->plan_ref_number . '"',
                            $rw->trademark,
                            $rw->style,
                            isset($art->art) ? $art->art : '-',
                            $rw->invoice,
                            $rw->podd_check,
                            $rw->country_name,
                            $rw->qty,
                            $rw->fwd,
                            $rw->shipmode,
                            $rw->ctn,
                            $rw->preparation,
                            $rw->sewing_onprogress,
                            $rw->sewing_completed,
                            $rw->inventory,
                            $rw->qcinspect,
                            $rw->loading,
                            $rw->shipping
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }

    public function sync_cancel()
    {
        try {
            $getCancel = DB::table('po_summary_sync')
                ->select('po_summary_sync.po_number', 'po_summary.plan_ref_number')
                ->join('po_summary', 'po_summary.po_number', '=', 'po_summary_sync.po_number')
                ->where(function ($query) {
                    $query->Where('po_summary_sync.is_cancel_order', true);
                    $query->orWhere('po_summary_sync.is_re_route', true);
                })
                ->where('po_summary.is_cancel_order', false)
                ->whereNull('po_summary.deleted_at')
                ->get();

            foreach ($getCancel as $gc) {
                DB::table('po_summary')
                    ->where('po_number', $gc->po_number)
                    ->where('plan_ref_number', $gc->plan_ref_number)
                    ->wherenull('deleted_at')
                    ->update(['is_cancel_order' => true, 'updated_at' => Carbon::now()]);

                DB::table('package_detail')
                    ->where('po_number', $gc->po_number)
                    ->where('plan_ref_number', $gc->plan_ref_number)
                    ->wherenull('deleted_at')
                    ->update(['is_cancel_order' => true, 'updated_at' => Carbon::now()]);
            }
        } catch (Exception $er) {
            DB::rollback();
            $message = $er->getMessage();
            ErrorHandler::db($message);
        }
    }
}
