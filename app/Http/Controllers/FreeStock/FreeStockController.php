<?php

namespace App\Http\Controllers\FreeStock;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

use Rap2hpoutre\FastExcel\FastExcel;

class FreeStockController extends Controller
{
    public function checkIn(){
    	return view('free_stock/check_in/index');
    }

    public function ajaxSetChcekIn(Request $request){
    	$barcode_id = $request->barcode_id;
    	$status = 'checkin';
    	$cek_pack =$this->cek_status($barcode_id,$status);
        $data = array();
        $ip_address = $request->ip();
    	$data_in = array();
        try {
            if ($cek_pack['permission']==true) {
                $barcode_cek = DB::table('package')
                                    ->join('package_detail','package.scan_id','=','package_detail.scan_id')
                                    ->where('package.barcode_id',$barcode_id)
                                    ->whereNull('package.deleted_at')
                                    ->whereNull('package_detail.deleted_at')
                                    ->select('package.barcode_id','package_detail.manufacturing_size','package_detail.inner_pack','package_detail.buyer_item')->first();

                $cek_qty = DB::table('v_ns_pdetail')
                            ->join('package','v_ns_pdetail.scan_id','=','package.scan_id')
                            ->where('package.barcode_id',$barcode_id)
                            ->whereNull('package.deleted_at')
                            ->select('v_ns_pdetail.po_number','v_ns_pdetail.manufacturing_size','v_ns_pdetail.inner_pack','v_ns_pdetail.buyer_item','package.barcode_id')
                            ->get();

                if ($cek_pack['qcinspect']==100) {
                    $isqc = true;
                }else{
                    $isqc = false;
                }

                db::begintransaction();
                    foreach ($cek_qty as $cq) {
                        $detail['barcode_id']=$cq->barcode_id ;
                        $detail['po_number']=$cq->po_number ;
                        $detail['upc']=$cek_pack['upc'] ;
                        $detail['buyer_item']=$cq->buyer_item ;
                        $detail['manufacturing_size']=$cq->manufacturing_size ;
                        $detail['inner_pack']=$cq->inner_pack ;
                        $detail['is_qcinspect']=$isqc ;
                        $detail['created_at']=carbon::now() ;
                        $detail['factory_id']=Auth::user()->factory_id ;
                        // $detail['qty_ori']=$cq->inner_pack ;
                        $data_in[] = $detail;
                    }
                    DB::table('free_stock_in')->insert($data_in);
                    $dx = array(
                        'barcode_id'=>$barcode_id,
                        'style'=>$cek_pack['upc'],
                        'buyer_item'=>$barcode_cek->buyer_item,
                        'manufacturing_size'=>$barcode_cek->manufacturing_size,
                        'qty'=>$barcode_cek->inner_pack
                    );

                    $data[]=$dx;
                    $this->_query_movements($barcode_id,$status,$ip_address,false);
                db::commit();

                return  view('free_stock/check_in/ajax_list')->with('data',$data);
            }else{
                $err = 'Barcode '.$barcode_id.' last location in '.$cek_pack['current_department'].' '.$cek_pack['current_status'];

                return response()->json($err,422);
            }
        } catch (Exception $e) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
                return response()->json($message,422);
        }
    	
    }

    private function cek_status($barcode_id,$status){
    	$package = DB::table('package')
    					->join('package_detail','package.scan_id','=','package_detail.scan_id')
    					->join('po_summary',function($join){
    						$join->on('package_detail.po_number','=','po_summary.po_number');
    						$join->on('package_detail.plan_ref_number','=','po_summary.plan_ref_number');
    					})
    					->where('package.barcode_id',$barcode_id)
    					->where('po_summary.factory_id',Auth::user()->factory_id)
    					->whereNull('package.deleted_at')
    					->whereNull('package_detail.deleted_at')
    					->whereNull('po_summary.deleted_at')
    					->select('package.current_department','package.current_status','po_summary.upc','po_summary.po_number','po_summary.factory_id','package_detail.scan_id','po_summary.season','po_summary.persen_qcinspect')
    					->first();


    	switch ($status) {
    		case 'checkin':
	    			if ((($package->current_department=='inventory') || ($package->current_department=='shipping' && $package->current_status=='onprogress'))) {
	    				$permission = true;
	    			}else{
	    				$permission = false;
	    			}
    			break;

    		case 'checkout':
	    			if ($package->current_department=='preparation' && $package->current_status=='completed') {
	    				$permission = true;
	    			}else{
	    				$permission = false;
	    			}
    			break;
    		
    		default:
    			$permission = false;
    			break;
    	}

    	
    	$per_array = array(
    		'permission'=>$permission,
    		'current_department'=>$package->current_department,
    		'current_status'=>$package->current_status,
    		'po_number'=>$package->po_number,
    		'upc'=>$package->upc,
    		'scan_id'=>$package->scan_id,
    		'season'=>$package->season,
            'qcinspect'=>$package->persen_qcinspect
    	);
    	
    	return $per_array;				
    } 

    // private function _query_movements($barcode_id,$status,$ip_address){

    //     switch ($status) {
    //         case 'checkin':
    //         try {
    //             db::begintransaction();
    //                 $last = DB::table('package_movements')
    //                         ->where('barcode_package',$barcode_id)
    //                         ->where('is_canceled',false)
    //                         ->whereNull('deleted_at')->first();
    //                 if (!$last) {
    //                      throw new \Exception('Package not found');
    //                 }else{
    //                     DB::table('package_movements')
    //                     ->where('id', $last->id)
    //                     ->update([
    //                         'deleted_at' => Carbon::now()
    //                     ]);
    //                 }

    //                 $dt_move = array(
    //                     'barcode_package'=>$barcode_id,
    //                     'department_from'=>$last->department_to,
    //                     'department_to'=>'preparation',
    //                     'status_from'=>$last->status_to,
    //                     'status_to'=>'completed',
    //                     'user_id'=>Auth::user()->id,
    //                     'created_at'=>Carbon::now(),
    //                     'is_canceled'=>false,
    //                     'ip_address'=>$ip_address,
    //                     'description'=>'check in to free stok'
    //                 );
    //                 $dt_pk = array(
    //                     'updated_at'=>Carbon::now(),
    //                     'current_department'=>'preparation',
    //                     'current_status'=>'completed',
    //                     'status_preparation'=>'completed',
    //                     'status_sewing'=>null,
    //                     'status_inventory'=>null,
    //                     'status_qcinspect'=>null,
    //                     'status_shipping'=>null
    //                 );

    //                 DB::table('package_movements')->insert($dt_move);
    //                 DB::table('package')->where('barcode_id',$barcode_id)->update($dt_pk);
    //             db::commit();
    //         } catch (Exception $ex) {
    //             DB::rollback();
    //             $message = $ex->getMessage();
    //             ErrorHandler::db($message);
    //         }
                
    //         return true;
    //             break;
    //         //checkin
    //     }
    // }

    public function listFreeStock(){
        return view('free_stock/list_freestock');
    }

    public function getListFreeStock(){
        // $po_number = trim($request->po_number);


         $data = DB::table('free_stock')->where('factory_id',Auth::user()->factory_id)->where('qty','<>',0)->whereNull('deleted_at');

        // if (!empty($po_number)) {
        //     $data = $data->where('po_number',$po_number);
        // }else{
        //     $data = $data;
        // }

        return DataTables::of($data)
                    ->make(true);
    }

    public function exportListFreeStock(){
        // $po_number = trim($request->po_number);

        $get_factory = DB::table('factory')->where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->first();

        $data = DB::table('free_stock')->where('factory_id',Auth::user()->factory_id)->where('qty','<>',0)->whereNull('deleted_at');

        // if (!empty($po_number)) {
        //     $data = $data->where('po_number',$po_number);
        //     $name = "_po_number_".$po_number;
        // }else{
        //     $data = $data;
        //     $name = "_all";
        // }
        $i = 1;
        $filename = $get_factory->factory_name."_freestock_".date_format(Carbon::now(), 'Y-m-d');

        $data_result = $data->get();
        foreach ($data_result as $data_res) {
            $data_res->no = $i++;
        }
        if ($data_result->count()>0) {
                return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use ($i){
                        return 
                            [
                                '#'=>$row->no,
                                'Po Number'=>$row->po_number,
                                'Style'=>$row->upc,
                                'Season'=>$row->season,
                                'Art. No'=>$row->buyer_item,
                                'Manuf. Size'=>$row->manufacturing_size,
                                'Qty'=>$row->qty
                            ];
                });
        }else{
            return response()->json('no data', 422);
        }

    }

    public function allocation(){
        return view('free_stock/allocation_freestock');
    }

    public function templateAllocation(){
        $export = \Excel::create('templateAllocation',function($excel){
            $excel->sheet('report',function($sheet){
                $sheet->appendRow(array(
                    'po_number',
                    'old_po',
                    'style',
                    'article',
                    'manuf_size',
                    'qty'
                ));
            });
        })->download('xlsx');

        return response()->json('success exporting',200);
    }

    public function uploadAllocation(Request $request){
        $allo_in =array();
        $result = array();
        $factory_id = Auth::user()->factory_id;
         if($request->hasFile('file_upload')){
            $extension = \File::extension($request->file_upload->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls") {
                $path = $request->file_upload->getRealPath();
                $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
            }
        }
        
        try {
            db::begintransaction();
            foreach ($datax as $dx) {
                $stock = $this->_cek_stock(trim($dx->old_po),trim($dx->article),trim($dx->manuf_size),$factory_id);


                
                if ($stock->qty>=(int)$dx->qty) {
                   

                    $allo = $this->_cek_allocation(trim($dx->po_number),trim($dx->article),trim($dx->manuf_size),$factory_id);

                    

                    if ($allo==null) {
                       $dt_alloc_in = array(
                            'po_number'=>$dx->po_number,
                            'old_po'=>$dx->old_po,
                            'upc'=>$dx->style,
                            'buyer_item'=>$dx->article,
                            'manufacturing_size'=>$dx->manuf_size,
                            'qty_allocation'=>(int) $dx->qty,
                            'factory_id'=>$factory_id,
                            'created_at'=>Carbon::now(),
                            'is_qcinspect'=>$stock->is_qcinspect
                        );

                       db::table('allocation_freestock')->insert($dt_alloc_in);
                    }else{
                        $new_alloc =$allo->qty_allocation + $dx->qty;
                        $old_po = $allo->old_po.', '.$dx->old_po;
                        $dt_alloc_up = array(
                            'qty_allocation'=>(int) $new_alloc,
                            'old_po'=>$old_po,
                            'updated_at'=>Carbon::now(),
                            'is_qcinspect'=>$stock->is_qcinspect
                        );

                        db::table('allocation_freestock')->where('id',$allo->id)->update($dt_alloc_up);
                    }
                    
                    
                    $new_stock =$stock->qty-$dx->qty;
                    $new_allocation = $stock->allocation_qty+$dx->qty;

                    if ($new_stock==0) {
                        $dt_new_stok = array(
                            'qty'=>(int) $new_stock,
                            'allocation_qty'=>(int) $new_allocation,
                            'deleted_at'=>Carbon::now()
                        );
                    }else{
                        $dt_new_stok = array(
                            'qty'=>(int) $new_stock,
                            'allocation_qty'=>(int) $new_allocation,
                            'updated_at'=>Carbon::now()
                        );
                    }

                    db::table('free_stock')->where('id',$stock->id)->update($dt_new_stok);

                    
                }
            }
            $data_response = [
                            'status' => 200,
                            'output' => 'Upload allocation success . . .'
                          ];
            db::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Upload allocation success . . .'
                          ];
        }

        
        return response()->json(['data'=>$data_response]);
        
    }

    public function getDataAllocation(Request $request){
        $po_number = $request->po_number;

        $data = DB::table('allocation_freestock')->where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->where(function($query) use ($po_number){
                            $query->where('po_number','LIKE','%'.$po_number.'%')
                                    ->orwhere('po_number','LIKE','%'.$po_number.'%');
                        });
        return DataTables::of($data)
                            ->make(true);
    }

    public function checkOut(){
        return view('free_stock/check_out/index');
    }

    public function ajaxSetChcekOut(Request $request){
        $barcode_id = $request->barcode_id;
        $status = 'checkout';
        $cek_pack =$this->cek_status($barcode_id,$status);
        $data = array();
        $ip_address = $request->ip();
        $factory_id = Auth::user()->factory_id;
        $data = array();

        $qcinst = false;
       
        try {
           
            if ($cek_pack['permission']==true) {
                 db::begintransaction();
                $cek_qty = DB::table('v_ns_pdetail')
                            ->join('package','v_ns_pdetail.scan_id','=','package.scan_id')
                            ->where('package.barcode_id',$barcode_id)
                            ->whereNull('package.deleted_at')
                            ->select('v_ns_pdetail.po_number','v_ns_pdetail.manufacturing_size','v_ns_pdetail.inner_pack','v_ns_pdetail.buyer_item','package.barcode_id')
                            ->get();

                $barcode_cek = DB::table('package')
                                    ->join('package_detail','package.scan_id','=','package_detail.scan_id')
                                    ->where('package.barcode_id',$barcode_id)
                                    ->whereNull('package.deleted_at')
                                    ->whereNull('package_detail.deleted_at')
                                    ->select('package.barcode_id','package_detail.manufacturing_size','package_detail.inner_pack','package_detail.buyer_item')->first();

                foreach ($cek_qty as $cq) {
                    $alloc = $this->_cek_allocation($cq->po_number,$cq->buyer_item,$cq->manufacturing_size,$factory_id);

                    $new_scan = $alloc->qty_scan + $cq->inner_pack;

                    $dt_scan = array('qty_scan'=>$new_scan,'updated_at'=>Carbon::now());
                    db::table('allocation_freestock')->where('id',$alloc->id)->update($dt_scan);

                    $qcinst = $alloc->is_qcinspect;
                    
                }

                $dx = array(
                        'barcode_id'=>$barcode_id,
                        'style'=>$cek_pack['upc'],
                        'buyer_item'=>$barcode_cek->buyer_item,
                        'manufacturing_size'=>$barcode_cek->manufacturing_size,
                        'qty'=>$barcode_cek->inner_pack
                    );
                $data[]=$dx;
                $this->_query_movements($barcode_id,'checkout',$ip_address,$qcinst);
                db::commit();
                return  view('free_stock/check_out/ajax_list')->with('data',$data);
            }else{
                $err = 'Barcode '.$barcode_id.' last location in '.$cek_pack['current_department'].' '.$cek_pack['current_status'];

                return response()->json($err,422);
            }


        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }
        $barcode_id = $request->barcode_id;

    }


    private function _cek_allocation($po_number,$buyer_item,$manufacturing_size,$factory_id){
        $cekallow = DB::table('allocation_freestock')->where('po_number',$po_number)->where('buyer_item',$buyer_item)->where('manufacturing_size',$manufacturing_size)->where('factory_id',$factory_id)->whereNull('deleted_at')->first();

        return $cekallow;
    }

    private function _cek_stock($oldpo,$buyer_item,$manufacturing_size,$factory_id){
        $cekstock = DB::table('free_stock')->where('po_number',$oldpo)->where('buyer_item',$buyer_item)->where('manufacturing_size',$manufacturing_size)->where('factory_id',$factory_id)->whereNull('deleted_at')->first();

        return $cekstock;
    }

    private function _query_movements($barcode_id,$status,$ip_address,$isqc){
        $carbon = carbon::now();
        $dt_move = array();
        switch ($status) {
            case 'checkin':
            try {
                db::begintransaction();
                    $last = DB::table('package_movements')
                            ->where('barcode_package',$barcode_id)
                            ->where('is_canceled',false)
                            ->whereNull('deleted_at')->first();
                    if (!$last) {
                         throw new \Exception('Package not found');
                    }else{
                        DB::table('package_movements')
                        ->where('id', $last->id)
                        ->update([
                            'deleted_at' => Carbon::now()
                        ]);
                    }

                    $dt_move = array(
                        'barcode_package'=>$barcode_id,
                        'department_from'=>$last->department_to,
                        'department_to'=>'preparation',
                        'status_from'=>$last->status_to,
                        'status_to'=>'completed',
                        'user_id'=>Auth::user()->id,
                        'created_at'=>$carbon,
                        'is_canceled'=>false,
                        'ip_address'=>$ip_address,
                        'description'=>'check in to free stok'
                    );
                    $dt_pk = array(
                        'updated_at'=>$carbon,
                        'current_department'=>'preparation',
                        'current_status'=>'completed',
                        'status_preparation'=>'completed',
                        'status_sewing'=>null,
                        'status_inventory'=>null,
                        'status_qcinspect'=>null,
                        'status_shipping'=>null
                    );

                    DB::table('package_movements')->insert($dt_move);
                    DB::table('package')->where('barcode_id',$barcode_id)->update($dt_pk);
                db::commit();
            } catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }
                
            return true;
                break;
            //checkin

            case 'checkout':
            try {
                db::begintransaction();

                $last = DB::table('package_movements')
                            ->where('barcode_package',$barcode_id)
                            ->where('is_canceled',false)
                            ->whereNull('deleted_at')->first();
                if (!$last) {
                     throw new \Exception('Package not found');
                }else{
                    DB::table('package_movements')
                    ->where('id', $last->id)
                    ->update([
                        'deleted_at' => Carbon::now()
                    ]);
                }

                $line = db::table('line')->where('factory_id',Auth::user()->factory_id)->where('free_stock',true)->first();

                if ($isqc==true) {

                    $dt_pk = array(
                        'updated_at'=>$carbon,
                        'current_department'=>'inventory',
                        'current_status'=>'onprogress',
                        'status_preparation'=>'completed',
                        'status_sewing'=>'completed',
                        'status_inventory'=>'onprogress',
                        'status_qcinspect'=>'completed',
                        'status_shipping'=>null
                    ); 

                    

                }else{
                   
                   $dt_pk = array(
                        'updated_at'=>$carbon,
                        'current_department'=>'inventory',
                        'current_status'=>'onprogress',
                        'status_preparation'=>'completed',
                        'status_sewing'=>'completed',
                        'status_inventory'=>'onprogress',
                        'status_qcinspect'=>null,
                        'status_shipping'=>null
                    );
                }

                DB::table('package')->where('barcode_id',$barcode_id)->update($dt_pk);

                // $dt_sewing_in = array(
                //     'barcode_package'=>$barcode_id,
                //     'department_from'=>'preparation',
                //     'department_to'=>'sewing',
                //     'status_from'=>'completed',
                //     'status_to'=>'onprogress',
                //     'user_id'=>Auth::user()->id,
                //     'created_at'=>$carbon,
                //     'deleted_at'=>$carbon,
                //     'is_canceled'=>false,
                //     'ip_address'=>$ip_address,
                //     'description'=>'check in sewing from free stok',
                //     'line_id'=>$line->id
                // );
                // DB::table('package_movements')->insert($dt_sewing_in);

                // $dt_sewing_out = array(
                //     'barcode_package'=>$barcode_id,
                //     'department_from'=>'sewing',
                //     'department_to'=>'sewing',
                //     'status_from'=>'onprogress',
                //     'status_to'=>'completed',
                //     'user_id'=>Auth::user()->id,
                //     'created_at'=>$carbon,
                //     'deleted_at'=>$carbon,
                //     'is_canceled'=>false,
                //     'ip_address'=>$ip_address,
                //     'description'=>'completed sewing from free stok',
                //     'line_id'=>$line->id
                // );
                // DB::table('package_movements')->insert($dt_sewing_out);

                // $dt_inventory_in = array(
                //     'barcode_package'=>$barcode_id,
                //     'department_from'=>'sewing',
                //     'department_to'=>'inventory',
                //     'status_from'=>'completed',
                //     'status_to'=>'onprogress',
                //     'user_id'=>Auth::user()->id,
                //     'created_at'=>$carbon,
                //     'is_canceled'=>false,
                //     'ip_address'=>$ip_address,
                //     'description'=>'check in inventory from free stok',
                // );
                // DB::table('package_movements')->insert($dt_inventory_in);

                $sewingin['barcode_package']=$barcode_id;
                $sewingin['department_from']='preparation' ;
                $sewingin['department_to']='sewing';
                $sewingin['status_from']='completed' ;
                $sewingin['status_to']='completed' ;
                $sewingin['user_id']=Auth::user()->id ;
                $sewingin['created_at']=$carbon ;
                $sewingin['deleted_at']=$carbon ;
                $sewingin['is_canceled']=false ;
                $sewingin['ip_address']=$ip_address ;
                $sewingin['description']='check in sewing from free stok' ;
                $sewingin['line_id']=$line->id ;
                $dt_move[]=$sewingin;

                $sewingout['barcode_package']=$barcode_id;
                $sewingout['department_from']='sewing' ;
                $sewingout['department_to']='sewing';
                $sewingout['status_from']='onprogress' ;
                $sewingout['status_to']='completed' ;
                $sewingout['user_id']=Auth::user()->id ;
                $sewingout['created_at']=$carbon ;
                $sewingout['deleted_at']=$carbon ;
                $sewingout['is_canceled']=false ;
                $sewingout['ip_address']=$ip_address ;
                $sewingout['description']='completed sewing from free stok' ;
                $sewingout['line_id']=$line->id ;
                $dt_move[]=$sewingout;

                $inventoryin['barcode_package']=$barcode_id;
                $inventoryin['department_from']='sewing' ;
                $inventoryin['department_to']='inventory';
                $inventoryin['status_from']='completed' ;
                $inventoryin['status_to']='onprogress' ;
                $inventoryin['user_id']=Auth::user()->id ;
                $inventoryin['created_at']=$carbon ;
                $inventoryin['deleted_at']=null ;
                $inventoryin['is_canceled']=false ;
                $inventoryin['ip_address']=$ip_address ;
                $inventoryin['description']='check in inventory from free stok' ;
                $inventoryin['line_id']=null;
                $dt_move[]=$inventoryin;

                DB::table('package_movements')->insert($dt_move);

                db::commit();
                
            } catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }
            return true;
            break;
        }
    }

    public function stockCalculate(){
        $data = DB::table('v_ns_free_stock_in')->where('factory_id',Auth::user()->factory_id)->get();
        
        try {

            db::begintransaction();
            foreach ($data as $dt) {
                $cek_stock = DB::table('free_stock')->whereNull('deleted_at')->where('po_number',$dt->po_number)->where('buyer_item',$dt->buyer_item)->where('manufacturing_size',$dt->manufacturing_size)->where('factory_id',Auth::user()->factory_id)->first();

                if ($cek_stock==null) {
                    $instock = array(
                        'po_number'=>$dt->po_number,
                        'upc'=>$dt->upc,
                        'buyer_item'=>$dt->buyer_item,
                        'manufacturing_size'=>$dt->manufacturing_size,
                        'qty'=>$dt->qty,
                        'created_at'=>carbon::now(),
                        'factory_id'=>$dt->factory_id,
                        'is_qcinspect'=>$dt->is_qcinspect
                    );

                    DB::table('free_stock')->insert($instock);
                }else{
                    $qty = $cek_stock->qty + $dt->qty;
                    $upstock = array(
                        'po_number'=>$dt->po_number,
                        'upc'=>$dt->upc,
                        'buyer_item'=>$dt->buyer_item,
                        'manufacturing_size'=>$dt->manufacturing_size,
                        'qty'=>$qty,
                        'updated_at'=>carbon::now(),
                        'factory_id'=>$dt->factory_id,
                        'is_qcinspect'=>$dt->is_qcinspect
                    );

                    DB::table('free_stock')->where('id',$cek_stock->id)->update($upstock);
                }

                DB::table('free_stock_in')->where('po_number',$dt->po_number)->where('buyer_item',$dt->buyer_item)->where('manufacturing_size',$dt->manufacturing_size)->where('factory_id',$dt->factory_id)->update(['deleted_at'=>carbon::now()]);    
            }
            $data_response = [
                            'status' => 200,
                            'output' => 'Calculate success . . .'
                          ];
            db::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                            'status' => 422,
                            'output' => 'Calculate fieled . . .'
                          ];
        }
        
        return response()->json(['data'=>$data_response]);
    }
}
