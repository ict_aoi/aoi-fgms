<?php 

 
namespace App\Http\Controllers\Plan;

use App\Http\Controllers\Controller;
use App\Invoice;
use App\POStuffing;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;
use Excel;
use Illuminate\Support\Facades\Cache;
use Rap2hpoutre\FastExcel\FastExcel;

class PlanLoadController extends Controller
{

	


	public function drafPlan(){
		
		return view('planload/draf_plan');
	}

	public function getDataDrafPlan(Request $request){
		$radio = $request->radio;
        ini_set('memory_limit', '2048M');
		
		
		$dates = explode('-', preg_replace('/\s+/', '',$request->date));
       	$from = date_format(date_create($dates[0]),'Y-m-d');
       	$to = date_format(date_create($dates[1]),'Y-m-d');

		
			$data = DB::connection('replikasi_fgms')->table('v_ns_draf_planload');
		if ($radio=='podd') {
			$data = $data->whereBetween('podd_check',[$from,$to]);
		}else if($radio=='lc'){
			$data = $data->whereBetween('kst_lcdate',[$from,$to]);
		}else if($radio=='psdd'){
            $data = $data->whereBetween('po_stat_date_adidas',[$from,$to]);
        }
 
        
        if($request->ajax()){
            Cache::forget('v_ns_draf_planload');
            $data = $data->orderby('id','asc');
            Cache::put('v_ns_draf_planload', $data->get(), 5);

            // Cache::put('v_ns_draf_planload', $data, 5);
           
            // $results = DB::table('po_stuffing')->wherenull('deleted_at')->get();

            return DataTables::of($data)
				->editColumn('plan_ref_number',function($data){
					if (isset($data->plan_ref_number)) {
						return $data->plan_ref_number;
					}else{
						return '<span class="badge badge-warning"><b>Not Upload</b></span>';
					}
				})
				->addColumn('remarks',function($data){
                    if (isset($data->id)) {
                        return '<button class="btn btn-primary" data-po="'.$data->po_number.'" data-plan="'.$data->plan_ref_number.'" onclick="remark(this);"><span class="icon-eye2"></span></button>';
                    }else{
                        return '';
                    }
					
				})
				->addColumn('action',function($data){
                    $btn = DB::table('po_stuffing')->where('po_number',$data->po_number)->where('plan_ref_number',$data->plan_ref_number)->whereNull('deleted_at')->first();
                    if (($btn!=null) && ($btn->invoice==null)) {
                       return '<button class="btn btn-danger" data-po="'.$data->po_number.'" data-plan="'.$data->plan_ref_number.'" onclick="deleteDraf(this);"><span class="icon-cross"></span></button>';
                    }else if (($btn!=null) && ($btn->invoice!=null)) {
                       return $data->invoice;
                    }else{
                        return '';
                    }
					
				})
                ->setRowAttr([
                    'style' => function($data)
                    {
                        if ($data->is_cancel_order==false) {
                            if ((!empty($data->po_check)) && (empty($data->invoice)) ) {
                                return  'background-color: #33d6ff;';
                            }else if ((!empty($data->po_check)) && (!empty($data->invoice))) {
                                return  'background-color: #00ff00;';
                            }else if ( (empty($data->po_check)) && (empty($data->invoice))) {
                                return  'background-color: #ffffff;';
                            }
                        }else{
                             return  'background-color: #ff4000;';
                        }
                        
                        
                    },
                ])
				->rawColumns(['plan_ref_number','remarks','action'])
				->make(true);
        }else{
            $data = [];
            return DataTables::of($data)->make(true);
        }
		
	}

    public function ajaxFilBrand(Request $request){
        $dates = explode('-', preg_replace('/\s+/', '',$request->date));
        $from = date_format(date_create($dates[0]),'Y-m-d');
        $to = date_format(date_create($dates[1]),'Y-m-d');
        $radio = $request->radio;
        // $data = Cache::get('v_ns_draf_planload');
        $data = Cache::get('v_ns_draf_planload');
        if($data == null){
            $data = DB::connection('replikasi_fgms')->table('v_ns_draf_planload')->select('trademark', 'document_type');
            if ($radio=='podd') {
                $data = $data->whereBetween('podd_check',[$from,$to]);
            }else if($radio=='lc'){
                $data = $data->whereBetween('kst_lcdate',[$from,$to]);
            }else if($radio=='psdd'){
                $data = $data->whereBetween('po_stat_date_adidas',[$from,$to]);
            }
    
            $data = $data->get();
            Cache::put('v_ns_draf_planload', $data, 5);

        }
        

        return response()->json(['brand'=>collect($data)->unique('trademark')],200);
 
        
    }

    public function ajaxDocType(Request $request){
        $dates = explode('-', preg_replace('/\s+/', '',$request->date));
        $from = date_format(date_create($dates[0]),'Y-m-d');
        $to = date_format(date_create($dates[1]),'Y-m-d');
        $radio = $request->radio;
        $brand = $request->brand;
        $data = Cache::get('v_ns_draf_planload');
        if($data == null){
            $data = DB::connection('replikasi_fgms')->table('v_ns_draf_planload')->select('trademark','document_type');
            if ($radio=='podd') {
                $data = $data->whereBetween('podd_check',[$from,$to]);
            }else if($radio=='lc'){
                $data = $data->whereBetween('kst_lcdate',[$from,$to]);
            }else if($radio=='psdd'){
                $data = $data->whereBetween('po_stat_date_adidas',[$from,$to]);
            }
            Cache::put('v_ns_draf_planload', $data->get(), 5);

            $data = $data->where('trademark',$brand)->get();
        }else{
            Cache::put('v_ns_draf_planload', $data, 5);

            $data = collect($data)->where('trademark',$brand)->all();
        }

        return response()->json(['doctype'=>collect($data)->unique('document_type')],200);
 
        
    }

    // public function templateUpload(){
    //     $name = "template_plan_load";
    //     return Excel::create($name,function($excel){
    //         $excel->sheet('active',function($sheet){
    //             $sheet->setCellValue('A1','po_number');
    //             $sheet->setCellValue('B1','plan_ref_number');
    //             $sheet->setCellValue('C1','shipmode');
    //             $sheet->setCellValue('D1','fwd');
    //             $sheet->setCellValue('E1','mdd');
    //             $sheet->setCellValue('F1','subsidary');
    //             $sheet->setCellValue('G1','whs_code');
    //             $sheet->setCellValue('H1','priority_code');
    //             $sheet->setCellValue('I1','psfd');
    //             $sheet->setCellValue('J1','remark_order');
    //             $sheet->setCellValue('K1','remark_exim');
    //             $sheet->setCellValue('L1','sports_cat');
    //             $sheet->setCellValue('M1','product_type');
    //             $sheet->setCellValue('N1','age_group');
    //             $sheet->setCellValue('O1','gender');
    //             $sheet->setCellValue('P1','material');

    //             $sheet->setWidth(array(
    //                 'A'=>15,
    //                 'B'=>15,
    //                 'C'=>15,
    //                 'D'=>15,
    //                 'E'=>15,
    //                 'F'=>15,
    //                 'G'=>15,
    //                 'H'=>15,
    //                 'I'=>15,
    //                 'J'=>15,
    //                 'K'=>15,
    //                 'L'=>15,
    //                 'M'=>15,
    //                 'N'=>15,
    //                 'O'=>15,
    //                 'P'=>15
    //             ));

    //             $sheet->cell('E1', function($cell){
    //                 $cell->setBackground('#00ffff');
    //                 $cell->setFontColor('#000000');
    //             });
    //             $sheet->cell('I1', function($cell){
    //                 $cell->setBackground('#00ffff');
    //                 $cell->setFontColor('#000000');
    //             });
    //         });
    //         $excel->setActiveSheetIndex(0);
    //     })->export('xlsx');
    // }

    public function templateUpload(){
        try {
          $path = storage_path('template/template_plan_load.xlsx');
          return response()->download($path);
        } catch (Exception $e) {
          $e->getMessage();
        }
    }

	public function exportDraf(Request $request){
		$date = $request->date;
		$radio = $request->radio;
		$radchoose="";

		$current_date = date_format(Carbon::now(),'Y-m-d');

		$dates = explode('-', preg_replace('/\s+/', '',$request->date));
       	$from = date_format(date_create($dates[0]),'Y-m-d');
       	$to = date_format(date_create($dates[1]),'Y-m-d');

		
		$data = DB::connection('replikasi_fgms')->table('v_ns_draf_planload');
        
		if ($radio=='podd') {
			$data = $data->whereBetween('podd_check',[$from,$to]);
			$radchoose = "podd_check";
		}else if($radio=='lc'){
			$data = $data->whereBetween('kst_lcdate',[$from,$to]);
			$radchoose = "lc_date";
		}else if($radio=='psdd'){
            $data = $data->whereBetween('po_stat_date_adidas',[$from,$to]);
            $radchoose = "stastical_date";
        }

        if (!empty($request->brand)) {
           $data = $data->where('trademark',$request->brand);
       }

       if (!empty($request->doctype)) {
           $data = $data->where('document_type',$request->doctype);
       }

	

		$data = $data->orderby('podd_check','asc')->orderby('plan_ref_number','desc')->get();

        $filename = 'draf_planload_'.$radchoose.'_'.$from."_".$to.'_download_date_'.$current_date;

        return Excel::create($filename,function($excel) use ($data){
            $excel->sheet('active',function($sheet) use ($data){

                $sheet->setCellValue('A1','#');
                $sheet->setCellValue('B1','LC Date');
                $sheet->setCellValue('C1','Order Type');
                $sheet->setCellValue('D1','Plan Stuffing');
                $sheet->setCellValue('E1','Job');
                $sheet->setCellValue('F1','Doc. Type');
                $sheet->setCellValue('G1','Brand');
                $sheet->setCellValue('H1','PO Number');
                $sheet->setCellValue('I1','Plan Ref. Number');
                $sheet->setCellValue('J1','PO Right');
                $sheet->setCellValue('K1','Invoice');
                $sheet->setCellValue('L1','SO');
                $sheet->setCellValue('M1','Art. No.');
                $sheet->setCellValue('N1','Season');
                $sheet->setCellValue('O1','Style');
                $sheet->setCellValue('P1','New Qty');
                $sheet->setCellValue('Q1','Shipmode');
                $sheet->setCellValue('R1','FWD');
                $sheet->setCellValue('S1','MDD');
                $sheet->setCellValue('T1','Subsidary');
                $sheet->setCellValue('U1','WHS Code');
                $sheet->setCellValue('V1','Priority Code');
                $sheet->setCellValue('W1','Crd');
                $sheet->setCellValue('X1','PSDD');
                $sheet->setCellValue('Y1','PODD');
                $sheet->setCellValue('Z1','PSFD');
                $sheet->setCellValue('AA1','Cust. Order No.');
                $sheet->setCellValue('AB1','Cust. No.');
                $sheet->setCellValue('AC1','Country');
                $sheet->setCellValue('AD1','Sewing Place');
                $sheet->setCellValue('AE1','Stuffing Place');
                $sheet->setCellValue('AF1','Qty Pack. List');
                $sheet->setCellValue('AG1','CTN');
                $sheet->setCellValue('AH1','CBM');
                $sheet->setCellValue('AI1','GW');
                $sheet->setCellValue('AJ1','NW');
                $sheet->setCellValue('AK1','Factory');
                $sheet->setCellValue('AL1','Remark Order');
                $sheet->setCellValue('AM1','Remark Exim');
                $sheet->setCellValue('AN1','Old PO');
                $sheet->setCellValue('AO1','Sports Cat');
                $sheet->setCellValue('AP1','Product Type');
                $sheet->setCellValue('AQ1','Age Group');
                $sheet->setCellValue('AR1','Gender');
                $sheet->setCellValue('AS1','Material');
                $sheet->setCellValue('AT1','Fact. Code');
                

                $i = 1;

                foreach ($data as $dt) {
                    
                    $rw = $i +1;
                    $inv = DB::table('invoice')->select('so')->where('invoice',$dt->invoice)->wherenull('deleted_at')->first();
                    $sheet->setCellValue('A'.$rw,$i);
                    $sheet->setCellValue('B'.$rw,$dt->kst_lcdate);
                    $sheet->setCellValue('C'.$rw,$dt->order_type);
                    $sheet->setCellValue('D'.$rw,$dt->plan_stuffing);
                    $sheet->setCellValue('E'.$rw,$dt->job);
                    $sheet->setCellValue('F'.$rw,$dt->document_type);
                    $sheet->setCellValue('G'.$rw,$dt->trademark);
                    $sheet->setCellValue('H'.$rw,$dt->po_number);
                    $sheet->setCellValue('I'.$rw,!empty($dt->plan_ref_number) ? '="'.$dt->plan_ref_number.'"' : 'Not Upload');
                    $sheet->setCellValue('J'.$rw,substr($dt->po_number, -9));
                    $sheet->setCellValue('K'.$rw,$dt->invoice);
                    $sheet->setCellValue('L'.$rw,!empty($inv->so) ? '="'.$inv->so.'"' : '');
                    $sheet->setCellValue('M'.$rw,$dt->buyer_item);
                    $sheet->setCellValue('N'.$rw,$dt->season);
                    $sheet->setCellValue('O'.$rw,$dt->style);
                    $sheet->setCellValue('P'.$rw,$dt->new_qty);
                    $sheet->setCellValue('Q'.$rw,$dt->shipmode);
                    $sheet->setCellValue('R'.$rw,$dt->fwd);
                    $sheet->setCellValue('S'.$rw,$dt->mdd);
                    $sheet->setCellValue('T'.$rw,$dt->subsidary);
                    $sheet->setCellValue('U'.$rw,$dt->whs_code);
                    $sheet->setCellValue('V'.$rw,$dt->priority_code);
                    $sheet->setCellValue('W'.$rw,$dt->crd);
                    $sheet->setCellValue('X'.$rw,$dt->po_stat_date_adidas);
                    $sheet->setCellValue('Y'.$rw,$dt->podd_check);
                    $sheet->setCellValue('Z'.$rw,$dt->psfd);
                    $sheet->setCellValue('AA'.$rw,$dt->customer_order_number);
                    $sheet->setCellValue('AB'.$rw,$dt->customer_number);
                    $sheet->setCellValue('AC'.$rw,$dt->country_name);
                    $sheet->setCellValue('AD'.$rw,$dt->stuffing_place_update);
                    $sheet->setCellValue('AE'.$rw,$dt->stuffing_place_update);
                    $sheet->setCellValue('AF'.$rw,$dt->item_qty);
                    $sheet->setCellValue('AG'.$rw,$dt->ctn);
                    $sheet->setCellValue('AH'.$rw,round($dt->cbm,3));
                    $sheet->setCellValue('AI'.$rw,round($dt->gross,3));
                    $sheet->setCellValue('AJ'.$rw,round($dt->net,3));
                    $sheet->setCellValue('AK'.$rw,$dt->factory_id);
                    $sheet->setCellValue('AL'.$rw,$dt->remark_order);
                    $sheet->setCellValue('AM'.$rw,$dt->remark_exim);
                    $sheet->setCellValue('AN'.$rw,$dt->old_po);
                    $sheet->setCellValue('AO'.$rw,$dt->sports_ctg);
                    $sheet->setCellValue('AP'.$rw,$dt->product_type);
                    $sheet->setCellValue('AQ'.$rw,$dt->age_group);
                    $sheet->setCellValue('AR'.$rw,$dt->gender);
                    $sheet->setCellValue('AS'.$rw,$dt->material);
                    $sheet->setCellValue('AT'.$rw, isset($dt->factory_code) ? $dt->factory_code : "-");

                    // if ($dt->po_check!=null && $dt->is_cancel_order==false) {
                    //     $sheet->cell('A'.$rw.':AR'.$rw, function($cell) {
                    //             $cell->setBackground('#00ffff');
                    //             $cell->setFontColor('#000000');
                    //     });
                    // }else if( ($dt->po_check!=null OR $dt->po_check==null ) && $dt->is_cancel_order==true){
                    //     $sheet->cell('A'.$rw.':AR'.$rw, function($cell) {
                    //             $cell->setBackground('#ff4000');
                    //             $cell->setFontColor('#000000');
                    //     });
                    // }

                    if ($dt->is_cancel_order==true) {
                        $sheet->cell('A'.$rw.':AT'.$rw, function($cell) {
                                $cell->setBackground('#ff4000');
                                $cell->setFontColor('#000000');
                        });
                    }else{
                        if (!empty($dt->po_check)&& empty($dt->invoice)) {
                            $sheet->cell('A'.$rw.':AT'.$rw, function($cell) {
                                    $cell->setBackground('#33d6ff');
                                    $cell->setFontColor('#000000');
                            });
                        }else if (!empty($dt->po_check)&& !empty($dt->invoice)) {
                            $sheet->cell('A'.$rw.':AT'.$rw, function($cell) {
                                    $cell->setBackground('#00ff00');
                                    $cell->setFontColor('#000000');
                            });
                        }else if (empty($dt->po_check)&& empty($dt->invoice)) {
                            $sheet->cell('A'.$rw.':AT'.$rw, function($cell) {
                                    $cell->setBackground('#ffffff');
                                    $cell->setFontColor('#000000');
                            });
                        }
                    }

                    $i++;
                }
            });
            $excel->setActiveSheetIndex(0);

        })->export('xlsx');

        // $i=1;

        // $export = \Excel::create($filename,function($excel) use ($data,$i){
        // 		$excel->sheet('report',function($sheet) use ($data,$i){
        // 			$sheet->appendRow(array(
        // 				'#',
        // 				'PO Number',
        // 				'Plan Ref. Number',
        // 				'Invoice',
        //                 'SO',
        // 				'Job',
        // 				'Art. No.',
        // 				'Season',
        // 				'Style',
        // 				'Brand',
        // 				'New Qty',
        // 				'Shipmode',
        // 				'FWD',
        // 				'Subsidary',
        // 				'Priority Code',
        // 				'LC Date',
        // 				'Order Type',
        // 				'Doc. Type',
        // 				'Crd',
        // 				'PSDD',
        // 				'PODD',
        // 				'PSFD',
        // 				'Cust. Order No.',
        // 				'Cust. No.',
        // 				'Country',
        // 				'Sewing Place',
        // 				'Stuffing Place',
        // 				'Qty Pack. List',
        // 				'CTN',
        // 				'CBM',
        // 				'GW',
        // 				'NW',
        // 				'Factory',
        // 				'Remark Order',
        // 				'Remark Exim',
        //                 'Remark Draf'
        // 			));
        // 			$data->chunk(500,function($row) use ($sheet,$i){
        // 				foreach ($row as $rw) {
        //                     $so = DB::table('invoice')->select('so')->where('invoice',$rw->invoice)->whereNull('deleted_at')->first();
                          
        // 					$sheet->appendRow(array(
        // 						$i++,
        // 						'="'.$rw->po_number.'"',
		      //   				!empty($rw->plan_ref_number) ? '="'.$rw->plan_ref_number.'"' : 'Not Upload',
		      //   				$rw->invoice,
        //                         !empty($so->so) ? '="'.$so->so.'"' : '-',
		      //   				$rw->job,
		      //   				$rw->buyer_item,
		      //   				$rw->season,
		      //   				$rw->style,
		      //   				$rw->trademark,
		      //   				$rw->new_qty,
		      //   				$rw->shipmode,
		      //   				$rw->fwd,
		      //   				$rw->subsidary,
		      //   				$rw->priority_code,
		      //   				$rw->kst_lcdate,
		      //   				$rw->order_type,
		      //   				$rw->document_type,
		      //   				$rw->crd,
		      //   				$rw->po_stat_date_adidas,
		      //   				$rw->podd_check,
		      //   				$rw->psfd,
		      //   				$rw->customer_order_number,
		      //   				$rw->customer_number,
		      //   				$rw->country_name,
		      //   				$rw->sewing_place_update,
		      //   				$rw->stuffing_place_update,
		      //   				$rw->item_qty,
		      //   				$rw->ctn,
		      //   				round($rw->cbm,3),
		      //   				round($rw->gross,3),
		      //   				round($rw->net,3),
		      //   				$rw->factory_check,
		      //   				$rw->remark_order,
		      //   				$rw->remark_exim,
        //                         !empty($rw->po_check) ? '="Draf was uploaded"' : 'Draf not upload'
        // 					));
        // 				}
        // 			});
        // 		});
        // })->download('xlsx');

        // return response()->json('Success exporting',200);

    

	}

	public function uploadDraf(Request $request){

        $pofield = [];
		$results = array();
        if($request->hasFile('file_upload')){
            $extension = \File::extension($request->file_upload->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls") {
                $path = $request->file_upload->getRealPath();
                $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
            }
        }

        

       

        try {
            db::beginTransaction();

                foreach ($datax as $key => $value) {
                    if (
                        trim($value->po_number)!=null && 
                        trim($value->plan_ref_number)!=null && 
                        trim($value->subsidary)!=null && 
                        trim($value->fwd)!=null && 
                        trim($value->psfd)!=null && 
                        trim($value->whs_code)!=null && 
                        trim($value->priority_code)!=null && 
                        (trim($value->ship_by!=null) ||  trim($value->shipmode)!="#N/A" ||  trim($value->shipmode)!="#REF!") && 
                        trim($value->sports_cat)!=null && 
                        trim($value->product_type)!=null && 
                        trim($value->age_group)!=null && 
                        trim($value->gender)!=null && 
                        trim($value->material)!=null
                        ) {

                     //    dd(date_format($value->mdd,"Y-m-d"),date_format($value->psfd,"Y-m-d"));
                        $cekPL = DB::table('po_summary')->where('po_number',trim($value->po_number))->where('plan_ref_number',trim($value->plan_ref_number))->wherenull('deleted_at')->first();

                        if ($this->_cekUpload(trim($value->po_number),trim($value->plan_ref_number),trim($value->shipmode),trim($value->fwd))==true && $cekPL!=null) {
                                // $cekmdd = $value->mdd ? date_format($value->mdd,"Y-m-d H:i:s") : null;
                             
                                // $dt['po_number']=trim($value->po_number);
                                // $dt['plan_ref_number']=trim($value->plan_ref_number);
                                // $dt['mdd']= $value->mdd ? date_format(date_create($value->mdd),"Y-m-d") : null;
                                // $dt['subsidary']=trim($value->subsidary);
                                // $dt['priority_code']=trim($value->priority_code);
                                // $dt['shipmode']=trim($value->shipmode);
                                // $dt['fwd']=trim($value->fwd);
                                // $dt['whs_code']=trim($value->whs_code);
                                // $dt['psfd']=$value->psfd ? date_format(date_create($value->psfd),"Y-m-d") : null;
                                // $dt['remark_order']=$value->remark_order;
                                // $dt['remark_exim']=$value->remark_exim;
                                // $dt['created_at']=Carbon::now();
                                // $dt['sports_ctg']=trim($value->sports_cat);
                                // $dt['product_type']=trim($value->product_type);
                                // $dt['age_group']=trim($value->age_group);
                                // $dt['gender']=trim($value->gender);
                                // $dt['material']=trim($value->material);
                                // // $dt['factory_id']=$cekPL->factory_id;
                                // $dt['trademark']=$cekPL->trademark;
                                // $dt['user_id']=Auth::user()->id;
                                // DB::table('po_stuffing')->insert($dt);
                                $psfd = str_replace('/', '-', $value->psfd);
                                $dt = [
                                    'po_number' => trim($value->po_number),
                                    'plan_ref_number' => trim($value->plan_ref_number),
                                    'mdd' => $value->mdd ? date_format(date_create($value->mdd), "Y-m-d") : null,
                                    'subsidary' => trim($value->subsidary),
                                    'priority_code' => trim($value->priority_code),
                                    'shipmode' => trim($value->shipmode),
                                    'fwd' => trim($value->fwd),
                                    'whs_code' => trim($value->whs_code),
                                    // 'psfd' => $value->psfd ? date_format(date_create($value->psfd), "Y-m-d") : null,
                                    'psfd' => $value->psfd ? date('Y-m-d', strtotime($psfd)) : null,
                                    'remark_order' => $value->remark_order,
                                    'remark_exim' => $value->remark_exim,
                                    'created_at' => Carbon::now(),
                                    'sports_ctg' => trim($value->sports_cat),
                                    'product_type' => trim($value->product_type),
                                    'age_group' => trim($value->age_group),
                                    'gender' => trim($value->gender),
                                    'material' => trim($value->material),
                                    'trademark' => $cekPL->trademark,
                                    'user_id' => Auth::user()->id,
                                ];
                                
                                POStuffing::updateOrCreate(
                                        ['po_number' => $dt['po_number'], 'plan_ref_number' => $dt['plan_ref_number'], 'deleted_at' => null],
                                        $dt
                                    );

                                $getid = DB::table('po_stuffing')->select('id')->where('po_number',trim($value->po_number))->where('plan_ref_number',trim($value->plan_ref_number))->whereNull('deleted_at')->first();

                                $this->_query_planmovement($getid->id,Auth::user()->id,"upload draf planload");
                                

                        }else{
                            $pofield[]=$value->po_number;
                        }
                    }else{
                        $pofield[]=$value->po_number;
                    }
                    
                }
                
                $data_response = [
                            'status' => 200,
                            'output' => $pofield
                          ];
            DB::commit();
   
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'upload filed'
                          ];
        }

        return response()->json(['data'=>$data_response]);
		
	}

    private function _cekUpload($po_number,$planref,$shipmode,$fwd){
       
        $cekdt = DB::table('po_stuffing')->where('po_number',$po_number)->where('plan_ref_number',$planref)->wherenull('deleted_at')->exists();
        $ceksync = DB::table('po_master_sync')->where('po_number',$po_number)->wherenull('deleted_at')->exists();
        $cekfwd = DB::table('master_forwarder')->where('forwarder',$fwd)->wherenull('deleted_at')->exists();
        $cekshipmode = DB::table('master_shipmode')->where('shipmode',$shipmode)->wherenull('deleted_at')->exists();

        if ($cekdt==false && $ceksync==true && $cekfwd==true && $cekshipmode==true) {
            $permission = true;
        }else{
            $permission = false;
        }

        return $permission;
    }

    public function getRemark(Request $request){
        $po_number = $request->po;
        $plan_ref_number = $request->plan;
        
        $data = DB::table('po_stuffing')->where('po_number',$po_number)->where('plan_ref_number',$plan_ref_number)->whereNull('deleted_at')->first();

        
        return response()->json(['exim'=>$data->remark_exim,'order'=>$data->remark_order],200);
    }

    public function setRemark(Request $request){
        $po_number = $request->po_number;
        $plan_ref_number = $request->plan_ref;
        $remark_order = $request->remark_order;
        $remark_exim = $request->remark_exim;
        
        $cek = DB::table('po_stuffing')->where('po_number',$po_number)->where('plan_ref_number',$plan_ref_number)->wherenull('deleted_at')->first();
        try {
            DB::begintransaction();
                DB::table('po_stuffing')->where('id',$cek->id)->update(['remark_exim'=>$remark_exim,'remark_order'=>$remark_order,'updated_at'=>Carbon::now()]); 
                
            DB::commit();
            return response()->json('Update Success . . .', 200);
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            return response()->json('Update Field . . . '.$message, 422);
            
        }
    }

    public function deletePoDraf(Request $request){
        $po_number = $request->po;
        $plan_ref = $request->plan;
            $getid = DB::table('po_stuffing')->where('po_number',$po_number)->where('plan_ref_number',$plan_ref)->wherenull('deleted_at')->first();
        try {
            DB::begintransaction();

                DB::table('po_stuffing')->where('id',$getid->id)->update(['deleted_at'=>Carbon::now()]);
                $this->_query_planmovement($getid->id,Auth::user()->id,"deleted po from draf");

            DB::commit();
            return response()->json('Delete Success . . .', 200);
        } catch (Exception $ex) {
             DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            return response()->json('Delete Field . . . '.$message, 422);
        }
    }

    public function editPoDraf(){
        return view('planload/edit_draf');
    }

    public function getDataPoDraf(Request $request){
        $po_number = trim($request->po_number);

        $data = DB::table('po_stuffing')
                        ->whereNull('invoice')
                        ->whereNull('deleted_at');
        if ($po_number==null) {
            $data = $data;
        }else{
            $data = $data->where('po_number','LIKE','%'.$po_number.'%');
        }

        $data = $data->orderby('po_number','asc')
                        ->orderby('plan_ref_number','asc');

        return DataTables::of($data)
                            ->editColumn('plan_ref_number',function($data){
                                return '<input type="text" id="planref_'.$data->po_number.'" data-id="'.$data->id.'" data-po="'.$data->po_number.'" data-plan="'.$data->plan_ref_number.'" class="form-control planref_unfilled" value="'.$data->plan_ref_number.'">';
                            })
                            ->editColumn('mdd',function($data){
                                
                                return '<input type="date" id="mdd_'.$data->po_number.'" data-id="'.$data->id.'" data-po="'.$data->po_number.'" data-plan="'.$data->plan_ref_number.'" class="form-control mdd_unfilled" value="'.$data->mdd.'">';
                            })
                            ->editColumn('subsidary',function($data){
                                return '<input type="text" id="subsidary_'.$data->po_number.'" data-id="'.$data->id.'" data-po="'.$data->po_number.'" data-plan="'.$data->plan_ref_number.'" class="form-control subsidary_unfilled" value="'.$data->subsidary.'">';
                            })
                            ->editColumn('shipmode',function($data){
                                $shipmode = DB::table('master_shipmode')->where('brand',$data->trademark)->whereNull('deleted_at')->get();

                                $op = '';
                                $sel = "";
                                    foreach ($shipmode as $sm) {
                                        if ($sm->shipmode==$data->shipmode) {
                                            $sel = "selected";
                                        }else{
                                            $sel = "";
                                        }
                                       $op = $op.' <option value="'.$sm->shipmode.'" '.$sel.'>'.$sm->shipmode.'</option>';
                                    }
                                    
                                return '<select class="form-control sm_unfilled" id="sm_'.$data->po_number.'" data-id="'.$data->id.'" data-plan="'.$data->plan_ref_number.'">
                                            <option value="NULL" >--Choose Shipmode--</option>
                                            '.$op.'
                                        </select>';
                            })
                            ->editColumn('fwd',function($data){
                                $fwd = DB::table('master_forwarder')->where('brand',$data->trademark)->whereNull('deleted_at')->get();
                                $op = '';
                                $sel = "";
                                    foreach ($fwd as $fw) {
                                        if ($fw->forwarder==$data->fwd) {
                                            $sel = "selected";
                                        }else{
                                            $sel = "";
                                        }
                                       $op = $op.' <option value="'.$fw->forwarder.'" '.$sel.'>'.$fw->forwarder.'</option>';
                                    }
                                    
                              return '<select class="form-control fwd_unfilled" id="fwd_'.$data->po_number.'" data-id="'.$data->id.'" data-plan="'.$data->plan_ref_number.'">
                                            <option value="NULL" >--Choose Forwarder--</option>
                                            '.$op.'
                                        </select>';
                            })
                            ->editColumn('psfd',function($data){
                                
                                return '<input type="date" id="psfd_'.$data->po_number.'" data-id="'.$data->id.'" data-po="'.$data->po_number.'" data-plan="'.$data->plan_ref_number.'" class="form-control psfd_unfilled" value="'.$data->psfd.'">';
                            })
                            ->editColumn('whs_code',function($data){
                                return '<input type="text" id="whs_'.$data->po_number.'" data-id="'.$data->id.'" data-po="'.$data->po_number.'" data-plan="'.$data->plan_ref_number.'" class="form-control whs_unfilled" value="'.$data->whs_code.'">';
                            })
                            ->editColumn('priority_code',function($data){
                                return '<input type="text" id="prio_'.$data->po_number.'" data-id="'.$data->id.'" data-po="'.$data->po_number.'" data-plan="'.$data->plan_ref_number.'" class="form-control prio_unfilled" value="'.$data->priority_code.'">';
                            })
                            ->addColumn('remark',function($data){
                                return '<button class="btn btn-primary" data-po="'.$data->po_number.'" data-plan="'.$data->plan_ref_number.'" onclick="remark(this);" ><span class="icon-eye2"></span></button>';
                            })
                            ->addColumn('action',function($data){
                                return '<button class="btn btn-danger" data-po="'.$data->po_number.'" data-plan="'.$data->plan_ref_number.'" onclick="deleteDraf(this);"><span class="icon-cross"></span></button>';
                            })
                            ->rawColumns(['plan_ref_number','mdd','subsidary','shipmode','fwd','psfd','whs_code','priority_code','remark','action'])
                            ->make(true);
    }

    public function updatePlanRef(Request $request){
        $id = $request->id;
        $po_number = $request->po_number;
        $plan_ref_number = $request->plan_ref_number;
        $newplanref = trim($request->newplan);

        try {
            DB::beginTransaction();
                DB::table('po_stuffing')
                    ->where('id', $id)
                    ->whereNull('deleted_at')
                    ->update([
                        'plan_ref_number' => $newplanref
                    ]);
            DB::commit();
        } catch (Exception $ex) {
             $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }

        $html = '<input type="date" id="planref_'.$request->po_number.'" data-id="'.$request->id.'" data-po="'.$request->po_number.'" data-plan="'.$request->plan_ref_number.'" class="form-control planref_unfilled" value="'.$request->plan_ref_number.'">';

        return response()->json($html, 200);

    }

    public function updateSubsidary(Request $request){
        $id = $request->id;
        $po_number = $request->po_number;
        $plan_ref_number = $request->plan_ref_number;
        $subsidary = trim($request->subsidary);

        try {
            DB::beginTransaction();
                DB::table('po_stuffing')
                    ->where('id', $id)
                    ->whereNull('deleted_at')
                    ->update([
                        'subsidary' => $subsidary
                    ]);
            DB::commit();
        } catch (Exception $ex) {
             $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }

        $html = '<input type="date" id="subsidary_'.$request->po_number.'" data-id="'.$request->id.'" data-po="'.$request->po_number.'" data-plan="'.$request->plan_ref_number.'" class="form-control subsidary_unfilled" value="'.$request->subsidary.'">';

        return response()->json($html, 200);

    }

    public function updatePsfd(Request $request){
        $id = $request->id;
        $po_number = $request->po_number;
        $plan_ref_number = $request->plan_ref_number;
        $psfd = $request->psfd;

        try {
            DB::beginTransaction();
                DB::table('po_stuffing')
                    ->where('id', $id)
                    ->whereNull('deleted_at')
                    ->update([
                        'psfd' => $psfd
                    ]);
            DB::commit();
        } catch (Exception $ex) {
             $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }

        $html = '<input type="date" id="psfd_'.$request->po_number.'" data-id="'.$request->id.'" data-po="'.$request->po_number.'" data-plan="'.$request->plan_ref_number.'" class="form-control psfd_unfilled" value="'.$request->subsidary.'">';

        return response()->json($html, 200);

    }

    public function updateWhs(Request $request){
        $id = $request->id;
        $po_number = $request->po_number;
        $plan_ref_number = $request->plan_ref_number;
        $whs = trim($request->whs);

        try {
            DB::beginTransaction();
                DB::table('po_stuffing')
                    ->where('id', $id)
                    ->whereNull('deleted_at')
                    ->update([
                        'whs_code' => $whs
                    ]);
            DB::commit();
        } catch (Exception $ex) {
             $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }

        $html = '<input type="date" id="whs_'.$request->po_number.'" data-id="'.$request->id.'" data-po="'.$request->po_number.'" data-plan="'.$request->plan_ref_number.'" class="form-control whs_unfilled" value="'.$request->subsidary.'">';

        return response()->json($html, 200);

    }













    // invoice storange

    public function invoiceStorage(){

        $mxinv = DB::table('invoice')->where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->max('invoice');

        $fcidt = substr($mxinv, 0,1);
        $num =(int)substr($mxinv, -6)+1;
        $invnext = $fcidt.$num;            
       
        return view('planload/invoice_storage')->with('maxinv',$invnext);
    } 

    public function getDataInvStrg(Request $request){
      
        $dates = explode('-', preg_replace('/\s+/', '',$request->date));
        $from = date_format(date_create($dates[0]),'Y-m-d');
        $to = date_format(date_create($dates[1]),'Y-m-d');

        $data = DB::connection('replikasi_fgms')->table('v_ns_draf_planload')->whereBetween('podd_check',[$from,$to])->where('factory_id',Auth::user()->factory_id)->where('po_check','<>',null)->wherenull('invoice')->orderby('customer_number','asc')->orderby('crd','asc')->orderby('po_stat_date_adidas','asc')->orderby('psfd','asc');

       

        return DataTables::of($data)
                        ->editColumn('po_number',function($data){
                            return '<b>'.$data->po_number.'</b><br> ('.$data->plan_ref_number.')';
                        })
                        ->addColumn('checkbox',function($data){
                            return '<input type="checkbox" class="poplan" name="selector[]" id="Inputselector" value="'.$data->id.'" data-cbm="'.$data->cbm.'" data-po="'.$data->po_number.'" data-planref="'.$data->plan_ref_number.'" data-mdd="'.$data->mdd.'" data-psdd="'.$data->po_stat_date_adidas.'" data-customer="'.$data->customer_number.'" data-country="'.$data->country_name.'" data-fwd="'.$data->fwd.'" data-ship="'.$data->shipmode.'" data-ctn="'.$data->ctn.'" data-qty="'.$data->item_qty.'" data-crd="'.$data->crd.'" data-psfd="'.$data->psfd.'" data-gw="'.$data->gross.'" data-nw="'.$data->net.'">';
                        })
                        ->addColumn('remark',function($data){
                            return '<button class="btn btn-primary" data-po="'.$data->po_number.'" data-plan="'.$data->plan_ref_number.'" data-id="'.$data->id.'" onclick="remarkInv(this);"><span class="icon-eye2"></span></button>';
                        })
                        ->addColumn('action',function($data){
                            return '<button " class="btn btn-danger" data-po="'.$data->po_number.'" data-plan="'.$data->plan_ref_number.'" data-id="'.$data->id.'" onclick="deletePlan(this);"><span class="icon-trash"></span></button>';
                        })
                        ->editColumn('mdd',function($data){
                            return '<input type="date" id="mdd_'.$data->po_number.'" data-id="'.$data->id.'" data-po="'.$data->po_number.'" data-plan="'.$data->plan_ref_number.'" class="form-control mdd_unfilled" value="'.$data->mdd.'">';
                        })
                        ->editColumn('shipmode',function($data){
                            $shipmode = DB::table('master_shipmode')->where('brand',$data->trademark)->whereNull('deleted_at')->get();

                                $op = '';
                                $sel = "";
                                    foreach ($shipmode as $sm) {
                                        if ($sm->shipmode==$data->shipmode) {
                                            $sel = "selected";
                                        }else{
                                            $sel = "";
                                        }
                                       $op = $op.' <option value="'.$sm->shipmode.'" '.$sel.'>'.$sm->shipmode.'</option>';
                                    }
                                    
                                return '<select class="form-control sm_unfilled" id="sm_'.$data->po_number.'" data-id="'.$data->id.'" data-plan="'.$data->plan_ref_number.'">
                                            <option value="NULL" >--Choose Shipmode--</option>
                                            '.$op.'
                                        </select>';
                        })
                        ->editColumn('fwd',function($data){
                          
                            $fwd = DB::table('master_forwarder')->where('brand',$data->trademark)->whereNull('deleted_at')->get();
                            $op = '';
                            $sel = "";
                                foreach ($fwd as $fw) {
                                    if ($fw->forwarder==$data->fwd) {
                                        $sel = "selected";
                                    }else{
                                        $sel = "";
                                    }
                                   $op = $op.' <option value="'.$fw->forwarder.'" '.$sel.'>'.$fw->forwarder.'</option>';
                                }
                                
                          return '<select class="form-control fwd_unfilled" id="fwd_'.$data->po_number.'" data-id="'.$data->id.'" data-plan="'.$data->plan_ref_number.'">
                                        <option value="NULL" >--Choose Shipmode--</option>
                                        '.$op.'
                                    </select>';
                        })
                        ->editColumn('priority_code',function($data){
                            return '<input type="text" id="prio_'.$data->po_number.'" data-id="'.$data->id.'" data-po="'.$data->po_number.'" data-plan="'.$data->plan_ref_number.'" class="form-control prio_unfilled" value="'.$data->priority_code.'">';
                        })
                        ->setRowAttr([
                            'style' => function($data)
                            {
                                if ($data->is_cancel_order==false) {
                                    return  'background-color: #ffffff;'; 
                                }else{
                                    return  'background-color: #ff4000;';
                                }
                                
                                
                            },
                        ])
                        ->rawColumns(['po_number','checkbox','remark','action','mdd','shipmode','fwd','priority_code'])
                        ->make(true);
    }

    public function ajaxGetCustomer(Request $request){
        $dates = explode('-', preg_replace('/\s+/', '',$request->date));
        $from = date_format(date_create($dates[0]),'Y-m-d');
        $to = date_format(date_create($dates[1]),'Y-m-d');
        $data = DB::connection('replikasi_fgms')->table('v_ns_draf_planload')->select('customer_number')->whereBetween('podd_check',[$from,$to])->where('factory_id',Auth::user()->factory_id)->where('po_check','<>',null)->where('customer_number','<>',null)->wherenull('invoice')->groupBy('customer_number')->get();
      
        return response()->json(['cust'=>$data],200);
    }

    public function ajaxGetMdd(Request $request){
        $dates = explode('-', preg_replace('/\s+/', '',$request->date));
        $from = date_format(date_create($dates[0]),'Y-m-d');
        $to = date_format(date_create($dates[1]),'Y-m-d');
        $data = DB::connection('replikasi_fgms')->table('v_ns_draf_planload')->select('mdd')->whereBetween('podd_check',[$from,$to])->where('factory_id',Auth::user()->factory_id)->where('po_check','<>',null)->where('mdd','<>',null)->where('customer_number',$request->custno)->wherenull('invoice')->groupBy('mdd')->get();
        
        return response()->json(['mdd'=>$data],200);
    }

    public function ajaxGetPsfd(Request $request){
        $dates = explode('-', preg_replace('/\s+/', '',$request->date));
        $from = date_format(date_create($dates[0]),'Y-m-d');
        $to = date_format(date_create($dates[1]),'Y-m-d');
        $data = DB::connection('replikasi_fgms')->table('v_ns_draf_planload')->select('psfd')->whereBetween('podd_check',[$from,$to])->where('factory_id',Auth::user()->factory_id)->where('po_check','<>',null)->where('psfd','<>',null)->where('customer_number',$request->custno)->where('mdd',$request->mdd)->wherenull('invoice')->groupBy('psfd')->get();
        
        return response()->json(['psfd'=>$data],200);
    }

    public function ajaxGetPriority(Request $request){
        $dates = explode('-', preg_replace('/\s+/', '',$request->date));
        $from = date_format(date_create($dates[0]),'Y-m-d');
        $to = date_format(date_create($dates[1]),'Y-m-d');
        $data = DB::connection('replikasi_fgms')->table('v_ns_draf_planload')->select('priority_code')->whereBetween('podd_check',[$from,$to])->where('factory_id',Auth::user()->factory_id)->where('po_check','<>',null)->where('priority_code','<>',null)->where('customer_number',$request->custno)->where('mdd',$request->mdd)->where('psfd',$request->psfd)->wherenull('invoice')->groupBy('priority_code')->get();
       
        return response()->json(['priority'=>$data],200);
    }
 
    public function ajaxGetWhs(Request $request){
        $dates = explode('-', preg_replace('/\s+/', '',$request->date));
        $from = date_format(date_create($dates[0]),'Y-m-d');
        $to = date_format(date_create($dates[1]),'Y-m-d');
        $data = DB::connection('replikasi_fgms')->table('v_ns_draf_planload')->select('whs_code')->whereBetween('podd_check',[$from,$to])->where('factory_id',Auth::user()->factory_id)->where('po_check','<>',null)->where('priority_code','<>',null)->where('customer_number',$request->custno)->where('mdd',$request->mdd)->where('psfd',$request->psfd)->where('priority_code',$request->priority)->wherenull('invoice')->groupBy('whs_code')->get();
       
        return response()->json(['whs'=>$data],200);
    }

    public function invGetRemark(Request $request){
        $data = DB::table('po_stuffing')->select('remark_exim','remark_order')->where('id',$request->id)->wherenull('deleted_at')->first();

        return response()->json(['remark'=>$data],200);
    }

    public function invSetRemark(Request $request){
        $id = $request->id;
        $rorder = $request->remark_order;
        $rexim = $request->remark_exim;
        
        try {
            DB::begintransaction();
            
                    
                    $data_up = array(
                        'remark_exim'=>$rorder,
                        'remark_order'=>$rexim,
                        'updated_at'=>Carbon::now()
                    );
                  DB::table('po_stuffing')->where('id',$id)->update(['remark_exim'=>$rorder,'remark_order'=>$rexim,'updated_at'=>Carbon::now()]); 
                
                return response()->json('Update Success . . .', 200);
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            return response()->json('Update Field . . . '.$message, 422);
            
        }
    }


    public function createInvoice(Request $request){
        $data = $request->data;
        $invoice_no = $request->invoice_no;
        $so = $request->so;
        $remark_cont = $request->remark_cont;
        $cbm = $request->cbm;
        $ctn = $request->ctn;
        $qty = $request->qty;
        $customer_number = $request->cust_no;
        $fwd = $request->fwd;
        $destination = $request->country;
        $shipmode = $request->shipmode;
        $mdd = $request->mdd;
        $planstuffing = $request->planstuffing;
        $gw = $request->gw;
        $nw = $request->nw;
        try {
            DB::begintransaction();

                
                $dt_inv = array(
                        'invoice'=>$invoice_no,
                        'plan_stuffing'=>$planstuffing,
                        'so'=>$so,
                        'mdd'=>$mdd,
                        'customer_number'=>$customer_number,
                        'destination'=>$destination,
                        'shipmode'=>$shipmode,
                        'fwd'=>$fwd,
                        'cbm'=>$cbm,
                        'ctn'=>$ctn,
                        'qty'=>$qty,
                        'remark_cont'=>$remark_cont,
                        'created_at'=>Carbon::now(),
                        'gw'=>round($gw,3),
                        'nw'=>round($nw,3),
                        'user_id'=>Auth::user()->id,
                        'factory_id'=>Auth::user()->factory_id
                    );

                $dt_upt= array(
                    'plan_stuffing'=>$planstuffing,
                    'invoice'=>$invoice_no,
                    'updated_at'=>Carbon::now()
                );
                // DB::table('invoice')->insert($dt_inv);
                $cek_invoice = Invoice::where('invoice', $dt_inv['invoice'])
                ->where('plan_stuffing', $dt_inv['plan_stuffing'])
                ->where('so', $dt_inv['so'])
                ->where('factory_id', $dt_inv['factory_id'])
                ->whereNull('deleted_at')
                ->first();
                if(!isset($cek_invoice)){
                    DB::table('invoice')->insert($dt_inv);
                }
                // Invoice::updateOrCreate([
                //     'invoice' => $dt_inv['invoice'],
                //     'plan_stuffing' => $dt_inv['plan_stuffing'],
                //     'so'=>$dt_inv['so'],
                //     'factory_id'=>$dt_inv['factory_id'],
                //     'deleted_at' => null
                // ],
                // $dt_inv);
                foreach ($data as $ky) {
                    DB::table('po_stuffing')->where('id',$ky['id'])->update($dt_upt);
                    $this->_query_planmovement($ky['id'],Auth::user()->id,"create to invoice (".$invoice_no.") ex-fact (".$planstuffing.")");
                }
                
             
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function updateMdd(Request $request){
        try {
            DB::beginTransaction();
                DB::table('po_stuffing')
                    ->where('id', $request->id)
                    ->whereNull('deleted_at')
                    ->update([
                        'mdd' => $request->mdd
                    ]);
            DB::commit();
        } catch (Exception $ex) {
             $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }

        $html = '<input type="date" id="mdd_'.$request->po_number.'" data-id="'.$request->id.'" data-po="'.$request->po_number.'" data-plan="'.$request->plan_ref_number.'" class="form-control mdd_unfilled" value="'.$request->mdd.'">';

        return response()->json($html, 200);
    }

    public function updateFwd(Request $request){
        
        try {
            DB::beginTransaction();
                DB::table('po_stuffing')
                    ->where('id', $request->id)
                    ->whereNull('deleted_at')
                    ->update([
                        'fwd' => trim($request->fwd)
                    ]);
            DB::commit();
        } catch (Exception $ex) {
             $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }

        $html = '<input type="text" id="fwd_'.$request->po_number.'" data-id="'.$request->id.'" data-po="'.$request->po_number.'" data-plan="'.$request->plan_ref_number.'" class="form-control fwd_unfilled" value="'.$request->fwd.'">';

        return response()->json($html, 200);
    }

    public function updatePriority(Request $request){
        
        try {
            DB::beginTransaction();
                DB::table('po_stuffing')
                    ->where('id', $request->id)
                    ->whereNull('deleted_at')
                    ->update([
                        'priority_code' => trim($request->priority_code)
                    ]);
            DB::commit();
        } catch (Exception $ex) {
             $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }

        $html = '<input type="text" id="prio_'.$request->po_number.'" data-id="'.$request->id.'" data-po="'.$request->po_number.'" data-plan="'.$request->plan_ref_number.'" class="form-control prio_unfilled" value="'.$request->priority_code.'">';

        return response()->json($html, 200);
    }

    public function updateShipmode(Request $request){
        
        try {
            DB::beginTransaction();
                DB::table('po_stuffing')
                    ->where('id', $request->id)
                    ->whereNull('deleted_at')
                    ->update([
                        'shipmode' => $request->shipmode
                    ]);
            DB::commit();
        } catch (Exception $ex) {
             $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }

        $html = '<select class="form-control sm_unfilled" id="sm_'.$request->po_number.'" data-id="'.$request->id.'" data-plan="'.$request->plan_ref_number.'"></select>';

        return response()->json($html, 200);
    }

    // invoice detail 
    public function invoiceDetail(){
        $factory = DB::table('factory')
                        ->whereNull('deleted_at')
                        ->get();

        return view('planload/invoice_detail')->with('factory',$factory);
    }

    public function getDetailInvoice(Request $request){
        $radio = $request->radio;
        $po_number = $request->po_number;
        $invoice = explode(";", trim($request->invoice));
        // dd($invoice);
        $exfact = $request->exfact;
        $factory = $request->factory_id;

        $date_exfact = explode('-', preg_replace('/\s+/', '', $request->exfact));
        $range_exfact = array(
            'from' => date_format(date_create($date_exfact[0]), 'Y-m-d'),
            'to' => date_format(date_create($date_exfact[1]), 'Y-m-d')
        );

        

        $data = DB::table('v_ns_detail_invoice')
                ->whereRaw("factory_id::integer = $factory")
                ->where('invoice','<>',null);
        switch ($radio) {
            case 'invoice':
                    
                    $data = $data->whereIn('invoice',$invoice);
                break;
            case 'po':
                    $data = $data->where('po_number','LIKE','%'.$po_number.'%');
                break;
            case 'plan':
                    $data = $data->whereBetween('plan_stuffing',[$range_exfact['from'],$range_exfact['to']]);
                break;
            
        }

        $data = $data->orderby('plan_stuffing','asc')
                        ->orderby('invoice','asc')
                        ->orderby('po_number','asc');

        return DataTables::of($data)
                            ->editColumn('plan_stuffing',function($data){
                                return '<input type="date" id="exfact_'.$data->invoice.'" data-invoice="'.$data->invoice.'" class="form-control exfact_unfilled" value="'.$data->plan_stuffing.'">';
                            })
                            ->addColumn('so',function($data){
                                $so = DB::table('invoice')
                                            ->where('invoice',$data->invoice)
                                            ->wherenull('deleted_at')
                                            ->first();

                                return $so->so;
                            })
                            ->addColumn('remark',function($data){
                                return '<button class="btn btn-primary" data-invoice="'.$data->invoice.'" onclick="remark(this);"><span class="icon-eye2"></span></button>';
                            })
                            ->addColumn('action',function($data){
                                if ($data->do_number==null) {
                                    return '<button class="btn btn-danger" data-invoice="'.$data->invoice.'" onclick="cancelInv(this);"><span class="icon-cross"></span></button>';
                                }
                            })
                            // ->setRowAttr([
                            //     'style' => function($data)
                            //     {
                            //         if ($data->is_cancel_order==false) {
                            //             return  'background-color: #ffffff;'; 
                            //         }else{
                            //             return  'background-color: #ff4000;';
                            //         }
                                    
                                    
                            //     },
                            // ])
                            ->rawColumns(['action','so','plan_stuffing','remark'])
                            ->make(true);
    }

    public function exportDetailInvoice(Request $request){
        $radio = $request->radio;
        $po_number = $request->po_number;
        $invoice = explode(";", trim($request->invoice));
        $exfact = $request->exfact;
        $factory = $request->factory_id;
        $setname='';
     
        $date_exfact = explode('-', preg_replace('/\s+/', '', $request->exfact));
        $range_exfact = array(
            'from' => date_format(date_create($date_exfact[0]), 'Y-m-d'),
            'to' => date_format(date_create($date_exfact[1]), 'Y-m-d')
        );

             

        $data = DB::connection('replikasi_fgms')->table('v_ns_draf_planload')
                ->where('factory_id',$factory)
                ->where('invoice','<>',null);

        switch ($radio) {
            case 'invoice':
                    $data = $data->whereIn('invoice',$invoice);
                    $setname = "search_by_invoice_";
                break;
            case 'po':
                    $data = $data->where('po_number',$po_number);
                    $setname = "search_by_po_number_".$po_number;
                break;
            case 'plan':
                    $data = $data->whereBetween('plan_stuffing',[$range_exfact['from'],$range_exfact['to']]);
                    $setname = "search_by_plan_stuffing_".$range_exfact['from']."-".$range_exfact['to'];
                break;
           
        }

        
        $data = $data->orderby('plan_stuffing','asc')
                        ->orderby('fwd','asc')
                        ->orderby('invoice','asc')
                        ->orderby('po_number','asc')->get();

         $get_factory = DB::table('factory')
                        ->where('factory_id', $factory)
                        ->wherenull('deleted_at')
                        ->first();
       
        $filename = $get_factory->factory_name.'_report_invoice_'.$setname;
  
        return Excel::create($filename,function($excel) use ($data){
                $excel->sheet('active',function($sheet) use ($data){
                    $sheet->setCellValue('A1','#');
                    $sheet->setCellValue('B1','Plan Stuffing');
                    $sheet->setCellValue('C1','Style');
                    $sheet->setCellValue('D1','PO Number');
                    $sheet->setCellValue('E1','CRD');
                    $sheet->setCellValue('F1','PSDD');
                    $sheet->setCellValue('G1','MDD');
                    $sheet->setCellValue('H1','Destination');
                    $sheet->setCellValue('I1','Cust. No.');
                    $sheet->setCellValue('J1','Shipmode');
                    $sheet->setCellValue('K1','Invoice');
                    $sheet->setCellValue('L1','FWD');
                    $sheet->setCellValue('M1','SO');
                    $sheet->setCellValue('N1','QTY');
                    $sheet->setCellValue('O1','CTN');
                    $sheet->setCellValue('P1','CBM');
                    $sheet->setCellValue('Q1','GW');
                    $sheet->setCellValue('R1','NW');
                    $sheet->setCellValue('S1','Remark Exim');
                    $sheet->setCellValue('T1','Remark Container');
                    $sheet->setCellValue('U1','Remark Order');
                    $sheet->setCellValue('V1','Priority');
                    $sheet->setCellValue('W1','WHS');
                    $sheet->setCellValue('X1','Subsidary');
                    $sheet->setCellValue('Y1','Art. No.');
                    $sheet->setCellValue('Z1','Order Type');              
                    $sheet->setCellValue('AA1','PSFD');
                    $sheet->setCellValue('AB1','LC Date');
                    $sheet->setCellValue('AC1','Plan Ref. No.');
                    $sheet->setCellValue('AD1','Factory');
                    $sheet->setCellValue('AE1','PODD');
                    $sheet->setCellValue('AF1','Fact. Code');
                    $sheet->setCellValue('AG1','Stuffing Place');

                    
                    
                    

                    $i = 1;
                        
                    foreach ($data as $key => $dt) {
              
                        $inv = DB::table('invoice')->where('invoice',$dt->invoice)->wherenull('deleted_at')->first();
                        $rw = $i+1;

                        $sheet->setCellValue('A'.$rw,$i);
                        $sheet->setCellValue('B'.$rw,carbon::parse($dt->plan_stuffing)->format('d-M-Y'));
                        $sheet->setCellValue('C'.$rw,$dt->style);
                        $sheet->setCellValue('D'.$rw,$dt->po_number);
                        $sheet->setCellValue('E'.$rw,carbon::parse($dt->crd)->format('d-M-Y'));
                        $sheet->setCellValue('F'.$rw,carbon::parse($dt->po_stat_date_adidas)->format('d-M-Y'));
                        $sheet->setCellValue('G'.$rw,carbon::parse($dt->mdd)->format('d-M-Y'));
                        $sheet->setCellValue('H'.$rw,$dt->country_name);
                        $sheet->setCellValue('I'.$rw,$dt->customer_number);
                        $sheet->setCellValue('J'.$rw,$dt->shipmode);
                        $sheet->setCellValue('K'.$rw,$dt->invoice);
                        $sheet->setCellValue('L'.$rw,$dt->fwd);
                        $sheet->setCellValue('M'.$rw,$inv->so);
                        $sheet->setCellValue('N'.$rw,$dt->item_qty);
                        $sheet->setCellValue('O'.$rw,$dt->ctn);
                        $sheet->setCellValue('P'.$rw,(float)round($dt->cbm,3));
                        $sheet->setCellValue('Q'.$rw,(float)round($dt->gross,3));
                        $sheet->setCellValue('R'.$rw,(float)round($dt->net,3));
                        $sheet->setCellValue('S'.$rw,$dt->remark_exim);
                        $sheet->setCellValue('T'.$rw,$inv->remark_cont);
                        $sheet->setCellValue('U'.$rw,$dt->remark_order);
                        $sheet->setCellValue('V'.$rw,$dt->priority_code);
                        $sheet->setCellValue('W'.$rw,$dt->whs_code);
                        $sheet->setCellValue('X'.$rw,$dt->subsidary);
                        $sheet->setCellValue('Y'.$rw,$dt->buyer_item);
                        $sheet->setCellValue('Z'.$rw,$dt->order_type);
                        $sheet->setCellValue('AA'.$rw,carbon::parse($dt->psfd)->format('d-M-Y'));
                        $sheet->setCellValue('AB'.$rw,carbon::parse($dt->kst_lcdate)->format('d-M-Y'));
                        $sheet->setCellValue('AC'.$rw,'="'.$dt->plan_ref_number.'"');
                        $sheet->setCellValue('AD'.$rw,'="'.$dt->factory_id.'"');
                        $sheet->setCellValue('AE'.$rw,carbon::parse($dt->podd_check)->format('d-M-Y'));
                        $sheet->setCellValue('AF'.$rw, isset($dt->factory_code) ? $dt->factory_code : "-");
                        $sheet->setCellValue('AG'.$rw, $dt->stuffing_place_update);
                        

                        if ($dt->is_cancel_order==true) {
                            $sheet->cell('A'.$rw.':AG'.$rw, function($cell) {
                                    $cell->setBackground('#ff4000');
                                    $cell->setFontColor('#000000');
                            });
                        }else{
                            $sheet->cell('A'.$rw.':AG'.$rw, function($cell) {
                                    $cell->setBackground('#ffffff');
                                    $cell->setFontColor('#000000');
                            });
                        }
                        $i++;

                    }

                    $sheet->setColumnFormat(array(
                        'P'=>'0.00',
                        'Q'=>'0.00',
                        'R'=>'0.00'
                    ));



                });
                $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }

    public function updateExfact(Request $request){
        $invoice = $request->invoice;
        $exfact = $request->exfact;
        $getid= db::table('po_stuffing')->where('invoice',$invoice)->wherenull('deleted_at')->get();
        try {
            
            db::begintransaction();
                $dtup = array('plan_stuffing'=>$exfact,'updated_at'=>Carbon::now());
                db::table('invoice')->where('invoice',$invoice)->wherenull('deleted_at')->update($dtup);
                db::table('po_stuffing')->where('invoice',$invoice)->wherenull('deleted_at')->update($dtup);
                foreach ($getid as $gi) {
                    $this->_query_planmovement($gi->id,Auth::user()->id,"invoice (".$invoice.") change to exfact (".$exfact.")");
                }
            db::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function cancelInvoice(Request $request){
        $invoice = $request->invoice;
        $getid = DB::table('po_stuffing')->where('invoice',$invoice)->wherenull('deleted_at')->get();
        try {
            DB::beginTransaction();
                // DB::table('invoice')->where('invoice',$invoice)->wherenull('deleted_at')->update(['deleted_at'=>Carbon::now()]);
                DB::table('invoice')->where('invoice',$invoice)->delete();

                DB::table('po_stuffing')->where('invoice',$invoice)->wherenull('deleted_at')->update(['invoice'=>null,'plan_stuffing'=>null,'updated_at'=>Carbon::now()]);

                foreach ($getid as $gi) {
                    $this->_query_planmovement($gi->id,Auth::user()->id,"Cancel From invoice ".$invoice);
                }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function getRemarkDtInv(Request $request){
        $invoice = $request->invoice;

        $rminv = DB::table('invoice')->select('remark_cont')->where('invoice',$invoice)->whereNull('deleted_at')->first();
        $rmpo = DB::table('po_stuffing')->select('po_number','plan_ref_number','remark_exim','remark_order')->where('invoice',$invoice)->whereNull('deleted_at')->get();


        return response()->json(['remark_cont'=>$rminv->remark_cont,'remarkpo'=>$rmpo]);
    }

    public function setRemarkDetInv(Request $request){
        
        $invoice = $request->invoice;
        
        $remark_cont = $request->remark_cont;

        try {
            DB::beginTransaction();
                // DB::table('po_stuffing')->where('po_number',$po_number)->where('plan_ref_number',$plan_ref)->whereNull('deleted_at')->update(['remark_exim'=>$remark_exim,'remark_order'=>$remark_order,'updated_at'=>Carbon::now()]);
                DB::table('invoice')->where('invoice',$invoice)->whereNull('deleted_at')->update(['remark_cont'=>$remark_cont,'updated_at'=>Carbon::now()]);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
    }

    
    // delivery orders start
    public function deliveryOrder(){
        $factory_id = Auth::user()->factory_id;
        $plan = DB::table('invoice')->whereNull('do_number')->where('factory_id',$factory_id)->whereNull('deleted_at')->groupBy('plan_stuffing')->select('plan_stuffing')->orderby('plan_stuffing','desc')->get();
        $whs = DB::table('master_warehouse')->whereNull('deleted_at')->orderby('address','asc')->get();

        $dmax = DB::table('delivery_order_new')->where('factory_id',$factory_id)->where('is_draf',true)->max('do_number');
        $date = Carbon::now()->format('Ymd');
        $num = (int) substr($dmax,-4) +1;
        $drafno = 'DRAF-'.$factory_id.'-'.$date.'-'.sprintf("%04s", $num);
        return view('planload/delivery')->with('planstuffing',$plan)->with('whs',$whs)->with('draf',$drafno);
    }

    public function getDataDO(){
        $data = DB::table('invoice')->wherenull('do_number')->where('factory_id',Auth::user()->factory_id)->wherenull('deleted_at')->get();

        return DataTables::of($data)
                            ->addColumn('checkbox',function ($data){
                                return '<input type="checkbox" class="invplan" name="selector[]" id="Inputselector" value="'.$data->id.'" 
                                    data-cbm="'.$data->cbm.'" 
                                    data-ctn="'.$data->ctn.'" 
                                    data-qty="'.$data->qty.'" 
                                    data-gw="'.$data->gw.'"
                                    data-nw="'.$data->nw.'"
                                    data-plan="'.$data->plan_stuffing.'"
                                    data-ship="'.$data->shipmode.'"
                                    data-fwd="'.$data->fwd.'"
                                    data-invoice="'.$data->invoice.'">';
                            })
                            ->addColumn('po_number',function($data){
                                
                                $po_number = DB::table('po_stuffing')->where('invoice',$data->invoice)->whereNull('deleted_at')->groupBy('invoice')->select(DB::raw("string_agg(po_stuffing.po_number, ', ') as po_number"))->first();

                                return $po_number->po_number;
                            })
                            ->rawColumns(['checkbox','po_number'])
                            ->make(true);
    }

    public function ajaxGetShip(Request $request){
        $data = DB::table('invoice')->where('plan_stuffing',$request->plan)->whereNull('do_number')->where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->groupBy('shipmode')->select('shipmode')->orderby('shipmode','desc')->get();
        return response()->json(['ship'=>$data],200);
    }

    public function ajaxGetFwd(Request $request){
        $data = DB::table('invoice')->where('plan_stuffing',$request->plan)->where('shipmode',$request->shipmode)->whereNull('do_number')->where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->groupBy('fwd')->select('fwd')->orderby('fwd','desc')->get();
        return response()->json(['fwd'=>$data],200);
    }

    public function ajaxGetWhsAdd(Request $request){
        
        $fwd =$request->fwd;
        $data = DB::table('master_warehouse')->whereNull('deleted_at')->where('forwarder','LIKE','%'.$fwd.'%')->get();
        

        return response()->json(['whs'=>$data],200);
    }

    public function getTruck(Request $request){
        $data = DB::table('master_truck')->whereNull('deleted_at')->orderby('nopol','asc');

        return DataTables::of($data)
                                ->addColumn('action',function($data){
                                    return '<button class="btn btn-success"
                                                data-id="'.$data->id.'"
                                                data-nopol="'.$data->nopol.'"
                                                data-type="'.$data->type.'"
                                                data-driver="'.$data->driver.'"
                                                data-notelp="'.$data->notelp.'"
                                                data-expedisi="'.$data->expedisi.'"
                                            onclick="setTruck(this);"><span class="icon-checkmark4"></span></button>';
                                })
                                ->rawColumns(['action'])
                                ->make(true);
    }

    public function createDO(Request $request){
        $data =$request->data;
        $whs_id =$request->warehouse;
        $remark =$request->remark;
        $cbm =$request->cbm;
        $ctn =$request->ctn;
        $qty =$request->qty;
        $gw =$request->gw;
        $nw =$request->nw;
        $plan =$request->plan;
        $ship =$request->ship;
        $fwd =$request->fwd;
        $do_number =$request->do_number;

        $inv_id = [];
        $invoice = [];

        

        $cekDO = DB::table('delivery_order_new')->where('do_number',$do_number)->where('factory_id',Auth::user()->id)->whereNull('deleted_at')->exists();

        if ($cekDO==true) {
            $data_response = [
                            'status' => 422,
                            'output' => 'Delivery Number was already . . .'
                          ];
            
        }else{

            foreach ($data as $dt) {
                    $inv_id[] = $dt['id'];
                    $invoice[] = $dt['invoice'];
            }

            $getPO = DB::table('po_stuffing')->whereIn('invoice',$invoice)->whereNull('deleted_at')->get();
        
            $in_DO = array(
                'do_number'=>$do_number,
                'whs_id'=>$whs_id,
                'plan_stuffing'=>$plan,
                'shipmode'=>$ship,
                'fwd'=>$fwd,
                'cbm'=>round($cbm,3),
                'ctn'=>$ctn,
                'qty'=>$qty,
                'gw'=>round($gw,3),
                'nw'=>round($nw,3),
                'remark'=>$remark,
                'user_id'=>Auth::user()->id,
                'factory_id'=>Auth::user()->factory_id,
                'created_at'=>carbon::now(),
                'is_draf'=>true
            );
            
            $up_inv = array(
                'do_number'=>$do_number,
                'updated_at'=>carbon::now()
            );

            try {
                db::begintransaction();

                    DB::table('delivery_order_new')->insert($in_DO);
                    DB::table('invoice')->whereIn('id',$inv_id)->update($up_inv);

                    foreach ($getPO as $po) {
                        $this->_query_planmovement($po->id,Auth::user()->id,"create to delivery order number");
                    }

                db::commit();
                $data_response = [
                                'status' => 200,
                                'output' => 'Success create draf delivery order number '.$do_number.' . . .'
                              ];
            } catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);

                $data_response = [
                                'status' => 422,
                                'output' => 'Filed create deliver number '.$message
                              ];
            }
        }

        
        return response()->json(['data'=>$data_response]);
    }


    public function detailDelivery(){
        $factory = DB::table('factory')->whereNull('deleted_at')->get();
        return view('planload/delivery_detail')->with('factory',$factory);
    }

    public function getDetailDelivery(Request $request){

        $radio =$request->radio;
        $do_number = trim($request->do);
        $factory_id = $request->factory_id;
       

        $data = DB::table('delivery_order_new')->where('factory_id',$factory_id)->whereNull('deleted_at')->orderby('plan_stuffing','desc');
        
        if ($radio=='do'&& $do_number!=null) {
            $data =$data->where('do_number','like','%'.$do_number);
        }else if ($radio=='plan'){
            $date_exfact = explode('-', preg_replace('/\s+/', '', $request->exfact));
            $range_exfact = array(
                'from' => date_format(date_create($date_exfact[0]), 'Y-m-d'),
                'to' => date_format(date_create($date_exfact[1]), 'Y-m-d')
            );

            $data = $data->whereBetween('plan_stuffing',[$range_exfact['from'],$range_exfact['to']]);
        }else{
            $data = array();
        }

        // return json_decode($data->pluck('do_number'));
        
        return DataTables::of($data)
                            ->addColumn('invoice',function($data){
                                $inv = DB::table('invoice')->where('do_number',$data->do_number)->whereNull('deleted_at')->groupBy('do_number')->select(DB::raw("string_agg(invoice.invoice, ', ') as invoice"))->first();
                                return $inv->invoice ?? '-';
                            })
                            ->editColumn('whs_id',function($data){
                                $whs = DB::table('master_warehouse')->where('id',$data->whs_id)->first();

                                return $whs->whs_name." - ".$whs->address;
                            })
                            ->addColumn('setting',function($data){
                               if ($data->is_draf==true) {
                                    $invc = DB::table('invoice')->where('do_number',$data->do_number)->whereNull('deleted_at')->groupBy('do_number')->select(DB::raw("string_agg(invoice.invoice, ', ') as invoice"))->first();
                                    
                                    return '<button class="btn btn-success" data-id="'.$data->id.'" data-do="'.$data->do_number.'" data-inv="'.$invc->invoice.'" onclick="setDotruck(this);"><span class="icon-truck"></span></button>';
                                }else{
                                    return '<button class="btn btn-info" data-id="'.$data->id.'" onclick="setting(this);"><span class="icon-list"></span></button>';
                                }
                            })
                            ->addColumn('action', function($data){
                                return '<button class="btn btn-danger" data-id="'.$data->id.'" data-do="'.$data->do_number.'" onclick="cancelDo(this);"><span class="icon-trash"></span></button>';
                            })
                            ->rawColumns(['invoice','whs_id','setting','action'])
                            ->make(true);
    }

    public function getSetting(Request $request){
        $id = $request->id;

        $gt = DB::table('delivery_order_new')->join('master_warehouse','master_warehouse.id','=','delivery_order_new.whs_id')->where('delivery_order_new.id',$id)->whereNull('delivery_order_new.deleted_at')->first();

        $data = array(
            'do_number'=>$gt->do_number,
            'plans'=>$gt->plan_stuffing,
            'whs'=>$gt->whs_name." - ".$gt->address,
            'address'=>$gt->address,
            'fwd'=>$gt->fwd,
            'ship'=>$gt->shipmode,
            'nopol'=>$gt->nopol,
            'driver'=>$gt->driver,
            'expedisi'=>$gt->expedisi,
            'type'=>$gt->type,
            'remark'=>$gt->remark,
            'finish_dt'=>isset($gt->stuffing_finished) ? date_format(date_create($gt->stuffing_finished),'Y-m-d') : null,
            'finish_hr'=> isset($gt->stuffing_finished) ? date_format(date_create($gt->stuffing_finished),'H') : null,
            'finish_mn'=> isset($gt->stuffing_finished) ? date_format(date_create($gt->stuffing_finished),'i') : null,
            'out_dt'=> isset($gt->out_factory) ? date_format(date_create($gt->out_factory),'Y-m-d') : null,
            'out_hr'=> isset($gt->out_factory) ? date_format(date_create($gt->out_factory),'H') : null,
            'out_mn'=> isset($gt->out_factory) ? date_format(date_create($gt->out_factory),'i') : null,
            'in_dt'=> isset($gt->in_whs) ? date_format(date_create($gt->in_whs),'Y-m-d') : null,
            'in_hr'=> isset($gt->in_whs) ? date_format(date_create($gt->in_whs),'H') : null,
            'in_mn'=> isset($gt->in_whs) ? date_format(date_create($gt->in_whs),'i') : null,
            'seal_number'=>$gt->seal_number

        );
        
        return response()->json(['data'=>$data],200);

    }

    public function updateDelivery(Request $request){
        $id = $request->id;
        $seal_number = trim($request->seal);
        $pic_seal = trim($request->picseal);
        $stuff = $request->dt_stuff.' '.$request->hr_stuff.':'.$request->mn_stuff.':00';
        $finish = date_format(date_create($stuff),'Y-m-d H:i:s');

        $outf = $request->dt_out.' '.$request->hr_out.':'.$request->mn_out.':00';
        $out = date_format(date_create($outf),'Y-m-d H:i:s');

        $inwh = $request->dt_in.' '.$request->hr_in.':'.$request->mn_in.':00';
        $in = date_format(date_create($inwh),'Y-m-d H:i:s');

        $remark = trim($request->remark);
        

      
        
        try {
            DB::begintransaction();
                $dt_up = array(
                    'seal_number'=>$seal_number,
                    'seal_pic'=>$pic_seal,
                    'stuffing_finished'=>$finish,
                    'out_factory'=>$out,
                    'in_whs'=>$in,
                    'updated_at'=>carbon::now(),
                    'remark'=>$remark
                );
                DB::table('delivery_order_new')->where('id',$id)->update($dt_up);

                $data_response = [
                            'status' => 200,
                            'output' => 'Update Delivery Order success . . .'
                          ];
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                            'status' => 422,
                            'output' => 'Update Delivery Filed !!!'
                          ];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function updateDO(Request $request){
        $id = $request->iddomd;
        $do = $request->do_numbermd;
        $expedisi = $request->mdexpedisi;
        $nopol = $request->mdnopol;
        $type = $request->mdtype;
        $driver = $request->mddriver;
        $notlp = $request->mdnotlp;
        $nocontiner = $request->mdnocont;
        $inv = explode(", ",$request->invmd);

        $cek_truck = DB::table('master_truck')->where('nopol',$nopol)->where('driver',$driver)->whereNull('deleted_at')->exists();

        $dt_trk = array(
            'nopol'=>$nopol,
            'type'=>$type,
            'expedisi'=>$expedisi,
            'driver'=>$driver,
            'notelp'=>$notlp,
            'created_at'=>carbon::now()
        );
        
        $up_do = array(
            'do_number'=>$do,
            'nopol'=>$nopol,
            'type'=>$type,
            'expedisi'=>$expedisi,
            'driver'=>$driver,
            'notelp'=>$notlp,
            'updated_at'=>carbon::now(),
            'is_draf'=>false,
            'no_container'=>$nocontiner
        );

        $up_inv = array(
            'do_number'=>$do,
            'updated_at'=>carbon::now()
        );

        try {
            db::begintransaction();
                DB::table('delivery_order_new')->where('id',$id)->whereNull('deleted_at')->update($up_do);
                DB::table('invoice')->whereIn('invoice',$inv)->whereNull('deleted_at')->update($up_inv);
                if ($cek_truck==false) {
                    DB::table('master_truck')->insert($dt_trk);
                }
            db::commit();
               $data_response = [
                            'status' => 200,
                            'output' => 'Update Delivery order '.$do.' success !!!'
                          ];
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                            'status' => 422,
                            'output' => 'Update Delivery order '.$do_number.' filed !!!'
                          ];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function exportDetailDelivery(Request $request){
        $radio = $request->radio;
        $do_number = trim($request->do);
        $exfact = $request->exfact;
        $factory_id = $request->factory_id;

        $data = DB::table('delivery_order_new')->where('factory_id',$factory_id)->whereNull('deleted_at');
        
        if ($radio=='do'&& $do_number!=null) {
            $data =$data->where('do_number','like','%'.$do_number);
        }else if ($radio=='plan'){
            $date_exfact = explode('-', preg_replace('/\s+/', '', $request->exfact));
            $range_exfact = array(
                'from' => date_format(date_create($date_exfact[0]), 'Y-m-d'),
                'to' => date_format(date_create($date_exfact[1]), 'Y-m-d')
            );

            $data = $data->whereBetween('plan_stuffing',[$range_exfact['from'],$range_exfact['to']]);
        }else{
            $data = array();
        }

        $get_factory = DB::table('factory')
                            ->where('factory_id',$factory_id)
                            ->whereNull('deleted_at')->first();

        $filename = $get_factory->factory_name.'_report_delivery_order_detail_filter_by_'.$radio;
        $i = 1;
        $data_result = $data->orderBy('plan_stuffing','asc')->orderBy('do_number','asc')->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }
        if ($data_result->count()>0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use ($i){
                $inv = DB::table('invoice')->where('do_number',$row->do_number)->whereNull('deleted_at')->groupBy('do_number')->select(DB::raw("string_agg(invoice.invoice, ', ') as invoice"))->first();

                $whs = DB::table('master_warehouse')->where('id',$row->whs_id)->first();

                return
                    [
                        '#'=> $row->no ,
                        'Delivery No.'=> $row->do_number ,
                        'Plan Stuff.'=> $row->plan_stuffing ,
                        'Warehouse'=> $whs->whs_name ,
                        'Address'=> $whs->address ,
                        'Shipmode'=> $row->shipmode ,
                        'Fwd'=> $row->fwd ,
                        'Police No.'=> $row->nopol ,
                        'Expedisi'=> $row->expedisi ,
                        'Driver'=> $row->driver ,
                        'No. Telp.'=> $row->notelp ,
                        'Type'=> $row->type ,
                        'Seal No.'=> $row->seal_number ,
                        'Stuffing Finished'=> date_format(date_create($row->stuffing_finished),'d-m-Y H:i:s') ,
                        'Out Factory'=> date_format(date_create($row->out_factory),'d-m-Y H:i:s') ,
                        'In Warehouse'=> date_format(date_create($row->in_whs),'d-m-Y H:i:s') ,
                        'CBM'=> $row->cbm ,
                        'CTN'=> $row->ctn ,
                        'QTY'=> $row->qty ,
                        'Gross'=> $row->gw ,
                        'Net'=> $row->nw ,
                        'Remark'=> $row->remark 

                    ];

            });
        }else{
            return response()->json('no data',422);
        }

    }

     public function exportTruckDetail(Request $request){
        $radio = $request->radio;
        $do_number = trim($request->do);
        $exfact = $request->exfact;
        $factory = $request->factory;

        $data = DB::table('v_ns_truck_erp')->where('factory_id',$factory);
        
        if ($radio=='do'&& $do_number!=null) {
            $data =$data->where('do_number','like','%'.$do_number);
        }else if ($radio=='plan'){
            $date_exfact = explode('-', preg_replace('/\s+/', '', $request->exfact));
            $range_exfact = array(
                'from' => date_format(date_create($date_exfact[0]), 'Y-m-d'),
                'to' => date_format(date_create($date_exfact[1]), 'Y-m-d')
            );

            $data = $data->whereBetween('plan_stuffing',[$range_exfact['from'],$range_exfact['to']]);
        }else{
            $data = array();
        }

        $get_factory = DB::table('factory')
                            ->where('factory_id',$factory)
                            ->whereNull('deleted_at')->first();

        $filename = $get_factory->factory_name.'_report_truck_invoice_'.$radio;
        $i = 1;
        $data_result = $data->orderBy('plan_stuffing','asc')->orderBy('do_number','asc')->get();

        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }
        if ($data_result->count()>0) {
            return (new FastExcel($data_result))->download($filename.'.csv',function($row) use ($i){
             

                


                return 
                    [
                        'Invoice Vendor No'=>"",
                        'Truck Type'=>$row->nopol,
                        'Driver Name'=>$row->driver,
                        'Driver Phone'=>'="'.$row->notelp.'"',
                        'Vendor Trucking Value'=>$row->expedisi,
                        'Stuffing finished date'=>date_format(date_create($row->stuffing_finished),'d/m/Y H:i'),
                        'Truck out'=>date_format(date_create($row->out_factory),'d/m/Y H:i'),
                        'Truck Get In Destination'=>date_format(date_create($row->in_whs),'d/m/Y H:i'),
                        'Seal Factory No'=>$row->seal_number,
                        'Seal Container NO'=>"",
                        'Warehouse Trucking'=>$row->whs_name,
                        'Accounting Loaction'=>$row->factory_name,
                        'Charge'=>"",
                        'Remark'=>"",
                        'Charge Amount'=>"",
                        'Invoice AOI'=>$row->invoice,
                        'Surat Jalan'=>$row->do_number,
                        'So Shipping Number'=>$row->so,
                        'Plan Stuffing'=>date_format(date_create($row->plan_stuffing),'d/m/Y')

                    ];

            });
        }else{
            return response()->json('no data',422);
        }

    }



    public function cancelDO(Request $request){
        $id = $request->id;
        $do_number = $request->do;
        $inv = array();
        $invid = array();
        $poid = array();


        $gnv = DB::table('invoice')->where('do_number',$do_number)->whereNull('deleted_at')->get();
        foreach ($gnv as $gnv ) {
            $inv[]=$gnv->invoice;
            $invid[]=$gnv->id;
        }

        try {
            db::begintransaction();
                $gpo = DB::table('po_stuffing')->whereIn('invoice',$inv)->whereNull('deleted_at')->get();

                foreach ($gpo as $gpo) {
                    $this->_query_planmovement($gpo->id,Auth::user()->id,"cancel from delivery order number ( ".$do_number." )");
                }

                DB::table('invoice')->whereIn('id',$invid)->whereNull('deleted_at')->update(['do_number'=>null,'updated_at'=>carbon::now()]);
                DB::table('delivery_order_new')->where('id',$id)->update(['do_number'=>null,'deleted_at'=>carbon::now()]);

                $data_response = [
                            'status' => 200,
                            'output' => 'Cancel Delivery order '.$do_number.' success !!!'
                          ];

            db::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                            'status' => 422,
                            'output' => 'Cancel Delivery order '.$do_number.' filed !!!'
                          ];
        }

        return response()->json(['data'=>$data_response]);
        
    }


    // end delivery orders
    


    //planload movements
    private function _query_planmovement($idstuff,$user,$desc){
     
        $q_move = array(
            'id_stuff'=>$idstuff,
            'user_id'=>$user,
            'description'=>$desc,
            'created_at'=>carbon::now()
        );

        try {
            db::beginTransaction();
                DB::table('planload_movements')->insert($q_move);
            db::commit();
        } catch (Exception $ex) {
            DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
        }

        return true;
    }
    //end plan load movements
} 
?>
