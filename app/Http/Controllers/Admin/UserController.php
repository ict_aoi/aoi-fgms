<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use Auth;
use Hash;
use Validator;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Role;
use App\Role_User;
use Redirect;

class UserController extends Controller
{

     public function index(Request $request){
            return view('admin.user.index');
        }

    public function getDatatables(Request $request){

           if ($request->ajax()){
           $users = DB::table('users')
                ->select('id','nik','name','email','division','warehouse','factory_id', 'factory_name', 'created_at','updated_at')
                ->where('factory_id', Auth::user()->factory_id)
                ->wherenull('deleted_at')
                ->orderBy('created_at', 'desc')
                ->get();

            //datatables
            return DataTables::of($users)
              ->addColumn('action', function($users){ return view('_action', [
                'edit' => route('user.edit', $users->id),
                'userid' => $users->id,
                'delete_user' => route('user.delete', $users->id)]);})
            ->make(true);
        }
    }

    public function create(){
        $users = User::all();
        $roles = Role::whereNull('deleted_at')->get();
        $factory = DB::table('factory')
                    ->select('factory_id', 'factory_name', 'deleted_at')
                    ->wherenull('deleted_at')
                    ->get();
            return view('admin.user.create', compact('roles'), compact('users'))
                    ->with('factory', $factory);
    }

    public function accountSetting($id)
    {
        $user = User::find($id);
        return view('admin.user.account_setting')
            ->with('user',$user);
    }

    public function resetPassword(Request $request)
    {
        $password = bcrypt('1234');

        $users = DB::table('users')
                        ->where('id', $request['id'])
                        ->update(['password' => $password]);

        return '1234';
    }

    public function updatepassword(Request $request, $id)
    {
        $validator =  Validator::make($request->all(), [
            'password_old' => 'required',
            'password_new' => 'required|different:password_old',
            'password_confirm' => 'required|same:password_new',
        ]);

        if ($validator->passes()) {
            try{
                db::beginTransaction();
                if (Hash::check($request->get('password_old'), Auth::user()->password)) {
                    $user = User::find($id);
                    $user->password = bcrypt($request->password_new);

                    if($user->update()){
                        db::commit();
                        return response()->json('Success', 200);
                    }
                }

            }catch (Exception $ex){
                db::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

        }else{
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ],400);
        }
    }


    public function store(Request $request){

        $data=$request->all();

        // $this->validate($request, [
        //     'name' => 'required|min:3',
        //     'nik' => 'required',
        //     'password' => 'required|min:6',
        //     'email' => 'required|email',
        //     'division' => '',
        //     'warehouse' => ''
        // ]);

    $data = new User;
    $data->name = $request['name'];
    $data->nik = $request['nik'];
    $data->password = bcrypt($request['password']);
    $data->email = $request['email'];
    // $data->factory_id = json_encode($request['factory']);
    $data->factory_id = $request['factory'];

    $fname = DB::table('factory')
                          ->select('factory_name', 'locator_sewing', 'locator_inventory')
                          ->where('factory_id', $request['factory'])
                          ->wherenull('deleted_at')
                          ->first();

    $data->factory_name = $fname->factory_name;

    $data->locator_sewing = $fname->locator_sewing;
    $data->locator_inventory = $fname->locator_inventory;

    // $data->division = $request['division'];
    // $data->warehouse = $request['warehouse'];
    $data->created_at = carbon::now();

    if($data->save()) {
         $role_user = DB::table('role_user')
                     ->insert(['user_id' => $data->id,
                               'role_id' => $request->role_id]);
    }

        return Redirect::back()->with('alert','Save Successful !');
    }

    public function edit($id){
        $roles = DB::table('roles')
            ->select('id', 'name', 'display_name', 'description', 'created_at', 'updated_at')
            ->whereNull('deleted_at')
            ->orderBy('created_at', 'desc')
            ->get();

        $user_roles = DB::table('role_user')
                          ->select('role_id')
                          ->where('user_id', $id)
                          ->first();
        $user = User::find($id);

        $factory = DB::table('factory')
                    ->select('factory_id', 'factory_name', 'deleted_at')
                    ->wherenull('deleted_at')
                    ->get();

            return view('admin.user.edit', compact('users'), compact('roles'))
            ->with('user',$user)
            ->with('factory',$factory)
            ->with('user_roles', $user_roles->role_id);
    }

    public function update(Request $request){
        $data = $request->all();

        $users['nik'] = $data['nik'];
        $users['name'] = $data['name'];
        $users['email'] = $data['email'];
        $users['factory_id'] = $data['factory'];
        // $users['division'] = $data['division'];
        // $users['warehouse'] = $data['warehouse'];

        $factory = DB::table('factory')
                    ->select('factory_id', 'factory_name', 'locator_sewing', 'locator_inventory')
                    ->where('factory_id', $data['factory'])
                    ->whereNull('deleted_at')
                    ->first();

        $users['factory_name'] = $factory->factory_name;
        $users['locator_sewing'] = $factory->locator_sewing;
        $users['locator_inventory'] = $factory->locator_inventory;

        if(isset($data['resetpassword'])) {
            if($data['resetpassword'] == "1"){
                $users['password'] = bcrypt($data['nik'].'123');
            }
        }

        $users = DB::table('users')
                        ->where('id', $data['id'])
                        ->update($users);

          $role_user = DB::table('role_user')
                      ->where('user_id', $data['id'])
                      ->update(['role_id' => $data['role_id']]);
                      return redirect('admin/user');

    }

    public function destroy(Request $request)
    {   //For Deleting users
        $id = $request->id;
        // $Users = User::find($id)->delete();
        $user = User::find($id);
        $user->deleted_at = carbon::now();
        $user->save();
    }

}
