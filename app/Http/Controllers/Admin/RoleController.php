<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use Auth;
use Validator;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Role;

class RoleController extends Controller
{

    public function index()
    {
        return view('admin.role.index');
    }

    public function getDatatables(Request $request)
    {
        if ($request->ajax()){
            $roles = DB::table('roles')
                ->select('id', 'name', 'display_name', 'description', 'created_at', 'updated_at')
                ->whereNull('deleted_at')
                ->orderBy('created_at', 'desc')
                ->get();

            //datatables
            return DataTables::of($roles)
                ->addColumn('action', function($roles){ return view('_action', [
                    'edit' => route('role.edit', [
                        'asd' => $roles->id,
                        '_id' => $roles->id,
                        'name' => $roles->name,
                        'display_name' => $roles->display_name,
                        'description' => $roles->description
                    ]),
                    'roleid' => $roles->id,
                    'delete_role' => route('role.delete', $roles->id)]);})
                ->make(true);
        }
    }

    public function create()
    {
        $permissions = DB::table('permissions')->select('id', 'name','display_name', 'description')->get();

        return view('admin.role.create', ['permissions'=>$permissions]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $permission_ids = $request->active;
        $this->validate($request, [
            'name' => 'required|min:3',
            'display_name' => 'required',
            'description' => 'required|min:3'
        ]);

        try {
            db::beginTransaction();
            //insert data ke table role
            $data = new Role;
            $data->name = $request['name'];
            $data->display_name = $request['display_name'];
            $data->description = $request['description'];
            $data->created_at = carbon::now();
            //jika insert sukses -> ambil role_id ->kemudian masukin ke permission_role
            if($data->save()) {
                foreach ($permission_ids as $key => $value) {
                    $permission_role = array(
                        'role_id' => $data->id,
                        'permission_id' => $value
                    );
                    $inserted_permissionrole[] = $permission_role;
                }

                DB::table('permission_role')->insert($inserted_permissionrole);
            }
            db::commit();
        }
        catch(Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return \Redirect::back()->with('alert','Insert Successful !');

    }

    public function edit(Request $request)
    {
        $role = $request->all();
        $permission_role = DB::table('permission_role')->select('permission_id')->where('role_id', '=', $role['_id'])->get();
        $permissions = DB::table('permissions')->select('id', 'name', 'display_name', 'description')->get();

        return view('admin.role.edit', ['permissions'=>$permissions, 'role'=>$role, 'permission_role' => $permission_role]);
    }

    public function update(Request $request)
    {
        $data = $request->all();

        try {
            db::beginTransaction();
            if ($data['active']) {
                DB::table('permission_role')->where('role_id', '=', $data['id'])->delete();
            }

            foreach ($data['active'] as $key => $value) {
                $permission_role = array(
                        'role_id' => $data['id'],
                        'permission_id' => $value
                    );
                    $inserted_permissionrole[] = $permission_role;
                }

                DB::table('permission_role')->insert($inserted_permissionrole);

            db::commit();
        }
        catch(Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return view('admin.role.index');
    }

    public function delete(Request $request)
    {
        //For Deleting Roles
        $id = $request->id;
        $roles = Role::find($id);
        $roles->deleted_at = carbon::now();
        $roles->save();

        return \Redirect::back()->with('alert','Delete Successful !');
    }
}
