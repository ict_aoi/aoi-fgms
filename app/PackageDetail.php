<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageDetail extends Model
{
    protected $table = 'package_detail';
    protected $primaryKey = 'scan_id';
    protected $keyType = 'string';
    public $incrementing = 'false';

    protected $fillable = [
        'scan_id','po_number','package_range','package_from',
        'package_to','serial_from','serial_to','pack_instruction_code',
        'line','sku','buyer_item','model_number','customer_size',
        'customer_number','manufacturing_size','technical_print_index',
        'gps_three_digit_size','service_identifier','packing_mode',
        'item_qty','inner_pack','inner_pkg_count','pkg_count',
        'net_net','net','gross','unit_1','length','width','height',
        'r','pkg_code','unit_2'
    ];

    protected $dates = ['created_at','updated_at','deleted_at'];
}
