<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = 'package';
    protected $primaryKey = 'barcode_id';
    protected $keyType = 'string';
    public $incrementing = 'false';

    protected $fillable = [
        'barcode_id',
        'scan_id',
        'package_number',
        'serial_number',
        'department_status',
        'status'
    ];

    protected $dates = ['created_at','updated_at','deleted_at'];
}
