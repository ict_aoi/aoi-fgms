<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locator extends Model
{
    protected $table = 'locators';
    protected $guarded = ['id'];
    protected $dates = ['created_at','updated_at','deleted_at'];
}
