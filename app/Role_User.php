<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole_User;
class Role_User extends EntrustRole_User
{
    //
   	protected $table = 'role_user';

     protected $fillable = ['user_id','role_id'];
}
