<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageMovement extends Model
{
    protected $table = 'package_movements';
    protected $guarded = ['id'];
    protected $dates = ['created_at'];
}
