<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class POStuffing extends Model
{
    protected $table = 'po_stuffing';
    protected $guarded = [];
}
