<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackingPlan extends Model
{
    protected $table = 'packing_plan';
    protected $primaryKey = 'plan_ref_number';
    protected $keyType = 'string';
    public $incrementing = 'false';

    protected $fillable = [
        'plan_ref_number', 'booking_number', 'final_workflow',
        'estimated_ex_factory_date', 'notes', 'plan_date',
        'authorization', 'customer_number', 'trigger_label',
        'buyer_name', 'buyer_address', 'buyer_city',
        'buyer_postalcode', 'buyer_country', 'buyer_phone',
        'buyer_fax', 'destination_name', 'destination_address',
        'destination_city', 'destination_country'
    ];
    protected $dates = ['created_at','updated_at','deleted_at'];
}
