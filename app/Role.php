<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    //
    protected $fillable = ['id','name','display_name','description','created_at','updated_at'];
    protected $dates = ['created_at','update_at'];


    public function permission(){
        return $this->belongsToMany('App\Permission','permission_role','role_id','permission_id');
    }

    public function user(){
        return $this->belongsToMany('App\User','role_user','role_id','user_id');
    }

}
