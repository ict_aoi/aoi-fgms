<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PoSummary extends Model
{
    protected $table = 'po_summary';
    protected $primaryKey = 'po_number';
    protected $keyType = 'string';
    public $incrementing = 'false';

    protected $fillable = [
        'po_number',
        'plan_ref_number',
        'status_preparation',
        'status_production',
        'status_scanpack',
        'status_stuffing'
    ];
    protected $dates = ['created_at','updated_at','deleted_at'];
}
