<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roles = [
            // [
            //     'name' => 'admin',
            //     'display_name' => 'Administrator',
            //     'description' => 'Administrator'
            // ],
            // [
            //     'name' => 'packing',
            //     'display_name' => 'Packing',
            //     'description' => 'Packing'
            // ]
            [
                'name' => 'sewing',
                'display_name' => 'Sewing',
                'description' => 'Sewing'
            ]
        ];

        foreach ($roles as $key => $value) {
            Role::create($value);
        }
    }
}
