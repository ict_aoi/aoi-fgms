<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = [
            // [
            //     'name' => 'Administrator',
            //     'nik' => 'Admin',
            //     'email' => 'admin@test.com',
            //     'warehouse'=> '1000011',
            //     'password'=> bcrypt('admin')
            // ],
            [
                'name' => 'User Inventory 1',
                'nik' => 'inventory1',
                'email' => 'inventory1@test.com',
                'warehouse'=> '1000011',
                'password'=> bcrypt('inventory1')
            ],
            [
                'name' => 'User QC Inspect 1',
                'nik' => 'qcinspect1',
                'email' => 'qcinspect1@test.com',
                'warehouse'=> '1000011',
                'password'=> bcrypt('qcinspect1')
            ],
            [
                'name' => 'User Shipping 1',
                'nik' => 'shipping1',
                'email' => 'shipping1@test.com',
                'warehouse'=> '1000011',
                'password'=> bcrypt('shipping1')
            ]
        ];

        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
