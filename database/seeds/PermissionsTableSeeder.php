<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $permissions = [
            [
                'name' => 'menu-admin-dashboard',
                'display_name' => 'Menu Admin Dashboard',
                'description' => 'Menu Admin Dashboard'
            ],
            [
                'name' => 'menu-packing-dashboard',
                'display_name' => 'Menu Packing Dashboard',
                'description' => 'Menu Packing Dashboard'
            ],
        ];

        foreach ($permissions as $key => $value) {
            Permission::create($value);
        }
    }
}
