<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//lOGIN
Route::get('/', function () {
    if(Auth::check()) {
        $getRoles = \DB::table('role_user')
                        ->select('name')
                        ->join('roles', 'roles.id', '=', 'role_user.role_id')
                        ->where('user_id', Auth::User()->id)
                        ->first();

        if($getRoles) {
            if(strtolower($getRoles->name) == 'storing') {
                return redirect()->route('inventory.placement');
            }
            elseif(strtolower($getRoles->name) == 'receiving') {
                return redirect()->route('inventory.checkin');
            }
        }

        return redirect()->route('dashboard');
    }
    return redirect('/login');
});

//LOGOUT DARURAT ONLY
Route::get('/logoutdarurat', function() {
    if(Auth::check()) {
        Auth::logout();
    }
    return redirect('/');
});

Route::get('/home', function() {
    if(Auth::check()) {
        return redirect('/');
    }
    return redirect('/login');
});

Auth::routes();

Route::middleware('auth')->group(function () {

    //DASHBOARD
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/dashboard/get-data', 'DashboardController@getData')->name('ajaxGetData');
    Route::get('/dashboard/po-number/{po_number}/package', 'DashboardController@viewPackage')->name('viewPackage');
    Route::get('/dashboard/po-number/package/get-data', 'DashboardController@viewPackageData')->name('ajaxPackageData');
    Route::get('/dashboard/po-number/{po_number}/package/{barcode_id}/print-barcode', 'DashboardController@printBarcodePackage')->name('printBarcodePackage');

    // get count request carton
    Route::get('/dashboard/get-data-carton', 'DashboardController@getDataCarton')->name('ajaxGetDataCarton');

    // export dashboard
    Route::get('/dashboard-export', 'DashboardController@dashboardExport')->name('dashboardExport');

    //account setting
    Route::get('/user/account-setting/{id}', 'Admin\UserController@accountSetting')->name('user.accountSetting');
    Route::post('/user/account-setting/update-password/{id}', 'Admin\UserController@updatepassword')->name('user.updatepassword');

    //ROUTE ADMIN
    Route::prefix('admin')->group(function () {
        Route::middleware('role:admin')->group(function () {

            //user management
            Route::get('/user', 'Admin\UserController@index')->name('user.index');
            Route::get('/user/getdatatables', 'Admin\UserController@getDatatables')->name('user.getDatatables');
            Route::get('/user/add-user', 'Admin\UserController@create')->name('user.create');
            Route::get('/user/edit-user/{id}', 'Admin\UserController@edit')->name('user.edit');
            Route::post('/user/user-store', 'Admin\UserController@store')->name('user.store');
            Route::post('/user/update-user', 'Admin\UserController@update')->name('user.update');
            Route::get('/user/edit-user/{id}/reset-password', 'Admin\UserController@resetPassword')->name('user.resetPassword');
            Route::get('/user/delete-user/{id}', 'Admin\UserController@destroy')->name('user.delete');
            Route::get('/user-role', 'Admin\UserController@role')->name('user.role');

            //role
            Route::get('/role', 'Admin\RoleController@index')->name('role.index');
            Route::get('/role/getdatatables', 'Admin\RoleController@getDatatables')->name('role.getDatatables');
            Route::get('/role/add-role', 'Admin\RoleController@create')->name('role.create');
            Route::post('/role/role-store', 'Admin\RoleController@store')->name('role.store');
            Route::get('/role/edit-role/{id}', 'Admin\RoleController@edit')->name('role.edit');
            Route::post('/role/update-role', 'Admin\RoleController@update')->name('role.update');
            Route::get('/role/delete-role/{id}', 'Admin\RoleController@delete')->name('role.delete');
        });
    });

    //ROUTE PACKING
    Route::prefix('packing')->group(function () {
        //ANOTHER
        // Route::get('/input-packing-plan', 'Packing\InputPackingPlanController@index')->name('packing.inputPackingPlan');
        // Route::post('/input-packing-plan/submit', 'Packing\InputPackingPlanController@ajaxSubmit')->name('packing.ajaxInputPackingPlan');
        // Route::get('/input-purchase-order', 'Packing\InputPurchaseOrderController@index')->name('packing.inputPurchaseOrder');
        // Route::get('/input-package', 'Packing\InputPackageController@index')->name('packing.inputPackage');
        // Route::post('/input-package/submit', 'Packing\InputPackageController@ajaxSubmit')->name('packing.ajaxInputPackage');
        // Route::get('/search-packing-plan', 'Packing\SearchPackingPlanController@index')->name('packing.searchPackingPlan');
        // Route::get('/search-purchase-order', 'Packing\SearchPurchaseOrderController@index')->name('packing.searchPurchaseOrder');
        // Route::get('/search-purchase-order/get-data', 'Packing\SearchPurchaseOrderController@getData')->name('packing.ajaxGetDataPo');
        // Route::get('/sewing-plan', 'Packing\SewingPlanController@index')->name('packing.sewingPlan');
        // Route::get('/sewing-plan/get-data', 'Packing\SewingPlanController@getData')->name('packing.ajaxGetSewingPlan');
        //END OF ANOTHER

        //packing list
        Route::get('/packing-list', 'Packing\PackingListController@index')->name('packing.packingList');
        Route::get('/packing-list/get-data', 'Packing\PackingListController@getData')->name('packing.ajaxGetPackingList');
        Route::post('/packing-list/get-detail', 'Packing\PackingListController@getDetail')->name('packing.ajaxGetDetail');
        Route::post('/packing-list/modal-upload', 'Packing\PackingListController@modalUpload')->name('packing.modalUpload');
        Route::post('/packing-list/upload-excel', 'Packing\PackingListController@uploadExcel')->name('packing.uploadExcel');
        Route::post('/packing-list/revisi-excel', 'Packing\PackingListController@revisiExcel')->name('packing.revisiExcel');
        Route::get('/packing-list/get-current-packinglist', 'Packing\PackingListController@getCurrentPackingList')->name('packing.getCurrentPackingList');
        Route::get('/packing-list/po-number/{po_number}/package', 'Packing\PackingListController@viewPackage')->name('packing.viewPackageDetail');
        Route::post('/packing-list/update-planref', 'Packing\PackingListController@updatePlanref')->name('packing.updatePlanref');
        Route::post('/packing-list/sync-item', 'Packing\PackingListController@sync_PL')->name('packing.sync_PL');

       

         // costco
        Route::get('/packing-list/getCostco', 'Packing\PackingListController@getCostco')->name('packing.getCostco');
        Route::post('/packing-list/uploadCostco/', 'Packing\PackingListController@uploadCostco')->name('packing.uploadCostco');
        Route::get('/packing-list/viewCostcoDetail/{plan_ref_number}', 'Packing\PackingListController@viewCostcoDetail')->name('packing.viewCostcoDetail');
        Route::get('/packing-list/getCostcoDetail/', 'Packing\PackingListController@getCostcoDetail')->name('packing.getCostcoDetail');
        // package plan ref
        Route::get('/packing-list/po-number/{po_number}/plan-ref/{plan_ref_number}/package', 'Packing\PackingListController@viewPackagePlanRefNumber')->name('packing.viewPackageDetailPlanRefNumber');
        //
        Route::get('/packing-list/po-number/package/get-data', 'Packing\PackingListController@viewPackageData')->name('packing.ajaxPackageDetailData');
        Route::get('/packing-list/po-number/package/print-preview', 'Packing\PackingListController@printPreview')->name('packing.printPreview');
        Route::post('/packing-list/po-number/package/set-completed', 'Packing\PackingListController@setCompleted')->name('packing.ajaxSetCompleted');
        Route::post('/packing-list/po-number/package/set-completed-all', 'Packing\PackingListController@setCompletedAll')->name('packing.ajaxSetCompletedAll');
        Route::post('/packing-list/po-number/package/generate_barcode', 'Packing\PackingListController@generateBarcode')->name('packing.ajaxGenerateBarcode');
        Route::post('/packing-list/po-number/package/view-template', 'Packing\PackingListController@viewTemplate')->name('packing.ajaxViewTemplate');

        // packing list cancel order
        Route::get('/packing-list-order', 'Packing\PackingListController@packingListCancel')->name('packing.packingListCancel');
        Route::post('/packing-list/modal-upload-cancel', 'Packing\PackingListController@modalUploadCancel')->name('packing.modalUploadCancel');

        Route::post('/packing-list/upload-excel-cancel', 'Packing\PackingListController@uploadExcelCancel')->name('packing.uploadExcelCancel');
        Route::post('/packing-list/revisi-excel-cancel', 'Packing\PackingListController@revisiExcelCancel')->name('packing.revisiExcelCancel');
        
        //history revisi
        Route::get('/history-revisi', 'Packing\HistoryRevisiController@index')->name('packing.historyRevisi');
        Route::get('/history-revisi/get-data', 'Packing\HistoryRevisiController@getData')->name('packing.ajaxGetHistory');

        //package detail
        Route::get('/package-detail', 'Packing\PackageDetailController@index')->name('packing.packageDetail');
        Route::get('/package-detail/get-data', 'Packing\PackageDetailController@getData')->name('packing.ajaxGetPackageDetail');

        //package preparation
        Route::get('/package-preparation', 'Packing\PackageDetailController@preparation')->name('packing.packagepreparation');
        Route::get('/package-preparation/get-data', 'Packing\PackageDetailController@getDataPreparation')->name('packing.ajaxGetPackagePreparation');
        Route::get('/package-preparation/delete-data', 'Packing\PackageDetailController@deleteDataPreparation')->name('packing.deleteDataPreparation');

        //package cancel
        Route::get('/package-cancel', 'Packing\PackageDetailController@packagecancel')->name('packing.packagecancel');
        Route::get('/package-cancel/get-data', 'Packing\PackageDetailController@getDataCancel')->name('packing.ajaxGetPackageCancel');
        Route::get('/package-cancel/delete-data', 'Packing\PackageDetailController@deleteDataCancel')->name('packing.deleteDataCancel');

        //request carton
        Route::get('/request-carton', 'Packing\PackageDetailController@requestcarton')->name('packing.requestcarton');
        Route::get('/request-carton/get-data', 'Packing\PackageDetailController@getDataRequestCarton')->name('packing.ajaxGetDataRequestCarton');
        Route::get('/request-carton/reject', 'Packing\PackageDetailController@reject')->name('packing.rejectCarton');
        Route::get('/request-carton/progress', 'Packing\PackageDetailController@progress')->name('packing.progressCarton');
        Route::get('/request-carton/done', 'Packing\PackageDetailController@done')->name('packing.doneCarton');

        Route::get('/request-carton/receive', 'Packing\PackageDetailController@receive')->name('packing.receiveCarton');

        //transfer style per po
        Route::get('/transfer-style', 'Packing\PackageDetailController@transferstyle')->name('packing.transferstyle');
        Route::get('/transfer-style/get-data', 'Packing\PackageDetailController@getDataTransfer')->name('packing.ajaxGetPackageTransfer');
        Route::get('/transfer-style/transfer-data', 'Packing\PackageDetailController@transferDataStyle')->name('packing.transferDataStyle');

        //PACKAGE RASIO
        Route::get('/package-rasio', 'Packing\PackageDetailController@packagerasio')->name('packing.packagerasio');
        Route::get('/package-rasio/get-data', 'Packing\PackageDetailController@getDataRasio')->name('packing.ajaxGetPackageRasio');
        Route::post('/update-manufacturing-size', 'Packing\PackageDetailController@updateManufacturingSize')->name('packing.updateManufacturingSize');

        //po sync from ERP
        Route::get('/po-sync', 'Packing\PackageDetailController@posync')->name('packing.posync');
        Route::get('/po-sync/export', 'Packing\PackageDetailController@exportPoSync')->name('packing.exportPoSync');
        Route::get('/get-po-sync', 'Packing\PackageDetailController@getDataPoSync')->name('packing.ajaxGetDataPoSync');

        //scan package
        Route::get('/scan-package', 'Packing\ScanPackageController@index')->name('packing.scanPackage');
        Route::get('/scan-package/search-package', 'Packing\ScanPackageController@searchPackage')->name('packing.ajaxSearchPackage');
        Route::post('/scan-package/status', 'Packing\ScanPackageController@setStatusPackage')->name('packing.ajaxSetStatusPackage');

        //sync plan from api aoi
        Route::get('/syncpackinglist', 'Packing\PackingListController@syncPackingList')->name('packing.syncpackinglist');

        Route::get('/edit-packinglist', 'Packing\PackingListController@editPl')->name('packing.editPl');
        Route::get('/edit-packinglist/get-data', 'Packing\PackingListController@getPackingList')->name('packing.getPackingList');
        Route::post('/edit-packinglist/edit-article', 'Packing\PackingListController@editBuyerItem')->name('packing.editBuyerItem');
        Route::post('/edit-packinglist/edit-customer', 'Packing\PackingListController@editCustomerSize')->name('packing.editCustomerSize');
        Route::post('/edit-packinglist/edit-manufacturing', 'Packing\PackingListController@editManufacturingSize')->name('packing.editManufacturingSize');

    });

    //ROUTE SEWING
    Route::prefix('sewing')->group(function () {
        Route::get('/line', 'Sewing\SewingController@ajaxGetLine')->name('sewing.ajaxGetLine');
        Route::get('/checkin', 'Sewing\SewingController@index')->name('sewing.checkin');
        Route::get('/checkout', 'Sewing\SewingController@checkout')->name('sewing.checkout');
        Route::post('/checkin/status/', 'Sewing\SewingController@setStatusCheckIn')->name('sewing.ajaxSetStatusCheckin');
        Route::post('/checkout/status/', 'Sewing\SewingController@setStatusCheckout')->name('sewing.ajaxSetStatusCheckout');

        //master sewing
        Route::get('/master', 'Sewing\SewingController@index2')->name('sewing.master');
        Route::get('/master/get-data', 'Sewing\SewingController@ajaxGetData')->name('sewing.ajaxGetData');
        Route::post('/master/add-sewing', 'Sewing\SewingController@addsewing')->name('sewing.addsewing');
        Route::get('/master/edit-sewing/{id}', 'Sewing\SewingController@edit')->name('sewing.edit');
        Route::post('/master/update-sewing', 'Sewing\SewingController@update')->name('sewing.update');
        Route::get('/master/delete-sewing/{id}', 'Sewing\SewingController@destroy')->name('sewing.delete');
    });

    //ROUTE INVENTORY
    Route::prefix('inventory')->group(function () {
        Route::get('/checkin', 'Inventory\InventoryController@index')->name('inventory.checkin');
        Route::get('/placement', 'Inventory\InventoryController@placement')->name('inventory.placement');
        Route::post('/checkin/status/', 'Inventory\InventoryController@setStatusCheckIn')->name('inventory.ajaxSetStatusCheckin');
        // check placement
        Route::get('/placement-check', 'Inventory\InventoryController@placementCheck')->name('inventory.stock');
        Route::post('/placement-check/checkstock', 'Inventory\InventoryController@checkstock')->name('inventory.ajaxCheckstock');
        //Route::post('/placement/getbarcode', 'Inventory\InventoryController@getBarcode')->name('inventory.ajaxGetBarcode'); //diumpetin dulu
        Route::post('/placement/getlocator', 'Inventory\InventoryController@getLocator')->name('inventory.ajaxGetLocator');
        Route::post('/placement/getArea', 'Inventory\InventoryController@getArea')->name('inventory.ajaxGetArea');
        Route::post('/placement/getLocatorByArea', 'Inventory\InventoryController@getLocatorByArea')->name('inventory.ajaxGetLocatorByArea');
        Route::post('/placement/checkout', 'Inventory\InventoryController@checkout')->name('inventory.ajaxCheckout');

        //AREA & LOCATION
        Route::get('/area', 'Inventory\AreaController@index')->name('area');
        Route::get('/area/get-data', 'Inventory\AreaController@ajaxGetData')->name('area.ajaxGetData');
        Route::get('/area/edit-area/{id}', 'Inventory\AreaController@edit')->name('area.edit');
        Route::post('/area/update-area', 'Inventory\AreaController@update')->name('area.update');
        Route::get('/area/delete-area/{id}', 'Inventory\AreaController@destroy')->name('area.delete');
        Route::post('/area/add-area', 'Inventory\AreaController@ajaxAddArea')->name('area.ajaxAddArea');

        Route::get('/location', 'Inventory\AreaController@index')->name('area');
        Route::get('/area/location', 'Inventory\AreaController@viewLocation')->name('area.location');
        Route::get('/area/get-data-location/{areaid}', 'Inventory\AreaController@ajaxGetLocation')->name('area.ajaxGetLocation');
        Route::get('/location/{id}/edit', 'Inventory\AreaController@editLocation')->name('location.edit');
        Route::post('/location/update', 'Inventory\AreaController@updateLocation')->name('location.update');
        Route::get('/area/location/delete/{id}', 'Inventory\AreaController@destroyLocation')->name('location.delete');
        Route::post('/area/location/add', 'Inventory\AreaController@ajaxAddLocation')->name('area.ajaxAddLocation');

        //print locator
        Route::get('/print-all-locator', 'Inventory\AreaController@printAllLocator')->name('area.printAllLocator');

        // print on locator rack
        Route::get('/print-rack-locator', 'Inventory\AreaController@printFG')->name('area.printFG');

        // print rack
        Route::get('/print-rack-FG', 'Inventory\AreaController@printRack')->name('area.rackFG');


        //by naufal setiawan move inventory
        Route::get('/move', 'Inventory\MoveController@index')->name('move');
        Route::get('/move/get-data', 'Inventory\MoveController@ajaxGetMove')->name('move.ajaxGetMove');
        Route::get('/move/movePo', 'Inventory\MoveController@movePo')->name('move.movePo');
        Route::get('/move/checkOut', 'Inventory\MoveController@checkOut')->name('move.checkOut');
        Route::post('/move/setCheckOut', 'Inventory\MoveController@setCheckOut')->name('move.setCheckOut');
        Route::get('/move/checkIn', 'Inventory\MoveController@checkIn')->name('move.checkIn');
        Route::post('/move/setCheckIn', 'Inventory\MoveController@setCheckIn')->name('move.setCheckIn');
        Route::get('/move/beaCukai', 'Inventory\MoveController@beaCukai')->name('move.beaCukai');
        Route::get('/move/getPoBc', 'Inventory\MoveController@getPoBc')->name('move.getPoBc');
        Route::post('/move/setBc', 'Inventory\MoveController@setBc')->name('move.setBc');

        // checkout revisi
        // Route::get('/checkout-revisi', 'Inventory\CheckoutRevisi@index')->name('checkout');
        // Route::post('/checkout-revisi/status', 'Inventory\CheckoutRevisi@ajaxSetStatus')->name('checkout.ajaxSetStatus');
    });

    //ROUTE QC INSPECT
    Route::prefix('qc-inspect')->group(function () {
        Route::get('/checkin', 'QcInspect\QcInspectController@index')->name('qcinspect.checkin');
        Route::get('/checkout', 'QcInspect\QcInspectController@checkout')->name('qcinspect.checkout');
        Route::post('/checkin/status/', 'QcInspect\QcInspectController@setStatusCheckIn')->name('qcinspect.ajaxSetStatusCheckin');
        Route::post('/checkout/status/', 'QcInspect\QcInspectController@setStatusCheckout')->name('qcinspect.ajaxSetStatusCheckout');
    });

    //ROUTE SHIPPING
    Route::prefix('shipping')->group(function () {
        Route::get('/checkin', 'Shipping\ShippingController@index')->name('shipping.checkin');
        Route::get('/checkout', 'Shipping\ShippingController@checkout')->name('shipping.checkout');
        Route::post('/checkin/status/', 'Shipping\ShippingController@setStatusCheckIn')->name('shipping.ajaxSetStatusCheckin');
        Route::post('/checkout/status/', 'Shipping\ShippingController@setStatusCheckout')->name('shipping.ajaxSetStatusCheckout');
    });

    //ROUTE REPORTING
    Route::prefix('report')->group(function () {

        //packing calculate
        Route::get('/packing-calculate', 'Report\PackingController@packingcalculate')->name('report.packingcalculate');
        Route::get('/packing-calculate/export', 'Report\PackingController@exportPackingCalculate')->name('report.exportPackingCalculate');
        Route::post('/get-report-packing-calculate', 'Report\PackingController@getDataPackingCalculate')->name('report.ajaxGetDataPackingCalculate');

        //packing
        Route::get('/packing', 'Report\PackingController@index')->name('report.packing');
        Route::get('/packing/export', 'Report\PackingController@exportPacking')->name('report.exportPacking');
        Route::post('/get-report-packing', 'Report\PackingController@getDataPacking')->name('report.ajaxGetDataPacking');

        //sewing
        Route::get('/sewing', 'Report\SewingController@index')->name('report.sewing');
        Route::get('/sewing/export', 'Report\SewingController@exportSewing')->name('report.exportSewing');
        Route::post('/get-report-sewing', 'Report\SewingController@getDataSewing')->name('report.ajaxGetDataSewing');

        //sewing completed
        Route::get('/sewing-out', 'Report\SewingController@sewingOut')->name('report.sewingOut');
        Route::get('/sewing/export-out', 'Report\SewingController@exportSewingOut')->name('report.exportSewingOut');
        Route::post('/get-report-sewing-out', 'Report\SewingController@ajaxGetDataSewingOut')->name('report.ajaxGetDataSewingOut');

        //inventory
        Route::get('/inventory', 'Report\InventoryController@index')->name('report.inventory');
        Route::get('/inventory/export', 'Report\InventoryController@exportInventory')->name('report.exportInventory');
        Route::post('/get-report-inventory', 'Report\InventoryController@getDataInventory')->name('report.ajaxGetDataInventory');

        //inventory per po
        Route::get('/inventory-po', 'Report\InventoryController@inventoryPo')->name('report.inventoryPo');
        Route::get('/inventory-po/export', 'Report\InventoryController@exportInventoryPo')->name('report.exportInventoryPo');
        Route::post('/get-report-inventory-po', 'Report\InventoryController@getDataInventoryPo')->name('report.ajaxGetDataInventoryPo');

        //back to sewing
        Route::get('/backtosewing', 'Report\InventoryController@backtosewing')->name('report.backtosewing');
        Route::get('/backtosewing/export', 'Report\InventoryController@exportBackToSewing')->name('report.exportBackToSewing');
        Route::post('/get-report-backtosewing', 'Report\InventoryController@getDataBackTosewing')->name('report.ajaxGetDataBackToSewing');

        //stock
        Route::get('/stock', 'Report\StockController@index')->name('report.stock');
        Route::get('/stock/export', 'Report\StockController@exportStock')->name('report.exportStock');
        Route::post('/get-report-stock', 'Report\StockController@getDataStock')->name('report.ajaxGetDataStock');
        // stock cancel
        Route::get('/stock-cancel', 'Report\StockController@stockCancel')->name('report.stockCancel');
        Route::post('/get-report-stock-cancel', 'Report\StockController@getDatastockCancel')->name('report.getDatastockCancel');
        Route::get('/stock/export-cancel', 'Report\StockController@exportStockCancel')->name('report.exportStockCancel');

        //ending balance
        Route::get('/stock-balance', 'Report\StockController@endingBalance')->name('report.endingBalance');
        Route::get('/stock-balance/export', 'Report\StockController@exportStockBalance')->name('report.exportStockBalance');
        Route::post('/get-report-stock-balance', 'Report\StockController@getDataStockBalance')->name('report.ajaxGetDataStockBalance');

        //stock balance by naufal
        Route::get('/balance-stock', 'Report\StockController@stockBalance')->name('report.stockBalance');
        Route::post('/get-balance-stock', 'Report\StockController@ajaxGetStockBalance')->name('report.ajaxGetStockBalance');
        Route::get('/export-balance-stock', 'Report\StockController@ajaxExportStockBalance')->name('report.ajaxExportStockBalance');

        // stock balance acc 
        Route::get('/acc/balance-stock', 'Report\StockController@stockBalanceAcc')->name('report.stockBalanceAcc');
        Route::get('/acc/get-balance-stock', 'Report\StockController@getStockBalanceAcc')->name('report.getStockBalanceAcc');
        Route::get('/acc/export-balance-stock', 'Report\StockController@exportStockBalanceAcc')->name('report.exportStockBalanceAcc');

        // report bapb
        Route::get('/bapb', 'Report\BapbController@index')->name('report.bapbindex');
        Route::post('/bapb/get-data', 'Report\BapbController@getDataBapb')->name('report.getDataBapb');
        Route::get('/bapb/export-data', 'Report\BapbController@exportBapb')->name('report.exportBapb');

        //dashboard locator
        Route::get('/dashboard-locator', 'Report\DashboardLocatorController@index')->name('report.dashboardlocator');
        Route::get('/dashboard-locator/export', 'Report\DashboardLocatorController@exportDashboardLocator')->name('report.exportDashboardLocator');
        Route::post('/get-report-dashboard-locator', 'Report\DashboardLocatorController@getDataDashboardLocator')->name('report.ajaxGetDataDashboardLocator');
        Route::post('/get-list-locator', 'Report\DashboardLocatorController@getListLocator')->name('report.ajaxGetListLocator');
        Route::post('/update-psd', 'Report\DashboardLocatorController@updatePSD')->name('report.updatePSD');
        Route::post('/update-invoice', 'Report\DashboardLocatorController@updateInvoice')->name('report.updateInvoice');
        Route::post('/result-dashboard', 'Report\DashboardLocatorController@resultDashboard')->name('report.ajaxDashboardSum');

        //qcinspect
        Route::get('/qcinspect', 'Report\QcinspectController@index')->name('report.qcinspect');
        Route::get('/qcinspect/export', 'Report\QcinspectController@exportQcinspect')->name('report.exportQcinspect');
        Route::post('/get-report-qcinspect', 'Report\QcinspectController@getDataQcinspect')->name('report.ajaxGetDataQcinspect');

        //shipping
        Route::get('/shipping', 'Report\ShippingController@index')->name('report.shipping');
        Route::get('/shipping/export', 'Report\ShippingController@exportShipping')->name('report.exportShipping');
        Route::post('/get-report-shipping', 'Report\ShippingController@getDataShipping')->name('report.ajaxGetDataShipping');

        Route::get('/shipping-in', 'Report\ShippingController@shippingIn')->name('report.shippingIn');
        Route::get('/shipping-in/export', 'Report\ShippingController@exportShippingIn')->name('report.exportShippingIn');
        Route::post('/get-report-shipping-in', 'Report\ShippingController@getDataShippingIn')->name('report.ajaxGetDataShippingIn');

        // list stuffing per po
        Route::get('/shipping-completed', 'Report\ShippingController@shippingCompleted')->name('report.shippingCompleted');
        Route::get('/shipping-completed/export', 'Report\ShippingController@exportShippingCompleted')->name('report.exportShippingCompleted');
        Route::post('/get-report-shipping-completed', 'Report\ShippingController@getDataShippingCompleted')->name('report.ajaxGetDataShippingCompleted');

        //list request carton
        Route::get('/request-carton', 'Report\RequestCartonController@listrequest')->name('report.listrequest');
        Route::get('/request-carton/export', 'Report\RequestCartonController@exportListRequest')->name('report.exportListRequest');
        Route::post('/get-report-request-carton', 'Report\RequestCartonController@getDataListRequest')->name('report.ajaxGetDataListRequest');

        //list request carton detail
        Route::get('/request-carton-detail', 'Report\RequestCartonController@listrequestdetail')->name('report.listrequestdetail');
        Route::get('/request-carton-detail/export', 'Report\RequestCartonController@exportListRequestDetail')->name('report.exportListRequestDetail');
        Route::post('/get-report-request-carton-detail', 'Report\RequestCartonController@getDataListRequestDetail')->name('report.ajaxGetDataListRequestDetail');
        // by naufal
        Route::get('/request-carton-report', 'Report\RequestCartonController@reportRequestCarton')->name('report.requestCarton');
        Route::post('/get-request-carton', 'Report\RequestCartonController@getReportRequestCarton')->name('report.getReportRequestCarton');
        Route::get('/get-report-request-carton', 'Report\RequestCartonController@exportRequestCarton')->name('report.exportRequestCarton');

        //list plan load delivery order
        Route::get('/delivery-order', 'Report\EximController@listdeliveryorder')->name('report.listdeliveryorder');
        Route::get('/delivery-order/export', 'Report\EximController@exportListDelivery')->name('report.exportListDelivery');
        Route::post('/get-report-delivery-order', 'Report\EximController@getDataListDelivery')->name('report.ajaxGetDataListDelivery');

        //report planload new
        Route::get('/invoice', 'Report\PlanloadController@reportInvoice')->name('report.reportInvoice');
        Route::get('/invoice/get-data', 'Report\PlanloadController@getReportInvoice')->name('report.getReportInvoice');
        Route::get('/invoice/export-data', 'Report\PlanloadController@exportReportInvoice2')->name('report.exportReportInvoice');

        // report rekon accounting
        Route::get('/rekon/inventory', 'Report\RekonController@inventoryIn')->name('report.Accinvetory');
        Route::post('/rekon/inventory/get-data', 'Report\RekonController@getDataInventory')->name('report.Accgetinventory');
        Route::get('/rekon/inventory/export-data', 'Report\RekonController@exportInventory')->name('report.Accexptinventory');

        Route::get('/rekon/shipment', 'Report\RekonController@shipmentOut')->name('report.Accshipment');
        Route::post('/rekon/shipment/get-data', 'Report\RekonController@getDataShipment')->name('report.Accgetshipping');
        Route::get('/rekon/shipment/export-data', 'Report\RekonController@exportShipment')->name('report.Accexptshipment');

        Route::get('/rekon/back-to-sewing', 'Report\RekonController@backToSewing')->name('report.Accbacktosewing');
        Route::post('/rekon/back-to-sewing/get-data', 'Report\RekonController@getDataBackToSewing')->name('report.Accgetbacktosewing');
        Route::get('/rekon/back-to-sewing/export-data', 'Report\RekonController@exportBackToSewing')->name('report.Accexptbacktosewing');

        Route::get('/rekon/balance-stock', 'Report\RekonController@balanceStock')->name('report.Accbalance');
        Route::post('/rekon/balance-stock/get-data', 'Report\RekonController@getDataBalanceStock')->name('report.Accgetbalance');
        Route::get('/rekon/balance-stock/export-data', 'Report\RekonController@exportBalanceStock')->name('report.Accexptbalance');

        Route::get('/rekon/delete-packinglist', 'Report\RekonController@deletePackingList')->name('report.Accdeletepl');
        Route::post('/rekon/delete-packinglist/get-data', 'Report\RekonController@getDataDeletePL')->name('report.Accgetdeletepl');
        Route::get('/rekon/delete-packinglist/export-data', 'Report\RekonController@exportDeletePL')->name('report.Accexptdeletepl');

        //tarikan rekon auto cron
        Route::get('/rekon/all', 'Report\RekonController@rekonAll')->name('report.rekonAll');
        Route::get('/rekon/all/inventory', 'Report\RekonController@rekonInv')->name('report.rekonInv');
        Route::get('/rekon/all/shipment', 'Report\RekonController@rekonShip')->name('report.rekonShip');
        Route::get('/rekon/all/backtosewing', 'Report\RekonController@rekonBts')->name('report.rekonBts');
        Route::get('/rekon/all/balancestock', 'Report\RekonController@rekonBalc')->name('report.rekonBalc');
        Route::get('/rekon/all/deletepl', 'Report\RekonController@rekonDelpl')->name('report.rekonDel');


        // metal finding
        Route::get('/metal-finding', 'Report\MetalController@metalReport')->name('report.metalReport');
        Route::get('/metal-finding/ajaxGetData', 'Report\MetalController@ajaxGetData')->name('report.ajaxGetData');
        Route::get('/metal-finding/exportMetal', 'Report\MetalController@exportMetal')->name('report.exportMetal');

    });

    //ROUTE EXIM
    Route::prefix('exim')->group(function () {
        Route::get('/plan-load', 'Exim\EximController@planload')->name('exim.planload');

        Route::post('/upload-excel-plan', 'Exim\EximController@uploadexcelPlan')->name('exim.uploadexcelPlan');

        // update plan/info
        Route::post('/update-plan', 'Exim\EximController@updatePlanLoad')->name('exim.updatePlanLoad');
        Route::post('/update-shipmode', 'Exim\EximController@updateShipmode')->name('exim.updateShipmode');
        Route::post('/update-mdd', 'Exim\EximController@updateMdd')->name('exim.updateMdd');
        Route::post('/update-remark', 'Exim\EximController@updateRemark')->name('exim.updateRemark');
        Route::post('/update-psfd-from', 'Exim\EximController@updatePsfdFrom')->name('exim.updatePsfdFrom');
        Route::post('/update-psfd-to', 'Exim\EximController@updatePsfdTo')->name('exim.updatePsfdTo');
 
        // get numbering invoice
        Route::get('/invoice-serial', 'Exim\EximController@invoiceSerial')->name('exim.invoiceSerial');
 
        // get numbering SJ
        Route::get('/sj-serial', 'Exim\EximController@sjSerial')->name('exim.sjSerial');

        // invoice
        Route::get('/plan-invoice', 'Exim\EximController@planinvoice')->name('exim.invoice');
        Route::get('/get-plan', 'Exim\EximController@getplanload')->name('exim.ajaxplanload');
        Route::get('/plan-load/export', 'Exim\EximController@exportplanload')->name('exim.exportplanload');
        Route::get('/detail-invoice', 'Exim\EximController@detailInvoice')->name('exim.detailInvoice');//by naufal
        Route::get('/detail-invoice/get-detail-invoice', 'Exim\EximController@getdDetailInvoice')->name('exim.getdDetailInvoice');//by naufal
        Route::post('/detail-invoice/delete-po', 'Exim\EximController@deletePO')->name('exim.deletePO');//by naufal
        Route::get('/detail-invoice/export-inv', 'Exim\EximController@exportDetailInvoice')->name('exim.exportDetailInvoice');
        Route::get('/detail-invoice/get-remark', 'Exim\EximController@getRemark')->name('exim.getRemark');
        Route::post('/detail-invoice/set-remark', 'Exim\EximController@setRemark')->name('exim.setRemark');
        Route::get('/detail-invoice/get-remarkPlan', 'Exim\EximController@getRemarkPlan')->name('exim.getRemarkPlan');
        Route::post('/detail-invoice/set-remarkPlan', 'Exim\EximController@setRemarkPlan')->name('exim.setRemarkPlan');

        //by naufal
 
        // group invoice
        Route::get('/group-invoice', 'Exim\EximController@groupinvoice')->name('exim.groupinvoice');
        Route::get('/get-invoice', 'Exim\EximController@getinvoice')->name('exim.ajaxinvoice');
        Route::get('/get-invoice/export', 'Exim\EximController@exportinvoice')->name('exim.exportinvoice');

        // update invoice
        Route::post('/update-invoice', 'Exim\EximController@updateInvoice')->name('exim.updateInvoice');

        // cancel invoice
        Route::post('/cancel-invoice', 'Exim\EximController@cancelInvoice')->name('exim.cancelInvoice');

        // get invoice cancel
        Route::get('/cancel-invoice/search', 'Exim\EximController@ajaxGetInvoiceCancel')->name('exim.ajaxGetInvoiceCancel');

        // get ajax invoice cust
        Route::get('/get-plan/ajax', 'Exim\EximController@getplanloadajax')->name('exim.ajaxplanloadajax');
        // get ajax invoice mdd
        Route::get('/get-plan/ajax-mdd', 'Exim\EximController@getplanloadajaxmdd')->name('exim.ajaxplanloadajaxmdd');
        // get ajax invoice psfd
        Route::get('/get-plan/ajax-psfd', 'Exim\EximController@getplanloadajaxpsfd')->name('exim.ajaxplanloadajaxpsfd');
        // get ajax invoice lc
        Route::get('/get-plan/ajax-lc', 'Exim\EximController@getplanloadajaxlc')->name('exim.ajaxplanloadajaxlc');
        // get ajax invoice priority code
        Route::get('/get-plan/ajax-priority', 'Exim\EximController@getplanloadajaxpriority')->name('exim.ajaxplanloadajaxpriority');
        // get ajax invoice whs code
        Route::get('/get-plan/ajax-whs', 'Exim\EximController@getplanloadajaxwhs')->name('exim.ajaxplanloadajaxwhs');

        // get ajax group invoice plan
        Route::get('/group-invoice/ajax-plan', 'Exim\EximController@getgroupinvoiceajaxplan')->name('exim.ajaxgroupinvoiceajaxplan');
        // get ajax group invoice shipmode
        Route::get('/group-invoice/ajax-shipmode', 'Exim\EximController@getgroupinvoiceajaxshipmode')->name('exim.ajaxgroupinvoiceajaxshipmode');
        // get ajax group invoice fwd
        Route::get('/group-invoice/ajax-fwd', 'Exim\EximController@getgroupinvoiceajaxfwd')->name('exim.ajaxgroupinvoiceajaxfwd');
        // get ajax group invoice destination
        Route::get('/group-invoice/ajax-destination', 'Exim\EximController@getgroupinvoiceajaxdestination')->name('exim.ajaxgroupinvoiceajaxdestination');
 
        //by naufal setiawan
        Route::post('update-fwd','Exim\EximController@updateFwd')->name('exim.updateFwd');
        Route::post('update-ship','Exim\EximController@updateShip')->name('exim.updateShip');
        Route::post('update-planstuffing','Exim\EximController@updatePlanStuffing')->name('exim.updatePlanStuffing');
        Route::post('update-so','Exim\EximController@updateSo')->name('exim.updateSo');

        // plan stuffing
        Route::get('/plan-stuffing', 'Exim\EximController@planstuffing')->name('exim.planstuffing');
        Route::post('/get-plan-stuffing', 'Exim\EximController@getDataPlanStuffing')->name('exim.ajaxGetDataPlanStuffing');

        Route::get('/plan-stuffing/export', 'Exim\EximController@exportplanstuffing')->name('exim.exportplanstuffing');
        
        // template plan load
        Route::post('/plan-load-list/view-template-header', 'Exim\EximController@viewTemplateHeader')->name('exim.ajaxViewTemplateHeader');
        Route::get('/plan-load-list/view-template-header/export', 'Exim\EximController@viewTemplateHeaderExport')->name('exim.ajaxViewTemplateHeaderExport');

        // sinkron plan load to table plan load invoice
        Route::get('/plan-load-exim', 'Exim\EximController@planloadexim')->name('exim.planloadexim');

        Route::post('update-planref','Exim\EximController@updatePlanRef')->name('exim.updatePlanRef');
        Route::post('update-fwdinv','Exim\EximController@updateFwdinv')->name('exim.updateFwdinv');
        Route::post('update-shipinv','Exim\EximController@updateShipinv')->name('exim.updateShipinv');

        Route::get('/detail-delivery', 'Exim\EximController@detailDelivery')->name('exim.detailDelivery');
        Route::get('/detail-delivery/get-data', 'Exim\EximController@ajaxGetDelivery')->name('exim.ajaxGetDelivery');
        Route::post('/detail-delivery/delete-inv', 'Exim\EximController@deleteInv')->name('exim.deleteInv');

    });



    // NEW PLAN LOAD
    Route::prefix('plan-load')->group(function () {
        
     


        //draf planload
        Route::get('/draf', 'Plan\PlanLoadController@drafPlan')->name('planload.drafPlan');
        Route::get('/draf/get-data', 'Plan\PlanLoadController@getDataDrafPlan')->name('planload.getDataDrafPlan');
        Route::get('/draf/get-data/filter-brand', 'Plan\PlanLoadController@ajaxFilBrand')->name('planload.ajaxFilBrand');
        Route::get('/draf/get-data/filter-doctype', 'Plan\PlanLoadController@ajaxDocType')->name('planload.ajaxDocType');
        Route::get('/draf/export-data', 'Plan\PlanLoadController@exportDraf')->name('planload.exportDraf');
        Route::post('/draf/upload-gtn', 'Plan\PlanLoadController@uploadDraf')->name('planload.uploadDraf');
        Route::get('/draf/get-data/get-remark', 'Plan\PlanLoadController@getRemark')->name('planload.getRemark');
        Route::post('/draf/get-data/set-remark', 'Plan\PlanLoadController@setRemark')->name('planload.setRemark');
        Route::get('/draf/delete-data', 'Plan\PlanLoadController@deletePoDraf')->name('planload.deletePoDraf');
        Route::get('/draf/download-template', 'Plan\PlanLoadController@templateUpload')->name('planload.templateUpload');

        //edit draf po
        Route::get('/draf/edit-po', 'Plan\PlanLoadController@editPoDraf')->name('planload.editPoDraf');
        Route::get('/draf/get-po', 'Plan\PlanLoadController@getDataPoDraf')->name('planload.getDataPoDraf');
        Route::post('/draf/update-planref', 'Plan\PlanLoadController@updatePlanRef')->name('planload.updatePlanRef');
        Route::post('/draf/update-subsidary', 'Plan\PlanLoadController@updateSubsidary')->name('planload.updateSubsidary');
        Route::post('/draf/update-psfd', 'Plan\PlanLoadController@updatePsfd')->name('planload.updatePsfd');
        Route::post('/draf/update-whs', 'Plan\PlanLoadController@updateWhs')->name('planload.updateWhs');

        // invoice storage
        Route::get('/invoice', 'Plan\PlanLoadController@invoiceStorage')->name('planload.invoiceStorage');
        Route::get('/invoice/get-data', 'Plan\PlanLoadController@getDataInvStrg')->name('planload.getDataInvStrg');
        Route::get('/invoice/get-data/f-cust', 'Plan\PlanLoadController@ajaxGetCustomer')->name('planload.ajaxGetCustomer');
        Route::get('/invoice/get-data/f-mdd', 'Plan\PlanLoadController@ajaxGetMdd')->name('planload.ajaxGetMdd');
        Route::get('/invoice/get-data/f-psfd', 'Plan\PlanLoadController@ajaxGetPsfd')->name('planload.ajaxGetPsfd');
        Route::get('/invoice/get-data/f-priority', 'Plan\PlanLoadController@ajaxGetPriority')->name('planload.ajaxGetPriority');
        Route::get('/invoice/get-data/f-whs', 'Plan\PlanLoadController@ajaxGetWhs')->name('planload.ajaxGetWhs');
        Route::get('/invoice/get-remark', 'Plan\PlanLoadController@invGetRemark')->name('planload.invGetRemark');
        Route::post('/invoice/set-remark', 'Plan\PlanLoadController@invSetRemark')->name('planload.invSetRemark');
        Route::post('/invoice/create-inv', 'Plan\PlanLoadController@createInvoice')->name('planload.createInvoice');
        Route::post('/invoice/update-mdd', 'Plan\PlanLoadController@updateMdd')->name('planload.updateMdd');
        Route::post('/invoice/update-fwd', 'Plan\PlanLoadController@updateFwd')->name('planload.updateFwd');
        Route::post('/invoice/update-priority', 'Plan\PlanLoadController@updatePriority')->name('planload.updatePriority');
        Route::post('/invoice/update-shipmode', 'Plan\PlanLoadController@updateShipmode')->name('planload.updateShipmode');



        // invoice detail
        Route::get('/invoice-detail', 'Plan\PlanLoadController@invoiceDetail')->name('planload.invoiceDetail');
        Route::get('/invoice-detail/get-data', 'Plan\PlanLoadController@getDetailInvoice')->name('planload.getDetailInvoice');
        Route::post('/invoice-detail/update-exfact', 'Plan\PlanLoadController@updateExfact')->name('planload.updateExfact');
        Route::post('/invoice-detail/cancel-invoice', 'Plan\PlanLoadController@cancelInvoice')->name('planload.cancelInvoice');
        Route::get('/invoice-detail/get-remark', 'Plan\PlanLoadController@getRemarkDtInv')->name('planload.getRemarkDtInv');
        Route::post('/invoice-detail/set-remark', 'Plan\PlanLoadController@setRemarkDetInv')->name('planload.setRemarkDetInv');
        Route::get('/invoice-detail/export-data', 'Plan\PlanLoadController@exportDetailInvoice')->name('planload.exportDetailInvoice');

        //delivery order
        Route::get('/delivery-order', 'Plan\PlanLoadController@deliveryOrder')->name('planload.deliveryOrder');
        Route::get('/delivery-order/get-data', 'Plan\PlanLoadController@getDataDO')->name('planload.getDataDO');
        Route::get('/delivery-order/ajaxGetShip', 'Plan\PlanLoadController@ajaxGetShip')->name('planload.ajaxGetShip');
        Route::get('/delivery-order/ajaxGetFwd', 'Plan\PlanLoadController@ajaxGetFwd')->name('planload.ajaxGetFwd');
        Route::get('/delivery-order/ajaxGetWhsAdd', 'Plan\PlanLoadController@ajaxGetWhsAdd')->name('planload.ajaxGetWhsAdd');
        Route::get('/delivery-order/getTruck', 'Plan\PlanLoadController@getTruck')->name('planload.getTruck');
        Route::post('/delivery-order/create-delivery-order', 'Plan\PlanLoadController@createDO')->name('planload.createDO');
    
        Route::get('/delivery-order/detail-delivery', 'Plan\PlanLoadController@detailDelivery')->name('planload.detailDelivery');
        Route::get('/delivery-order/get-detail', 'Plan\PlanLoadController@getDetailDelivery')->name('planload.getDetailDelivery');
        Route::get('/delivery-order/getSetting', 'Plan\PlanLoadController@getSetting')->name('planload.getSetting');
        Route::post('/delivery-order/update-delivery', 'Plan\PlanLoadController@updateDelivery')->name('planload.updateDelivery');
        Route::get('/delivery-order/export-detail', 'Plan\PlanLoadController@exportDetailDelivery')->name('planload.exportDetailDelivery');
        Route::get('/delivery-order/export-truck-detail', 'Plan\PlanLoadController@exportTruckDetail')->name('planload.exportTruckDetail');
        Route::post('/delivery-order/cancel-delivery', 'Plan\PlanLoadController@cancelDO')->name('planload.cancelDO');
        Route::post('/delivery-order/update-updateDO', 'Plan\PlanLoadController@updateDO')->name('planload.updateDO');

    });

    // surat jalan
    Route::prefix('delivery-order')->group(function () {

        Route::get('', 'Delivery\DeliveryOrderController@index')->name('delivery.index');
        Route::get('/get-data-do', 'Delivery\DeliveryOrderController@getDataDO')->name('delivery.getDataDO');
        Route::get('/get-detail-do', 'Delivery\DeliveryOrderController@getdetailDO')->name('delivery.getdetailDO');
        Route::get('/export-do', 'Delivery\DeliveryOrderController@exportDO')->name('delivery.exportDO');

    });


    // free stock
    Route::prefix('free-stock')->group(function () {

        Route::get('/checkin', 'FreeStock\FreeStockController@checkIn')->name('freestock.checkIn');
        Route::post('/checkin/ajaxSetChcekIn', 'FreeStock\FreeStockController@ajaxSetChcekIn')->name('freestock.ajaxSetChcekIn');
        Route::get('/list', 'FreeStock\FreeStockController@listFreeStock')->name('freestock.listFreeStock');
        Route::get('/list/get-data', 'FreeStock\FreeStockController@getListFreeStock')->name('freestock.getListFreeStock');
        Route::get('/list/export-data', 'FreeStock\FreeStockController@exportListFreeStock')->name('freestock.exportListFreeStock');
        Route::get('/list/calculate', 'FreeStock\FreeStockController@stockCalculate')->name('freestock.stockCalculate');
        Route::get('/allocation', 'FreeStock\FreeStockController@allocation')->name('freestock.allocation');
        Route::get('/allocation/export-template', 'FreeStock\FreeStockController@templateAllocation')->name('freestock.templateAllocation');
        Route::post('/allocation/upload-data', 'FreeStock\FreeStockController@uploadAllocation')->name('freestock.uploadAllocation');
        Route::get('/allocation/get-allocation', 'FreeStock\FreeStockController@getDataAllocation')->name('freestock.getDataAllocation');
        Route::get('/checkout', 'FreeStock\FreeStockController@checkOut')->name('freestock.checkOut');
        Route::post('/checkin/ajaxSetChcekOut', 'FreeStock\FreeStockController@ajaxSetChcekOut')->name('freestock.ajaxSetChcekOut');
    });
    

    //accouting sto
    Route::prefix('stock-opname')->group(function () {

        Route::get('/scan-sto', 'Accouting\StoController@scanSto')->name('accSto.scanSto');
        Route::get('/scan-sto/get-locator', 'Accouting\StoController@getLocator')->name('accSto.getLocator');
        Route::post('/scan-sto/checkScan', 'Accouting\StoController@checkScan')->name('accSto.checkScan');
        Route::get('/report-scan-sto', 'Accouting\StoController@reportSto')->name('accSto.reportSto');
        Route::get('/report-scan-sto/get-data', 'Accouting\StoController@ajaxGetDataSto')->name('accSto.ajaxGetDataSto');
        Route::get('/report-scan-sto/export-data', 'Accouting\StoController@exportGetDataSto')->name('accSto.exportGetDataSto');
        Route::get('/report-final-sto', 'Accouting\StoController@finalReportSto')->name('accSto.finalReportSto');
        Route::get('/report-final-sto/get-data', 'Accouting\StoController@ajaxGetDataFinal')->name('accSto.ajaxGetDataFinal');
        Route::get('/report-final-sto/export-data', 'Accouting\StoController@exportFinalSto')->name('accSto.exportFinalSto');
    });


    Route::prefix('metal')->group(function () {
        // operator metal
        Route::get('/scan-metal', 'Metal\MetalController@scanMetal')->name('metalfind.scanMetal');
        Route::post('/scan-metal/ajaxScanMetal', 'Metal\MetalController@ajaxScanMetal')->name('metalfind.ajaxScanMetal');
        Route::get('/scan-metal/list', 'Metal\MetalController@listScanMetal')->name('metalfind.listScanMetal');
        Route::get('/scan-metal/list/ajaxListScanMetal', 'Metal\MetalController@ajaxListScanMetal')->name('metalfind.ajaxListScanMetal');
        // operator metal

        // pso metal
        Route::get('/verify', 'Metal\MetalController@psoVerify')->name('metalpso.psoVerify');
        Route::get('/verify/ajaxListVerify', 'Metal\MetalController@ajaxListVerify')->name('metalpso.ajaxListVerify');
        Route::get('/verify/getMetFind', 'Metal\MetalController@getMetFind')->name('metalpso.getMetFind');
        Route::post('/verify/changeStatus', 'Metal\MetalController@changeStatus')->name('metalpso.changeStatus');
        // pso metal
    });
    
});


// route for cron
// po master sync
Route::prefix('/cron')->group(function(){
    Route::get('/po-master-sync', 'Cron\SynchronController@poMasterSync')->name('cron.poMasterSync');
    Route::get('/remark-cancel', 'Cron\SynchronController@remarkCancel')->name('cron.remarkCancel');
    Route::get('/syncpo/{$po_number}', 'Cron\SynchronController@syncpo')->name('cron.syncpo');

    //export rekon
    Route::get('/rekon-all', 'Cron\SynchronController@getDate')->name('cron.getDate');

    Route::get('/rekoninv', 'Cron\SynchronController@rekInventory')->name('cron.rekInventory');
    Route::get('/rekonship', 'Cron\SynchronController@rekShipping')->name('cron.rekShipping');
    Route::get('/rekonbts', 'Cron\SynchronController@rekBacktosewing')->name('cron.rekBacktosewing');
    Route::get('/rekonbalance', 'Cron\SynchronController@rekBalanceStock')->name('cron.rekBalanceStock');
    Route::get('/rekondelpl', 'Cron\SynchronController@rekDeletePL')->name('cron.rekDeletePL');
});


